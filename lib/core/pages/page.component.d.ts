import { OnDestroy, OnInit } from '@angular/core';
import { PageService } from './page.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
export declare class PageComponent implements OnInit, OnDestroy {
    private route;
    private router;
    private pages;
    appName: string;
    page$: Observable<{
        name: any;
        description: any;
        widgets: any;
    }>;
    subscription: any;
    constructor(route: ActivatedRoute, router: Router, pages: PageService);
    ngOnInit(): void;
    trackByFn(idx: number, item: any): string;
    ngOnDestroy(): void;
}
