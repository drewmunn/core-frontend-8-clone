export declare class BlockService {
    blocks: any;
    constructor();
    replaceBlocks(blocks: any): void;
    getBlocks(): any;
    hasBlock(identifier: any): any;
    getBlock(identifier: any): any;
    addBlock(identifier: any, component: any): void;
}
