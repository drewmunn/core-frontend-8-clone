import { CacheService } from '../cache.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
export declare class PageService extends CacheService {
    protected router: Router;
    protected api: HttpClient;
    page: any;
    breadcrumbs: {};
    constructor(router: Router, api: HttpClient);
    refresh(): void;
    setCurrentPage(page: any): void;
    getCurrentPage(): any;
    getAll(headers?: {}): any;
    getPage(identifier: any, blockInteraction?: boolean, headers?: {}): any;
    getPages(headers?: {}): any;
    getBlock(blockId: any): any;
    loadBlock(blockId: any): any;
    sortBlocks(blocks: any): void;
    getBreadcrumbs(requestedPageId: number): any;
    performSearch(query: string): void;
}
