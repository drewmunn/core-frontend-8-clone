import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { PageService } from './page.service';
import { SpinnerService } from '../utils/spinner/spinner.service';
import { TranslateService } from './translate.service';
export declare class SearchService extends PageService {
    protected router: Router;
    protected api: HttpClient;
    protected spinner: SpinnerService;
    protected translate: TranslateService;
    query: string;
    limit: number;
    offset: number;
    results: any[];
    totalResults: number;
    eddie: any;
    constructor(router: Router, api: HttpClient, spinner: SpinnerService, translate: TranslateService);
    refresh(): void;
    setQuery(query: string): void;
    getQuery(): string;
    performSearch(query: string): any;
    getTotalResults(): number;
}
