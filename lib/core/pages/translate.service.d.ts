import { OnDestroy } from '@angular/core';
import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { CacheService } from '../cache.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
export declare class TranslateService extends CacheService implements TranslateLoader, OnDestroy {
    protected router: Router;
    protected api: HttpClient;
    constructor(router: Router, api: HttpClient);
    translations: Observable<any>;
    defaultLanguage: string;
    getTranslation(lang: string): Observable<any>;
    getLanguages(): any;
    getTranslations(languageCode: any): any;
    loadTranslations(languageCode: any): any;
    getDefaultLanguage(): string;
    setDefaultLanguage(languageCode: string): void;
    ngOnDestroy(): void;
}
