import { NavigationState } from './navigation.reducer';
export declare const selectNavigationState: import("@ngrx/store").MemoizedSelector<object, NavigationState, import("@ngrx/store").DefaultProjectorFn<NavigationState>>;
export declare const selectNavigationItems: import("@ngrx/store").MemoizedSelector<object, import("./navigation.reducer").NavigationItem[], import("@ngrx/store").DefaultProjectorFn<import("./navigation.reducer").NavigationItem[]>>;
export declare const selectFilter: import("@ngrx/store").MemoizedSelector<object, {
    active: boolean;
    text: string;
}, import("@ngrx/store").DefaultProjectorFn<{
    active: boolean;
    text: string;
}>>;
export declare const selectResult: import("@ngrx/store").MemoizedSelector<object, {
    active: boolean;
    total: number;
    matched: number;
}, import("@ngrx/store").DefaultProjectorFn<{
    active: boolean;
    total: number;
    matched: number;
}>>;
