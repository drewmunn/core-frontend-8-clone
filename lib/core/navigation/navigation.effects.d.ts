import { Actions } from '@ngrx/effects';
import { BreakpointObserver } from '@angular/cdk/layout';
export declare class NavigationEffects {
    private actions$;
    mapToActiveUrl$: import("rxjs").Observable<{
        url: string;
    } & import("@ngrx/store/src/models").TypedAction<"[Navigation] Active Url">> & import("@ngrx/effects").CreateEffectMetadata;
    mobileNavigation$: import("rxjs").Observable<{
        open: boolean;
    } & import("@ngrx/store/src/models").TypedAction<"[Navigation] Mobile Navigation">> & import("@ngrx/effects").CreateEffectMetadata;
    constructor(actions$: Actions, breakpointObserver: BreakpointObserver);
}
