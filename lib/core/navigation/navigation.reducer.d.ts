import { Action } from '@ngrx/store';
export interface NavigationItem {
    badge: any;
    item: any;
    name: string;
    title: string;
    icon?: string;
    tags?: string;
    routerLink?: string;
    url?: string;
    active?: boolean;
    open?: boolean;
    items?: NavigationItem[];
    matched?: boolean;
    navTitle?: boolean;
}
export interface NavigationState {
    items: NavigationItem[];
    total: number;
    filterActive: boolean;
    filterText: string;
    matched: number;
}
export declare const initialState: NavigationState;
export declare function reducer(state: NavigationState, action: Action): NavigationState;
