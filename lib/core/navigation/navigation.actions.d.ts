import { NavigationItem } from './navigation.reducer';
export declare const toggleNavSection: import("@ngrx/store").ActionCreator<"[Navigation] Toggle Nav Section", (props: {
    item: NavigationItem;
}) => {
    item: NavigationItem;
} & import("@ngrx/store/src/models").TypedAction<"[Navigation] Toggle Nav Section">>;
export declare const activeUrl: import("@ngrx/store").ActionCreator<"[Navigation] Active Url", (props: {
    url: string;
}) => {
    url: string;
} & import("@ngrx/store/src/models").TypedAction<"[Navigation] Active Url">>;
export declare const toggleNavigationFilter: import("@ngrx/store").ActionCreator<"[Navigation] Toggle Filter", () => import("@ngrx/store/src/models").TypedAction<"[Navigation] Toggle Filter">>;
export declare const navigationFilter: import("@ngrx/store").ActionCreator<"[Navigation] Navigation Filter", (props: {
    text: string;
}) => {
    text: string;
} & import("@ngrx/store/src/models").TypedAction<"[Navigation] Navigation Filter">>;
export declare const mobileNavigation: import("@ngrx/store").ActionCreator<"[Navigation] Mobile Navigation", (props: {
    open: boolean;
}) => {
    open: boolean;
} & import("@ngrx/store/src/models").TypedAction<"[Navigation] Mobile Navigation">>;
