import { HttpClient } from '@angular/common/http';
export declare class CoreModule {
    private http;
    constructor(parentModule: CoreModule, http: HttpClient);
}
export declare function throwIfAlreadyLoaded(parentModule: any, moduleName: string): void;
