import { ComponentFactoryResolver, OnInit, ViewContainerRef } from '@angular/core';
import { PageService } from '../pages/page.service';
import { Block } from './block-block';
import { BlockService } from '../pages/block.service';
export declare class BlockComponent implements OnInit {
    protected pages: PageService;
    protected blocks: BlockService;
    protected resolver: ComponentFactoryResolver;
    protected viewContainerRef: ViewContainerRef;
    block: Block;
    blockWrapper: Block;
    subscription: any;
    constructor(pages: PageService, blocks: BlockService, resolver: ComponentFactoryResolver, viewContainerRef: ViewContainerRef);
    loadComponent(): void;
    ngOnInit(): void;
}
