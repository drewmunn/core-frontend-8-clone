import { Type } from '@angular/core';
export declare class Block {
    component: Type<any>;
    id: number;
    identifier: number;
    widgetId: number;
    content: any[];
    uid: any;
    data: any;
    constructor(component: Type<any>);
}
