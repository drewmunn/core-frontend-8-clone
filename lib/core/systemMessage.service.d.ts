export declare class SystemMessageService {
    private systemMessageService;
    interceptMessage: string;
    constructor(systemMessageService: SystemMessageService);
    clearInterceptMessage(): void;
    setInterceptMessage(message: string): void;
    getInterceptMessage(): string;
}
