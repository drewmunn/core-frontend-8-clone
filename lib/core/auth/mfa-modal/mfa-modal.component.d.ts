import { BsModalRef } from 'ngx-bootstrap/modal';
import { SystemMessageService } from '../../systemMessage.service';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
export declare class MfaModalComponent {
    bsModalRef: BsModalRef;
    private messages;
    private api;
    private router;
    mfaToken: FormControl;
    mfaToken2: FormControl;
    googleSecret: FormControl;
    secondaryUsername: FormControl;
    secondaryPassword: FormControl;
    mobile: FormControl;
    interceptedResponse: {
        subject: any;
        response: {
            request: {
                body: any;
                headers: any;
                clone(p: {
                    headers: any[];
                    body: any[];
                    observe: string;
                }): any;
            };
            repeatRequest(newRequest: any): any;
        };
    };
    mfa: {
        type: string;
        state: number;
        error?: boolean;
        mfaToken?: string;
        loading?: boolean;
        mfaResponse?: any;
    };
    constructor(bsModalRef: BsModalRef, messages: SystemMessageService, api: HttpClient, router: Router);
    submit(): void;
    getInterceptMessage(): string;
    closeModal($event: MouseEvent): void;
    setType(type: string): void;
    getType(): string;
    setState(state: number): void;
    getState(): number;
    isMFAEnabled(google: string): boolean;
}
