import { OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { SpinnerService } from '../../utils/spinner/spinner.service';
export declare class LoginComponent implements OnInit {
    protected authService: AuthService;
    protected spinner: SpinnerService;
    loading: boolean;
    errorMessage: string | boolean;
    loginForm: FormGroup;
    appName: string;
    copyright: string;
    constructor(authService: AuthService, spinner: SpinnerService);
    login(): import("rxjs").Subscription;
    ngOnInit(): void;
}
