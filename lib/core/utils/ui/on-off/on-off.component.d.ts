import { EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
export declare class OnOffComponent implements OnChanges {
    checked: boolean;
    class: string;
    checkedChange: EventEmitter<{}>;
    ngOnChanges(changes: SimpleChanges): void;
    onCheck($event: MouseEvent): void;
}
