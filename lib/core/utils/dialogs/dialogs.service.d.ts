import { Renderer2, RendererFactory2 } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
export declare class DialogsService {
    private modalService;
    bsModalRef: BsModalRef;
    renderer: Renderer2;
    constructor(rendererFactory: RendererFactory2, modalService: BsModalService);
    confirm(initialState: DialogOptions): Observable<boolean>;
    playSound(sound: string, path?: string): void;
}
export interface DialogOptions {
    title: string;
    message: string;
    buttons: DialogButtons;
}
export interface DialogButton {
    label: string;
    className: string;
}
export interface DialogButtons {
    confirm?: DialogButton;
    cancel?: DialogButton;
}
