import { BsModalRef } from 'ngx-bootstrap';
import { DialogButtons } from '../dialogs.service';
import { Subject } from 'rxjs';
export declare class ConfirmDialogComponent {
    bsModalRef: BsModalRef;
    title: string;
    message: string;
    buttons: DialogButtons;
    onClose: Subject<{}>;
    constructor(bsModalRef: BsModalRef);
    confirm($event: MouseEvent): void;
    cancel($event: MouseEvent): void;
}
