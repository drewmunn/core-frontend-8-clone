import { ElementRef, OnChanges, OnInit, Renderer2, SimpleChanges } from '@angular/core';
import { DialogsService } from '../../dialogs/dialogs.service';
export declare class PanelComponent implements OnInit, OnChanges {
    private dialogs;
    private el;
    private renderer;
    hasPannel: boolean;
    collapsible: boolean;
    collapsed: boolean;
    fullscreenable: boolean;
    fullscreenIn: boolean;
    clossable: boolean;
    closed: boolean;
    headerClass: string;
    panelTitle: any;
    constructor(headerClass: string, dialogs: DialogsService, el: ElementRef, renderer: Renderer2);
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    readonly pannelClasses: string[];
    readonly pannelContainerClasses: string[];
    readonly pannelContentClasses: string[];
    toggleCollapse($event: MouseEvent): void;
    toggleFullscreen($event: MouseEvent): void;
    closePanel($event: MouseEvent): Promise<void>;
    setHeaderClass(headerClass: string): void;
}
