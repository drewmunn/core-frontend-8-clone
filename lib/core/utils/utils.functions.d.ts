export declare function scrollToTop(scrollDuration: number): void;
export declare function toggleFullscreen(): void;
export declare function handleClassCondition(condition: boolean, className: string, el: HTMLElement): void;
