import { Angulartics2 } from 'angulartics2';
export declare class AngularticsGCLOG {
    protected angulartics2: Angulartics2;
    constructor(angulartics2: Angulartics2);
    startTracking(): void;
    pageTrack(path: string): void;
    eventTrack(action: string, properties: any): void;
    exceptionTrack(properties: any): void;
    setUsername(userId: string): void;
}
