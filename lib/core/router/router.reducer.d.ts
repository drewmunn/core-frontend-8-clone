import { Params, RouterStateSnapshot } from '@angular/router';
import * as fromRouter from '@ngrx/router-store';
export interface RouterStateUrl {
    url: string;
    queryParams: Params;
    params: Params;
    data: any;
}
export declare const reducer: typeof fromRouter.routerReducer;
export declare class CustomSerializer implements fromRouter.RouterStateSerializer<RouterStateUrl> {
    serialize(routerState: RouterStateSnapshot): RouterStateUrl;
}
