export declare class ParserService {
    replaceAll(text: any, find: any, replace: any): any;
    serializeRgQuery(obj: any, prefix?: any): string;
    getCookie(cname: any): string | false;
    deleteCookie(name: any, path: any, domain?: any): void;
}
