import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
export declare class CacheService {
    protected router?: Router;
    protected api?: HttpClient;
    cacheProbeTimestamp: any;
    cacheProbePromises$: any;
    objectLoadObservables$: any;
    constructor(router?: Router, api?: HttpClient);
    refresh(): void;
    resetObjectLoad(label: any): void;
    createCacheKey(inputs: any): string;
    getFromStorage(item: any): string | false;
    removeFromStorage(item: any): void;
    getOneThroughCache(route: any, identifier: any, headers?: any, cacheKey?: any, query?: any): any;
    getListThroughCache(route: any, query?: any, headers?: any, cacheKey?: any): any;
}
