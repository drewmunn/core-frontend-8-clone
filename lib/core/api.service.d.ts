import { ParserService } from './parser.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
export declare class APIInterceptor implements HttpInterceptor {
    protected parser: ParserService;
    constructor(parser: ParserService);
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
export declare class ApiService {
    interceptedResponse: {};
    setInterceptedResponse(response: {}): void;
    getInterceptedResponse(): {};
}
