import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { RouterReducerState } from '@ngrx/router-store';
import { SettingsState } from './settings/settings.reducer';
import { NavigationState } from './navigation/navigation.reducer';
import { RouterStateUrl } from './router/router.reducer';
import { NavigationEffects } from './navigation/navigation.effects';
export interface AppState {
    settings: SettingsState;
    navigation: NavigationState;
    router: RouterReducerState<RouterStateUrl>;
}
export declare const reducers: ActionReducerMap<AppState>;
export declare function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any>;
export declare const metaReducers: MetaReducer<AppState>[];
export declare const effects: (typeof NavigationEffects)[];
