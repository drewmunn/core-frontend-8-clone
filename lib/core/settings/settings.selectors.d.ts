import { SettingsState } from './settings.reducer';
export declare const selectSettingsState: import("@ngrx/store").MemoizedSelector<object, SettingsState, import("@ngrx/store").DefaultProjectorFn<SettingsState>>;
