export declare const toggleFixedHeader: import("@ngrx/store").ActionCreator<"[Settings] Toggle Fixed Header", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Fixed Header">>;
export declare const toggleFixedNavigation: import("@ngrx/store").ActionCreator<"[Settings] Toggle Fixed Navigation", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Fixed Navigation">>;
export declare const toggleMinifyNavigation: import("@ngrx/store").ActionCreator<"[Settings] Toggle Minify Navigation", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Minify Navigation">>;
export declare const toggleHideNavigation: import("@ngrx/store").ActionCreator<"[Settings] Toggle Hide Navigation", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Hide Navigation">>;
export declare const toggleTopNavigation: import("@ngrx/store").ActionCreator<"[Settings] Toggle Top Navigation", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Top Navigation">>;
export declare const toggleBoxedLayout: import("@ngrx/store").ActionCreator<"[Settings] Toggle Boxed Layout", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Boxed Layout">>;
export declare const togglePushContent: import("@ngrx/store").ActionCreator<"[Settings] Toggle Push Content", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Push Content">>;
export declare const toggleNoOverlay: import("@ngrx/store").ActionCreator<"[Settings] Toggle No Overlay", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle No Overlay">>;
export declare const toggleOffCanvas: import("@ngrx/store").ActionCreator<"[Settings] Toggle Off Canvas", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Off Canvas">>;
export declare const toggleBiggerContentFont: import("@ngrx/store").ActionCreator<"[Settings] Toggle Bigger Content Font", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Bigger Content Font">>;
export declare const toggleHighContrastText: import("@ngrx/store").ActionCreator<"[Settings] Toggle High Contrast Text", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle High Contrast Text">>;
export declare const toggleDaltonism: import("@ngrx/store").ActionCreator<"[Settings] Toggle Daltonism", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Daltonism">>;
export declare const toggleRtl: import("@ngrx/store").ActionCreator<"[Settings] Toggle RTL", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle RTL">>;
export declare const togglePreloaderInsise: import("@ngrx/store").ActionCreator<"[Settings] Toggle Preloader Insise", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Preloader Insise">>;
export declare const toggleCleanPageBackground: import("@ngrx/store").ActionCreator<"[Settings] Toggle Clean Page Background", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Clean Page Background">>;
export declare const toggleHideNavigationIcons: import("@ngrx/store").ActionCreator<"[Settings] Toggle Hide Navigation Icons", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Hide Navigation Icons">>;
export declare const toggleDisableCSSAnimation: import("@ngrx/store").ActionCreator<"[Settings] Toggle Disable CSS Animation", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Disable CSS Animation">>;
export declare const toggleHideInfoCard: import("@ngrx/store").ActionCreator<"[Settings] Toggle Hide Info Card", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Hide Info Card">>;
export declare const toggleLeanSubheader: import("@ngrx/store").ActionCreator<"[Settings] Toggle Lean Subheader", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Lean Subheader">>;
export declare const toggleHierarchicalNavigation: import("@ngrx/store").ActionCreator<"[Settings] Toggle Hierarchical Navigation", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Toggle Hierarchical Navigation">>;
export declare const setGlobalFontSize: import("@ngrx/store").ActionCreator<"[Settings] Set Global Font Size", (props: {
    size: string;
}) => {
    size: string;
} & import("@ngrx/store/src/models").TypedAction<"[Settings] Set Global Font Size">>;
export declare const appReset: import("@ngrx/store").ActionCreator<"[Settings] App Reset", () => import("@ngrx/store/src/models").TypedAction<"[Settings] App Reset">>;
export declare const factoryReset: import("@ngrx/store").ActionCreator<"[Settings] Factory Reset", () => import("@ngrx/store/src/models").TypedAction<"[Settings] Factory Reset">>;
export declare const SettingsActionTypes: string[];
