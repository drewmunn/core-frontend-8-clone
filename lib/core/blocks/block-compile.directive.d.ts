import { Compiler, ComponentRef, OnChanges, ViewContainerRef } from '@angular/core';
export declare class BlockCompileDirective implements OnChanges {
    private vcRef;
    private compiler;
    coreBlockCompile: string;
    coreBlockCompileContext: any[];
    compRef: ComponentRef<any>;
    constructor(vcRef: ViewContainerRef, compiler: Compiler);
    ngOnChanges(): void;
    updateProperties(): void;
    private createDynamicComponent;
    private createDynamicModule;
}
