import { OnInit } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
export declare class BlockDownloadComponent extends BlockAbstractComponent implements OnInit {
    protected authService: AuthService;
    constructor(authService: AuthService);
    ngOnInit(): void;
}
