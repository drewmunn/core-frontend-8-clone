import { OnInit } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
export declare class BlockImageComponent extends BlockAbstractComponent implements OnInit {
    url: string;
    alt: string;
    constructor();
    ngOnInit(): void;
}
