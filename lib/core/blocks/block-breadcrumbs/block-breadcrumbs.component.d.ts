import { OnInit } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { PageService } from '../../pages/page.service';
import { AuthService } from '../../auth/auth.service';
export declare class BlockBreadcrumbsComponent extends BlockAbstractComponent implements OnInit {
    protected authService: AuthService;
    protected pages: PageService;
    breadcrumbs: any;
    date: number;
    constructor(authService: AuthService, pages: PageService);
    ngOnInit(): void;
}
