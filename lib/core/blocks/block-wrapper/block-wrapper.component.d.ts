import { OnInit } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
export declare class BlockWrapperComponent extends BlockAbstractComponent implements OnInit {
    constructor();
    ngOnInit(): void;
    trackByFn(idx: number, item?: any): number;
}
