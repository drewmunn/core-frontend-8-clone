import { OnInit } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { TranslateService } from '../../pages/translate.service';
import { TranslateService as TranslateProvider } from '@ngx-translate/core';
export declare class BlockLanguageComponent extends BlockAbstractComponent implements OnInit {
    protected translate: TranslateService;
    protected translateProvider: TranslateProvider;
    languages$: any;
    language: any;
    constructor(translate: TranslateService, translateProvider: TranslateProvider);
    ngOnInit(): void;
    setLanguage(languageCode: string): void;
}
