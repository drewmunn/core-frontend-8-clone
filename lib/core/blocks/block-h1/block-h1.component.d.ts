import { OnInit } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
export declare class BlockH1Component extends BlockAbstractComponent implements OnInit {
    protected authService: AuthService;
    text: string;
    constructor(authService: AuthService);
    ngOnInit(): void;
}
