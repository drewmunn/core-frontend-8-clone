import { OnInit } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
export declare class BlockNumberedComponent extends BlockAbstractComponent implements OnInit {
    constructor();
    ngOnInit(): void;
    trackByFn(idx: number): number;
}
