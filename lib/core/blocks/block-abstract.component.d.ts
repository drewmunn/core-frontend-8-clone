import { Compiler, ComponentFactoryResolver, NgModuleFactory, OnInit, ViewContainerRef } from '@angular/core';
import { PageService } from '../pages/page.service';
import { TranslateService } from '../pages/translate.service';
import { Block } from '../block/block-block';
import { AuthService } from '../auth/auth.service';
export declare class BlockAbstractComponent implements OnInit {
    protected authService?: AuthService;
    protected pages?: PageService;
    protected resolver?: ComponentFactoryResolver;
    protected viewContainerRef?: ViewContainerRef;
    protected translate?: TranslateService;
    protected compiler?: Compiler;
    block: Block;
    user: {
        data: any;
    };
    dynamicComponent: any;
    dynamicModule: NgModuleFactory<any>;
    constructor(authService?: AuthService, pages?: PageService, resolver?: ComponentFactoryResolver, viewContainerRef?: ViewContainerRef, translate?: TranslateService, compiler?: Compiler);
    ngOnInit(): void;
    protected createDynamicBlock(contents: any): void;
    protected createComponentModule(componentType: any): any;
    protected createNewComponent(contents: string): any;
    trackByFn(idx: number, item?: any): number;
}
