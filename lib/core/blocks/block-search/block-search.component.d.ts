import { OnInit } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { FormControl } from '@angular/forms';
import { SearchService } from '../../pages/search.service';
export declare class BlockSearchComponent extends BlockAbstractComponent implements OnInit {
    protected search: SearchService;
    query: FormControl;
    constructor(search: SearchService);
    ngOnInit(): void;
    performSearch(): void;
}
