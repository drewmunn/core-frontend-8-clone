import { ModuleWithProviders } from '@angular/core';
export declare class CoreFrontendModule {
    static forRoot(): ModuleWithProviders;
}
