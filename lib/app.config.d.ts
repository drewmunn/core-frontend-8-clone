export declare const APP_CONFIG: {
    appName: string;
    api: string;
    authHeaders: {
        xUserName: any;
        xUserToken: any;
        xServiceProvider: any;
    };
    provider: any;
    user: string;
    email: string;
    twitter: string;
    avatar: string;
    version: string;
    bs4v: string;
    logo: string;
    logoM: string;
    copyright: string;
};
