import { Angulartics2GoogleTagManager } from 'angulartics2/gtm';
import { Router } from '@angular/router';
import { TranslateService as TranslateProvider } from '@ngx-translate/core';
import { TranslateService } from './core/pages/translate.service';
import { AngularticsGCLOG } from './core/gclog/gclogAngulartics';
export declare class CoreFrontendComponent {
    private translate;
    private translateProvider;
    router: Router;
    gtm: Angulartics2GoogleTagManager;
    gclog: AngularticsGCLOG;
    title: string;
    constructor(translate: TranslateService, translateProvider: TranslateProvider, router: Router, gtm: Angulartics2GoogleTagManager, gclog: AngularticsGCLOG);
}
