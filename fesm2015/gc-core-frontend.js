import { Component, Injectable, RendererFactory2, NgModule, ɵɵdefineInjectable, ɵɵinject, Optional, SkipSelf, EventEmitter, Input, Output, Directive, ViewContainerRef, Compiler, ChangeDetectionStrategy, ComponentFactoryResolver, Pipe, HostListener, Attribute, ElementRef, Renderer2, ContentChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { createAction, props, createReducer, on, INIT, StoreModule, createFeatureSelector, createSelector } from '@ngrx/store';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { routerReducer, ROUTER_NAVIGATED, StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule as ModalModule$1, BsModalRef as BsModalRef$1 } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsModalRef, BsModalService, ModalModule, PopoverModule, TabsModule as TabsModule$1, AlertModule as AlertModule$1, TooltipModule as TooltipModule$1 } from 'ngx-bootstrap';
import { Subject, ReplaySubject, forkJoin, BehaviorSubject, throwError, of } from 'rxjs';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TabsModule } from 'ngx-bootstrap/tabs';
import sha1 from 'js-sha1';
import { Angulartics2Module, Angulartics2 } from 'angulartics2';
import { TranslateModule, TranslateLoader, TranslateService as TranslateService$1 } from '@ngx-translate/core';
import { Router, RouterModule, ActivatedRoute, NavigationEnd } from '@angular/router';
import { map, tap, catchError } from 'rxjs/operators';
import { localStorageSync } from 'ngrx-store-localstorage';
import { BreakpointObserver } from '@angular/cdk/layout';
import { FormGroup, FormControl, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { isEmpty, omit } from 'lodash';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { trigger, state, style, transition, group, animate } from '@angular/animations';
import { __awaiter } from 'tslib';
import { Angulartics2GoogleTagManager } from 'angulartics2/gtm';
import { BrowserModule } from '@angular/platform-browser';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/environments/environment.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const environment = {
    production: false,
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/dialogs/confirm-dialog/confirm-dialog.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ConfirmDialogComponent {
    /**
     * @param {?} bsModalRef
     */
    constructor(bsModalRef) {
        this.bsModalRef = bsModalRef;
        this.onClose = new Subject();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    confirm($event) {
        this.onClose.next(true);
        this.onClose.complete();
        this.bsModalRef.hide();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    cancel($event) {
        this.onClose.next(false);
        this.onClose.complete();
        this.bsModalRef.hide();
    }
}
ConfirmDialogComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-confirm-dialog',
                template: `
        <div class="modal-header">
            <div class="modal-title" [innerHTML]="title">
            </div>
        </div>
        <div class="modal-body" [innerHTML]="message"></div>
        <div class="modal-footer" *ngIf="buttons">
            <button (click)="confirm($event)" type="button" class="btn bootbox-accept {{buttons.confirm.className}}">
                {{buttons.confirm.label}}</button>
            <button (click)="cancel($event)" type="button" class="btn bootbox-cancel {{buttons.cancel.className}}">
                {{buttons.cancel.label}}</button>
        </div>
    `
            }] }
];
/** @nocollapse */
ConfirmDialogComponent.ctorParameters = () => [
    { type: BsModalRef }
];
if (false) {
    /** @type {?} */
    ConfirmDialogComponent.prototype.title;
    /** @type {?} */
    ConfirmDialogComponent.prototype.message;
    /** @type {?} */
    ConfirmDialogComponent.prototype.buttons;
    /** @type {?} */
    ConfirmDialogComponent.prototype.onClose;
    /** @type {?} */
    ConfirmDialogComponent.prototype.bsModalRef;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/dialogs/dialogs.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DialogsService {
    /**
     * @param {?} rendererFactory
     * @param {?} modalService
     */
    constructor(rendererFactory, modalService) {
        this.modalService = modalService;
        this.renderer = rendererFactory.createRenderer(null, null);
    }
    /**
     * @param {?} initialState
     * @return {?}
     */
    confirm(initialState) {
        this.playSound('messagebox');
        this.bsModalRef = this.modalService.show(ConfirmDialogComponent, {
            initialState,
            backdrop: 'static',
            keyboard: false,
            class: 'modal-dialog-centered'
        });
        this.renderer.addClass(document.querySelector('.modal'), 'modal-alert');
        return (/** @type {?} */ (this.bsModalRef.content.onClose));
    }
    /**
     * @param {?} sound
     * @param {?=} path
     * @return {?}
     */
    playSound(sound, path = 'assets/media/sound') {
        /** @type {?} */
        const audioElement = document.createElement('audio');
        if (navigator.userAgent.match('Firefox/')) {
            audioElement.setAttribute('src', path + '/' + sound + '.ogg');
        }
        else {
            audioElement.setAttribute('src', path + '/' + sound + '.mp3');
        }
        audioElement.addEventListener('load', (/**
         * @return {?}
         */
        () => {
            audioElement.play();
        }), true);
        audioElement.pause();
        audioElement.play();
    }
}
DialogsService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
DialogsService.ctorParameters = () => [
    { type: RendererFactory2 },
    { type: BsModalService }
];
if (false) {
    /** @type {?} */
    DialogsService.prototype.bsModalRef;
    /** @type {?} */
    DialogsService.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    DialogsService.prototype.modalService;
}
/**
 * @record
 */
function DialogOptions() { }
if (false) {
    /** @type {?} */
    DialogOptions.prototype.title;
    /** @type {?} */
    DialogOptions.prototype.message;
    /** @type {?} */
    DialogOptions.prototype.buttons;
}
/**
 * @record
 */
function DialogButton() { }
if (false) {
    /** @type {?} */
    DialogButton.prototype.label;
    /** @type {?} */
    DialogButton.prototype.className;
}
/**
 * @record
 */
function DialogButtons() { }
if (false) {
    /** @type {?|undefined} */
    DialogButtons.prototype.confirm;
    /** @type {?|undefined} */
    DialogButtons.prototype.cancel;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/dialogs/dialogs.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DialogsModule {
}
DialogsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ConfirmDialogComponent],
                entryComponents: [ConfirmDialogComponent],
                imports: [
                    CommonModule,
                    ModalModule
                ],
                providers: [DialogsService]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/app.config.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const APP_CONFIG = {
    appName: 'CoreFrontend',
    api: '',
    authHeaders: {
        xUserName: null,
        xUserToken: null,
        xServiceProvider: null,
    },
    provider: null,
    user: 'Matt Dixon',
    email: 'core@builtoncore.com',
    twitter: 'builtoncore',
    avatar: 'avatar-admin.png',
    version: '1.0.0',
    bs4v: '4.3',
    logo: 'logo.svg',
    logoM: 'logo.svg',
    copyright: new Date().getFullYear() + ' © <a href="https://builtoncore.com" class="text-primary fw-500" ' +
        'title="Builtoncore" target="_blank">Gallagher Communication</a>',
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/parser.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ParserService {
    /**
     * @param {?} text
     * @param {?} find
     * @param {?} replace
     * @return {?}
     */
    replaceAll(text, find, replace) {
        /**
         * @param {?} textString
         * @return {?}
         */
        function escape(textString) {
            return textString.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
        }
        return text.replace(new RegExp(escape(find), 'g'), replace);
    }
    /**
     * @param {?} obj
     * @param {?=} prefix
     * @return {?}
     */
    serializeRgQuery(obj, prefix) {
        /** @type {?} */
        const str = [];
        /** @type {?} */
        const keys = [];
        for (const k in obj) {
            if (obj.hasOwnProperty(k)) {
                keys.push(k);
            }
        }
        keys.sort();
        for (let i = 0; i < keys.length; i++) {
            /** @type {?} */
            let k = prefix ? prefix + '[' + keys[i] + ']' : keys[i];
            /** @type {?} */
            let v = obj[keys[i]];
            str.push(typeof v == 'object' ?
                encodeURIComponent(k) + '=' + encodeURIComponent(JSON.stringify(v)).replace(/%3A/g, ':') :
                encodeURIComponent(k) + '=' + encodeURIComponent(v));
        }
        /** @type {?} */
        let finalStr = str.join('&');
        finalStr = this.replaceAll(finalStr, '%40', '@');
        finalStr = this.replaceAll(finalStr, '%3A', ':');
        finalStr = this.replaceAll(finalStr, '%2C', ',');
        finalStr = this.replaceAll(finalStr, '%20', '+');
        return finalStr;
    }
    /**
     * @param {?} cname
     * @return {?}
     */
    getCookie(cname) {
        /** @type {?} */
        const name = cname + '=';
        /** @type {?} */
        const decodedCookie = decodeURIComponent(document.cookie);
        /** @type {?} */
        const ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            /** @type {?} */
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return false;
    }
    /**
     * @param {?} name
     * @param {?} path
     * @param {?=} domain
     * @return {?}
     */
    deleteCookie(name, path, domain) {
        if (this.getCookie(name)) {
            try {
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') +
                    ((domain) ? ';domain=' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') +
                    ((domain) ? ';domain=.' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') + ';domain=' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') + ';domain=.' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((domain) ? ';domain=' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((domain) ? ';domain=.' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ';domain=' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ';domain=.' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
            }
            catch (err) {
            }
        }
    }
}
ParserService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ ParserService.ngInjectableDef = ɵɵdefineInjectable({ factory: function ParserService_Factory() { return new ParserService(); }, token: ParserService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/api.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class APIInterceptor {
    /**
     * @param {?} parser
     */
    constructor(parser) {
        this.parser = parser;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        // const pURL = (req.url + (this.parser.serializeRgQuery(req.params) ? '?' + this.parser.serializeRgQuery(req.params) : ''));
        /** @type {?} */
        const pURL = APP_CONFIG.api + '/' + req.urlWithParams;
        /** @type {?} */
        let responseHash;
        if (req.method.substr(0, 3) === 'GET') {
            responseHash = (pURL + APP_CONFIG.authHeaders.xUserToken);
        }
        else {
            responseHash = (pURL + (req.body ? JSON.stringify(req.body) : '') + APP_CONFIG.authHeaders.xUserToken);
        }
        /** @type {?} */
        const apiReq = req.clone({
            url: `${APP_CONFIG.api}/${req.url}`,
            headers: req.headers
                .set('x-service-provider', APP_CONFIG.authHeaders.xServiceProvider || '')
                .set('x-service-user-name', APP_CONFIG.authHeaders.xUserName || '')
                .set('Cache-Control', 'no-cache')
                .set('Pragma', 'no-cache')
                .set('x-service-request-hash', sha1(responseHash))
        });
        return next.handle(apiReq);
        // .pipe(
        //     catchError((error: HttpErrorResponse) => {
        //         let errorMessage: string;
        //         if (error.error instanceof ErrorEvent) {
        //             errorMessage = `Error: ${error.error.message}`;
        //         } else {
        //             errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        //         }
        //         return throwError(errorMessage);
        //     })
        // );
    }
}
APIInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
APIInterceptor.ctorParameters = () => [
    { type: ParserService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    APIInterceptor.prototype.parser;
}
class ApiService {
    /**
     * @param {?} response
     * @return {?}
     */
    setInterceptedResponse(response) {
        this.interceptedResponse = response;
    }
    /**
     * @return {?}
     */
    getInterceptedResponse() {
        return this.interceptedResponse;
    }
}
ApiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
    { type: NgModule, args: [{
                imports: [],
            },] }
];
/** @nocollapse */ ApiService.ngInjectableDef = ɵɵdefineInjectable({ factory: function ApiService_Factory() { return new ApiService(); }, token: ApiService, providedIn: "root" });
if (false) {
    /** @type {?} */
    ApiService.prototype.interceptedResponse;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/cache.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CacheService {
    /**
     * @param {?=} router
     * @param {?=} api
     */
    constructor(router, api) {
        this.router = router;
        this.api = api;
        this.refresh();
    }
    /**
     * @return {?}
     */
    refresh() {
        this.cacheProbeTimestamp = {};
        this.objectLoadObservables$ = {};
        this.cacheProbePromises$ = {};
    }
    /**
     * @param {?} label
     * @return {?}
     */
    resetObjectLoad(label) {
        delete this.objectLoadObservables$[label];
    }
    /**
     * @param {?} inputs
     * @return {?}
     */
    createCacheKey(inputs) {
        return JSON.stringify(inputs);
    }
    /**
     * @param {?} item
     * @return {?}
     */
    getFromStorage(item) {
        try {
            if (localStorage.getItem(item)) {
                return localStorage.getItem(item);
            }
        }
        catch (e) {
        }
        try {
            if (sessionStorage.getItem(item)) {
                return sessionStorage.getItem(item);
            }
        }
        catch (e) {
        }
        return false;
    }
    /**
     * @param {?} item
     * @return {?}
     */
    removeFromStorage(item) {
        try {
            localStorage.removeItem(item);
        }
        catch (e) {
        }
        try {
            sessionStorage.removeItem(item);
        }
        catch (e) {
        }
    }
    /**
     * @param {?} route
     * @param {?} identifier
     * @param {?=} headers
     * @param {?=} cacheKey
     * @param {?=} query
     * @return {?}
     */
    getOneThroughCache(route, identifier, headers, cacheKey, query) {
        if (!cacheKey) {
            cacheKey = this.createCacheKey({
                route,
                identifier,
                headers,
                query,
            });
        }
        if (!this.objectLoadObservables$) {
            this.objectLoadObservables$ = {};
        }
        if (this.objectLoadObservables$[cacheKey]) {
            return this.objectLoadObservables$[cacheKey];
        }
        this.objectLoadObservables$[cacheKey] = new ReplaySubject();
        this.api.get(route + (identifier ? '/' + identifier : ''), {
            params: query || {},
            headers: headers || {}
        }).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            try {
                this.objectLoadObservables$[cacheKey].next(response);
            }
            catch (err) {
            }
        }));
        return this.objectLoadObservables$[cacheKey];
    }
    /**
     * @param {?} route
     * @param {?=} query
     * @param {?=} headers
     * @param {?=} cacheKey
     * @return {?}
     */
    getListThroughCache(route, query, headers, cacheKey) {
        if (!cacheKey) {
            cacheKey = this.createCacheKey({
                route,
                query,
                headers
            });
        }
        if (!this.objectLoadObservables$) {
            this.objectLoadObservables$ = {};
        }
        if (this.objectLoadObservables$[cacheKey]) {
            return this.objectLoadObservables$[cacheKey];
        }
        this.objectLoadObservables$[cacheKey] = new ReplaySubject();
        this.api.get(route, {
            params: query || {},
            headers: headers || {}
        })
            .subscribe((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            try {
                this.objectLoadObservables$[cacheKey].next(response);
            }
            catch (err) {
            }
        }));
        return this.objectLoadObservables$[cacheKey];
    }
}
CacheService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CacheService.ctorParameters = () => [
    { type: Router },
    { type: HttpClient }
];
/** @nocollapse */ CacheService.ngInjectableDef = ɵɵdefineInjectable({ factory: function CacheService_Factory() { return new CacheService(ɵɵinject(Router), ɵɵinject(HttpClient)); }, token: CacheService, providedIn: "root" });
if (false) {
    /** @type {?} */
    CacheService.prototype.cacheProbeTimestamp;
    /** @type {?} */
    CacheService.prototype.cacheProbePromises$;
    /** @type {?} */
    CacheService.prototype.objectLoadObservables$;
    /**
     * @type {?}
     * @protected
     */
    CacheService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    CacheService.prototype.api;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/translate.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TranslateService extends CacheService {
    /**
     * @param {?} router
     * @param {?} api
     */
    constructor(router, api) {
        super();
        this.router = router;
        this.api = api;
        this.defaultLanguage = 'en_GB';
        this.refresh();
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    getTranslation(lang) {
        this.translations = this.getTranslations(lang).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            return response.data;
        })));
        return (/** @type {?} */ (this.translations));
    }
    /**
     * @return {?}
     */
    getLanguages() {
        if (this.objectLoadObservables$.languages) {
            return this.objectLoadObservables$.languages;
        }
        this.objectLoadObservables$.languages = new ReplaySubject();
        this.api.get('languages').subscribe((/**
         * @param {?} languages
         * @return {?}
         */
        languages => {
            sessionStorage.setItem('languages', JSON.stringify(languages));
            this.objectLoadObservables$.languages.next(languages);
        }));
        return this.objectLoadObservables$.languages;
    }
    /**
     * @param {?} languageCode
     * @return {?}
     */
    getTranslations(languageCode) {
        // if (this.getFromStorage('translations:' + languageCode)) {
        //     const translations = JSON.parse(this.getFromStorage('translations:' + languageCode) as string);
        //     if (!translations.timestamp) {
        //         return this.loadTranslations(languageCode);
        //     }
        //     let observables = [];
        //     if (!this.cacheProbeTimestamp.translations) {
        //         if (!this.cacheProbePromises$.translations) {
        //             this.cacheProbePromises$.translations = this.restangular.one('cache-probe').get({q: 'translations'}).pipe(
        //                 map((response) => {
        //                     this.cacheProbeTimestamp.translations = response as number;
        //                 })
        //             );
        //         }
        //     }
        //     observables.push(this.cacheProbePromises$.translations);
        //     return forkJoin(observables).subscribe(response => {
        //         if (!this.cacheProbeTimestamp.translations || translation.timestamp < this.cacheProbeTimestamp.translations) {
        //             return this.loadTranslations(languageCode);
        //         }
        //
        //         return deferred.promise;
        //     });
        // }
        return this.loadTranslations(languageCode);
    }
    /**
     * @param {?} languageCode
     * @return {?}
     */
    loadTranslations(languageCode) {
        if (this.objectLoadObservables$['translations:' + languageCode]) {
            return this.objectLoadObservables$['translations:' + languageCode];
        }
        this.objectLoadObservables$['translations:' + languageCode] = this.api.get('translations', {
            params: { languageCode },
            observe: 'response'
        }).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            /** @type {?} */
            const newServiceInfo = JSON.parse(response.headers.get('x-service-info'));
            /** @type {?} */
            const translations = {
                timestamp: this.cacheProbeTimestamp.translations || newServiceInfo.timestamp,
                data: {}
            };
            for (const translation of (/** @type {?} */ (response.body))) {
                translations.data[translation.dataKey] = translation.dataValue;
            }
            try {
                sessionStorage.setItem('translations:' + languageCode, JSON.stringify(translations));
            }
            catch (e) {
            }
            try {
                localStorage.setItem('translations:' + languageCode, JSON.stringify(translations));
            }
            catch (e) {
            }
            return translations;
        })));
        return this.objectLoadObservables$['translations:' + languageCode];
    }
    /**
     * @return {?}
     */
    getDefaultLanguage() {
        if (sessionStorage.getItem('defaultLanguage')) {
            return sessionStorage.getItem('defaultLanguage');
        }
        if (localStorage.getItem('defaultLanguage')) {
            return localStorage.getItem('defaultLanguage');
        }
        return this.defaultLanguage;
    }
    /**
     * @param {?} languageCode
     * @return {?}
     */
    setDefaultLanguage(languageCode) {
        try {
            sessionStorage.setItem('defaultLanguage', languageCode);
            localStorage.setItem('defaultLanguage', languageCode);
        }
        catch (_a) {
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        console.log('Destroy translate');
    }
}
TranslateService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TranslateService.ctorParameters = () => [
    { type: Router },
    { type: HttpClient }
];
/** @nocollapse */ TranslateService.ngInjectableDef = ɵɵdefineInjectable({ factory: function TranslateService_Factory() { return new TranslateService(ɵɵinject(Router), ɵɵinject(HttpClient)); }, token: TranslateService, providedIn: "root" });
if (false) {
    /** @type {?} */
    TranslateService.prototype.translations;
    /** @type {?} */
    TranslateService.prototype.defaultLanguage;
    /**
     * @type {?}
     * @protected
     */
    TranslateService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    TranslateService.prototype.api;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/settings/settings.actions.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const toggleFixedHeader = createAction('[Settings] Toggle Fixed Header');
/** @type {?} */
const toggleFixedNavigation = createAction('[Settings] Toggle Fixed Navigation');
/** @type {?} */
const toggleMinifyNavigation = createAction('[Settings] Toggle Minify Navigation');
/** @type {?} */
const toggleHideNavigation = createAction('[Settings] Toggle Hide Navigation');
/** @type {?} */
const toggleTopNavigation = createAction('[Settings] Toggle Top Navigation');
/** @type {?} */
const toggleBoxedLayout = createAction('[Settings] Toggle Boxed Layout');
/** @type {?} */
const togglePushContent = createAction('[Settings] Toggle Push Content');
/** @type {?} */
const toggleNoOverlay = createAction('[Settings] Toggle No Overlay');
/** @type {?} */
const toggleOffCanvas = createAction('[Settings] Toggle Off Canvas');
/** @type {?} */
const toggleBiggerContentFont = createAction('[Settings] Toggle Bigger Content Font');
/** @type {?} */
const toggleHighContrastText = createAction('[Settings] Toggle High Contrast Text');
/** @type {?} */
const toggleDaltonism = createAction('[Settings] Toggle Daltonism');
/** @type {?} */
const toggleRtl = createAction('[Settings] Toggle RTL');
/** @type {?} */
const togglePreloaderInsise = createAction('[Settings] Toggle Preloader Insise');
/** @type {?} */
const toggleCleanPageBackground = createAction('[Settings] Toggle Clean Page Background');
/** @type {?} */
const toggleHideNavigationIcons = createAction('[Settings] Toggle Hide Navigation Icons');
/** @type {?} */
const toggleDisableCSSAnimation = createAction('[Settings] Toggle Disable CSS Animation');
/** @type {?} */
const toggleHideInfoCard = createAction('[Settings] Toggle Hide Info Card');
/** @type {?} */
const toggleLeanSubheader = createAction('[Settings] Toggle Lean Subheader');
/** @type {?} */
const toggleHierarchicalNavigation = createAction('[Settings] Toggle Hierarchical Navigation');
/** @type {?} */
const setGlobalFontSize = createAction('[Settings] Set Global Font Size', props());
/** @type {?} */
const appReset = createAction('[Settings] App Reset');
/** @type {?} */
const factoryReset = createAction('[Settings] Factory Reset');
/** @type {?} */
const SettingsActionTypes = [
    toggleFixedHeader.type,
    toggleFixedNavigation.type,
    toggleMinifyNavigation.type,
    toggleHideNavigation.type,
    toggleTopNavigation.type,
    toggleBoxedLayout.type,
    togglePushContent.type,
    toggleNoOverlay.type,
    toggleOffCanvas.type,
    toggleBiggerContentFont.type,
    toggleHighContrastText.type,
    toggleDaltonism.type,
    toggleRtl.type,
    togglePreloaderInsise.type,
    toggleCleanPageBackground.type,
    toggleHideNavigationIcons.type,
    toggleDisableCSSAnimation.type,
    toggleHideInfoCard.type,
    toggleLeanSubheader.type,
    toggleHierarchicalNavigation.type,
    setGlobalFontSize.type,
    appReset.type,
    factoryReset.type,
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/settings/settings.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function SettingsState() { }
if (false) {
    /** @type {?} */
    SettingsState.prototype.fixedHeader;
    /** @type {?} */
    SettingsState.prototype.fixedNavigation;
    /** @type {?} */
    SettingsState.prototype.minifyNavigation;
    /** @type {?} */
    SettingsState.prototype.hideNavigation;
    /** @type {?} */
    SettingsState.prototype.topNavigation;
    /** @type {?} */
    SettingsState.prototype.boxedLayout;
    /** @type {?} */
    SettingsState.prototype.pushContent;
    /** @type {?} */
    SettingsState.prototype.noOverlay;
    /** @type {?} */
    SettingsState.prototype.offCanvas;
    /** @type {?} */
    SettingsState.prototype.biggerContentFont;
    /** @type {?} */
    SettingsState.prototype.highContrastText;
    /** @type {?} */
    SettingsState.prototype.daltonism;
    /** @type {?} */
    SettingsState.prototype.preloaderInside;
    /** @type {?} */
    SettingsState.prototype.rtl;
    /** @type {?} */
    SettingsState.prototype.cleanPageBackground;
    /** @type {?} */
    SettingsState.prototype.hideNavigationIcons;
    /** @type {?} */
    SettingsState.prototype.disableCSSAnimation;
    /** @type {?} */
    SettingsState.prototype.hideInfoCard;
    /** @type {?} */
    SettingsState.prototype.leanSubheader;
    /** @type {?} */
    SettingsState.prototype.hierarchicalNavigation;
    /** @type {?} */
    SettingsState.prototype.globalFontSize;
}
// here you can configure initial state of your app
// for all your users
/** @type {?} */
const initialState = {
    // app layout
    fixedHeader: true,
    fixedNavigation: false,
    minifyNavigation: false,
    hideNavigation: false,
    topNavigation: false,
    boxedLayout: false,
    // mobile menu
    pushContent: false,
    noOverlay: false,
    offCanvas: false,
    // accessibility
    biggerContentFont: false,
    highContrastText: false,
    daltonism: false,
    preloaderInside: false,
    rtl: false,
    // global modifications
    cleanPageBackground: false,
    hideNavigationIcons: false,
    disableCSSAnimation: false,
    hideInfoCard: false,
    leanSubheader: false,
    hierarchicalNavigation: false,
    // global font size
    globalFontSize: 'md',
};
const ɵ0 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { fixedHeader: !state.fixedHeader })), ɵ1 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { fixedNavigation: !state.fixedNavigation })), ɵ2 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { minifyNavigation: !state.minifyNavigation })), ɵ3 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { hideNavigation: !state.hideNavigation })), ɵ4 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { topNavigation: !state.topNavigation })), ɵ5 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { boxedLayout: !state.boxedLayout })), ɵ6 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { pushContent: !state.pushContent })), ɵ7 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { noOverlay: !state.noOverlay })), ɵ8 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { offCanvas: !state.offCanvas })), ɵ9 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { biggerContentFont: !state.biggerContentFont })), ɵ10 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { highContrastText: !state.highContrastText })), ɵ11 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { daltonism: !state.daltonism })), ɵ12 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { rtl: !state.rtl })), ɵ13 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { preloaderInside: !state.preloaderInside })), ɵ14 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { cleanPageBackground: !state.cleanPageBackground })), ɵ15 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { hideNavigationIcons: !state.hideNavigationIcons })), ɵ16 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { disableCSSAnimation: !state.disableCSSAnimation })), ɵ17 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { hideInfoCard: !state.hideInfoCard })), ɵ18 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { leanSubheader: !state.leanSubheader })), ɵ19 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { hierarchicalNavigation: !state.hierarchicalNavigation })), ɵ20 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
(state, action) => (Object.assign({}, state, { globalFontSize: action.size })), ɵ21 = /**
 * @return {?}
 */
() => (Object.assign({}, initialState));
/** @type {?} */
const settingsReducer = createReducer(initialState, on(toggleFixedHeader, (ɵ0)), on(toggleFixedNavigation, (ɵ1)), on(toggleMinifyNavigation, (ɵ2)), on(toggleHideNavigation, (ɵ3)), on(toggleTopNavigation, (ɵ4)), on(toggleBoxedLayout, (ɵ5)), on(togglePushContent, (ɵ6)), on(toggleNoOverlay, (ɵ7)), on(toggleOffCanvas, (ɵ8)), on(toggleBiggerContentFont, (ɵ9)), on(toggleHighContrastText, (ɵ10)), on(toggleDaltonism, (ɵ11)), on(toggleRtl, (ɵ12)), on(togglePreloaderInsise, (ɵ13)), on(toggleCleanPageBackground, (ɵ14)), on(toggleHideNavigationIcons, (ɵ15)), on(toggleDisableCSSAnimation, (ɵ16)), on(toggleHideInfoCard, (ɵ17)), on(toggleLeanSubheader, (ɵ18)), on(toggleHierarchicalNavigation, (ɵ19)), on(setGlobalFontSize, (ɵ20)), on(appReset, (ɵ21)));
/**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
function reducer(state, action) {
    return settingsReducer(state, action);
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/navigation/navigation.actions.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const toggleNavSection = createAction('[Navigation] Toggle Nav Section', props());
/** @type {?} */
const activeUrl = createAction('[Navigation] Active Url', props());
/** @type {?} */
const toggleNavigationFilter = createAction('[Navigation] Toggle Filter');
/** @type {?} */
const navigationFilter = createAction('[Navigation] Navigation Filter', props());
/** @type {?} */
const mobileNavigation = createAction('[Navigation] Mobile Navigation', props());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core.navigation.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NavigationItems = [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/navigation/navigation.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function NavigationItem() { }
if (false) {
    /** @type {?} */
    NavigationItem.prototype.badge;
    /** @type {?} */
    NavigationItem.prototype.item;
    /** @type {?} */
    NavigationItem.prototype.name;
    /** @type {?} */
    NavigationItem.prototype.title;
    /** @type {?|undefined} */
    NavigationItem.prototype.icon;
    /** @type {?|undefined} */
    NavigationItem.prototype.tags;
    /** @type {?|undefined} */
    NavigationItem.prototype.routerLink;
    /** @type {?|undefined} */
    NavigationItem.prototype.url;
    /** @type {?|undefined} */
    NavigationItem.prototype.active;
    /** @type {?|undefined} */
    NavigationItem.prototype.open;
    /** @type {?|undefined} */
    NavigationItem.prototype.items;
    /** @type {?|undefined} */
    NavigationItem.prototype.matched;
    /** @type {?|undefined} */
    NavigationItem.prototype.navTitle;
}
/**
 * @record
 */
function NavigationState() { }
if (false) {
    /** @type {?} */
    NavigationState.prototype.items;
    /** @type {?} */
    NavigationState.prototype.total;
    /** @type {?} */
    NavigationState.prototype.filterActive;
    /** @type {?} */
    NavigationState.prototype.filterText;
    /** @type {?} */
    NavigationState.prototype.matched;
}
/** @type {?} */
const initialState$1 = {
    items: decorateItems(NavigationItems),
    total: countTotal(NavigationItems),
    filterActive: false,
    filterText: '',
    matched: 0
};
const ɵ0$1 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
(state, action) => (Object.assign({}, state, { items: detectActiveItems(state.items, action.url) })), ɵ1$1 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
(state, action) => (Object.assign({}, state, { items: toggleItems(state.items, action.item) })), ɵ2$1 = /**
 * @param {?} state
 * @return {?}
 */
state => {
    if (state.filterActive) {
        return Object.assign({}, state, { filterActive: false, matched: 0, items: state.items.map((/**
             * @param {?} _
             * @return {?}
             */
            _ => (Object.assign({}, _, { matched: null })))) });
    }
    else {
        /** @type {?} */
        const items = filterItems(state.items, state.filterText);
        return Object.assign({}, state, { filterActive: true, items, matched: countMatched(items) });
    }
}, ɵ3$1 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
(state, action) => {
    /** @type {?} */
    const items = filterItems(state.items, action.text);
    return Object.assign({}, state, { filterText: action.text, items, matched: countMatched(items) });
};
/** @type {?} */
const navigationReducer = createReducer(initialState$1, on(activeUrl, (ɵ0$1)), on(toggleNavSection, (ɵ1$1)), on(toggleNavigationFilter, (ɵ2$1)), on(navigationFilter, (ɵ3$1)));
/**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
function reducer$1(state, action) {
    return navigationReducer(state, action);
}
/**
 * @param {?} navItems
 * @return {?}
 */
function decorateItems(navItems) {
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    navItem => {
        /** @type {?} */
        const item = Object.assign({}, navItem, { active: false, matched: null });
        if (navItem.items) {
            item.open = false;
            item.items = decorateItems(navItem.items);
        }
        item.navTitle = !navItem.items && !navItem.routerLink && !!navItem.title;
        return item;
    }));
}
/**
 * @param {?} navItems
 * @return {?}
 */
function countTotal(navItems) {
    /** @type {?} */
    let total = navItems.length;
    navItems.filter((/**
     * @param {?} _
     * @return {?}
     */
    _ => !!_.items)).forEach((/**
     * @param {?} _
     * @return {?}
     */
    _ => {
        total += countTotal(_.items);
    }));
    return total;
}
/**
 * @param {?} navItems
 * @return {?}
 */
function countMatched(navItems) {
    /** @type {?} */
    let matched = navItems.filter((/**
     * @param {?} _
     * @return {?}
     */
    _ => !!_.matched)).length;
    navItems.filter((/**
     * @param {?} _
     * @return {?}
     */
    _ => !!_.items)).forEach((/**
     * @param {?} _
     * @return {?}
     */
    _ => {
        matched += countMatched(_.items);
    }));
    return matched;
}
/**
 * @param {?} navItems
 * @param {?} activeUrl
 * @return {?}
 */
function detectActiveItems(navItems, activeUrl) {
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    navItem => {
        /** @type {?} */
        const isActive = itemIsActive(navItem, activeUrl);
        /** @type {?} */
        const item = Object.assign({}, navItem, { active: isActive });
        if (navItem.items) {
            item.open = isActive;
            item.items = detectActiveItems(navItem.items, activeUrl);
        }
        return item;
    }));
}
/**
 * @param {?} item
 * @param {?} activeUrl
 * @return {?}
 */
function itemIsActive(item, activeUrl) {
    if (item.routerLink === activeUrl) {
        return true;
    }
    else if (item.items) {
        return item.items.some((/**
         * @param {?} _
         * @return {?}
         */
        _ => itemIsActive(_, activeUrl)));
    }
    else {
        return false;
    }
}
/**
 * @param {?} navItems
 * @param {?} toggledItem
 * @return {?}
 */
function toggleItems(navItems, toggledItem) {
    /** @type {?} */
    const isToggledItemLevel = navItems.some((/**
     * @param {?} _
     * @return {?}
     */
    _ => _ === toggledItem));
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    navItem => {
        /** @type {?} */
        const item = Object.assign({}, navItem);
        if (isToggledItemLevel && item.items && navItem !== toggledItem) {
            item.open = false;
        }
        if (navItem === toggledItem) {
            item.open = !navItem.open;
        }
        if (navItem.items) {
            item.items = toggleItems(navItem.items, toggledItem);
        }
        return item;
    }));
}
/**
 * @param {?} navItems
 * @param {?} text
 * @return {?}
 */
function filterItems(navItems, text) {
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    navItem => {
        /** @type {?} */
        const item = Object.assign({}, navItem);
        if (navItem.items) {
            item.matched = navItemMatch(navItem, text) || navItem.items.some((/**
             * @param {?} _
             * @return {?}
             */
            _ => navItemMatch(_, text)));
            item.items = filterItems(navItem.items, text);
        }
        else {
            item.matched = navItemMatch(navItem, text);
        }
        return item;
    }));
}
/**
 * @param {?} item
 * @param {?} text
 * @return {?}
 */
function navItemMatch(item, text) {
    return (!text.trim() || (item.tags && !!item.tags.match(new RegExp(`.*${text.trim()}.*`, 'gi'))));
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/router/router.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function RouterStateUrl() { }
if (false) {
    /** @type {?} */
    RouterStateUrl.prototype.url;
    /** @type {?} */
    RouterStateUrl.prototype.queryParams;
    /** @type {?} */
    RouterStateUrl.prototype.params;
    /** @type {?} */
    RouterStateUrl.prototype.data;
}
/** @type {?} */
const reducer$2 = routerReducer;
class CustomSerializer {
    /**
     * @param {?} routerState
     * @return {?}
     */
    serialize(routerState) {
        const { url } = routerState;
        const { queryParams } = routerState.root;
        /** @type {?} */
        let state = routerState.root;
        while (state.firstChild) {
            state = state.firstChild;
        }
        const { params, data } = state;
        return { url, queryParams, params, data };
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/utils.functions.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} scrollDuration
 * @return {?}
 */
function scrollToTop(scrollDuration) {
    /** @type {?} */
    const cosParameter = window.scrollY / 2;
    /** @type {?} */
    let scrollCount = 0;
    /** @type {?} */
    let oldTimestamp = performance.now();
    /**
     * @param {?} newTimestamp
     * @return {?}
     */
    function step(newTimestamp) {
        scrollCount += Math.PI / (scrollDuration / (newTimestamp - oldTimestamp));
        if (scrollCount >= Math.PI) {
            window.scrollTo(0, 0);
        }
        if (window.scrollY === 0) {
            return;
        }
        window.scrollTo(0, Math.round(cosParameter + cosParameter * Math.cos(scrollCount)));
        oldTimestamp = newTimestamp;
        window.requestAnimationFrame(step);
    }
    window.requestAnimationFrame(step);
}
/* tslint:disable */
/**
 * @return {?}
 */
function toggleFullscreen() {
    if (!document.fullscreenElement /* Standard browsers */
        && !document['msFullscreenElement'] /* Internet Explorer */
        && !document['mozFullScreenElement'] /* Firefox */
        && !document['webkitFullscreenElement'] /* Chrome */) {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        }
        else if (document.documentElement['msRequestFullscreen']) {
            document.documentElement['msRequestFullscreen']();
        }
        else if (document.documentElement['mozRequestFullScreen']) {
            document.documentElement['mozRequestFullScreen']();
        }
        else if (document.documentElement['webkitRequestFullscreen']) {
            document.documentElement['webkitRequestFullscreen'](Element['ALLOW_KEYBOARD_INPUT']);
        }
    }
    else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document['msExitFullscreen']) {
            document['msExitFullscreen']();
        }
        else if (document['mozCancelFullScreen']) {
            document['mozCancelFullScreen']();
        }
        else if (document['webkitExitFullscreen']) {
            document['webkitExitFullscreen']();
        }
    }
}
/* tslint:enable */
// conditionaly apply css class to target
/**
 * @param {?} condition
 * @param {?} className
 * @param {?} el
 * @return {?}
 */
function handleClassCondition(condition, className, el) {
    if (!condition && el.classList.contains(className)) {
        el.classList.remove(className);
    }
    if (condition && !el.classList.contains(className)) {
        el.classList.add(className);
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/settings/settings.meta.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const html = document.querySelector('html');
/** @type {?} */
const body = document.querySelector('body');
// meta reducer that applies layout classes based on settings reducer
/**
 * @param {?} reducer
 * @return {?}
 */
function settingsMetaReducer(reducer) {
    return (/**
     * @param {?} state
     * @param {?} action
     * @return {?}
     */
    (state, action) => {
        // build new state
        /** @type {?} */
        const result = reducer(state, action);
        // use our middleware only for INIT action and for Settings actions
        if (action.type === INIT || SettingsActionTypes.includes(action.type)) {
            handleCssClasses(result.settings, action);
        }
        // pass state into next chain
        return result;
    });
}
/**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
function handleCssClasses(state, action) {
    handleClassCondition(state.fixedHeader, 'header-function-fixed', body);
    handleClassCondition(state.fixedNavigation, 'nav-function-fixed', body);
    handleClassCondition(state.minifyNavigation, 'nav-function-minify', body);
    handleClassCondition(state.hideNavigation, 'nav-function-hidden', body);
    handleClassCondition(state.topNavigation, 'nav-function-top', body);
    handleClassCondition(state.boxedLayout, 'mod-main-boxed', body);
    handleClassCondition(state.pushContent, 'nav-mobile-push', body);
    handleClassCondition(state.noOverlay, 'nav-mobile-no-overlay', body);
    handleClassCondition(state.offCanvas, 'nav-mobile-slide-out', body);
    handleClassCondition(state.biggerContentFont, 'mod-bigger-font', body);
    handleClassCondition(state.highContrastText, 'mod-high-contrast', body);
    handleClassCondition(state.daltonism, 'mod-color-blind', body);
    handleClassCondition(state.cleanPageBackground, 'mod-clean-page-bg', body);
    handleClassCondition(state.hideNavigationIcons, 'mod-hide-nav-icons', body);
    handleClassCondition(state.disableCSSAnimation, 'mod-disable-animation', body);
    handleClassCondition(state.hideInfoCard, 'mod-hide-info-card', body);
    handleClassCondition(state.leanSubheader, 'mod-lean-subheader', body);
    handleClassCondition(state.hierarchicalNavigation, 'mod-nav-link', body);
    handleClassCondition(state.globalFontSize === 'sm', 'root-text-sm', html);
    handleClassCondition(state.globalFontSize === 'md', 'root-text', html);
    handleClassCondition(state.globalFontSize === 'lg', 'root-text-lg', html);
    handleClassCondition(state.globalFontSize === 'xl', 'root-text-xl', html);
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/navigation/navigation.effects.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NavigationEffects {
    /**
     * @param {?} actions$
     * @param {?} breakpointObserver
     */
    constructor(actions$, breakpointObserver) {
        this.actions$ = actions$;
        this.mapToActiveUrl$ = createEffect((/**
         * @return {?}
         */
        () => this.actions$.pipe(ofType(ROUTER_NAVIGATED), map((/**
         * @param {?} action
         * @return {?}
         */
        (action) => activeUrl({ url: action.payload.event.url }))), tap((/**
         * @param {?} action
         * @return {?}
         */
        action => handleClassCondition(false, 'mobile-nav-on', document.querySelector('body')))))));
        this.mobileNavigation$ = createEffect((/**
         * @return {?}
         */
        () => this.actions$.pipe(ofType(mobileNavigation), tap((/**
         * @param {?} action
         * @return {?}
         */
        action => handleClassCondition(action.open, 'mobile-nav-on', document.querySelector('body')))))), { dispatch: false });
        breakpointObserver.observe('(max-width: 600px)').subscribe((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            handleClassCondition(result.matches, 'mobile-view-activated', document.querySelector('body'));
        }));
    }
}
NavigationEffects.decorators = [
    { type: Injectable }
];
/** @nocollapse */
NavigationEffects.ctorParameters = () => [
    { type: Actions },
    { type: BreakpointObserver }
];
if (false) {
    /** @type {?} */
    NavigationEffects.prototype.mapToActiveUrl$;
    /** @type {?} */
    NavigationEffects.prototype.mobileNavigation$;
    /**
     * @type {?}
     * @private
     */
    NavigationEffects.prototype.actions$;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/index.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function AppState() { }
if (false) {
    /** @type {?} */
    AppState.prototype.settings;
    /** @type {?} */
    AppState.prototype.navigation;
    /** @type {?} */
    AppState.prototype.router;
}
/** @type {?} */
const reducers = {
    settings: reducer,
    navigation: reducer$1,
    router: reducer$2
};
/**
 * @param {?} reducer
 * @return {?}
 */
function localStorageSyncReducer(reducer) {
    return localStorageSync({
        keys: ['settings'],
        rehydrate: true,
    })(reducer);
}
/** @type {?} */
const metaReducers = [
    localStorageSyncReducer,
    settingsMetaReducer
].concat([]);
/** @type {?} */
const effects = [
    NavigationEffects
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/core.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoreModule {
    /**
     * @param {?} parentModule
     * @param {?} http
     */
    constructor(parentModule, http) {
        this.http = http;
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
}
CoreModule.decorators = [
    { type: NgModule, args: [{
                declarations: [],
                imports: [
                    CommonModule,
                    BrowserAnimationsModule,
                    HttpClientModule,
                    StoreModule.forRoot(reducers, {
                        metaReducers,
                        runtimeChecks: {
                            strictStateImmutability: false,
                            strictActionImmutability: false,
                            strictStateSerializability: false,
                            strictActionSerializability: false,
                        },
                    }),
                    EffectsModule.forRoot([...effects]),
                    StoreDevtoolsModule.instrument({
                        maxAge: 25, logOnly: environment.production,
                        actionsBlocklist: ['@ngrx/router*']
                    }),
                    StoreRouterConnectingModule.forRoot(),
                    AccordionModule.forRoot(),
                    AlertModule.forRoot(),
                    BsDropdownModule.forRoot(),
                    ButtonsModule.forRoot(),
                    CollapseModule.forRoot(),
                    ModalModule$1.forRoot(),
                    TooltipModule.forRoot(),
                    TabsModule.forRoot(),
                    PopoverModule.forRoot(),
                    DialogsModule,
                    ApiService,
                    Angulartics2Module.forRoot(),
                    TranslateModule.forRoot({
                        loader: {
                            provide: TranslateLoader,
                            useClass: TranslateService,
                        }
                    }),
                ],
                providers: [
                    {
                        provide: RouterStateSerializer,
                        useClass: CustomSerializer
                    },
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: APIInterceptor,
                        multi: true
                    },
                ]
            },] }
];
/** @nocollapse */
CoreModule.ctorParameters = () => [
    { type: CoreModule, decorators: [{ type: Optional }, { type: SkipSelf }] },
    { type: HttpClient }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoreModule.prototype.http;
}
/**
 * @param {?} parentModule
 * @param {?} moduleName
 * @return {?}
 */
function throwIfAlreadyLoaded(parentModule, moduleName) {
    if (parentModule) {
        throw new Error(`${moduleName} has already been loaded. Import ${moduleName} modules in the AppModule only.`);
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/page.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PageService extends CacheService {
    /**
     * @param {?} router
     * @param {?} api
     */
    constructor(router, api) {
        super(router, api);
        this.router = router;
        this.api = api;
    }
    /**
     * @return {?}
     */
    refresh() {
        super.refresh();
        this.page = false;
        this.breadcrumbs = {};
    }
    /**
     * @param {?} page
     * @return {?}
     */
    setCurrentPage(page) {
        this.page = page;
    }
    /**
     * @return {?}
     */
    getCurrentPage() {
        return this.page;
    }
    /**
     * @param {?=} headers
     * @return {?}
     */
    getAll(headers) {
        return this.getListThroughCache('pages', {}, headers || {});
    }
    /**
     * @param {?} identifier
     * @param {?=} blockInteraction
     * @param {?=} headers
     * @return {?}
     */
    getPage(identifier, blockInteraction, headers) {
        /** @type {?} */
        const cacheKey = this.createCacheKey(['page', identifier, headers]);
        if (!blockInteraction && this.objectLoadObservables$[cacheKey]) {
            this.api.get('page/' + identifier, { headers: headers || {} }).subscribe(); // Trigger interaction
            return this.objectLoadObservables$[cacheKey];
        }
        else if (blockInteraction) {
            headers = Object.assign({}, (headers || {}), { 'x-service-info': JSON.stringify({ blockInteraction: true }) });
        }
        this.objectLoadObservables$[cacheKey] = this.getOneThroughCache('page', identifier, headers, cacheKey);
        return this.objectLoadObservables$[cacheKey];
    }
    /**
     * @param {?=} headers
     * @return {?}
     */
    getPages(headers) {
        return this.getListThroughCache('pages', {}, headers || {});
    }
    /**
     * @param {?} blockId
     * @return {?}
     */
    getBlock(blockId) {
        try {
            /** @type {?} */
            const observables = [];
            if (this.getFromStorage('block:' + blockId) !== false) {
                /** @type {?} */
                const block = JSON.parse((/** @type {?} */ (this.getFromStorage('block:' + blockId))));
                if (!block.timestamp) {
                    return this.loadBlock(blockId);
                }
                if (!this.cacheProbeTimestamp.blocks) {
                    this.cacheProbePromises$.blocks = this.api.get('cache-probe', { params: { q: 'widget' } }).pipe(map((/**
                     * @param {?} response
                     * @return {?}
                     */
                    (response) => {
                        this.cacheProbeTimestamp.blocks = (/** @type {?} */ (response));
                    })));
                    observables.push(this.cacheProbePromises$.blocks);
                }
                return forkJoin(observables).toPromise().then((/**
                 * @param {?} response
                 * @return {?}
                 */
                response => {
                    if (!this.cacheProbeTimestamp.blocks || block.timestamp < this.cacheProbeTimestamp.blocks) {
                        return this.loadBlock(blockId);
                    }
                    return block;
                }));
            }
        }
        catch (err) {
        }
        return this.loadBlock(blockId);
    }
    /**
     * @param {?} blockId
     * @return {?}
     */
    loadBlock(blockId) {
        if (this.objectLoadObservables$['block:' + blockId]) {
            return this.objectLoadObservables$['block:' + blockId];
        }
        this.objectLoadObservables$['block:' + blockId] = this.api.get('widget/' + blockId)
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            /** @type {?} */
            const block = response;
            try {
                localStorage.setItem('block:' + blockId, JSON.stringify(block));
            }
            catch (e) {
            }
            try {
                sessionStorage.setItem('block:' + blockId, JSON.stringify(block));
            }
            catch (e) {
            }
            return block;
        }))).toPromise();
        return this.objectLoadObservables$['block:' + blockId];
    }
    /**
     * @param {?} blocks
     * @return {?}
     */
    sortBlocks(blocks) {
        blocks.content.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        (a, b) => (a.data.row < b.data.row) ? 1 : -1))
            .sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        (a, b) => (a.data.col < b.data.col) ? 1 : -1));
        for (const subBlocks of blocks.content) {
            this.sortBlocks(subBlocks);
        }
    }
    /**
     * @param {?} requestedPageId
     * @return {?}
     */
    getBreadcrumbs(requestedPageId) {
        return this.getPages().pipe(map((/**
         * @param {?} pages
         * @return {?}
         */
        (pages) => {
            if (isEmpty(this.breadcrumbs)) {
                /** @type {?} */
                const extractIds = (/**
                 * @param {?} page
                 * @param {?} parents
                 * @return {?}
                 */
                (page, parents) => {
                    /** @type {?} */
                    const pageMin = omit(page, ['children', 'createdBy', 'dateCreated', 'displayOrder', 'groups',
                        'hidden', 'lastUpdated', 'permissions', 'tags', 'widgets']);
                    pageMin.parents = parents;
                    this.breadcrumbs[page.id] = page;
                    for (const child of page.children) {
                        extractIds(child, parents.concat([pageMin]));
                    }
                });
                for (const page of pages) {
                    extractIds(page, []);
                }
            }
            return this.breadcrumbs[requestedPageId] ? this.breadcrumbs[requestedPageId] : [];
        })));
    }
    /**
     * @param {?} query
     * @return {?}
     */
    performSearch(query) {
        console.log(query);
    }
}
PageService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PageService.ctorParameters = () => [
    { type: Router },
    { type: HttpClient }
];
/** @nocollapse */ PageService.ngInjectableDef = ɵɵdefineInjectable({ factory: function PageService_Factory() { return new PageService(ɵɵinject(Router), ɵɵinject(HttpClient)); }, token: PageService, providedIn: "root" });
if (false) {
    /** @type {?} */
    PageService.prototype.page;
    /** @type {?} */
    PageService.prototype.breadcrumbs;
    /**
     * @type {?}
     * @protected
     */
    PageService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    PageService.prototype.api;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/auth/auth.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AuthService {
    /**
     * @param {?} router
     * @param {?} api
     * @param {?} angulartics
     * @param {?} parser
     * @param {?} cache
     * @param {?} pages
     */
    constructor(router, api, angulartics, parser, cache, pages) {
        this.router = router;
        this.api = api;
        this.angulartics = angulartics;
        this.parser = parser;
        this.cache = cache;
        this.pages = pages;
        this.authChangeEmit = new BehaviorSubject(true);
        this.failedLogins = 0;
        this.loginLimit = 4;
    }
    /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    canActivate(next, state) {
        if (!this.authResumed) {
            this.resume();
        }
        if (this.isAuthenticated()) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }
    /**
     * @return {?}
     */
    isAuthenticated() {
        return this.getUser() || this.cookiesExist();
    }
    /**
     * @return {?}
     */
    getUser() {
        return this.user;
    }
    /**
     * @param {?} username
     * @param {?} password
     * @param {?=} recaptchaResponse
     * @param {?=} authToken
     * @return {?}
     */
    login(username, password, recaptchaResponse, authToken) {
        this.reset();
        /** @type {?} */
        const user = {
            username,
            password,
            recaptchaResponse,
            authToken,
            mfa_token: this.mfaToken,
            google2step: this.google2step
        };
        return this.api.post('auth', user, { observe: 'response' })
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            this.user = response.body;
            APP_CONFIG.authHeaders.xUserName = response.headers.get('x-service-user-name');
            APP_CONFIG.authHeaders.xUserToken = response.headers.get('x-service-user-token');
            document.cookie = 'x-service-user-name=' + response.headers.get('x-service-user-name') + ';path=/';
            document.cookie = 'x-service-user-token=' + response.headers.get('x-service-user-token') + ';path=/';
            if (response.headers.get('x-service-info')) {
                /** @type {?} */
                const serviceInfo = JSON.parse(response.headers.get('x-service-info'));
                this.thirdPartyOnline = serviceInfo.thirdPartyOnline || false;
            }
            return this.initialise().subscribe((/**
             * @return {?}
             */
            () => {
                this.angulartics.eventTrack.next({
                    action: 'Auth succeeded',
                });
                return this.router.navigate(['/']);
            }), (/**
             * @param {?} resp
             * @return {?}
             */
            (resp) => {
                this.angulartics.eventTrack.next({
                    action: 'Auth failed',
                });
                if (this.parser.getCookie('x-service-login-token')) {
                    if ((/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))))
                        > this.failedLogins) {
                        this.failedLogins = (/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))));
                    }
                }
                document.cookie = 'x-service-login-token=' + btoa((/** @type {?} */ ((/** @type {?} */ ((this.failedLogins + 1)))))) + ';path=/';
                if (this.failedLogins >= this.loginLimit) {
                    this.recaptchaEnabled = true;
                }
                return throwError('Failed to authenticate');
            }));
        })));
    }
    /**
     * @return {?}
     */
    resume() {
        this.authResumed = true;
        if (this.parser.getCookie('x-service-user-name') || this.parser.getCookie('x-service-emulated-user-name')) {
            APP_CONFIG.authHeaders.xUserName = this.parser.getCookie('x-service-user-name');
            APP_CONFIG.authHeaders.xUserToken = this.parser.getCookie('x-service-user-token');
            this.isEmulated = this.parser.getCookie('x-service-emulated-user-name') !== false;
            if (this.parser.getCookie('x-service-emulated-user-name')) {
                APP_CONFIG.authHeaders.xUserName = this.parser.getCookie('x-service-emulated-user-name');
            }
            return this.api.get('auth', { observe: 'response' }).toPromise().then((/**
             * @param {?} response
             * @return {?}
             */
            (response) => {
                this.user = response.body;
                if (response.headers.get('x-service-info')) {
                    /** @type {?} */
                    const serviceInfo = JSON.parse(response.headers.get('x-service-info'));
                    this.thirdPartyOnline = serviceInfo.thirdPartyOnline || false;
                }
                this.authChangeEmit.next(true);
                return this.initialise().subscribe();
            })).catch((/**
             * @return {?}
             */
            () => {
                this.reset();
                return this.router.navigate(['/login']);
            }));
        }
        else {
            if (this.parser.getCookie('x-service-login-token')) {
                if ((/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))))
                    > this.failedLogins) {
                    this.failedLogins = (/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))));
                }
            }
            if ((/** @type {?} */ (this.failedLogins)) >= this.loginLimit) {
                this.recaptchaEnabled = true;
            }
        }
        return false;
    }
    /**
     * @private
     * @return {?}
     */
    getServiceProvider() {
        return this.cache.getOneThroughCache('service-provider', 2, {}, false).subscribe((/**
         * @param {?} provider
         * @return {?}
         */
        provider => {
            this.serviceProvider = provider;
            if (this.serviceProvider.status !== 'ACTIVE') {
                throwError('Service provider offline');
                return this.router.navigate(['/maintenance']);
            }
            this.recaptchaEnabled = this.serviceProvider.data.recaptchaEnabled;
        }));
    }
    /**
     * @return {?}
     */
    initialise() {
        /** @type {?} */
        const observables = [];
        if (this.authCheckTimer) {
            clearInterval(this.authCheckTimer);
        }
        // @TODO: await
        this.getServiceProvider();
        if (this.isAuthenticated()) {
            if (!this.analyticsLoaded) {
                observables.push(this.api.get('analytics-custom-vars').toPromise().then((/**
                 * @param {?} response
                 * @return {?}
                 */
                response => {
                    for (const dimension of (/** @type {?} */ (response))) {
                        for (const [key, value] of Object.entries(dimension)) {
                            try {
                                ga('set', key, value);
                                if (key === 'dimension1') {
                                    try {
                                        ga('set', 'userId', value);
                                        if (gclog) {
                                            gclog.setCustomAttribute('userId', value);
                                        }
                                    }
                                    catch (e) {
                                    }
                                    this.angulartics.eventTrack.next({
                                        action: 'Analytics initialised',
                                    });
                                }
                            }
                            catch (err) {
                            }
                        }
                    }
                })));
            }
            this.authCheckTimer = setInterval((/**
             * @return {?}
             */
            () => {
                return this.triggerPing();
            }), 2500);
        }
        return forkJoin(observables);
    }
    /**
     * @return {?}
     */
    cookiesExist() {
        return this.parser.getCookie('x-service-user-name') !== false
            || this.parser.getCookie('x-service-emulated-user-name') !== false;
    }
    /**
     * @return {?}
     */
    triggerPing() {
        if (!this.cookiesExist()) {
            this.reset();
            return this.router.navigate(['/login']);
        }
    }
    /**
     * @return {?}
     */
    reset() {
        this.user = false;
        this.cache.refresh();
        this.pages.refresh();
        this.analyticsLoaded = false;
        this.thirdPartyOnline = false;
        this.parser.deleteCookie('x-service-user-name', '/');
        this.parser.deleteCookie('x-service-emulated-user-name', '/');
        this.parser.deleteCookie('x-service-user-token', '/');
        this.parser.deleteCookie('TPSESSION', '/');
        APP_CONFIG.authHeaders = Object.assign({}, APP_CONFIG.authHeaders, {
            xUserName: null,
            xUserToken: null,
        });
        this.authChangeEmit.next(true);
        if (this.authCheckTimer) {
            clearInterval(this.authCheckTimer);
        }
    }
    /**
     * @return {?}
     */
    logout() {
        this.reset();
        this.router.navigate(['/login']);
        return this.api.delete('auth').subscribe((/**
         * @return {?}
         */
        () => {
            this.angulartics.eventTrack.next({
                action: 'Auth closed',
            });
        }), (/**
         * @return {?}
         */
        () => {
            this.angulartics.eventTrack.next({
                action: 'Auth closure failed',
            });
        }));
    }
}
AuthService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
    { type: NgModule, args: [{
                imports: [
                    TabsModule$1,
                ],
            },] }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: Router },
    { type: HttpClient },
    { type: Angulartics2 },
    { type: ParserService },
    { type: CacheService },
    { type: PageService }
];
/** @nocollapse */ AuthService.ngInjectableDef = ɵɵdefineInjectable({ factory: function AuthService_Factory() { return new AuthService(ɵɵinject(Router), ɵɵinject(HttpClient), ɵɵinject(Angulartics2), ɵɵinject(ParserService), ɵɵinject(CacheService), ɵɵinject(PageService)); }, token: AuthService, providedIn: "root" });
if (false) {
    /** @type {?} */
    AuthService.prototype.authChangeEmit;
    /** @type {?} */
    AuthService.prototype.authResumed;
    /** @type {?} */
    AuthService.prototype.user;
    /** @type {?} */
    AuthService.prototype.mfaToken;
    /** @type {?} */
    AuthService.prototype.google2step;
    /** @type {?} */
    AuthService.prototype.analyticsLoaded;
    /** @type {?} */
    AuthService.prototype.thirdPartyOnline;
    /** @type {?} */
    AuthService.prototype.recaptchaEnabled;
    /** @type {?} */
    AuthService.prototype.serviceProvider;
    /** @type {?} */
    AuthService.prototype.authCheckTimer;
    /** @type {?} */
    AuthService.prototype.isEmulated;
    /** @type {?} */
    AuthService.prototype.failedLogins;
    /** @type {?} */
    AuthService.prototype.loginLimit;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.api;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.angulartics;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.parser;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.cache;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.pages;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/spinner/spinner.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SpinnerService {
    constructor() {
        this.spinnerCache = new Set();
    }
    /**
     * @param {?} spinner
     * @return {?}
     */
    _register(spinner) {
        this.spinnerCache.add(spinner);
    }
    /**
     * @param {?} spinnerToRemove
     * @return {?}
     */
    _unregister(spinnerToRemove) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner === spinnerToRemove) {
                this.spinnerCache.delete(spinner);
            }
        }));
    }
    /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    _unregisterGroup(spinnerGroup) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.group === spinnerGroup) {
                this.spinnerCache.delete(spinner);
            }
        }));
    }
    /**
     * @return {?}
     */
    _unregisterAll() {
        this.spinnerCache.clear();
    }
    /**
     * @param {?} spinnerName
     * @return {?}
     */
    show(spinnerName) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.name === spinnerName) {
                spinner.show = true;
            }
        }));
    }
    /**
     * @param {?} spinnerName
     * @return {?}
     */
    hide(spinnerName) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.name === spinnerName) {
                spinner.show = false;
            }
        }));
    }
    /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    showGroup(spinnerGroup) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.group === spinnerGroup) {
                spinner.show = true;
            }
        }));
    }
    /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    hideGroup(spinnerGroup) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.group === spinnerGroup) {
                spinner.show = false;
            }
        }));
    }
    /**
     * @return {?}
     */
    showAll() {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => spinner.show = true));
    }
    /**
     * @return {?}
     */
    hideAll() {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => spinner.show = false));
    }
    /**
     * @param {?} spinnerName
     * @return {?}
     */
    isShowing(spinnerName) {
        /** @type {?} */
        let showing = undefined;
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.name === spinnerName) {
                showing = spinner.show;
            }
        }));
        return showing;
    }
}
SpinnerService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ SpinnerService.ngInjectableDef = ɵɵdefineInjectable({ factory: function SpinnerService_Factory() { return new SpinnerService(); }, token: SpinnerService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    SpinnerService.prototype.spinnerCache;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/auth/login/login.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoginComponent {
    /**
     * @param {?} authService
     * @param {?} spinner
     */
    constructor(authService, spinner) {
        this.authService = authService;
        this.spinner = spinner;
        this.loginForm = new FormGroup({
            username: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
        this.appName = APP_CONFIG.appName;
        this.copyright = APP_CONFIG.copyright;
    }
    /**
     * @return {?}
     */
    login() {
        this.loading = true;
        this.errorMessage = false;
        this.spinner.show('login');
        return this.authService.login(this.loginForm.get('username').value, this.loginForm.get('password').value).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            this.spinner.hide('login');
            this.loading = false;
            this.errorMessage = `<p>Please check your email address and password are correct, and try again.</p>
                                    <p>Error reference: ${error.headers.get('x-service-request-id')}</small></p>`;
            return throwError(`Error Code: ${error.status}\\nMessage: ${error.message}`);
        }))).subscribe((/**
         * @return {?}
         */
        () => {
            this.spinner.hide('login');
            this.loading = false;
        }));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
LoginComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-login',
                template: "<div class=\"page-wrapper\">\n    <div class=\"page-inner bg-brand-gradient\">\n        <div class=\"page-content-wrapper bg-transparent m-0\">\n            <div class=\"height-10 w-100 shadow-lg px-4 bg-brand-gradient\">\n                <div class=\"d-flex align-items-center container p-0\">\n                    <div class=\"page-logo width-mobile-auto m-0 align-items-center justify-content-center p-0 bg-transparent bg-img-none shadow-0 height-9\">\n                        <a href=\"javascript:void(0)\" class=\"page-logo-link press-scale-down d-flex align-items-center\">\n                            <span class=\"page-logo-text mr-1\">{{appName}}</span>\n                        </a>\n                    </div>\n                </div>\n            </div>\n            <div class=\"flex-1\"\n                 style=\"background: url(assets/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover;\">\n                <div class=\"container py-4 py-lg-5 my-lg-5 px-4 px-sm-0\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12 col-md-6 col-lg-5 col-xl-4 offset-xl-4 offset-md-3\">\n                            <h1 class=\"text-white fw-300 mb-3 d-sm-block d-md-none\">\n                                Secure login\n                            </h1>\n\n                            <div class=\"card p-4 rounded-plus bg-faded\">\n\n                                <form [formGroup]=\"loginForm\" (ngSubmit)=\"login()\">\n                                    <div class=\"form-group\">\n                                        <label class=\"form-label\">Username</label>\n                                        <input type=\"email\"\n                                               formControlName=\"username\"\n                                               name=\"username\"\n                                               class=\"form-control form-control-lg\"\n                                               required=\"true\"\n                                               [required]=\"true\"/>\n                                        <div class=\"invalid-feedback\">No, you missed this one.</div>\n                                        <div class=\"help-block\">Your username</div>\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label class=\"form-label\">Password</label>\n                                        <input type=\"password\"\n                                               formControlName=\"password\"\n                                               name=\"password\"\n                                               class=\"form-control form-control-lg\"\n                                               required=\"true\"\n                                               [required]=\"true\"/>\n                                        <div class=\"invalid-feedback\">Sorry, you missed this one.</div>\n                                        <div class=\"help-block\">Your password</div>\n                                    </div>\n\n                                    <div class=\"row no-gutters\">\n                                        <div class=\"col-lg-12 pl-lg-1 my-2\">\n                                            <button id=\"js-login-btn\" type=\"submit\"\n                                                    class=\"btn btn-primary btn-block btn-lg\"\n                                                    [class.spinner]=\"loading\"\n                                                    [disabled]=\"loading || !loginForm.valid\">\n                                                <core-spinner name=\"login\"></core-spinner>\n                                                <span>Secure Login</span>\n                                            </button>\n                                        </div>\n                                    </div>\n                                </form>\n\n                                <alert *ngIf=\"errorMessage\" class=\"alert alert-danger\" [innerHTML]=\"errorMessage\"></alert>\n\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"position-absolute pos-bottom pos-left pos-right p-3 text-center text-white\"\n                         [innerHTML]=\"copyright\"></div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"
            }] }
];
/** @nocollapse */
LoginComponent.ctorParameters = () => [
    { type: AuthService },
    { type: SpinnerService }
];
if (false) {
    /** @type {?} */
    LoginComponent.prototype.loading;
    /** @type {?} */
    LoginComponent.prototype.errorMessage;
    /** @type {?} */
    LoginComponent.prototype.loginForm;
    /** @type {?} */
    LoginComponent.prototype.appName;
    /** @type {?} */
    LoginComponent.prototype.copyright;
    /**
     * @type {?}
     * @protected
     */
    LoginComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    LoginComponent.prototype.spinner;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/spinner/spinner.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SpinnerComponent {
    /**
     * @param {?} spinnerService
     */
    constructor(spinnerService) {
        this.spinnerService = spinnerService;
        this.isShowing = false;
        this.showChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    get show() {
        return this.isShowing;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set show(val) {
        this.isShowing = val;
        this.showChange.emit(this.isShowing);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.name) {
            throw new Error('Spinner must have a \'name\' attribute.');
        }
        this.spinnerService._register(this);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.spinnerService._unregister(this);
    }
}
SpinnerComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-spinner',
                template: `
        <mat-spinner color="primary" diameter="30" *ngIf="show"></mat-spinner>
    `
            }] }
];
/** @nocollapse */
SpinnerComponent.ctorParameters = () => [
    { type: SpinnerService }
];
SpinnerComponent.propDecorators = {
    name: [{ type: Input }],
    group: [{ type: Input }],
    loadingImage: [{ type: Input }],
    show: [{ type: Input }],
    showChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    SpinnerComponent.prototype.name;
    /** @type {?} */
    SpinnerComponent.prototype.group;
    /** @type {?} */
    SpinnerComponent.prototype.loadingImage;
    /**
     * @type {?}
     * @private
     */
    SpinnerComponent.prototype.isShowing;
    /** @type {?} */
    SpinnerComponent.prototype.showChange;
    /**
     * @type {?}
     * @private
     */
    SpinnerComponent.prototype.spinnerService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/spinner/spinner.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SpinnerModule {
}
SpinnerModule.decorators = [
    { type: NgModule, args: [{
                declarations: [SpinnerComponent],
                imports: [CommonModule, MatProgressSpinnerModule],
                exports: [SpinnerComponent],
                providers: [SpinnerService]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/systemMessage.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SystemMessageService {
    /**
     * @param {?} systemMessageService
     */
    constructor(systemMessageService) {
        this.systemMessageService = systemMessageService;
    }
    /**
     * @return {?}
     */
    clearInterceptMessage() {
        this.interceptMessage = null;
    }
    /**
     * @param {?} message
     * @return {?}
     */
    setInterceptMessage(message) {
        this.interceptMessage = message;
    }
    /**
     * @return {?}
     */
    getInterceptMessage() {
        return this.interceptMessage;
    }
}
SystemMessageService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SystemMessageService.ctorParameters = () => [
    { type: SystemMessageService }
];
/** @nocollapse */ SystemMessageService.ngInjectableDef = ɵɵdefineInjectable({ factory: function SystemMessageService_Factory() { return new SystemMessageService(ɵɵinject(SystemMessageService)); }, token: SystemMessageService, providedIn: "root" });
if (false) {
    /** @type {?} */
    SystemMessageService.prototype.interceptMessage;
    /**
     * @type {?}
     * @private
     */
    SystemMessageService.prototype.systemMessageService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/auth/mfa-modal/mfa-modal.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MfaModalComponent {
    /**
     * @param {?} bsModalRef
     * @param {?} messages
     * @param {?} api
     * @param {?} router
     */
    constructor(bsModalRef, messages, api, router) {
        this.bsModalRef = bsModalRef;
        this.messages = messages;
        this.api = api;
        this.router = router;
        this.mfaToken = new FormControl('');
        this.mfaToken2 = new FormControl('');
        this.googleSecret = new FormControl('');
        this.secondaryUsername = new FormControl('');
        this.secondaryPassword = new FormControl('');
        this.mobile = new FormControl('');
        this.mfa = {
            type: 'google',
            state: 418,
        };
    }
    /**
     * @return {?}
     */
    submit() {
        /** @type {?} */
        const newData = Object.assign({}, this.interceptedResponse.response.request.body, {
            mfa_token: this.mfaToken.value,
            mfa_token2: this.mfaToken2.value,
            mfaType: this.mfa.type,
            google2step: this.getState() === 419,
            googleSecret: this.googleSecret.value,
            secondary_username: this.secondaryUsername.value,
            secondary_password: this.secondaryPassword.value,
            mobile: this.mobile.value
        });
        /** @type {?} */
        const responseHash = this.router.url + JSON.stringify(newData) + APP_CONFIG.authHeaders.xUserToken;
        /** @type {?} */
        const newHeaders = this.interceptedResponse.response.request.headers.set('x-service-request-hash', sha1(responseHash));
        /** @type {?} */
        const newRequest = this.interceptedResponse.response.request.clone({
            headers: newHeaders,
            body: newData,
            observe: (/** @type {?} */ ('response')),
        });
        console.log(newRequest);
        // return this.api.post('auth')(newRequest.body, null, {
        //         headers: newHeaders,
        //         // @TODO: Headers not set
        //     }).subscribe(
        //         this.interceptedResponse.subject,
        //         response => {
        //             this.mfa.mfaResponse = response.headers.get('x-service-mfa');
        //             this.setState(response.status);
        //
        //             switch (this.getState()) {
        //                 // case 417:
        //                 //     $rootScope.successMessage = {text: response.data};
        //                 //     $modalInstance.close();
        //                 //     break;
        //                 case 418:
        //                     return this._messages.setInterceptMessage('Invalid Authentication Code');
        //                 // case 419:
        //                 //     $rootScope.interceptMessage = {text: 'Enable Multi-Factor Authentication'};
        //                 //     $('div.modal-lg').addClass('modal-lg').removeClass('modal-sm');
        //                 //     break;
        //                 // case 421:
        //                 //     $rootScope.passwordPolicy = {
        //                 //         response: response,
        //                 //         deferred: $rootScope.twoStep.deferred
        //                 //     };
        //                 //     $rootScope.interceptMessage = {text: "Your password does not comply with the latest Core password policy, enforced in September 2019. Please update your password to ensure the utmost security of this application."};
        //                 //     $injector.invoke(['passwordPolicyModal', function (passwordPolicyModal) {
        //                 //         passwordPolicyModal.show();
        //                 //     }]);
        //                 //     return false;
        //             }
        //
        //         }
        //     );
    }
    /**
     * @return {?}
     */
    getInterceptMessage() {
        return this.messages.getInterceptMessage();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    closeModal($event) {
        this.bsModalRef.hide();
    }
    /**
     * @param {?} type
     * @return {?}
     */
    setType(type) {
        this.mfa.type = type;
    }
    /**
     * @return {?}
     */
    getType() {
        return this.mfa.type;
    }
    /**
     * @param {?} state
     * @return {?}
     */
    setState(state) {
        this.mfa.state = state;
    }
    /**
     * @return {?}
     */
    getState() {
        return this.mfa.state;
    }
    /**
     * @param {?} google
     * @return {?}
     */
    isMFAEnabled(google) {
        return false;
    }
}
MfaModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-mfa-modal',
                template: "<div class=\"modal-header\">\r\n    <h3 class=\"modal-title\">Multi-Factor Authentication</h3>\r\n</div>\r\n<div class=\"modal-content\">\r\n    <div class=\"modal-body\">\r\n        <div class=\"modal-body\">\r\n\r\n            <div (show)=\"getInterceptMessage() && getState() !== 421\">\r\n                <div class=\"new-item alert alert-danger\">\r\n                    <span class=\"fal fa-exclamation-circle\"></span>\r\n                    {{getInterceptMessage()}}\r\n                </div>\r\n            </div>\r\n\r\n            <div [ngSwitch]=\"mfa.state\">\r\n                <div class=\"form-group\" *ngSwitchCase=\"419\">\r\n\r\n                    <tabset>\r\n\r\n                        <tab heading=\"Google Authenticator Setup\" (click)=\"setType('google')\"\r\n                             *ngIf=\"!isMFAEnabled('google')\">\r\n                            <div class=\"box box-default\">\r\n                                <div class=\"box-body\">\r\n\r\n                                    <p>To setup Multi-Factor Authentication with Google, you will need to install the\r\n                                        free\r\n                                        Google Authenticator application on your mobile device, adding an additional\r\n                                        layer\r\n                                        of\r\n                                        security to this application. Please select your mobile phone operating system\r\n                                        below:</p>\r\n\r\n                                    <tabset>\r\n                                        <tab heading=\"Apple iOS\">\r\n                                            <div class=\"box box-default\">\r\n                                                <div class=\"box-body\">\r\n                                                    <h4>Downloading the app</h4>\r\n                                                    <ol>\r\n                                                        <li>Visit the iTunes App Store</li>\r\n                                                        <li>Search for Google Authenticator</li>\r\n                                                        <li>Download and install the application</li>\r\n                                                    </ol>\r\n\r\n                                                    <h4>Setting up the app</h4>\r\n                                                    <ol>\r\n                                                        <li>On your phone, open the Google Authenticator application\r\n                                                        </li>\r\n                                                        <li>Tap the plus icon</li>\r\n                                                        <li>You can add your account using the QR code below or\r\n                                                            manually:\r\n                                                            <ul>\r\n                                                                <li>Using Barcode: Tap \"Scan Barcode\" and then point\r\n                                                                    your\r\n                                                                    camera\r\n                                                                    at the QR code below\r\n                                                                </li>\r\n                                                                <li>Using Manual Entry: Tap \"Manual Entry\" and enter the\r\n                                                                    secret\r\n                                                                    key:\r\n                                                                    <!--                                                                <strong>{{twoStep.googleSecret}}</strong></li>-->\r\n                                                            </ul>\r\n                                                        </li>\r\n                                                        <li>The first response code will now show in your application,\r\n                                                            enter\r\n                                                            it\r\n                                                            into the box below to complete setup\r\n                                                        </li>\r\n                                                    </ol>\r\n                                                </div>\r\n                                            </div>\r\n                                        </tab>\r\n                                        <tab heading=\"Google Android\">\r\n                                            <div class=\"box box-default\">\r\n                                                <div class=\"box-body\">\r\n                                                    <h4>Downloading the app</h4>\r\n                                                    <ol>\r\n                                                        <li>Visit Google Play</li>\r\n                                                        <li>Search for Google Authenticator</li>\r\n                                                        <li>Download and install the application</li>\r\n                                                    </ol>\r\n\r\n                                                    <h4>Setting up the app</h4>\r\n                                                    <ol>\r\n                                                        <li>On your phone, open the Google Authenticator application\r\n                                                        </li>\r\n                                                        <li>If this is the first time you have used Authenticator, click\r\n                                                            the\r\n                                                            Add\r\n                                                            an account button. If you are adding a new account, choose\r\n                                                            \u201CAdd\r\n                                                            an\r\n                                                            account\u201D from the app\u2019s menu\r\n                                                        </li>\r\n                                                        <li>You can add your account using the QR code below or\r\n                                                            manually:\r\n                                                            <ul>\r\n                                                                <li>Using Barcode: Tap \"Scan Barcode\". If the\r\n                                                                    Authenticator\r\n                                                                    app\r\n                                                                    cannot locate a barcode scanner app on your phone,\r\n                                                                    you\r\n                                                                    might\r\n                                                                    be prompted to download and install one,\r\n                                                                    alternatively\r\n                                                                    use\r\n                                                                    the manual entry option below. If you want to\r\n                                                                    install a\r\n                                                                    barcode scanner app so you can complete the setup\r\n                                                                    process,\r\n                                                                    click install and then point your camera at the QR\r\n                                                                    code\r\n                                                                    below\r\n                                                                </li>\r\n                                                                <li>Using Manual Entry: Tap \"Manual Entry\" and enter the\r\n                                                                    secret\r\n                                                                    key:\r\n                                                                    <!--                                                                <strong>{{twoStep.googleSecret}}</strong><br/>Make sure-->\r\n                                                                    you've chosen to make the key Time based and press\r\n                                                                    'Save'\r\n                                                                </li>\r\n                                                            </ul>\r\n                                                        </li>\r\n                                                        <li>The first response code will now show in your application,\r\n                                                            enter\r\n                                                            it\r\n                                                            into the box below to complete setup\r\n                                                        </li>\r\n                                                    </ol>\r\n                                                </div>\r\n                                            </div>\r\n                                        </tab>\r\n                                        <tab heading=\"Microsoft Windows\">\r\n                                            <div class=\"box box-default\">\r\n                                                <div class=\"box-body\">\r\n                                                    <h4>Downloading the app</h4>\r\n                                                    <ol>\r\n                                                        <li>Visit the Windows App Store</li>\r\n                                                        <li>Search for Microsoft Authenticator</li>\r\n                                                        <li>Download and install the application</li>\r\n                                                    </ol>\r\n\r\n                                                    <h4>Setting up the app</h4>\r\n                                                    <ol>\r\n                                                        <li>On your phone, open the Microsoft Authenticator\r\n                                                            application\r\n                                                        </li>\r\n                                                        <li>Tap the Add (+) icon</li>\r\n                                                        <li>You can add your account using the QR code below or\r\n                                                            manually:\r\n                                                            <ul>\r\n                                                                <li>Using Barcode: Tap \"Scan Barcode\" and then point\r\n                                                                    your\r\n                                                                    camera\r\n                                                                    at the QR code below\r\n                                                                </li>\r\n                                                                <li>Using Manual Entry: Tap \"Manual Entry\" and enter the\r\n                                                                    secret\r\n                                                                    key:\r\n                                                                    <!--                                                                <strong>{{twoStep.googleSecret}}</strong></li>-->\r\n                                                            </ul>\r\n                                                        </li>\r\n                                                        <li>The first response code will now show in your application,\r\n                                                            enter\r\n                                                            it\r\n                                                            into the box below to complete setup\r\n                                                        </li>\r\n                                                    </ol>\r\n                                                </div>\r\n                                            </div>\r\n                                        </tab>\r\n                                    </tabset>\r\n\r\n                                    <p>This setup process needs to completed only once, subsequent access to this\r\n                                        application\r\n                                        will request the latest code shown in your authenticator application to be\r\n                                        entered\r\n                                        to\r\n                                        ensure maximum application security.</p>\r\n\r\n                                    <div class=\"input-group text-center\" style=\"width:100%; margin:14px 0;\">\r\n                                        <!--                                    <img ng-src=\"{{twoStep.qrcode}}\" alt=\"QR Code\"/>-->\r\n                                    </div>\r\n\r\n                                    <div class=\"row\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter Authentication Code</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"6\" name=\"mfa_token\"\r\n                                                       ng-model=\"twoStep.mfa_token\" class=\"form-control input-lg\"\r\n                                                       autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                                                       ng-required=\"twoStep.mfaType === 'google'\"/>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <div class=\"row\" ng-if=\"isMFAEnabled('sms')\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter your SMS Authentication Code</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"13\" name=\"mfa_token2\"\r\n                                                       ng-model=\"twoStep.mfa_token2\" class=\"form-control input-lg\"\r\n                                                       autocomplete=\"off\" style=\"letter-spacing: 15px;\"\r\n                                                       ng-required=\"twoStep.mfaType === 'google'\"/>\r\n                                            </div>\r\n                                            <small class=\"pull-right\">This is required to validate setup</small>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n                            </div>\r\n                        </tab>\r\n\r\n                        <tab heading=\"SMS Setup\" ng-click=\"twoStep.mfaType = 'sms'\" ng-if=\"!isMFAEnabled('sms')\">\r\n                            <div class=\"box box-default\">\r\n                                <div class=\"box-body\">\r\n\r\n                                    <p>To setup Multi-Factor Authentication with SMS, you will need you to enter your\r\n                                        mobile\r\n                                        phone number with country code.</p>\r\n\r\n                                    <div class=\"row\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter your mobile phone number</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-phone\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"13\" name=\"mobile\"\r\n                                                       ng-model=\"twoStep.mobile\"\r\n                                                       class=\"form-control input-lg\" autocomplete=\"off\"\r\n                                                       style=\"letter-spacing: 15px;\" placeholder=\"+44\" auto-focus\r\n                                                       ng-required=\"twoStep.mfaType === 'sms'\"/>\r\n                                            </div>\r\n                                            <small class=\"pull-right\">E.g. +44771234567</small>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <div class=\"row\" ng-if=\"isMFAEnabled('google')\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter your Google Authentication Code</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"6\" name=\"mfa_token2\"\r\n                                                       ng-model=\"twoStep.mfa_token2\" class=\"form-control input-lg\"\r\n                                                       autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                                                       ng-required=\"twoStep.mfaType === 'sms'\"/>\r\n                                            </div>\r\n                                            <small class=\"pull-right\">This is required to validate setup</small>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n                            </div>\r\n                        </tab>\r\n\r\n                    </tabset>\r\n\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngSwitchCase=\"418\">\r\n                    <div class=\"input-group\" *ngIf=\"mfa.type === 'google'\">\r\n                        <div class=\"input-group-prepend\"><i class=\"fal fa-lock\"></i></div>\r\n                        <input type=\"text\" maxlength=\"6\" [formControl]=\"mfaToken\"\r\n                               class=\"form-control form-control-lg\" autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                               [required]=\"true\"/>\r\n                    </div>\r\n\r\n                    <div *ngIf=\"mfa.type === 'sms'\">\r\n                        <p class=\"small\">An SMS with a 6-digit verification code has been sent to\r\n                            <!--                                                    {{mfa.mfaDetails.number}}-->\r\n                        </p>\r\n                        <div class=\"input-group\">\r\n                            <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                            <input type=\"text\" maxlength=\"6\" name=\"mfaToken\" ngModel\r\n                                   class=\"form-control input-lg\" autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                                   [required]=\"true\"/>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <a class=\"pull-right small\" href=\"javascript:void(0)\" (click)=\"setState(421)\">More\r\n                        options</a>\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngSwitchCase=\"420\">\r\n                    <div class=\"input-group\">\r\n                        <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                        <input type=\"text\" name=\"username\" placeholder=\"Username\" ng-model=\"twoStep.secondary_username\"\r\n                               class=\"form-control input-lg\" autocomplete=\"off\" readonly\r\n                               onfocus=\"this.removeAttribute('readonly');\" ng-required=\"true\"/>\r\n                        <input type=\"password\" placeholder=\"Password\" ng-model=\"twoStep.secondary_password\"\r\n                               class=\"form-control input-lg\" autocomplete=\"off\" readonly\r\n                               onfocus=\"this.removeAttribute('readonly');\" ng-required=\"true\"/>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngSwitchCase=\"421\">\r\n                    <h4>Try another way to sign in</h4>\r\n                    <ul class=\"list-group\">\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFAType('google')\"\r\n                            ng-show=\"isMFAEnabled('google')\">\r\n                            <i class=\"fal pull-left ga\"></i>Get a verification code from the\r\n                            <strong>Google Authenticator</strong> app\r\n                        </li>\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFATypeSetup('google')\"\r\n                            ng-show=\"!isMFAEnabled('google')\">\r\n                            <i class=\"fal pull-left ga\"></i>Setup <strong>Google Authenticator</strong>\r\n                        </li>\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFAType('sms')\"\r\n                            ng-show=\"isMFAEnabled('sms')\">\r\n                            <!--                        <i class=\"fal pull-left sms\"></i>Get an SMS verification code at {{twoStep.mfaDetails.number}}-->\r\n                        </li>\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFATypeSetup('sms')\"\r\n                            ng-show=\"!isMFAEnabled('sms')\">\r\n                            <i class=\"fal pull-left sms\"></i>Setup SMS verification\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"modal-footer\" (show)=\"getInterceptMessage() && getState() !== 421\">\r\n            <button class=\"btn btn-warning pull-right\" (click)=\"submit()\" ng-disabled=\"mfaForm.$invalid\">Submit</button>\r\n        </div>\r\n\r\n    </div>\r\n</div>"
            }] }
];
/** @nocollapse */
MfaModalComponent.ctorParameters = () => [
    { type: BsModalRef$1 },
    { type: SystemMessageService },
    { type: HttpClient },
    { type: Router }
];
if (false) {
    /** @type {?} */
    MfaModalComponent.prototype.mfaToken;
    /** @type {?} */
    MfaModalComponent.prototype.mfaToken2;
    /** @type {?} */
    MfaModalComponent.prototype.googleSecret;
    /** @type {?} */
    MfaModalComponent.prototype.secondaryUsername;
    /** @type {?} */
    MfaModalComponent.prototype.secondaryPassword;
    /** @type {?} */
    MfaModalComponent.prototype.mobile;
    /** @type {?} */
    MfaModalComponent.prototype.interceptedResponse;
    /** @type {?} */
    MfaModalComponent.prototype.mfa;
    /** @type {?} */
    MfaModalComponent.prototype.bsModalRef;
    /**
     * @type {?}
     * @private
     */
    MfaModalComponent.prototype.messages;
    /**
     * @type {?}
     * @private
     */
    MfaModalComponent.prototype.api;
    /**
     * @type {?}
     * @private
     */
    MfaModalComponent.prototype.router;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/auth/auth.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
const ɵ0$2 = { breadcrumbs: ['Login'] };
class AuthModule {
}
AuthModule.decorators = [
    { type: NgModule, args: [{
                declarations: [LoginComponent, MfaModalComponent],
                imports: [
                    CommonModule,
                    RouterModule.forChild([
                        { path: '', pathMatch: 'full', redirectTo: 'login' },
                        {
                            path: 'login', component: LoginComponent,
                            data: ɵ0$2
                        },
                    ]),
                    FormsModule,
                    ReactiveFormsModule,
                    MatProgressBarModule,
                    MatProgressSpinnerModule,
                    MatButtonModule,
                    MatIconModule,
                    SpinnerModule,
                    AlertModule$1,
                    TabsModule$1
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/block/block-block.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class Block {
    /**
     * @param {?} component
     */
    constructor(component) {
        this.component = component;
    }
}
if (false) {
    /** @type {?} */
    Block.prototype.id;
    /** @type {?} */
    Block.prototype.identifier;
    /** @type {?} */
    Block.prototype.widgetId;
    /** @type {?} */
    Block.prototype.content;
    /** @type {?} */
    Block.prototype.uid;
    /** @type {?} */
    Block.prototype.data;
    /** @type {?} */
    Block.prototype.component;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-compile.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockCompileDirective {
    /**
     * @param {?} vcRef
     * @param {?} compiler
     */
    constructor(vcRef, compiler) {
        this.vcRef = vcRef;
        this.compiler = compiler;
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        if (!this.coreBlockCompile) {
            if (this.compRef) {
                this.updateProperties();
                return;
            }
            throw Error('Template not specified');
        }
        this.vcRef.clear();
        this.compRef = null;
        /** @type {?} */
        const component = this.createDynamicComponent(this.coreBlockCompile);
        /** @type {?} */
        const module = this.createDynamicModule(component);
        this.compiler.compileModuleAndAllComponentsAsync(module)
            .then((/**
         * @param {?} moduleWithFactories
         * @return {?}
         */
        (moduleWithFactories) => {
            /** @type {?} */
            const compFactory = moduleWithFactories.componentFactories.find((/**
             * @param {?} a
             * @return {?}
             */
            a => a.componentType === component));
            this.compRef = this.vcRef.createComponent(compFactory);
            this.updateProperties();
        }));
    }
    /**
     * @return {?}
     */
    updateProperties() {
        for (const prop of Object.values(this.coreBlockCompileContext)) {
            this.compRef.instance[prop] = this.coreBlockCompileContext[prop];
        }
    }
    /**
     * @private
     * @param {?} template
     * @return {?}
     */
    createDynamicComponent(template) {
        class DynamicComponent {
            /**
             * @param {?} authService
             */
            constructor(authService) {
                this.authService = authService;
            }
            /**
             * @return {?}
             */
            ngOnInit() {
                this.user = this.authService.getUser();
            }
        }
        DynamicComponent.decorators = [
            { type: Component, args: [{
                        selector: 'core-dynamic-block',
                        template,
                    },] },
        ];
        /** @nocollapse */
        DynamicComponent.ctorParameters = () => [
            { type: AuthService }
        ];
        if (false) {
            /** @type {?} */
            DynamicComponent.prototype.user;
            /**
             * @type {?}
             * @protected
             */
            DynamicComponent.prototype.authService;
        }
        return DynamicComponent;
    }
    /**
     * @private
     * @param {?} component
     * @return {?}
     */
    createDynamicModule(component) {
        class DynamicModule {
        }
        DynamicModule.decorators = [
            { type: NgModule, args: [{
                        imports: [CommonModule, TranslateModule],
                        declarations: [component]
                    },] },
        ];
        return DynamicModule;
    }
}
BlockCompileDirective.decorators = [
    { type: Directive, args: [{
                selector: '[coreBlockCompile]'
            },] }
];
/** @nocollapse */
BlockCompileDirective.ctorParameters = () => [
    { type: ViewContainerRef },
    { type: Compiler }
];
BlockCompileDirective.propDecorators = {
    coreBlockCompile: [{ type: Input }],
    coreBlockCompileContext: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockCompileDirective.prototype.coreBlockCompile;
    /** @type {?} */
    BlockCompileDirective.prototype.coreBlockCompileContext;
    /** @type {?} */
    BlockCompileDirective.prototype.compRef;
    /**
     * @type {?}
     * @private
     */
    BlockCompileDirective.prototype.vcRef;
    /**
     * @type {?}
     * @private
     */
    BlockCompileDirective.prototype.compiler;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-abstract.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockAbstractComponent {
    /**
     * @param {?=} authService
     * @param {?=} pages
     * @param {?=} resolver
     * @param {?=} viewContainerRef
     * @param {?=} translate
     * @param {?=} compiler
     */
    constructor(authService, pages, resolver, viewContainerRef, translate, compiler) {
        this.authService = authService;
        this.pages = pages;
        this.resolver = resolver;
        this.viewContainerRef = viewContainerRef;
        this.translate = translate;
        this.compiler = compiler;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.authService) {
            this.user = this.authService.getUser();
        }
    }
    /**
     * @protected
     * @param {?} contents
     * @return {?}
     */
    createDynamicBlock(contents) {
        this.dynamicComponent = this.createNewComponent(contents);
        this.dynamicModule = this.compiler.compileModuleSync(this.createComponentModule(this.dynamicComponent));
    }
    /**
     * @protected
     * @param {?} componentType
     * @return {?}
     */
    createComponentModule(componentType) {
        class RuntimeComponentModule {
        }
        RuntimeComponentModule.decorators = [
            { type: NgModule, args: [{
                        imports: [TranslateModule],
                        declarations: [componentType],
                        entryComponents: [componentType]
                    },] },
        ];
        return RuntimeComponentModule;
    }
    /**
     * @protected
     * @param {?} contents
     * @return {?}
     */
    createNewComponent(contents) {
        class DynamicComponent {
            /**
             * @param {?} authService
             */
            constructor(authService) {
                this.authService = authService;
            }
            /**
             * @return {?}
             */
            ngOnInit() {
                this.contents = contents;
                this.user = this.authService.getUser();
            }
        }
        DynamicComponent.decorators = [
            { type: Component, args: [{
                        selector: 'core-block-dynamic',
                        template: `${contents}`,
                    },] },
        ];
        /** @nocollapse */
        DynamicComponent.ctorParameters = () => [
            { type: AuthService }
        ];
        if (false) {
            /** @type {?} */
            DynamicComponent.prototype.contents;
            /** @type {?} */
            DynamicComponent.prototype.user;
            /**
             * @type {?}
             * @protected
             */
            DynamicComponent.prototype.authService;
        }
        return DynamicComponent;
    }
    /**
     * @param {?} idx
     * @param {?=} item
     * @return {?}
     */
    trackByFn(idx, item) {
        return idx;
    }
}
BlockAbstractComponent.decorators = [
    { type: Component, args: [{
                template: "<core-block class=\"{{block.data.class}} {{block.data.visibility}}\"\n            *ngFor=\"let subBlock of block.content trackBy: trackByFn\"\n            [block]=\"subBlock\"\n            [ngClass]=\"{ 'stickyDiv': block.data.specialType == 'sticky',\n            'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n             'col-sm-12': block.data.columnsSmall == 'full',\n             'col-md-12': block.data.columnsNormal == 'full'}\"\n            [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n>\n</core-block>",
                changeDetection: ChangeDetectionStrategy.Default
            }] }
];
/** @nocollapse */
BlockAbstractComponent.ctorParameters = () => [
    { type: AuthService },
    { type: PageService },
    { type: ComponentFactoryResolver },
    { type: ViewContainerRef },
    { type: TranslateService },
    { type: Compiler }
];
BlockAbstractComponent.propDecorators = {
    block: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockAbstractComponent.prototype.block;
    /** @type {?} */
    BlockAbstractComponent.prototype.user;
    /** @type {?} */
    BlockAbstractComponent.prototype.dynamicComponent;
    /** @type {?} */
    BlockAbstractComponent.prototype.dynamicModule;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.pages;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.resolver;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.translate;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.compiler;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/search.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const EXCERPT_SIZE = 500;
class SearchService extends PageService {
    /**
     * @param {?} router
     * @param {?} api
     * @param {?} spinner
     * @param {?} translate
     */
    constructor(router, api, spinner, translate) {
        super(router, api);
        this.router = router;
        this.api = api;
        this.spinner = spinner;
        this.translate = translate;
    }
    /**
     * @return {?}
     */
    refresh() {
        this.limit = 10;
        this.offset = 0;
        this.results = [];
        this.eddie = false;
        this.totalResults = 0;
    }
    /**
     * @param {?} query
     * @return {?}
     */
    setQuery(query) {
        this.query = query;
    }
    /**
     * @return {?}
     */
    getQuery() {
        return this.query;
    }
    /**
     * @param {?} query
     * @return {?}
     */
    performSearch(query) {
        this.refresh();
        this.setQuery(query);
        this.spinner.show('search');
        return this.getOneThroughCache('search', null, {}, false, {
            q: query,
            limit: this.limit,
            offset: this.offset,
            excerpt: EXCERPT_SIZE
        }).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            if (response.response.docs) {
                /** @type {?} */
                const searchTermRegex = new RegExp(query, 'gi');
                this.results = [...response.response.docs];
                this.totalResults = response.response.numFound;
                for (const result of this.results) {
                    if (result.content) {
                        result.content = result.content[0].substring(0, 1000).replace(searchTermRegex, '<em>$&</em>');
                        if (response.highlighting[result.uniqueId].content) {
                            result.content = response.highlighting[result.uniqueId].content[0];
                        }
                        else if (response.highlighting[result.uniqueId].text && response.highlighting[result.uniqueId].text[0]) {
                            result.content = response.highlighting[result.uniqueId].text[0];
                        }
                        result.content = result.content.replace(/\|\|/g, ', ').replace(/\ufffd/g, ' ') + '&hellip;';
                        if (result.categories) {
                            result.category = result.categories[0];
                        }
                        // result.name = LanguageService.getPageSpecific('name', results);
                        // result.url = LanguageService.getPageSpecific('url', results);
                        // result.description = LanguageService.getPageSpecific('description', results);
                    }
                }
            }
        }));
    }
    /**
     * @return {?}
     */
    getTotalResults() {
        return this.totalResults;
    }
}
SearchService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchService.ctorParameters = () => [
    { type: Router },
    { type: HttpClient },
    { type: SpinnerService },
    { type: TranslateService }
];
/** @nocollapse */ SearchService.ngInjectableDef = ɵɵdefineInjectable({ factory: function SearchService_Factory() { return new SearchService(ɵɵinject(Router), ɵɵinject(HttpClient), ɵɵinject(SpinnerService), ɵɵinject(TranslateService)); }, token: SearchService, providedIn: "root" });
if (false) {
    /** @type {?} */
    SearchService.prototype.query;
    /** @type {?} */
    SearchService.prototype.limit;
    /** @type {?} */
    SearchService.prototype.offset;
    /** @type {?} */
    SearchService.prototype.results;
    /** @type {?} */
    SearchService.prototype.totalResults;
    /** @type {?} */
    SearchService.prototype.eddie;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.api;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.spinner;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.translate;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-wrapper/block-wrapper.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockWrapperComponent extends BlockAbstractComponent {
    constructor() {
        super();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} idx
     * @param {?=} item
     * @return {?}
     */
    trackByFn(idx, item) {
        return idx;
    }
}
BlockWrapperComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-wrapper',
                template: "<core-block class=\"{{block.data.class}} {{block.data.visibility}}\"\n            *ngFor=\"let subBlock of block.content trackBy: trackByFn\"\n            [block]=\"subBlock\"\n            [ngClass]=\"{ 'stickyDiv': block.data.specialType == 'sticky',\n            'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n             'col-sm-12': block.data.columnsSmall == 'full',\n             'col-md-12': block.data.columnsNormal == 'full'}\"\n            [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n>\n</core-block>"
            }] }
];
/** @nocollapse */
BlockWrapperComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-language/block-language.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockLanguageComponent extends BlockAbstractComponent {
    /**
     * @param {?} translate
     * @param {?} translateProvider
     */
    constructor(translate, translateProvider) {
        super();
        this.translate = translate;
        this.translateProvider = translateProvider;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.translate.getLanguages().subscribe((/**
         * @param {?} languages
         * @return {?}
         */
        languages => {
            this.languages$ = languages;
        }));
    }
    /**
     * @param {?} languageCode
     * @return {?}
     */
    setLanguage(languageCode) {
        this.translateProvider.use(languageCode);
        this.translate.setDefaultLanguage(languageCode);
    }
}
BlockLanguageComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-language',
                template: "<mat-nav-list>\n    <mat-list-item *ngFor=\"let language of languages$\" [ngClass]=\"{'active' : language.code === ''}\">\n        <a matLine (click)=\"setLanguage(language.code)\">{{language.name}}</a>\n    </mat-list-item>\n</mat-nav-list>"
            }] }
];
/** @nocollapse */
BlockLanguageComponent.ctorParameters = () => [
    { type: TranslateService },
    { type: TranslateService$1 }
];
if (false) {
    /** @type {?} */
    BlockLanguageComponent.prototype.languages$;
    /** @type {?} */
    BlockLanguageComponent.prototype.language;
    /**
     * @type {?}
     * @protected
     */
    BlockLanguageComponent.prototype.translate;
    /**
     * @type {?}
     * @protected
     */
    BlockLanguageComponent.prototype.translateProvider;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-paragraph/block-paragraph.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockParagraphComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockParagraphComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-paragraph',
                template: `
        <ng-container *coreBlockCompile="text; context: this"></ng-container>
    `
            }] }
];
/** @nocollapse */
BlockParagraphComponent.ctorParameters = () => [
    { type: AuthService }
];
BlockParagraphComponent.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockParagraphComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockParagraphComponent.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-paragraph-lead/block-paragraph-lead.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockParagraphLeadComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockParagraphLeadComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-paragraph-lead',
                template: `
        <ng-container *coreBlockCompile="text; context: this" class="lead"></ng-container>
    `
            }] }
];
/** @nocollapse */
BlockParagraphLeadComponent.ctorParameters = () => [
    { type: AuthService }
];
BlockParagraphLeadComponent.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockParagraphLeadComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockParagraphLeadComponent.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-table/block-table.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockTableComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.header = this.block.data.tableData[0];
        this.data = this.block.data.tableData;
        this.data.shift();
    }
    /**
     * @param {?} idx
     * @return {?}
     */
    trackByFn(idx) {
        return idx;
    }
}
BlockTableComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-table',
                template: "<div class=\"table-wrapper\">\n    <table class=\"table\">\n        <thead>\n        <tr>\n            <th *ngFor=\"let cell of header trackBy: trackByFn\" [innerHTML]=\"cell.content\"></th>\n        </tr>\n        </thead>\n        <tbody>\n        <tr *ngFor=\"let row of data trackBy: trackByFn\">\n            <td *ngFor=\"let cell of row trackBy: trackByFn\" [innerHTML]=\"cell.content\"></td>\n        </tr>\n        </tbody>\n    </table>\n</div>"
            }] }
];
/** @nocollapse */
BlockTableComponent.ctorParameters = () => [
    { type: AuthService }
];
if (false) {
    /** @type {?} */
    BlockTableComponent.prototype.header;
    /** @type {?} */
    BlockTableComponent.prototype.data;
    /**
     * @type {?}
     * @protected
     */
    BlockTableComponent.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-accordion/block-accordion.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockAccordionComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} idx
     * @return {?}
     */
    trackByFn(idx) {
        return idx;
    }
}
BlockAccordionComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-accordion',
                template: "<mat-accordion>\n    <mat-expansion-panel *ngFor=\"let item of block.content trackBy: trackByFn\">\n        <mat-expansion-panel-header>\n            <mat-panel-title [innerHTML]=\"item.data.title\"></mat-panel-title>\n        </mat-expansion-panel-header>\n        <div class=\"{{block.data.title}} {{block.data.visibility}}\"\n             [ngClass]=\"{container: block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n                 'stickyDiv': block.data.specialType == 'sticky',\n                  'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n                  'col-sm-12': block.data.columnsSmall == 'full',\n                  'col-md-12': block.data.columnsNormal == 'full'}\"\n             [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n        >\n            <core-block\n                    *ngFor=\"let subBlock of item.content\"\n                    [block]=\"subBlock\"\n                    class=\"subWidget {{subBlock.data.class}}\">\n            </core-block>\n        </div>\n    </mat-expansion-panel>\n</mat-accordion>\n"
            }] }
];
/** @nocollapse */
BlockAccordionComponent.ctorParameters = () => [
    { type: AuthService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockAccordionComponent.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-h1/block-h1.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockH1Component extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockH1Component.decorators = [
    { type: Component, args: [{
                selector: 'core-block-h1',
                template: `
        <h1>
            <ng-container *coreBlockCompile="text; context: this"></ng-container>
        </h1>`
            }] }
];
/** @nocollapse */
BlockH1Component.ctorParameters = () => [
    { type: AuthService }
];
BlockH1Component.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockH1Component.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockH1Component.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-h2/block-h2.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockH2Component extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockH2Component.decorators = [
    { type: Component, args: [{
                selector: 'core-block-h2',
                template: `
        <h2>
            <ng-container *coreBlockCompile="text; context: this"></ng-container>
        </h2>`
            }] }
];
/** @nocollapse */
BlockH2Component.ctorParameters = () => [
    { type: AuthService }
];
BlockH2Component.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockH2Component.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockH2Component.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-h3/block-h3.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockH3Component extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockH3Component.decorators = [
    { type: Component, args: [{
                selector: 'core-block-h3',
                template: `
        <h3>
            <ng-container *coreBlockCompile="text; context: this"></ng-container>
        </h3>`
            }] }
];
/** @nocollapse */
BlockH3Component.ctorParameters = () => [
    { type: AuthService }
];
BlockH3Component.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockH3Component.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockH3Component.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-h4/block-h4.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockH4Component extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockH4Component.decorators = [
    { type: Component, args: [{
                selector: 'core-block-h4',
                template: `
        <h4>
            <ng-container *coreBlockCompile="text; context: this"></ng-container>
        </h4>`
            }] }
];
/** @nocollapse */
BlockH4Component.ctorParameters = () => [
    { type: AuthService }
];
BlockH4Component.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockH4Component.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockH4Component.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-h5/block-h5.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockH5Component extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockH5Component.decorators = [
    { type: Component, args: [{
                selector: 'core-block-h5',
                template: `
        <h5>
            <ng-container *coreBlockCompile="text; context: this"></ng-container>
        </h5>`
            }] }
];
/** @nocollapse */
BlockH5Component.ctorParameters = () => [
    { type: AuthService }
];
BlockH5Component.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockH5Component.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockH5Component.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-h6/block-h6.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockH6Component extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockH6Component.decorators = [
    { type: Component, args: [{
                selector: 'core-block-h6',
                template: `
        <h6>
            <ng-container *coreBlockCompile="text; context: this"></ng-container>
        </h6>`
            }] }
];
/** @nocollapse */
BlockH6Component.ctorParameters = () => [
    { type: AuthService }
];
BlockH6Component.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockH6Component.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockH6Component.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-image/block-image.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockImageComponent extends BlockAbstractComponent {
    constructor() {
        super();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.url = this.block.data.image_url;
        this.alt = this.block.data.alt_text;
    }
}
BlockImageComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-image',
                template: `
        <img class="img-responsive image-block" [src]="url" alt="{{alt}}"/>`
            }] }
];
/** @nocollapse */
BlockImageComponent.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    BlockImageComponent.prototype.url;
    /** @type {?} */
    BlockImageComponent.prototype.alt;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-breadcrumbs/block-breadcrumbs.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockBreadcrumbsComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     * @param {?} pages
     */
    constructor(authService, pages) {
        super(authService);
        this.authService = authService;
        this.pages = pages;
        this.date = Date.now();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.pages.getBreadcrumbs(this.pages.getCurrentPage().id).subscribe((/**
         * @param {?} breadcrumbs
         * @return {?}
         */
        breadcrumbs => {
            this.breadcrumbs = breadcrumbs || [];
        }));
    }
}
BlockBreadcrumbsComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-breadcrumbs',
                template: "<ol class=\"breadcrumb page-breadcrumb\">\n    <li class=\"breadcrumb-item\"><a href=\"#\" coreStubClick>Home</a></li>\n    <li class=\"breadcrumb-item\" *ngFor=\"let breadcrumb of breadcrumbs.parents\">\n        <a [routerLink]=\"breadcrumb.url\">{{breadcrumb.name}}</a>\n    </li>\n    <li class=\"breadcrumb-item\">{{breadcrumbs.name}}</li>\n    <li class=\"position-absolute pos-top pos-right d-none d-sm-block\">\n        <span>{{ date | date }}</span>\n    </li>\n</ol>"
            }] }
];
/** @nocollapse */
BlockBreadcrumbsComponent.ctorParameters = () => [
    { type: AuthService },
    { type: PageService }
];
if (false) {
    /** @type {?} */
    BlockBreadcrumbsComponent.prototype.breadcrumbs;
    /** @type {?} */
    BlockBreadcrumbsComponent.prototype.date;
    /**
     * @type {?}
     * @protected
     */
    BlockBreadcrumbsComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    BlockBreadcrumbsComponent.prototype.pages;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-carousel/block-carousel.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockCarouselComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
BlockCarouselComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-carousel',
                template: "CAROUSEL"
            }] }
];
/** @nocollapse */
BlockCarouselComponent.ctorParameters = () => [
    { type: AuthService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockCarouselComponent.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-video/block-video.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockVideoComponent extends BlockAbstractComponent {
    constructor() {
        super();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
BlockVideoComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-video',
                template: "VIDEO"
            }] }
];
/** @nocollapse */
BlockVideoComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-bullets/block-bullets.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockBulletsComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
    }
    /**
     * @param {?} idx
     * @return {?}
     */
    trackByFn(idx) {
        return idx;
    }
}
BlockBulletsComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-bullets',
                template: "<mat-list>\n    <mat-list-item *ngFor=\"let item of block.content trackBy: trackByFn\" [innerHTML]=\"item.data.text\"></mat-list-item>\n</mat-list>"
            }] }
];
/** @nocollapse */
BlockBulletsComponent.ctorParameters = () => [
    { type: AuthService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockBulletsComponent.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-numbered/block-numbered.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockNumberedComponent extends BlockAbstractComponent {
    constructor() {
        super();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
    }
    /**
     * @param {?} idx
     * @return {?}
     */
    trackByFn(idx) {
        return idx;
    }
}
BlockNumberedComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-numbered',
                template: "<mat-list>\n    <mat-list-item *ngFor=\"let item of block.content trackBy: trackByFn\" [innerHTML]=\"item.data.text\"></mat-list-item>\n</mat-list>"
            }] }
];
/** @nocollapse */
BlockNumberedComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-blockquote/block-blockquote.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockBlockquoteComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.blockQuote;
    }
}
BlockBlockquoteComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-blockquote',
                template: "<blockquote>\n    <p>\n        <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n    </p>\n    <footer *ngIf=\"block.data.cite\">\n        <cite>\n            <a [href]=\"block.data.citeLink\" *ngIf=\"block.data.citeLink\">{{block.data.cite}}</a>\n            <span *ngIf=\"!block.data.citeLink\">{{block.data.cite}}</span>\n        </cite>\n    </footer>\n</blockquote>"
            }] }
];
/** @nocollapse */
BlockBlockquoteComponent.ctorParameters = () => [
    { type: AuthService }
];
BlockBlockquoteComponent.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockBlockquoteComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockBlockquoteComponent.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-download/block-download.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockDownloadComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
    }
}
BlockDownloadComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-download',
                template: "<div class=\"download\">\n    <p *ngIf=\"block.data.downloadText\" [innerHTML]=\"block.data.downloadText\"></p>\n    <a [href]=\"block.data.documentId ? block.data.documentId : block.data.buttonLink\"\n       target=\"{{block.data.target}}\" class=\"btn btn-primary\" role=\"button\">{{block.data.buttonText}}</a>\n</div>\n"
            }] }
];
/** @nocollapse */
BlockDownloadComponent.ctorParameters = () => [
    { type: AuthService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockDownloadComponent.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-button/block-button.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockButtonComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.buttonPreText;
    }
}
BlockButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-button',
                template: "<div class=\"button-block\">\n    <p *ngIf=\"block.data.buttonPreText\">\n        <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n    </p>\n    <a [href]=\"block.data.documentId ? block.data.documentId : block.data.buttonLink\"\n       target=\"{{block.data.target}}\" class=\"btn {{block.data.buttonType}}\" role=\"button\">{{block.data.buttonText}}\n    </a>\n</div>\n"
            }] }
];
/** @nocollapse */
BlockButtonComponent.ctorParameters = () => [
    { type: AuthService }
];
BlockButtonComponent.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockButtonComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockButtonComponent.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-divider/block-divider.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockDividerComponent extends BlockAbstractComponent {
    constructor() {
        super();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
    }
}
BlockDividerComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-divider',
                template: `
        <hr class="divider"/>
    `
            }] }
];
/** @nocollapse */
BlockDividerComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-progress/block-progress.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockProgressComponent extends BlockAbstractComponent {
    constructor() {
        super();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
    }
}
BlockProgressComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-progress',
                template: `
        <mat-progress-bar mode="indeterminate"></mat-progress-bar>
    `
            }] }
];
/** @nocollapse */
BlockProgressComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-tabs/block-tabs.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockTabsComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} idx
     * @return {?}
     */
    trackByFn(idx) {
        return idx;
    }
}
BlockTabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-tabs',
                template: "<mat-tab-group>\n    <mat-tab *ngFor=\"let item of block.content trackBy: trackByFn\">\n        <ng-template mat-tab-label>\n            <ng-container>{{item.data.header}}</ng-container>\n        </ng-template>\n        <div [innerHTML]=\"item.data.content\"></div>\n    </mat-tab>\n</mat-tab-group>\n"
            }] }
];
/** @nocollapse */
BlockTabsComponent.ctorParameters = () => [
    { type: AuthService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockTabsComponent.prototype.authService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-search-results/block-search-results.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockSearchResultsComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     * @param {?} search
     */
    constructor(authService, search) {
        super(authService);
        this.authService = authService;
        this.search = search;
        this.feedback = new FormControl();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.reset();
    }
    /**
     * @return {?}
     */
    reset() {
        this.feedbackEnabled = this.block.data.showFeedback;
        this.eddieExpanded = false;
    }
    /**
     * @param {?} idx
     * @return {?}
     */
    trackByFn(idx) {
        return idx;
    }
    /**
     * @return {?}
     */
    getResults() {
        return this.search.results;
    }
    /**
     * @return {?}
     */
    hasResults() {
        return this.search.results.length > 0;
    }
    /**
     * @return {?}
     */
    getEddie() {
        return this.search.eddie;
    }
    /**
     * @return {?}
     */
    hasEddie() {
        return this.search.eddie !== false;
    }
    /**
     * @return {?}
     */
    hasSearchTerm() {
        return true;
    }
    /**
     * @param {?=} type
     * @return {?}
     */
    hasFeedback(type) {
        return false;
    }
    /**
     * @return {?}
     */
    givePositiveFeedback() {
    }
    /**
     * @return {?}
     */
    giveNegativeFeedback() {
    }
    /**
     * @return {?}
     */
    closeFeedback() {
    }
    /**
     * @param {?} query
     * @return {?}
     */
    performSearch(query) {
    }
    /**
     * @return {?}
     */
    loadMore() {
    }
    /**
     * @return {?}
     */
    getTotalResults() {
        return this.search.getTotalResults();
    }
}
BlockSearchResultsComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-search-results',
                template: "<div class=\"search-results-content\">\n\n    <core-spinner name=\"search\"></core-spinner>\n\n    <div class=\"row search-top\">\n        <div class=\"col-sm-12 text-center\">\n            <p>\n                <span *ngIf=\"!hasResults() && !hasEddie() && hasSearchTerm()\"\n                      [innerHTML]=\"noResultsText\"></span>\n                <span *ngIf=\"hasResults() && introText\" [innerHTML]=\"introText\"></span>\n            </p>\n        </div>\n    </div>\n\n    <h3 *ngIf=\"hasResults() || hasEddie()\">Results</h3>\n\n    <!-- Eddie -->\n    <div class=\"eddie\" *ngIf=\"hasEddie()\">\n        <h4 class=\"eddie__question\" [innerHTML]=\"getEddie().question\"></h4>\n\n        <div [hidden]=\"eddieExpanded\">\n            <div class=\"eddie__answer\" [innerHTML]=\"getEddie().answer | limitTo: 150\"></div>\n            <div class=\"eddie__ellipsis\" *ngIf=\"getEddie().answer.length > 150\">...</div>\n        </div>\n\n        <div class=\"eddie__answer-expanded\" [hidden]=\"!eddieExpanded\">\n            <div class=\"eddie__answer expanded\" [innerHTML]=\"getEddie().answer\"></div>\n            <div class=\"eddie__helpful-links\" *ngIf=\"getEddie().supplementalData.links.length\">\n                <h5>Helpful Links</h5>\n                <a *ngFor=\"let link of getEddie().supplementalData.links\" [href]=\"link.href\"\n                   target=\"_blank\">{{link.label}}</a>\n            </div>\n\n            <div class=\"eddie__feeback\">\n                <div class=\"eddie__feedback-question\">Did this answer your question?</div>\n                <div class=\"eddie__feedback-answers-block\">\n                    <button [disabled]=\"hasFeedback()\" (click)=\"givePositiveFeedback()\"\n                            [ngClass]=\"{'active' : hasFeedback('positive') }\" class=\"btn btn-feedback\">\n                        Yes\n                    </button>\n                    <button [disabled]=\"hasFeedback()\" (click)=\"giveNegativeFeedback()\"\n                            [ngClass]=\"{'active' : hasFeedback('negative') }\" class=\"btn btn-feedback\">\n                        No\n                    </button>\n                </div>\n            </div>\n\n            <div [hidden]=\"!supportSubmitted\" class=\"alert alert-success\">\n                <p><strong>Thank you, we really value your feedback.</strong>\n                    <br/>The search and support feature is evolving all the time \u2013 the more you use it the better we can\n                    make it!</p>\n            </div>\n\n            <div class=\"eddie__feedback-form\" *ngIf=\"feedbackEnabled\">\n                <button type=\"button\" class=\"eddie__feedback-close\"\n                        (click)=\"closeFeedback();\">\n                    <img src=\"/assets/img/close.svg\" alt=\"Close\"/>\n                </button>\n\n                <p><strong>We really value your feedback, please tell us how we could improve this answer.</strong></p>\n                <form #feedbackForm=\"ngForm\">\n                    <div class=\"form-group\">\n                        <label>\n                            <span class=\"sr-only\">Message</span>\n                            <textarea class=\"form-control\" [formControl]=\"feedback\"></textarea>\n                        </label>\n                    </div>\n\n                    <div class=\"form-button\">\n                        <button class=\"btn btn-primary\"\n                                [disabled]=\"!feedback || feedback.value.length < 5 || supportSubmitted\"\n                                (click)=\"giveNegativeFeedback()\">Leave feedback\n                        </button>\n                    </div>\n                </form>\n            </div>\n        </div>\n\n        <div class=\"eddie__expand-button\"\n             (click)=\"eddieExpanded = !eddieExpanded\">{{eddieExpanded ? 'Hide full answer' : 'Show full answer'}}</div>\n    </div>\n\n    <div *ngIf=\"getEddie() && getEddie().relatedQuestions.length > 0\">\n        <core-block-divider></core-block-divider>\n        <div class=\"eddie-related\">\n            <h5>Your colleagues are also asking</h5>\n            <ul>\n                <li *ngFor=\"let question of getEddie().relatedQuestions\">\n                    <a (click)=\"performSearch(question);\">{{ question }}</a>\n                </li>\n            </ul>\n        </div>\n    </div>\n\n    <core-block-divider *ngIf=\"hasResults() && hasEddie()\"></core-block-divider>\n\n    <!-- Regular results -->\n    <div class=\"additional-results\" *ngIf=\"hasResults()\">\n        <h5 *ngIf=\"hasEddie()\">We also found related content on these pages</h5>\n\n        <ul class=\"search-results-list no-bullet\">\n            <li class=\"search-results-list__item\" *ngFor=\"let result of getResults() trackBy: trackByFn\">\n                <div class=\"search-results-wrapper\">\n                    <a class=\"search-results-list__link\" *ngIf=\"result.url\" [routerLink]=\"'/page/' + result.url\">\n                        <p class=\"result-name\" [innerHTML]=\"result.name\"></p>\n                    </a>\n                    <a class=\"search-results-list__link\" *ngIf=\"result.nodeId\"\n                       [href]=\"'/api/1.0/download/' + result.nodeId\"\n                       [target]=\"'_blank'\">\n                        <p class=\"result-name\" [innerHTML]=\"result.name\"></p>\n                    </a>\n                    <!--                    <div core-breadcrumb page=\"result\" ng-if=\"result.pageId\"></div>-->\n                    <p>{{ result.description | limitTo: 100 }}<span *ngIf=\"result.description.length > 100\">...</span>\n                    </p>\n                </div>\n            </li>\n        </ul>\n\n        <div class=\"button-wrapper text-left\" *ngIf=\"hasResults() && getResults().length < getTotalResults()\">\n            <button class=\"btn btn-default\" (click)=\"loadMore()\">{{block.data.loadMoreText}}</button>\n        </div>\n    </div>\n\n    <div *ngIf=\"hasResults() || hasEddie()\">\n        <core-block-divider></core-block-divider>\n        <p><strong>Not what you were looking for? Try searching again.</strong></p>\n    </div>\n\n</div>"
            }] }
];
/** @nocollapse */
BlockSearchResultsComponent.ctorParameters = () => [
    { type: AuthService },
    { type: SearchService }
];
if (false) {
    /** @type {?} */
    BlockSearchResultsComponent.prototype.feedbackEnabled;
    /** @type {?} */
    BlockSearchResultsComponent.prototype.eddieExpanded;
    /** @type {?} */
    BlockSearchResultsComponent.prototype.supportSubmitted;
    /** @type {?} */
    BlockSearchResultsComponent.prototype.noResultsText;
    /** @type {?} */
    BlockSearchResultsComponent.prototype.introText;
    /** @type {?} */
    BlockSearchResultsComponent.prototype.feedback;
    /**
     * @type {?}
     * @protected
     */
    BlockSearchResultsComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    BlockSearchResultsComponent.prototype.search;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-search/block-search.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockSearchComponent extends BlockAbstractComponent {
    /**
     * @param {?} search
     */
    constructor(search) {
        super();
        this.search = search;
        this.query = new FormControl();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.search.refresh();
        this.query.setValue(this.search.getQuery());
    }
    /**
     * @return {?}
     */
    performSearch() {
        this.search.performSearch(this.query.value);
    }
}
BlockSearchComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-search',
                template: "<form #searchForm=\"ngForm\" (ngSubmit)=\"performSearch()\">\n    <div class=\"form-group\">\n        <label class=\"form-label question-box\">\n            <span class=\"sr-only\">Search</span></label>\n        <input type=\"text\" class=\"form-control form-control-lg\" [formControl]=\"query\"\n               placeholder=\"Type your question...\"/>\n    </div>\n\n    <button class=\"btn btn-primary\" [ngClass]=\"{'selected': query.value && query.value.length > 2}\"\n            [disabled]=\"!query.value || query.value.length < 3\">Search\n    </button>\n</form>"
            }] }
];
/** @nocollapse */
BlockSearchComponent.ctorParameters = () => [
    { type: SearchService }
];
if (false) {
    /** @type {?} */
    BlockSearchComponent.prototype.query;
    /**
     * @type {?}
     * @protected
     */
    BlockSearchComponent.prototype.search;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/block.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockService {
    constructor() {
        this.replaceBlocks({
            WRAPPER: BlockWrapperComponent,
            LANGUAGE: BlockLanguageComponent,
            PARAGRAPH: BlockParagraphComponent,
            'PARAGRAPH-LEAD': BlockParagraphLeadComponent,
            TABLE: BlockTableComponent,
            ACCORDION: BlockAccordionComponent,
            H1: BlockH1Component,
            H2: BlockH2Component,
            H3: BlockH3Component,
            H4: BlockH4Component,
            H5: BlockH5Component,
            H6: BlockH6Component,
            IMAGE: BlockImageComponent,
            BREADCRUMBS: BlockBreadcrumbsComponent,
            CAROUSEL: BlockCarouselComponent,
            VIDEO: BlockVideoComponent,
            BULLETS: BlockBulletsComponent,
            NUMBERED: BlockNumberedComponent,
            BLOCKQUOTE: BlockBlockquoteComponent,
            DOWNLOAD: BlockDownloadComponent,
            BUTTON: BlockButtonComponent,
            DIVIDER: BlockDividerComponent,
            PROGRESS: BlockProgressComponent,
            TABS: BlockTabsComponent,
            'SEARCH-RESULTS': BlockSearchResultsComponent,
            SEARCH: BlockSearchComponent,
        });
    }
    /**
     * @param {?} blocks
     * @return {?}
     */
    replaceBlocks(blocks) {
        this.blocks = blocks;
    }
    /**
     * @return {?}
     */
    getBlocks() {
        return this.blocks;
    }
    /**
     * @param {?} identifier
     * @return {?}
     */
    hasBlock(identifier) {
        return this.blocks.hasOwnProperty(identifier);
    }
    /**
     * @param {?} identifier
     * @return {?}
     */
    getBlock(identifier) {
        return this.blocks[identifier];
    }
    /**
     * @param {?} identifier
     * @param {?} component
     * @return {?}
     */
    addBlock(identifier, component) {
        this.blocks[identifier] = component;
    }
}
BlockService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
BlockService.ctorParameters = () => [];
/** @nocollapse */ BlockService.ngInjectableDef = ɵɵdefineInjectable({ factory: function BlockService_Factory() { return new BlockService(); }, token: BlockService, providedIn: "root" });
if (false) {
    /** @type {?} */
    BlockService.prototype.blocks;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/page.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PageComponent {
    /**
     * @param {?} route
     * @param {?} router
     * @param {?} pages
     */
    constructor(route, router, pages) {
        this.route = route;
        this.router = router;
        this.pages = pages;
        this.appName = APP_CONFIG.appName;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.route.paramMap.subscribe((/**
         * @param {?} params
         * @return {?}
         */
        params => {
            this.subscription = this.pages.getPage(params.get('identifier') || 'default').pipe(map((/**
             * @param {?} page
             * @return {?}
             */
            (page) => {
                this.pages.setCurrentPage(page);
                this.page$ = of(page);
            }))).subscribe();
        }));
    }
    /**
     * @param {?} idx
     * @param {?} item
     * @return {?}
     */
    trackByFn(idx, item) {
        return item.uid + '_' + idx;
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        console.log('Page destroyed');
        this.subscription.unsubscribe();
    }
}
PageComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-home',
                template: "<div *ngIf=\"page$ | async as page\">\n    <core-block *ngFor=\"let block of page.widgets trackBy: trackByFn\" [block]=\"block\" class=\"core-block-l1\"></core-block>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.Default
            }] }
];
/** @nocollapse */
PageComponent.ctorParameters = () => [
    { type: ActivatedRoute },
    { type: Router },
    { type: PageService }
];
if (false) {
    /** @type {?} */
    PageComponent.prototype.appName;
    /** @type {?} */
    PageComponent.prototype.page$;
    /** @type {?} */
    PageComponent.prototype.subscription;
    /**
     * @type {?}
     * @private
     */
    PageComponent.prototype.route;
    /**
     * @type {?}
     * @private
     */
    PageComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    PageComponent.prototype.pages;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/block/block.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BlockComponent {
    /**
     * @param {?} pages
     * @param {?} blocks
     * @param {?} resolver
     * @param {?} viewContainerRef
     */
    constructor(pages, blocks, resolver, viewContainerRef) {
        this.pages = pages;
        this.blocks = blocks;
        this.resolver = resolver;
        this.viewContainerRef = viewContainerRef;
    }
    /**
     * @return {?}
     */
    loadComponent() {
        if (!this.blocks.hasBlock(this.blockWrapper.identifier)) {
            throw new Error(`${this.blockWrapper.id} has not been mapped (${this.blockWrapper.identifier})`);
        }
        /** @type {?} */
        const componentFactory = this.resolver.resolveComponentFactory(this.blocks.getBlock(this.blockWrapper.identifier));
        /** @type {?} */
        const componentRef = this.viewContainerRef.createComponent(componentFactory);
        this.pages.sortBlocks(this.block);
        // @ts-ignore
        componentRef.instance.block = this.block;
        componentRef.changeDetectorRef.detectChanges();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscription = this.pages.getBlock(this.block.widgetId).then((/**
         * @param {?} block
         * @return {?}
         */
        (block) => {
            this.blockWrapper = block;
            this.loadComponent();
        }));
    }
}
BlockComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block',
                template: "<!-- BLOCK -->",
                changeDetection: ChangeDetectionStrategy.Default
            }] }
];
/** @nocollapse */
BlockComponent.ctorParameters = () => [
    { type: PageService },
    { type: BlockService },
    { type: ComponentFactoryResolver },
    { type: ViewContainerRef }
];
BlockComponent.propDecorators = {
    block: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockComponent.prototype.block;
    /** @type {?} */
    BlockComponent.prototype.blockWrapper;
    /** @type {?} */
    BlockComponent.prototype.subscription;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.pages;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.blocks;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.resolver;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.viewContainerRef;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/pipes/limitto.pipe.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TruncatePipe {
    /**
     * @param {?} value
     * @param {?} args
     * @return {?}
     */
    transform(value, args) {
        /** @type {?} */
        const limit = args || 10;
        /** @type {?} */
        const trail = '...';
        return value.length > limit ? value.substring(0, limit) + trail : value;
    }
}
TruncatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'limitTo'
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/page.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
const ɵ0$3 = { breadcrumbs: ['Page'] };
class PageModule {
}
PageModule.decorators = [
    { type: NgModule, args: [{
                exports: [
                    BlockComponent,
                ],
                declarations: [
                    PageComponent,
                    TruncatePipe,
                    BlockComponent,
                    BlockAbstractComponent,
                    BlockWrapperComponent,
                    BlockLanguageComponent,
                    BlockParagraphComponent,
                    BlockParagraphLeadComponent,
                    BlockTableComponent,
                    BlockAccordionComponent,
                    BlockH1Component,
                    BlockH2Component,
                    BlockH3Component,
                    BlockH4Component,
                    BlockH5Component,
                    BlockH6Component,
                    BlockImageComponent,
                    BlockBreadcrumbsComponent,
                    BlockCarouselComponent,
                    BlockVideoComponent,
                    BlockBulletsComponent,
                    BlockNumberedComponent,
                    BlockBlockquoteComponent,
                    BlockDownloadComponent,
                    BlockButtonComponent,
                    BlockDividerComponent,
                    BlockProgressComponent,
                    BlockTabsComponent,
                    BlockSearchResultsComponent,
                    BlockSearchComponent,
                    BlockCompileDirective,
                ],
                imports: [
                    CommonModule,
                    RouterModule.forChild([
                        {
                            path: ':identifier', component: PageComponent,
                            data: ɵ0$3
                        }
                    ]),
                    MatExpansionModule,
                    MatListModule,
                    MatProgressBarModule,
                    MatTabsModule,
                    SpinnerModule,
                    ReactiveFormsModule,
                    FormsModule
                ],
                entryComponents: [
                    BlockAbstractComponent,
                    BlockWrapperComponent,
                    BlockLanguageComponent,
                    BlockParagraphComponent,
                    BlockParagraphLeadComponent,
                    BlockTableComponent,
                    BlockAccordionComponent,
                    BlockH1Component,
                    BlockH2Component,
                    BlockH3Component,
                    BlockH4Component,
                    BlockH5Component,
                    BlockH6Component,
                    BlockImageComponent,
                    BlockBreadcrumbsComponent,
                    BlockCarouselComponent,
                    BlockVideoComponent,
                    BlockBulletsComponent,
                    BlockNumberedComponent,
                    BlockBlockquoteComponent,
                    BlockDownloadComponent,
                    BlockButtonComponent,
                    BlockDividerComponent,
                    BlockProgressComponent,
                    BlockTabsComponent,
                    BlockSearchResultsComponent,
                    BlockSearchComponent,
                ],
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/directives/stub-click.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class StubClickDirective {
    /**
     * @param {?} event
     * @return {?}
     */
    onMouseDown(event) {
        event.preventDefault();
    }
}
StubClickDirective.decorators = [
    { type: Directive, args: [{
                selector: '[coreStubClick]'
            },] }
];
StubClickDirective.propDecorators = {
    onMouseDown: [{ type: HostListener, args: ['click', ['$event'],] }]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/utils.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UtilsModule {
}
UtilsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [StubClickDirective],
                imports: [
                    CommonModule
                ],
                exports: [StubClickDirective]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/animations.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @return {?}
 */
function makeSlideInOut() {
    return trigger('slideInOut', [
        state('in', style({ height: '*', opacity: 0 })),
        transition(':leave', [
            style({ height: '*', opacity: 1 }),
            group([
                animate('200ms ease-in-out', style({ height: 0 })),
                animate('200ms ease-in-out', style({ opacity: '0' }))
            ])
        ]),
        transition(':enter', [
            style({ height: '0', opacity: 0 }),
            group([
                animate('200ms ease-in-out', style({ height: '*' })),
                animate('200ms ease-in-out', style({ opacity: '1' }))
            ])
        ])
    ]);
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/panels/panel/panel.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PanelComponent {
    /**
     * @param {?} headerClass
     * @param {?} dialogs
     * @param {?} el
     * @param {?} renderer
     */
    constructor(headerClass, dialogs, el, renderer) {
        this.dialogs = dialogs;
        this.el = el;
        this.renderer = renderer;
        this.hasPannel = false;
        this.collapsible = false;
        this.collapsed = false;
        this.fullscreenable = false;
        this.headerClass = headerClass;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (typeof this.collapsed !== 'undefined') {
            this.collapsible = true;
        }
        if (typeof this.fullscreenIn !== 'undefined') {
            this.fullscreenable = true;
        }
        if (typeof this.closed !== 'undefined') {
            this.clossable = true;
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        // console.log(22, changes);
        // if (typeof changes.fullscreenIn !== 'undefined') {
        //   console.log('111', changes.fullscreenIn.currentValue, this.fullscreenable);
        // }
    }
    /**
     * @return {?}
     */
    get pannelClasses() {
        /** @type {?} */
        const classes = ['panel'];
        classes.push(this.collapsed ? 'panel-collapsed' : '');
        classes.push(this.fullscreenIn ? 'panel-fullscreen' : '');
        return classes;
    }
    /**
     * @return {?}
     */
    get pannelContainerClasses() {
        /** @type {?} */
        const classes = ['panel-container'];
        if (this.collapsible) {
            // classes.push(this.collapsed ? 'collapse' : 'show');
            classes.push(this.collapsed ? '' : 'show');
        }
        return classes;
    }
    /**
     * @return {?}
     */
    get pannelContentClasses() {
        /** @type {?} */
        const classes = ['panel-content'];
        return classes;
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    toggleCollapse($event) {
        $event.preventDefault();
        this.collapsed = !this.collapsed;
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    toggleFullscreen($event) {
        $event.preventDefault();
        this.fullscreenIn = !this.fullscreenIn;
        handleClassCondition(this.fullscreenIn, 'panel-fullscreen', document.querySelector('body'));
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    closePanel($event) {
        return __awaiter(this, void 0, void 0, function* () {
            $event.preventDefault();
            /** @type {?} */
            const titleEl = this.el.nativeElement.querySelector('h1')
                || this.el.nativeElement.querySelector('h2')
                || this.el.nativeElement.querySelector('h3');
            /** @type {?} */
            const title = titleEl ? titleEl.innerText : '';
            /** @type {?} */
            const result = yield this.dialogs.confirm({
                title: `<i class='fal fa-times-circle text-danger mr-2'></i>
      Do you wish to delete panel <span class='fw-500'>&nbsp;'${title}'&nbsp;</span>?`,
                message: `<span><strong>Warning:</strong> This action cannot be undone!</span>`,
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-danger shadow-0'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-default'
                    }
                }
            }).toPromise();
            if (result) {
                this.renderer.addClass(this.el.nativeElement, 'd-none');
            }
        });
    }
    /**
     * @param {?} headerClass
     * @return {?}
     */
    setHeaderClass(headerClass) {
        this.headerClass = headerClass;
    }
}
PanelComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-panel',
                template: "<div class=\"panel\" [ngClass]=\"pannelClasses\">\r\n  <div class=\"panel-hdr {{ headerClass }}\">\r\n    <ng-content select=\"[panelTitle]\" #panelTitle></ng-content>\r\n    <ng-content select=\"[panelToolbar]\"> </ng-content>\r\n    <div class=\"panel-toolbar\">\r\n      <button\r\n        *ngIf=\"collapsible\"\r\n        class=\"btn btn-panel\"\r\n        data-action=\"panel-collapse\"\r\n        (click)=\"toggleCollapse($event)\"\r\n        tooltip=\"Collapse\"\r\n      ></button>\r\n      <button\r\n        class=\"btn btn-panel\"\r\n        *ngIf=\"fullscreenable\"\r\n        data-action=\"panel-fullscreen\"\r\n        (click)=\"toggleFullscreen($event)\"\r\n        tooltip=\"Fullscreen\"\r\n      ></button>\r\n      <button\r\n        class=\"btn btn-panel\"\r\n        *ngIf=\"clossable\"\r\n        data-action=\"panel-close\"\r\n        (click)=\"closePanel($event)\"\r\n        tooltip=\"Close\"\r\n      ></button>      \r\n    </div>    \r\n  </div>\r\n\r\n  <div class=\"panel-container\" [ngClass]=\"pannelContainerClasses\">\r\n    <div\r\n      class=\"panel-content\"\r\n      [ngClass]=\"pannelContentClasses\"\r\n      *ngIf=\"!collapsed\"\r\n      [@slideInOut]\r\n    >\r\n      <ng-content select=\"[panelContent]\"> </ng-content>\r\n    </div>\r\n\r\n    <ng-content select=\"[panelFooter]\"> </ng-content>\r\n  </div>\r\n</div>\r\n",
                animations: [makeSlideInOut()]
            }] }
];
/** @nocollapse */
PanelComponent.ctorParameters = () => [
    { type: String, decorators: [{ type: Attribute, args: ['headerClass',] }] },
    { type: DialogsService },
    { type: ElementRef },
    { type: Renderer2 }
];
PanelComponent.propDecorators = {
    collapsible: [{ type: Input }],
    collapsed: [{ type: Input }],
    fullscreenable: [{ type: Input }],
    fullscreenIn: [{ type: Input }],
    clossable: [{ type: Input }],
    closed: [{ type: Input }],
    panelTitle: [{ type: ContentChild, args: ['panelTitle', { static: true },] }]
};
if (false) {
    /** @type {?} */
    PanelComponent.prototype.hasPannel;
    /** @type {?} */
    PanelComponent.prototype.collapsible;
    /** @type {?} */
    PanelComponent.prototype.collapsed;
    /** @type {?} */
    PanelComponent.prototype.fullscreenable;
    /** @type {?} */
    PanelComponent.prototype.fullscreenIn;
    /** @type {?} */
    PanelComponent.prototype.clossable;
    /** @type {?} */
    PanelComponent.prototype.closed;
    /** @type {?} */
    PanelComponent.prototype.headerClass;
    /** @type {?} */
    PanelComponent.prototype.panelTitle;
    /**
     * @type {?}
     * @private
     */
    PanelComponent.prototype.dialogs;
    /**
     * @type {?}
     * @private
     */
    PanelComponent.prototype.el;
    /**
     * @type {?}
     * @private
     */
    PanelComponent.prototype.renderer;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/panels/panels.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PanelsModule {
}
PanelsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [PanelComponent],
                imports: [
                    TooltipModule$1,
                    CommonModule,
                    DialogsModule
                ],
                exports: [PanelComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/gclog/gclogAngulartics.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AngularticsGCLOG {
    /**
     * @param {?} angulartics2
     */
    constructor(angulartics2) {
        this.angulartics2 = angulartics2;
    }
    /**
     * @return {?}
     */
    startTracking() {
        this.angulartics2.pageTrack
            .pipe(this.angulartics2.filterDeveloperMode())
            .subscribe((/**
         * @param {?} x
         * @return {?}
         */
        (x) => this.pageTrack(x.path)));
        this.angulartics2.eventTrack
            .pipe(this.angulartics2.filterDeveloperMode())
            .subscribe((/**
         * @param {?} x
         * @return {?}
         */
        (x) => this.eventTrack(x.action, x.properties)));
        this.angulartics2.exceptionTrack
            .pipe(this.angulartics2.filterDeveloperMode())
            .subscribe((/**
         * @param {?} x
         * @return {?}
         */
        (x) => this.exceptionTrack(x)));
        this.angulartics2.setUsername
            .pipe(this.angulartics2.filterDeveloperMode())
            .subscribe((/**
         * @param {?} x
         * @return {?}
         */
        (x) => this.setUsername(x)));
    }
    /**
     * @param {?} path
     * @return {?}
     */
    pageTrack(path) {
        try {
            gclog.addPageAction(path);
        }
        catch (err) {
        }
    }
    /**
     * @param {?} action
     * @param {?} properties
     * @return {?}
     */
    eventTrack(action, properties) {
        try {
            gclog.addPageAction(action, properties);
        }
        catch (err) {
        }
    }
    /**
     * @param {?} properties
     * @return {?}
     */
    exceptionTrack(properties) {
        if (properties.fatal === undefined) {
            console.log('No "fatal" provided, sending with fatal=true');
            properties.exFatal = true;
        }
        properties.exDescription = properties.event ? properties.event.stack : properties.description;
        this.eventTrack(`Exception thrown for ${properties.appName} <${properties.appId}@${properties.appVersion}>`, {
            category: 'Exception',
            label: properties.exDescription,
        });
    }
    /**
     * @param {?} userId
     * @return {?}
     */
    setUsername(userId) {
        try {
            gclog.setCustomAttribute('username', userId);
        }
        catch (err) {
        }
    }
}
AngularticsGCLOG.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
AngularticsGCLOG.ctorParameters = () => [
    { type: Angulartics2 }
];
/** @nocollapse */ AngularticsGCLOG.ngInjectableDef = ɵɵdefineInjectable({ factory: function AngularticsGCLOG_Factory() { return new AngularticsGCLOG(ɵɵinject(Angulartics2)); }, token: AngularticsGCLOG, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AngularticsGCLOG.prototype.angulartics2;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/ui/on-off/on-off.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OnOffComponent {
    constructor() {
        this.checked = false;
        this.checkedChange = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (!changes.checked || changes.checked.currentValue === this.checked) {
            return;
        }
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onCheck($event) {
        $event.preventDefault();
        this.checked = !this.checked;
        this.checkedChange.emit(this.checked);
    }
}
OnOffComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-on-off',
                template: `
        <a
                href="#"
                (click)="onCheck($event)"
                class="btn btn-switch {{class}}"
                [class.active]="checked"></a>
    `,
                host: {
                    class: 'd-inline-block'
                },
                changeDetection: ChangeDetectionStrategy.OnPush
            }] }
];
OnOffComponent.propDecorators = {
    checked: [{ type: Input }],
    class: [{ type: Input }],
    checkedChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    OnOffComponent.prototype.checked;
    /** @type {?} */
    OnOffComponent.prototype.class;
    /** @type {?} */
    OnOffComponent.prototype.checkedChange;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/ui/ui.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UiModule {
}
UiModule.decorators = [
    { type: NgModule, args: [{
                declarations: [OnOffComponent],
                imports: [
                    CommonModule
                ],
                exports: [OnOffComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/navigation/navigation.selectors.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const selectNavigationState = createFeatureSelector('navigation');
const ɵ0$4 = /**
 * @param {?} state
 * @return {?}
 */
state => state.items;
/** @type {?} */
const selectNavigationItems = createSelector(selectNavigationState, (ɵ0$4));
const ɵ1$2 = /**
 * @param {?} state
 * @return {?}
 */
state => ({
    active: state.filterActive,
    text: state.filterText
});
/** @type {?} */
const selectFilter = createSelector(selectNavigationState, (ɵ1$2));
const ɵ2$2 = /**
 * @param {?} state
 * @return {?}
 */
state => ({
    active: state.filterActive && !!state.filterText.trim(),
    total: state.total,
    matched: state.matched
});
/** @type {?} */
const selectResult = createSelector(selectNavigationState, (ɵ2$2));

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/settings/settings.selectors.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const selectSettingsState = createFeatureSelector('settings');

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-frontend.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoreFrontendService {
    constructor() { }
}
CoreFrontendService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CoreFrontendService.ctorParameters = () => [];
/** @nocollapse */ CoreFrontendService.ngInjectableDef = ɵɵdefineInjectable({ factory: function CoreFrontendService_Factory() { return new CoreFrontendService(); }, token: CoreFrontendService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-frontend.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoreFrontendComponent {
    /**
     * @param {?} translate
     * @param {?} translateProvider
     * @param {?} router
     * @param {?} gtm
     * @param {?} gclog
     */
    constructor(translate, translateProvider, router, gtm, gclog) {
        this.translate = translate;
        this.translateProvider = translateProvider;
        this.router = router;
        this.gtm = gtm;
        this.gclog = gclog;
        this.title = 'core-frontend';
        gtm.startTracking();
        gclog.startTracking();
        translateProvider.setDefaultLang(translate.getDefaultLanguage());
        translate.getLanguages().subscribe((/**
         * @param {?} languages
         * @return {?}
         */
        languages => {
            translateProvider.addLangs(languages.map((/**
             * @param {?} language
             * @return {?}
             */
            language => {
                return language.code;
            })));
        }));
        this.router.events.subscribe((/**
         * @param {?} event
         * @return {?}
         */
        event => {
            if (event instanceof NavigationEnd) {
                try {
                    gtm.pageTrack(event.urlAfterRedirects);
                    gclog.pageTrack(event.urlAfterRedirects);
                }
                catch (err) {
                    console.log(err);
                }
            }
        }));
    }
}
CoreFrontendComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-root',
                template: '<router-outlet></router-outlet>'
            }] }
];
/** @nocollapse */
CoreFrontendComponent.ctorParameters = () => [
    { type: TranslateService },
    { type: TranslateService$1 },
    { type: Router },
    { type: Angulartics2GoogleTagManager },
    { type: AngularticsGCLOG }
];
if (false) {
    /** @type {?} */
    CoreFrontendComponent.prototype.title;
    /**
     * @type {?}
     * @private
     */
    CoreFrontendComponent.prototype.translate;
    /**
     * @type {?}
     * @private
     */
    CoreFrontendComponent.prototype.translateProvider;
    /** @type {?} */
    CoreFrontendComponent.prototype.router;
    /** @type {?} */
    CoreFrontendComponent.prototype.gtm;
    /** @type {?} */
    CoreFrontendComponent.prototype.gclog;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-routing.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
const ɵ0$5 = { breadcrumbs: ['Home'] };
/** @type {?} */
const routes = [
    {
        path: '',
        canActivate: [AuthService],
        children: [
            {
                path: '', component: PageComponent,
                data: ɵ0$5
            },
        ]
    },
];
class CoreRoutingModule {
}
CoreRoutingModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    RouterModule.forRoot(routes)
                ],
                exports: [RouterModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-frontend.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoreFrontendModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return {
            ngModule: CoreFrontendModule,
            providers: [
                CacheService,
                PageService,
                BlockService,
                TranslateService,
            ]
        };
    }
}
CoreFrontendModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    CoreFrontendComponent,
                ],
                imports: [
                    BrowserModule,
                    CoreModule,
                    AuthModule,
                    PanelsModule,
                    BrowserAnimationsModule,
                    MatExpansionModule,
                    TranslateModule,
                    PageModule,
                    SpinnerModule,
                    CoreRoutingModule,
                ],
                providers: [],
                bootstrap: [CoreFrontendComponent],
                exports: [
                    CoreModule,
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: gc-core-frontend.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { APP_CONFIG, AngularticsGCLOG, AuthModule, AuthService, Block, BlockAbstractComponent, BlockCompileDirective, BlockService, CacheService, CoreFrontendComponent, CoreFrontendModule, CoreFrontendService, CoreModule, MfaModalComponent, NavigationEffects, PageComponent, PageModule, PageService, PanelsModule, SearchService, SettingsActionTypes, SpinnerModule, SpinnerService, TranslateService, UiModule, UtilsModule, activeUrl, appReset, factoryReset, handleClassCondition, initialState$1 as initialState, makeSlideInOut, mobileNavigation, navigationFilter, reducer$1 as reducer, scrollToTop, selectFilter, selectNavigationItems, selectNavigationState, selectResult, selectSettingsState, setGlobalFontSize, settingsMetaReducer, throwIfAlreadyLoaded, toggleBiggerContentFont, toggleBoxedLayout, toggleCleanPageBackground, toggleDaltonism, toggleDisableCSSAnimation, toggleFixedHeader, toggleFixedNavigation, toggleFullscreen, toggleHideInfoCard, toggleHideNavigation, toggleHideNavigationIcons, toggleHierarchicalNavigation, toggleHighContrastText, toggleLeanSubheader, toggleMinifyNavigation, toggleNavSection, toggleNavigationFilter, toggleNoOverlay, toggleOffCanvas, togglePreloaderInsise, togglePushContent, toggleRtl, toggleTopNavigation, reducers as ɵa, localStorageSyncReducer as ɵb, BlockH2Component as ɵba, BlockH3Component as ɵbb, BlockH4Component as ɵbc, BlockH5Component as ɵbd, BlockH6Component as ɵbe, BlockImageComponent as ɵbf, BlockBreadcrumbsComponent as ɵbg, BlockCarouselComponent as ɵbh, BlockVideoComponent as ɵbi, BlockBulletsComponent as ɵbj, BlockNumberedComponent as ɵbk, BlockBlockquoteComponent as ɵbl, BlockDownloadComponent as ɵbm, BlockButtonComponent as ɵbn, BlockDividerComponent as ɵbo, BlockProgressComponent as ɵbp, BlockTabsComponent as ɵbq, BlockSearchResultsComponent as ɵbr, BlockSearchComponent as ɵbs, StubClickDirective as ɵbt, PanelComponent as ɵbu, OnOffComponent as ɵbv, CoreRoutingModule as ɵbw, metaReducers as ɵc, effects as ɵd, initialState as ɵe, reducer as ɵf, CustomSerializer as ɵg, environment as ɵh, DialogsModule as ɵi, ConfirmDialogComponent as ɵj, DialogsService as ɵk, APIInterceptor as ɵl, ApiService as ɵm, ParserService as ɵn, LoginComponent as ɵo, SystemMessageService as ɵp, SpinnerComponent as ɵq, BlockComponent as ɵr, TruncatePipe as ɵs, BlockWrapperComponent as ɵt, BlockLanguageComponent as ɵu, BlockParagraphComponent as ɵv, BlockParagraphLeadComponent as ɵw, BlockTableComponent as ɵx, BlockAccordionComponent as ɵy, BlockH1Component as ɵz };
//# sourceMappingURL=gc-core-frontend.js.map
