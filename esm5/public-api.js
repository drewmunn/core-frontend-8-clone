/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of core-frontend
 */
export { throwIfAlreadyLoaded, CoreModule } from './lib/core/core.module';
export { APP_CONFIG } from './lib/app.config';
export { AuthModule } from './lib/core/auth/auth.module';
export { AuthService } from './lib/core/auth/auth.service';
export { Block } from './lib/core/block/block-block';
export { BlockCompileDirective } from './lib/core/blocks/block-compile.directive';
export { BlockAbstractComponent } from './lib/core/blocks/block-abstract.component';
export { MfaModalComponent } from './lib/core/auth/mfa-modal/mfa-modal.component';
export { TranslateService } from './lib/core/pages/translate.service';
export { SearchService } from './lib/core/pages/search.service';
export { CacheService } from './lib/core/cache.service';
export { PageService } from './lib/core/pages/page.service';
export { BlockService } from './lib/core/pages/block.service';
export { PageModule } from './lib/core/pages/page.module';
export { PageComponent } from './lib/core/pages/page.component';
export { SpinnerModule } from './lib/core/utils/spinner/spinner.module';
export { SpinnerService } from './lib/core/utils/spinner/spinner.service';
export { UtilsModule } from './lib/core/utils/utils.module';
export { makeSlideInOut } from './lib/core/utils/animations';
export { PanelsModule } from './lib/core/utils/panels/panels.module';
export { AngularticsGCLOG } from './lib/core/gclog/gclogAngulartics';
export { UiModule } from './lib/core/utils/ui/ui.module';
export { scrollToTop, toggleFullscreen, handleClassCondition } from './lib/core/utils/utils.functions';
export { toggleNavSection, activeUrl, toggleNavigationFilter, navigationFilter, mobileNavigation } from './lib/core/navigation/navigation.actions';
export { NavigationEffects } from './lib/core/navigation/navigation.effects';
export { reducer, initialState } from './lib/core/navigation/navigation.reducer';
export { selectNavigationState, selectNavigationItems, selectFilter, selectResult } from './lib/core/navigation/navigation.selectors';
export { toggleFixedHeader, toggleFixedNavigation, toggleMinifyNavigation, toggleHideNavigation, toggleTopNavigation, toggleBoxedLayout, togglePushContent, toggleNoOverlay, toggleOffCanvas, toggleBiggerContentFont, toggleHighContrastText, toggleDaltonism, toggleRtl, togglePreloaderInsise, toggleCleanPageBackground, toggleHideNavigationIcons, toggleDisableCSSAnimation, toggleHideInfoCard, toggleLeanSubheader, toggleHierarchicalNavigation, setGlobalFontSize, appReset, factoryReset, SettingsActionTypes } from './lib/core/settings/settings.actions';
export { settingsMetaReducer } from './lib/core/settings/settings.meta';
export { selectSettingsState } from './lib/core/settings/settings.selectors';
export { CoreFrontendService } from './lib/core-frontend.service';
export { CoreFrontendComponent } from './lib/core-frontend.component';
export { CoreFrontendModule } from './lib/core-frontend.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsicHVibGljLWFwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBLGlEQUFjLHdCQUF3QixDQUFDO0FBQ3ZDLDJCQUFjLGtCQUFrQixDQUFDO0FBQ2pDLDJCQUFjLDZCQUE2QixDQUFDO0FBQzVDLDRCQUFjLDhCQUE4QixDQUFDO0FBQzdDLHNCQUFjLDhCQUE4QixDQUFDO0FBQzdDLHNDQUFjLDJDQUEyQyxDQUFDO0FBQzFELHVDQUFjLDRDQUE0QyxDQUFDO0FBRTNELGtDQUFjLCtDQUErQyxDQUFDO0FBRTlELGlDQUFjLG9DQUFvQyxDQUFDO0FBQ25ELDhCQUFjLGlDQUFpQyxDQUFDO0FBQ2hELDZCQUFjLDBCQUEwQixDQUFDO0FBQ3pDLDRCQUFjLCtCQUErQixDQUFDO0FBQzlDLDZCQUFjLGdDQUFnQyxDQUFDO0FBQy9DLDJCQUFjLDhCQUE4QixDQUFDO0FBQzdDLDhCQUFjLGlDQUFpQyxDQUFDO0FBQ2hELDhCQUFjLHlDQUF5QyxDQUFDO0FBQ3hELCtCQUFjLDBDQUEwQyxDQUFDO0FBQ3pELDRCQUFjLCtCQUErQixDQUFDO0FBQzlDLCtCQUFjLDZCQUE2QixDQUFDO0FBQzVDLDZCQUFjLHVDQUF1QyxDQUFDO0FBQ3RELGlDQUFjLG1DQUFtQyxDQUFDO0FBQ2xELHlCQUFjLCtCQUErQixDQUFDO0FBRTlDLG9FQUFjLGtDQUFrQyxDQUFDO0FBQ2pELHdHQUFjLDBDQUEwQyxDQUFDO0FBQ3pELGtDQUFjLDBDQUEwQyxDQUFDO0FBQ3pELHNDQUFjLDBDQUEwQyxDQUFDO0FBQ3pELHlGQUFjLDRDQUE0QyxDQUFDO0FBQzNELGdnQkFBYyxzQ0FBc0MsQ0FBQztBQUNyRCxvQ0FBYyxtQ0FBbUMsQ0FBQztBQUNsRCxvQ0FBYyx3Q0FBd0MsQ0FBQztBQUV2RCxvQ0FBYyw2QkFBNkIsQ0FBQztBQUM1QyxzQ0FBYywrQkFBK0IsQ0FBQztBQUM5QyxtQ0FBYyw0QkFBNEIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBQdWJsaWMgQVBJIFN1cmZhY2Ugb2YgY29yZS1mcm9udGVuZFxuICovXG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb3JlL2NvcmUubW9kdWxlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2FwcC5jb25maWcnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS9hdXRoL2F1dGgubW9kdWxlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvcmUvYXV0aC9hdXRoLnNlcnZpY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS9ibG9jay9ibG9jay1ibG9jayc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb3JlL2Jsb2Nrcy9ibG9jay1jb21waWxlLmRpcmVjdGl2ZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb3JlL2Jsb2Nrcy9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuXG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb3JlL2F1dGgvbWZhLW1vZGFsL21mYS1tb2RhbC5jb21wb25lbnQnO1xuXG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb3JlL3BhZ2VzL3RyYW5zbGF0ZS5zZXJ2aWNlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvcmUvcGFnZXMvc2VhcmNoLnNlcnZpY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS9jYWNoZS5zZXJ2aWNlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvcmUvcGFnZXMvcGFnZS5zZXJ2aWNlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvcmUvcGFnZXMvYmxvY2suc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb3JlL3BhZ2VzL3BhZ2UubW9kdWxlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvcmUvcGFnZXMvcGFnZS5jb21wb25lbnQnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS91dGlscy9zcGlubmVyL3NwaW5uZXIubW9kdWxlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvcmUvdXRpbHMvc3Bpbm5lci9zcGlubmVyLnNlcnZpY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS91dGlscy91dGlscy5tb2R1bGUnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS91dGlscy9hbmltYXRpb25zJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvcmUvdXRpbHMvcGFuZWxzL3BhbmVscy5tb2R1bGUnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS9nY2xvZy9nY2xvZ0FuZ3VsYXJ0aWNzJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvcmUvdXRpbHMvdWkvdWkubW9kdWxlJztcblxuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS91dGlscy91dGlscy5mdW5jdGlvbnMnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS9uYXZpZ2F0aW9uL25hdmlnYXRpb24uYWN0aW9ucyc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb3JlL25hdmlnYXRpb24vbmF2aWdhdGlvbi5lZmZlY3RzJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvcmUvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLnJlZHVjZXInO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS9uYXZpZ2F0aW9uL25hdmlnYXRpb24uc2VsZWN0b3JzJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NvcmUvc2V0dGluZ3Mvc2V0dGluZ3MuYWN0aW9ucyc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb3JlL3NldHRpbmdzL3NldHRpbmdzLm1ldGEnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS9zZXR0aW5ncy9zZXR0aW5ncy5zZWxlY3RvcnMnO1xuXG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb3JlLWZyb250ZW5kLnNlcnZpY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS1mcm9udGVuZC5jb21wb25lbnQnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvY29yZS1mcm9udGVuZC5tb2R1bGUnO1xuXG5cbiJdfQ==