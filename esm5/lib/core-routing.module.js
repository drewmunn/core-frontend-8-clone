/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-routing.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthService } from './core/auth/auth.service';
import { PageComponent } from './core/pages/page.component';
var ɵ0 = { breadcrumbs: ['Home'] };
/** @type {?} */
var routes = [
    {
        path: '',
        canActivate: [AuthService],
        children: [
            {
                path: '', component: PageComponent,
                data: ɵ0
            },
        ]
    },
];
var CoreRoutingModule = /** @class */ (function () {
    function CoreRoutingModule() {
    }
    CoreRoutingModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        RouterModule.forRoot(routes)
                    ],
                    exports: [RouterModule]
                },] }
    ];
    return CoreRoutingModule;
}());
export { CoreRoutingModule };
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1yb3V0aW5nLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUtcm91dGluZy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQVMsTUFBTSxpQkFBaUIsQ0FBQztBQUNyRCxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLDZCQUE2QixDQUFDO1NBVXBDLEVBQUMsV0FBVyxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQUM7O0lBUnZDLE1BQU0sR0FBVztJQUVuQjtRQUNJLElBQUksRUFBRSxFQUFFO1FBQ1IsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDO1FBQzFCLFFBQVEsRUFBRTtZQUNOO2dCQUNJLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGFBQWE7Z0JBQ2xDLElBQUksSUFBeUI7YUFDaEM7U0FDSjtLQUNKO0NBQ0o7QUFFRDtJQUFBO0lBT0EsQ0FBQzs7Z0JBUEEsUUFBUSxTQUFDO29CQUNOLE9BQU8sRUFBRTt3QkFDTCxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQztxQkFDL0I7b0JBQ0QsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO2lCQUMxQjs7SUFFRCx3QkFBQztDQUFBLEFBUEQsSUFPQztTQURZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtSb3V0ZXJNb2R1bGUsIFJvdXRlc30gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7QXV0aFNlcnZpY2V9IGZyb20gJy4vY29yZS9hdXRoL2F1dGguc2VydmljZSc7XG5pbXBvcnQge1BhZ2VDb21wb25lbnR9IGZyb20gJy4vY29yZS9wYWdlcy9wYWdlLmNvbXBvbmVudCc7XG5cbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xuXG4gICAge1xuICAgICAgICBwYXRoOiAnJyxcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtBdXRoU2VydmljZV0sXG4gICAgICAgIGNoaWxkcmVuOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcGF0aDogJycsIGNvbXBvbmVudDogUGFnZUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBkYXRhOiB7YnJlYWRjcnVtYnM6IFsnSG9tZSddfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgXVxuICAgIH0sXG5dO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtcbiAgICAgICAgUm91dGVyTW9kdWxlLmZvclJvb3Qocm91dGVzKVxuICAgIF0sXG4gICAgZXhwb3J0czogW1JvdXRlck1vZHVsZV1cbn0pXG5leHBvcnQgY2xhc3MgQ29yZVJvdXRpbmdNb2R1bGUge1xufVxuIl19