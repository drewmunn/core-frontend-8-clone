/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-frontend.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var CoreFrontendService = /** @class */ (function () {
    function CoreFrontendService() {
    }
    CoreFrontendService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    CoreFrontendService.ctorParameters = function () { return []; };
    /** @nocollapse */ CoreFrontendService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CoreFrontendService_Factory() { return new CoreFrontendService(); }, token: CoreFrontendService, providedIn: "root" });
    return CoreFrontendService;
}());
export { CoreFrontendService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1mcm9udGVuZC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS1mcm9udGVuZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFFM0M7SUFLRTtJQUFnQixDQUFDOztnQkFMbEIsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7Ozs7OEJBSkQ7Q0FRQyxBQU5ELElBTUM7U0FIWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIENvcmVGcm9udGVuZFNlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG59XG4iXX0=