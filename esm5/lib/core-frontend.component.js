/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-frontend.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Angulartics2GoogleTagManager } from 'angulartics2/gtm';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService as TranslateProvider } from '@ngx-translate/core';
import { TranslateService } from './core/pages/translate.service';
import { AngularticsGCLOG } from './core/gclog/gclogAngulartics';
var CoreFrontendComponent = /** @class */ (function () {
    function CoreFrontendComponent(translate, translateProvider, router, gtm, gclog) {
        this.translate = translate;
        this.translateProvider = translateProvider;
        this.router = router;
        this.gtm = gtm;
        this.gclog = gclog;
        this.title = 'core-frontend';
        gtm.startTracking();
        gclog.startTracking();
        translateProvider.setDefaultLang(translate.getDefaultLanguage());
        translate.getLanguages().subscribe((/**
         * @param {?} languages
         * @return {?}
         */
        function (languages) {
            translateProvider.addLangs(languages.map((/**
             * @param {?} language
             * @return {?}
             */
            function (language) {
                return language.code;
            })));
        }));
        this.router.events.subscribe((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            if (event instanceof NavigationEnd) {
                try {
                    gtm.pageTrack(event.urlAfterRedirects);
                    gclog.pageTrack(event.urlAfterRedirects);
                }
                catch (err) {
                    console.log(err);
                }
            }
        }));
    }
    CoreFrontendComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-root',
                    template: '<router-outlet></router-outlet>'
                }] }
    ];
    /** @nocollapse */
    CoreFrontendComponent.ctorParameters = function () { return [
        { type: TranslateService },
        { type: TranslateProvider },
        { type: Router },
        { type: Angulartics2GoogleTagManager },
        { type: AngularticsGCLOG }
    ]; };
    return CoreFrontendComponent;
}());
export { CoreFrontendComponent };
if (false) {
    /** @type {?} */
    CoreFrontendComponent.prototype.title;
    /**
     * @type {?}
     * @private
     */
    CoreFrontendComponent.prototype.translate;
    /**
     * @type {?}
     * @private
     */
    CoreFrontendComponent.prototype.translateProvider;
    /** @type {?} */
    CoreFrontendComponent.prototype.router;
    /** @type {?} */
    CoreFrontendComponent.prototype.gtm;
    /** @type {?} */
    CoreFrontendComponent.prototype.gclog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1mcm9udGVuZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlLWZyb250ZW5kLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDeEMsT0FBTyxFQUFDLDRCQUE0QixFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDOUQsT0FBTyxFQUFDLGFBQWEsRUFBRSxNQUFNLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUN0RCxPQUFPLEVBQUMsZ0JBQWdCLElBQUksaUJBQWlCLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUMxRSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQUUvRDtJQVFJLCtCQUNZLFNBQTJCLEVBQzNCLGlCQUFvQyxFQUNyQyxNQUFjLEVBQ2QsR0FBaUMsRUFDakMsS0FBdUI7UUFKdEIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0Isc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNyQyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsUUFBRyxHQUFILEdBQUcsQ0FBOEI7UUFDakMsVUFBSyxHQUFMLEtBQUssQ0FBa0I7UUFQbEMsVUFBSyxHQUFHLGVBQWUsQ0FBQztRQVNwQixHQUFHLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDcEIsS0FBSyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRXRCLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDO1FBQ2pFLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxTQUFTO1lBQ3hDLGlCQUFpQixDQUFDLFFBQVEsQ0FDdEIsU0FBUyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFBLFFBQVE7Z0JBQ2xCLE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQztZQUN6QixDQUFDLEVBQUMsQ0FDTCxDQUFDO1FBQ04sQ0FBQyxFQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxLQUFLO1lBQzlCLElBQUksS0FBSyxZQUFZLGFBQWEsRUFBRTtnQkFDaEMsSUFBSTtvQkFDQSxHQUFHLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO29CQUN2QyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2lCQUM1QztnQkFBQyxPQUFPLEdBQUcsRUFBRTtvQkFDVixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUNwQjthQUNKO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFFUCxDQUFDOztnQkF0Q0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxXQUFXO29CQUNyQixRQUFRLEVBQUUsaUNBQWlDO2lCQUM5Qzs7OztnQkFOTyxnQkFBZ0I7Z0JBREksaUJBQWlCO2dCQUR0QixNQUFNO2dCQURyQiw0QkFBNEI7Z0JBSTVCLGdCQUFnQjs7SUEwQ3hCLDRCQUFDO0NBQUEsQUF4Q0QsSUF3Q0M7U0FuQ1kscUJBQXFCOzs7SUFDOUIsc0NBQXdCOzs7OztJQUdwQiwwQ0FBbUM7Ozs7O0lBQ25DLGtEQUE0Qzs7SUFDNUMsdUNBQXFCOztJQUNyQixvQ0FBd0M7O0lBQ3hDLHNDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QW5ndWxhcnRpY3MyR29vZ2xlVGFnTWFuYWdlcn0gZnJvbSAnYW5ndWxhcnRpY3MyL2d0bSc7XG5pbXBvcnQge05hdmlnYXRpb25FbmQsIFJvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7VHJhbnNsYXRlU2VydmljZSBhcyBUcmFuc2xhdGVQcm92aWRlcn0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQge1RyYW5zbGF0ZVNlcnZpY2V9IGZyb20gJy4vY29yZS9wYWdlcy90cmFuc2xhdGUuc2VydmljZSc7XG5pbXBvcnQge0FuZ3VsYXJ0aWNzR0NMT0d9IGZyb20gJy4vY29yZS9nY2xvZy9nY2xvZ0FuZ3VsYXJ0aWNzJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdjb3JlLXJvb3QnLFxuICAgIHRlbXBsYXRlOiAnPHJvdXRlci1vdXRsZXQ+PC9yb3V0ZXItb3V0bGV0Pidcbn0pXG5cbmV4cG9ydCBjbGFzcyBDb3JlRnJvbnRlbmRDb21wb25lbnQge1xuICAgIHRpdGxlID0gJ2NvcmUtZnJvbnRlbmQnO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIHRyYW5zbGF0ZVByb3ZpZGVyOiBUcmFuc2xhdGVQcm92aWRlcixcbiAgICAgICAgcHVibGljIHJvdXRlcjogUm91dGVyLFxuICAgICAgICBwdWJsaWMgZ3RtOiBBbmd1bGFydGljczJHb29nbGVUYWdNYW5hZ2VyLFxuICAgICAgICBwdWJsaWMgZ2Nsb2c6IEFuZ3VsYXJ0aWNzR0NMT0csXG4gICAgKSB7XG4gICAgICAgIGd0bS5zdGFydFRyYWNraW5nKCk7XG4gICAgICAgIGdjbG9nLnN0YXJ0VHJhY2tpbmcoKTtcblxuICAgICAgICB0cmFuc2xhdGVQcm92aWRlci5zZXREZWZhdWx0TGFuZyh0cmFuc2xhdGUuZ2V0RGVmYXVsdExhbmd1YWdlKCkpO1xuICAgICAgICB0cmFuc2xhdGUuZ2V0TGFuZ3VhZ2VzKCkuc3Vic2NyaWJlKGxhbmd1YWdlcyA9PiB7XG4gICAgICAgICAgICB0cmFuc2xhdGVQcm92aWRlci5hZGRMYW5ncyhcbiAgICAgICAgICAgICAgICBsYW5ndWFnZXMubWFwKGxhbmd1YWdlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGxhbmd1YWdlLmNvZGU7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMucm91dGVyLmV2ZW50cy5zdWJzY3JpYmUoZXZlbnQgPT4ge1xuICAgICAgICAgICAgaWYgKGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCkge1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIGd0bS5wYWdlVHJhY2soZXZlbnQudXJsQWZ0ZXJSZWRpcmVjdHMpO1xuICAgICAgICAgICAgICAgICAgICBnY2xvZy5wYWdlVHJhY2soZXZlbnQudXJsQWZ0ZXJSZWRpcmVjdHMpO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICB9XG5cbn1cbiJdfQ==