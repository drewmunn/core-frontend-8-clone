/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/panels/panels.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './panel/panel.component';
import { TooltipModule } from 'ngx-bootstrap';
import { DialogsModule } from '../dialogs/dialogs.module';
var PanelsModule = /** @class */ (function () {
    function PanelsModule() {
    }
    PanelsModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [PanelComponent],
                    imports: [
                        TooltipModule,
                        CommonModule,
                        DialogsModule
                    ],
                    exports: [PanelComponent]
                },] }
    ];
    return PanelsModule;
}());
export { PanelsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWxzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvdXRpbHMvcGFuZWxzL3BhbmVscy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFMUQ7SUFBQTtJQVM0QixDQUFDOztnQkFUNUIsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDLGNBQWMsQ0FBQztvQkFDOUIsT0FBTyxFQUFFO3dCQUNQLGFBQWE7d0JBQ2IsWUFBWTt3QkFDWixhQUFhO3FCQUNkO29CQUNELE9BQU8sRUFBRSxDQUFDLGNBQWMsQ0FBQztpQkFDMUI7O0lBQzJCLG1CQUFDO0NBQUEsQUFUN0IsSUFTNkI7U0FBaEIsWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFBhbmVsQ29tcG9uZW50IH0gZnJvbSAnLi9wYW5lbC9wYW5lbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUb29sdGlwTW9kdWxlIH0gZnJvbSAnbmd4LWJvb3RzdHJhcCc7XHJcbmltcG9ydCB7IERpYWxvZ3NNb2R1bGUgfSBmcm9tICcuLi9kaWFsb2dzL2RpYWxvZ3MubW9kdWxlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbUGFuZWxDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIFRvb2x0aXBNb2R1bGUsXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBEaWFsb2dzTW9kdWxlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbUGFuZWxDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQYW5lbHNNb2R1bGUgeyB9XHJcbiJdfQ==