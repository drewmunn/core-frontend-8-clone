/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/panels/panel/panel.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Attribute, Component, ContentChild, ElementRef, Input, Renderer2 } from '@angular/core';
import { makeSlideInOut } from '../../animations';
import { handleClassCondition } from '../../utils.functions';
import { DialogsService } from '../../dialogs/dialogs.service';
var PanelComponent = /** @class */ (function () {
    function PanelComponent(headerClass, dialogs, el, renderer) {
        this.dialogs = dialogs;
        this.el = el;
        this.renderer = renderer;
        this.hasPannel = false;
        this.collapsible = false;
        this.collapsed = false;
        this.fullscreenable = false;
        this.headerClass = headerClass;
    }
    /**
     * @return {?}
     */
    PanelComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (typeof this.collapsed !== 'undefined') {
            this.collapsible = true;
        }
        if (typeof this.fullscreenIn !== 'undefined') {
            this.fullscreenable = true;
        }
        if (typeof this.closed !== 'undefined') {
            this.clossable = true;
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    PanelComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        // console.log(22, changes);
        // if (typeof changes.fullscreenIn !== 'undefined') {
        //   console.log('111', changes.fullscreenIn.currentValue, this.fullscreenable);
        // }
    };
    Object.defineProperty(PanelComponent.prototype, "pannelClasses", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var classes = ['panel'];
            classes.push(this.collapsed ? 'panel-collapsed' : '');
            classes.push(this.fullscreenIn ? 'panel-fullscreen' : '');
            return classes;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PanelComponent.prototype, "pannelContainerClasses", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var classes = ['panel-container'];
            if (this.collapsible) {
                // classes.push(this.collapsed ? 'collapse' : 'show');
                classes.push(this.collapsed ? '' : 'show');
            }
            return classes;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PanelComponent.prototype, "pannelContentClasses", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var classes = ['panel-content'];
            return classes;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} $event
     * @return {?}
     */
    PanelComponent.prototype.toggleCollapse = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        $event.preventDefault();
        this.collapsed = !this.collapsed;
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    PanelComponent.prototype.toggleFullscreen = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        $event.preventDefault();
        this.fullscreenIn = !this.fullscreenIn;
        handleClassCondition(this.fullscreenIn, 'panel-fullscreen', document.querySelector('body'));
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    PanelComponent.prototype.closePanel = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var titleEl, title, result;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        $event.preventDefault();
                        titleEl = this.el.nativeElement.querySelector('h1')
                            || this.el.nativeElement.querySelector('h2')
                            || this.el.nativeElement.querySelector('h3');
                        title = titleEl ? titleEl.innerText : '';
                        return [4 /*yield*/, this.dialogs.confirm({
                                title: "<i class='fal fa-times-circle text-danger mr-2'></i>\n      Do you wish to delete panel <span class='fw-500'>&nbsp;'" + title + "'&nbsp;</span>?",
                                message: "<span><strong>Warning:</strong> This action cannot be undone!</span>",
                                buttons: {
                                    confirm: {
                                        label: 'Yes',
                                        className: 'btn-danger shadow-0'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-default'
                                    }
                                }
                            }).toPromise()];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            this.renderer.addClass(this.el.nativeElement, 'd-none');
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} headerClass
     * @return {?}
     */
    PanelComponent.prototype.setHeaderClass = /**
     * @param {?} headerClass
     * @return {?}
     */
    function (headerClass) {
        this.headerClass = headerClass;
    };
    PanelComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-panel',
                    template: "<div class=\"panel\" [ngClass]=\"pannelClasses\">\r\n  <div class=\"panel-hdr {{ headerClass }}\">\r\n    <ng-content select=\"[panelTitle]\" #panelTitle></ng-content>\r\n    <ng-content select=\"[panelToolbar]\"> </ng-content>\r\n    <div class=\"panel-toolbar\">\r\n      <button\r\n        *ngIf=\"collapsible\"\r\n        class=\"btn btn-panel\"\r\n        data-action=\"panel-collapse\"\r\n        (click)=\"toggleCollapse($event)\"\r\n        tooltip=\"Collapse\"\r\n      ></button>\r\n      <button\r\n        class=\"btn btn-panel\"\r\n        *ngIf=\"fullscreenable\"\r\n        data-action=\"panel-fullscreen\"\r\n        (click)=\"toggleFullscreen($event)\"\r\n        tooltip=\"Fullscreen\"\r\n      ></button>\r\n      <button\r\n        class=\"btn btn-panel\"\r\n        *ngIf=\"clossable\"\r\n        data-action=\"panel-close\"\r\n        (click)=\"closePanel($event)\"\r\n        tooltip=\"Close\"\r\n      ></button>      \r\n    </div>    \r\n  </div>\r\n\r\n  <div class=\"panel-container\" [ngClass]=\"pannelContainerClasses\">\r\n    <div\r\n      class=\"panel-content\"\r\n      [ngClass]=\"pannelContentClasses\"\r\n      *ngIf=\"!collapsed\"\r\n      [@slideInOut]\r\n    >\r\n      <ng-content select=\"[panelContent]\"> </ng-content>\r\n    </div>\r\n\r\n    <ng-content select=\"[panelFooter]\"> </ng-content>\r\n  </div>\r\n</div>\r\n",
                    animations: [makeSlideInOut()]
                }] }
    ];
    /** @nocollapse */
    PanelComponent.ctorParameters = function () { return [
        { type: String, decorators: [{ type: Attribute, args: ['headerClass',] }] },
        { type: DialogsService },
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    PanelComponent.propDecorators = {
        collapsible: [{ type: Input }],
        collapsed: [{ type: Input }],
        fullscreenable: [{ type: Input }],
        fullscreenIn: [{ type: Input }],
        clossable: [{ type: Input }],
        closed: [{ type: Input }],
        panelTitle: [{ type: ContentChild, args: ['panelTitle', { static: true },] }]
    };
    return PanelComponent;
}());
export { PanelComponent };
if (false) {
    /** @type {?} */
    PanelComponent.prototype.hasPannel;
    /** @type {?} */
    PanelComponent.prototype.collapsible;
    /** @type {?} */
    PanelComponent.prototype.collapsed;
    /** @type {?} */
    PanelComponent.prototype.fullscreenable;
    /** @type {?} */
    PanelComponent.prototype.fullscreenIn;
    /** @type {?} */
    PanelComponent.prototype.clossable;
    /** @type {?} */
    PanelComponent.prototype.closed;
    /** @type {?} */
    PanelComponent.prototype.headerClass;
    /** @type {?} */
    PanelComponent.prototype.panelTitle;
    /**
     * @type {?}
     * @private
     */
    PanelComponent.prototype.dialogs;
    /**
     * @type {?}
     * @private
     */
    PanelComponent.prototype.el;
    /**
     * @type {?}
     * @private
     */
    PanelComponent.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy9wYW5lbHMvcGFuZWwvcGFuZWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFDSCxTQUFTLEVBQ1QsU0FBUyxFQUNULFlBQVksRUFDWixVQUFVLEVBQ1YsS0FBSyxFQUdMLFNBQVMsRUFFWixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDaEQsT0FBTyxFQUFDLG9CQUFvQixFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDM0QsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLCtCQUErQixDQUFDO0FBRTdEO0lBa0JJLHdCQUM4QixXQUFtQixFQUNyQyxPQUF1QixFQUN2QixFQUFjLEVBQ2QsUUFBbUI7UUFGbkIsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFDdkIsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQVc7UUFmL0IsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNULGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFjNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7SUFDbkMsQ0FBQzs7OztJQUVELGlDQUFROzs7SUFBUjtRQUNJLElBQUksT0FBTyxJQUFJLENBQUMsU0FBUyxLQUFLLFdBQVcsRUFBRTtZQUN2QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztTQUMzQjtRQUNELElBQUksT0FBTyxJQUFJLENBQUMsWUFBWSxLQUFLLFdBQVcsRUFBRTtZQUMxQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM5QjtRQUNELElBQUksT0FBTyxJQUFJLENBQUMsTUFBTSxLQUFLLFdBQVcsRUFBRTtZQUNwQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztTQUN6QjtJQUNMLENBQUM7Ozs7O0lBRUQsb0NBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQzlCLDRCQUE0QjtRQUM1QixxREFBcUQ7UUFDckQsZ0ZBQWdGO1FBQ2hGLElBQUk7SUFDUixDQUFDO0lBRUQsc0JBQUkseUNBQWE7Ozs7UUFBakI7O2dCQUNVLE9BQU8sR0FBRyxDQUFDLE9BQU8sQ0FBQztZQUN6QixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN0RCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMxRCxPQUFPLE9BQU8sQ0FBQztRQUNuQixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGtEQUFzQjs7OztRQUExQjs7Z0JBQ1UsT0FBTyxHQUFHLENBQUMsaUJBQWlCLENBQUM7WUFDbkMsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNsQixzREFBc0Q7Z0JBQ3RELE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUM5QztZQUNELE9BQU8sT0FBTyxDQUFDO1FBQ25CLENBQUM7OztPQUFBO0lBRUQsc0JBQUksZ0RBQW9COzs7O1FBQXhCOztnQkFDVSxPQUFPLEdBQUcsQ0FBQyxlQUFlLENBQUM7WUFDakMsT0FBTyxPQUFPLENBQUM7UUFDbkIsQ0FBQzs7O09BQUE7Ozs7O0lBRUQsdUNBQWM7Ozs7SUFBZCxVQUFlLE1BQWtCO1FBQzdCLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUNyQyxDQUFDOzs7OztJQUVELHlDQUFnQjs7OztJQUFoQixVQUFpQixNQUFrQjtRQUMvQixNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDdkMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxrQkFBa0IsRUFBRSxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDaEcsQ0FBQzs7Ozs7SUFFSyxtQ0FBVTs7OztJQUFoQixVQUFpQixNQUFrQjs7Ozs7O3dCQUMvQixNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7d0JBQ2xCLE9BQU8sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDOytCQUNsRCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDOytCQUN6QyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO3dCQUUxQyxLQUFLLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUUvQixxQkFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQztnQ0FDdEMsS0FBSyxFQUFFLHlIQUM2QyxLQUFLLG9CQUFpQjtnQ0FDMUUsT0FBTyxFQUFFLHNFQUFzRTtnQ0FDL0UsT0FBTyxFQUFFO29DQUNMLE9BQU8sRUFBRTt3Q0FDTCxLQUFLLEVBQUUsS0FBSzt3Q0FDWixTQUFTLEVBQUUscUJBQXFCO3FDQUNuQztvQ0FDRCxNQUFNLEVBQUU7d0NBQ0osS0FBSyxFQUFFLElBQUk7d0NBQ1gsU0FBUyxFQUFFLGFBQWE7cUNBQzNCO2lDQUNKOzZCQUNKLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBZFIsTUFBTSxHQUFHLFNBY0Q7d0JBRWQsSUFBSSxNQUFNLEVBQUU7NEJBQ1IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsUUFBUSxDQUFDLENBQUM7eUJBQzNEOzs7OztLQUVKOzs7OztJQUVELHVDQUFjOzs7O0lBQWQsVUFBZSxXQUFtQjtRQUM5QixJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztJQUNuQyxDQUFDOztnQkE5R0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxZQUFZO29CQUN0QixrMkNBQXFDO29CQUNyQyxVQUFVLEVBQUUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztpQkFDakM7Ozs7NkNBZVEsU0FBUyxTQUFDLGFBQWE7Z0JBckJ4QixjQUFjO2dCQVRsQixVQUFVO2dCQUlWLFNBQVM7Ozs4QkFlUixLQUFLOzRCQUNMLEtBQUs7aUNBQ0wsS0FBSzsrQkFDTCxLQUFLOzRCQUNMLEtBQUs7eUJBQ0wsS0FBSzs2QkFHTCxZQUFZLFNBQUMsWUFBWSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQzs7SUFnRzlDLHFCQUFDO0NBQUEsQUFoSEQsSUFnSEM7U0EzR1ksY0FBYzs7O0lBRXZCLG1DQUFrQjs7SUFDbEIscUNBQTZCOztJQUM3QixtQ0FBMkI7O0lBQzNCLHdDQUFnQzs7SUFDaEMsc0NBQStCOztJQUMvQixtQ0FBNEI7O0lBQzVCLGdDQUF5Qjs7SUFDekIscUNBQW9COztJQUVwQixvQ0FBdUQ7Ozs7O0lBSW5ELGlDQUErQjs7Ozs7SUFDL0IsNEJBQXNCOzs7OztJQUN0QixrQ0FBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgQXR0cmlidXRlLFxyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgQ29udGVudENoaWxkLFxyXG4gICAgRWxlbWVudFJlZixcclxuICAgIElucHV0LFxyXG4gICAgT25DaGFuZ2VzLFxyXG4gICAgT25Jbml0LFxyXG4gICAgUmVuZGVyZXIyLFxyXG4gICAgU2ltcGxlQ2hhbmdlc1xyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge21ha2VTbGlkZUluT3V0fSBmcm9tICcuLi8uLi9hbmltYXRpb25zJztcclxuaW1wb3J0IHtoYW5kbGVDbGFzc0NvbmRpdGlvbn0gZnJvbSAnLi4vLi4vdXRpbHMuZnVuY3Rpb25zJztcclxuaW1wb3J0IHtEaWFsb2dzU2VydmljZX0gZnJvbSAnLi4vLi4vZGlhbG9ncy9kaWFsb2dzLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2NvcmUtcGFuZWwnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3BhbmVsLmNvbXBvbmVudC5odG1sJyxcclxuICAgIGFuaW1hdGlvbnM6IFttYWtlU2xpZGVJbk91dCgpXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIFBhbmVsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG5cclxuICAgIGhhc1Bhbm5lbCA9IGZhbHNlO1xyXG4gICAgQElucHV0KCkgY29sbGFwc2libGUgPSBmYWxzZTtcclxuICAgIEBJbnB1dCgpIGNvbGxhcHNlZCA9IGZhbHNlO1xyXG4gICAgQElucHV0KCkgZnVsbHNjcmVlbmFibGUgPSBmYWxzZTtcclxuICAgIEBJbnB1dCgpIGZ1bGxzY3JlZW5JbjogYm9vbGVhbjtcclxuICAgIEBJbnB1dCgpIGNsb3NzYWJsZTogYm9vbGVhbjtcclxuICAgIEBJbnB1dCgpIGNsb3NlZDogYm9vbGVhbjtcclxuICAgIGhlYWRlckNsYXNzOiBzdHJpbmc7XHJcblxyXG4gICAgQENvbnRlbnRDaGlsZCgncGFuZWxUaXRsZScsIHtzdGF0aWM6IHRydWV9KSBwYW5lbFRpdGxlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIEBBdHRyaWJ1dGUoJ2hlYWRlckNsYXNzJykgaGVhZGVyQ2xhc3M6IHN0cmluZyxcclxuICAgICAgICBwcml2YXRlIGRpYWxvZ3M6IERpYWxvZ3NTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgZWw6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyLFxyXG4gICAgKSB7XHJcbiAgICAgICAgdGhpcy5oZWFkZXJDbGFzcyA9IGhlYWRlckNsYXNzO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0eXBlb2YgdGhpcy5jb2xsYXBzZWQgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29sbGFwc2libGUgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodHlwZW9mIHRoaXMuZnVsbHNjcmVlbkluICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICB0aGlzLmZ1bGxzY3JlZW5hYmxlID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHR5cGVvZiB0aGlzLmNsb3NlZCAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgdGhpcy5jbG9zc2FibGUgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coMjIsIGNoYW5nZXMpO1xyXG4gICAgICAgIC8vIGlmICh0eXBlb2YgY2hhbmdlcy5mdWxsc2NyZWVuSW4gIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgLy8gICBjb25zb2xlLmxvZygnMTExJywgY2hhbmdlcy5mdWxsc2NyZWVuSW4uY3VycmVudFZhbHVlLCB0aGlzLmZ1bGxzY3JlZW5hYmxlKTtcclxuICAgICAgICAvLyB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHBhbm5lbENsYXNzZXMoKSB7XHJcbiAgICAgICAgY29uc3QgY2xhc3NlcyA9IFsncGFuZWwnXTtcclxuICAgICAgICBjbGFzc2VzLnB1c2godGhpcy5jb2xsYXBzZWQgPyAncGFuZWwtY29sbGFwc2VkJyA6ICcnKTtcclxuICAgICAgICBjbGFzc2VzLnB1c2godGhpcy5mdWxsc2NyZWVuSW4gPyAncGFuZWwtZnVsbHNjcmVlbicgOiAnJyk7XHJcbiAgICAgICAgcmV0dXJuIGNsYXNzZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHBhbm5lbENvbnRhaW5lckNsYXNzZXMoKSB7XHJcbiAgICAgICAgY29uc3QgY2xhc3NlcyA9IFsncGFuZWwtY29udGFpbmVyJ107XHJcbiAgICAgICAgaWYgKHRoaXMuY29sbGFwc2libGUpIHtcclxuICAgICAgICAgICAgLy8gY2xhc3Nlcy5wdXNoKHRoaXMuY29sbGFwc2VkID8gJ2NvbGxhcHNlJyA6ICdzaG93Jyk7XHJcbiAgICAgICAgICAgIGNsYXNzZXMucHVzaCh0aGlzLmNvbGxhcHNlZCA/ICcnIDogJ3Nob3cnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGNsYXNzZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHBhbm5lbENvbnRlbnRDbGFzc2VzKCkge1xyXG4gICAgICAgIGNvbnN0IGNsYXNzZXMgPSBbJ3BhbmVsLWNvbnRlbnQnXTtcclxuICAgICAgICByZXR1cm4gY2xhc3NlcztcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVDb2xsYXBzZSgkZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB0aGlzLmNvbGxhcHNlZCA9ICF0aGlzLmNvbGxhcHNlZDtcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVGdWxsc2NyZWVuKCRldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHRoaXMuZnVsbHNjcmVlbkluID0gIXRoaXMuZnVsbHNjcmVlbkluO1xyXG4gICAgICAgIGhhbmRsZUNsYXNzQ29uZGl0aW9uKHRoaXMuZnVsbHNjcmVlbkluLCAncGFuZWwtZnVsbHNjcmVlbicsIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2JvZHknKSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgY2xvc2VQYW5lbCgkZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBjb25zdCB0aXRsZUVsID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2gxJylcclxuICAgICAgICAgICAgfHwgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2gyJylcclxuICAgICAgICAgICAgfHwgdGhpcy5lbC5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2gzJyk7XHJcblxyXG4gICAgICAgIGNvbnN0IHRpdGxlID0gdGl0bGVFbCA/IHRpdGxlRWwuaW5uZXJUZXh0IDogJyc7XHJcblxyXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IHRoaXMuZGlhbG9ncy5jb25maXJtKHtcclxuICAgICAgICAgICAgdGl0bGU6IGA8aSBjbGFzcz0nZmFsIGZhLXRpbWVzLWNpcmNsZSB0ZXh0LWRhbmdlciBtci0yJz48L2k+XHJcbiAgICAgIERvIHlvdSB3aXNoIHRvIGRlbGV0ZSBwYW5lbCA8c3BhbiBjbGFzcz0nZnctNTAwJz4mbmJzcDsnJHt0aXRsZX0nJm5ic3A7PC9zcGFuPj9gLFxyXG4gICAgICAgICAgICBtZXNzYWdlOiBgPHNwYW4+PHN0cm9uZz5XYXJuaW5nOjwvc3Ryb25nPiBUaGlzIGFjdGlvbiBjYW5ub3QgYmUgdW5kb25lITwvc3Bhbj5gLFxyXG4gICAgICAgICAgICBidXR0b25zOiB7XHJcbiAgICAgICAgICAgICAgICBjb25maXJtOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw6ICdZZXMnLFxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ2J0bi1kYW5nZXIgc2hhZG93LTAnXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY2FuY2VsOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw6ICdObycsXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnYnRuLWRlZmF1bHQnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS50b1Byb21pc2UoKTtcclxuXHJcbiAgICAgICAgaWYgKHJlc3VsdCkge1xyXG4gICAgICAgICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKHRoaXMuZWwubmF0aXZlRWxlbWVudCwgJ2Qtbm9uZScpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgc2V0SGVhZGVyQ2xhc3MoaGVhZGVyQ2xhc3M6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuaGVhZGVyQ2xhc3MgPSBoZWFkZXJDbGFzcztcclxuICAgIH1cclxuXHJcbn1cclxuIl19