/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/directives/stub-click.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener } from '@angular/core';
var StubClickDirective = /** @class */ (function () {
    function StubClickDirective() {
    }
    /**
     * @param {?} event
     * @return {?}
     */
    StubClickDirective.prototype.onMouseDown = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        event.preventDefault();
    };
    StubClickDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[coreStubClick]'
                },] }
    ];
    StubClickDirective.propDecorators = {
        onMouseDown: [{ type: HostListener, args: ['click', ['$event'],] }]
    };
    return StubClickDirective;
}());
export { StubClickDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R1Yi1jbGljay5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3V0aWxzL2RpcmVjdGl2ZXMvc3R1Yi1jbGljay5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV4RDtJQUFBO0lBU0EsQ0FBQzs7Ozs7SUFIQyx3Q0FBVzs7OztJQURYLFVBQ1ksS0FBaUI7UUFDM0IsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3pCLENBQUM7O2dCQVJGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsaUJBQWlCO2lCQUM1Qjs7OzhCQUdFLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7O0lBSW5DLHlCQUFDO0NBQUEsQUFURCxJQVNDO1NBTlksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBIb3N0TGlzdGVuZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gIHNlbGVjdG9yOiAnW2NvcmVTdHViQ2xpY2tdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU3R1YkNsaWNrRGlyZWN0aXZlIHtcclxuXHJcbiAgQEhvc3RMaXN0ZW5lcignY2xpY2snLCBbJyRldmVudCddKVxyXG4gIG9uTW91c2VEb3duKGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gIH1cclxufVxyXG4iXX0=