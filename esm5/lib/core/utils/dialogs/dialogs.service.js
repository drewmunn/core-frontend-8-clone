/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/dialogs/dialogs.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, RendererFactory2 } from '@angular/core';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { BsModalService } from 'ngx-bootstrap';
var DialogsService = /** @class */ (function () {
    function DialogsService(rendererFactory, modalService) {
        this.modalService = modalService;
        this.renderer = rendererFactory.createRenderer(null, null);
    }
    /**
     * @param {?} initialState
     * @return {?}
     */
    DialogsService.prototype.confirm = /**
     * @param {?} initialState
     * @return {?}
     */
    function (initialState) {
        this.playSound('messagebox');
        this.bsModalRef = this.modalService.show(ConfirmDialogComponent, {
            initialState: initialState,
            backdrop: 'static',
            keyboard: false,
            class: 'modal-dialog-centered'
        });
        this.renderer.addClass(document.querySelector('.modal'), 'modal-alert');
        return (/** @type {?} */ (this.bsModalRef.content.onClose));
    };
    /**
     * @param {?} sound
     * @param {?=} path
     * @return {?}
     */
    DialogsService.prototype.playSound = /**
     * @param {?} sound
     * @param {?=} path
     * @return {?}
     */
    function (sound, path) {
        if (path === void 0) { path = 'assets/media/sound'; }
        /** @type {?} */
        var audioElement = document.createElement('audio');
        if (navigator.userAgent.match('Firefox/')) {
            audioElement.setAttribute('src', path + '/' + sound + '.ogg');
        }
        else {
            audioElement.setAttribute('src', path + '/' + sound + '.mp3');
        }
        audioElement.addEventListener('load', (/**
         * @return {?}
         */
        function () {
            audioElement.play();
        }), true);
        audioElement.pause();
        audioElement.play();
    };
    DialogsService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    DialogsService.ctorParameters = function () { return [
        { type: RendererFactory2 },
        { type: BsModalService }
    ]; };
    return DialogsService;
}());
export { DialogsService };
if (false) {
    /** @type {?} */
    DialogsService.prototype.bsModalRef;
    /** @type {?} */
    DialogsService.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    DialogsService.prototype.modalService;
}
/**
 * @record
 */
export function DialogOptions() { }
if (false) {
    /** @type {?} */
    DialogOptions.prototype.title;
    /** @type {?} */
    DialogOptions.prototype.message;
    /** @type {?} */
    DialogOptions.prototype.buttons;
}
/**
 * @record
 */
export function DialogButton() { }
if (false) {
    /** @type {?} */
    DialogButton.prototype.label;
    /** @type {?} */
    DialogButton.prototype.className;
}
/**
 * @record
 */
export function DialogButtons() { }
if (false) {
    /** @type {?|undefined} */
    DialogButtons.prototype.confirm;
    /** @type {?|undefined} */
    DialogButtons.prototype.cancel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9ncy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy9kaWFsb2dzL2RpYWxvZ3Muc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQWEsZ0JBQWdCLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDdEUsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sMkNBQTJDLENBQUM7QUFDakYsT0FBTyxFQUFhLGNBQWMsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUd6RDtJQUtJLHdCQUNJLGVBQWlDLEVBQ3pCLFlBQTRCO1FBQTVCLGlCQUFZLEdBQVosWUFBWSxDQUFnQjtRQUNwQyxJQUFJLENBQUMsUUFBUSxHQUFHLGVBQWUsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQy9ELENBQUM7Ozs7O0lBRU0sZ0NBQU87Ozs7SUFBZCxVQUFlLFlBQTJCO1FBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFDM0Q7WUFDSSxZQUFZLGNBQUE7WUFDWixRQUFRLEVBQUUsUUFBUTtZQUNsQixRQUFRLEVBQUUsS0FBSztZQUNmLEtBQUssRUFBRSx1QkFBdUI7U0FDakMsQ0FBQyxDQUFDO1FBQ1AsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUN4RSxPQUFPLG1CQUFBLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBdUIsQ0FBQztJQUNsRSxDQUFDOzs7Ozs7SUFFTSxrQ0FBUzs7Ozs7SUFBaEIsVUFBaUIsS0FBYSxFQUFFLElBQTJCO1FBQTNCLHFCQUFBLEVBQUEsMkJBQTJCOztZQUNqRCxZQUFZLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUM7UUFDcEQsSUFBSSxTQUFTLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUN2QyxZQUFZLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxJQUFJLEdBQUcsR0FBRyxHQUFHLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQztTQUNqRTthQUFNO1lBQ0gsWUFBWSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsSUFBSSxHQUFHLEdBQUcsR0FBRyxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUM7U0FDakU7UUFFRCxZQUFZLENBQUMsZ0JBQWdCLENBQUMsTUFBTTs7O1FBQUU7WUFDbEMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3hCLENBQUMsR0FBRSxJQUFJLENBQUMsQ0FBQztRQUNULFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNyQixZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDeEIsQ0FBQzs7Z0JBckNKLFVBQVU7Ozs7Z0JBTG9CLGdCQUFnQjtnQkFFM0IsY0FBYzs7SUEwQ2xDLHFCQUFDO0NBQUEsQUF2Q0QsSUF1Q0M7U0F0Q1ksY0FBYzs7O0lBQ3ZCLG9DQUF1Qjs7SUFDdkIsa0NBQW9COzs7OztJQUloQixzQ0FBb0M7Ozs7O0FBa0M1QyxtQ0FJQzs7O0lBSEcsOEJBQWM7O0lBQ2QsZ0NBQWdCOztJQUNoQixnQ0FBdUI7Ozs7O0FBRzNCLGtDQUdDOzs7SUFGRyw2QkFBYzs7SUFDZCxpQ0FBa0I7Ozs7O0FBR3RCLG1DQUdDOzs7SUFGRyxnQ0FBdUI7O0lBQ3ZCLCtCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZSwgUmVuZGVyZXIyLCBSZW5kZXJlckZhY3RvcnkyfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtDb25maXJtRGlhbG9nQ29tcG9uZW50fSBmcm9tICcuL2NvbmZpcm0tZGlhbG9nL2NvbmZpcm0tZGlhbG9nLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QnNNb2RhbFJlZiwgQnNNb2RhbFNlcnZpY2V9IGZyb20gJ25neC1ib290c3RyYXAnO1xyXG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgRGlhbG9nc1NlcnZpY2Uge1xyXG4gICAgYnNNb2RhbFJlZjogQnNNb2RhbFJlZjtcclxuICAgIHJlbmRlcmVyOiBSZW5kZXJlcjI7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcmVuZGVyZXJGYWN0b3J5OiBSZW5kZXJlckZhY3RvcnkyLFxyXG4gICAgICAgIHByaXZhdGUgbW9kYWxTZXJ2aWNlOiBCc01vZGFsU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIgPSByZW5kZXJlckZhY3RvcnkuY3JlYXRlUmVuZGVyZXIobnVsbCwgbnVsbCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNvbmZpcm0oaW5pdGlhbFN0YXRlOiBEaWFsb2dPcHRpb25zKSB7XHJcbiAgICAgICAgdGhpcy5wbGF5U291bmQoJ21lc3NhZ2Vib3gnKTtcclxuICAgICAgICB0aGlzLmJzTW9kYWxSZWYgPSB0aGlzLm1vZGFsU2VydmljZS5zaG93KENvbmZpcm1EaWFsb2dDb21wb25lbnQsXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGluaXRpYWxTdGF0ZSxcclxuICAgICAgICAgICAgICAgIGJhY2tkcm9wOiAnc3RhdGljJyxcclxuICAgICAgICAgICAgICAgIGtleWJvYXJkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGNsYXNzOiAnbW9kYWwtZGlhbG9nLWNlbnRlcmVkJ1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tb2RhbCcpLCAnbW9kYWwtYWxlcnQnKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5ic01vZGFsUmVmLmNvbnRlbnQub25DbG9zZSBhcyBPYnNlcnZhYmxlPGJvb2xlYW4+O1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBwbGF5U291bmQoc291bmQ6IHN0cmluZywgcGF0aCA9ICdhc3NldHMvbWVkaWEvc291bmQnKSB7XHJcbiAgICAgICAgY29uc3QgYXVkaW9FbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYXVkaW8nKTtcclxuICAgICAgICBpZiAobmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgnRmlyZWZveC8nKSkge1xyXG4gICAgICAgICAgICBhdWRpb0VsZW1lbnQuc2V0QXR0cmlidXRlKCdzcmMnLCBwYXRoICsgJy8nICsgc291bmQgKyAnLm9nZycpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGF1ZGlvRWxlbWVudC5zZXRBdHRyaWJ1dGUoJ3NyYycsIHBhdGggKyAnLycgKyBzb3VuZCArICcubXAzJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBhdWRpb0VsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbG9hZCcsICgpID0+IHtcclxuICAgICAgICAgICAgYXVkaW9FbGVtZW50LnBsYXkoKTtcclxuICAgICAgICB9LCB0cnVlKTtcclxuICAgICAgICBhdWRpb0VsZW1lbnQucGF1c2UoKTtcclxuICAgICAgICBhdWRpb0VsZW1lbnQucGxheSgpO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBEaWFsb2dPcHRpb25zIHtcclxuICAgIHRpdGxlOiBzdHJpbmc7XHJcbiAgICBtZXNzYWdlOiBzdHJpbmc7XHJcbiAgICBidXR0b25zOiBEaWFsb2dCdXR0b25zO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIERpYWxvZ0J1dHRvbiB7XHJcbiAgICBsYWJlbDogc3RyaW5nO1xyXG4gICAgY2xhc3NOYW1lOiBzdHJpbmc7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRGlhbG9nQnV0dG9ucyB7XHJcbiAgICBjb25maXJtPzogRGlhbG9nQnV0dG9uO1xyXG4gICAgY2FuY2VsPzogRGlhbG9nQnV0dG9uO1xyXG59XHJcbiJdfQ==