/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/dialogs/confirm-dialog/confirm-dialog.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
var ConfirmDialogComponent = /** @class */ (function () {
    function ConfirmDialogComponent(bsModalRef) {
        this.bsModalRef = bsModalRef;
        this.onClose = new Subject();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    ConfirmDialogComponent.prototype.confirm = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.onClose.next(true);
        this.onClose.complete();
        this.bsModalRef.hide();
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    ConfirmDialogComponent.prototype.cancel = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.onClose.next(false);
        this.onClose.complete();
        this.bsModalRef.hide();
    };
    ConfirmDialogComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-confirm-dialog',
                    template: "\n        <div class=\"modal-header\">\n            <div class=\"modal-title\" [innerHTML]=\"title\">\n            </div>\n        </div>\n        <div class=\"modal-body\" [innerHTML]=\"message\"></div>\n        <div class=\"modal-footer\" *ngIf=\"buttons\">\n            <button (click)=\"confirm($event)\" type=\"button\" class=\"btn bootbox-accept {{buttons.confirm.className}}\">\n                {{buttons.confirm.label}}</button>\n            <button (click)=\"cancel($event)\" type=\"button\" class=\"btn bootbox-cancel {{buttons.cancel.className}}\">\n                {{buttons.cancel.label}}</button>\n        </div>\n    "
                }] }
    ];
    /** @nocollapse */
    ConfirmDialogComponent.ctorParameters = function () { return [
        { type: BsModalRef }
    ]; };
    return ConfirmDialogComponent;
}());
export { ConfirmDialogComponent };
if (false) {
    /** @type {?} */
    ConfirmDialogComponent.prototype.title;
    /** @type {?} */
    ConfirmDialogComponent.prototype.message;
    /** @type {?} */
    ConfirmDialogComponent.prototype.buttons;
    /** @type {?} */
    ConfirmDialogComponent.prototype.onClose;
    /** @type {?} */
    ConfirmDialogComponent.prototype.bsModalRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybS1kaWFsb2cuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy9kaWFsb2dzL2NvbmZpcm0tZGlhbG9nL2NvbmZpcm0tZGlhbG9nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDeEMsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUMsT0FBTyxFQUFDLE1BQU0sTUFBTSxDQUFDO0FBRTdCO0lBc0JJLGdDQUFtQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBRnpDLFlBQU8sR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO0lBR3hCLENBQUM7Ozs7O0lBRUQsd0NBQU87Ozs7SUFBUCxVQUFRLE1BQWtCO1FBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVELHVDQUFNOzs7O0lBQU4sVUFBTyxNQUFrQjtRQUNyQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7Z0JBbkNKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixRQUFRLEVBQUUsMG5CQVlUO2lCQUNKOzs7O2dCQW5CTyxVQUFVOztJQXlDbEIsNkJBQUM7Q0FBQSxBQXJDRCxJQXFDQztTQXJCWSxzQkFBc0I7OztJQUMvQix1Q0FBYzs7SUFDZCx5Q0FBZ0I7O0lBQ2hCLHlDQUF1Qjs7SUFDdkIseUNBQXdCOztJQUVaLDRDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtCc01vZGFsUmVmfSBmcm9tICduZ3gtYm9vdHN0cmFwJztcclxuaW1wb3J0IHtEaWFsb2dCdXR0b25zfSBmcm9tICcuLi9kaWFsb2dzLnNlcnZpY2UnO1xyXG5pbXBvcnQge1N1YmplY3R9IGZyb20gJ3J4anMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2NvcmUtY29uZmlybS1kaWFsb2cnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtaGVhZGVyXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC10aXRsZVwiIFtpbm5lckhUTUxdPVwidGl0bGVcIj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIiBbaW5uZXJIVE1MXT1cIm1lc3NhZ2VcIj48L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZm9vdGVyXCIgKm5nSWY9XCJidXR0b25zXCI+XHJcbiAgICAgICAgICAgIDxidXR0b24gKGNsaWNrKT1cImNvbmZpcm0oJGV2ZW50KVwiIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBib290Ym94LWFjY2VwdCB7e2J1dHRvbnMuY29uZmlybS5jbGFzc05hbWV9fVwiPlxyXG4gICAgICAgICAgICAgICAge3tidXR0b25zLmNvbmZpcm0ubGFiZWx9fTwvYnV0dG9uPlxyXG4gICAgICAgICAgICA8YnV0dG9uIChjbGljayk9XCJjYW5jZWwoJGV2ZW50KVwiIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBib290Ym94LWNhbmNlbCB7e2J1dHRvbnMuY2FuY2VsLmNsYXNzTmFtZX19XCI+XHJcbiAgICAgICAgICAgICAgICB7e2J1dHRvbnMuY2FuY2VsLmxhYmVsfX08L2J1dHRvbj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIGAsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb25maXJtRGlhbG9nQ29tcG9uZW50IHtcclxuICAgIHRpdGxlOiBzdHJpbmc7XHJcbiAgICBtZXNzYWdlOiBzdHJpbmc7XHJcbiAgICBidXR0b25zOiBEaWFsb2dCdXR0b25zO1xyXG4gICAgb25DbG9zZSA9IG5ldyBTdWJqZWN0KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGJzTW9kYWxSZWY6IEJzTW9kYWxSZWYpIHtcclxuICAgIH1cclxuXHJcbiAgICBjb25maXJtKCRldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgIHRoaXMub25DbG9zZS5uZXh0KHRydWUpO1xyXG4gICAgICAgIHRoaXMub25DbG9zZS5jb21wbGV0ZSgpO1xyXG4gICAgICAgIHRoaXMuYnNNb2RhbFJlZi5oaWRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuY2VsKCRldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgIHRoaXMub25DbG9zZS5uZXh0KGZhbHNlKTtcclxuICAgICAgICB0aGlzLm9uQ2xvc2UuY29tcGxldGUoKTtcclxuICAgICAgICB0aGlzLmJzTW9kYWxSZWYuaGlkZSgpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=