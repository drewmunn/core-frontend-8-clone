/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/dialogs/dialogs.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { ModalModule } from 'ngx-bootstrap';
import { DialogsService } from './dialogs.service';
var DialogsModule = /** @class */ (function () {
    function DialogsModule() {
    }
    DialogsModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [ConfirmDialogComponent],
                    entryComponents: [ConfirmDialogComponent],
                    imports: [
                        CommonModule,
                        ModalModule
                    ],
                    providers: [DialogsService]
                },] }
    ];
    return DialogsModule;
}());
export { DialogsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9ncy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3V0aWxzL2RpYWxvZ3MvZGlhbG9ncy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSwyQ0FBMkMsQ0FBQztBQUNqRixPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzFDLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUVqRDtJQUFBO0lBVUEsQ0FBQzs7Z0JBVkEsUUFBUSxTQUFDO29CQUNOLFlBQVksRUFBRSxDQUFDLHNCQUFzQixDQUFDO29CQUN0QyxlQUFlLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztvQkFDekMsT0FBTyxFQUFFO3dCQUNMLFlBQVk7d0JBQ1osV0FBVztxQkFDZDtvQkFDRCxTQUFTLEVBQUUsQ0FBQyxjQUFjLENBQUM7aUJBQzlCOztJQUVELG9CQUFDO0NBQUEsQUFWRCxJQVVDO1NBRFksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHtDb25maXJtRGlhbG9nQ29tcG9uZW50fSBmcm9tICcuL2NvbmZpcm0tZGlhbG9nL2NvbmZpcm0tZGlhbG9nLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7TW9kYWxNb2R1bGV9IGZyb20gJ25neC1ib290c3RyYXAnO1xyXG5pbXBvcnQge0RpYWxvZ3NTZXJ2aWNlfSBmcm9tICcuL2RpYWxvZ3Muc2VydmljZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbQ29uZmlybURpYWxvZ0NvbXBvbmVudF0sXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtDb25maXJtRGlhbG9nQ29tcG9uZW50XSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgTW9kYWxNb2R1bGVcclxuICAgIF0sXHJcbiAgICBwcm92aWRlcnM6IFtEaWFsb2dzU2VydmljZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIERpYWxvZ3NNb2R1bGUge1xyXG59XHJcbiJdfQ==