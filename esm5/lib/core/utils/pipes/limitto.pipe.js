/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/pipes/limitto.pipe.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var TruncatePipe = /** @class */ (function () {
    function TruncatePipe() {
    }
    /**
     * @param {?} value
     * @param {?} args
     * @return {?}
     */
    TruncatePipe.prototype.transform = /**
     * @param {?} value
     * @param {?} args
     * @return {?}
     */
    function (value, args) {
        /** @type {?} */
        var limit = args || 10;
        /** @type {?} */
        var trail = '...';
        return value.length > limit ? value.substring(0, limit) + trail : value;
    };
    TruncatePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'limitTo'
                },] }
    ];
    return TruncatePipe;
}());
export { TruncatePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGltaXR0by5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy9waXBlcy9saW1pdHRvLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsSUFBSSxFQUFnQixNQUFNLGVBQWUsQ0FBQztBQUVsRDtJQUFBO0lBYUEsQ0FBQzs7Ozs7O0lBUEcsZ0NBQVM7Ozs7O0lBQVQsVUFBVSxLQUFhLEVBQUUsSUFBWTs7WUFDM0IsS0FBSyxHQUFHLElBQUksSUFBSSxFQUFFOztZQUNsQixLQUFLLEdBQUcsS0FBSztRQUVuQixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUM1RSxDQUFDOztnQkFYSixJQUFJLFNBQUM7b0JBQ0YsSUFBSSxFQUFFLFNBQVM7aUJBQ2xCOztJQVdELG1CQUFDO0NBQUEsQUFiRCxJQWFDO1NBVFksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7UGlwZSwgUGlwZVRyYW5zZm9ybX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgICBuYW1lOiAnbGltaXRUbydcbn0pXG5cbmV4cG9ydCBjbGFzcyBUcnVuY2F0ZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICAgIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nLCBhcmdzOiBudW1iZXIpOiBzdHJpbmcge1xuICAgICAgICBjb25zdCBsaW1pdCA9IGFyZ3MgfHwgMTA7XG4gICAgICAgIGNvbnN0IHRyYWlsID0gJy4uLic7XG5cbiAgICAgICAgcmV0dXJuIHZhbHVlLmxlbmd0aCA+IGxpbWl0ID8gdmFsdWUuc3Vic3RyaW5nKDAsIGxpbWl0KSArIHRyYWlsIDogdmFsdWU7XG4gICAgfVxuXG59XG4iXX0=