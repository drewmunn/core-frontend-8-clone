/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/ui/ui.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnOffComponent } from './on-off/on-off.component';
var UiModule = /** @class */ (function () {
    function UiModule() {
    }
    UiModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [OnOffComponent],
                    imports: [
                        CommonModule
                    ],
                    exports: [OnOffComponent]
                },] }
    ];
    return UiModule;
}());
export { UiModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWkubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy91aS91aS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sMkJBQTJCLENBQUM7QUFFekQ7SUFBQTtJQVFBLENBQUM7O2dCQVJBLFFBQVEsU0FBQztvQkFDTixZQUFZLEVBQUUsQ0FBQyxjQUFjLENBQUM7b0JBQzlCLE9BQU8sRUFBRTt3QkFDTCxZQUFZO3FCQUNmO29CQUNELE9BQU8sRUFBRSxDQUFDLGNBQWMsQ0FBQztpQkFDNUI7O0lBRUQsZUFBQztDQUFBLEFBUkQsSUFRQztTQURZLFFBQVEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7T25PZmZDb21wb25lbnR9IGZyb20gJy4vb24tb2ZmL29uLW9mZi5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW09uT2ZmQ29tcG9uZW50XSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbT25PZmZDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBVaU1vZHVsZSB7XHJcbn1cclxuIl19