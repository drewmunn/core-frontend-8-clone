/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/ui/on-off/on-off.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
var OnOffComponent = /** @class */ (function () {
    function OnOffComponent() {
        this.checked = false;
        this.checkedChange = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    OnOffComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (!changes.checked || changes.checked.currentValue === this.checked) {
            return;
        }
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    OnOffComponent.prototype.onCheck = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        $event.preventDefault();
        this.checked = !this.checked;
        this.checkedChange.emit(this.checked);
    };
    OnOffComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-on-off',
                    template: "\n        <a\n                href=\"#\"\n                (click)=\"onCheck($event)\"\n                class=\"btn btn-switch {{class}}\"\n                [class.active]=\"checked\"></a>\n    ",
                    host: {
                        class: 'd-inline-block'
                    },
                    changeDetection: ChangeDetectionStrategy.OnPush
                }] }
    ];
    OnOffComponent.propDecorators = {
        checked: [{ type: Input }],
        class: [{ type: Input }],
        checkedChange: [{ type: Output }]
    };
    return OnOffComponent;
}());
export { OnOffComponent };
if (false) {
    /** @type {?} */
    OnOffComponent.prototype.checked;
    /** @type {?} */
    OnOffComponent.prototype.class;
    /** @type {?} */
    OnOffComponent.prototype.checkedChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib24tb2ZmLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvdXRpbHMvdWkvb24tb2ZmL29uLW9mZi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsdUJBQXVCLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQWEsTUFBTSxFQUFnQixNQUFNLGVBQWUsQ0FBQztBQUd4SDtJQUFBO1FBZ0JhLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFFZixrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFjakQsQ0FBQzs7Ozs7SUFaRyxvQ0FBVzs7OztJQUFYLFVBQVksT0FBc0I7UUFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEtBQUssSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNuRSxPQUFPO1NBQ1Y7SUFDTCxDQUFDOzs7OztJQUVELGdDQUFPOzs7O0lBQVAsVUFBUSxNQUFrQjtRQUN0QixNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7UUFFN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzFDLENBQUM7O2dCQS9CSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLFFBQVEsRUFBRSxrTUFNVDtvQkFDRCxJQUFJLEVBQUU7d0JBQ0YsS0FBSyxFQUFFLGdCQUFnQjtxQkFDMUI7b0JBQ0QsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07aUJBQ2xEOzs7MEJBR0ksS0FBSzt3QkFDTCxLQUFLO2dDQUNMLE1BQU07O0lBY1gscUJBQUM7Q0FBQSxBQWhDRCxJQWdDQztTQWxCWSxjQUFjOzs7SUFFdkIsaUNBQXlCOztJQUN6QiwrQkFBdUI7O0lBQ3ZCLHVDQUE2QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25DaGFuZ2VzLCBPdXRwdXQsIFNpbXBsZUNoYW5nZXN9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdjb3JlLW9uLW9mZicsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgICAoY2xpY2spPVwib25DaGVjaygkZXZlbnQpXCJcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwiYnRuIGJ0bi1zd2l0Y2gge3tjbGFzc319XCJcclxuICAgICAgICAgICAgICAgIFtjbGFzcy5hY3RpdmVdPVwiY2hlY2tlZFwiPjwvYT5cclxuICAgIGAsXHJcbiAgICBob3N0OiB7XHJcbiAgICAgICAgY2xhc3M6ICdkLWlubGluZS1ibG9jaydcclxuICAgIH0sXHJcbiAgICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxyXG59KVxyXG5leHBvcnQgY2xhc3MgT25PZmZDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xyXG5cclxuICAgIEBJbnB1dCgpIGNoZWNrZWQgPSBmYWxzZTtcclxuICAgIEBJbnB1dCgpIGNsYXNzOiBzdHJpbmc7XHJcbiAgICBAT3V0cHV0KCkgY2hlY2tlZENoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKCFjaGFuZ2VzLmNoZWNrZWQgfHwgY2hhbmdlcy5jaGVja2VkLmN1cnJlbnRWYWx1ZSA9PT0gdGhpcy5jaGVja2VkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25DaGVjaygkZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB0aGlzLmNoZWNrZWQgPSAhdGhpcy5jaGVja2VkO1xyXG5cclxuICAgICAgICB0aGlzLmNoZWNrZWRDaGFuZ2UuZW1pdCh0aGlzLmNoZWNrZWQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==