/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/animations.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { trigger, state, style, transition, animate, group } from '@angular/animations';
/**
 * @return {?}
 */
export function makeSlideInOut() {
    return trigger('slideInOut', [
        state('in', style({ height: '*', opacity: 0 })),
        transition(':leave', [
            style({ height: '*', opacity: 1 }),
            group([
                animate('200ms ease-in-out', style({ height: 0 })),
                animate('200ms ease-in-out', style({ opacity: '0' }))
            ])
        ]),
        transition(':enter', [
            style({ height: '0', opacity: 0 }),
            group([
                animate('200ms ease-in-out', style({ height: '*' })),
                animate('200ms ease-in-out', style({ opacity: '1' }))
            ])
        ])
    ]);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5pbWF0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvdXRpbHMvYW5pbWF0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLHFCQUFxQixDQUFDOzs7O0FBRXhGLE1BQU0sVUFBVSxjQUFjO0lBQzVCLE9BQU8sT0FBTyxDQUFDLFlBQVksRUFBRTtRQUMzQixLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDL0MsVUFBVSxDQUFDLFFBQVEsRUFBRTtZQUNuQixLQUFLLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUVsQyxLQUFLLENBQUM7Z0JBQ0osT0FBTyxDQUFDLG1CQUFtQixFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNsRCxPQUFPLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7YUFDdEQsQ0FBQztTQUVILENBQUM7UUFDRixVQUFVLENBQUMsUUFBUSxFQUFFO1lBQ25CLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDO1lBRWxDLEtBQUssQ0FBQztnQkFDSixPQUFPLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBQ3BELE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQzthQUN0RCxDQUFDO1NBRUgsQ0FBQztLQUNILENBQUMsQ0FBQztBQUNMLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB0cmlnZ2VyLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIGFuaW1hdGUsIGdyb3VwIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gbWFrZVNsaWRlSW5PdXQoKSB7XHJcbiAgcmV0dXJuIHRyaWdnZXIoJ3NsaWRlSW5PdXQnLCBbXHJcbiAgICBzdGF0ZSgnaW4nLCBzdHlsZSh7IGhlaWdodDogJyonLCBvcGFjaXR5OiAwIH0pKSxcclxuICAgIHRyYW5zaXRpb24oJzpsZWF2ZScsIFtcclxuICAgICAgc3R5bGUoeyBoZWlnaHQ6ICcqJywgb3BhY2l0eTogMSB9KSxcclxuXHJcbiAgICAgIGdyb3VwKFtcclxuICAgICAgICBhbmltYXRlKCcyMDBtcyBlYXNlLWluLW91dCcsIHN0eWxlKHsgaGVpZ2h0OiAwIH0pKSxcclxuICAgICAgICBhbmltYXRlKCcyMDBtcyBlYXNlLWluLW91dCcsIHN0eWxlKHsgb3BhY2l0eTogJzAnIH0pKVxyXG4gICAgICBdKVxyXG5cclxuICAgIF0pLFxyXG4gICAgdHJhbnNpdGlvbignOmVudGVyJywgW1xyXG4gICAgICBzdHlsZSh7IGhlaWdodDogJzAnLCBvcGFjaXR5OiAwIH0pLFxyXG5cclxuICAgICAgZ3JvdXAoW1xyXG4gICAgICAgIGFuaW1hdGUoJzIwMG1zIGVhc2UtaW4tb3V0Jywgc3R5bGUoeyBoZWlnaHQ6ICcqJyB9KSksXHJcbiAgICAgICAgYW5pbWF0ZSgnMjAwbXMgZWFzZS1pbi1vdXQnLCBzdHlsZSh7IG9wYWNpdHk6ICcxJyB9KSlcclxuICAgICAgXSlcclxuXHJcbiAgICBdKVxyXG4gIF0pO1xyXG59XHJcbiJdfQ==