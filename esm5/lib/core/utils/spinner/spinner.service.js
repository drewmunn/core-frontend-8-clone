/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/spinner/spinner.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var SpinnerService = /** @class */ (function () {
    function SpinnerService() {
        this.spinnerCache = new Set();
    }
    /**
     * @param {?} spinner
     * @return {?}
     */
    SpinnerService.prototype._register = /**
     * @param {?} spinner
     * @return {?}
     */
    function (spinner) {
        this.spinnerCache.add(spinner);
    };
    /**
     * @param {?} spinnerToRemove
     * @return {?}
     */
    SpinnerService.prototype._unregister = /**
     * @param {?} spinnerToRemove
     * @return {?}
     */
    function (spinnerToRemove) {
        var _this = this;
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        function (spinner) {
            if (spinner === spinnerToRemove) {
                _this.spinnerCache.delete(spinner);
            }
        }));
    };
    /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    SpinnerService.prototype._unregisterGroup = /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    function (spinnerGroup) {
        var _this = this;
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        function (spinner) {
            if (spinner.group === spinnerGroup) {
                _this.spinnerCache.delete(spinner);
            }
        }));
    };
    /**
     * @return {?}
     */
    SpinnerService.prototype._unregisterAll = /**
     * @return {?}
     */
    function () {
        this.spinnerCache.clear();
    };
    /**
     * @param {?} spinnerName
     * @return {?}
     */
    SpinnerService.prototype.show = /**
     * @param {?} spinnerName
     * @return {?}
     */
    function (spinnerName) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        function (spinner) {
            if (spinner.name === spinnerName) {
                spinner.show = true;
            }
        }));
    };
    /**
     * @param {?} spinnerName
     * @return {?}
     */
    SpinnerService.prototype.hide = /**
     * @param {?} spinnerName
     * @return {?}
     */
    function (spinnerName) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        function (spinner) {
            if (spinner.name === spinnerName) {
                spinner.show = false;
            }
        }));
    };
    /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    SpinnerService.prototype.showGroup = /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    function (spinnerGroup) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        function (spinner) {
            if (spinner.group === spinnerGroup) {
                spinner.show = true;
            }
        }));
    };
    /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    SpinnerService.prototype.hideGroup = /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    function (spinnerGroup) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        function (spinner) {
            if (spinner.group === spinnerGroup) {
                spinner.show = false;
            }
        }));
    };
    /**
     * @return {?}
     */
    SpinnerService.prototype.showAll = /**
     * @return {?}
     */
    function () {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        function (spinner) { return spinner.show = true; }));
    };
    /**
     * @return {?}
     */
    SpinnerService.prototype.hideAll = /**
     * @return {?}
     */
    function () {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        function (spinner) { return spinner.show = false; }));
    };
    /**
     * @param {?} spinnerName
     * @return {?}
     */
    SpinnerService.prototype.isShowing = /**
     * @param {?} spinnerName
     * @return {?}
     */
    function (spinnerName) {
        /** @type {?} */
        var showing = undefined;
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        function (spinner) {
            if (spinner.name === spinnerName) {
                showing = spinner.show;
            }
        }));
        return showing;
    };
    SpinnerService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */ SpinnerService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SpinnerService_Factory() { return new SpinnerService(); }, token: SpinnerService, providedIn: "root" });
    return SpinnerService;
}());
export { SpinnerService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SpinnerService.prototype.spinnerCache;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Bpbm5lci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy9zcGlubmVyL3NwaW5uZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7O0FBR3pDO0lBQUE7UUFJWSxpQkFBWSxHQUFHLElBQUksR0FBRyxFQUFvQixDQUFDO0tBMkV0RDs7Ozs7SUF6RUcsa0NBQVM7Ozs7SUFBVCxVQUFVLE9BQXlCO1FBQy9CLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsb0NBQVc7Ozs7SUFBWCxVQUFZLGVBQWlDO1FBQTdDLGlCQU1DO1FBTEcsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQSxPQUFPO1lBQzdCLElBQUksT0FBTyxLQUFLLGVBQWUsRUFBRTtnQkFDN0IsS0FBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDckM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQseUNBQWdCOzs7O0lBQWhCLFVBQWlCLFlBQW9CO1FBQXJDLGlCQU1DO1FBTEcsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQSxPQUFPO1lBQzdCLElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxZQUFZLEVBQUU7Z0JBQ2hDLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ3JDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsdUNBQWM7OztJQUFkO1FBQ0ksSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUM5QixDQUFDOzs7OztJQUVELDZCQUFJOzs7O0lBQUosVUFBSyxXQUFtQjtRQUNwQixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU87Ozs7UUFBQyxVQUFBLE9BQU87WUFDN0IsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFBRTtnQkFDOUIsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7YUFDdkI7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsNkJBQUk7Ozs7SUFBSixVQUFLLFdBQW1CO1FBQ3BCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTzs7OztRQUFDLFVBQUEsT0FBTztZQUM3QixJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssV0FBVyxFQUFFO2dCQUM5QixPQUFPLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQzthQUN4QjtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxrQ0FBUzs7OztJQUFULFVBQVUsWUFBb0I7UUFDMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQSxPQUFPO1lBQzdCLElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxZQUFZLEVBQUU7Z0JBQ2hDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELGtDQUFTOzs7O0lBQVQsVUFBVSxZQUFvQjtRQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU87Ozs7UUFBQyxVQUFBLE9BQU87WUFDN0IsSUFBSSxPQUFPLENBQUMsS0FBSyxLQUFLLFlBQVksRUFBRTtnQkFDaEMsT0FBTyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7YUFDeEI7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxnQ0FBTzs7O0lBQVA7UUFDSSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU87Ozs7UUFBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxFQUFuQixDQUFtQixFQUFDLENBQUM7SUFDOUQsQ0FBQzs7OztJQUVELGdDQUFPOzs7SUFBUDtRQUNJLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTzs7OztRQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLElBQUksR0FBRyxLQUFLLEVBQXBCLENBQW9CLEVBQUMsQ0FBQztJQUMvRCxDQUFDOzs7OztJQUVELGtDQUFTOzs7O0lBQVQsVUFBVSxXQUFtQjs7WUFDckIsT0FBTyxHQUFHLFNBQVM7UUFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQSxPQUFPO1lBQzdCLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxXQUFXLEVBQUU7Z0JBQzlCLE9BQU8sR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDO2FBQzFCO1FBQ0wsQ0FBQyxFQUFDLENBQUM7UUFDSCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDOztnQkE5RUosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7O3lCQUxEO0NBa0ZDLEFBL0VELElBK0VDO1NBNUVZLGNBQWM7Ozs7OztJQUN2QixzQ0FBbUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtTcGlubmVyQ29tcG9uZW50fSBmcm9tICcuL3NwaW5uZXIuY29tcG9uZW50JztcblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBTcGlubmVyU2VydmljZSB7XG4gICAgcHJpdmF0ZSBzcGlubmVyQ2FjaGUgPSBuZXcgU2V0PFNwaW5uZXJDb21wb25lbnQ+KCk7XG5cbiAgICBfcmVnaXN0ZXIoc3Bpbm5lcjogU3Bpbm5lckNvbXBvbmVudCk6IHZvaWQge1xuICAgICAgICB0aGlzLnNwaW5uZXJDYWNoZS5hZGQoc3Bpbm5lcik7XG4gICAgfVxuXG4gICAgX3VucmVnaXN0ZXIoc3Bpbm5lclRvUmVtb3ZlOiBTcGlubmVyQ29tcG9uZW50KTogdm9pZCB7XG4gICAgICAgIHRoaXMuc3Bpbm5lckNhY2hlLmZvckVhY2goc3Bpbm5lciA9PiB7XG4gICAgICAgICAgICBpZiAoc3Bpbm5lciA9PT0gc3Bpbm5lclRvUmVtb3ZlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZGVsZXRlKHNwaW5uZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBfdW5yZWdpc3Rlckdyb3VwKHNwaW5uZXJHcm91cDogc3RyaW5nKTogdm9pZCB7XG4gICAgICAgIHRoaXMuc3Bpbm5lckNhY2hlLmZvckVhY2goc3Bpbm5lciA9PiB7XG4gICAgICAgICAgICBpZiAoc3Bpbm5lci5ncm91cCA9PT0gc3Bpbm5lckdyb3VwKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZGVsZXRlKHNwaW5uZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBfdW5yZWdpc3RlckFsbCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuY2xlYXIoKTtcbiAgICB9XG5cbiAgICBzaG93KHNwaW5uZXJOYW1lOiBzdHJpbmcpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZm9yRWFjaChzcGlubmVyID0+IHtcbiAgICAgICAgICAgIGlmIChzcGlubmVyLm5hbWUgPT09IHNwaW5uZXJOYW1lKSB7XG4gICAgICAgICAgICAgICAgc3Bpbm5lci5zaG93ID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgaGlkZShzcGlubmVyTmFtZTogc3RyaW5nKTogdm9pZCB7XG4gICAgICAgIHRoaXMuc3Bpbm5lckNhY2hlLmZvckVhY2goc3Bpbm5lciA9PiB7XG4gICAgICAgICAgICBpZiAoc3Bpbm5lci5uYW1lID09PSBzcGlubmVyTmFtZSkge1xuICAgICAgICAgICAgICAgIHNwaW5uZXIuc2hvdyA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzaG93R3JvdXAoc3Bpbm5lckdyb3VwOiBzdHJpbmcpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZm9yRWFjaChzcGlubmVyID0+IHtcbiAgICAgICAgICAgIGlmIChzcGlubmVyLmdyb3VwID09PSBzcGlubmVyR3JvdXApIHtcbiAgICAgICAgICAgICAgICBzcGlubmVyLnNob3cgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBoaWRlR3JvdXAoc3Bpbm5lckdyb3VwOiBzdHJpbmcpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZm9yRWFjaChzcGlubmVyID0+IHtcbiAgICAgICAgICAgIGlmIChzcGlubmVyLmdyb3VwID09PSBzcGlubmVyR3JvdXApIHtcbiAgICAgICAgICAgICAgICBzcGlubmVyLnNob3cgPSBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc2hvd0FsbCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZm9yRWFjaChzcGlubmVyID0+IHNwaW5uZXIuc2hvdyA9IHRydWUpO1xuICAgIH1cblxuICAgIGhpZGVBbGwoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuc3Bpbm5lckNhY2hlLmZvckVhY2goc3Bpbm5lciA9PiBzcGlubmVyLnNob3cgPSBmYWxzZSk7XG4gICAgfVxuXG4gICAgaXNTaG93aW5nKHNwaW5uZXJOYW1lOiBzdHJpbmcpOiBib29sZWFuIHwgdW5kZWZpbmVkIHtcbiAgICAgICAgbGV0IHNob3dpbmcgPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMuc3Bpbm5lckNhY2hlLmZvckVhY2goc3Bpbm5lciA9PiB7XG4gICAgICAgICAgICBpZiAoc3Bpbm5lci5uYW1lID09PSBzcGlubmVyTmFtZSkge1xuICAgICAgICAgICAgICAgIHNob3dpbmcgPSBzcGlubmVyLnNob3c7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gc2hvd2luZztcbiAgICB9XG59Il19