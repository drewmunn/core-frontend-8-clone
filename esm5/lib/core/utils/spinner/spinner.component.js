/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/spinner/spinner.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SpinnerService } from './spinner.service';
var SpinnerComponent = /** @class */ (function () {
    function SpinnerComponent(spinnerService) {
        this.spinnerService = spinnerService;
        this.isShowing = false;
        this.showChange = new EventEmitter();
    }
    Object.defineProperty(SpinnerComponent.prototype, "show", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isShowing;
        },
        set: /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            this.isShowing = val;
            this.showChange.emit(this.isShowing);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    SpinnerComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (!this.name) {
            throw new Error('Spinner must have a \'name\' attribute.');
        }
        this.spinnerService._register(this);
    };
    /**
     * @return {?}
     */
    SpinnerComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.spinnerService._unregister(this);
    };
    SpinnerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-spinner',
                    template: "\n        <mat-spinner color=\"primary\" diameter=\"30\" *ngIf=\"show\"></mat-spinner>\n    "
                }] }
    ];
    /** @nocollapse */
    SpinnerComponent.ctorParameters = function () { return [
        { type: SpinnerService }
    ]; };
    SpinnerComponent.propDecorators = {
        name: [{ type: Input }],
        group: [{ type: Input }],
        loadingImage: [{ type: Input }],
        show: [{ type: Input }],
        showChange: [{ type: Output }]
    };
    return SpinnerComponent;
}());
export { SpinnerComponent };
if (false) {
    /** @type {?} */
    SpinnerComponent.prototype.name;
    /** @type {?} */
    SpinnerComponent.prototype.group;
    /** @type {?} */
    SpinnerComponent.prototype.loadingImage;
    /**
     * @type {?}
     * @private
     */
    SpinnerComponent.prototype.isShowing;
    /** @type {?} */
    SpinnerComponent.prototype.showChange;
    /**
     * @type {?}
     * @private
     */
    SpinnerComponent.prototype.spinnerService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Bpbm5lci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3V0aWxzL3NwaW5uZXIvc3Bpbm5lci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN4RixPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFFakQ7SUFPSSwwQkFBb0IsY0FBOEI7UUFBOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBTzFDLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFPaEIsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFiMUMsQ0FBQztJQVFELHNCQUNJLGtDQUFJOzs7O1FBRFI7WUFFSSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDMUIsQ0FBQzs7Ozs7UUFJRCxVQUFTLEdBQVk7WUFDakIsSUFBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7WUFDckIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pDLENBQUM7OztPQVBBOzs7O0lBU0QsbUNBQVE7OztJQUFSO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDWixNQUFNLElBQUksS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUE7U0FDN0Q7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QyxDQUFDOzs7O0lBRUQsc0NBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Z0JBckNKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsUUFBUSxFQUFFLDhGQUVUO2lCQUNKOzs7O2dCQVBPLGNBQWM7Ozt1QkFZakIsS0FBSzt3QkFDTCxLQUFLOytCQUNMLEtBQUs7dUJBSUwsS0FBSzs2QkFLTCxNQUFNOztJQWtCWCx1QkFBQztDQUFBLEFBdkNELElBdUNDO1NBakNZLGdCQUFnQjs7O0lBSXpCLGdDQUFzQjs7SUFDdEIsaUNBQXVCOztJQUN2Qix3Q0FBOEI7Ozs7O0lBRTlCLHFDQUEwQjs7SUFPMUIsc0NBQTBDOzs7OztJQWQ5QiwwQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25EZXN0cm95LCBPbkluaXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1NwaW5uZXJTZXJ2aWNlfSBmcm9tICcuL3NwaW5uZXIuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1zcGlubmVyJyxcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8bWF0LXNwaW5uZXIgY29sb3I9XCJwcmltYXJ5XCIgZGlhbWV0ZXI9XCIzMFwiICpuZ0lmPVwic2hvd1wiPjwvbWF0LXNwaW5uZXI+XG4gICAgYFxufSlcbmV4cG9ydCBjbGFzcyBTcGlubmVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgc3Bpbm5lclNlcnZpY2U6IFNwaW5uZXJTZXJ2aWNlKSB7XG4gICAgfVxuXG4gICAgQElucHV0KCkgbmFtZTogc3RyaW5nO1xuICAgIEBJbnB1dCgpIGdyb3VwOiBzdHJpbmc7XG4gICAgQElucHV0KCkgbG9hZGluZ0ltYWdlOiBzdHJpbmc7XG5cbiAgICBwcml2YXRlIGlzU2hvd2luZyA9IGZhbHNlO1xuXG4gICAgQElucHV0KClcbiAgICBnZXQgc2hvdygpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTaG93aW5nO1xuICAgIH1cblxuICAgIEBPdXRwdXQoKSBzaG93Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgc2V0IHNob3codmFsOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuaXNTaG93aW5nID0gdmFsO1xuICAgICAgICB0aGlzLnNob3dDaGFuZ2UuZW1pdCh0aGlzLmlzU2hvd2luZyk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgICAgIGlmICghdGhpcy5uYW1lKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1NwaW5uZXIgbXVzdCBoYXZlIGEgXFwnbmFtZVxcJyBhdHRyaWJ1dGUuJylcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnNwaW5uZXJTZXJ2aWNlLl9yZWdpc3Rlcih0aGlzKTtcbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyU2VydmljZS5fdW5yZWdpc3Rlcih0aGlzKTtcbiAgICB9XG5cbn1cbiJdfQ==