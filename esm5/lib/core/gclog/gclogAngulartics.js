/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/gclog/gclogAngulartics.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Angulartics2 } from 'angulartics2';
import * as i0 from "@angular/core";
import * as i1 from "angulartics2";
var AngularticsGCLOG = /** @class */ (function () {
    function AngularticsGCLOG(angulartics2) {
        this.angulartics2 = angulartics2;
    }
    /**
     * @return {?}
     */
    AngularticsGCLOG.prototype.startTracking = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.angulartics2.pageTrack
            .pipe(this.angulartics2.filterDeveloperMode())
            .subscribe((/**
         * @param {?} x
         * @return {?}
         */
        function (x) { return _this.pageTrack(x.path); }));
        this.angulartics2.eventTrack
            .pipe(this.angulartics2.filterDeveloperMode())
            .subscribe((/**
         * @param {?} x
         * @return {?}
         */
        function (x) { return _this.eventTrack(x.action, x.properties); }));
        this.angulartics2.exceptionTrack
            .pipe(this.angulartics2.filterDeveloperMode())
            .subscribe((/**
         * @param {?} x
         * @return {?}
         */
        function (x) { return _this.exceptionTrack(x); }));
        this.angulartics2.setUsername
            .pipe(this.angulartics2.filterDeveloperMode())
            .subscribe((/**
         * @param {?} x
         * @return {?}
         */
        function (x) { return _this.setUsername(x); }));
    };
    /**
     * @param {?} path
     * @return {?}
     */
    AngularticsGCLOG.prototype.pageTrack = /**
     * @param {?} path
     * @return {?}
     */
    function (path) {
        try {
            gclog.addPageAction(path);
        }
        catch (err) {
        }
    };
    /**
     * @param {?} action
     * @param {?} properties
     * @return {?}
     */
    AngularticsGCLOG.prototype.eventTrack = /**
     * @param {?} action
     * @param {?} properties
     * @return {?}
     */
    function (action, properties) {
        try {
            gclog.addPageAction(action, properties);
        }
        catch (err) {
        }
    };
    /**
     * @param {?} properties
     * @return {?}
     */
    AngularticsGCLOG.prototype.exceptionTrack = /**
     * @param {?} properties
     * @return {?}
     */
    function (properties) {
        if (properties.fatal === undefined) {
            console.log('No "fatal" provided, sending with fatal=true');
            properties.exFatal = true;
        }
        properties.exDescription = properties.event ? properties.event.stack : properties.description;
        this.eventTrack("Exception thrown for " + properties.appName + " <" + properties.appId + "@" + properties.appVersion + ">", {
            category: 'Exception',
            label: properties.exDescription,
        });
    };
    /**
     * @param {?} userId
     * @return {?}
     */
    AngularticsGCLOG.prototype.setUsername = /**
     * @param {?} userId
     * @return {?}
     */
    function (userId) {
        try {
            gclog.setCustomAttribute('username', userId);
        }
        catch (err) {
        }
    };
    AngularticsGCLOG.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    AngularticsGCLOG.ctorParameters = function () { return [
        { type: Angulartics2 }
    ]; };
    /** @nocollapse */ AngularticsGCLOG.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AngularticsGCLOG_Factory() { return new AngularticsGCLOG(i0.ɵɵinject(i1.Angulartics2)); }, token: AngularticsGCLOG, providedIn: "root" });
    return AngularticsGCLOG;
}());
export { AngularticsGCLOG };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AngularticsGCLOG.prototype.angulartics2;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2Nsb2dBbmd1bGFydGljcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvZ2Nsb2cvZ2Nsb2dBbmd1bGFydGljcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGNBQWMsQ0FBQzs7O0FBSTFDO0lBR0ksMEJBQ2MsWUFBMEI7UUFBMUIsaUJBQVksR0FBWixZQUFZLENBQWM7SUFFeEMsQ0FBQzs7OztJQUVELHdDQUFhOzs7SUFBYjtRQUFBLGlCQWFDO1FBWkcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTO2FBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFtQixFQUFFLENBQUM7YUFDN0MsU0FBUzs7OztRQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQXRCLENBQXNCLEVBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVU7YUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsbUJBQW1CLEVBQUUsQ0FBQzthQUM3QyxTQUFTOzs7O1FBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxFQUF2QyxDQUF1QyxFQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjO2FBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFtQixFQUFFLENBQUM7YUFDN0MsU0FBUzs7OztRQUFDLFVBQUMsQ0FBTSxJQUFLLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsRUFBdEIsQ0FBc0IsRUFBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVzthQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2FBQzdDLFNBQVM7Ozs7UUFBQyxVQUFDLENBQVMsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQW5CLENBQW1CLEVBQUMsQ0FBQztJQUN2RCxDQUFDOzs7OztJQUVELG9DQUFTOzs7O0lBQVQsVUFBVSxJQUFZO1FBQ2xCLElBQUk7WUFDQSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzdCO1FBQUMsT0FBTyxHQUFHLEVBQUU7U0FDYjtJQUNMLENBQUM7Ozs7OztJQUVELHFDQUFVOzs7OztJQUFWLFVBQVcsTUFBYyxFQUFFLFVBQWU7UUFDdEMsSUFBSTtZQUNKLEtBQUssQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQ3ZDO1FBQUMsT0FBTyxHQUFHLEVBQUU7U0FDYjtJQUNMLENBQUM7Ozs7O0lBRUQseUNBQWM7Ozs7SUFBZCxVQUFlLFVBQWU7UUFDMUIsSUFBSSxVQUFVLENBQUMsS0FBSyxLQUFLLFNBQVMsRUFBRTtZQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLDhDQUE4QyxDQUFDLENBQUM7WUFDNUQsVUFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7U0FDN0I7UUFFRCxVQUFVLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO1FBRTlGLElBQUksQ0FBQyxVQUFVLENBQUMsMEJBQXdCLFVBQVUsQ0FBQyxPQUFPLFVBQUssVUFBVSxDQUFDLEtBQUssU0FBSSxVQUFVLENBQUMsVUFBVSxNQUFHLEVBQUU7WUFDekcsUUFBUSxFQUFFLFdBQVc7WUFDckIsS0FBSyxFQUFFLFVBQVUsQ0FBQyxhQUFhO1NBQ2xDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsc0NBQVc7Ozs7SUFBWCxVQUFZLE1BQWM7UUFDdEIsSUFBSTtZQUNBLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDaEQ7UUFBQyxPQUFPLEdBQUcsRUFBRTtTQUNiO0lBQ0wsQ0FBQzs7Z0JBeERKLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7Ozs7Z0JBSnhCLFlBQVk7OzsyQkFEcEI7Q0ErREMsQUExREQsSUEwREM7U0F6RFksZ0JBQWdCOzs7Ozs7SUFHckIsd0NBQW9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QW5ndWxhcnRpY3MyfSBmcm9tICdhbmd1bGFydGljczInO1xuXG5kZWNsYXJlIGxldCBnY2xvZzogYW55O1xuXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcbmV4cG9ydCBjbGFzcyBBbmd1bGFydGljc0dDTE9HIHtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgYW5ndWxhcnRpY3MyOiBBbmd1bGFydGljczIsXG4gICAgKSB7XG4gICAgfVxuXG4gICAgc3RhcnRUcmFja2luZygpIHtcbiAgICAgICAgdGhpcy5hbmd1bGFydGljczIucGFnZVRyYWNrXG4gICAgICAgICAgICAucGlwZSh0aGlzLmFuZ3VsYXJ0aWNzMi5maWx0ZXJEZXZlbG9wZXJNb2RlKCkpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKCh4KSA9PiB0aGlzLnBhZ2VUcmFjayh4LnBhdGgpKTtcbiAgICAgICAgdGhpcy5hbmd1bGFydGljczIuZXZlbnRUcmFja1xuICAgICAgICAgICAgLnBpcGUodGhpcy5hbmd1bGFydGljczIuZmlsdGVyRGV2ZWxvcGVyTW9kZSgpKVxuICAgICAgICAgICAgLnN1YnNjcmliZSgoeCkgPT4gdGhpcy5ldmVudFRyYWNrKHguYWN0aW9uLCB4LnByb3BlcnRpZXMpKTtcbiAgICAgICAgdGhpcy5hbmd1bGFydGljczIuZXhjZXB0aW9uVHJhY2tcbiAgICAgICAgICAgIC5waXBlKHRoaXMuYW5ndWxhcnRpY3MyLmZpbHRlckRldmVsb3Blck1vZGUoKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHg6IGFueSkgPT4gdGhpcy5leGNlcHRpb25UcmFjayh4KSk7XG4gICAgICAgIHRoaXMuYW5ndWxhcnRpY3MyLnNldFVzZXJuYW1lXG4gICAgICAgICAgICAucGlwZSh0aGlzLmFuZ3VsYXJ0aWNzMi5maWx0ZXJEZXZlbG9wZXJNb2RlKCkpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKCh4OiBzdHJpbmcpID0+IHRoaXMuc2V0VXNlcm5hbWUoeCkpO1xuICAgIH1cblxuICAgIHBhZ2VUcmFjayhwYXRoOiBzdHJpbmcpIHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGdjbG9nLmFkZFBhZ2VBY3Rpb24ocGF0aCk7XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZXZlbnRUcmFjayhhY3Rpb246IHN0cmluZywgcHJvcGVydGllczogYW55KSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgIGdjbG9nLmFkZFBhZ2VBY3Rpb24oYWN0aW9uLCBwcm9wZXJ0aWVzKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBleGNlcHRpb25UcmFjayhwcm9wZXJ0aWVzOiBhbnkpIHtcbiAgICAgICAgaWYgKHByb3BlcnRpZXMuZmF0YWwgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ05vIFwiZmF0YWxcIiBwcm92aWRlZCwgc2VuZGluZyB3aXRoIGZhdGFsPXRydWUnKTtcbiAgICAgICAgICAgIHByb3BlcnRpZXMuZXhGYXRhbCA9IHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBwcm9wZXJ0aWVzLmV4RGVzY3JpcHRpb24gPSBwcm9wZXJ0aWVzLmV2ZW50ID8gcHJvcGVydGllcy5ldmVudC5zdGFjayA6IHByb3BlcnRpZXMuZGVzY3JpcHRpb247XG5cbiAgICAgICAgdGhpcy5ldmVudFRyYWNrKGBFeGNlcHRpb24gdGhyb3duIGZvciAke3Byb3BlcnRpZXMuYXBwTmFtZX0gPCR7cHJvcGVydGllcy5hcHBJZH1AJHtwcm9wZXJ0aWVzLmFwcFZlcnNpb259PmAsIHtcbiAgICAgICAgICAgIGNhdGVnb3J5OiAnRXhjZXB0aW9uJyxcbiAgICAgICAgICAgIGxhYmVsOiBwcm9wZXJ0aWVzLmV4RGVzY3JpcHRpb24sXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHNldFVzZXJuYW1lKHVzZXJJZDogc3RyaW5nKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBnY2xvZy5zZXRDdXN0b21BdHRyaWJ1dGUoJ3VzZXJuYW1lJywgdXNlcklkKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgIH1cbiAgICB9XG5cbn1cbiJdfQ==