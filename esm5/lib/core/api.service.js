/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/api.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, NgModule } from '@angular/core';
import { APP_CONFIG } from '../app.config';
import { ParserService } from './parser.service';
import sha1 from 'js-sha1';
import * as i0 from "@angular/core";
var APIInterceptor = /** @class */ (function () {
    function APIInterceptor(parser) {
        this.parser = parser;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    APIInterceptor.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        // const pURL = (req.url + (this.parser.serializeRgQuery(req.params) ? '?' + this.parser.serializeRgQuery(req.params) : ''));
        /** @type {?} */
        var pURL = APP_CONFIG.api + '/' + req.urlWithParams;
        /** @type {?} */
        var responseHash;
        if (req.method.substr(0, 3) === 'GET') {
            responseHash = (pURL + APP_CONFIG.authHeaders.xUserToken);
        }
        else {
            responseHash = (pURL + (req.body ? JSON.stringify(req.body) : '') + APP_CONFIG.authHeaders.xUserToken);
        }
        /** @type {?} */
        var apiReq = req.clone({
            url: APP_CONFIG.api + "/" + req.url,
            headers: req.headers
                .set('x-service-provider', APP_CONFIG.authHeaders.xServiceProvider || '')
                .set('x-service-user-name', APP_CONFIG.authHeaders.xUserName || '')
                .set('Cache-Control', 'no-cache')
                .set('Pragma', 'no-cache')
                .set('x-service-request-hash', sha1(responseHash))
        });
        return next.handle(apiReq);
        // .pipe(
        //     catchError((error: HttpErrorResponse) => {
        //         let errorMessage: string;
        //         if (error.error instanceof ErrorEvent) {
        //             errorMessage = `Error: ${error.error.message}`;
        //         } else {
        //             errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        //         }
        //         return throwError(errorMessage);
        //     })
        // );
    };
    APIInterceptor.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    APIInterceptor.ctorParameters = function () { return [
        { type: ParserService }
    ]; };
    return APIInterceptor;
}());
export { APIInterceptor };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    APIInterceptor.prototype.parser;
}
var ApiService = /** @class */ (function () {
    function ApiService() {
    }
    /**
     * @param {?} response
     * @return {?}
     */
    ApiService.prototype.setInterceptedResponse = /**
     * @param {?} response
     * @return {?}
     */
    function (response) {
        this.interceptedResponse = response;
    };
    /**
     * @return {?}
     */
    ApiService.prototype.getInterceptedResponse = /**
     * @return {?}
     */
    function () {
        return this.interceptedResponse;
    };
    ApiService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
        { type: NgModule, args: [{
                    imports: [],
                },] }
    ];
    /** @nocollapse */ ApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ApiService_Factory() { return new ApiService(); }, token: ApiService, providedIn: "root" });
    return ApiService;
}());
export { ApiService };
if (false) {
    /** @type {?} */
    ApiService.prototype.interceptedResponse;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2FwaS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBRSxRQUFRLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDL0MsT0FBTyxJQUFJLE1BQU0sU0FBUyxDQUFDOztBQUkzQjtJQUdJLHdCQUNjLE1BQXFCO1FBQXJCLFdBQU0sR0FBTixNQUFNLENBQWU7SUFFbkMsQ0FBQzs7Ozs7O0lBRUQsa0NBQVM7Ozs7O0lBQVQsVUFBVSxHQUFxQixFQUFFLElBQWlCOzs7WUFFeEMsSUFBSSxHQUFHLFVBQVUsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxhQUFhOztZQUNqRCxZQUFvQjtRQUN4QixJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLEVBQUU7WUFDbkMsWUFBWSxHQUFHLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDN0Q7YUFBTTtZQUNILFlBQVksR0FBRyxDQUFDLElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxVQUFVLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQzFHOztZQUVLLE1BQU0sR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ3JCLEdBQUcsRUFBSyxVQUFVLENBQUMsR0FBRyxTQUFJLEdBQUcsQ0FBQyxHQUFLO1lBQ25DLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTztpQkFDZixHQUFHLENBQUMsb0JBQW9CLEVBQUUsVUFBVSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsSUFBSSxFQUFFLENBQUM7aUJBQ3hFLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxVQUFVLENBQUMsV0FBVyxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUM7aUJBQ2xFLEdBQUcsQ0FBQyxlQUFlLEVBQUUsVUFBVSxDQUFDO2lCQUNoQyxHQUFHLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQztpQkFDekIsR0FBRyxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN6RCxDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzNCLFNBQVM7UUFDVCxpREFBaUQ7UUFDakQsb0NBQW9DO1FBQ3BDLG1EQUFtRDtRQUNuRCw4REFBOEQ7UUFDOUQsbUJBQW1CO1FBQ25CLHVGQUF1RjtRQUN2RixZQUFZO1FBQ1osMkNBQTJDO1FBQzNDLFNBQVM7UUFDVCxLQUFLO0lBQ1QsQ0FBQzs7Z0JBeENKLFVBQVU7Ozs7Z0JBTEgsYUFBYTs7SUE4Q3JCLHFCQUFDO0NBQUEsQUF6Q0QsSUF5Q0M7U0F4Q1ksY0FBYzs7Ozs7O0lBR25CLGdDQUErQjs7QUF1Q3ZDO0lBQUE7S0FtQkM7Ozs7O0lBUkcsMkNBQXNCOzs7O0lBQXRCLFVBQXVCLFFBQVk7UUFDL0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFFBQVEsQ0FBQztJQUN4QyxDQUFDOzs7O0lBRUQsMkNBQXNCOzs7SUFBdEI7UUFDSSxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztJQUNwQyxDQUFDOztnQkFqQkosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjtnQkFFQSxRQUFRLFNBQUM7b0JBQ04sT0FBTyxFQUFFLEVBQUU7aUJBQ2Q7OztxQkF4REQ7Q0FxRUMsQUFuQkQsSUFtQkM7U0FYWSxVQUFVOzs7SUFDbkIseUNBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlLCBOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0FQUF9DT05GSUd9IGZyb20gJy4uL2FwcC5jb25maWcnO1xuaW1wb3J0IHtQYXJzZXJTZXJ2aWNlfSBmcm9tICcuL3BhcnNlci5zZXJ2aWNlJztcbmltcG9ydCBzaGExIGZyb20gJ2pzLXNoYTEnO1xuaW1wb3J0IHtIdHRwRXZlbnQsIEh0dHBIYW5kbGVyLCBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXF1ZXN0fSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQVBJSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCBwYXJzZXI6IFBhcnNlclNlcnZpY2VcbiAgICApIHtcbiAgICB9XG5cbiAgICBpbnRlcmNlcHQocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcbiAgICAgICAgLy8gY29uc3QgcFVSTCA9IChyZXEudXJsICsgKHRoaXMucGFyc2VyLnNlcmlhbGl6ZVJnUXVlcnkocmVxLnBhcmFtcykgPyAnPycgKyB0aGlzLnBhcnNlci5zZXJpYWxpemVSZ1F1ZXJ5KHJlcS5wYXJhbXMpIDogJycpKTtcbiAgICAgICAgY29uc3QgcFVSTCA9IEFQUF9DT05GSUcuYXBpICsgJy8nICsgcmVxLnVybFdpdGhQYXJhbXM7XG4gICAgICAgIGxldCByZXNwb25zZUhhc2g6IHN0cmluZztcbiAgICAgICAgaWYgKHJlcS5tZXRob2Quc3Vic3RyKDAsIDMpID09PSAnR0VUJykge1xuICAgICAgICAgICAgcmVzcG9uc2VIYXNoID0gKHBVUkwgKyBBUFBfQ09ORklHLmF1dGhIZWFkZXJzLnhVc2VyVG9rZW4pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmVzcG9uc2VIYXNoID0gKHBVUkwgKyAocmVxLmJvZHkgPyBKU09OLnN0cmluZ2lmeShyZXEuYm9keSkgOiAnJykgKyBBUFBfQ09ORklHLmF1dGhIZWFkZXJzLnhVc2VyVG9rZW4pO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgYXBpUmVxID0gcmVxLmNsb25lKHtcbiAgICAgICAgICAgIHVybDogYCR7QVBQX0NPTkZJRy5hcGl9LyR7cmVxLnVybH1gLFxuICAgICAgICAgICAgaGVhZGVyczogcmVxLmhlYWRlcnNcbiAgICAgICAgICAgICAgICAuc2V0KCd4LXNlcnZpY2UtcHJvdmlkZXInLCBBUFBfQ09ORklHLmF1dGhIZWFkZXJzLnhTZXJ2aWNlUHJvdmlkZXIgfHwgJycpXG4gICAgICAgICAgICAgICAgLnNldCgneC1zZXJ2aWNlLXVzZXItbmFtZScsIEFQUF9DT05GSUcuYXV0aEhlYWRlcnMueFVzZXJOYW1lIHx8ICcnKVxuICAgICAgICAgICAgICAgIC5zZXQoJ0NhY2hlLUNvbnRyb2wnLCAnbm8tY2FjaGUnKVxuICAgICAgICAgICAgICAgIC5zZXQoJ1ByYWdtYScsICduby1jYWNoZScpXG4gICAgICAgICAgICAgICAgLnNldCgneC1zZXJ2aWNlLXJlcXVlc3QtaGFzaCcsIHNoYTEocmVzcG9uc2VIYXNoKSlcbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKGFwaVJlcSk7XG4gICAgICAgIC8vIC5waXBlKFxuICAgICAgICAvLyAgICAgY2F0Y2hFcnJvcigoZXJyb3I6IEh0dHBFcnJvclJlc3BvbnNlKSA9PiB7XG4gICAgICAgIC8vICAgICAgICAgbGV0IGVycm9yTWVzc2FnZTogc3RyaW5nO1xuICAgICAgICAvLyAgICAgICAgIGlmIChlcnJvci5lcnJvciBpbnN0YW5jZW9mIEVycm9yRXZlbnQpIHtcbiAgICAgICAgLy8gICAgICAgICAgICAgZXJyb3JNZXNzYWdlID0gYEVycm9yOiAke2Vycm9yLmVycm9yLm1lc3NhZ2V9YDtcbiAgICAgICAgLy8gICAgICAgICB9IGVsc2Uge1xuICAgICAgICAvLyAgICAgICAgICAgICBlcnJvck1lc3NhZ2UgPSBgRXJyb3IgQ29kZTogJHtlcnJvci5zdGF0dXN9XFxuTWVzc2FnZTogJHtlcnJvci5tZXNzYWdlfWA7XG4gICAgICAgIC8vICAgICAgICAgfVxuICAgICAgICAvLyAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycm9yTWVzc2FnZSk7XG4gICAgICAgIC8vICAgICB9KVxuICAgICAgICAvLyApO1xuICAgIH1cbn1cblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbXSxcbn0pXG5cbmV4cG9ydCBjbGFzcyBBcGlTZXJ2aWNlIHtcbiAgICBpbnRlcmNlcHRlZFJlc3BvbnNlOiB7fTtcblxuICAgIHNldEludGVyY2VwdGVkUmVzcG9uc2UocmVzcG9uc2U6IHt9KSB7XG4gICAgICAgIHRoaXMuaW50ZXJjZXB0ZWRSZXNwb25zZSA9IHJlc3BvbnNlO1xuICAgIH1cblxuICAgIGdldEludGVyY2VwdGVkUmVzcG9uc2UoKToge30ge1xuICAgICAgICByZXR1cm4gdGhpcy5pbnRlcmNlcHRlZFJlc3BvbnNlO1xuICAgIH1cblxufVxuIl19