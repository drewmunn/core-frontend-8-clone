/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/router/router.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as fromRouter from '@ngrx/router-store';
/**
 * @record
 */
export function RouterStateUrl() { }
if (false) {
    /** @type {?} */
    RouterStateUrl.prototype.url;
    /** @type {?} */
    RouterStateUrl.prototype.queryParams;
    /** @type {?} */
    RouterStateUrl.prototype.params;
    /** @type {?} */
    RouterStateUrl.prototype.data;
}
/** @type {?} */
export var reducer = fromRouter.routerReducer;
var CustomSerializer = /** @class */ (function () {
    function CustomSerializer() {
    }
    /**
     * @param {?} routerState
     * @return {?}
     */
    CustomSerializer.prototype.serialize = /**
     * @param {?} routerState
     * @return {?}
     */
    function (routerState) {
        var url = routerState.url;
        var queryParams = routerState.root.queryParams;
        /** @type {?} */
        var state = routerState.root;
        while (state.firstChild) {
            state = state.firstChild;
        }
        var params = state.params, data = state.data;
        return { url: url, queryParams: queryParams, params: params, data: data };
    };
    return CustomSerializer;
}());
export { CustomSerializer };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGVyLnJlZHVjZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3JvdXRlci9yb3V0ZXIucmVkdWNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQU1BLE9BQU8sS0FBSyxVQUFVLE1BQU0sb0JBQW9CLENBQUM7Ozs7QUFFakQsb0NBS0M7OztJQUpDLDZCQUFZOztJQUNaLHFDQUFvQjs7SUFDcEIsZ0NBQWU7O0lBQ2YsOEJBQVU7OztBQUdaLE1BQU0sS0FBTyxPQUFPLEdBQUcsVUFBVSxDQUFDLGFBQWE7QUFFL0M7SUFBQTtJQWVBLENBQUM7Ozs7O0lBYlEsb0NBQVM7Ozs7SUFBaEIsVUFBaUIsV0FBZ0M7UUFDdkMsSUFBQSxxQkFBRztRQUNILElBQUEsMENBQVc7O1lBRWYsS0FBSyxHQUEyQixXQUFXLENBQUMsSUFBSTtRQUNwRCxPQUFPLEtBQUssQ0FBQyxVQUFVLEVBQUU7WUFDdkIsS0FBSyxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUM7U0FDMUI7UUFFTyxJQUFBLHFCQUFNLEVBQUUsaUJBQUk7UUFFcEIsT0FBTyxFQUFFLEdBQUcsS0FBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLE1BQU0sUUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUM7SUFDNUMsQ0FBQztJQUNILHVCQUFDO0FBQUQsQ0FBQyxBQWZELElBZUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsXHJcbiAgUGFyYW1zLFxyXG4gIFJvdXRlclN0YXRlU25hcHNob3QsXHJcbn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmltcG9ydCAqIGFzIGZyb21Sb3V0ZXIgZnJvbSAnQG5ncngvcm91dGVyLXN0b3JlJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgUm91dGVyU3RhdGVVcmwge1xyXG4gIHVybDogc3RyaW5nO1xyXG4gIHF1ZXJ5UGFyYW1zOiBQYXJhbXM7XHJcbiAgcGFyYW1zOiBQYXJhbXM7XHJcbiAgZGF0YTogYW55O1xyXG59XHJcblxyXG5leHBvcnQgY29uc3QgcmVkdWNlciA9IGZyb21Sb3V0ZXIucm91dGVyUmVkdWNlcjtcclxuXHJcbmV4cG9ydCBjbGFzcyBDdXN0b21TZXJpYWxpemVyXHJcbiAgaW1wbGVtZW50cyBmcm9tUm91dGVyLlJvdXRlclN0YXRlU2VyaWFsaXplcjxSb3V0ZXJTdGF0ZVVybD4ge1xyXG4gIHB1YmxpYyBzZXJpYWxpemUocm91dGVyU3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBSb3V0ZXJTdGF0ZVVybCB7XHJcbiAgICBjb25zdCB7IHVybCB9ID0gcm91dGVyU3RhdGU7XHJcbiAgICBjb25zdCB7IHF1ZXJ5UGFyYW1zIH0gPSByb3V0ZXJTdGF0ZS5yb290O1xyXG5cclxuICAgIGxldCBzdGF0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCA9IHJvdXRlclN0YXRlLnJvb3Q7XHJcbiAgICB3aGlsZSAoc3RhdGUuZmlyc3RDaGlsZCkge1xyXG4gICAgICBzdGF0ZSA9IHN0YXRlLmZpcnN0Q2hpbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgeyBwYXJhbXMsIGRhdGEgfSA9IHN0YXRlO1xyXG5cclxuICAgIHJldHVybiB7IHVybCwgcXVlcnlQYXJhbXMsIHBhcmFtcywgZGF0YSB9O1xyXG4gIH1cclxufVxyXG4iXX0=