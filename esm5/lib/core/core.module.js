/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/core.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { environment } from '../environments/environment';
import { DialogsModule } from './utils/dialogs/dialogs.module';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PopoverModule } from 'ngx-bootstrap';
import { APIInterceptor, ApiService } from './api.service';
import { Angulartics2Module } from 'angulartics2';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateService } from './pages/translate.service';
import { effects, metaReducers, reducers } from './index';
import { CustomSerializer } from './router/router.reducer';
var CoreModule = /** @class */ (function () {
    function CoreModule(parentModule, http) {
        this.http = http;
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
    CoreModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [],
                    imports: [
                        CommonModule,
                        BrowserAnimationsModule,
                        HttpClientModule,
                        StoreModule.forRoot(reducers, {
                            metaReducers: metaReducers,
                            runtimeChecks: {
                                strictStateImmutability: false,
                                strictActionImmutability: false,
                                strictStateSerializability: false,
                                strictActionSerializability: false,
                            },
                        }),
                        EffectsModule.forRoot(tslib_1.__spread(effects)),
                        StoreDevtoolsModule.instrument({
                            maxAge: 25, logOnly: environment.production,
                            actionsBlocklist: ['@ngrx/router*']
                        }),
                        StoreRouterConnectingModule.forRoot(),
                        AccordionModule.forRoot(),
                        AlertModule.forRoot(),
                        BsDropdownModule.forRoot(),
                        ButtonsModule.forRoot(),
                        CollapseModule.forRoot(),
                        ModalModule.forRoot(),
                        TooltipModule.forRoot(),
                        TabsModule.forRoot(),
                        PopoverModule.forRoot(),
                        DialogsModule,
                        ApiService,
                        Angulartics2Module.forRoot(),
                        TranslateModule.forRoot({
                            loader: {
                                provide: TranslateLoader,
                                useClass: TranslateService,
                            }
                        }),
                    ],
                    providers: [
                        {
                            provide: RouterStateSerializer,
                            useClass: CustomSerializer
                        },
                        {
                            provide: HTTP_INTERCEPTORS,
                            useClass: APIInterceptor,
                            multi: true
                        },
                    ]
                },] }
    ];
    /** @nocollapse */
    CoreModule.ctorParameters = function () { return [
        { type: CoreModule, decorators: [{ type: Optional }, { type: SkipSelf }] },
        { type: HttpClient }
    ]; };
    return CoreModule;
}());
export { CoreModule };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoreModule.prototype.http;
}
/**
 * @param {?} parentModule
 * @param {?} moduleName
 * @return {?}
 */
export function throwIfAlreadyLoaded(parentModule, moduleName) {
    if (parentModule) {
        throw new Error(moduleName + " has already been loaded. Import " + moduleName + " modules in the AppModule only.");
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2NvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUMzRCxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLHVCQUF1QixFQUFDLE1BQU0sc0NBQXNDLENBQUM7QUFDN0UsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLGFBQWEsQ0FBQztBQUN4QyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzVDLE9BQU8sRUFBQyxtQkFBbUIsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ3pELE9BQU8sRUFBQyxxQkFBcUIsRUFBRSwyQkFBMkIsRUFBQyxNQUFNLG9CQUFvQixDQUFDO0FBQ3RGLE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUN4RCxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFDaEQsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFDeEQsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHVCQUF1QixDQUFDO0FBQ3BELE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUN0RCxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFDaEQsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHVCQUF1QixDQUFDO0FBQ3BELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUN4RCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sZ0NBQWdDLENBQUM7QUFDN0QsT0FBTyxFQUFDLGlCQUFpQixFQUFFLFVBQVUsRUFBRSxnQkFBZ0IsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ3JGLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxvQkFBb0IsQ0FBQztBQUM5QyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzVDLE9BQU8sRUFBQyxjQUFjLEVBQUUsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLGNBQWMsQ0FBQztBQUNoRCxPQUFPLEVBQUMsZUFBZSxFQUFFLGVBQWUsRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQ3JFLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQzNELE9BQU8sRUFBQyxPQUFPLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBQyxNQUFNLFNBQVMsQ0FBQztBQUN4RCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUV6RDtJQTRESSxvQkFBb0MsWUFBd0IsRUFBVSxJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2xGLG9CQUFvQixDQUFDLFlBQVksRUFBRSxZQUFZLENBQUMsQ0FBQztJQUNyRCxDQUFDOztnQkE5REosUUFBUSxTQUFDO29CQUNOLFlBQVksRUFBRSxFQUFFO29CQUNoQixPQUFPLEVBQUU7d0JBQ0wsWUFBWTt3QkFDWix1QkFBdUI7d0JBQ3ZCLGdCQUFnQjt3QkFFaEIsV0FBVyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUU7NEJBQzFCLFlBQVksY0FBQTs0QkFDWixhQUFhLEVBQUU7Z0NBQ1gsdUJBQXVCLEVBQUUsS0FBSztnQ0FDOUIsd0JBQXdCLEVBQUUsS0FBSztnQ0FDL0IsMEJBQTBCLEVBQUUsS0FBSztnQ0FDakMsMkJBQTJCLEVBQUUsS0FBSzs2QkFDckM7eUJBQ0osQ0FBQzt3QkFDRixhQUFhLENBQUMsT0FBTyxrQkFBSyxPQUFPLEVBQUU7d0JBQ25DLG1CQUFtQixDQUFDLFVBQVUsQ0FBQzs0QkFDM0IsTUFBTSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsV0FBVyxDQUFDLFVBQVU7NEJBQzNDLGdCQUFnQixFQUFFLENBQUMsZUFBZSxDQUFDO3lCQUN0QyxDQUFDO3dCQUVGLDJCQUEyQixDQUFDLE9BQU8sRUFBRTt3QkFFckMsZUFBZSxDQUFDLE9BQU8sRUFBRTt3QkFDekIsV0FBVyxDQUFDLE9BQU8sRUFBRTt3QkFDckIsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO3dCQUMxQixhQUFhLENBQUMsT0FBTyxFQUFFO3dCQUN2QixjQUFjLENBQUMsT0FBTyxFQUFFO3dCQUN4QixXQUFXLENBQUMsT0FBTyxFQUFFO3dCQUNyQixhQUFhLENBQUMsT0FBTyxFQUFFO3dCQUN2QixVQUFVLENBQUMsT0FBTyxFQUFFO3dCQUNwQixhQUFhLENBQUMsT0FBTyxFQUFFO3dCQUV2QixhQUFhO3dCQUViLFVBQVU7d0JBRVYsa0JBQWtCLENBQUMsT0FBTyxFQUFFO3dCQUM1QixlQUFlLENBQUMsT0FBTyxDQUFDOzRCQUNwQixNQUFNLEVBQUU7Z0NBQ0osT0FBTyxFQUFFLGVBQWU7Z0NBQ3hCLFFBQVEsRUFBRSxnQkFBZ0I7NkJBQzdCO3lCQUNKLENBQUM7cUJBQ0w7b0JBQ0QsU0FBUyxFQUFFO3dCQUNQOzRCQUNJLE9BQU8sRUFBRSxxQkFBcUI7NEJBQzlCLFFBQVEsRUFBRSxnQkFBZ0I7eUJBQzdCO3dCQUNEOzRCQUNJLE9BQU8sRUFBRSxpQkFBaUI7NEJBQzFCLFFBQVEsRUFBRSxjQUFjOzRCQUN4QixLQUFLLEVBQUUsSUFBSTt5QkFDZDtxQkFDSjtpQkFDSjs7OztnQkFHcUQsVUFBVSx1QkFBL0MsUUFBUSxZQUFJLFFBQVE7Z0JBdEVWLFVBQVU7O0lBeUVyQyxpQkFBQztDQUFBLEFBL0RELElBK0RDO1NBSlksVUFBVTs7Ozs7O0lBQzJDLDBCQUF3Qjs7Ozs7OztBQUsxRixNQUFNLFVBQVUsb0JBQW9CLENBQUMsWUFBaUIsRUFBRSxVQUFrQjtJQUN0RSxJQUFJLFlBQVksRUFBRTtRQUNkLE1BQU0sSUFBSSxLQUFLLENBQUksVUFBVSx5Q0FBb0MsVUFBVSxvQ0FBaUMsQ0FBQyxDQUFDO0tBQ2pIO0FBQ0wsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGUsIE9wdGlvbmFsLCBTa2lwU2VsZn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQge0Jyb3dzZXJBbmltYXRpb25zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQge1N0b3JlTW9kdWxlfSBmcm9tICdAbmdyeC9zdG9yZSc7XHJcbmltcG9ydCB7RWZmZWN0c01vZHVsZX0gZnJvbSAnQG5ncngvZWZmZWN0cyc7XHJcbmltcG9ydCB7U3RvcmVEZXZ0b29sc01vZHVsZX0gZnJvbSAnQG5ncngvc3RvcmUtZGV2dG9vbHMnO1xyXG5pbXBvcnQge1JvdXRlclN0YXRlU2VyaWFsaXplciwgU3RvcmVSb3V0ZXJDb25uZWN0aW5nTW9kdWxlfSBmcm9tICdAbmdyeC9yb3V0ZXItc3RvcmUnO1xyXG5pbXBvcnQge0FjY29yZGlvbk1vZHVsZX0gZnJvbSAnbmd4LWJvb3RzdHJhcC9hY2NvcmRpb24nO1xyXG5pbXBvcnQge0FsZXJ0TW9kdWxlfSBmcm9tICduZ3gtYm9vdHN0cmFwL2FsZXJ0JztcclxuaW1wb3J0IHtCc0Ryb3Bkb3duTW9kdWxlfSBmcm9tICduZ3gtYm9vdHN0cmFwL2Ryb3Bkb3duJztcclxuaW1wb3J0IHtCdXR0b25zTW9kdWxlfSBmcm9tICduZ3gtYm9vdHN0cmFwL2J1dHRvbnMnO1xyXG5pbXBvcnQge0NvbGxhcHNlTW9kdWxlfSBmcm9tICduZ3gtYm9vdHN0cmFwL2NvbGxhcHNlJztcclxuaW1wb3J0IHtNb2RhbE1vZHVsZX0gZnJvbSAnbmd4LWJvb3RzdHJhcC9tb2RhbCc7XHJcbmltcG9ydCB7VG9vbHRpcE1vZHVsZX0gZnJvbSAnbmd4LWJvb3RzdHJhcC90b29sdGlwJztcclxuaW1wb3J0IHtlbnZpcm9ubWVudH0gZnJvbSAnLi4vZW52aXJvbm1lbnRzL2Vudmlyb25tZW50JztcclxuaW1wb3J0IHtEaWFsb2dzTW9kdWxlfSBmcm9tICcuL3V0aWxzL2RpYWxvZ3MvZGlhbG9ncy5tb2R1bGUnO1xyXG5pbXBvcnQge0hUVFBfSU5URVJDRVBUT1JTLCBIdHRwQ2xpZW50LCBIdHRwQ2xpZW50TW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7VGFic01vZHVsZX0gZnJvbSAnbmd4LWJvb3RzdHJhcC90YWJzJztcclxuaW1wb3J0IHtQb3BvdmVyTW9kdWxlfSBmcm9tICduZ3gtYm9vdHN0cmFwJztcclxuaW1wb3J0IHtBUElJbnRlcmNlcHRvciwgQXBpU2VydmljZX0gZnJvbSAnLi9hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7QW5ndWxhcnRpY3MyTW9kdWxlfSBmcm9tICdhbmd1bGFydGljczInO1xyXG5pbXBvcnQge1RyYW5zbGF0ZUxvYWRlciwgVHJhbnNsYXRlTW9kdWxlfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuaW1wb3J0IHtUcmFuc2xhdGVTZXJ2aWNlfSBmcm9tICcuL3BhZ2VzL3RyYW5zbGF0ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHtlZmZlY3RzLCBtZXRhUmVkdWNlcnMsIHJlZHVjZXJzfSBmcm9tICcuL2luZGV4JztcclxuaW1wb3J0IHtDdXN0b21TZXJpYWxpemVyfSBmcm9tICcuL3JvdXRlci9yb3V0ZXIucmVkdWNlcic7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgQnJvd3NlckFuaW1hdGlvbnNNb2R1bGUsXHJcbiAgICAgICAgSHR0cENsaWVudE1vZHVsZSxcclxuXHJcbiAgICAgICAgU3RvcmVNb2R1bGUuZm9yUm9vdChyZWR1Y2Vycywge1xyXG4gICAgICAgICAgICBtZXRhUmVkdWNlcnMsXHJcbiAgICAgICAgICAgIHJ1bnRpbWVDaGVja3M6IHtcclxuICAgICAgICAgICAgICAgIHN0cmljdFN0YXRlSW1tdXRhYmlsaXR5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHN0cmljdEFjdGlvbkltbXV0YWJpbGl0eTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBzdHJpY3RTdGF0ZVNlcmlhbGl6YWJpbGl0eTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBzdHJpY3RBY3Rpb25TZXJpYWxpemFiaWxpdHk6IGZhbHNlLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIEVmZmVjdHNNb2R1bGUuZm9yUm9vdChbLi4uZWZmZWN0c10pLFxyXG4gICAgICAgIFN0b3JlRGV2dG9vbHNNb2R1bGUuaW5zdHJ1bWVudCh7XHJcbiAgICAgICAgICAgIG1heEFnZTogMjUsIGxvZ09ubHk6IGVudmlyb25tZW50LnByb2R1Y3Rpb24sXHJcbiAgICAgICAgICAgIGFjdGlvbnNCbG9ja2xpc3Q6IFsnQG5ncngvcm91dGVyKiddXHJcbiAgICAgICAgfSksXHJcblxyXG4gICAgICAgIFN0b3JlUm91dGVyQ29ubmVjdGluZ01vZHVsZS5mb3JSb290KCksXHJcblxyXG4gICAgICAgIEFjY29yZGlvbk1vZHVsZS5mb3JSb290KCksXHJcbiAgICAgICAgQWxlcnRNb2R1bGUuZm9yUm9vdCgpLFxyXG4gICAgICAgIEJzRHJvcGRvd25Nb2R1bGUuZm9yUm9vdCgpLFxyXG4gICAgICAgIEJ1dHRvbnNNb2R1bGUuZm9yUm9vdCgpLFxyXG4gICAgICAgIENvbGxhcHNlTW9kdWxlLmZvclJvb3QoKSxcclxuICAgICAgICBNb2RhbE1vZHVsZS5mb3JSb290KCksXHJcbiAgICAgICAgVG9vbHRpcE1vZHVsZS5mb3JSb290KCksXHJcbiAgICAgICAgVGFic01vZHVsZS5mb3JSb290KCksXHJcbiAgICAgICAgUG9wb3Zlck1vZHVsZS5mb3JSb290KCksXHJcblxyXG4gICAgICAgIERpYWxvZ3NNb2R1bGUsXHJcblxyXG4gICAgICAgIEFwaVNlcnZpY2UsXHJcblxyXG4gICAgICAgIEFuZ3VsYXJ0aWNzMk1vZHVsZS5mb3JSb290KCksXHJcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlLmZvclJvb3Qoe1xyXG4gICAgICAgICAgICBsb2FkZXI6IHtcclxuICAgICAgICAgICAgICAgIHByb3ZpZGU6IFRyYW5zbGF0ZUxvYWRlcixcclxuICAgICAgICAgICAgICAgIHVzZUNsYXNzOiBUcmFuc2xhdGVTZXJ2aWNlLFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSksXHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBwcm92aWRlOiBSb3V0ZXJTdGF0ZVNlcmlhbGl6ZXIsXHJcbiAgICAgICAgICAgIHVzZUNsYXNzOiBDdXN0b21TZXJpYWxpemVyXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxyXG4gICAgICAgICAgICB1c2VDbGFzczogQVBJSW50ZXJjZXB0b3IsXHJcbiAgICAgICAgICAgIG11bHRpOiB0cnVlXHJcbiAgICAgICAgfSxcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDb3JlTW9kdWxlIHtcclxuICAgIGNvbnN0cnVjdG9yKEBPcHRpb25hbCgpIEBTa2lwU2VsZigpIHBhcmVudE1vZHVsZTogQ29yZU1vZHVsZSwgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7XHJcbiAgICAgICAgdGhyb3dJZkFscmVhZHlMb2FkZWQocGFyZW50TW9kdWxlLCAnQ29yZU1vZHVsZScpO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gdGhyb3dJZkFscmVhZHlMb2FkZWQocGFyZW50TW9kdWxlOiBhbnksIG1vZHVsZU5hbWU6IHN0cmluZykge1xyXG4gICAgaWYgKHBhcmVudE1vZHVsZSkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihgJHttb2R1bGVOYW1lfSBoYXMgYWxyZWFkeSBiZWVuIGxvYWRlZC4gSW1wb3J0ICR7bW9kdWxlTmFtZX0gbW9kdWxlcyBpbiB0aGUgQXBwTW9kdWxlIG9ubHkuYCk7XHJcbiAgICB9XHJcbn1cclxuXHJcbiJdfQ==