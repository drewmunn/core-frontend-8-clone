/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/translate.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { CacheService } from '../cache.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common/http";
var TranslateService = /** @class */ (function (_super) {
    tslib_1.__extends(TranslateService, _super);
    function TranslateService(router, api) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this.api = api;
        _this.defaultLanguage = 'en_GB';
        _this.refresh();
        return _this;
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    TranslateService.prototype.getTranslation = /**
     * @param {?} lang
     * @return {?}
     */
    function (lang) {
        this.translations = this.getTranslations(lang).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return response.data;
        })));
        return (/** @type {?} */ (this.translations));
    };
    /**
     * @return {?}
     */
    TranslateService.prototype.getLanguages = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.objectLoadObservables$.languages) {
            return this.objectLoadObservables$.languages;
        }
        this.objectLoadObservables$.languages = new ReplaySubject();
        this.api.get('languages').subscribe((/**
         * @param {?} languages
         * @return {?}
         */
        function (languages) {
            sessionStorage.setItem('languages', JSON.stringify(languages));
            _this.objectLoadObservables$.languages.next(languages);
        }));
        return this.objectLoadObservables$.languages;
    };
    /**
     * @param {?} languageCode
     * @return {?}
     */
    TranslateService.prototype.getTranslations = /**
     * @param {?} languageCode
     * @return {?}
     */
    function (languageCode) {
        // if (this.getFromStorage('translations:' + languageCode)) {
        //     const translations = JSON.parse(this.getFromStorage('translations:' + languageCode) as string);
        //     if (!translations.timestamp) {
        //         return this.loadTranslations(languageCode);
        //     }
        //     let observables = [];
        //     if (!this.cacheProbeTimestamp.translations) {
        //         if (!this.cacheProbePromises$.translations) {
        //             this.cacheProbePromises$.translations = this.restangular.one('cache-probe').get({q: 'translations'}).pipe(
        //                 map((response) => {
        //                     this.cacheProbeTimestamp.translations = response as number;
        //                 })
        //             );
        //         }
        //     }
        //     observables.push(this.cacheProbePromises$.translations);
        //     return forkJoin(observables).subscribe(response => {
        //         if (!this.cacheProbeTimestamp.translations || translation.timestamp < this.cacheProbeTimestamp.translations) {
        //             return this.loadTranslations(languageCode);
        //         }
        //
        //         return deferred.promise;
        //     });
        // }
        return this.loadTranslations(languageCode);
    };
    /**
     * @param {?} languageCode
     * @return {?}
     */
    TranslateService.prototype.loadTranslations = /**
     * @param {?} languageCode
     * @return {?}
     */
    function (languageCode) {
        var _this = this;
        if (this.objectLoadObservables$['translations:' + languageCode]) {
            return this.objectLoadObservables$['translations:' + languageCode];
        }
        this.objectLoadObservables$['translations:' + languageCode] = this.api.get('translations', {
            params: { languageCode: languageCode },
            observe: 'response'
        }).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            var e_1, _a;
            /** @type {?} */
            var newServiceInfo = JSON.parse(response.headers.get('x-service-info'));
            /** @type {?} */
            var translations = {
                timestamp: _this.cacheProbeTimestamp.translations || newServiceInfo.timestamp,
                data: {}
            };
            try {
                for (var _b = tslib_1.__values((/** @type {?} */ (response.body))), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var translation = _c.value;
                    translations.data[translation.dataKey] = translation.dataValue;
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            try {
                sessionStorage.setItem('translations:' + languageCode, JSON.stringify(translations));
            }
            catch (e) {
            }
            try {
                localStorage.setItem('translations:' + languageCode, JSON.stringify(translations));
            }
            catch (e) {
            }
            return translations;
        })));
        return this.objectLoadObservables$['translations:' + languageCode];
    };
    /**
     * @return {?}
     */
    TranslateService.prototype.getDefaultLanguage = /**
     * @return {?}
     */
    function () {
        if (sessionStorage.getItem('defaultLanguage')) {
            return sessionStorage.getItem('defaultLanguage');
        }
        if (localStorage.getItem('defaultLanguage')) {
            return localStorage.getItem('defaultLanguage');
        }
        return this.defaultLanguage;
    };
    /**
     * @param {?} languageCode
     * @return {?}
     */
    TranslateService.prototype.setDefaultLanguage = /**
     * @param {?} languageCode
     * @return {?}
     */
    function (languageCode) {
        try {
            sessionStorage.setItem('defaultLanguage', languageCode);
            localStorage.setItem('defaultLanguage', languageCode);
        }
        catch (_a) {
        }
    };
    /**
     * @return {?}
     */
    TranslateService.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        console.log('Destroy translate');
    };
    TranslateService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    TranslateService.ctorParameters = function () { return [
        { type: Router },
        { type: HttpClient }
    ]; };
    /** @nocollapse */ TranslateService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TranslateService_Factory() { return new TranslateService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.HttpClient)); }, token: TranslateService, providedIn: "root" });
    return TranslateService;
}(CacheService));
export { TranslateService };
if (false) {
    /** @type {?} */
    TranslateService.prototype.translations;
    /** @type {?} */
    TranslateService.prototype.defaultLanguage;
    /**
     * @type {?}
     * @protected
     */
    TranslateService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    TranslateService.prototype.api;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3BhZ2VzL3RyYW5zbGF0ZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQVksTUFBTSxlQUFlLENBQUM7QUFFcEQsT0FBTyxFQUFhLGFBQWEsRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUMvQyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDOUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBQ25DLE9BQU8sRUFBQyxNQUFNLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUN2QyxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sc0JBQXNCLENBQUM7Ozs7QUFFaEQ7SUFJc0MsNENBQVk7SUFFOUMsMEJBQ2MsTUFBYyxFQUNkLEdBQWU7UUFGN0IsWUFJSSxpQkFBTyxTQUVWO1FBTGEsWUFBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLFNBQUcsR0FBSCxHQUFHLENBQVk7UUFPN0IscUJBQWUsR0FBRyxPQUFPLENBQUM7UUFKdEIsS0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDOztJQUNuQixDQUFDOzs7OztJQUtELHlDQUFjOzs7O0lBQWQsVUFBZSxJQUFZO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQy9DLEdBQUc7Ozs7UUFBQyxVQUFDLFFBQXVCO1lBQ3hCLE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQztRQUN6QixDQUFDLEVBQUMsQ0FBQyxDQUFDO1FBRVIsT0FBTyxtQkFBQSxJQUFJLENBQUMsWUFBWSxFQUFtQixDQUFDO0lBQ2hELENBQUM7Ozs7SUFFRCx1Q0FBWTs7O0lBQVo7UUFBQSxpQkFVQztRQVRHLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRTtZQUN2QyxPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7U0FDaEQ7UUFDRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxHQUFHLElBQUksYUFBYSxFQUFZLENBQUM7UUFDdEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsU0FBUztZQUN6QyxjQUFjLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDL0QsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDMUQsQ0FBQyxFQUFDLENBQUM7UUFDSCxPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7SUFDakQsQ0FBQzs7Ozs7SUFFRCwwQ0FBZTs7OztJQUFmLFVBQWdCLFlBQVk7UUFDeEIsNkRBQTZEO1FBQzdELHNHQUFzRztRQUN0RyxxQ0FBcUM7UUFDckMsc0RBQXNEO1FBQ3RELFFBQVE7UUFDUiw0QkFBNEI7UUFDNUIsb0RBQW9EO1FBQ3BELHdEQUF3RDtRQUN4RCx5SEFBeUg7UUFDekgsc0NBQXNDO1FBQ3RDLGtGQUFrRjtRQUNsRixxQkFBcUI7UUFDckIsaUJBQWlCO1FBQ2pCLFlBQVk7UUFDWixRQUFRO1FBQ1IsK0RBQStEO1FBQy9ELDJEQUEyRDtRQUMzRCx5SEFBeUg7UUFDekgsMERBQTBEO1FBQzFELFlBQVk7UUFDWixFQUFFO1FBQ0YsbUNBQW1DO1FBQ25DLFVBQVU7UUFDVixJQUFJO1FBRUosT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Ozs7SUFFRCwyQ0FBZ0I7Ozs7SUFBaEIsVUFBaUIsWUFBWTtRQUE3QixpQkFnQ0M7UUEvQkcsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxHQUFHLFlBQVksQ0FBQyxFQUFFO1lBQzdELE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsR0FBRyxZQUFZLENBQUMsQ0FBQztTQUN0RTtRQUNELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLEdBQUcsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUNyRjtZQUNJLE1BQU0sRUFBRSxFQUFDLFlBQVksY0FBQSxFQUFDO1lBQ3RCLE9BQU8sRUFBRSxVQUFVO1NBQ3RCLENBQUMsQ0FBQyxJQUFJLENBQ1AsR0FBRzs7OztRQUFDLFVBQUEsUUFBUTs7O2dCQUNGLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7O2dCQUNuRSxZQUFZLEdBQUc7Z0JBQ2pCLFNBQVMsRUFBRSxLQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxJQUFJLGNBQWMsQ0FBQyxTQUFTO2dCQUM1RSxJQUFJLEVBQUUsRUFBRTthQUNYOztnQkFDRCwrQkFBMEIsbUJBQUEsUUFBUSxDQUFDLElBQUksRUFBTyw2Q0FBRTtvQkFBM0MsSUFBTSxXQUFXLFdBQUE7b0JBQ2xCLFlBQVksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUM7aUJBQ2xFOzs7Ozs7Ozs7WUFDRCxJQUFJO2dCQUNBLGNBQWMsQ0FBQyxPQUFPLENBQUMsZUFBZSxHQUFHLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7YUFDeEY7WUFBQyxPQUFPLENBQUMsRUFBRTthQUNYO1lBQ0QsSUFBSTtnQkFDQSxZQUFZLENBQUMsT0FBTyxDQUFDLGVBQWUsR0FBRyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2FBQ3RGO1lBQUMsT0FBTyxDQUFDLEVBQUU7YUFDWDtZQUVELE9BQU8sWUFBWSxDQUFDO1FBQ3hCLENBQUMsRUFBQyxDQUNMLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLEdBQUcsWUFBWSxDQUFDLENBQUM7SUFDdkUsQ0FBQzs7OztJQUVELDZDQUFrQjs7O0lBQWxCO1FBQ0ksSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEVBQUU7WUFDM0MsT0FBTyxjQUFjLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDcEQ7UUFDRCxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsRUFBRTtZQUN6QyxPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUNsRDtRQUNELE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUNoQyxDQUFDOzs7OztJQUVELDZDQUFrQjs7OztJQUFsQixVQUFtQixZQUFvQjtRQUNuQyxJQUFJO1lBQ0EsY0FBYyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxZQUFZLENBQUMsQ0FBQztZQUN4RCxZQUFZLENBQUMsT0FBTyxDQUFDLGlCQUFpQixFQUFFLFlBQVksQ0FBQyxDQUFDO1NBQ3pEO1FBQUMsV0FBTTtTQUNQO0lBQ0wsQ0FBQzs7OztJQUVELHNDQUFXOzs7SUFBWDtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUNyQyxDQUFDOztnQkF6SEosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFMTyxNQUFNO2dCQUNOLFVBQVU7OzsyQkFObEI7Q0FtSUMsQUEzSEQsQ0FJc0MsWUFBWSxHQXVIakQ7U0F2SFksZ0JBQWdCOzs7SUFVekIsd0NBQThCOztJQUM5QiwyQ0FBMEI7Ozs7O0lBUnRCLGtDQUF3Qjs7Ozs7SUFDeEIsK0JBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlLCBPbkRlc3Ryb3l9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtUcmFuc2xhdGVMb2FkZXJ9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHtPYnNlcnZhYmxlLCBSZXBsYXlTdWJqZWN0fSBmcm9tICdyeGpzJztcbmltcG9ydCB7Q2FjaGVTZXJ2aWNlfSBmcm9tICcuLi9jYWNoZS5zZXJ2aWNlJztcbmltcG9ydCB7bWFwfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQge1JvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7SHR0cENsaWVudH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5leHBvcnQgY2xhc3MgVHJhbnNsYXRlU2VydmljZSBleHRlbmRzIENhY2hlU2VydmljZSBpbXBsZW1lbnRzIFRyYW5zbGF0ZUxvYWRlciwgT25EZXN0cm95IHtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgICAgIHByb3RlY3RlZCBhcGk6IEh0dHBDbGllbnQsXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgICAgIHRoaXMucmVmcmVzaCgpO1xuICAgIH1cblxuICAgIHRyYW5zbGF0aW9uczogT2JzZXJ2YWJsZTxhbnk+O1xuICAgIGRlZmF1bHRMYW5ndWFnZSA9ICdlbl9HQic7XG5cbiAgICBnZXRUcmFuc2xhdGlvbihsYW5nOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgICAgICB0aGlzLnRyYW5zbGF0aW9ucyA9IHRoaXMuZ2V0VHJhbnNsYXRpb25zKGxhbmcpLnBpcGUoXG4gICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiB7IGRhdGE6IGFueSB9KSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB9KSk7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRpb25zIGFzIE9ic2VydmFibGU8YW55PjtcbiAgICB9XG5cbiAgICBnZXRMYW5ndWFnZXMoKSB7XG4gICAgICAgIGlmICh0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyQubGFuZ3VhZ2VzKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkLmxhbmd1YWdlcztcbiAgICAgICAgfVxuICAgICAgICB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyQubGFuZ3VhZ2VzID0gbmV3IFJlcGxheVN1YmplY3Q8UmVzcG9uc2U+KCk7XG4gICAgICAgIHRoaXMuYXBpLmdldCgnbGFuZ3VhZ2VzJykuc3Vic2NyaWJlKGxhbmd1YWdlcyA9PiB7XG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdsYW5ndWFnZXMnLCBKU09OLnN0cmluZ2lmeShsYW5ndWFnZXMpKTtcbiAgICAgICAgICAgIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJC5sYW5ndWFnZXMubmV4dChsYW5ndWFnZXMpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJC5sYW5ndWFnZXM7XG4gICAgfVxuXG4gICAgZ2V0VHJhbnNsYXRpb25zKGxhbmd1YWdlQ29kZSkge1xuICAgICAgICAvLyBpZiAodGhpcy5nZXRGcm9tU3RvcmFnZSgndHJhbnNsYXRpb25zOicgKyBsYW5ndWFnZUNvZGUpKSB7XG4gICAgICAgIC8vICAgICBjb25zdCB0cmFuc2xhdGlvbnMgPSBKU09OLnBhcnNlKHRoaXMuZ2V0RnJvbVN0b3JhZ2UoJ3RyYW5zbGF0aW9uczonICsgbGFuZ3VhZ2VDb2RlKSBhcyBzdHJpbmcpO1xuICAgICAgICAvLyAgICAgaWYgKCF0cmFuc2xhdGlvbnMudGltZXN0YW1wKSB7XG4gICAgICAgIC8vICAgICAgICAgcmV0dXJuIHRoaXMubG9hZFRyYW5zbGF0aW9ucyhsYW5ndWFnZUNvZGUpO1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyAgICAgbGV0IG9ic2VydmFibGVzID0gW107XG4gICAgICAgIC8vICAgICBpZiAoIXRoaXMuY2FjaGVQcm9iZVRpbWVzdGFtcC50cmFuc2xhdGlvbnMpIHtcbiAgICAgICAgLy8gICAgICAgICBpZiAoIXRoaXMuY2FjaGVQcm9iZVByb21pc2VzJC50cmFuc2xhdGlvbnMpIHtcbiAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5jYWNoZVByb2JlUHJvbWlzZXMkLnRyYW5zbGF0aW9ucyA9IHRoaXMucmVzdGFuZ3VsYXIub25lKCdjYWNoZS1wcm9iZScpLmdldCh7cTogJ3RyYW5zbGF0aW9ucyd9KS5waXBlKFxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgbWFwKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAvLyAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2FjaGVQcm9iZVRpbWVzdGFtcC50cmFuc2xhdGlvbnMgPSByZXNwb25zZSBhcyBudW1iZXI7XG4gICAgICAgIC8vICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAvLyAgICAgICAgICAgICApO1xuICAgICAgICAvLyAgICAgICAgIH1cbiAgICAgICAgLy8gICAgIH1cbiAgICAgICAgLy8gICAgIG9ic2VydmFibGVzLnB1c2godGhpcy5jYWNoZVByb2JlUHJvbWlzZXMkLnRyYW5zbGF0aW9ucyk7XG4gICAgICAgIC8vICAgICByZXR1cm4gZm9ya0pvaW4ob2JzZXJ2YWJsZXMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XG4gICAgICAgIC8vICAgICAgICAgaWYgKCF0aGlzLmNhY2hlUHJvYmVUaW1lc3RhbXAudHJhbnNsYXRpb25zIHx8IHRyYW5zbGF0aW9uLnRpbWVzdGFtcCA8IHRoaXMuY2FjaGVQcm9iZVRpbWVzdGFtcC50cmFuc2xhdGlvbnMpIHtcbiAgICAgICAgLy8gICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9hZFRyYW5zbGF0aW9ucyhsYW5ndWFnZUNvZGUpO1xuICAgICAgICAvLyAgICAgICAgIH1cbiAgICAgICAgLy9cbiAgICAgICAgLy8gICAgICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICAgICAgLy8gICAgIH0pO1xuICAgICAgICAvLyB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMubG9hZFRyYW5zbGF0aW9ucyhsYW5ndWFnZUNvZGUpO1xuICAgIH1cblxuICAgIGxvYWRUcmFuc2xhdGlvbnMobGFuZ3VhZ2VDb2RlKSB7XG4gICAgICAgIGlmICh0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbJ3RyYW5zbGF0aW9uczonICsgbGFuZ3VhZ2VDb2RlXSkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFsndHJhbnNsYXRpb25zOicgKyBsYW5ndWFnZUNvZGVdO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFsndHJhbnNsYXRpb25zOicgKyBsYW5ndWFnZUNvZGVdID0gdGhpcy5hcGkuZ2V0KCd0cmFuc2xhdGlvbnMnLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHBhcmFtczoge2xhbmd1YWdlQ29kZX0sXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZTogJ3Jlc3BvbnNlJ1xuICAgICAgICAgICAgfSkucGlwZShcbiAgICAgICAgICAgIG1hcChyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgbmV3U2VydmljZUluZm8gPSBKU09OLnBhcnNlKHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCd4LXNlcnZpY2UtaW5mbycpKTtcbiAgICAgICAgICAgICAgICBjb25zdCB0cmFuc2xhdGlvbnMgPSB7XG4gICAgICAgICAgICAgICAgICAgIHRpbWVzdGFtcDogdGhpcy5jYWNoZVByb2JlVGltZXN0YW1wLnRyYW5zbGF0aW9ucyB8fCBuZXdTZXJ2aWNlSW5mby50aW1lc3RhbXAsXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHt9XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IHRyYW5zbGF0aW9uIG9mIHJlc3BvbnNlLmJvZHkgYXMgYW55KSB7XG4gICAgICAgICAgICAgICAgICAgIHRyYW5zbGF0aW9ucy5kYXRhW3RyYW5zbGF0aW9uLmRhdGFLZXldID0gdHJhbnNsYXRpb24uZGF0YVZhbHVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCd0cmFuc2xhdGlvbnM6JyArIGxhbmd1YWdlQ29kZSwgSlNPTi5zdHJpbmdpZnkodHJhbnNsYXRpb25zKSk7XG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgndHJhbnNsYXRpb25zOicgKyBsYW5ndWFnZUNvZGUsIEpTT04uc3RyaW5naWZ5KHRyYW5zbGF0aW9ucykpO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gdHJhbnNsYXRpb25zO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkWyd0cmFuc2xhdGlvbnM6JyArIGxhbmd1YWdlQ29kZV07XG4gICAgfVxuXG4gICAgZ2V0RGVmYXVsdExhbmd1YWdlKCkge1xuICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSgnZGVmYXVsdExhbmd1YWdlJykpIHtcbiAgICAgICAgICAgIHJldHVybiBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKCdkZWZhdWx0TGFuZ3VhZ2UnKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2RlZmF1bHRMYW5ndWFnZScpKSB7XG4gICAgICAgICAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2RlZmF1bHRMYW5ndWFnZScpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHRMYW5ndWFnZTtcbiAgICB9XG5cbiAgICBzZXREZWZhdWx0TGFuZ3VhZ2UobGFuZ3VhZ2VDb2RlOiBzdHJpbmcpIHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oJ2RlZmF1bHRMYW5ndWFnZScsIGxhbmd1YWdlQ29kZSk7XG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnZGVmYXVsdExhbmd1YWdlJywgbGFuZ3VhZ2VDb2RlKTtcbiAgICAgICAgfSBjYXRjaCB7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0Rlc3Ryb3kgdHJhbnNsYXRlJyk7XG4gICAgfVxuXG59XG4iXX0=