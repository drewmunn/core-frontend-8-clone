/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/page.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { CacheService } from '../cache.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs';
import * as _ from 'lodash';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common/http";
var PageService = /** @class */ (function (_super) {
    tslib_1.__extends(PageService, _super);
    function PageService(router, api) {
        var _this = _super.call(this, router, api) || this;
        _this.router = router;
        _this.api = api;
        return _this;
    }
    /**
     * @return {?}
     */
    PageService.prototype.refresh = /**
     * @return {?}
     */
    function () {
        _super.prototype.refresh.call(this);
        this.page = false;
        this.breadcrumbs = {};
    };
    /**
     * @param {?} page
     * @return {?}
     */
    PageService.prototype.setCurrentPage = /**
     * @param {?} page
     * @return {?}
     */
    function (page) {
        this.page = page;
    };
    /**
     * @return {?}
     */
    PageService.prototype.getCurrentPage = /**
     * @return {?}
     */
    function () {
        return this.page;
    };
    /**
     * @param {?=} headers
     * @return {?}
     */
    PageService.prototype.getAll = /**
     * @param {?=} headers
     * @return {?}
     */
    function (headers) {
        return this.getListThroughCache('pages', {}, headers || {});
    };
    /**
     * @param {?} identifier
     * @param {?=} blockInteraction
     * @param {?=} headers
     * @return {?}
     */
    PageService.prototype.getPage = /**
     * @param {?} identifier
     * @param {?=} blockInteraction
     * @param {?=} headers
     * @return {?}
     */
    function (identifier, blockInteraction, headers) {
        /** @type {?} */
        var cacheKey = this.createCacheKey(['page', identifier, headers]);
        if (!blockInteraction && this.objectLoadObservables$[cacheKey]) {
            this.api.get('page/' + identifier, { headers: headers || {} }).subscribe(); // Trigger interaction
            return this.objectLoadObservables$[cacheKey];
        }
        else if (blockInteraction) {
            headers = Object.assign({}, (headers || {}), { 'x-service-info': JSON.stringify({ blockInteraction: true }) });
        }
        this.objectLoadObservables$[cacheKey] = this.getOneThroughCache('page', identifier, headers, cacheKey);
        return this.objectLoadObservables$[cacheKey];
    };
    /**
     * @param {?=} headers
     * @return {?}
     */
    PageService.prototype.getPages = /**
     * @param {?=} headers
     * @return {?}
     */
    function (headers) {
        return this.getListThroughCache('pages', {}, headers || {});
    };
    /**
     * @param {?} blockId
     * @return {?}
     */
    PageService.prototype.getBlock = /**
     * @param {?} blockId
     * @return {?}
     */
    function (blockId) {
        var _this = this;
        try {
            /** @type {?} */
            var observables = [];
            if (this.getFromStorage('block:' + blockId) !== false) {
                /** @type {?} */
                var block_1 = JSON.parse((/** @type {?} */ (this.getFromStorage('block:' + blockId))));
                if (!block_1.timestamp) {
                    return this.loadBlock(blockId);
                }
                if (!this.cacheProbeTimestamp.blocks) {
                    this.cacheProbePromises$.blocks = this.api.get('cache-probe', { params: { q: 'widget' } }).pipe(map((/**
                     * @param {?} response
                     * @return {?}
                     */
                    function (response) {
                        _this.cacheProbeTimestamp.blocks = (/** @type {?} */ (response));
                    })));
                    observables.push(this.cacheProbePromises$.blocks);
                }
                return forkJoin(observables).toPromise().then((/**
                 * @param {?} response
                 * @return {?}
                 */
                function (response) {
                    if (!_this.cacheProbeTimestamp.blocks || block_1.timestamp < _this.cacheProbeTimestamp.blocks) {
                        return _this.loadBlock(blockId);
                    }
                    return block_1;
                }));
            }
        }
        catch (err) {
        }
        return this.loadBlock(blockId);
    };
    /**
     * @param {?} blockId
     * @return {?}
     */
    PageService.prototype.loadBlock = /**
     * @param {?} blockId
     * @return {?}
     */
    function (blockId) {
        if (this.objectLoadObservables$['block:' + blockId]) {
            return this.objectLoadObservables$['block:' + blockId];
        }
        this.objectLoadObservables$['block:' + blockId] = this.api.get('widget/' + blockId)
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            /** @type {?} */
            var block = response;
            try {
                localStorage.setItem('block:' + blockId, JSON.stringify(block));
            }
            catch (e) {
            }
            try {
                sessionStorage.setItem('block:' + blockId, JSON.stringify(block));
            }
            catch (e) {
            }
            return block;
        }))).toPromise();
        return this.objectLoadObservables$['block:' + blockId];
    };
    /**
     * @param {?} blocks
     * @return {?}
     */
    PageService.prototype.sortBlocks = /**
     * @param {?} blocks
     * @return {?}
     */
    function (blocks) {
        var e_1, _a;
        blocks.content.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        function (a, b) { return (a.data.row < b.data.row) ? 1 : -1; }))
            .sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        function (a, b) { return (a.data.col < b.data.col) ? 1 : -1; }));
        try {
            for (var _b = tslib_1.__values(blocks.content), _c = _b.next(); !_c.done; _c = _b.next()) {
                var subBlocks = _c.value;
                this.sortBlocks(subBlocks);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    /**
     * @param {?} requestedPageId
     * @return {?}
     */
    PageService.prototype.getBreadcrumbs = /**
     * @param {?} requestedPageId
     * @return {?}
     */
    function (requestedPageId) {
        var _this = this;
        return this.getPages().pipe(map((/**
         * @param {?} pages
         * @return {?}
         */
        function (pages) {
            var e_2, _a;
            if (_.isEmpty(_this.breadcrumbs)) {
                /** @type {?} */
                var extractIds_1 = (/**
                 * @param {?} page
                 * @param {?} parents
                 * @return {?}
                 */
                function (page, parents) {
                    var e_3, _a;
                    /** @type {?} */
                    var pageMin = _.omit(page, ['children', 'createdBy', 'dateCreated', 'displayOrder', 'groups',
                        'hidden', 'lastUpdated', 'permissions', 'tags', 'widgets']);
                    pageMin.parents = parents;
                    _this.breadcrumbs[page.id] = page;
                    try {
                        for (var _b = tslib_1.__values(page.children), _c = _b.next(); !_c.done; _c = _b.next()) {
                            var child = _c.value;
                            extractIds_1(child, parents.concat([pageMin]));
                        }
                    }
                    catch (e_3_1) { e_3 = { error: e_3_1 }; }
                    finally {
                        try {
                            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                        }
                        finally { if (e_3) throw e_3.error; }
                    }
                });
                try {
                    for (var pages_1 = tslib_1.__values(pages), pages_1_1 = pages_1.next(); !pages_1_1.done; pages_1_1 = pages_1.next()) {
                        var page = pages_1_1.value;
                        extractIds_1(page, []);
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (pages_1_1 && !pages_1_1.done && (_a = pages_1.return)) _a.call(pages_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
            return _this.breadcrumbs[requestedPageId] ? _this.breadcrumbs[requestedPageId] : [];
        })));
    };
    /**
     * @param {?} query
     * @return {?}
     */
    PageService.prototype.performSearch = /**
     * @param {?} query
     * @return {?}
     */
    function (query) {
        console.log(query);
    };
    PageService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    PageService.ctorParameters = function () { return [
        { type: Router },
        { type: HttpClient }
    ]; };
    /** @nocollapse */ PageService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PageService_Factory() { return new PageService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.HttpClient)); }, token: PageService, providedIn: "root" });
    return PageService;
}(CacheService));
export { PageService };
if (false) {
    /** @type {?} */
    PageService.prototype.page;
    /** @type {?} */
    PageService.prototype.breadcrumbs;
    /**
     * @type {?}
     * @protected
     */
    PageService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    PageService.prototype.api;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9wYWdlcy9wYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDOUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBQ25DLE9BQU8sRUFBQyxNQUFNLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUN2QyxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sc0JBQXNCLENBQUM7QUFDaEQsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUM5QixPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQzs7OztBQUU1QjtJQUlpQyx1Q0FBWTtJQUt6QyxxQkFDYyxNQUFjLEVBQ2QsR0FBZTtRQUY3QixZQUlJLGtCQUNJLE1BQU0sRUFDTixHQUFHLENBQ04sU0FDSjtRQVBhLFlBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxTQUFHLEdBQUgsR0FBRyxDQUFZOztJQU03QixDQUFDOzs7O0lBRUQsNkJBQU87OztJQUFQO1FBQ0ksaUJBQU0sT0FBTyxXQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7UUFDbEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFFRCxvQ0FBYzs7OztJQUFkLFVBQWUsSUFBUztRQUNwQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUNyQixDQUFDOzs7O0lBRUQsb0NBQWM7OztJQUFkO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRUQsNEJBQU07Ozs7SUFBTixVQUFPLE9BQVk7UUFDZixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsRUFBRSxFQUFFLE9BQU8sSUFBSSxFQUFFLENBQUMsQ0FBQztJQUNoRSxDQUFDOzs7Ozs7O0lBRUQsNkJBQU87Ozs7OztJQUFQLFVBQVEsVUFBVSxFQUFFLGdCQUEwQixFQUFFLE9BQVk7O1lBQ2xELFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzVELElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBRyxVQUFVLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxJQUFJLEVBQUUsRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxzQkFBc0I7WUFDaEcsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDaEQ7YUFBTSxJQUFJLGdCQUFnQixFQUFFO1lBQ3pCLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBQyxnQkFBZ0IsRUFBRSxJQUFJLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQztTQUM5RztRQUNELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDdkcsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDakQsQ0FBQzs7Ozs7SUFFRCw4QkFBUTs7OztJQUFSLFVBQVMsT0FBWTtRQUNqQixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsRUFBRSxFQUFFLE9BQU8sSUFBSSxFQUFFLENBQUMsQ0FBQztJQUNoRSxDQUFDOzs7OztJQUVELDhCQUFROzs7O0lBQVIsVUFBUyxPQUFPO1FBQWhCLGlCQTBCQztRQXpCRyxJQUFJOztnQkFDTSxXQUFXLEdBQUcsRUFBRTtZQUN0QixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxLQUFLLEtBQUssRUFBRTs7b0JBQzdDLE9BQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFBLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxFQUFVLENBQUM7Z0JBQzNFLElBQUksQ0FBQyxPQUFLLENBQUMsU0FBUyxFQUFFO29CQUNsQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ2xDO2dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFO29CQUNsQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxFQUFDLE1BQU0sRUFBRSxFQUFDLENBQUMsRUFBRSxRQUFRLEVBQUMsRUFBQyxDQUFDLENBQUMsSUFBSSxDQUN2RixHQUFHOzs7O29CQUFDLFVBQUMsUUFBUTt3QkFDVCxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLG1CQUFBLFFBQVEsRUFBVSxDQUFDO29CQUN6RCxDQUFDLEVBQUMsQ0FDTCxDQUFDO29CQUNGLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUNyRDtnQkFDRCxPQUFPLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJOzs7O2dCQUFDLFVBQUEsUUFBUTtvQkFDbEQsSUFBSSxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLElBQUksT0FBSyxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFO3dCQUN2RixPQUFPLEtBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ2xDO29CQUNELE9BQU8sT0FBSyxDQUFDO2dCQUNqQixDQUFDLEVBQUMsQ0FBQzthQUNOO1NBQ0o7UUFBQyxPQUFPLEdBQUcsRUFBRTtTQUNiO1FBQ0QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsK0JBQVM7Ozs7SUFBVCxVQUFVLE9BQU87UUFDYixJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLEVBQUU7WUFDakQsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxDQUFDO1NBQzFEO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO2FBQzlFLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsVUFBQyxRQUFROztnQkFDSCxLQUFLLEdBQUcsUUFBUTtZQUN0QixJQUFJO2dCQUNBLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7YUFDbkU7WUFBQyxPQUFPLENBQUMsRUFBRTthQUNYO1lBQ0QsSUFBSTtnQkFDQSxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2FBQ3JFO1lBQUMsT0FBTyxDQUFDLEVBQUU7YUFDWDtZQUNELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUMsRUFBQyxDQUNMLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbEIsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxDQUFDO0lBQzNELENBQUM7Ozs7O0lBRUQsZ0NBQVU7Ozs7SUFBVixVQUFXLE1BQU07O1FBQ2IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7OztRQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBbEMsQ0FBa0MsRUFBQzthQUM1RCxJQUFJOzs7OztRQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBbEMsQ0FBa0MsRUFBQyxDQUFDOztZQUN4RCxLQUF3QixJQUFBLEtBQUEsaUJBQUEsTUFBTSxDQUFDLE9BQU8sQ0FBQSxnQkFBQSw0QkFBRTtnQkFBbkMsSUFBTSxTQUFTLFdBQUE7Z0JBQ2hCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDOUI7Ozs7Ozs7OztJQUNMLENBQUM7Ozs7O0lBRUQsb0NBQWM7Ozs7SUFBZCxVQUFlLGVBQXVCO1FBQXRDLGlCQW9CQztRQW5CRyxPQUFPLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQ3ZCLEdBQUc7Ozs7UUFBQyxVQUFDLEtBQVM7O1lBQ1YsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsRUFBRTs7b0JBQ3ZCLFlBQVU7Ozs7O2dCQUFHLFVBQUMsSUFBSSxFQUFFLE9BQU87Ozt3QkFDdkIsT0FBTyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsVUFBVSxFQUFFLFdBQVcsRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFFLFFBQVE7d0JBQzFGLFFBQVEsRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztvQkFDL0QsT0FBTyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQzs7d0JBQ2pDLEtBQW9CLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsUUFBUSxDQUFBLGdCQUFBLDRCQUFFOzRCQUE5QixJQUFNLEtBQUssV0FBQTs0QkFDWixZQUFVLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQ2hEOzs7Ozs7Ozs7Z0JBQ0wsQ0FBQyxDQUFBOztvQkFDRCxLQUFtQixJQUFBLFVBQUEsaUJBQUEsS0FBSyxDQUFBLDRCQUFBLCtDQUFFO3dCQUFyQixJQUFNLElBQUksa0JBQUE7d0JBQ1gsWUFBVSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztxQkFDeEI7Ozs7Ozs7OzthQUNKO1lBQ0QsT0FBTyxLQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDdEYsQ0FBQyxFQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRUQsbUNBQWE7Ozs7SUFBYixVQUFjLEtBQWE7UUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QixDQUFDOztnQkF2SUosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFQTyxNQUFNO2dCQUNOLFVBQVU7OztzQkFKbEI7Q0FpSkMsQUF6SUQsQ0FJaUMsWUFBWSxHQXFJNUM7U0FySVksV0FBVzs7O0lBRXBCLDJCQUFVOztJQUNWLGtDQUFnQjs7Ozs7SUFHWiw2QkFBd0I7Ozs7O0lBQ3hCLDBCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NhY2hlU2VydmljZX0gZnJvbSAnLi4vY2FjaGUuc2VydmljZSc7XG5pbXBvcnQge21hcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQge0h0dHBDbGllbnR9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7Zm9ya0pvaW59IGZyb20gJ3J4anMnO1xuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5leHBvcnQgY2xhc3MgUGFnZVNlcnZpY2UgZXh0ZW5kcyBDYWNoZVNlcnZpY2Uge1xuXG4gICAgcGFnZTogYW55O1xuICAgIGJyZWFkY3J1bWJzOiB7fTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgICAgIHByb3RlY3RlZCBhcGk6IEh0dHBDbGllbnQsXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKFxuICAgICAgICAgICAgcm91dGVyLFxuICAgICAgICAgICAgYXBpLFxuICAgICAgICApO1xuICAgIH1cblxuICAgIHJlZnJlc2goKTogdm9pZCB7XG4gICAgICAgIHN1cGVyLnJlZnJlc2goKTtcbiAgICAgICAgdGhpcy5wYWdlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuYnJlYWRjcnVtYnMgPSB7fTtcbiAgICB9XG5cbiAgICBzZXRDdXJyZW50UGFnZShwYWdlOiBhbnkpIHtcbiAgICAgICAgdGhpcy5wYWdlID0gcGFnZTtcbiAgICB9XG5cbiAgICBnZXRDdXJyZW50UGFnZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucGFnZTtcbiAgICB9XG5cbiAgICBnZXRBbGwoaGVhZGVycz86IHt9KSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldExpc3RUaHJvdWdoQ2FjaGUoJ3BhZ2VzJywge30sIGhlYWRlcnMgfHwge30pO1xuICAgIH1cblxuICAgIGdldFBhZ2UoaWRlbnRpZmllciwgYmxvY2tJbnRlcmFjdGlvbj86IGJvb2xlYW4sIGhlYWRlcnM/OiB7fSkge1xuICAgICAgICBjb25zdCBjYWNoZUtleSA9IHRoaXMuY3JlYXRlQ2FjaGVLZXkoWydwYWdlJywgaWRlbnRpZmllciwgaGVhZGVyc10pO1xuICAgICAgICBpZiAoIWJsb2NrSW50ZXJhY3Rpb24gJiYgdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XSkge1xuICAgICAgICAgICAgdGhpcy5hcGkuZ2V0KCdwYWdlLycgKyBpZGVudGlmaWVyLCB7aGVhZGVyczogaGVhZGVycyB8fCB7fX0pLnN1YnNjcmliZSgpOyAvLyBUcmlnZ2VyIGludGVyYWN0aW9uXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XTtcbiAgICAgICAgfSBlbHNlIGlmIChibG9ja0ludGVyYWN0aW9uKSB7XG4gICAgICAgICAgICBoZWFkZXJzID0gT2JqZWN0LmFzc2lnbih7fSwgKGhlYWRlcnMgfHwge30pLCB7J3gtc2VydmljZS1pbmZvJzogSlNPTi5zdHJpbmdpZnkoe2Jsb2NrSW50ZXJhY3Rpb246IHRydWV9KX0pO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFtjYWNoZUtleV0gPSB0aGlzLmdldE9uZVRocm91Z2hDYWNoZSgncGFnZScsIGlkZW50aWZpZXIsIGhlYWRlcnMsIGNhY2hlS2V5KTtcbiAgICAgICAgcmV0dXJuIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFtjYWNoZUtleV07XG4gICAgfVxuXG4gICAgZ2V0UGFnZXMoaGVhZGVycz86IHt9KSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldExpc3RUaHJvdWdoQ2FjaGUoJ3BhZ2VzJywge30sIGhlYWRlcnMgfHwge30pO1xuICAgIH1cblxuICAgIGdldEJsb2NrKGJsb2NrSWQpIHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IG9ic2VydmFibGVzID0gW107XG4gICAgICAgICAgICBpZiAodGhpcy5nZXRGcm9tU3RvcmFnZSgnYmxvY2s6JyArIGJsb2NrSWQpICE9PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IGJsb2NrID0gSlNPTi5wYXJzZSh0aGlzLmdldEZyb21TdG9yYWdlKCdibG9jazonICsgYmxvY2tJZCkgYXMgc3RyaW5nKTtcbiAgICAgICAgICAgICAgICBpZiAoIWJsb2NrLnRpbWVzdGFtcCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5sb2FkQmxvY2soYmxvY2tJZCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5jYWNoZVByb2JlVGltZXN0YW1wLmJsb2Nrcykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhY2hlUHJvYmVQcm9taXNlcyQuYmxvY2tzID0gdGhpcy5hcGkuZ2V0KCdjYWNoZS1wcm9iZScsIHtwYXJhbXM6IHtxOiAnd2lkZ2V0J319KS5waXBlKFxuICAgICAgICAgICAgICAgICAgICAgICAgbWFwKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2FjaGVQcm9iZVRpbWVzdGFtcC5ibG9ja3MgPSByZXNwb25zZSBhcyBudW1iZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICBvYnNlcnZhYmxlcy5wdXNoKHRoaXMuY2FjaGVQcm9iZVByb21pc2VzJC5ibG9ja3MpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gZm9ya0pvaW4ob2JzZXJ2YWJsZXMpLnRvUHJvbWlzZSgpLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIXRoaXMuY2FjaGVQcm9iZVRpbWVzdGFtcC5ibG9ja3MgfHwgYmxvY2sudGltZXN0YW1wIDwgdGhpcy5jYWNoZVByb2JlVGltZXN0YW1wLmJsb2Nrcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9hZEJsb2NrKGJsb2NrSWQpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBibG9jaztcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMubG9hZEJsb2NrKGJsb2NrSWQpO1xuICAgIH1cblxuICAgIGxvYWRCbG9jayhibG9ja0lkKSB7XG4gICAgICAgIGlmICh0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbJ2Jsb2NrOicgKyBibG9ja0lkXSkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFsnYmxvY2s6JyArIGJsb2NrSWRdO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFsnYmxvY2s6JyArIGJsb2NrSWRdID0gdGhpcy5hcGkuZ2V0KCd3aWRnZXQvJyArIGJsb2NrSWQpXG4gICAgICAgICAgICAucGlwZShcbiAgICAgICAgICAgICAgICBtYXAoKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGJsb2NrID0gcmVzcG9uc2U7XG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnYmxvY2s6JyArIGJsb2NrSWQsIEpTT04uc3RyaW5naWZ5KGJsb2NrKSk7XG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnYmxvY2s6JyArIGJsb2NrSWQsIEpTT04uc3RyaW5naWZ5KGJsb2NrKSk7XG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gYmxvY2s7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICkudG9Qcm9taXNlKCk7XG4gICAgICAgIHJldHVybiB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbJ2Jsb2NrOicgKyBibG9ja0lkXTtcbiAgICB9XG5cbiAgICBzb3J0QmxvY2tzKGJsb2Nrcykge1xuICAgICAgICBibG9ja3MuY29udGVudC5zb3J0KChhLCBiKSA9PiAoYS5kYXRhLnJvdyA8IGIuZGF0YS5yb3cpID8gMSA6IC0xKVxuICAgICAgICAgICAgLnNvcnQoKGEsIGIpID0+IChhLmRhdGEuY29sIDwgYi5kYXRhLmNvbCkgPyAxIDogLTEpO1xuICAgICAgICBmb3IgKGNvbnN0IHN1YkJsb2NrcyBvZiBibG9ja3MuY29udGVudCkge1xuICAgICAgICAgICAgdGhpcy5zb3J0QmxvY2tzKHN1YkJsb2Nrcyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBnZXRCcmVhZGNydW1icyhyZXF1ZXN0ZWRQYWdlSWQ6IG51bWJlcikge1xuICAgICAgICByZXR1cm4gdGhpcy5nZXRQYWdlcygpLnBpcGUoXG4gICAgICAgICAgICBtYXAoKHBhZ2VzOiBbXSkgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChfLmlzRW1wdHkodGhpcy5icmVhZGNydW1icykpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXh0cmFjdElkcyA9IChwYWdlLCBwYXJlbnRzKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwYWdlTWluID0gXy5vbWl0KHBhZ2UsIFsnY2hpbGRyZW4nLCAnY3JlYXRlZEJ5JywgJ2RhdGVDcmVhdGVkJywgJ2Rpc3BsYXlPcmRlcicsICdncm91cHMnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdoaWRkZW4nLCAnbGFzdFVwZGF0ZWQnLCAncGVybWlzc2lvbnMnLCAndGFncycsICd3aWRnZXRzJ10pO1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFnZU1pbi5wYXJlbnRzID0gcGFyZW50cztcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYnJlYWRjcnVtYnNbcGFnZS5pZF0gPSBwYWdlO1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBjaGlsZCBvZiBwYWdlLmNoaWxkcmVuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXh0cmFjdElkcyhjaGlsZCwgcGFyZW50cy5jb25jYXQoW3BhZ2VNaW5dKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgcGFnZSBvZiBwYWdlcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXh0cmFjdElkcyhwYWdlLCBbXSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuYnJlYWRjcnVtYnNbcmVxdWVzdGVkUGFnZUlkXSA/IHRoaXMuYnJlYWRjcnVtYnNbcmVxdWVzdGVkUGFnZUlkXSA6IFtdO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBwZXJmb3JtU2VhcmNoKHF1ZXJ5OiBzdHJpbmcpIHtcbiAgICAgICAgY29uc29sZS5sb2cocXVlcnkpO1xuICAgIH1cblxufVxuIl19