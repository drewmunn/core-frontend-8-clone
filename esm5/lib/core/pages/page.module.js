/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/page.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PageComponent } from './page.component';
import { BlockWrapperComponent } from '../blocks/block-wrapper/block-wrapper.component';
import { BlockLanguageComponent } from '../blocks/block-language/block-language.component';
import { BlockParagraphComponent } from '../blocks/block-paragraph/block-paragraph.component';
import { BlockParagraphLeadComponent } from '../blocks/block-paragraph-lead/block-paragraph-lead.component';
import { BlockTableComponent } from '../blocks/block-table/block-table.component';
import { BlockAccordionComponent } from '../blocks/block-accordion/block-accordion.component';
import { BlockH1Component } from '../blocks/block-h1/block-h1.component';
import { BlockH2Component } from '../blocks/block-h2/block-h2.component';
import { BlockH3Component } from '../blocks/block-h3/block-h3.component';
import { BlockH4Component } from '../blocks/block-h4/block-h4.component';
import { BlockH5Component } from '../blocks/block-h5/block-h5.component';
import { BlockH6Component } from '../blocks/block-h6/block-h6.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { BlockComponent } from '../block/block.component';
import { BlockAbstractComponent } from '../blocks/block-abstract.component';
import { BlockImageComponent } from '../blocks/block-image/block-image.component';
import { BlockBreadcrumbsComponent } from '../blocks/block-breadcrumbs/block-breadcrumbs.component';
import { MatListModule } from '@angular/material/list';
import { BlockCarouselComponent } from '../blocks/block-carousel/block-carousel.component';
import { BlockVideoComponent } from '../blocks/block-video/block-video.component';
import { BlockBulletsComponent } from '../blocks/block-bullets/block-bullets.component';
import { BlockNumberedComponent } from '../blocks/block-numbered/block-numbered.component';
import { BlockBlockquoteComponent } from '../blocks/block-blockquote/block-blockquote.component';
import { BlockDownloadComponent } from '../blocks/block-download/block-download.component';
import { BlockButtonComponent } from '../blocks/block-button/block-button.component';
import { BlockDividerComponent } from '../blocks/block-divider/block-divider.component';
import { BlockProgressComponent } from '../blocks/block-progress/block-progress.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { BlockTabsComponent } from '../blocks/block-tabs/block-tabs.component';
import { MatTabsModule } from '@angular/material/tabs';
import { SpinnerModule } from '../utils/spinner/spinner.module';
import { TruncatePipe } from '../utils/pipes/limitto.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlockSearchResultsComponent } from '../blocks/block-search-results/block-search-results.component';
import { BlockSearchComponent } from '../blocks/block-search/block-search.component';
import { BlockCompileDirective } from '../blocks/block-compile.directive';
var ɵ0 = { breadcrumbs: ['Page'] };
var PageModule = /** @class */ (function () {
    function PageModule() {
    }
    PageModule.decorators = [
        { type: NgModule, args: [{
                    exports: [
                        BlockComponent,
                    ],
                    declarations: [
                        PageComponent,
                        TruncatePipe,
                        BlockComponent,
                        BlockAbstractComponent,
                        BlockWrapperComponent,
                        BlockLanguageComponent,
                        BlockParagraphComponent,
                        BlockParagraphLeadComponent,
                        BlockTableComponent,
                        BlockAccordionComponent,
                        BlockH1Component,
                        BlockH2Component,
                        BlockH3Component,
                        BlockH4Component,
                        BlockH5Component,
                        BlockH6Component,
                        BlockImageComponent,
                        BlockBreadcrumbsComponent,
                        BlockCarouselComponent,
                        BlockVideoComponent,
                        BlockBulletsComponent,
                        BlockNumberedComponent,
                        BlockBlockquoteComponent,
                        BlockDownloadComponent,
                        BlockButtonComponent,
                        BlockDividerComponent,
                        BlockProgressComponent,
                        BlockTabsComponent,
                        BlockSearchResultsComponent,
                        BlockSearchComponent,
                        BlockCompileDirective,
                    ],
                    imports: [
                        CommonModule,
                        RouterModule.forChild([
                            {
                                path: ':identifier', component: PageComponent,
                                data: ɵ0
                            }
                        ]),
                        MatExpansionModule,
                        MatListModule,
                        MatProgressBarModule,
                        MatTabsModule,
                        SpinnerModule,
                        ReactiveFormsModule,
                        FormsModule
                    ],
                    entryComponents: [
                        BlockAbstractComponent,
                        BlockWrapperComponent,
                        BlockLanguageComponent,
                        BlockParagraphComponent,
                        BlockParagraphLeadComponent,
                        BlockTableComponent,
                        BlockAccordionComponent,
                        BlockH1Component,
                        BlockH2Component,
                        BlockH3Component,
                        BlockH4Component,
                        BlockH5Component,
                        BlockH6Component,
                        BlockImageComponent,
                        BlockBreadcrumbsComponent,
                        BlockCarouselComponent,
                        BlockVideoComponent,
                        BlockBulletsComponent,
                        BlockNumberedComponent,
                        BlockBlockquoteComponent,
                        BlockDownloadComponent,
                        BlockButtonComponent,
                        BlockDividerComponent,
                        BlockProgressComponent,
                        BlockTabsComponent,
                        BlockSearchResultsComponent,
                        BlockSearchComponent,
                    ],
                },] }
    ];
    return PageModule;
}());
export { PageModule };
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3BhZ2VzL3BhZ2UubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxrQkFBa0IsQ0FBQztBQUMvQyxPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSxpREFBaUQsQ0FBQztBQUN0RixPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSxtREFBbUQsQ0FBQztBQUN6RixPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSxxREFBcUQsQ0FBQztBQUM1RixPQUFPLEVBQUMsMkJBQTJCLEVBQUMsTUFBTSwrREFBK0QsQ0FBQztBQUMxRyxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSw2Q0FBNkMsQ0FBQztBQUNoRixPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSxxREFBcUQsQ0FBQztBQUM1RixPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsa0JBQWtCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUMvRCxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDeEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sb0NBQW9DLENBQUM7QUFDMUUsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sNkNBQTZDLENBQUM7QUFDaEYsT0FBTyxFQUFDLHlCQUF5QixFQUFDLE1BQU0seURBQXlELENBQUM7QUFDbEcsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBQ3JELE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLG1EQUFtRCxDQUFDO0FBQ3pGLE9BQU8sRUFBQyxtQkFBbUIsRUFBQyxNQUFNLDZDQUE2QyxDQUFDO0FBQ2hGLE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLGlEQUFpRCxDQUFDO0FBQ3RGLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLG1EQUFtRCxDQUFDO0FBQ3pGLE9BQU8sRUFBQyx3QkFBd0IsRUFBQyxNQUFNLHVEQUF1RCxDQUFDO0FBQy9GLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLG1EQUFtRCxDQUFDO0FBQ3pGLE9BQU8sRUFBQyxvQkFBb0IsRUFBQyxNQUFNLCtDQUErQyxDQUFDO0FBQ25GLE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLGlEQUFpRCxDQUFDO0FBQ3RGLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLG1EQUFtRCxDQUFDO0FBQ3pGLE9BQU8sRUFBQyxvQkFBb0IsRUFBQyxNQUFNLGdDQUFnQyxDQUFDO0FBQ3BFLE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLDJDQUEyQyxDQUFDO0FBQzdFLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUNyRCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0saUNBQWlDLENBQUM7QUFDOUQsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBQ3pELE9BQU8sRUFBQyxXQUFXLEVBQUUsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNoRSxPQUFPLEVBQUMsMkJBQTJCLEVBQUMsTUFBTSwrREFBK0QsQ0FBQztBQUMxRyxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSwrQ0FBK0MsQ0FBQztBQUNuRixPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSxtQ0FBbUMsQ0FBQztTQTRDbEQsRUFBQyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUMsRUFBQztBQTFDN0M7SUFBQTtJQW9GQSxDQUFDOztnQkFwRkEsUUFBUSxTQUFDO29CQUNOLE9BQU8sRUFBRTt3QkFDTCxjQUFjO3FCQUNqQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1YsYUFBYTt3QkFDYixZQUFZO3dCQUNaLGNBQWM7d0JBQ2Qsc0JBQXNCO3dCQUN0QixxQkFBcUI7d0JBQ3JCLHNCQUFzQjt3QkFDdEIsdUJBQXVCO3dCQUN2QiwyQkFBMkI7d0JBQzNCLG1CQUFtQjt3QkFDbkIsdUJBQXVCO3dCQUN2QixnQkFBZ0I7d0JBQ2hCLGdCQUFnQjt3QkFDaEIsZ0JBQWdCO3dCQUNoQixnQkFBZ0I7d0JBQ2hCLGdCQUFnQjt3QkFDaEIsZ0JBQWdCO3dCQUNoQixtQkFBbUI7d0JBQ25CLHlCQUF5Qjt3QkFDekIsc0JBQXNCO3dCQUN0QixtQkFBbUI7d0JBQ25CLHFCQUFxQjt3QkFDckIsc0JBQXNCO3dCQUN0Qix3QkFBd0I7d0JBQ3hCLHNCQUFzQjt3QkFDdEIsb0JBQW9CO3dCQUNwQixxQkFBcUI7d0JBQ3JCLHNCQUFzQjt3QkFDdEIsa0JBQWtCO3dCQUNsQiwyQkFBMkI7d0JBQzNCLG9CQUFvQjt3QkFDcEIscUJBQXFCO3FCQUN4QjtvQkFDRCxPQUFPLEVBQUU7d0JBQ0wsWUFBWTt3QkFDWixZQUFZLENBQUMsUUFBUSxDQUFDOzRCQUNsQjtnQ0FDSSxJQUFJLEVBQUUsYUFBYSxFQUFFLFNBQVMsRUFBRSxhQUFhO2dDQUM3QyxJQUFJLElBQXlCOzZCQUNoQzt5QkFDSixDQUFDO3dCQUNGLGtCQUFrQjt3QkFDbEIsYUFBYTt3QkFDYixvQkFBb0I7d0JBQ3BCLGFBQWE7d0JBQ2IsYUFBYTt3QkFDYixtQkFBbUI7d0JBQ25CLFdBQVc7cUJBQ2Q7b0JBQ0QsZUFBZSxFQUFFO3dCQUNiLHNCQUFzQjt3QkFDdEIscUJBQXFCO3dCQUNyQixzQkFBc0I7d0JBQ3RCLHVCQUF1Qjt3QkFDdkIsMkJBQTJCO3dCQUMzQixtQkFBbUI7d0JBQ25CLHVCQUF1Qjt3QkFDdkIsZ0JBQWdCO3dCQUNoQixnQkFBZ0I7d0JBQ2hCLGdCQUFnQjt3QkFDaEIsZ0JBQWdCO3dCQUNoQixnQkFBZ0I7d0JBQ2hCLGdCQUFnQjt3QkFDaEIsbUJBQW1CO3dCQUNuQix5QkFBeUI7d0JBQ3pCLHNCQUFzQjt3QkFDdEIsbUJBQW1CO3dCQUNuQixxQkFBcUI7d0JBQ3JCLHNCQUFzQjt3QkFDdEIsd0JBQXdCO3dCQUN4QixzQkFBc0I7d0JBQ3RCLG9CQUFvQjt3QkFDcEIscUJBQXFCO3dCQUNyQixzQkFBc0I7d0JBQ3RCLGtCQUFrQjt3QkFDbEIsMkJBQTJCO3dCQUMzQixvQkFBb0I7cUJBQ3ZCO2lCQUNKOztJQUVELGlCQUFDO0NBQUEsQUFwRkQsSUFvRkM7U0FEWSxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQge1JvdXRlck1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHtQYWdlQ29tcG9uZW50fSBmcm9tICcuL3BhZ2UuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja1dyYXBwZXJDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay13cmFwcGVyL2Jsb2NrLXdyYXBwZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja0xhbmd1YWdlQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stbGFuZ3VhZ2UvYmxvY2stbGFuZ3VhZ2UuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja1BhcmFncmFwaENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXBhcmFncmFwaC9ibG9jay1wYXJhZ3JhcGguY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja1BhcmFncmFwaExlYWRDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1wYXJhZ3JhcGgtbGVhZC9ibG9jay1wYXJhZ3JhcGgtbGVhZC5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrVGFibGVDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay10YWJsZS9ibG9jay10YWJsZS5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrQWNjb3JkaW9uQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stYWNjb3JkaW9uL2Jsb2NrLWFjY29yZGlvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrSDFDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1oMS9ibG9jay1oMS5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrSDJDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1oMi9ibG9jay1oMi5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrSDNDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1oMy9ibG9jay1oMy5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrSDRDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1oNC9ibG9jay1oNC5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrSDVDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1oNS9ibG9jay1oNS5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrSDZDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1oNi9ibG9jay1oNi5jb21wb25lbnQnO1xyXG5pbXBvcnQge01hdEV4cGFuc2lvbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZXhwYW5zaW9uJztcclxuaW1wb3J0IHtCbG9ja0NvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2svYmxvY2suY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja0ltYWdlQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2staW1hZ2UvYmxvY2staW1hZ2UuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja0JyZWFkY3J1bWJzQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stYnJlYWRjcnVtYnMvYmxvY2stYnJlYWRjcnVtYnMuY29tcG9uZW50JztcclxuaW1wb3J0IHtNYXRMaXN0TW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9saXN0JztcclxuaW1wb3J0IHtCbG9ja0Nhcm91c2VsQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stY2Fyb3VzZWwvYmxvY2stY2Fyb3VzZWwuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja1ZpZGVvQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stdmlkZW8vYmxvY2stdmlkZW8uY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja0J1bGxldHNDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1idWxsZXRzL2Jsb2NrLWJ1bGxldHMuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja051bWJlcmVkQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stbnVtYmVyZWQvYmxvY2stbnVtYmVyZWQuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja0Jsb2NrcXVvdGVDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1ibG9ja3F1b3RlL2Jsb2NrLWJsb2NrcXVvdGUuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja0Rvd25sb2FkQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stZG93bmxvYWQvYmxvY2stZG93bmxvYWQuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja0J1dHRvbkNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWJ1dHRvbi9ibG9jay1idXR0b24uY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja0RpdmlkZXJDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1kaXZpZGVyL2Jsb2NrLWRpdmlkZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHtCbG9ja1Byb2dyZXNzQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stcHJvZ3Jlc3MvYmxvY2stcHJvZ3Jlc3MuY29tcG9uZW50JztcclxuaW1wb3J0IHtNYXRQcm9ncmVzc0Jhck1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcHJvZ3Jlc3MtYmFyJztcclxuaW1wb3J0IHtCbG9ja1RhYnNDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay10YWJzL2Jsb2NrLXRhYnMuY29tcG9uZW50JztcclxuaW1wb3J0IHtNYXRUYWJzTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJzJztcclxuaW1wb3J0IHtTcGlubmVyTW9kdWxlfSBmcm9tICcuLi91dGlscy9zcGlubmVyL3NwaW5uZXIubW9kdWxlJztcclxuaW1wb3J0IHtUcnVuY2F0ZVBpcGV9IGZyb20gJy4uL3V0aWxzL3BpcGVzL2xpbWl0dG8ucGlwZSc7XHJcbmltcG9ydCB7Rm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHtCbG9ja1NlYXJjaFJlc3VsdHNDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1zZWFyY2gtcmVzdWx0cy9ibG9jay1zZWFyY2gtcmVzdWx0cy5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrU2VhcmNoQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stc2VhcmNoL2Jsb2NrLXNlYXJjaC5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrQ29tcGlsZURpcmVjdGl2ZX0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWNvbXBpbGUuZGlyZWN0aXZlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgQmxvY2tDb21wb25lbnQsXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgUGFnZUNvbXBvbmVudCxcclxuICAgICAgICBUcnVuY2F0ZVBpcGUsXHJcbiAgICAgICAgQmxvY2tDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tBYnN0cmFjdENvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1dyYXBwZXJDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tMYW5ndWFnZUNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1BhcmFncmFwaENvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1BhcmFncmFwaExlYWRDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tUYWJsZUNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0FjY29yZGlvbkNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0gxQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrSDJDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tIM0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0g0Q29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrSDVDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tINkNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0ltYWdlQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrQnJlYWRjcnVtYnNDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tDYXJvdXNlbENvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1ZpZGVvQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrQnVsbGV0c0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja051bWJlcmVkQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrQmxvY2txdW90ZUNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0Rvd25sb2FkQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrRGl2aWRlckNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1Byb2dyZXNzQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrVGFic0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1NlYXJjaFJlc3VsdHNDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tTZWFyY2hDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tDb21waWxlRGlyZWN0aXZlLFxyXG4gICAgXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgUm91dGVyTW9kdWxlLmZvckNoaWxkKFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcGF0aDogJzppZGVudGlmaWVyJywgY29tcG9uZW50OiBQYWdlQ29tcG9uZW50LFxyXG4gICAgICAgICAgICAgICAgZGF0YToge2JyZWFkY3J1bWJzOiBbJ1BhZ2UnXX1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF0pLFxyXG4gICAgICAgIE1hdEV4cGFuc2lvbk1vZHVsZSxcclxuICAgICAgICBNYXRMaXN0TW9kdWxlLFxyXG4gICAgICAgIE1hdFByb2dyZXNzQmFyTW9kdWxlLFxyXG4gICAgICAgIE1hdFRhYnNNb2R1bGUsXHJcbiAgICAgICAgU3Bpbm5lck1vZHVsZSxcclxuICAgICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG4gICAgICAgIEZvcm1zTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbXHJcbiAgICAgICAgQmxvY2tBYnN0cmFjdENvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1dyYXBwZXJDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tMYW5ndWFnZUNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1BhcmFncmFwaENvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1BhcmFncmFwaExlYWRDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tUYWJsZUNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0FjY29yZGlvbkNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0gxQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrSDJDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tIM0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0g0Q29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrSDVDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tINkNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0ltYWdlQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrQnJlYWRjcnVtYnNDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tDYXJvdXNlbENvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1ZpZGVvQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrQnVsbGV0c0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja051bWJlcmVkQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrQmxvY2txdW90ZUNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0Rvd25sb2FkQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrRGl2aWRlckNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1Byb2dyZXNzQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrVGFic0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1NlYXJjaFJlc3VsdHNDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tTZWFyY2hDb21wb25lbnQsXHJcbiAgICBdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGFnZU1vZHVsZSB7XHJcbn1cclxuIl19