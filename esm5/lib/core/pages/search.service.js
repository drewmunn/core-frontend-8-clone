/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/search.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { PageService } from './page.service';
import { SpinnerService } from '../utils/spinner/spinner.service';
import { TranslateService } from './translate.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common/http";
import * as i3 from "../utils/spinner/spinner.service";
import * as i4 from "./translate.service";
/** @type {?} */
var EXCERPT_SIZE = 500;
var SearchService = /** @class */ (function (_super) {
    tslib_1.__extends(SearchService, _super);
    function SearchService(router, api, spinner, translate) {
        var _this = _super.call(this, router, api) || this;
        _this.router = router;
        _this.api = api;
        _this.spinner = spinner;
        _this.translate = translate;
        return _this;
    }
    /**
     * @return {?}
     */
    SearchService.prototype.refresh = /**
     * @return {?}
     */
    function () {
        this.limit = 10;
        this.offset = 0;
        this.results = [];
        this.eddie = false;
        this.totalResults = 0;
    };
    /**
     * @param {?} query
     * @return {?}
     */
    SearchService.prototype.setQuery = /**
     * @param {?} query
     * @return {?}
     */
    function (query) {
        this.query = query;
    };
    /**
     * @return {?}
     */
    SearchService.prototype.getQuery = /**
     * @return {?}
     */
    function () {
        return this.query;
    };
    /**
     * @param {?} query
     * @return {?}
     */
    SearchService.prototype.performSearch = /**
     * @param {?} query
     * @return {?}
     */
    function (query) {
        var _this = this;
        this.refresh();
        this.setQuery(query);
        this.spinner.show('search');
        return this.getOneThroughCache('search', null, {}, false, {
            q: query,
            limit: this.limit,
            offset: this.offset,
            excerpt: EXCERPT_SIZE
        }).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            var e_1, _a;
            if (response.response.docs) {
                /** @type {?} */
                var searchTermRegex = new RegExp(query, 'gi');
                _this.results = tslib_1.__spread(response.response.docs);
                _this.totalResults = response.response.numFound;
                try {
                    for (var _b = tslib_1.__values(_this.results), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var result = _c.value;
                        if (result.content) {
                            result.content = result.content[0].substring(0, 1000).replace(searchTermRegex, '<em>$&</em>');
                            if (response.highlighting[result.uniqueId].content) {
                                result.content = response.highlighting[result.uniqueId].content[0];
                            }
                            else if (response.highlighting[result.uniqueId].text && response.highlighting[result.uniqueId].text[0]) {
                                result.content = response.highlighting[result.uniqueId].text[0];
                            }
                            result.content = result.content.replace(/\|\|/g, ', ').replace(/\ufffd/g, ' ') + '&hellip;';
                            if (result.categories) {
                                result.category = result.categories[0];
                            }
                            // result.name = LanguageService.getPageSpecific('name', results);
                            // result.url = LanguageService.getPageSpecific('url', results);
                            // result.description = LanguageService.getPageSpecific('description', results);
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
        }));
    };
    /**
     * @return {?}
     */
    SearchService.prototype.getTotalResults = /**
     * @return {?}
     */
    function () {
        return this.totalResults;
    };
    SearchService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SearchService.ctorParameters = function () { return [
        { type: Router },
        { type: HttpClient },
        { type: SpinnerService },
        { type: TranslateService }
    ]; };
    /** @nocollapse */ SearchService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SearchService_Factory() { return new SearchService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.HttpClient), i0.ɵɵinject(i3.SpinnerService), i0.ɵɵinject(i4.TranslateService)); }, token: SearchService, providedIn: "root" });
    return SearchService;
}(PageService));
export { SearchService };
if (false) {
    /** @type {?} */
    SearchService.prototype.query;
    /** @type {?} */
    SearchService.prototype.limit;
    /** @type {?} */
    SearchService.prototype.offset;
    /** @type {?} */
    SearchService.prototype.results;
    /** @type {?} */
    SearchService.prototype.totalResults;
    /** @type {?} */
    SearchService.prototype.eddie;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.api;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.spinner;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3BhZ2VzL3NlYXJjaC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNoRCxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDM0MsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLGtDQUFrQyxDQUFDO0FBQ2hFLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLHFCQUFxQixDQUFDOzs7Ozs7O0lBRS9DLFlBQVksR0FBRyxHQUFHO0FBRXhCO0lBSW1DLHlDQUFXO0lBWTFDLHVCQUNjLE1BQWMsRUFDZCxHQUFlLEVBQ2YsT0FBdUIsRUFDdkIsU0FBMkI7UUFKekMsWUFNSSxrQkFDSSxNQUFNLEVBQ04sR0FBRyxDQUNOLFNBQ0o7UUFUYSxZQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsU0FBRyxHQUFILEdBQUcsQ0FBWTtRQUNmLGFBQU8sR0FBUCxPQUFPLENBQWdCO1FBQ3ZCLGVBQVMsR0FBVCxTQUFTLENBQWtCOztJQU16QyxDQUFDOzs7O0lBRUQsK0JBQU87OztJQUFQO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFFRCxnQ0FBUTs7OztJQUFSLFVBQVMsS0FBYTtRQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDOzs7O0lBRUQsZ0NBQVE7OztJQUFSO1FBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQscUNBQWE7Ozs7SUFBYixVQUFjLEtBQWE7UUFBM0IsaUJBa0NDO1FBakNHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNmLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFNUIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFO1lBQ3RELENBQUMsRUFBRSxLQUFLO1lBQ1IsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNuQixPQUFPLEVBQUUsWUFBWTtTQUN4QixDQUFDLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsUUFBUTs7WUFDakIsSUFBSSxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRTs7b0JBQ2xCLGVBQWUsR0FBRyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO2dCQUMvQyxLQUFJLENBQUMsT0FBTyxvQkFBTyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzQyxLQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDOztvQkFDL0MsS0FBcUIsSUFBQSxLQUFBLGlCQUFBLEtBQUksQ0FBQyxPQUFPLENBQUEsZ0JBQUEsNEJBQUU7d0JBQTlCLElBQU0sTUFBTSxXQUFBO3dCQUNiLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTs0QkFDaEIsTUFBTSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSxhQUFhLENBQUMsQ0FBQzs0QkFDOUYsSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLEVBQUU7Z0NBQ2hELE1BQU0sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUN0RTtpQ0FBTSxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3RHLE1BQU0sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUNuRTs0QkFDRCxNQUFNLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxHQUFHLFVBQVUsQ0FBQzs0QkFDNUYsSUFBSSxNQUFNLENBQUMsVUFBVSxFQUFFO2dDQUNuQixNQUFNLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7NkJBQzFDOzRCQUNELGtFQUFrRTs0QkFDbEUsZ0VBQWdFOzRCQUNoRSxnRkFBZ0Y7eUJBQ25GO3FCQUNKOzs7Ozs7Ozs7YUFDSjtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELHVDQUFlOzs7SUFBZjtRQUNJLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDOztnQkFsRkosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFWTyxNQUFNO2dCQUNOLFVBQVU7Z0JBRVYsY0FBYztnQkFDZCxnQkFBZ0I7Ozt3QkFMeEI7Q0E2RkMsQUFwRkQsQ0FJbUMsV0FBVyxHQWdGN0M7U0FoRlksYUFBYTs7O0lBRXRCLDhCQUFjOztJQUVkLDhCQUFjOztJQUNkLCtCQUFlOztJQUVmLGdDQUFlOztJQUNmLHFDQUFxQjs7SUFFckIsOEJBQVc7Ozs7O0lBR1AsK0JBQXdCOzs7OztJQUN4Qiw0QkFBeUI7Ozs7O0lBQ3pCLGdDQUFpQzs7Ozs7SUFDakMsa0NBQXFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Um91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHtIdHRwQ2xpZW50fSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge1BhZ2VTZXJ2aWNlfSBmcm9tICcuL3BhZ2Uuc2VydmljZSc7XG5pbXBvcnQge1NwaW5uZXJTZXJ2aWNlfSBmcm9tICcuLi91dGlscy9zcGlubmVyL3NwaW5uZXIuc2VydmljZSc7XG5pbXBvcnQge1RyYW5zbGF0ZVNlcnZpY2V9IGZyb20gJy4vdHJhbnNsYXRlLnNlcnZpY2UnO1xuXG5jb25zdCBFWENFUlBUX1NJWkUgPSA1MDA7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5cbmV4cG9ydCBjbGFzcyBTZWFyY2hTZXJ2aWNlIGV4dGVuZHMgUGFnZVNlcnZpY2Uge1xuXG4gICAgcXVlcnk6IHN0cmluZztcblxuICAgIGxpbWl0OiBudW1iZXI7XG4gICAgb2Zmc2V0OiBudW1iZXI7XG5cbiAgICByZXN1bHRzOiBhbnlbXTtcbiAgICB0b3RhbFJlc3VsdHM6IG51bWJlcjtcblxuICAgIGVkZGllOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIHJvdXRlcjogUm91dGVyLFxuICAgICAgICBwcm90ZWN0ZWQgYXBpOiBIdHRwQ2xpZW50LFxuICAgICAgICBwcm90ZWN0ZWQgc3Bpbm5lcjogU3Bpbm5lclNlcnZpY2UsXG4gICAgICAgIHByb3RlY3RlZCB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKFxuICAgICAgICAgICAgcm91dGVyLFxuICAgICAgICAgICAgYXBpLFxuICAgICAgICApO1xuICAgIH1cblxuICAgIHJlZnJlc2goKTogdm9pZCB7XG4gICAgICAgIHRoaXMubGltaXQgPSAxMDtcbiAgICAgICAgdGhpcy5vZmZzZXQgPSAwO1xuICAgICAgICB0aGlzLnJlc3VsdHMgPSBbXTtcbiAgICAgICAgdGhpcy5lZGRpZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLnRvdGFsUmVzdWx0cyA9IDA7XG4gICAgfVxuXG4gICAgc2V0UXVlcnkocXVlcnk6IHN0cmluZykge1xuICAgICAgICB0aGlzLnF1ZXJ5ID0gcXVlcnk7XG4gICAgfVxuXG4gICAgZ2V0UXVlcnkoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnF1ZXJ5O1xuICAgIH1cblxuICAgIHBlcmZvcm1TZWFyY2gocXVlcnk6IHN0cmluZykge1xuICAgICAgICB0aGlzLnJlZnJlc2goKTtcbiAgICAgICAgdGhpcy5zZXRRdWVyeShxdWVyeSk7XG4gICAgICAgIHRoaXMuc3Bpbm5lci5zaG93KCdzZWFyY2gnKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5nZXRPbmVUaHJvdWdoQ2FjaGUoJ3NlYXJjaCcsIG51bGwsIHt9LCBmYWxzZSwge1xuICAgICAgICAgICAgcTogcXVlcnksXG4gICAgICAgICAgICBsaW1pdDogdGhpcy5saW1pdCxcbiAgICAgICAgICAgIG9mZnNldDogdGhpcy5vZmZzZXQsXG4gICAgICAgICAgICBleGNlcnB0OiBFWENFUlBUX1NJWkVcbiAgICAgICAgfSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgIGlmIChyZXNwb25zZS5yZXNwb25zZS5kb2NzKSB7XG4gICAgICAgICAgICAgICAgY29uc3Qgc2VhcmNoVGVybVJlZ2V4ID0gbmV3IFJlZ0V4cChxdWVyeSwgJ2dpJyk7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXN1bHRzID0gWy4uLnJlc3BvbnNlLnJlc3BvbnNlLmRvY3NdO1xuICAgICAgICAgICAgICAgIHRoaXMudG90YWxSZXN1bHRzID0gcmVzcG9uc2UucmVzcG9uc2UubnVtRm91bmQ7XG4gICAgICAgICAgICAgICAgZm9yIChjb25zdCByZXN1bHQgb2YgdGhpcy5yZXN1bHRzKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQuY29udGVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbnRlbnQgPSByZXN1bHQuY29udGVudFswXS5zdWJzdHJpbmcoMCwgMTAwMCkucmVwbGFjZShzZWFyY2hUZXJtUmVnZXgsICc8ZW0+JCY8L2VtPicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmhpZ2hsaWdodGluZ1tyZXN1bHQudW5pcXVlSWRdLmNvbnRlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuY29udGVudCA9IHJlc3BvbnNlLmhpZ2hsaWdodGluZ1tyZXN1bHQudW5pcXVlSWRdLmNvbnRlbnRbMF07XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHJlc3BvbnNlLmhpZ2hsaWdodGluZ1tyZXN1bHQudW5pcXVlSWRdLnRleHQgJiYgcmVzcG9uc2UuaGlnaGxpZ2h0aW5nW3Jlc3VsdC51bmlxdWVJZF0udGV4dFswXSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5jb250ZW50ID0gcmVzcG9uc2UuaGlnaGxpZ2h0aW5nW3Jlc3VsdC51bmlxdWVJZF0udGV4dFswXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5jb250ZW50ID0gcmVzdWx0LmNvbnRlbnQucmVwbGFjZSgvXFx8XFx8L2csICcsICcpLnJlcGxhY2UoL1xcdWZmZmQvZywgJyAnKSArICcmaGVsbGlwOyc7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0LmNhdGVnb3JpZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuY2F0ZWdvcnkgPSByZXN1bHQuY2F0ZWdvcmllc1swXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHJlc3VsdC5uYW1lID0gTGFuZ3VhZ2VTZXJ2aWNlLmdldFBhZ2VTcGVjaWZpYygnbmFtZScsIHJlc3VsdHMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gcmVzdWx0LnVybCA9IExhbmd1YWdlU2VydmljZS5nZXRQYWdlU3BlY2lmaWMoJ3VybCcsIHJlc3VsdHMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gcmVzdWx0LmRlc2NyaXB0aW9uID0gTGFuZ3VhZ2VTZXJ2aWNlLmdldFBhZ2VTcGVjaWZpYygnZGVzY3JpcHRpb24nLCByZXN1bHRzKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZ2V0VG90YWxSZXN1bHRzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy50b3RhbFJlc3VsdHM7XG4gICAgfVxuXG59XG4iXX0=