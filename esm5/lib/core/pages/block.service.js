/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/block.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BlockWrapperComponent } from '../blocks/block-wrapper/block-wrapper.component';
import { BlockLanguageComponent } from '../blocks/block-language/block-language.component';
import { BlockParagraphComponent } from '../blocks/block-paragraph/block-paragraph.component';
import { BlockParagraphLeadComponent } from '../blocks/block-paragraph-lead/block-paragraph-lead.component';
import { BlockTableComponent } from '../blocks/block-table/block-table.component';
import { BlockAccordionComponent } from '../blocks/block-accordion/block-accordion.component';
import { BlockH1Component } from '../blocks/block-h1/block-h1.component';
import { BlockH2Component } from '../blocks/block-h2/block-h2.component';
import { BlockH3Component } from '../blocks/block-h3/block-h3.component';
import { BlockH4Component } from '../blocks/block-h4/block-h4.component';
import { BlockH5Component } from '../blocks/block-h5/block-h5.component';
import { BlockH6Component } from '../blocks/block-h6/block-h6.component';
import { BlockImageComponent } from '../blocks/block-image/block-image.component';
import { BlockBreadcrumbsComponent } from '../blocks/block-breadcrumbs/block-breadcrumbs.component';
import { BlockCarouselComponent } from '../blocks/block-carousel/block-carousel.component';
import { BlockVideoComponent } from '../blocks/block-video/block-video.component';
import { BlockBulletsComponent } from '../blocks/block-bullets/block-bullets.component';
import { BlockNumberedComponent } from '../blocks/block-numbered/block-numbered.component';
import { BlockBlockquoteComponent } from '../blocks/block-blockquote/block-blockquote.component';
import { BlockDownloadComponent } from '../blocks/block-download/block-download.component';
import { BlockButtonComponent } from '../blocks/block-button/block-button.component';
import { BlockDividerComponent } from '../blocks/block-divider/block-divider.component';
import { BlockProgressComponent } from '../blocks/block-progress/block-progress.component';
import { BlockTabsComponent } from '../blocks/block-tabs/block-tabs.component';
import { BlockSearchResultsComponent } from '../blocks/block-search-results/block-search-results.component';
import { BlockSearchComponent } from '../blocks/block-search/block-search.component';
import * as i0 from "@angular/core";
var BlockService = /** @class */ (function () {
    function BlockService() {
        this.replaceBlocks({
            WRAPPER: BlockWrapperComponent,
            LANGUAGE: BlockLanguageComponent,
            PARAGRAPH: BlockParagraphComponent,
            'PARAGRAPH-LEAD': BlockParagraphLeadComponent,
            TABLE: BlockTableComponent,
            ACCORDION: BlockAccordionComponent,
            H1: BlockH1Component,
            H2: BlockH2Component,
            H3: BlockH3Component,
            H4: BlockH4Component,
            H5: BlockH5Component,
            H6: BlockH6Component,
            IMAGE: BlockImageComponent,
            BREADCRUMBS: BlockBreadcrumbsComponent,
            CAROUSEL: BlockCarouselComponent,
            VIDEO: BlockVideoComponent,
            BULLETS: BlockBulletsComponent,
            NUMBERED: BlockNumberedComponent,
            BLOCKQUOTE: BlockBlockquoteComponent,
            DOWNLOAD: BlockDownloadComponent,
            BUTTON: BlockButtonComponent,
            DIVIDER: BlockDividerComponent,
            PROGRESS: BlockProgressComponent,
            TABS: BlockTabsComponent,
            'SEARCH-RESULTS': BlockSearchResultsComponent,
            SEARCH: BlockSearchComponent,
        });
    }
    /**
     * @param {?} blocks
     * @return {?}
     */
    BlockService.prototype.replaceBlocks = /**
     * @param {?} blocks
     * @return {?}
     */
    function (blocks) {
        this.blocks = blocks;
    };
    /**
     * @return {?}
     */
    BlockService.prototype.getBlocks = /**
     * @return {?}
     */
    function () {
        return this.blocks;
    };
    /**
     * @param {?} identifier
     * @return {?}
     */
    BlockService.prototype.hasBlock = /**
     * @param {?} identifier
     * @return {?}
     */
    function (identifier) {
        return this.blocks.hasOwnProperty(identifier);
    };
    /**
     * @param {?} identifier
     * @return {?}
     */
    BlockService.prototype.getBlock = /**
     * @param {?} identifier
     * @return {?}
     */
    function (identifier) {
        return this.blocks[identifier];
    };
    /**
     * @param {?} identifier
     * @param {?} component
     * @return {?}
     */
    BlockService.prototype.addBlock = /**
     * @param {?} identifier
     * @param {?} component
     * @return {?}
     */
    function (identifier, component) {
        this.blocks[identifier] = component;
    };
    BlockService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    BlockService.ctorParameters = function () { return []; };
    /** @nocollapse */ BlockService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function BlockService_Factory() { return new BlockService(); }, token: BlockService, providedIn: "root" });
    return BlockService;
}());
export { BlockService };
if (false) {
    /** @type {?} */
    BlockService.prototype.blocks;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2suc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvcGFnZXMvYmxvY2suc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0saURBQWlELENBQUM7QUFDdEYsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFDekYsT0FBTyxFQUFDLHVCQUF1QixFQUFDLE1BQU0scURBQXFELENBQUM7QUFDNUYsT0FBTyxFQUFDLDJCQUEyQixFQUFDLE1BQU0sK0RBQStELENBQUM7QUFDMUcsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sNkNBQTZDLENBQUM7QUFDaEYsT0FBTyxFQUFDLHVCQUF1QixFQUFDLE1BQU0scURBQXFELENBQUM7QUFDNUYsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sNkNBQTZDLENBQUM7QUFDaEYsT0FBTyxFQUFDLHlCQUF5QixFQUFDLE1BQU0seURBQXlELENBQUM7QUFDbEcsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFDekYsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sNkNBQTZDLENBQUM7QUFDaEYsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0saURBQWlELENBQUM7QUFDdEYsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFDekYsT0FBTyxFQUFDLHdCQUF3QixFQUFDLE1BQU0sdURBQXVELENBQUM7QUFDL0YsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFDekYsT0FBTyxFQUFDLG9CQUFvQixFQUFDLE1BQU0sK0NBQStDLENBQUM7QUFDbkYsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0saURBQWlELENBQUM7QUFDdEYsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFDekYsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sMkNBQTJDLENBQUM7QUFDN0UsT0FBTyxFQUFDLDJCQUEyQixFQUFDLE1BQU0sK0RBQStELENBQUM7QUFDMUcsT0FBTyxFQUFDLG9CQUFvQixFQUFDLE1BQU0sK0NBQStDLENBQUM7O0FBRW5GO0lBUUk7UUFDSSxJQUFJLENBQUMsYUFBYSxDQUNkO1lBQ0ksT0FBTyxFQUFFLHFCQUFxQjtZQUM5QixRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLFNBQVMsRUFBRSx1QkFBdUI7WUFDbEMsZ0JBQWdCLEVBQUUsMkJBQTJCO1lBQzdDLEtBQUssRUFBRSxtQkFBbUI7WUFDMUIsU0FBUyxFQUFFLHVCQUF1QjtZQUNsQyxFQUFFLEVBQUUsZ0JBQWdCO1lBQ3BCLEVBQUUsRUFBRSxnQkFBZ0I7WUFDcEIsRUFBRSxFQUFFLGdCQUFnQjtZQUNwQixFQUFFLEVBQUUsZ0JBQWdCO1lBQ3BCLEVBQUUsRUFBRSxnQkFBZ0I7WUFDcEIsRUFBRSxFQUFFLGdCQUFnQjtZQUNwQixLQUFLLEVBQUUsbUJBQW1CO1lBQzFCLFdBQVcsRUFBRSx5QkFBeUI7WUFDdEMsUUFBUSxFQUFFLHNCQUFzQjtZQUNoQyxLQUFLLEVBQUUsbUJBQW1CO1lBQzFCLE9BQU8sRUFBRSxxQkFBcUI7WUFDOUIsUUFBUSxFQUFFLHNCQUFzQjtZQUNoQyxVQUFVLEVBQUUsd0JBQXdCO1lBQ3BDLFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMsTUFBTSxFQUFFLG9CQUFvQjtZQUM1QixPQUFPLEVBQUUscUJBQXFCO1lBQzlCLFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMsSUFBSSxFQUFFLGtCQUFrQjtZQUN4QixnQkFBZ0IsRUFBRSwyQkFBMkI7WUFDN0MsTUFBTSxFQUFFLG9CQUFvQjtTQUMvQixDQUNKLENBQUM7SUFDTixDQUFDOzs7OztJQUVELG9DQUFhOzs7O0lBQWIsVUFBYyxNQUFNO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0lBQ3pCLENBQUM7Ozs7SUFFRCxnQ0FBUzs7O0lBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFFRCwrQkFBUTs7OztJQUFSLFVBQVMsVUFBVTtRQUNmLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDbEQsQ0FBQzs7Ozs7SUFFRCwrQkFBUTs7OztJQUFSLFVBQVMsVUFBVTtRQUNmLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7Ozs7SUFFRCwrQkFBUTs7Ozs7SUFBUixVQUFTLFVBQVUsRUFBRSxTQUFTO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLEdBQUcsU0FBUyxDQUFDO0lBQ3hDLENBQUM7O2dCQTNESixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7Ozt1QkE5QkQ7Q0F5RkMsQUE3REQsSUE2REM7U0F6RFksWUFBWTs7O0lBRXJCLDhCQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tXcmFwcGVyQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2std3JhcHBlci9ibG9jay13cmFwcGVyLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrTGFuZ3VhZ2VDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1sYW5ndWFnZS9ibG9jay1sYW5ndWFnZS5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja1BhcmFncmFwaENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXBhcmFncmFwaC9ibG9jay1wYXJhZ3JhcGguY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tQYXJhZ3JhcGhMZWFkQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stcGFyYWdyYXBoLWxlYWQvYmxvY2stcGFyYWdyYXBoLWxlYWQuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tUYWJsZUNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXRhYmxlL2Jsb2NrLXRhYmxlLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrQWNjb3JkaW9uQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stYWNjb3JkaW9uL2Jsb2NrLWFjY29yZGlvbi5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja0gxQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2staDEvYmxvY2staDEuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tIMkNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWgyL2Jsb2NrLWgyLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrSDNDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1oMy9ibG9jay1oMy5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja0g0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2staDQvYmxvY2staDQuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tINUNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWg1L2Jsb2NrLWg1LmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrSDZDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1oNi9ibG9jay1oNi5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja0ltYWdlQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2staW1hZ2UvYmxvY2staW1hZ2UuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tCcmVhZGNydW1ic0NvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWJyZWFkY3J1bWJzL2Jsb2NrLWJyZWFkY3J1bWJzLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrQ2Fyb3VzZWxDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1jYXJvdXNlbC9ibG9jay1jYXJvdXNlbC5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja1ZpZGVvQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stdmlkZW8vYmxvY2stdmlkZW8uY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tCdWxsZXRzQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stYnVsbGV0cy9ibG9jay1idWxsZXRzLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrTnVtYmVyZWRDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1udW1iZXJlZC9ibG9jay1udW1iZXJlZC5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja0Jsb2NrcXVvdGVDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1ibG9ja3F1b3RlL2Jsb2NrLWJsb2NrcXVvdGUuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tEb3dubG9hZENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWRvd25sb2FkL2Jsb2NrLWRvd25sb2FkLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrQnV0dG9uQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stYnV0dG9uL2Jsb2NrLWJ1dHRvbi5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja0RpdmlkZXJDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1kaXZpZGVyL2Jsb2NrLWRpdmlkZXIuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tQcm9ncmVzc0NvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXByb2dyZXNzL2Jsb2NrLXByb2dyZXNzLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrVGFic0NvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXRhYnMvYmxvY2stdGFicy5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja1NlYXJjaFJlc3VsdHNDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1zZWFyY2gtcmVzdWx0cy9ibG9jay1zZWFyY2gtcmVzdWx0cy5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja1NlYXJjaENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXNlYXJjaC9ibG9jay1zZWFyY2guY29tcG9uZW50JztcblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcblxuZXhwb3J0IGNsYXNzIEJsb2NrU2VydmljZSB7XG5cbiAgICBibG9ja3M6IGFueTtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICB0aGlzLnJlcGxhY2VCbG9ja3MoXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgV1JBUFBFUjogQmxvY2tXcmFwcGVyQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIExBTkdVQUdFOiBCbG9ja0xhbmd1YWdlQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIFBBUkFHUkFQSDogQmxvY2tQYXJhZ3JhcGhDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgJ1BBUkFHUkFQSC1MRUFEJzogQmxvY2tQYXJhZ3JhcGhMZWFkQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIFRBQkxFOiBCbG9ja1RhYmxlQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIEFDQ09SRElPTjogQmxvY2tBY2NvcmRpb25Db21wb25lbnQsXG4gICAgICAgICAgICAgICAgSDE6IEJsb2NrSDFDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgSDI6IEJsb2NrSDJDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgSDM6IEJsb2NrSDNDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgSDQ6IEJsb2NrSDRDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgSDU6IEJsb2NrSDVDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgSDY6IEJsb2NrSDZDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgSU1BR0U6IEJsb2NrSW1hZ2VDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgQlJFQURDUlVNQlM6IEJsb2NrQnJlYWRjcnVtYnNDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgQ0FST1VTRUw6IEJsb2NrQ2Fyb3VzZWxDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgVklERU86IEJsb2NrVmlkZW9Db21wb25lbnQsXG4gICAgICAgICAgICAgICAgQlVMTEVUUzogQmxvY2tCdWxsZXRzQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIE5VTUJFUkVEOiBCbG9ja051bWJlcmVkQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIEJMT0NLUVVPVEU6IEJsb2NrQmxvY2txdW90ZUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBET1dOTE9BRDogQmxvY2tEb3dubG9hZENvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBCVVRUT046IEJsb2NrQnV0dG9uQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIERJVklERVI6IEJsb2NrRGl2aWRlckNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBQUk9HUkVTUzogQmxvY2tQcm9ncmVzc0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBUQUJTOiBCbG9ja1RhYnNDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgJ1NFQVJDSC1SRVNVTFRTJzogQmxvY2tTZWFyY2hSZXN1bHRzQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIFNFQVJDSDogQmxvY2tTZWFyY2hDb21wb25lbnQsXG4gICAgICAgICAgICB9XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcmVwbGFjZUJsb2NrcyhibG9ja3MpIHtcbiAgICAgICAgdGhpcy5ibG9ja3MgPSBibG9ja3M7XG4gICAgfVxuXG4gICAgZ2V0QmxvY2tzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5ibG9ja3M7XG4gICAgfVxuXG4gICAgaGFzQmxvY2soaWRlbnRpZmllcikge1xuICAgICAgICByZXR1cm4gdGhpcy5ibG9ja3MuaGFzT3duUHJvcGVydHkoaWRlbnRpZmllcik7XG4gICAgfVxuXG4gICAgZ2V0QmxvY2soaWRlbnRpZmllcikge1xuICAgICAgICByZXR1cm4gdGhpcy5ibG9ja3NbaWRlbnRpZmllcl07XG4gICAgfVxuXG4gICAgYWRkQmxvY2soaWRlbnRpZmllciwgY29tcG9uZW50KSB7XG4gICAgICAgIHRoaXMuYmxvY2tzW2lkZW50aWZpZXJdID0gY29tcG9uZW50O1xuICAgIH1cblxufVxuIl19