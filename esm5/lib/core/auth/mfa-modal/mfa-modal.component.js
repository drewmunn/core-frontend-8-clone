/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/auth/mfa-modal/mfa-modal.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { SystemMessageService } from '../../systemMessage.service';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { APP_CONFIG } from '../../../app.config';
import sha1 from 'js-sha1';
var MfaModalComponent = /** @class */ (function () {
    function MfaModalComponent(bsModalRef, messages, api, router) {
        this.bsModalRef = bsModalRef;
        this.messages = messages;
        this.api = api;
        this.router = router;
        this.mfaToken = new FormControl('');
        this.mfaToken2 = new FormControl('');
        this.googleSecret = new FormControl('');
        this.secondaryUsername = new FormControl('');
        this.secondaryPassword = new FormControl('');
        this.mobile = new FormControl('');
        this.mfa = {
            type: 'google',
            state: 418,
        };
    }
    /**
     * @return {?}
     */
    MfaModalComponent.prototype.submit = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var newData = Object.assign({}, this.interceptedResponse.response.request.body, {
            mfa_token: this.mfaToken.value,
            mfa_token2: this.mfaToken2.value,
            mfaType: this.mfa.type,
            google2step: this.getState() === 419,
            googleSecret: this.googleSecret.value,
            secondary_username: this.secondaryUsername.value,
            secondary_password: this.secondaryPassword.value,
            mobile: this.mobile.value
        });
        /** @type {?} */
        var responseHash = this.router.url + JSON.stringify(newData) + APP_CONFIG.authHeaders.xUserToken;
        /** @type {?} */
        var newHeaders = this.interceptedResponse.response.request.headers.set('x-service-request-hash', sha1(responseHash));
        /** @type {?} */
        var newRequest = this.interceptedResponse.response.request.clone({
            headers: newHeaders,
            body: newData,
            observe: (/** @type {?} */ ('response')),
        });
        console.log(newRequest);
        // return this.api.post('auth')(newRequest.body, null, {
        //         headers: newHeaders,
        //         // @TODO: Headers not set
        //     }).subscribe(
        //         this.interceptedResponse.subject,
        //         response => {
        //             this.mfa.mfaResponse = response.headers.get('x-service-mfa');
        //             this.setState(response.status);
        //
        //             switch (this.getState()) {
        //                 // case 417:
        //                 //     $rootScope.successMessage = {text: response.data};
        //                 //     $modalInstance.close();
        //                 //     break;
        //                 case 418:
        //                     return this._messages.setInterceptMessage('Invalid Authentication Code');
        //                 // case 419:
        //                 //     $rootScope.interceptMessage = {text: 'Enable Multi-Factor Authentication'};
        //                 //     $('div.modal-lg').addClass('modal-lg').removeClass('modal-sm');
        //                 //     break;
        //                 // case 421:
        //                 //     $rootScope.passwordPolicy = {
        //                 //         response: response,
        //                 //         deferred: $rootScope.twoStep.deferred
        //                 //     };
        //                 //     $rootScope.interceptMessage = {text: "Your password does not comply with the latest Core password policy, enforced in September 2019. Please update your password to ensure the utmost security of this application."};
        //                 //     $injector.invoke(['passwordPolicyModal', function (passwordPolicyModal) {
        //                 //         passwordPolicyModal.show();
        //                 //     }]);
        //                 //     return false;
        //             }
        //
        //         }
        //     );
    };
    /**
     * @return {?}
     */
    MfaModalComponent.prototype.getInterceptMessage = /**
     * @return {?}
     */
    function () {
        return this.messages.getInterceptMessage();
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    MfaModalComponent.prototype.closeModal = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.bsModalRef.hide();
    };
    /**
     * @param {?} type
     * @return {?}
     */
    MfaModalComponent.prototype.setType = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        this.mfa.type = type;
    };
    /**
     * @return {?}
     */
    MfaModalComponent.prototype.getType = /**
     * @return {?}
     */
    function () {
        return this.mfa.type;
    };
    /**
     * @param {?} state
     * @return {?}
     */
    MfaModalComponent.prototype.setState = /**
     * @param {?} state
     * @return {?}
     */
    function (state) {
        this.mfa.state = state;
    };
    /**
     * @return {?}
     */
    MfaModalComponent.prototype.getState = /**
     * @return {?}
     */
    function () {
        return this.mfa.state;
    };
    /**
     * @param {?} google
     * @return {?}
     */
    MfaModalComponent.prototype.isMFAEnabled = /**
     * @param {?} google
     * @return {?}
     */
    function (google) {
        return false;
    };
    MfaModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-mfa-modal',
                    template: "<div class=\"modal-header\">\r\n    <h3 class=\"modal-title\">Multi-Factor Authentication</h3>\r\n</div>\r\n<div class=\"modal-content\">\r\n    <div class=\"modal-body\">\r\n        <div class=\"modal-body\">\r\n\r\n            <div (show)=\"getInterceptMessage() && getState() !== 421\">\r\n                <div class=\"new-item alert alert-danger\">\r\n                    <span class=\"fal fa-exclamation-circle\"></span>\r\n                    {{getInterceptMessage()}}\r\n                </div>\r\n            </div>\r\n\r\n            <div [ngSwitch]=\"mfa.state\">\r\n                <div class=\"form-group\" *ngSwitchCase=\"419\">\r\n\r\n                    <tabset>\r\n\r\n                        <tab heading=\"Google Authenticator Setup\" (click)=\"setType('google')\"\r\n                             *ngIf=\"!isMFAEnabled('google')\">\r\n                            <div class=\"box box-default\">\r\n                                <div class=\"box-body\">\r\n\r\n                                    <p>To setup Multi-Factor Authentication with Google, you will need to install the\r\n                                        free\r\n                                        Google Authenticator application on your mobile device, adding an additional\r\n                                        layer\r\n                                        of\r\n                                        security to this application. Please select your mobile phone operating system\r\n                                        below:</p>\r\n\r\n                                    <tabset>\r\n                                        <tab heading=\"Apple iOS\">\r\n                                            <div class=\"box box-default\">\r\n                                                <div class=\"box-body\">\r\n                                                    <h4>Downloading the app</h4>\r\n                                                    <ol>\r\n                                                        <li>Visit the iTunes App Store</li>\r\n                                                        <li>Search for Google Authenticator</li>\r\n                                                        <li>Download and install the application</li>\r\n                                                    </ol>\r\n\r\n                                                    <h4>Setting up the app</h4>\r\n                                                    <ol>\r\n                                                        <li>On your phone, open the Google Authenticator application\r\n                                                        </li>\r\n                                                        <li>Tap the plus icon</li>\r\n                                                        <li>You can add your account using the QR code below or\r\n                                                            manually:\r\n                                                            <ul>\r\n                                                                <li>Using Barcode: Tap \"Scan Barcode\" and then point\r\n                                                                    your\r\n                                                                    camera\r\n                                                                    at the QR code below\r\n                                                                </li>\r\n                                                                <li>Using Manual Entry: Tap \"Manual Entry\" and enter the\r\n                                                                    secret\r\n                                                                    key:\r\n                                                                    <!--                                                                <strong>{{twoStep.googleSecret}}</strong></li>-->\r\n                                                            </ul>\r\n                                                        </li>\r\n                                                        <li>The first response code will now show in your application,\r\n                                                            enter\r\n                                                            it\r\n                                                            into the box below to complete setup\r\n                                                        </li>\r\n                                                    </ol>\r\n                                                </div>\r\n                                            </div>\r\n                                        </tab>\r\n                                        <tab heading=\"Google Android\">\r\n                                            <div class=\"box box-default\">\r\n                                                <div class=\"box-body\">\r\n                                                    <h4>Downloading the app</h4>\r\n                                                    <ol>\r\n                                                        <li>Visit Google Play</li>\r\n                                                        <li>Search for Google Authenticator</li>\r\n                                                        <li>Download and install the application</li>\r\n                                                    </ol>\r\n\r\n                                                    <h4>Setting up the app</h4>\r\n                                                    <ol>\r\n                                                        <li>On your phone, open the Google Authenticator application\r\n                                                        </li>\r\n                                                        <li>If this is the first time you have used Authenticator, click\r\n                                                            the\r\n                                                            Add\r\n                                                            an account button. If you are adding a new account, choose\r\n                                                            \u201CAdd\r\n                                                            an\r\n                                                            account\u201D from the app\u2019s menu\r\n                                                        </li>\r\n                                                        <li>You can add your account using the QR code below or\r\n                                                            manually:\r\n                                                            <ul>\r\n                                                                <li>Using Barcode: Tap \"Scan Barcode\". If the\r\n                                                                    Authenticator\r\n                                                                    app\r\n                                                                    cannot locate a barcode scanner app on your phone,\r\n                                                                    you\r\n                                                                    might\r\n                                                                    be prompted to download and install one,\r\n                                                                    alternatively\r\n                                                                    use\r\n                                                                    the manual entry option below. If you want to\r\n                                                                    install a\r\n                                                                    barcode scanner app so you can complete the setup\r\n                                                                    process,\r\n                                                                    click install and then point your camera at the QR\r\n                                                                    code\r\n                                                                    below\r\n                                                                </li>\r\n                                                                <li>Using Manual Entry: Tap \"Manual Entry\" and enter the\r\n                                                                    secret\r\n                                                                    key:\r\n                                                                    <!--                                                                <strong>{{twoStep.googleSecret}}</strong><br/>Make sure-->\r\n                                                                    you've chosen to make the key Time based and press\r\n                                                                    'Save'\r\n                                                                </li>\r\n                                                            </ul>\r\n                                                        </li>\r\n                                                        <li>The first response code will now show in your application,\r\n                                                            enter\r\n                                                            it\r\n                                                            into the box below to complete setup\r\n                                                        </li>\r\n                                                    </ol>\r\n                                                </div>\r\n                                            </div>\r\n                                        </tab>\r\n                                        <tab heading=\"Microsoft Windows\">\r\n                                            <div class=\"box box-default\">\r\n                                                <div class=\"box-body\">\r\n                                                    <h4>Downloading the app</h4>\r\n                                                    <ol>\r\n                                                        <li>Visit the Windows App Store</li>\r\n                                                        <li>Search for Microsoft Authenticator</li>\r\n                                                        <li>Download and install the application</li>\r\n                                                    </ol>\r\n\r\n                                                    <h4>Setting up the app</h4>\r\n                                                    <ol>\r\n                                                        <li>On your phone, open the Microsoft Authenticator\r\n                                                            application\r\n                                                        </li>\r\n                                                        <li>Tap the Add (+) icon</li>\r\n                                                        <li>You can add your account using the QR code below or\r\n                                                            manually:\r\n                                                            <ul>\r\n                                                                <li>Using Barcode: Tap \"Scan Barcode\" and then point\r\n                                                                    your\r\n                                                                    camera\r\n                                                                    at the QR code below\r\n                                                                </li>\r\n                                                                <li>Using Manual Entry: Tap \"Manual Entry\" and enter the\r\n                                                                    secret\r\n                                                                    key:\r\n                                                                    <!--                                                                <strong>{{twoStep.googleSecret}}</strong></li>-->\r\n                                                            </ul>\r\n                                                        </li>\r\n                                                        <li>The first response code will now show in your application,\r\n                                                            enter\r\n                                                            it\r\n                                                            into the box below to complete setup\r\n                                                        </li>\r\n                                                    </ol>\r\n                                                </div>\r\n                                            </div>\r\n                                        </tab>\r\n                                    </tabset>\r\n\r\n                                    <p>This setup process needs to completed only once, subsequent access to this\r\n                                        application\r\n                                        will request the latest code shown in your authenticator application to be\r\n                                        entered\r\n                                        to\r\n                                        ensure maximum application security.</p>\r\n\r\n                                    <div class=\"input-group text-center\" style=\"width:100%; margin:14px 0;\">\r\n                                        <!--                                    <img ng-src=\"{{twoStep.qrcode}}\" alt=\"QR Code\"/>-->\r\n                                    </div>\r\n\r\n                                    <div class=\"row\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter Authentication Code</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"6\" name=\"mfa_token\"\r\n                                                       ng-model=\"twoStep.mfa_token\" class=\"form-control input-lg\"\r\n                                                       autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                                                       ng-required=\"twoStep.mfaType === 'google'\"/>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <div class=\"row\" ng-if=\"isMFAEnabled('sms')\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter your SMS Authentication Code</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"13\" name=\"mfa_token2\"\r\n                                                       ng-model=\"twoStep.mfa_token2\" class=\"form-control input-lg\"\r\n                                                       autocomplete=\"off\" style=\"letter-spacing: 15px;\"\r\n                                                       ng-required=\"twoStep.mfaType === 'google'\"/>\r\n                                            </div>\r\n                                            <small class=\"pull-right\">This is required to validate setup</small>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n                            </div>\r\n                        </tab>\r\n\r\n                        <tab heading=\"SMS Setup\" ng-click=\"twoStep.mfaType = 'sms'\" ng-if=\"!isMFAEnabled('sms')\">\r\n                            <div class=\"box box-default\">\r\n                                <div class=\"box-body\">\r\n\r\n                                    <p>To setup Multi-Factor Authentication with SMS, you will need you to enter your\r\n                                        mobile\r\n                                        phone number with country code.</p>\r\n\r\n                                    <div class=\"row\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter your mobile phone number</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-phone\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"13\" name=\"mobile\"\r\n                                                       ng-model=\"twoStep.mobile\"\r\n                                                       class=\"form-control input-lg\" autocomplete=\"off\"\r\n                                                       style=\"letter-spacing: 15px;\" placeholder=\"+44\" auto-focus\r\n                                                       ng-required=\"twoStep.mfaType === 'sms'\"/>\r\n                                            </div>\r\n                                            <small class=\"pull-right\">E.g. +44771234567</small>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <div class=\"row\" ng-if=\"isMFAEnabled('google')\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter your Google Authentication Code</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"6\" name=\"mfa_token2\"\r\n                                                       ng-model=\"twoStep.mfa_token2\" class=\"form-control input-lg\"\r\n                                                       autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                                                       ng-required=\"twoStep.mfaType === 'sms'\"/>\r\n                                            </div>\r\n                                            <small class=\"pull-right\">This is required to validate setup</small>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n                            </div>\r\n                        </tab>\r\n\r\n                    </tabset>\r\n\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngSwitchCase=\"418\">\r\n                    <div class=\"input-group\" *ngIf=\"mfa.type === 'google'\">\r\n                        <div class=\"input-group-prepend\"><i class=\"fal fa-lock\"></i></div>\r\n                        <input type=\"text\" maxlength=\"6\" [formControl]=\"mfaToken\"\r\n                               class=\"form-control form-control-lg\" autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                               [required]=\"true\"/>\r\n                    </div>\r\n\r\n                    <div *ngIf=\"mfa.type === 'sms'\">\r\n                        <p class=\"small\">An SMS with a 6-digit verification code has been sent to\r\n                            <!--                                                    {{mfa.mfaDetails.number}}-->\r\n                        </p>\r\n                        <div class=\"input-group\">\r\n                            <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                            <input type=\"text\" maxlength=\"6\" name=\"mfaToken\" ngModel\r\n                                   class=\"form-control input-lg\" autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                                   [required]=\"true\"/>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <a class=\"pull-right small\" href=\"javascript:void(0)\" (click)=\"setState(421)\">More\r\n                        options</a>\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngSwitchCase=\"420\">\r\n                    <div class=\"input-group\">\r\n                        <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                        <input type=\"text\" name=\"username\" placeholder=\"Username\" ng-model=\"twoStep.secondary_username\"\r\n                               class=\"form-control input-lg\" autocomplete=\"off\" readonly\r\n                               onfocus=\"this.removeAttribute('readonly');\" ng-required=\"true\"/>\r\n                        <input type=\"password\" placeholder=\"Password\" ng-model=\"twoStep.secondary_password\"\r\n                               class=\"form-control input-lg\" autocomplete=\"off\" readonly\r\n                               onfocus=\"this.removeAttribute('readonly');\" ng-required=\"true\"/>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngSwitchCase=\"421\">\r\n                    <h4>Try another way to sign in</h4>\r\n                    <ul class=\"list-group\">\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFAType('google')\"\r\n                            ng-show=\"isMFAEnabled('google')\">\r\n                            <i class=\"fal pull-left ga\"></i>Get a verification code from the\r\n                            <strong>Google Authenticator</strong> app\r\n                        </li>\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFATypeSetup('google')\"\r\n                            ng-show=\"!isMFAEnabled('google')\">\r\n                            <i class=\"fal pull-left ga\"></i>Setup <strong>Google Authenticator</strong>\r\n                        </li>\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFAType('sms')\"\r\n                            ng-show=\"isMFAEnabled('sms')\">\r\n                            <!--                        <i class=\"fal pull-left sms\"></i>Get an SMS verification code at {{twoStep.mfaDetails.number}}-->\r\n                        </li>\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFATypeSetup('sms')\"\r\n                            ng-show=\"!isMFAEnabled('sms')\">\r\n                            <i class=\"fal pull-left sms\"></i>Setup SMS verification\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"modal-footer\" (show)=\"getInterceptMessage() && getState() !== 421\">\r\n            <button class=\"btn btn-warning pull-right\" (click)=\"submit()\" ng-disabled=\"mfaForm.$invalid\">Submit</button>\r\n        </div>\r\n\r\n    </div>\r\n</div>"
                }] }
    ];
    /** @nocollapse */
    MfaModalComponent.ctorParameters = function () { return [
        { type: BsModalRef },
        { type: SystemMessageService },
        { type: HttpClient },
        { type: Router }
    ]; };
    return MfaModalComponent;
}());
export { MfaModalComponent };
if (false) {
    /** @type {?} */
    MfaModalComponent.prototype.mfaToken;
    /** @type {?} */
    MfaModalComponent.prototype.mfaToken2;
    /** @type {?} */
    MfaModalComponent.prototype.googleSecret;
    /** @type {?} */
    MfaModalComponent.prototype.secondaryUsername;
    /** @type {?} */
    MfaModalComponent.prototype.secondaryPassword;
    /** @type {?} */
    MfaModalComponent.prototype.mobile;
    /** @type {?} */
    MfaModalComponent.prototype.interceptedResponse;
    /** @type {?} */
    MfaModalComponent.prototype.mfa;
    /** @type {?} */
    MfaModalComponent.prototype.bsModalRef;
    /**
     * @type {?}
     * @private
     */
    MfaModalComponent.prototype.messages;
    /**
     * @type {?}
     * @private
     */
    MfaModalComponent.prototype.api;
    /**
     * @type {?}
     * @private
     */
    MfaModalComponent.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWZhLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYXV0aC9tZmEtbW9kYWwvbWZhLW1vZGFsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDeEMsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQy9DLE9BQU8sRUFBQyxvQkFBb0IsRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBQ2pFLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sc0JBQXNCLENBQUM7QUFDaEQsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUMvQyxPQUFPLElBQUksTUFBTSxTQUFTLENBQUM7QUFFM0I7SUFrQ0ksMkJBQ1csVUFBc0IsRUFDckIsUUFBOEIsRUFDOUIsR0FBZSxFQUNmLE1BQWM7UUFIZixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3JCLGFBQVEsR0FBUixRQUFRLENBQXNCO1FBQzlCLFFBQUcsR0FBSCxHQUFHLENBQVk7UUFDZixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBaEMxQixhQUFRLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDL0IsY0FBUyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2hDLGlCQUFZLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbkMsc0JBQWlCLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDeEMsc0JBQWlCLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDeEMsV0FBTSxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBNkJ6QixJQUFJLENBQUMsR0FBRyxHQUFHO1lBQ1AsSUFBSSxFQUFFLFFBQVE7WUFDZCxLQUFLLEVBQUUsR0FBRztTQUNiLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsa0NBQU07OztJQUFOOztZQUVVLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUU7WUFDOUUsU0FBUyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSztZQUM5QixVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLO1lBQ2hDLE9BQU8sRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUk7WUFDdEIsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsS0FBSyxHQUFHO1lBQ3BDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUs7WUFDckMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUs7WUFDaEQsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUs7WUFDaEQsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSztTQUM1QixDQUFDOztZQUVJLFlBQVksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUMsVUFBVTs7WUFDNUYsVUFBVSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDOztZQUNoSCxVQUFVLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO1lBQy9ELE9BQU8sRUFBRSxVQUFVO1lBQ25CLElBQUksRUFBRSxPQUFPO1lBQ2IsT0FBTyxFQUFFLG1CQUFBLFVBQVUsRUFBVTtTQUNoQyxDQUFDO1FBRUYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN4Qix3REFBd0Q7UUFDeEQsK0JBQStCO1FBQy9CLG9DQUFvQztRQUNwQyxvQkFBb0I7UUFDcEIsNENBQTRDO1FBQzVDLHdCQUF3QjtRQUN4Qiw0RUFBNEU7UUFDNUUsOENBQThDO1FBQzlDLEVBQUU7UUFDRix5Q0FBeUM7UUFDekMsK0JBQStCO1FBQy9CLDRFQUE0RTtRQUM1RSxpREFBaUQ7UUFDakQsZ0NBQWdDO1FBQ2hDLDRCQUE0QjtRQUM1QixnR0FBZ0c7UUFDaEcsK0JBQStCO1FBQy9CLHFHQUFxRztRQUNyRyx5RkFBeUY7UUFDekYsZ0NBQWdDO1FBQ2hDLCtCQUErQjtRQUMvQix1REFBdUQ7UUFDdkQsaURBQWlEO1FBQ2pELG1FQUFtRTtRQUNuRSw0QkFBNEI7UUFDNUIsaVBBQWlQO1FBQ2pQLG1HQUFtRztRQUNuRyx5REFBeUQ7UUFDekQsOEJBQThCO1FBQzlCLHVDQUF1QztRQUN2QyxnQkFBZ0I7UUFDaEIsRUFBRTtRQUNGLFlBQVk7UUFDWixTQUFTO0lBQ2IsQ0FBQzs7OztJQUVELCtDQUFtQjs7O0lBQW5CO1FBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDL0MsQ0FBQzs7Ozs7SUFFRCxzQ0FBVTs7OztJQUFWLFVBQVcsTUFBa0I7UUFDekIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVELG1DQUFPOzs7O0lBQVAsVUFBUSxJQUFZO1FBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsbUNBQU87OztJQUFQO1FBQ0ksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztJQUN6QixDQUFDOzs7OztJQUVELG9DQUFROzs7O0lBQVIsVUFBUyxLQUFhO1FBQ2xCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUMzQixDQUFDOzs7O0lBRUQsb0NBQVE7OztJQUFSO1FBQ0ksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztJQUMxQixDQUFDOzs7OztJQUVELHdDQUFZOzs7O0lBQVosVUFBYSxNQUFjO1FBQ3ZCLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7O2dCQWxJSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIsOGp1QkFBeUM7aUJBQzVDOzs7O2dCQVhPLFVBQVU7Z0JBQ1Ysb0JBQW9CO2dCQUVwQixVQUFVO2dCQUNWLE1BQU07O0lBdUlkLHdCQUFDO0NBQUEsQUFuSUQsSUFtSUM7U0E5SFksaUJBQWlCOzs7SUFDMUIscUNBQStCOztJQUMvQixzQ0FBZ0M7O0lBQ2hDLHlDQUFtQzs7SUFDbkMsOENBQXdDOztJQUN4Qyw4Q0FBd0M7O0lBQ3hDLG1DQUE2Qjs7SUFFN0IsZ0RBVUU7O0lBRUYsZ0NBT0U7O0lBR0UsdUNBQTZCOzs7OztJQUM3QixxQ0FBc0M7Ozs7O0lBQ3RDLGdDQUF1Qjs7Ozs7SUFDdkIsbUNBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge0JzTW9kYWxSZWZ9IGZyb20gJ25neC1ib290c3RyYXAvbW9kYWwnO1xyXG5pbXBvcnQge1N5c3RlbU1lc3NhZ2VTZXJ2aWNlfSBmcm9tICcuLi8uLi9zeXN0ZW1NZXNzYWdlLnNlcnZpY2UnO1xyXG5pbXBvcnQge0Zvcm1Db250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7SHR0cENsaWVudH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQge1JvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHtBUFBfQ09ORklHfSBmcm9tICcuLi8uLi8uLi9hcHAuY29uZmlnJztcclxuaW1wb3J0IHNoYTEgZnJvbSAnanMtc2hhMSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY29yZS1tZmEtbW9kYWwnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL21mYS1tb2RhbC5jb21wb25lbnQuaHRtbCcsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgTWZhTW9kYWxDb21wb25lbnQge1xyXG4gICAgbWZhVG9rZW4gPSBuZXcgRm9ybUNvbnRyb2woJycpO1xyXG4gICAgbWZhVG9rZW4yID0gbmV3IEZvcm1Db250cm9sKCcnKTtcclxuICAgIGdvb2dsZVNlY3JldCA9IG5ldyBGb3JtQ29udHJvbCgnJyk7XHJcbiAgICBzZWNvbmRhcnlVc2VybmFtZSA9IG5ldyBGb3JtQ29udHJvbCgnJyk7XHJcbiAgICBzZWNvbmRhcnlQYXNzd29yZCA9IG5ldyBGb3JtQ29udHJvbCgnJyk7XHJcbiAgICBtb2JpbGUgPSBuZXcgRm9ybUNvbnRyb2woJycpO1xyXG5cclxuICAgIGludGVyY2VwdGVkUmVzcG9uc2U6IHtcclxuICAgICAgICBzdWJqZWN0OiBhbnk7XHJcbiAgICAgICAgcmVzcG9uc2U6IHtcclxuICAgICAgICAgICAgcmVxdWVzdDoge1xyXG4gICAgICAgICAgICAgICAgYm9keTogYW55O1xyXG4gICAgICAgICAgICAgICAgaGVhZGVyczogYW55O1xyXG4gICAgICAgICAgICAgICAgY2xvbmUocDogeyBoZWFkZXJzOiBhbnlbXSwgYm9keTogYW55W10sIG9ic2VydmU6IHN0cmluZyB9KTogYW55O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJlcGVhdFJlcXVlc3QobmV3UmVxdWVzdDogYW55KTogYW55O1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgbWZhOiB7XHJcbiAgICAgICAgdHlwZTogc3RyaW5nLFxyXG4gICAgICAgIHN0YXRlOiBudW1iZXIsXHJcbiAgICAgICAgZXJyb3I/OiBib29sZWFuLFxyXG4gICAgICAgIG1mYVRva2VuPzogc3RyaW5nLFxyXG4gICAgICAgIGxvYWRpbmc/OiBib29sZWFuLFxyXG4gICAgICAgIG1mYVJlc3BvbnNlPzogYW55XHJcbiAgICB9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBic01vZGFsUmVmOiBCc01vZGFsUmVmLFxyXG4gICAgICAgIHByaXZhdGUgbWVzc2FnZXM6IFN5c3RlbU1lc3NhZ2VTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgYXBpOiBIdHRwQ2xpZW50LFxyXG4gICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXJcclxuICAgICkge1xyXG4gICAgICAgIHRoaXMubWZhID0ge1xyXG4gICAgICAgICAgICB0eXBlOiAnZ29vZ2xlJyxcclxuICAgICAgICAgICAgc3RhdGU6IDQxOCxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHN1Ym1pdCgpIHtcclxuXHJcbiAgICAgICAgY29uc3QgbmV3RGF0YSA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMuaW50ZXJjZXB0ZWRSZXNwb25zZS5yZXNwb25zZS5yZXF1ZXN0LmJvZHksIHtcclxuICAgICAgICAgICAgbWZhX3Rva2VuOiB0aGlzLm1mYVRva2VuLnZhbHVlLFxyXG4gICAgICAgICAgICBtZmFfdG9rZW4yOiB0aGlzLm1mYVRva2VuMi52YWx1ZSxcclxuICAgICAgICAgICAgbWZhVHlwZTogdGhpcy5tZmEudHlwZSxcclxuICAgICAgICAgICAgZ29vZ2xlMnN0ZXA6IHRoaXMuZ2V0U3RhdGUoKSA9PT0gNDE5LFxyXG4gICAgICAgICAgICBnb29nbGVTZWNyZXQ6IHRoaXMuZ29vZ2xlU2VjcmV0LnZhbHVlLFxyXG4gICAgICAgICAgICBzZWNvbmRhcnlfdXNlcm5hbWU6IHRoaXMuc2Vjb25kYXJ5VXNlcm5hbWUudmFsdWUsXHJcbiAgICAgICAgICAgIHNlY29uZGFyeV9wYXNzd29yZDogdGhpcy5zZWNvbmRhcnlQYXNzd29yZC52YWx1ZSxcclxuICAgICAgICAgICAgbW9iaWxlOiB0aGlzLm1vYmlsZS52YWx1ZVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCByZXNwb25zZUhhc2ggPSB0aGlzLnJvdXRlci51cmwgKyBKU09OLnN0cmluZ2lmeShuZXdEYXRhKSArIEFQUF9DT05GSUcuYXV0aEhlYWRlcnMueFVzZXJUb2tlbjtcclxuICAgICAgICBjb25zdCBuZXdIZWFkZXJzID0gdGhpcy5pbnRlcmNlcHRlZFJlc3BvbnNlLnJlc3BvbnNlLnJlcXVlc3QuaGVhZGVycy5zZXQoJ3gtc2VydmljZS1yZXF1ZXN0LWhhc2gnLCBzaGExKHJlc3BvbnNlSGFzaCkpO1xyXG4gICAgICAgIGNvbnN0IG5ld1JlcXVlc3QgPSB0aGlzLmludGVyY2VwdGVkUmVzcG9uc2UucmVzcG9uc2UucmVxdWVzdC5jbG9uZSh7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6IG5ld0hlYWRlcnMsXHJcbiAgICAgICAgICAgIGJvZHk6IG5ld0RhdGEsXHJcbiAgICAgICAgICAgIG9ic2VydmU6ICdyZXNwb25zZScgYXMgJ2JvZHknLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhuZXdSZXF1ZXN0KTtcclxuICAgICAgICAvLyByZXR1cm4gdGhpcy5hcGkucG9zdCgnYXV0aCcpKG5ld1JlcXVlc3QuYm9keSwgbnVsbCwge1xyXG4gICAgICAgIC8vICAgICAgICAgaGVhZGVyczogbmV3SGVhZGVycyxcclxuICAgICAgICAvLyAgICAgICAgIC8vIEBUT0RPOiBIZWFkZXJzIG5vdCBzZXRcclxuICAgICAgICAvLyAgICAgfSkuc3Vic2NyaWJlKFxyXG4gICAgICAgIC8vICAgICAgICAgdGhpcy5pbnRlcmNlcHRlZFJlc3BvbnNlLnN1YmplY3QsXHJcbiAgICAgICAgLy8gICAgICAgICByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5tZmEubWZhUmVzcG9uc2UgPSByZXNwb25zZS5oZWFkZXJzLmdldCgneC1zZXJ2aWNlLW1mYScpO1xyXG4gICAgICAgIC8vICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUocmVzcG9uc2Uuc3RhdHVzKTtcclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vICAgICAgICAgICAgIHN3aXRjaCAodGhpcy5nZXRTdGF0ZSgpKSB7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIC8vIGNhc2UgNDE3OlxyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAvLyAgICAgJHJvb3RTY29wZS5zdWNjZXNzTWVzc2FnZSA9IHt0ZXh0OiByZXNwb25zZS5kYXRhfTtcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgLy8gICAgICRtb2RhbEluc3RhbmNlLmNsb3NlKCk7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIC8vICAgICBicmVhaztcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgY2FzZSA0MTg6XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5fbWVzc2FnZXMuc2V0SW50ZXJjZXB0TWVzc2FnZSgnSW52YWxpZCBBdXRoZW50aWNhdGlvbiBDb2RlJyk7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIC8vIGNhc2UgNDE5OlxyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAvLyAgICAgJHJvb3RTY29wZS5pbnRlcmNlcHRNZXNzYWdlID0ge3RleHQ6ICdFbmFibGUgTXVsdGktRmFjdG9yIEF1dGhlbnRpY2F0aW9uJ307XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIC8vICAgICAkKCdkaXYubW9kYWwtbGcnKS5hZGRDbGFzcygnbW9kYWwtbGcnKS5yZW1vdmVDbGFzcygnbW9kYWwtc20nKTtcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgLy8gICAgIGJyZWFrO1xyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAvLyBjYXNlIDQyMTpcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgLy8gICAgICRyb290U2NvcGUucGFzc3dvcmRQb2xpY3kgPSB7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIC8vICAgICAgICAgcmVzcG9uc2U6IHJlc3BvbnNlLFxyXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAvLyAgICAgICAgIGRlZmVycmVkOiAkcm9vdFNjb3BlLnR3b1N0ZXAuZGVmZXJyZWRcclxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgLy8gICAgIH07XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIC8vICAgICAkcm9vdFNjb3BlLmludGVyY2VwdE1lc3NhZ2UgPSB7dGV4dDogXCJZb3VyIHBhc3N3b3JkIGRvZXMgbm90IGNvbXBseSB3aXRoIHRoZSBsYXRlc3QgQ29yZSBwYXNzd29yZCBwb2xpY3ksIGVuZm9yY2VkIGluIFNlcHRlbWJlciAyMDE5LiBQbGVhc2UgdXBkYXRlIHlvdXIgcGFzc3dvcmQgdG8gZW5zdXJlIHRoZSB1dG1vc3Qgc2VjdXJpdHkgb2YgdGhpcyBhcHBsaWNhdGlvbi5cIn07XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIC8vICAgICAkaW5qZWN0b3IuaW52b2tlKFsncGFzc3dvcmRQb2xpY3lNb2RhbCcsIGZ1bmN0aW9uIChwYXNzd29yZFBvbGljeU1vZGFsKSB7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIC8vICAgICAgICAgcGFzc3dvcmRQb2xpY3lNb2RhbC5zaG93KCk7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIC8vICAgICB9XSk7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIC8vICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgLy8gICAgICAgICAgICAgfVxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gICAgICAgICB9XHJcbiAgICAgICAgLy8gICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SW50ZXJjZXB0TWVzc2FnZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5tZXNzYWdlcy5nZXRJbnRlcmNlcHRNZXNzYWdlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2xvc2VNb2RhbCgkZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICB0aGlzLmJzTW9kYWxSZWYuaGlkZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFR5cGUodHlwZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5tZmEudHlwZSA9IHR5cGU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VHlwZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5tZmEudHlwZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRTdGF0ZShzdGF0ZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5tZmEuc3RhdGUgPSBzdGF0ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRTdGF0ZSgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm1mYS5zdGF0ZTtcclxuICAgIH1cclxuXHJcbiAgICBpc01GQUVuYWJsZWQoZ29vZ2xlOiBzdHJpbmcpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuIl19