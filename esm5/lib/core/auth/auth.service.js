/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/auth/auth.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, forkJoin, throwError } from 'rxjs';
import { APP_CONFIG } from '../../app.config';
import { TabsModule } from 'ngx-bootstrap';
import { Angulartics2 } from 'angulartics2';
import { ParserService } from '../parser.service';
import { CacheService } from '../cache.service';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { PageService } from '../pages/page.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common/http";
import * as i3 from "angulartics2";
import * as i4 from "../parser.service";
import * as i5 from "../cache.service";
import * as i6 from "../pages/page.service";
var AuthService = /** @class */ (function () {
    function AuthService(router, api, angulartics, parser, cache, pages) {
        this.router = router;
        this.api = api;
        this.angulartics = angulartics;
        this.parser = parser;
        this.cache = cache;
        this.pages = pages;
        this.authChangeEmit = new BehaviorSubject(true);
        this.failedLogins = 0;
        this.loginLimit = 4;
    }
    /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    AuthService.prototype.canActivate = /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    function (next, state) {
        if (!this.authResumed) {
            this.resume();
        }
        if (this.isAuthenticated()) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    /**
     * @return {?}
     */
    AuthService.prototype.isAuthenticated = /**
     * @return {?}
     */
    function () {
        return this.getUser() || this.cookiesExist();
    };
    /**
     * @return {?}
     */
    AuthService.prototype.getUser = /**
     * @return {?}
     */
    function () {
        return this.user;
    };
    /**
     * @param {?} username
     * @param {?} password
     * @param {?=} recaptchaResponse
     * @param {?=} authToken
     * @return {?}
     */
    AuthService.prototype.login = /**
     * @param {?} username
     * @param {?} password
     * @param {?=} recaptchaResponse
     * @param {?=} authToken
     * @return {?}
     */
    function (username, password, recaptchaResponse, authToken) {
        var _this = this;
        this.reset();
        /** @type {?} */
        var user = {
            username: username,
            password: password,
            recaptchaResponse: recaptchaResponse,
            authToken: authToken,
            mfa_token: this.mfaToken,
            google2step: this.google2step
        };
        return this.api.post('auth', user, { observe: 'response' })
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            _this.user = response.body;
            APP_CONFIG.authHeaders.xUserName = response.headers.get('x-service-user-name');
            APP_CONFIG.authHeaders.xUserToken = response.headers.get('x-service-user-token');
            document.cookie = 'x-service-user-name=' + response.headers.get('x-service-user-name') + ';path=/';
            document.cookie = 'x-service-user-token=' + response.headers.get('x-service-user-token') + ';path=/';
            if (response.headers.get('x-service-info')) {
                /** @type {?} */
                var serviceInfo = JSON.parse(response.headers.get('x-service-info'));
                _this.thirdPartyOnline = serviceInfo.thirdPartyOnline || false;
            }
            return _this.initialise().subscribe((/**
             * @return {?}
             */
            function () {
                _this.angulartics.eventTrack.next({
                    action: 'Auth succeeded',
                });
                return _this.router.navigate(['/']);
            }), (/**
             * @param {?} resp
             * @return {?}
             */
            function (resp) {
                _this.angulartics.eventTrack.next({
                    action: 'Auth failed',
                });
                if (_this.parser.getCookie('x-service-login-token')) {
                    if ((/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (_this.parser.getCookie('x-service-login-token'))))))))
                        > _this.failedLogins) {
                        _this.failedLogins = (/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (_this.parser.getCookie('x-service-login-token'))))))));
                    }
                }
                document.cookie = 'x-service-login-token=' + btoa((/** @type {?} */ ((/** @type {?} */ ((_this.failedLogins + 1)))))) + ';path=/';
                if (_this.failedLogins >= _this.loginLimit) {
                    _this.recaptchaEnabled = true;
                }
                return throwError('Failed to authenticate');
            }));
        })));
    };
    /**
     * @return {?}
     */
    AuthService.prototype.resume = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.authResumed = true;
        if (this.parser.getCookie('x-service-user-name') || this.parser.getCookie('x-service-emulated-user-name')) {
            APP_CONFIG.authHeaders.xUserName = this.parser.getCookie('x-service-user-name');
            APP_CONFIG.authHeaders.xUserToken = this.parser.getCookie('x-service-user-token');
            this.isEmulated = this.parser.getCookie('x-service-emulated-user-name') !== false;
            if (this.parser.getCookie('x-service-emulated-user-name')) {
                APP_CONFIG.authHeaders.xUserName = this.parser.getCookie('x-service-emulated-user-name');
            }
            return this.api.get('auth', { observe: 'response' }).toPromise().then((/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                _this.user = response.body;
                if (response.headers.get('x-service-info')) {
                    /** @type {?} */
                    var serviceInfo = JSON.parse(response.headers.get('x-service-info'));
                    _this.thirdPartyOnline = serviceInfo.thirdPartyOnline || false;
                }
                _this.authChangeEmit.next(true);
                return _this.initialise().subscribe();
            })).catch((/**
             * @return {?}
             */
            function () {
                _this.reset();
                return _this.router.navigate(['/login']);
            }));
        }
        else {
            if (this.parser.getCookie('x-service-login-token')) {
                if ((/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))))
                    > this.failedLogins) {
                    this.failedLogins = (/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))));
                }
            }
            if ((/** @type {?} */ (this.failedLogins)) >= this.loginLimit) {
                this.recaptchaEnabled = true;
            }
        }
        return false;
    };
    /**
     * @private
     * @return {?}
     */
    AuthService.prototype.getServiceProvider = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        return this.cache.getOneThroughCache('service-provider', 2, {}, false).subscribe((/**
         * @param {?} provider
         * @return {?}
         */
        function (provider) {
            _this.serviceProvider = provider;
            if (_this.serviceProvider.status !== 'ACTIVE') {
                throwError('Service provider offline');
                return _this.router.navigate(['/maintenance']);
            }
            _this.recaptchaEnabled = _this.serviceProvider.data.recaptchaEnabled;
        }));
    };
    /**
     * @return {?}
     */
    AuthService.prototype.initialise = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var observables = [];
        if (this.authCheckTimer) {
            clearInterval(this.authCheckTimer);
        }
        // @TODO: await
        this.getServiceProvider();
        if (this.isAuthenticated()) {
            if (!this.analyticsLoaded) {
                observables.push(this.api.get('analytics-custom-vars').toPromise().then((/**
                 * @param {?} response
                 * @return {?}
                 */
                function (response) {
                    var e_1, _a, e_2, _b;
                    try {
                        for (var _c = tslib_1.__values((/** @type {?} */ (response))), _d = _c.next(); !_d.done; _d = _c.next()) {
                            var dimension = _d.value;
                            try {
                                for (var _e = tslib_1.__values(Object.entries(dimension)), _f = _e.next(); !_f.done; _f = _e.next()) {
                                    var _g = tslib_1.__read(_f.value, 2), key = _g[0], value = _g[1];
                                    try {
                                        ga('set', key, value);
                                        if (key === 'dimension1') {
                                            try {
                                                ga('set', 'userId', value);
                                                if (gclog) {
                                                    gclog.setCustomAttribute('userId', value);
                                                }
                                            }
                                            catch (e) {
                                            }
                                            _this.angulartics.eventTrack.next({
                                                action: 'Analytics initialised',
                                            });
                                        }
                                    }
                                    catch (err) {
                                    }
                                }
                            }
                            catch (e_2_1) { e_2 = { error: e_2_1 }; }
                            finally {
                                try {
                                    if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                                }
                                finally { if (e_2) throw e_2.error; }
                            }
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                })));
            }
            this.authCheckTimer = setInterval((/**
             * @return {?}
             */
            function () {
                return _this.triggerPing();
            }), 2500);
        }
        return forkJoin(observables);
    };
    /**
     * @return {?}
     */
    AuthService.prototype.cookiesExist = /**
     * @return {?}
     */
    function () {
        return this.parser.getCookie('x-service-user-name') !== false
            || this.parser.getCookie('x-service-emulated-user-name') !== false;
    };
    /**
     * @return {?}
     */
    AuthService.prototype.triggerPing = /**
     * @return {?}
     */
    function () {
        if (!this.cookiesExist()) {
            this.reset();
            return this.router.navigate(['/login']);
        }
    };
    /**
     * @return {?}
     */
    AuthService.prototype.reset = /**
     * @return {?}
     */
    function () {
        this.user = false;
        this.cache.refresh();
        this.pages.refresh();
        this.analyticsLoaded = false;
        this.thirdPartyOnline = false;
        this.parser.deleteCookie('x-service-user-name', '/');
        this.parser.deleteCookie('x-service-emulated-user-name', '/');
        this.parser.deleteCookie('x-service-user-token', '/');
        this.parser.deleteCookie('TPSESSION', '/');
        APP_CONFIG.authHeaders = Object.assign({}, APP_CONFIG.authHeaders, {
            xUserName: null,
            xUserToken: null,
        });
        this.authChangeEmit.next(true);
        if (this.authCheckTimer) {
            clearInterval(this.authCheckTimer);
        }
    };
    /**
     * @return {?}
     */
    AuthService.prototype.logout = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.reset();
        this.router.navigate(['/login']);
        return this.api.delete('auth').subscribe((/**
         * @return {?}
         */
        function () {
            _this.angulartics.eventTrack.next({
                action: 'Auth closed',
            });
        }), (/**
         * @return {?}
         */
        function () {
            _this.angulartics.eventTrack.next({
                action: 'Auth closure failed',
            });
        }));
    };
    AuthService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
        { type: NgModule, args: [{
                    imports: [
                        TabsModule,
                    ],
                },] }
    ];
    /** @nocollapse */
    AuthService.ctorParameters = function () { return [
        { type: Router },
        { type: HttpClient },
        { type: Angulartics2 },
        { type: ParserService },
        { type: CacheService },
        { type: PageService }
    ]; };
    /** @nocollapse */ AuthService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthService_Factory() { return new AuthService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.HttpClient), i0.ɵɵinject(i3.Angulartics2), i0.ɵɵinject(i4.ParserService), i0.ɵɵinject(i5.CacheService), i0.ɵɵinject(i6.PageService)); }, token: AuthService, providedIn: "root" });
    return AuthService;
}());
export { AuthService };
if (false) {
    /** @type {?} */
    AuthService.prototype.authChangeEmit;
    /** @type {?} */
    AuthService.prototype.authResumed;
    /** @type {?} */
    AuthService.prototype.user;
    /** @type {?} */
    AuthService.prototype.mfaToken;
    /** @type {?} */
    AuthService.prototype.google2step;
    /** @type {?} */
    AuthService.prototype.analyticsLoaded;
    /** @type {?} */
    AuthService.prototype.thirdPartyOnline;
    /** @type {?} */
    AuthService.prototype.recaptchaEnabled;
    /** @type {?} */
    AuthService.prototype.serviceProvider;
    /** @type {?} */
    AuthService.prototype.authCheckTimer;
    /** @type {?} */
    AuthService.prototype.isEmulated;
    /** @type {?} */
    AuthService.prototype.failedLogins;
    /** @type {?} */
    AuthService.prototype.loginLimit;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.api;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.angulartics;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.parser;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.cache;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.pages;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9hdXRoL2F1dGguc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFFLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQXNDLE1BQU0sRUFBc0IsTUFBTSxpQkFBaUIsQ0FBQztBQUNqRyxPQUFPLEVBQUMsZUFBZSxFQUFFLFFBQVEsRUFBNEIsVUFBVSxFQUFDLE1BQU0sTUFBTSxDQUFDO0FBQ3JGLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxrQkFBa0IsQ0FBQztBQUM1QyxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxjQUFjLENBQUM7QUFDMUMsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQ2hELE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxrQkFBa0IsQ0FBQztBQUM5QyxPQUFPLEVBQUMsR0FBRyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbkMsT0FBTyxFQUFDLFVBQVUsRUFBZSxNQUFNLHNCQUFzQixDQUFDO0FBQzlELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSx1QkFBdUIsQ0FBQzs7Ozs7Ozs7QUFLbEQ7SUFpQ0kscUJBQ1ksTUFBYyxFQUNkLEdBQWUsRUFDZixXQUF5QixFQUN6QixNQUFxQixFQUNyQixLQUFtQixFQUNuQixLQUFrQjtRQUxsQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsUUFBRyxHQUFILEdBQUcsQ0FBWTtRQUNmLGdCQUFXLEdBQVgsV0FBVyxDQUFjO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQWU7UUFDckIsVUFBSyxHQUFMLEtBQUssQ0FBYztRQUNuQixVQUFLLEdBQUwsS0FBSyxDQUFhO1FBM0I5QixtQkFBYyxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBa0IzQyxpQkFBWSxHQUFHLENBQUMsQ0FBQztRQUNqQixlQUFVLEdBQUcsQ0FBQyxDQUFDO0lBVWYsQ0FBQzs7Ozs7O0lBRUQsaUNBQVc7Ozs7O0lBQVgsVUFBWSxJQUE0QixFQUFFLEtBQTBCO1FBQ2hFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ25CLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUNqQjtRQUNELElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRSxFQUFFO1lBQ3hCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDakMsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUVELHFDQUFlOzs7SUFBZjtRQUNJLE9BQU8sSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNqRCxDQUFDOzs7O0lBRUQsNkJBQU87OztJQUFQO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ3JCLENBQUM7Ozs7Ozs7O0lBRUQsMkJBQUs7Ozs7Ozs7SUFBTCxVQUFNLFFBQWdCLEVBQUUsUUFBZ0IsRUFBRSxpQkFBMEIsRUFBRSxTQUFrQjtRQUF4RixpQkF5REM7UUF4REcsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDOztZQUVQLElBQUksR0FBRztZQUNULFFBQVEsVUFBQTtZQUNSLFFBQVEsVUFBQTtZQUNSLGlCQUFpQixtQkFBQTtZQUNqQixTQUFTLFdBQUE7WUFDVCxTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDeEIsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXO1NBQ2hDO1FBRUQsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBdUIsTUFBTSxFQUFFLElBQUksRUFBRSxFQUFDLE9BQU8sRUFBRSxVQUFVLEVBQUMsQ0FBQzthQUMxRSxJQUFJLENBQ0QsR0FBRzs7OztRQUNDLFVBQUMsUUFBUTtZQUNMLEtBQUksQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUUxQixVQUFVLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQy9FLFVBQVUsQ0FBQyxXQUFXLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFFakYsUUFBUSxDQUFDLE1BQU0sR0FBRyxzQkFBc0IsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLFNBQVMsQ0FBQztZQUNuRyxRQUFRLENBQUMsTUFBTSxHQUFHLHVCQUF1QixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLEdBQUcsU0FBUyxDQUFDO1lBRXJHLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsRUFBRTs7b0JBQ2xDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3RFLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxXQUFXLENBQUMsZ0JBQWdCLElBQUksS0FBSyxDQUFDO2FBQ2pFO1lBRUQsT0FBTyxLQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsU0FBUzs7O1lBQUM7Z0JBQy9CLEtBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztvQkFDN0IsTUFBTSxFQUFFLGdCQUFnQjtpQkFDM0IsQ0FBQyxDQUFDO2dCQUNILE9BQU8sS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3ZDLENBQUM7Ozs7WUFBRSxVQUFDLElBQUk7Z0JBQ0osS0FBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO29CQUM3QixNQUFNLEVBQUUsYUFBYTtpQkFDeEIsQ0FBQyxDQUFDO2dCQUVILElBQUksS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsdUJBQXVCLENBQUMsRUFBRTtvQkFDaEQsSUFBSSxtQkFBQSxtQkFBQSxJQUFJLENBQUMsbUJBQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsdUJBQXVCLENBQUMsRUFBVSxDQUFDLEVBQVcsRUFBVTswQkFDakYsS0FBSSxDQUFDLFlBQVksRUFBRTt3QkFDckIsS0FBSSxDQUFDLFlBQVksR0FBRyxtQkFBQSxtQkFBQSxJQUFJLENBQUMsbUJBQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsdUJBQXVCLENBQUMsRUFBVSxDQUFDLEVBQVcsRUFBVSxDQUFDO3FCQUMzRztpQkFDSjtnQkFDRCxRQUFRLENBQUMsTUFBTSxHQUFHLHdCQUF3QixHQUFHLElBQUksQ0FBQyxtQkFBQSxtQkFBQSxDQUFDLEtBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLEVBQVcsRUFBVSxDQUFDLEdBQUcsU0FBUyxDQUFDO2dCQUU1RyxJQUFJLEtBQUksQ0FBQyxZQUFZLElBQUksS0FBSSxDQUFDLFVBQVUsRUFBRTtvQkFDdEMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztpQkFDaEM7Z0JBRUQsT0FBTyxVQUFVLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUNoRCxDQUFDLEVBQUMsQ0FBQztRQUVQLENBQUMsRUFDSixDQUNKLENBQUM7SUFDVixDQUFDOzs7O0lBRUQsNEJBQU07OztJQUFOO1FBQUEsaUJBd0NDO1FBdkNHLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyw4QkFBOEIsQ0FBQyxFQUFFO1lBRXZHLFVBQVUsQ0FBQyxXQUFXLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDaEYsVUFBVSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUNsRixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUFDLEtBQUssS0FBSyxDQUFDO1lBRWxGLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsOEJBQThCLENBQUMsRUFBRTtnQkFDdkQsVUFBVSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsOEJBQThCLENBQUMsQ0FBQzthQUM1RjtZQUVELE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEVBQUMsT0FBTyxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSTs7OztZQUFDLFVBQUMsUUFBUTtnQkFDekUsS0FBSSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUUxQixJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLEVBQUU7O3dCQUNsQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO29CQUN0RSxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsV0FBVyxDQUFDLGdCQUFnQixJQUFJLEtBQUssQ0FBQztpQkFDakU7Z0JBRUQsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRS9CLE9BQU8sS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBRXpDLENBQUMsRUFBQyxDQUFDLEtBQUs7OztZQUFDO2dCQUNMLEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDYixPQUFPLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUM1QyxDQUFDLEVBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLEVBQUU7Z0JBQ2hELElBQUksbUJBQUEsbUJBQUEsSUFBSSxDQUFDLG1CQUFBLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLEVBQVUsQ0FBQyxFQUFXLEVBQVU7c0JBQ2pGLElBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsbUJBQUEsbUJBQUEsSUFBSSxDQUFDLG1CQUFBLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLEVBQVUsQ0FBQyxFQUFXLEVBQVUsQ0FBQztpQkFDM0c7YUFDSjtZQUNELElBQUksbUJBQUEsSUFBSSxDQUFDLFlBQVksRUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7YUFDaEM7U0FDSjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7O0lBRU8sd0NBQWtCOzs7O0lBQTFCO1FBQUEsaUJBU0M7UUFSRyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxLQUFLLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxRQUFRO1lBQ3JGLEtBQUksQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDO1lBQ2hDLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEtBQUssUUFBUSxFQUFFO2dCQUMxQyxVQUFVLENBQUMsMEJBQTBCLENBQUMsQ0FBQztnQkFDdkMsT0FBTyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7YUFDakQ7WUFDRCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDdkUsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsZ0NBQVU7OztJQUFWO1FBQUEsaUJBMENDOztZQXpDUyxXQUFXLEdBQUcsRUFBRTtRQUV0QixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDckIsYUFBYSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUN0QztRQUVELGVBQWU7UUFDZixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUUxQixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUUsRUFBRTtZQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFDdkIsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDLElBQUk7Ozs7Z0JBQUMsVUFBQSxRQUFROzs7d0JBQzVFLCtCQUF3QixtQkFBQSxRQUFRLEVBQU8sNkNBQUU7NEJBQXBDLElBQU0sU0FBUyxXQUFBOztnQ0FDaEIsS0FBMkIsSUFBQSxLQUFBLGlCQUFBLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUEsZ0JBQUEsNEJBQUU7b0NBQTNDLElBQUEsZ0NBQVksRUFBWCxXQUFHLEVBQUUsYUFBSztvQ0FDbEIsSUFBSTt3Q0FDQSxFQUFFLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQzt3Q0FDdEIsSUFBSSxHQUFHLEtBQUssWUFBWSxFQUFFOzRDQUN0QixJQUFJO2dEQUNBLEVBQUUsQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dEQUMzQixJQUFJLEtBQUssRUFBRTtvREFDUCxLQUFLLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO2lEQUM3Qzs2Q0FDSjs0Q0FBQyxPQUFPLENBQUMsRUFBRTs2Q0FDWDs0Q0FDRCxLQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7Z0RBQzdCLE1BQU0sRUFBRSx1QkFBdUI7NkNBQ2xDLENBQUMsQ0FBQzt5Q0FDTjtxQ0FDSjtvQ0FBQyxPQUFPLEdBQUcsRUFBRTtxQ0FDYjtpQ0FDSjs7Ozs7Ozs7O3lCQUNKOzs7Ozs7Ozs7Z0JBQ0wsQ0FBQyxFQUFDLENBQUMsQ0FBQzthQUNQO1lBRUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxXQUFXOzs7WUFBQztnQkFDOUIsT0FBTyxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUIsQ0FBQyxHQUFFLElBQUksQ0FBQyxDQUFDO1NBQ1o7UUFFRCxPQUFPLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7O0lBRUQsa0NBQVk7OztJQUFaO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLEtBQUs7ZUFDdEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsOEJBQThCLENBQUMsS0FBSyxLQUFLLENBQUM7SUFDM0UsQ0FBQzs7OztJQUVELGlDQUFXOzs7SUFBWDtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUU7WUFDdEIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7U0FDM0M7SUFDTCxDQUFDOzs7O0lBRUQsMkJBQUs7OztJQUFMO1FBQ0ksSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7UUFFbEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBRXJCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFFOUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMscUJBQXFCLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsOEJBQThCLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsc0JBQXNCLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRTNDLFVBQVUsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLFdBQVcsRUFBRTtZQUMvRCxTQUFTLEVBQUUsSUFBSTtZQUNmLFVBQVUsRUFBRSxJQUFJO1NBQ25CLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRS9CLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNyQixhQUFhLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ3RDO0lBQ0wsQ0FBQzs7OztJQUVELDRCQUFNOzs7SUFBTjtRQUFBLGlCQVlDO1FBWEcsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUzs7O1FBQUM7WUFDckMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO2dCQUM3QixNQUFNLEVBQUUsYUFBYTthQUN4QixDQUFDLENBQUM7UUFDUCxDQUFDOzs7UUFBRTtZQUNDLEtBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztnQkFDN0IsTUFBTSxFQUFFLHFCQUFxQjthQUNoQyxDQUFDLENBQUM7UUFDUCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7O2dCQTVRSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCO2dCQUVBLFFBQVEsU0FBQztvQkFDTixPQUFPLEVBQUU7d0JBQ0wsVUFBVTtxQkFDYjtpQkFDSjs7OztnQkF0QjRDLE1BQU07Z0JBUTNDLFVBQVU7Z0JBSlYsWUFBWTtnQkFDWixhQUFhO2dCQUNiLFlBQVk7Z0JBR1osV0FBVzs7O3NCQVZuQjtDQTZSQyxBQTlRRCxJQThRQztTQXBRWSxXQUFXOzs7SUFFcEIscUNBQTJDOztJQUUzQyxrQ0FBcUI7O0lBQ3JCLDJCQUFVOztJQUNWLCtCQUFpQjs7SUFDakIsa0NBQXFCOztJQUNyQixzQ0FBeUI7O0lBQ3pCLHVDQUEwQjs7SUFDMUIsdUNBQTBCOztJQUMxQixzQ0FNRTs7SUFDRixxQ0FBb0I7O0lBQ3BCLGlDQUFvQjs7SUFDcEIsbUNBQWlCOztJQUNqQixpQ0FBZTs7Ozs7SUFHWCw2QkFBc0I7Ozs7O0lBQ3RCLDBCQUF1Qjs7Ozs7SUFDdkIsa0NBQWlDOzs7OztJQUNqQyw2QkFBNkI7Ozs7O0lBQzdCLDRCQUEyQjs7Ozs7SUFDM0IsNEJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlLCBOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0FjdGl2YXRlZFJvdXRlU25hcHNob3QsIENhbkFjdGl2YXRlLCBSb3V0ZXIsIFJvdXRlclN0YXRlU25hcHNob3R9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQge0JlaGF2aW9yU3ViamVjdCwgZm9ya0pvaW4sIE9ic2VydmFibGUsIFN1YnNjcmlwdGlvbiwgdGhyb3dFcnJvcn0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0FQUF9DT05GSUd9IGZyb20gJy4uLy4uL2FwcC5jb25maWcnO1xuaW1wb3J0IHtUYWJzTW9kdWxlfSBmcm9tICduZ3gtYm9vdHN0cmFwJztcbmltcG9ydCB7QW5ndWxhcnRpY3MyfSBmcm9tICdhbmd1bGFydGljczInO1xuaW1wb3J0IHtQYXJzZXJTZXJ2aWNlfSBmcm9tICcuLi9wYXJzZXIuc2VydmljZSc7XG5pbXBvcnQge0NhY2hlU2VydmljZX0gZnJvbSAnLi4vY2FjaGUuc2VydmljZSc7XG5pbXBvcnQge21hcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHtIdHRwQ2xpZW50LCBIdHRwUmVzcG9uc2V9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7UGFnZVNlcnZpY2V9IGZyb20gJy4uL3BhZ2VzL3BhZ2Uuc2VydmljZSc7XG5cbmRlY2xhcmUgbGV0IGdhOiAocDogc3RyaW5nLCBwYWdlOiBzdHJpbmcsIHVybEFmdGVyUmVkaXJlY3RzPzogdW5rbm93bikgPT4gdm9pZDtcbmRlY2xhcmUgbGV0IGdjbG9nOiBhbnk7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW1xuICAgICAgICBUYWJzTW9kdWxlLFxuICAgIF0sXG59KVxuXG5leHBvcnQgY2xhc3MgQXV0aFNlcnZpY2UgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZSB7XG5cbiAgICBhdXRoQ2hhbmdlRW1pdCA9IG5ldyBCZWhhdmlvclN1YmplY3QodHJ1ZSk7XG5cbiAgICBhdXRoUmVzdW1lZDogYm9vbGVhbjtcbiAgICB1c2VyOiBhbnk7XG4gICAgbWZhVG9rZW46IHN0cmluZztcbiAgICBnb29nbGUyc3RlcDogYm9vbGVhbjtcbiAgICBhbmFseXRpY3NMb2FkZWQ6IGJvb2xlYW47XG4gICAgdGhpcmRQYXJ0eU9ubGluZTogYm9vbGVhbjtcbiAgICByZWNhcHRjaGFFbmFibGVkOiBib29sZWFuO1xuICAgIHNlcnZpY2VQcm92aWRlcjoge1xuICAgICAgICBuYW1lOiBzdHJpbmc7XG4gICAgICAgIGRhdGE6IGFueTtcbiAgICAgICAgc3RhdHVzOiBzdHJpbmc7XG4gICAgICAgIGxhbmd1YWdlczogYW55O1xuICAgICAgICBjb25maWd1cmF0aW9uOiBhbnk7XG4gICAgfTtcbiAgICBhdXRoQ2hlY2tUaW1lcjogYW55O1xuICAgIGlzRW11bGF0ZWQ6IGJvb2xlYW47XG4gICAgZmFpbGVkTG9naW5zID0gMDtcbiAgICBsb2dpbkxpbWl0ID0gNDtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgICAgICBwcml2YXRlIGFwaTogSHR0cENsaWVudCxcbiAgICAgICAgcHJpdmF0ZSBhbmd1bGFydGljczogQW5ndWxhcnRpY3MyLFxuICAgICAgICBwcml2YXRlIHBhcnNlcjogUGFyc2VyU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBjYWNoZTogQ2FjaGVTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIHBhZ2VzOiBQYWdlU2VydmljZVxuICAgICkge1xuICAgIH1cblxuICAgIGNhbkFjdGl2YXRlKG5leHQ6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KTogT2JzZXJ2YWJsZTxib29sZWFuPiB8IFByb21pc2U8Ym9vbGVhbj4gfCBib29sZWFuIHtcbiAgICAgICAgaWYgKCF0aGlzLmF1dGhSZXN1bWVkKSB7XG4gICAgICAgICAgICB0aGlzLnJlc3VtZSgpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmlzQXV0aGVudGljYXRlZCgpKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGlzQXV0aGVudGljYXRlZCgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VXNlcigpIHx8IHRoaXMuY29va2llc0V4aXN0KCk7XG4gICAgfVxuXG4gICAgZ2V0VXNlcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMudXNlcjtcbiAgICB9XG5cbiAgICBsb2dpbih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nLCByZWNhcHRjaGFSZXNwb25zZT86IHN0cmluZywgYXV0aFRva2VuPzogc3RyaW5nKTogT2JzZXJ2YWJsZTxTdWJzY3JpcHRpb24+IHtcbiAgICAgICAgdGhpcy5yZXNldCgpO1xuXG4gICAgICAgIGNvbnN0IHVzZXIgPSB7XG4gICAgICAgICAgICB1c2VybmFtZSxcbiAgICAgICAgICAgIHBhc3N3b3JkLFxuICAgICAgICAgICAgcmVjYXB0Y2hhUmVzcG9uc2UsXG4gICAgICAgICAgICBhdXRoVG9rZW4sXG4gICAgICAgICAgICBtZmFfdG9rZW46IHRoaXMubWZhVG9rZW4sXG4gICAgICAgICAgICBnb29nbGUyc3RlcDogdGhpcy5nb29nbGUyc3RlcFxuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiB0aGlzLmFwaS5wb3N0PEh0dHBSZXNwb25zZTxvYmplY3Q+PignYXV0aCcsIHVzZXIsIHtvYnNlcnZlOiAncmVzcG9uc2UnfSlcbiAgICAgICAgICAgIC5waXBlKFxuICAgICAgICAgICAgICAgIG1hcChcbiAgICAgICAgICAgICAgICAgICAgKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXIgPSByZXNwb25zZS5ib2R5O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBBUFBfQ09ORklHLmF1dGhIZWFkZXJzLnhVc2VyTmFtZSA9IHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCd4LXNlcnZpY2UtdXNlci1uYW1lJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBBUFBfQ09ORklHLmF1dGhIZWFkZXJzLnhVc2VyVG9rZW4gPSByZXNwb25zZS5oZWFkZXJzLmdldCgneC1zZXJ2aWNlLXVzZXItdG9rZW4nKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuY29va2llID0gJ3gtc2VydmljZS11c2VyLW5hbWU9JyArIHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCd4LXNlcnZpY2UtdXNlci1uYW1lJykgKyAnO3BhdGg9Lyc7XG4gICAgICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSAneC1zZXJ2aWNlLXVzZXItdG9rZW49JyArIHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCd4LXNlcnZpY2UtdXNlci10b2tlbicpICsgJztwYXRoPS8nO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuaGVhZGVycy5nZXQoJ3gtc2VydmljZS1pbmZvJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzZXJ2aWNlSW5mbyA9IEpTT04ucGFyc2UocmVzcG9uc2UuaGVhZGVycy5nZXQoJ3gtc2VydmljZS1pbmZvJykpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGhpcmRQYXJ0eU9ubGluZSA9IHNlcnZpY2VJbmZvLnRoaXJkUGFydHlPbmxpbmUgfHwgZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmluaXRpYWxpc2UoKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYW5ndWxhcnRpY3MuZXZlbnRUcmFjay5uZXh0KHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnQXV0aCBzdWNjZWVkZWQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy8nXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9LCAocmVzcCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYW5ndWxhcnRpY3MuZXZlbnRUcmFjay5uZXh0KHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnQXV0aCBmYWlsZWQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucGFyc2VyLmdldENvb2tpZSgneC1zZXJ2aWNlLWxvZ2luLXRva2VuJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGF0b2IodGhpcy5wYXJzZXIuZ2V0Q29va2llKCd4LXNlcnZpY2UtbG9naW4tdG9rZW4nKSBhcyBzdHJpbmcpIGFzIHVua25vd24gYXMgbnVtYmVyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+IHRoaXMuZmFpbGVkTG9naW5zKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZhaWxlZExvZ2lucyA9IGF0b2IodGhpcy5wYXJzZXIuZ2V0Q29va2llKCd4LXNlcnZpY2UtbG9naW4tdG9rZW4nKSBhcyBzdHJpbmcpIGFzIHVua25vd24gYXMgbnVtYmVyO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LmNvb2tpZSA9ICd4LXNlcnZpY2UtbG9naW4tdG9rZW49JyArIGJ0b2EoKHRoaXMuZmFpbGVkTG9naW5zICsgMSkgYXMgdW5rbm93biBhcyBzdHJpbmcpICsgJztwYXRoPS8nO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZmFpbGVkTG9naW5zID49IHRoaXMubG9naW5MaW1pdCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlY2FwdGNoYUVuYWJsZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKCdGYWlsZWQgdG8gYXV0aGVudGljYXRlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICk7XG4gICAgfVxuXG4gICAgcmVzdW1lKCkge1xuICAgICAgICB0aGlzLmF1dGhSZXN1bWVkID0gdHJ1ZTtcbiAgICAgICAgaWYgKHRoaXMucGFyc2VyLmdldENvb2tpZSgneC1zZXJ2aWNlLXVzZXItbmFtZScpIHx8IHRoaXMucGFyc2VyLmdldENvb2tpZSgneC1zZXJ2aWNlLWVtdWxhdGVkLXVzZXItbmFtZScpKSB7XG5cbiAgICAgICAgICAgIEFQUF9DT05GSUcuYXV0aEhlYWRlcnMueFVzZXJOYW1lID0gdGhpcy5wYXJzZXIuZ2V0Q29va2llKCd4LXNlcnZpY2UtdXNlci1uYW1lJyk7XG4gICAgICAgICAgICBBUFBfQ09ORklHLmF1dGhIZWFkZXJzLnhVc2VyVG9rZW4gPSB0aGlzLnBhcnNlci5nZXRDb29raWUoJ3gtc2VydmljZS11c2VyLXRva2VuJyk7XG4gICAgICAgICAgICB0aGlzLmlzRW11bGF0ZWQgPSB0aGlzLnBhcnNlci5nZXRDb29raWUoJ3gtc2VydmljZS1lbXVsYXRlZC11c2VyLW5hbWUnKSAhPT0gZmFsc2U7XG5cbiAgICAgICAgICAgIGlmICh0aGlzLnBhcnNlci5nZXRDb29raWUoJ3gtc2VydmljZS1lbXVsYXRlZC11c2VyLW5hbWUnKSkge1xuICAgICAgICAgICAgICAgIEFQUF9DT05GSUcuYXV0aEhlYWRlcnMueFVzZXJOYW1lID0gdGhpcy5wYXJzZXIuZ2V0Q29va2llKCd4LXNlcnZpY2UtZW11bGF0ZWQtdXNlci1uYW1lJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwaS5nZXQoJ2F1dGgnLCB7b2JzZXJ2ZTogJ3Jlc3BvbnNlJ30pLnRvUHJvbWlzZSgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy51c2VyID0gcmVzcG9uc2UuYm9keTtcblxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5oZWFkZXJzLmdldCgneC1zZXJ2aWNlLWluZm8nKSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBzZXJ2aWNlSW5mbyA9IEpTT04ucGFyc2UocmVzcG9uc2UuaGVhZGVycy5nZXQoJ3gtc2VydmljZS1pbmZvJykpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnRoaXJkUGFydHlPbmxpbmUgPSBzZXJ2aWNlSW5mby50aGlyZFBhcnR5T25saW5lIHx8IGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHRoaXMuYXV0aENoYW5nZUVtaXQubmV4dCh0cnVlKTtcblxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmluaXRpYWxpc2UoKS5zdWJzY3JpYmUoKTtcblxuICAgICAgICAgICAgfSkuY2F0Y2goKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMucmVzZXQoKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbG9naW4nXSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnBhcnNlci5nZXRDb29raWUoJ3gtc2VydmljZS1sb2dpbi10b2tlbicpKSB7XG4gICAgICAgICAgICAgICAgaWYgKGF0b2IodGhpcy5wYXJzZXIuZ2V0Q29va2llKCd4LXNlcnZpY2UtbG9naW4tdG9rZW4nKSBhcyBzdHJpbmcpIGFzIHVua25vd24gYXMgbnVtYmVyXG4gICAgICAgICAgICAgICAgICAgID4gdGhpcy5mYWlsZWRMb2dpbnMpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mYWlsZWRMb2dpbnMgPSBhdG9iKHRoaXMucGFyc2VyLmdldENvb2tpZSgneC1zZXJ2aWNlLWxvZ2luLXRva2VuJykgYXMgc3RyaW5nKSBhcyB1bmtub3duIGFzIG51bWJlcjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodGhpcy5mYWlsZWRMb2dpbnMgYXMgbnVtYmVyID49IHRoaXMubG9naW5MaW1pdCkge1xuICAgICAgICAgICAgICAgIHRoaXMucmVjYXB0Y2hhRW5hYmxlZCA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0U2VydmljZVByb3ZpZGVyKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jYWNoZS5nZXRPbmVUaHJvdWdoQ2FjaGUoJ3NlcnZpY2UtcHJvdmlkZXInLCAyLCB7fSwgZmFsc2UpLnN1YnNjcmliZShwcm92aWRlciA9PiB7XG4gICAgICAgICAgICB0aGlzLnNlcnZpY2VQcm92aWRlciA9IHByb3ZpZGVyO1xuICAgICAgICAgICAgaWYgKHRoaXMuc2VydmljZVByb3ZpZGVyLnN0YXR1cyAhPT0gJ0FDVElWRScpIHtcbiAgICAgICAgICAgICAgICB0aHJvd0Vycm9yKCdTZXJ2aWNlIHByb3ZpZGVyIG9mZmxpbmUnKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbWFpbnRlbmFuY2UnXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnJlY2FwdGNoYUVuYWJsZWQgPSB0aGlzLnNlcnZpY2VQcm92aWRlci5kYXRhLnJlY2FwdGNoYUVuYWJsZWQ7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGluaXRpYWxpc2UoKSB7XG4gICAgICAgIGNvbnN0IG9ic2VydmFibGVzID0gW107XG5cbiAgICAgICAgaWYgKHRoaXMuYXV0aENoZWNrVGltZXIpIHtcbiAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5hdXRoQ2hlY2tUaW1lcik7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBAVE9ETzogYXdhaXRcbiAgICAgICAgdGhpcy5nZXRTZXJ2aWNlUHJvdmlkZXIoKTtcblxuICAgICAgICBpZiAodGhpcy5pc0F1dGhlbnRpY2F0ZWQoKSkge1xuICAgICAgICAgICAgaWYgKCF0aGlzLmFuYWx5dGljc0xvYWRlZCkge1xuICAgICAgICAgICAgICAgIG9ic2VydmFibGVzLnB1c2godGhpcy5hcGkuZ2V0KCdhbmFseXRpY3MtY3VzdG9tLXZhcnMnKS50b1Byb21pc2UoKS50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBkaW1lbnNpb24gb2YgcmVzcG9uc2UgYXMgYW55KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IFtrZXksIHZhbHVlXSBvZiBPYmplY3QuZW50cmllcyhkaW1lbnNpb24pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2EoJ3NldCcsIGtleSwgdmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoa2V5ID09PSAnZGltZW5zaW9uMScpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2EoJ3NldCcsICd1c2VySWQnLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGdjbG9nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdjbG9nLnNldEN1c3RvbUF0dHJpYnV0ZSgndXNlcklkJywgdmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYW5ndWxhcnRpY3MuZXZlbnRUcmFjay5uZXh0KHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdBbmFseXRpY3MgaW5pdGlhbGlzZWQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuYXV0aENoZWNrVGltZXIgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMudHJpZ2dlclBpbmcoKTtcbiAgICAgICAgICAgIH0sIDI1MDApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGZvcmtKb2luKG9ic2VydmFibGVzKTtcbiAgICB9XG5cbiAgICBjb29raWVzRXhpc3QoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLnBhcnNlci5nZXRDb29raWUoJ3gtc2VydmljZS11c2VyLW5hbWUnKSAhPT0gZmFsc2VcbiAgICAgICAgICAgIHx8IHRoaXMucGFyc2VyLmdldENvb2tpZSgneC1zZXJ2aWNlLWVtdWxhdGVkLXVzZXItbmFtZScpICE9PSBmYWxzZTtcbiAgICB9XG5cbiAgICB0cmlnZ2VyUGluZygpIHtcbiAgICAgICAgaWYgKCF0aGlzLmNvb2tpZXNFeGlzdCgpKSB7XG4gICAgICAgICAgICB0aGlzLnJlc2V0KCk7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbG9naW4nXSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXNldCgpIHtcbiAgICAgICAgdGhpcy51c2VyID0gZmFsc2U7XG5cbiAgICAgICAgdGhpcy5jYWNoZS5yZWZyZXNoKCk7XG4gICAgICAgIHRoaXMucGFnZXMucmVmcmVzaCgpO1xuXG4gICAgICAgIHRoaXMuYW5hbHl0aWNzTG9hZGVkID0gZmFsc2U7XG4gICAgICAgIHRoaXMudGhpcmRQYXJ0eU9ubGluZSA9IGZhbHNlO1xuXG4gICAgICAgIHRoaXMucGFyc2VyLmRlbGV0ZUNvb2tpZSgneC1zZXJ2aWNlLXVzZXItbmFtZScsICcvJyk7XG4gICAgICAgIHRoaXMucGFyc2VyLmRlbGV0ZUNvb2tpZSgneC1zZXJ2aWNlLWVtdWxhdGVkLXVzZXItbmFtZScsICcvJyk7XG4gICAgICAgIHRoaXMucGFyc2VyLmRlbGV0ZUNvb2tpZSgneC1zZXJ2aWNlLXVzZXItdG9rZW4nLCAnLycpO1xuICAgICAgICB0aGlzLnBhcnNlci5kZWxldGVDb29raWUoJ1RQU0VTU0lPTicsICcvJyk7XG5cbiAgICAgICAgQVBQX0NPTkZJRy5hdXRoSGVhZGVycyA9IE9iamVjdC5hc3NpZ24oe30sIEFQUF9DT05GSUcuYXV0aEhlYWRlcnMsIHtcbiAgICAgICAgICAgIHhVc2VyTmFtZTogbnVsbCxcbiAgICAgICAgICAgIHhVc2VyVG9rZW46IG51bGwsXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuYXV0aENoYW5nZUVtaXQubmV4dCh0cnVlKTtcblxuICAgICAgICBpZiAodGhpcy5hdXRoQ2hlY2tUaW1lcikge1xuICAgICAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLmF1dGhDaGVja1RpbWVyKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGxvZ291dCgpIHtcbiAgICAgICAgdGhpcy5yZXNldCgpO1xuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBpLmRlbGV0ZSgnYXV0aCcpLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmFuZ3VsYXJ0aWNzLmV2ZW50VHJhY2submV4dCh7XG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnQXV0aCBjbG9zZWQnLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuYW5ndWxhcnRpY3MuZXZlbnRUcmFjay5uZXh0KHtcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdBdXRoIGNsb3N1cmUgZmFpbGVkJyxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbn1cbiJdfQ==