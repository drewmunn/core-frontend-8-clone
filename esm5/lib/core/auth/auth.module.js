/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/auth/auth.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { SpinnerModule } from '../utils/spinner/spinner.module';
import { AlertModule, TabsModule } from 'ngx-bootstrap';
import { MfaModalComponent } from './mfa-modal/mfa-modal.component';
var ɵ0 = { breadcrumbs: ['Login'] };
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [LoginComponent, MfaModalComponent],
                    imports: [
                        CommonModule,
                        RouterModule.forChild([
                            { path: '', pathMatch: 'full', redirectTo: 'login' },
                            {
                                path: 'login', component: LoginComponent,
                                data: ɵ0
                            },
                        ]),
                        FormsModule,
                        ReactiveFormsModule,
                        MatProgressBarModule,
                        MatProgressSpinnerModule,
                        MatButtonModule,
                        MatIconModule,
                        SpinnerModule,
                        AlertModule,
                        TabsModule
                    ]
                },] }
    ];
    return AuthModule;
}());
export { AuthModule };
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2F1dGgvYXV0aC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBQ3ZELE9BQU8sRUFBQyxXQUFXLEVBQUUsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNoRSxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNwRSxPQUFPLEVBQUMsd0JBQXdCLEVBQUMsTUFBTSxvQ0FBb0MsQ0FBQztBQUM1RSxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDekQsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBQ3JELE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxpQ0FBaUMsQ0FBQztBQUM5RCxPQUFPLEVBQUMsV0FBVyxFQUFFLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN0RCxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxpQ0FBaUMsQ0FBQztTQVU1QyxFQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxFQUFDO0FBUjlDO0lBQUE7SUF1QkEsQ0FBQzs7Z0JBdkJBLFFBQVEsU0FBQztvQkFDTixZQUFZLEVBQUUsQ0FBQyxjQUFjLEVBQUUsaUJBQWlCLENBQUM7b0JBQ2pELE9BQU8sRUFBRTt3QkFDTCxZQUFZO3dCQUNaLFlBQVksQ0FBQyxRQUFRLENBQUM7NEJBQ2xCLEVBQUMsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUM7NEJBQ2xEO2dDQUNJLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLGNBQWM7Z0NBQ3hDLElBQUksSUFBMEI7NkJBQ2pDO3lCQUNKLENBQUM7d0JBQ0YsV0FBVzt3QkFDWCxtQkFBbUI7d0JBQ25CLG9CQUFvQjt3QkFDcEIsd0JBQXdCO3dCQUN4QixlQUFlO3dCQUNmLGFBQWE7d0JBQ2IsYUFBYTt3QkFDYixXQUFXO3dCQUNYLFVBQVU7cUJBQ2I7aUJBQ0o7O0lBRUQsaUJBQUM7Q0FBQSxBQXZCRCxJQXVCQztTQURZLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7Um91dGVyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQge0xvZ2luQ29tcG9uZW50fSBmcm9tICcuL2xvZ2luL2xvZ2luLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7Rm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHtNYXRQcm9ncmVzc0Jhck1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcHJvZ3Jlc3MtYmFyJztcclxuaW1wb3J0IHtNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3Byb2dyZXNzLXNwaW5uZXInO1xyXG5pbXBvcnQge01hdEJ1dHRvbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYnV0dG9uJztcclxuaW1wb3J0IHtNYXRJY29uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9pY29uJztcclxuaW1wb3J0IHtTcGlubmVyTW9kdWxlfSBmcm9tICcuLi91dGlscy9zcGlubmVyL3NwaW5uZXIubW9kdWxlJztcclxuaW1wb3J0IHtBbGVydE1vZHVsZSwgVGFic01vZHVsZX0gZnJvbSAnbmd4LWJvb3RzdHJhcCc7XHJcbmltcG9ydCB7TWZhTW9kYWxDb21wb25lbnR9IGZyb20gJy4vbWZhLW1vZGFsL21mYS1tb2RhbC5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW0xvZ2luQ29tcG9uZW50LCBNZmFNb2RhbENvbXBvbmVudF0sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIFJvdXRlck1vZHVsZS5mb3JDaGlsZChbXHJcbiAgICAgICAgICAgIHtwYXRoOiAnJywgcGF0aE1hdGNoOiAnZnVsbCcsIHJlZGlyZWN0VG86ICdsb2dpbid9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBwYXRoOiAnbG9naW4nLCBjb21wb25lbnQ6IExvZ2luQ29tcG9uZW50LFxyXG4gICAgICAgICAgICAgICAgZGF0YToge2JyZWFkY3J1bWJzOiBbJ0xvZ2luJ119XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXSksXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuICAgICAgICBNYXRQcm9ncmVzc0Jhck1vZHVsZSxcclxuICAgICAgICBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGUsXHJcbiAgICAgICAgTWF0QnV0dG9uTW9kdWxlLFxyXG4gICAgICAgIE1hdEljb25Nb2R1bGUsXHJcbiAgICAgICAgU3Bpbm5lck1vZHVsZSxcclxuICAgICAgICBBbGVydE1vZHVsZSxcclxuICAgICAgICBUYWJzTW9kdWxlXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBdXRoTW9kdWxlIHtcclxufVxyXG4iXX0=