/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/auth/login/login.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { APP_CONFIG } from '../../../app.config';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { SpinnerService } from '../../utils/spinner/spinner.service';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, spinner) {
        this.authService = authService;
        this.spinner = spinner;
        this.loginForm = new FormGroup({
            username: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
        this.appName = APP_CONFIG.appName;
        this.copyright = APP_CONFIG.copyright;
    }
    /**
     * @return {?}
     */
    LoginComponent.prototype.login = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.errorMessage = false;
        this.spinner.show('login');
        return this.authService.login(this.loginForm.get('username').value, this.loginForm.get('password').value).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            _this.spinner.hide('login');
            _this.loading = false;
            _this.errorMessage = "<p>Please check your email address and password are correct, and try again.</p>\n                                    <p>Error reference: " + error.headers.get('x-service-request-id') + "</small></p>";
            return throwError("Error Code: " + error.status + "\\nMessage: " + error.message);
        }))).subscribe((/**
         * @return {?}
         */
        function () {
            _this.spinner.hide('login');
            _this.loading = false;
        }));
    };
    /**
     * @return {?}
     */
    LoginComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    LoginComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-login',
                    template: "<div class=\"page-wrapper\">\n    <div class=\"page-inner bg-brand-gradient\">\n        <div class=\"page-content-wrapper bg-transparent m-0\">\n            <div class=\"height-10 w-100 shadow-lg px-4 bg-brand-gradient\">\n                <div class=\"d-flex align-items-center container p-0\">\n                    <div class=\"page-logo width-mobile-auto m-0 align-items-center justify-content-center p-0 bg-transparent bg-img-none shadow-0 height-9\">\n                        <a href=\"javascript:void(0)\" class=\"page-logo-link press-scale-down d-flex align-items-center\">\n                            <span class=\"page-logo-text mr-1\">{{appName}}</span>\n                        </a>\n                    </div>\n                </div>\n            </div>\n            <div class=\"flex-1\"\n                 style=\"background: url(assets/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover;\">\n                <div class=\"container py-4 py-lg-5 my-lg-5 px-4 px-sm-0\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12 col-md-6 col-lg-5 col-xl-4 offset-xl-4 offset-md-3\">\n                            <h1 class=\"text-white fw-300 mb-3 d-sm-block d-md-none\">\n                                Secure login\n                            </h1>\n\n                            <div class=\"card p-4 rounded-plus bg-faded\">\n\n                                <form [formGroup]=\"loginForm\" (ngSubmit)=\"login()\">\n                                    <div class=\"form-group\">\n                                        <label class=\"form-label\">Username</label>\n                                        <input type=\"email\"\n                                               formControlName=\"username\"\n                                               name=\"username\"\n                                               class=\"form-control form-control-lg\"\n                                               required=\"true\"\n                                               [required]=\"true\"/>\n                                        <div class=\"invalid-feedback\">No, you missed this one.</div>\n                                        <div class=\"help-block\">Your username</div>\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label class=\"form-label\">Password</label>\n                                        <input type=\"password\"\n                                               formControlName=\"password\"\n                                               name=\"password\"\n                                               class=\"form-control form-control-lg\"\n                                               required=\"true\"\n                                               [required]=\"true\"/>\n                                        <div class=\"invalid-feedback\">Sorry, you missed this one.</div>\n                                        <div class=\"help-block\">Your password</div>\n                                    </div>\n\n                                    <div class=\"row no-gutters\">\n                                        <div class=\"col-lg-12 pl-lg-1 my-2\">\n                                            <button id=\"js-login-btn\" type=\"submit\"\n                                                    class=\"btn btn-primary btn-block btn-lg\"\n                                                    [class.spinner]=\"loading\"\n                                                    [disabled]=\"loading || !loginForm.valid\">\n                                                <core-spinner name=\"login\"></core-spinner>\n                                                <span>Secure Login</span>\n                                            </button>\n                                        </div>\n                                    </div>\n                                </form>\n\n                                <alert *ngIf=\"errorMessage\" class=\"alert alert-danger\" [innerHTML]=\"errorMessage\"></alert>\n\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"position-absolute pos-bottom pos-left pos-right p-3 text-center text-white\"\n                         [innerHTML]=\"copyright\"></div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"
                }] }
    ];
    /** @nocollapse */
    LoginComponent.ctorParameters = function () { return [
        { type: AuthService },
        { type: SpinnerService }
    ]; };
    return LoginComponent;
}());
export { LoginComponent };
if (false) {
    /** @type {?} */
    LoginComponent.prototype.loading;
    /** @type {?} */
    LoginComponent.prototype.errorMessage;
    /** @type {?} */
    LoginComponent.prototype.loginForm;
    /** @type {?} */
    LoginComponent.prototype.appName;
    /** @type {?} */
    LoginComponent.prototype.copyright;
    /**
     * @type {?}
     * @protected
     */
    LoginComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    LoginComponent.prototype.spinner;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9hdXRoL2xvZ2luL2xvZ2luLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFFaEQsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQy9DLE9BQU8sRUFBQyxXQUFXLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBQ2xFLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM1QyxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0scUNBQXFDLENBQUM7QUFFbkUsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBQzFDLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFFaEM7SUFpQkksd0JBQ2MsV0FBd0IsRUFDeEIsT0FBdUI7UUFEdkIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFYckMsY0FBUyxHQUFHLElBQUksU0FBUyxDQUFDO1lBQ2xCLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN0RSxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7U0FDckQsQ0FDSixDQUFDO1FBRUYsWUFBTyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7UUFDN0IsY0FBUyxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUM7SUFNakMsQ0FBQzs7OztJQUVELDhCQUFLOzs7SUFBTDtRQUFBLGlCQWdCQztRQWZHLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FDMUcsVUFBVTs7OztRQUFDLFVBQUMsS0FBd0I7WUFDaEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDM0IsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksR0FBRyw4SUFDc0IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsaUJBQWMsQ0FBQztZQUNsRyxPQUFPLFVBQVUsQ0FBQyxpQkFBZSxLQUFLLENBQUMsTUFBTSxvQkFBZSxLQUFLLENBQUMsT0FBUyxDQUFDLENBQUM7UUFDakYsQ0FBQyxFQUFDLENBQ0wsQ0FBQyxTQUFTOzs7UUFBQztZQUNSLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzNCLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELGlDQUFROzs7SUFBUjtJQUNBLENBQUM7O2dCQTFDSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLHM0SUFBcUM7aUJBQ3hDOzs7O2dCQVRPLFdBQVc7Z0JBQ1gsY0FBYzs7SUFpRHRCLHFCQUFDO0NBQUEsQUE1Q0QsSUE0Q0M7U0F4Q1ksY0FBYzs7O0lBQ3ZCLGlDQUFpQjs7SUFDakIsc0NBQStCOztJQUUvQixtQ0FJRTs7SUFFRixpQ0FBNkI7O0lBQzdCLG1DQUFpQzs7Ozs7SUFHN0IscUNBQWtDOzs7OztJQUNsQyxpQ0FBaUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7QVBQX0NPTkZJR30gZnJvbSAnLi4vLi4vLi4vYXBwLmNvbmZpZyc7XHJcbmltcG9ydCB7Rm9ybUNvbnRyb2wsIEZvcm1Hcm91cCwgVmFsaWRhdG9yc30gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi9hdXRoLnNlcnZpY2UnO1xyXG5pbXBvcnQge1NwaW5uZXJTZXJ2aWNlfSBmcm9tICcuLi8uLi91dGlscy9zcGlubmVyL3NwaW5uZXIuc2VydmljZSc7XHJcbmltcG9ydCB7SHR0cEVycm9yUmVzcG9uc2V9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHtjYXRjaEVycm9yfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7dGhyb3dFcnJvcn0gZnJvbSAncnhqcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY29yZS1sb2dpbicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vbG9naW4uY29tcG9uZW50Lmh0bWwnLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTG9naW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgbG9hZGluZzogYm9vbGVhbjtcclxuICAgIGVycm9yTWVzc2FnZTogc3RyaW5nIHwgYm9vbGVhbjtcclxuXHJcbiAgICBsb2dpbkZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgdXNlcm5hbWU6IG5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMuZW1haWxdKSxcclxuICAgICAgICAgICAgcGFzc3dvcmQ6IG5ldyBGb3JtQ29udHJvbCgnJywgVmFsaWRhdG9ycy5yZXF1aXJlZClcclxuICAgICAgICB9XHJcbiAgICApO1xyXG5cclxuICAgIGFwcE5hbWUgPSBBUFBfQ09ORklHLmFwcE5hbWU7XHJcbiAgICBjb3B5cmlnaHQgPSBBUFBfQ09ORklHLmNvcHlyaWdodDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcm90ZWN0ZWQgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxyXG4gICAgICAgIHByb3RlY3RlZCBzcGlubmVyOiBTcGlubmVyU2VydmljZVxyXG4gICAgKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbG9naW4oKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVycm9yTWVzc2FnZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuc3Bpbm5lci5zaG93KCdsb2dpbicpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmF1dGhTZXJ2aWNlLmxvZ2luKHRoaXMubG9naW5Gb3JtLmdldCgndXNlcm5hbWUnKS52YWx1ZSwgdGhpcy5sb2dpbkZvcm0uZ2V0KCdwYXNzd29yZCcpLnZhbHVlKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcjogSHR0cEVycm9yUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3Bpbm5lci5oaWRlKCdsb2dpbicpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVycm9yTWVzc2FnZSA9IGA8cD5QbGVhc2UgY2hlY2sgeW91ciBlbWFpbCBhZGRyZXNzIGFuZCBwYXNzd29yZCBhcmUgY29ycmVjdCwgYW5kIHRyeSBhZ2Fpbi48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPkVycm9yIHJlZmVyZW5jZTogJHtlcnJvci5oZWFkZXJzLmdldCgneC1zZXJ2aWNlLXJlcXVlc3QtaWQnKX08L3NtYWxsPjwvcD5gO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoYEVycm9yIENvZGU6ICR7ZXJyb3Iuc3RhdHVzfVxcXFxuTWVzc2FnZTogJHtlcnJvci5tZXNzYWdlfWApO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICkuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zcGlubmVyLmhpZGUoJ2xvZ2luJyk7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=