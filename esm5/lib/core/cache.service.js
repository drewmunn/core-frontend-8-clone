/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/cache.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common/http";
var CacheService = /** @class */ (function () {
    function CacheService(router, api) {
        this.router = router;
        this.api = api;
        this.refresh();
    }
    /**
     * @return {?}
     */
    CacheService.prototype.refresh = /**
     * @return {?}
     */
    function () {
        this.cacheProbeTimestamp = {};
        this.objectLoadObservables$ = {};
        this.cacheProbePromises$ = {};
    };
    /**
     * @param {?} label
     * @return {?}
     */
    CacheService.prototype.resetObjectLoad = /**
     * @param {?} label
     * @return {?}
     */
    function (label) {
        delete this.objectLoadObservables$[label];
    };
    /**
     * @param {?} inputs
     * @return {?}
     */
    CacheService.prototype.createCacheKey = /**
     * @param {?} inputs
     * @return {?}
     */
    function (inputs) {
        return JSON.stringify(inputs);
    };
    /**
     * @param {?} item
     * @return {?}
     */
    CacheService.prototype.getFromStorage = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        try {
            if (localStorage.getItem(item)) {
                return localStorage.getItem(item);
            }
        }
        catch (e) {
        }
        try {
            if (sessionStorage.getItem(item)) {
                return sessionStorage.getItem(item);
            }
        }
        catch (e) {
        }
        return false;
    };
    /**
     * @param {?} item
     * @return {?}
     */
    CacheService.prototype.removeFromStorage = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        try {
            localStorage.removeItem(item);
        }
        catch (e) {
        }
        try {
            sessionStorage.removeItem(item);
        }
        catch (e) {
        }
    };
    /**
     * @param {?} route
     * @param {?} identifier
     * @param {?=} headers
     * @param {?=} cacheKey
     * @param {?=} query
     * @return {?}
     */
    CacheService.prototype.getOneThroughCache = /**
     * @param {?} route
     * @param {?} identifier
     * @param {?=} headers
     * @param {?=} cacheKey
     * @param {?=} query
     * @return {?}
     */
    function (route, identifier, headers, cacheKey, query) {
        var _this = this;
        if (!cacheKey) {
            cacheKey = this.createCacheKey({
                route: route,
                identifier: identifier,
                headers: headers,
                query: query,
            });
        }
        if (!this.objectLoadObservables$) {
            this.objectLoadObservables$ = {};
        }
        if (this.objectLoadObservables$[cacheKey]) {
            return this.objectLoadObservables$[cacheKey];
        }
        this.objectLoadObservables$[cacheKey] = new ReplaySubject();
        this.api.get(route + (identifier ? '/' + identifier : ''), {
            params: query || {},
            headers: headers || {}
        }).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            try {
                _this.objectLoadObservables$[cacheKey].next(response);
            }
            catch (err) {
            }
        }));
        return this.objectLoadObservables$[cacheKey];
    };
    /**
     * @param {?} route
     * @param {?=} query
     * @param {?=} headers
     * @param {?=} cacheKey
     * @return {?}
     */
    CacheService.prototype.getListThroughCache = /**
     * @param {?} route
     * @param {?=} query
     * @param {?=} headers
     * @param {?=} cacheKey
     * @return {?}
     */
    function (route, query, headers, cacheKey) {
        var _this = this;
        if (!cacheKey) {
            cacheKey = this.createCacheKey({
                route: route,
                query: query,
                headers: headers
            });
        }
        if (!this.objectLoadObservables$) {
            this.objectLoadObservables$ = {};
        }
        if (this.objectLoadObservables$[cacheKey]) {
            return this.objectLoadObservables$[cacheKey];
        }
        this.objectLoadObservables$[cacheKey] = new ReplaySubject();
        this.api.get(route, {
            params: query || {},
            headers: headers || {}
        })
            .subscribe((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            try {
                _this.objectLoadObservables$[cacheKey].next(response);
            }
            catch (err) {
            }
        }));
        return this.objectLoadObservables$[cacheKey];
    };
    CacheService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    CacheService.ctorParameters = function () { return [
        { type: Router },
        { type: HttpClient }
    ]; };
    /** @nocollapse */ CacheService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CacheService_Factory() { return new CacheService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.HttpClient)); }, token: CacheService, providedIn: "root" });
    return CacheService;
}());
export { CacheService };
if (false) {
    /** @type {?} */
    CacheService.prototype.cacheProbeTimestamp;
    /** @type {?} */
    CacheService.prototype.cacheProbePromises$;
    /** @type {?} */
    CacheService.prototype.objectLoadObservables$;
    /**
     * @type {?}
     * @protected
     */
    CacheService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    CacheService.prototype.api;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FjaGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvY2FjaGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFDbkMsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLHNCQUFzQixDQUFDOzs7O0FBRWhEO0lBU0ksc0JBQ2MsTUFBZSxFQUNmLEdBQWdCO1FBRGhCLFdBQU0sR0FBTixNQUFNLENBQVM7UUFDZixRQUFHLEdBQUgsR0FBRyxDQUFhO1FBRTFCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDOzs7O0lBRUQsOEJBQU87OztJQUFQO1FBQ0ksSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFFRCxzQ0FBZTs7OztJQUFmLFVBQWdCLEtBQUs7UUFDakIsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUMsQ0FBQzs7Ozs7SUFFRCxxQ0FBYzs7OztJQUFkLFVBQWUsTUFBTTtRQUNqQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFFRCxxQ0FBYzs7OztJQUFkLFVBQWUsSUFBSTtRQUNmLElBQUk7WUFDQSxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzVCLE9BQU8sWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNyQztTQUNKO1FBQUMsT0FBTyxDQUFDLEVBQUU7U0FDWDtRQUNELElBQUk7WUFDQSxJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzlCLE9BQU8sY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN2QztTQUNKO1FBQUMsT0FBTyxDQUFDLEVBQUU7U0FDWDtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7O0lBRUQsd0NBQWlCOzs7O0lBQWpCLFVBQWtCLElBQUk7UUFDbEIsSUFBSTtZQUNBLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDakM7UUFBQyxPQUFPLENBQUMsRUFBRTtTQUNYO1FBQ0QsSUFBSTtZQUNBLGNBQWMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbkM7UUFBQyxPQUFPLENBQUMsRUFBRTtTQUNYO0lBQ0wsQ0FBQzs7Ozs7Ozs7O0lBRUQseUNBQWtCOzs7Ozs7OztJQUFsQixVQUFtQixLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQVEsRUFBRSxRQUFTLEVBQUUsS0FBTTtRQUFqRSxpQkEyQkM7UUExQkcsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNYLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO2dCQUMzQixLQUFLLE9BQUE7Z0JBQ0wsVUFBVSxZQUFBO2dCQUNWLE9BQU8sU0FBQTtnQkFDUCxLQUFLLE9BQUE7YUFDUixDQUFDLENBQUM7U0FDTjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQztTQUNwQztRQUNELElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3ZDLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ2hEO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksYUFBYSxFQUFZLENBQUM7UUFDdEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFDckQ7WUFDSSxNQUFNLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDbkIsT0FBTyxFQUFFLE9BQU8sSUFBSSxFQUFFO1NBQ3pCLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxRQUFRO1lBQ3JCLElBQUk7Z0JBQ0EsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN4RDtZQUFDLE9BQU8sR0FBRyxFQUFFO2FBQ2I7UUFDTCxDQUFDLEVBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7Ozs7O0lBRUQsMENBQW1COzs7Ozs7O0lBQW5CLFVBQW9CLEtBQUssRUFBRSxLQUFNLEVBQUUsT0FBUSxFQUFFLFFBQVM7UUFBdEQsaUJBNEJDO1FBM0JHLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDWCxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztnQkFDM0IsS0FBSyxPQUFBO2dCQUNMLEtBQUssT0FBQTtnQkFDTCxPQUFPLFNBQUE7YUFDVixDQUFDLENBQUM7U0FDTjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQztTQUNwQztRQUNELElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3ZDLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ2hEO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksYUFBYSxFQUFZLENBQUM7UUFDdEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUNkO1lBQ0ksTUFBTSxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ25CLE9BQU8sRUFBRSxPQUFPLElBQUksRUFBRTtTQUN6QixDQUFDO2FBQ0QsU0FBUzs7OztRQUFDLFVBQUEsUUFBUTtZQUNYLElBQUk7Z0JBQ0EsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN4RDtZQUFDLE9BQU8sR0FBRyxFQUFFO2FBQ2I7UUFDTCxDQUFDLEVBQ0osQ0FBQztRQUNOLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2pELENBQUM7O2dCQWxISixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQU5PLE1BQU07Z0JBRU4sVUFBVTs7O3VCQUhsQjtDQXlIQyxBQXBIRCxJQW9IQztTQWhIWSxZQUFZOzs7SUFDckIsMkNBQXlCOztJQUN6QiwyQ0FBeUI7O0lBQ3pCLDhDQUE0Qjs7Ozs7SUFHeEIsOEJBQXlCOzs7OztJQUN6QiwyQkFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQge1JlcGxheVN1YmplY3R9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtIdHRwQ2xpZW50fSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5cbmV4cG9ydCBjbGFzcyBDYWNoZVNlcnZpY2Uge1xuICAgIGNhY2hlUHJvYmVUaW1lc3RhbXA6IGFueTtcbiAgICBjYWNoZVByb2JlUHJvbWlzZXMkOiBhbnk7XG4gICAgb2JqZWN0TG9hZE9ic2VydmFibGVzJDogYW55O1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCByb3V0ZXI/OiBSb3V0ZXIsXG4gICAgICAgIHByb3RlY3RlZCBhcGk/OiBIdHRwQ2xpZW50LFxuICAgICkge1xuICAgICAgICB0aGlzLnJlZnJlc2goKTtcbiAgICB9XG5cbiAgICByZWZyZXNoKCk6IHZvaWQge1xuICAgICAgICB0aGlzLmNhY2hlUHJvYmVUaW1lc3RhbXAgPSB7fTtcbiAgICAgICAgdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkID0ge307XG4gICAgICAgIHRoaXMuY2FjaGVQcm9iZVByb21pc2VzJCA9IHt9O1xuICAgIH1cblxuICAgIHJlc2V0T2JqZWN0TG9hZChsYWJlbCkge1xuICAgICAgICBkZWxldGUgdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2xhYmVsXTtcbiAgICB9XG5cbiAgICBjcmVhdGVDYWNoZUtleShpbnB1dHMpIHtcbiAgICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KGlucHV0cyk7XG4gICAgfVxuXG4gICAgZ2V0RnJvbVN0b3JhZ2UoaXRlbSkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgaWYgKGxvY2FsU3RvcmFnZS5nZXRJdGVtKGl0ZW0pKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKGl0ZW0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIH1cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGl0ZW0pKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oaXRlbSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgcmVtb3ZlRnJvbVN0b3JhZ2UoaXRlbSkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oaXRlbSk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgfVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2UucmVtb3ZlSXRlbShpdGVtKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0T25lVGhyb3VnaENhY2hlKHJvdXRlLCBpZGVudGlmaWVyLCBoZWFkZXJzPywgY2FjaGVLZXk/LCBxdWVyeT8pIHtcbiAgICAgICAgaWYgKCFjYWNoZUtleSkge1xuICAgICAgICAgICAgY2FjaGVLZXkgPSB0aGlzLmNyZWF0ZUNhY2hlS2V5KHtcbiAgICAgICAgICAgICAgICByb3V0ZSxcbiAgICAgICAgICAgICAgICBpZGVudGlmaWVyLFxuICAgICAgICAgICAgICAgIGhlYWRlcnMsXG4gICAgICAgICAgICAgICAgcXVlcnksXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoIXRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJCkge1xuICAgICAgICAgICAgdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkID0ge307XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFtjYWNoZUtleV0pIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbY2FjaGVLZXldO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFtjYWNoZUtleV0gPSBuZXcgUmVwbGF5U3ViamVjdDxSZXNwb25zZT4oKTtcbiAgICAgICAgdGhpcy5hcGkuZ2V0KHJvdXRlICsgKGlkZW50aWZpZXIgPyAnLycgKyBpZGVudGlmaWVyIDogJycpLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHBhcmFtczogcXVlcnkgfHwge30sXG4gICAgICAgICAgICAgICAgaGVhZGVyczogaGVhZGVycyB8fCB7fVxuICAgICAgICAgICAgfSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XS5uZXh0KHJlc3BvbnNlKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFtjYWNoZUtleV07XG4gICAgfVxuXG4gICAgZ2V0TGlzdFRocm91Z2hDYWNoZShyb3V0ZSwgcXVlcnk/LCBoZWFkZXJzPywgY2FjaGVLZXk/KSB7XG4gICAgICAgIGlmICghY2FjaGVLZXkpIHtcbiAgICAgICAgICAgIGNhY2hlS2V5ID0gdGhpcy5jcmVhdGVDYWNoZUtleSh7XG4gICAgICAgICAgICAgICAgcm91dGUsXG4gICAgICAgICAgICAgICAgcXVlcnksXG4gICAgICAgICAgICAgICAgaGVhZGVyc1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCF0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyQpIHtcbiAgICAgICAgICAgIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJCA9IHt9O1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbY2FjaGVLZXldKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbY2FjaGVLZXldID0gbmV3IFJlcGxheVN1YmplY3Q8UmVzcG9uc2U+KCk7XG4gICAgICAgIHRoaXMuYXBpLmdldChyb3V0ZSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHF1ZXJ5IHx8IHt9LFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IGhlYWRlcnMgfHwge31cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFtjYWNoZUtleV0ubmV4dChyZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICAgICAgcmV0dXJuIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFtjYWNoZUtleV07XG4gICAgfVxuXG59XG4iXX0=