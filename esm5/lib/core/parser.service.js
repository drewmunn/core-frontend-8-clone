/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/parser.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var ParserService = /** @class */ (function () {
    function ParserService() {
    }
    /**
     * @param {?} text
     * @param {?} find
     * @param {?} replace
     * @return {?}
     */
    ParserService.prototype.replaceAll = /**
     * @param {?} text
     * @param {?} find
     * @param {?} replace
     * @return {?}
     */
    function (text, find, replace) {
        /**
         * @param {?} textString
         * @return {?}
         */
        function escape(textString) {
            return textString.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
        }
        return text.replace(new RegExp(escape(find), 'g'), replace);
    };
    /**
     * @param {?} obj
     * @param {?=} prefix
     * @return {?}
     */
    ParserService.prototype.serializeRgQuery = /**
     * @param {?} obj
     * @param {?=} prefix
     * @return {?}
     */
    function (obj, prefix) {
        /** @type {?} */
        var str = [];
        /** @type {?} */
        var keys = [];
        for (var k in obj) {
            if (obj.hasOwnProperty(k)) {
                keys.push(k);
            }
        }
        keys.sort();
        for (var i = 0; i < keys.length; i++) {
            /** @type {?} */
            var k = prefix ? prefix + '[' + keys[i] + ']' : keys[i];
            /** @type {?} */
            var v = obj[keys[i]];
            str.push(typeof v == 'object' ?
                encodeURIComponent(k) + '=' + encodeURIComponent(JSON.stringify(v)).replace(/%3A/g, ':') :
                encodeURIComponent(k) + '=' + encodeURIComponent(v));
        }
        /** @type {?} */
        var finalStr = str.join('&');
        finalStr = this.replaceAll(finalStr, '%40', '@');
        finalStr = this.replaceAll(finalStr, '%3A', ':');
        finalStr = this.replaceAll(finalStr, '%2C', ',');
        finalStr = this.replaceAll(finalStr, '%20', '+');
        return finalStr;
    };
    /**
     * @param {?} cname
     * @return {?}
     */
    ParserService.prototype.getCookie = /**
     * @param {?} cname
     * @return {?}
     */
    function (cname) {
        /** @type {?} */
        var name = cname + '=';
        /** @type {?} */
        var decodedCookie = decodeURIComponent(document.cookie);
        /** @type {?} */
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            /** @type {?} */
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return false;
    };
    /**
     * @param {?} name
     * @param {?} path
     * @param {?=} domain
     * @return {?}
     */
    ParserService.prototype.deleteCookie = /**
     * @param {?} name
     * @param {?} path
     * @param {?=} domain
     * @return {?}
     */
    function (name, path, domain) {
        if (this.getCookie(name)) {
            try {
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') +
                    ((domain) ? ';domain=' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') +
                    ((domain) ? ';domain=.' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') + ';domain=' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') + ';domain=.' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((domain) ? ';domain=' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((domain) ? ';domain=.' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ';domain=' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ';domain=.' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
            }
            catch (err) {
            }
        }
    };
    ParserService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */ ParserService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ParserService_Factory() { return new ParserService(); }, token: ParserService, providedIn: "root" });
    return ParserService;
}());
export { ParserService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFyc2VyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3BhcnNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQzs7QUFFekM7SUFBQTtLQTZGQzs7Ozs7OztJQXZGRyxrQ0FBVTs7Ozs7O0lBQVYsVUFBVyxJQUFJLEVBQUUsSUFBSSxFQUFFLE9BQU87Ozs7O1FBQzFCLFNBQVMsTUFBTSxDQUFDLFVBQVU7WUFDdEIsT0FBTyxVQUFVLENBQUMsT0FBTyxDQUFDLDZCQUE2QixFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3JFLENBQUM7UUFFRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7OztJQUVELHdDQUFnQjs7Ozs7SUFBaEIsVUFBaUIsR0FBRyxFQUFFLE1BQU87O1lBQ25CLEdBQUcsR0FBRyxFQUFFOztZQUNSLElBQUksR0FBRyxFQUFFO1FBQ2YsS0FBSyxJQUFNLENBQUMsSUFBSSxHQUFHLEVBQUU7WUFDakIsSUFBSSxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2hCO1NBQ0o7UUFDRCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDWixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7Z0JBQzlCLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs7Z0JBQ25ELENBQUMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksUUFBUSxDQUFDLENBQUM7Z0JBQzNCLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUMxRixrQkFBa0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUM1RDs7WUFDRyxRQUFRLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDNUIsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNqRCxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDakQsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztRQUVqRCxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDOzs7OztJQUVELGlDQUFTOzs7O0lBQVQsVUFBVSxLQUFLOztZQUNMLElBQUksR0FBRyxLQUFLLEdBQUcsR0FBRzs7WUFDbEIsYUFBYSxHQUFHLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7O1lBQ25ELEVBQUUsR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUNuQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7Z0JBQzVCLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2IsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtnQkFDeEIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdEI7WUFDRCxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUN2QixPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDN0M7U0FDSjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7Ozs7SUFFRCxvQ0FBWTs7Ozs7O0lBQVosVUFBYSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU87UUFDNUIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3RCLElBQUk7Z0JBQ0EsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsR0FBRztvQkFDeEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQy9CLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUNyQyx3Q0FBd0MsQ0FBQztnQkFDN0MsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsR0FBRztvQkFDeEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQy9CLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUN0Qyx3Q0FBd0MsQ0FBQztnQkFFN0MsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsR0FBRztvQkFDeEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxVQUFVLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUN2RSx3Q0FBd0MsQ0FBQztnQkFDN0MsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsR0FBRztvQkFDeEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxXQUFXLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUN4RSx3Q0FBd0MsQ0FBQztnQkFFN0MsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsR0FBRztvQkFDeEIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ3JDLHdDQUF3QyxDQUFDO2dCQUM3QyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksR0FBRyxHQUFHO29CQUN4QixDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDdEMsd0NBQXdDLENBQUM7Z0JBRTdDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLEdBQUc7b0JBQ3hCLFVBQVUsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVE7b0JBQ3JDLHdDQUF3QyxDQUFDO2dCQUM3QyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksR0FBRyxHQUFHO29CQUN4QixXQUFXLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUN0Qyx3Q0FBd0MsQ0FBQzthQUVoRDtZQUFDLE9BQU8sR0FBRyxFQUFFO2FBQ2I7U0FDSjtJQUNMLENBQUM7O2dCQTNGSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7d0JBSkQ7Q0ErRkMsQUE3RkQsSUE2RkM7U0F6RlksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5cbmV4cG9ydCBjbGFzcyBQYXJzZXJTZXJ2aWNlIHtcblxuICAgIHJlcGxhY2VBbGwodGV4dCwgZmluZCwgcmVwbGFjZSkge1xuICAgICAgICBmdW5jdGlvbiBlc2NhcGUodGV4dFN0cmluZykge1xuICAgICAgICAgICAgcmV0dXJuIHRleHRTdHJpbmcucmVwbGFjZSgvKFsuKis/Xj0hOiR7fSgpfFxcW1xcXVxcL1xcXFxdKS9nLCAnXFxcXCQxJyk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdGV4dC5yZXBsYWNlKG5ldyBSZWdFeHAoZXNjYXBlKGZpbmQpLCAnZycpLCByZXBsYWNlKTtcbiAgICB9XG5cbiAgICBzZXJpYWxpemVSZ1F1ZXJ5KG9iaiwgcHJlZml4Pykge1xuICAgICAgICBjb25zdCBzdHIgPSBbXTtcbiAgICAgICAgY29uc3Qga2V5cyA9IFtdO1xuICAgICAgICBmb3IgKGNvbnN0IGsgaW4gb2JqKSB7XG4gICAgICAgICAgICBpZiAob2JqLmhhc093blByb3BlcnR5KGspKSB7XG4gICAgICAgICAgICAgICAga2V5cy5wdXNoKGspO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGtleXMuc29ydCgpO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGtleXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGxldCBrID0gcHJlZml4ID8gcHJlZml4ICsgJ1snICsga2V5c1tpXSArICddJyA6IGtleXNbaV0sXG4gICAgICAgICAgICAgICAgdiA9IG9ialtrZXlzW2ldXTtcbiAgICAgICAgICAgIHN0ci5wdXNoKHR5cGVvZiB2ID09ICdvYmplY3QnID9cbiAgICAgICAgICAgICAgICBlbmNvZGVVUklDb21wb25lbnQoaykgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkodikpLnJlcGxhY2UoLyUzQS9nLCAnOicpIDpcbiAgICAgICAgICAgICAgICBlbmNvZGVVUklDb21wb25lbnQoaykgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQodikpO1xuICAgICAgICB9XG4gICAgICAgIGxldCBmaW5hbFN0ciA9IHN0ci5qb2luKCcmJyk7XG4gICAgICAgIGZpbmFsU3RyID0gdGhpcy5yZXBsYWNlQWxsKGZpbmFsU3RyLCAnJTQwJywgJ0AnKTtcbiAgICAgICAgZmluYWxTdHIgPSB0aGlzLnJlcGxhY2VBbGwoZmluYWxTdHIsICclM0EnLCAnOicpO1xuICAgICAgICBmaW5hbFN0ciA9IHRoaXMucmVwbGFjZUFsbChmaW5hbFN0ciwgJyUyQycsICcsJyk7XG4gICAgICAgIGZpbmFsU3RyID0gdGhpcy5yZXBsYWNlQWxsKGZpbmFsU3RyLCAnJTIwJywgJysnKTtcblxuICAgICAgICByZXR1cm4gZmluYWxTdHI7XG4gICAgfVxuXG4gICAgZ2V0Q29va2llKGNuYW1lKSB7XG4gICAgICAgIGNvbnN0IG5hbWUgPSBjbmFtZSArICc9JztcbiAgICAgICAgY29uc3QgZGVjb2RlZENvb2tpZSA9IGRlY29kZVVSSUNvbXBvbmVudChkb2N1bWVudC5jb29raWUpO1xuICAgICAgICBjb25zdCBjYSA9IGRlY29kZWRDb29raWUuc3BsaXQoJzsnKTtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjYS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IGMgPSBjYVtpXTtcbiAgICAgICAgICAgIHdoaWxlIChjLmNoYXJBdCgwKSA9PT0gJyAnKSB7XG4gICAgICAgICAgICAgICAgYyA9IGMuc3Vic3RyaW5nKDEpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGMuaW5kZXhPZihuYW1lKSA9PT0gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBjLnN1YnN0cmluZyhuYW1lLmxlbmd0aCwgYy5sZW5ndGgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBkZWxldGVDb29raWUobmFtZSwgcGF0aCwgZG9tYWluPykge1xuICAgICAgICBpZiAodGhpcy5nZXRDb29raWUobmFtZSkpIHtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuY29va2llID0gbmFtZSArICc9JyArXG4gICAgICAgICAgICAgICAgICAgICgocGF0aCkgPyAnO3BhdGg9JyArIHBhdGggOiAnJykgK1xuICAgICAgICAgICAgICAgICAgICAoKGRvbWFpbikgPyAnO2RvbWFpbj0nICsgZG9tYWluIDogJycpICtcbiAgICAgICAgICAgICAgICAgICAgJztleHBpcmVzPVRodSwgMDEgSmFuIDE5NzAgMDA6MDA6MDEgR01UJztcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSBuYW1lICsgJz0nICtcbiAgICAgICAgICAgICAgICAgICAgKChwYXRoKSA/ICc7cGF0aD0nICsgcGF0aCA6ICcnKSArXG4gICAgICAgICAgICAgICAgICAgICgoZG9tYWluKSA/ICc7ZG9tYWluPS4nICsgZG9tYWluIDogJycpICtcbiAgICAgICAgICAgICAgICAgICAgJztleHBpcmVzPVRodSwgMDEgSmFuIDE5NzAgMDA6MDA6MDEgR01UJztcblxuICAgICAgICAgICAgICAgIGRvY3VtZW50LmNvb2tpZSA9IG5hbWUgKyAnPScgK1xuICAgICAgICAgICAgICAgICAgICAoKHBhdGgpID8gJztwYXRoPScgKyBwYXRoIDogJycpICsgJztkb21haW49JyArIHdpbmRvdy5sb2NhdGlvbi5ob3N0bmFtZSArXG4gICAgICAgICAgICAgICAgICAgICc7ZXhwaXJlcz1UaHUsIDAxIEphbiAxOTcwIDAwOjAwOjAxIEdNVCc7XG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuY29va2llID0gbmFtZSArICc9JyArXG4gICAgICAgICAgICAgICAgICAgICgocGF0aCkgPyAnO3BhdGg9JyArIHBhdGggOiAnJykgKyAnO2RvbWFpbj0uJyArIHdpbmRvdy5sb2NhdGlvbi5ob3N0bmFtZSArXG4gICAgICAgICAgICAgICAgICAgICc7ZXhwaXJlcz1UaHUsIDAxIEphbiAxOTcwIDAwOjAwOjAxIEdNVCc7XG5cbiAgICAgICAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSBuYW1lICsgJz0nICtcbiAgICAgICAgICAgICAgICAgICAgKChkb21haW4pID8gJztkb21haW49JyArIGRvbWFpbiA6ICcnKSArXG4gICAgICAgICAgICAgICAgICAgICc7ZXhwaXJlcz1UaHUsIDAxIEphbiAxOTcwIDAwOjAwOjAxIEdNVCc7XG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuY29va2llID0gbmFtZSArICc9JyArXG4gICAgICAgICAgICAgICAgICAgICgoZG9tYWluKSA/ICc7ZG9tYWluPS4nICsgZG9tYWluIDogJycpICtcbiAgICAgICAgICAgICAgICAgICAgJztleHBpcmVzPVRodSwgMDEgSmFuIDE5NzAgMDA6MDA6MDEgR01UJztcblxuICAgICAgICAgICAgICAgIGRvY3VtZW50LmNvb2tpZSA9IG5hbWUgKyAnPScgK1xuICAgICAgICAgICAgICAgICAgICAnO2RvbWFpbj0nICsgd2luZG93LmxvY2F0aW9uLmhvc3RuYW1lICtcbiAgICAgICAgICAgICAgICAgICAgJztleHBpcmVzPVRodSwgMDEgSmFuIDE5NzAgMDA6MDA6MDEgR01UJztcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSBuYW1lICsgJz0nICtcbiAgICAgICAgICAgICAgICAgICAgJztkb21haW49LicgKyB3aW5kb3cubG9jYXRpb24uaG9zdG5hbWUgK1xuICAgICAgICAgICAgICAgICAgICAnO2V4cGlyZXM9VGh1LCAwMSBKYW4gMTk3MCAwMDowMDowMSBHTVQnO1xuXG4gICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxufVxuIl19