/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/systemMessage.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var SystemMessageService = /** @class */ (function () {
    function SystemMessageService(systemMessageService) {
        this.systemMessageService = systemMessageService;
    }
    /**
     * @return {?}
     */
    SystemMessageService.prototype.clearInterceptMessage = /**
     * @return {?}
     */
    function () {
        this.interceptMessage = null;
    };
    /**
     * @param {?} message
     * @return {?}
     */
    SystemMessageService.prototype.setInterceptMessage = /**
     * @param {?} message
     * @return {?}
     */
    function (message) {
        this.interceptMessage = message;
    };
    /**
     * @return {?}
     */
    SystemMessageService.prototype.getInterceptMessage = /**
     * @return {?}
     */
    function () {
        return this.interceptMessage;
    };
    SystemMessageService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SystemMessageService.ctorParameters = function () { return [
        { type: SystemMessageService }
    ]; };
    /** @nocollapse */ SystemMessageService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SystemMessageService_Factory() { return new SystemMessageService(i0.ɵɵinject(SystemMessageService)); }, token: SystemMessageService, providedIn: "root" });
    return SystemMessageService;
}());
export { SystemMessageService };
if (false) {
    /** @type {?} */
    SystemMessageService.prototype.interceptMessage;
    /**
     * @type {?}
     * @private
     */
    SystemMessageService.prototype.systemMessageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtTWVzc2FnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9zeXN0ZW1NZXNzYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDOztBQUV6QztJQU9JLDhCQUFvQixvQkFBMEM7UUFBMUMseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtJQUM5RCxDQUFDOzs7O0lBRUQsb0RBQXFCOzs7SUFBckI7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRUQsa0RBQW1COzs7O0lBQW5CLFVBQW9CLE9BQWU7UUFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE9BQU8sQ0FBQztJQUNwQyxDQUFDOzs7O0lBRUQsa0RBQW1COzs7SUFBbkI7UUFDSSxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUNqQyxDQUFDOztnQkFwQkosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFLNkMsb0JBQW9COzs7K0JBVGxFO0NBd0JDLEFBdEJELElBc0JDO1NBbEJZLG9CQUFvQjs7O0lBQzdCLGdEQUF5Qjs7Ozs7SUFFYixvREFBa0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5leHBvcnQgY2xhc3MgU3lzdGVtTWVzc2FnZVNlcnZpY2Uge1xuICAgIGludGVyY2VwdE1lc3NhZ2U6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgc3lzdGVtTWVzc2FnZVNlcnZpY2U6IFN5c3RlbU1lc3NhZ2VTZXJ2aWNlKSB7XG4gICAgfVxuXG4gICAgY2xlYXJJbnRlcmNlcHRNZXNzYWdlKCkge1xuICAgICAgICB0aGlzLmludGVyY2VwdE1lc3NhZ2UgPSBudWxsO1xuICAgIH1cblxuICAgIHNldEludGVyY2VwdE1lc3NhZ2UobWVzc2FnZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMuaW50ZXJjZXB0TWVzc2FnZSA9IG1lc3NhZ2U7XG4gICAgfVxuXG4gICAgZ2V0SW50ZXJjZXB0TWVzc2FnZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW50ZXJjZXB0TWVzc2FnZTtcbiAgICB9XG5cbn1cbiJdfQ==