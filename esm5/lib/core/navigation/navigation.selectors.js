/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/navigation/navigation.selectors.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { createFeatureSelector, createSelector } from '@ngrx/store';
/** @type {?} */
export var selectNavigationState = createFeatureSelector('navigation');
var ɵ0 = /**
 * @param {?} state
 * @return {?}
 */
function (state) { return state.items; };
/** @type {?} */
export var selectNavigationItems = createSelector(selectNavigationState, (ɵ0));
var ɵ1 = /**
 * @param {?} state
 * @return {?}
 */
function (state) { return ({
    active: state.filterActive,
    text: state.filterText
}); };
/** @type {?} */
export var selectFilter = createSelector(selectNavigationState, (ɵ1));
var ɵ2 = /**
 * @param {?} state
 * @return {?}
 */
function (state) { return ({
    active: state.filterActive && !!state.filterText.trim(),
    total: state.total,
    matched: state.matched
}); };
/** @type {?} */
export var selectResult = createSelector(selectNavigationState, (ɵ2));
export { ɵ0, ɵ1, ɵ2 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5zZWxlY3RvcnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL25hdmlnYXRpb24vbmF2aWdhdGlvbi5zZWxlY3RvcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMscUJBQXFCLEVBQUUsY0FBYyxFQUFDLE1BQU0sYUFBYSxDQUFDOztBQUdsRSxNQUFNLEtBQU8scUJBQXFCLEdBQUcscUJBQXFCLENBQWtCLFlBQVksQ0FBQzs7Ozs7QUFFZCxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxLQUFLLEVBQVgsQ0FBVzs7QUFBL0YsTUFBTSxLQUFPLHFCQUFxQixHQUFHLGNBQWMsQ0FBQyxxQkFBcUIsT0FBdUI7Ozs7O0FBQzlCLFVBQUEsS0FBSyxJQUFJLE9BQUEsQ0FBQztJQUN4RSxNQUFNLEVBQUUsS0FBSyxDQUFDLFlBQVk7SUFDMUIsSUFBSSxFQUFFLEtBQUssQ0FBQyxVQUFVO0NBQ3pCLENBQUMsRUFIeUUsQ0FHekU7O0FBSEYsTUFBTSxLQUFPLFlBQVksR0FBRyxjQUFjLENBQUMscUJBQXFCLE9BRzdEOzs7OztBQUcrRCxVQUFBLEtBQUssSUFBSSxPQUFBLENBQUM7SUFDeEUsTUFBTSxFQUFFLEtBQUssQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFO0lBQ3ZELEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSztJQUNsQixPQUFPLEVBQUUsS0FBSyxDQUFDLE9BQU87Q0FDekIsQ0FBQyxFQUp5RSxDQUl6RTs7QUFKRixNQUFNLEtBQU8sWUFBWSxHQUFHLGNBQWMsQ0FBQyxxQkFBcUIsT0FJN0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge2NyZWF0ZUZlYXR1cmVTZWxlY3RvciwgY3JlYXRlU2VsZWN0b3J9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7TmF2aWdhdGlvblN0YXRlfSBmcm9tICcuL25hdmlnYXRpb24ucmVkdWNlcic7XG5cbmV4cG9ydCBjb25zdCBzZWxlY3ROYXZpZ2F0aW9uU3RhdGUgPSBjcmVhdGVGZWF0dXJlU2VsZWN0b3I8TmF2aWdhdGlvblN0YXRlPignbmF2aWdhdGlvbicpO1xuXG5leHBvcnQgY29uc3Qgc2VsZWN0TmF2aWdhdGlvbkl0ZW1zID0gY3JlYXRlU2VsZWN0b3Ioc2VsZWN0TmF2aWdhdGlvblN0YXRlLCBzdGF0ZSA9PiBzdGF0ZS5pdGVtcyk7XG5leHBvcnQgY29uc3Qgc2VsZWN0RmlsdGVyID0gY3JlYXRlU2VsZWN0b3Ioc2VsZWN0TmF2aWdhdGlvblN0YXRlLCBzdGF0ZSA9PiAoe1xuICAgIGFjdGl2ZTogc3RhdGUuZmlsdGVyQWN0aXZlLFxuICAgIHRleHQ6IHN0YXRlLmZpbHRlclRleHRcbn0pKTtcblxuXG5leHBvcnQgY29uc3Qgc2VsZWN0UmVzdWx0ID0gY3JlYXRlU2VsZWN0b3Ioc2VsZWN0TmF2aWdhdGlvblN0YXRlLCBzdGF0ZSA9PiAoe1xuICAgIGFjdGl2ZTogc3RhdGUuZmlsdGVyQWN0aXZlICYmICEhc3RhdGUuZmlsdGVyVGV4dC50cmltKCksXG4gICAgdG90YWw6IHN0YXRlLnRvdGFsLFxuICAgIG1hdGNoZWQ6IHN0YXRlLm1hdGNoZWRcbn0pKTtcbiJdfQ==