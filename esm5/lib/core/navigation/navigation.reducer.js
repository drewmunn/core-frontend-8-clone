/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/navigation/navigation.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { createReducer, on } from '@ngrx/store';
import * as NavigationActions from './navigation.actions';
import { NavigationItems } from '../../core.navigation';
/**
 * @record
 */
export function NavigationItem() { }
if (false) {
    /** @type {?} */
    NavigationItem.prototype.badge;
    /** @type {?} */
    NavigationItem.prototype.item;
    /** @type {?} */
    NavigationItem.prototype.name;
    /** @type {?} */
    NavigationItem.prototype.title;
    /** @type {?|undefined} */
    NavigationItem.prototype.icon;
    /** @type {?|undefined} */
    NavigationItem.prototype.tags;
    /** @type {?|undefined} */
    NavigationItem.prototype.routerLink;
    /** @type {?|undefined} */
    NavigationItem.prototype.url;
    /** @type {?|undefined} */
    NavigationItem.prototype.active;
    /** @type {?|undefined} */
    NavigationItem.prototype.open;
    /** @type {?|undefined} */
    NavigationItem.prototype.items;
    /** @type {?|undefined} */
    NavigationItem.prototype.matched;
    /** @type {?|undefined} */
    NavigationItem.prototype.navTitle;
}
/**
 * @record
 */
export function NavigationState() { }
if (false) {
    /** @type {?} */
    NavigationState.prototype.items;
    /** @type {?} */
    NavigationState.prototype.total;
    /** @type {?} */
    NavigationState.prototype.filterActive;
    /** @type {?} */
    NavigationState.prototype.filterText;
    /** @type {?} */
    NavigationState.prototype.matched;
}
/** @type {?} */
export var initialState = {
    items: decorateItems(NavigationItems),
    total: countTotal(NavigationItems),
    filterActive: false,
    filterText: '',
    matched: 0
};
var ɵ0 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
function (state, action) { return (tslib_1.__assign({}, state, { items: detectActiveItems(state.items, action.url) })); }, ɵ1 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
function (state, action) { return (tslib_1.__assign({}, state, { items: toggleItems(state.items, action.item) })); }, ɵ2 = /**
 * @param {?} state
 * @return {?}
 */
function (state) {
    if (state.filterActive) {
        return tslib_1.__assign({}, state, { filterActive: false, matched: 0, items: state.items.map((/**
             * @param {?} _
             * @return {?}
             */
            function (_) { return (tslib_1.__assign({}, _, { matched: null })); })) });
    }
    else {
        /** @type {?} */
        var items = filterItems(state.items, state.filterText);
        return tslib_1.__assign({}, state, { filterActive: true, items: items, matched: countMatched(items) });
    }
}, ɵ3 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
function (state, action) {
    /** @type {?} */
    var items = filterItems(state.items, action.text);
    return tslib_1.__assign({}, state, { filterText: action.text, items: items, matched: countMatched(items) });
};
/** @type {?} */
var navigationReducer = createReducer(initialState, on(NavigationActions.activeUrl, (ɵ0)), on(NavigationActions.toggleNavSection, (ɵ1)), on(NavigationActions.toggleNavigationFilter, (ɵ2)), on(NavigationActions.navigationFilter, (ɵ3)));
/**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
export function reducer(state, action) {
    return navigationReducer(state, action);
}
/**
 * @param {?} navItems
 * @return {?}
 */
function decorateItems(navItems) {
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    function (navItem) {
        /** @type {?} */
        var item = tslib_1.__assign({}, navItem, { active: false, matched: null });
        if (navItem.items) {
            item.open = false;
            item.items = decorateItems(navItem.items);
        }
        item.navTitle = !navItem.items && !navItem.routerLink && !!navItem.title;
        return item;
    }));
}
/**
 * @param {?} navItems
 * @return {?}
 */
function countTotal(navItems) {
    /** @type {?} */
    var total = navItems.length;
    navItems.filter((/**
     * @param {?} _
     * @return {?}
     */
    function (_) { return !!_.items; })).forEach((/**
     * @param {?} _
     * @return {?}
     */
    function (_) {
        total += countTotal(_.items);
    }));
    return total;
}
/**
 * @param {?} navItems
 * @return {?}
 */
function countMatched(navItems) {
    /** @type {?} */
    var matched = navItems.filter((/**
     * @param {?} _
     * @return {?}
     */
    function (_) { return !!_.matched; })).length;
    navItems.filter((/**
     * @param {?} _
     * @return {?}
     */
    function (_) { return !!_.items; })).forEach((/**
     * @param {?} _
     * @return {?}
     */
    function (_) {
        matched += countMatched(_.items);
    }));
    return matched;
}
/**
 * @param {?} navItems
 * @param {?} activeUrl
 * @return {?}
 */
function detectActiveItems(navItems, activeUrl) {
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    function (navItem) {
        /** @type {?} */
        var isActive = itemIsActive(navItem, activeUrl);
        /** @type {?} */
        var item = tslib_1.__assign({}, navItem, { active: isActive });
        if (navItem.items) {
            item.open = isActive;
            item.items = detectActiveItems(navItem.items, activeUrl);
        }
        return item;
    }));
}
/**
 * @param {?} item
 * @param {?} activeUrl
 * @return {?}
 */
function itemIsActive(item, activeUrl) {
    if (item.routerLink === activeUrl) {
        return true;
    }
    else if (item.items) {
        return item.items.some((/**
         * @param {?} _
         * @return {?}
         */
        function (_) { return itemIsActive(_, activeUrl); }));
    }
    else {
        return false;
    }
}
/**
 * @param {?} navItems
 * @param {?} toggledItem
 * @return {?}
 */
function toggleItems(navItems, toggledItem) {
    /** @type {?} */
    var isToggledItemLevel = navItems.some((/**
     * @param {?} _
     * @return {?}
     */
    function (_) { return _ === toggledItem; }));
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    function (navItem) {
        /** @type {?} */
        var item = tslib_1.__assign({}, navItem);
        if (isToggledItemLevel && item.items && navItem !== toggledItem) {
            item.open = false;
        }
        if (navItem === toggledItem) {
            item.open = !navItem.open;
        }
        if (navItem.items) {
            item.items = toggleItems(navItem.items, toggledItem);
        }
        return item;
    }));
}
/**
 * @param {?} navItems
 * @param {?} text
 * @return {?}
 */
function filterItems(navItems, text) {
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    function (navItem) {
        /** @type {?} */
        var item = tslib_1.__assign({}, navItem);
        if (navItem.items) {
            item.matched = navItemMatch(navItem, text) || navItem.items.some((/**
             * @param {?} _
             * @return {?}
             */
            function (_) { return navItemMatch(_, text); }));
            item.items = filterItems(navItem.items, text);
        }
        else {
            item.matched = navItemMatch(navItem, text);
        }
        return item;
    }));
}
/**
 * @param {?} item
 * @param {?} text
 * @return {?}
 */
function navItemMatch(item, text) {
    return (!text.trim() || (item.tags && !!item.tags.match(new RegExp(".*" + text.trim() + ".*", 'gi'))));
}
export { ɵ0, ɵ1, ɵ2, ɵ3 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5yZWR1Y2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9uYXZpZ2F0aW9uL25hdmlnYXRpb24ucmVkdWNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQVMsYUFBYSxFQUFFLEVBQUUsRUFBQyxNQUFNLGFBQWEsQ0FBQztBQUN0RCxPQUFPLEtBQUssaUJBQWlCLE1BQU0sc0JBQXNCLENBQUM7QUFDMUQsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLHVCQUF1QixDQUFDOzs7O0FBRXRELG9DQWNDOzs7SUFiRywrQkFBVzs7SUFDWCw4QkFBVTs7SUFDViw4QkFBYTs7SUFDYiwrQkFBYzs7SUFDZCw4QkFBYzs7SUFDZCw4QkFBYzs7SUFDZCxvQ0FBb0I7O0lBQ3BCLDZCQUFhOztJQUNiLGdDQUFpQjs7SUFDakIsOEJBQWU7O0lBQ2YsK0JBQXlCOztJQUN6QixpQ0FBa0I7O0lBQ2xCLGtDQUFtQjs7Ozs7QUFHdkIscUNBTUM7OztJQUxHLGdDQUF3Qjs7SUFDeEIsZ0NBQWM7O0lBQ2QsdUNBQXNCOztJQUN0QixxQ0FBbUI7O0lBQ25CLGtDQUFnQjs7O0FBR3BCLE1BQU0sS0FBTyxZQUFZLEdBQW9CO0lBQ3pDLEtBQUssRUFBRSxhQUFhLENBQUMsZUFBZSxDQUFDO0lBQ3JDLEtBQUssRUFBRSxVQUFVLENBQUMsZUFBZSxDQUFDO0lBQ2xDLFlBQVksRUFBRSxLQUFLO0lBQ25CLFVBQVUsRUFBRSxFQUFFO0lBQ2QsT0FBTyxFQUFFLENBQUM7Q0FDYjs7Ozs7O0FBS21DLFVBQUMsS0FBSyxFQUFFLE1BQU0sSUFBSyxPQUFBLHNCQUM1QyxLQUFLLElBQ1IsS0FBSyxFQUFFLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUNuRCxFQUhpRCxDQUdqRDs7Ozs7QUFDcUMsVUFBQyxLQUFLLEVBQUUsTUFBTSxJQUFLLE9BQUEsc0JBQ25ELEtBQUssSUFDUixLQUFLLEVBQUUsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUM5QyxFQUh3RCxDQUd4RDs7OztBQUMyQyxVQUFBLEtBQUs7SUFDOUMsSUFBSSxLQUFLLENBQUMsWUFBWSxFQUFFO1FBQ3BCLDRCQUNPLEtBQUssSUFDUixZQUFZLEVBQUUsS0FBSyxFQUNuQixPQUFPLEVBQUUsQ0FBQyxFQUNWLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLHNCQUFLLENBQUMsSUFBRSxPQUFPLEVBQUUsSUFBSSxJQUFFLEVBQXZCLENBQXVCLEVBQUMsSUFDdEQ7S0FDTDtTQUFNOztZQUNHLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDO1FBQ3hELDRCQUNPLEtBQUssSUFDUixZQUFZLEVBQUUsSUFBSSxFQUNsQixLQUFLLE9BQUEsRUFDTCxPQUFPLEVBQUUsWUFBWSxDQUFDLEtBQUssQ0FBQyxJQUM5QjtLQUNMO0FBRUwsQ0FBQzs7Ozs7QUFDc0MsVUFBQyxLQUFLLEVBQUUsTUFBTTs7UUFDM0MsS0FBSyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDbkQsNEJBQ08sS0FBSyxJQUNSLFVBQVUsRUFBRSxNQUFNLENBQUMsSUFBSSxFQUN2QixLQUFLLE9BQUEsRUFDTCxPQUFPLEVBQUUsWUFBWSxDQUFDLEtBQUssQ0FBQyxJQUM5QjtBQUNOLENBQUM7O0lBckNDLGlCQUFpQixHQUFHLGFBQWEsQ0FDbkMsWUFBWSxFQUNaLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLE9BRzNCLEVBQ0gsRUFBRSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixPQUdsQyxFQUNILEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsT0FrQnpDLEVBQ0YsRUFBRSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixPQVFuQyxDQUNMOzs7Ozs7QUFHRCxNQUFNLFVBQVUsT0FBTyxDQUFDLEtBQXNCLEVBQUUsTUFBYztJQUMxRCxPQUFPLGlCQUFpQixDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztBQUM1QyxDQUFDOzs7OztBQUVELFNBQVMsYUFBYSxDQUFDLFFBQTBCO0lBQzdDLE9BQU8sUUFBUSxDQUFDLEdBQUc7Ozs7SUFBQyxVQUFBLE9BQU87O1lBQ2pCLElBQUksd0JBQ0gsT0FBTyxJQUNWLE1BQU0sRUFBRSxLQUFLLEVBQ2IsT0FBTyxFQUFFLElBQUksR0FDaEI7UUFDRCxJQUFJLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDZixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztZQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDN0M7UUFFRCxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFFekUsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQyxFQUFDLENBQUM7QUFDUCxDQUFDOzs7OztBQUVELFNBQVMsVUFBVSxDQUFDLFFBQTBCOztRQUN0QyxLQUFLLEdBQUcsUUFBUSxDQUFDLE1BQU07SUFDM0IsUUFBUSxDQUFDLE1BQU07Ozs7SUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFULENBQVMsRUFBQyxDQUFDLE9BQU87Ozs7SUFBQyxVQUFBLENBQUM7UUFDckMsS0FBSyxJQUFJLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQyxFQUFDLENBQUM7SUFDSCxPQUFPLEtBQUssQ0FBQztBQUNqQixDQUFDOzs7OztBQUVELFNBQVMsWUFBWSxDQUFDLFFBQTBCOztRQUN4QyxPQUFPLEdBQUcsUUFBUSxDQUFDLE1BQU07Ozs7SUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFYLENBQVcsRUFBQyxDQUFDLE1BQU07SUFDdEQsUUFBUSxDQUFDLE1BQU07Ozs7SUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFULENBQVMsRUFBQyxDQUFDLE9BQU87Ozs7SUFBQyxVQUFBLENBQUM7UUFDckMsT0FBTyxJQUFJLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQyxFQUFDLENBQUM7SUFDSCxPQUFPLE9BQU8sQ0FBQztBQUNuQixDQUFDOzs7Ozs7QUFFRCxTQUFTLGlCQUFpQixDQUFDLFFBQTBCLEVBQUUsU0FBaUI7SUFDcEUsT0FBTyxRQUFRLENBQUMsR0FBRzs7OztJQUFDLFVBQUEsT0FBTzs7WUFDakIsUUFBUSxHQUFHLFlBQVksQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDOztZQUMzQyxJQUFJLHdCQUNILE9BQU8sSUFDVixNQUFNLEVBQUUsUUFBUSxHQUNuQjtRQUNELElBQUksT0FBTyxDQUFDLEtBQUssRUFBRTtZQUNmLElBQUksQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQztTQUM1RDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUMsRUFBQyxDQUFDO0FBQ1AsQ0FBQzs7Ozs7O0FBRUQsU0FBUyxZQUFZLENBQUMsSUFBb0IsRUFBRSxTQUFpQjtJQUN6RCxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxFQUFFO1FBQy9CLE9BQU8sSUFBSSxDQUFDO0tBQ2Y7U0FBTSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7UUFDbkIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLFlBQVksQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLEVBQTFCLENBQTBCLEVBQUMsQ0FBQztLQUMzRDtTQUFNO1FBQ0gsT0FBTyxLQUFLLENBQUM7S0FDaEI7QUFDTCxDQUFDOzs7Ozs7QUFFRCxTQUFTLFdBQVcsQ0FBQyxRQUEwQixFQUFFLFdBQTJCOztRQUVsRSxrQkFBa0IsR0FBRyxRQUFRLENBQUMsSUFBSTs7OztJQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxLQUFLLFdBQVcsRUFBakIsQ0FBaUIsRUFBQztJQUNoRSxPQUFPLFFBQVEsQ0FBQyxHQUFHOzs7O0lBQUMsVUFBQSxPQUFPOztZQUVqQixJQUFJLHdCQUNILE9BQU8sQ0FDYjtRQUVELElBQUksa0JBQWtCLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxPQUFPLEtBQUssV0FBVyxFQUFFO1lBQzdELElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1NBQ3JCO1FBQ0QsSUFBSSxPQUFPLEtBQUssV0FBVyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1NBQzdCO1FBQ0QsSUFBSSxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ2YsSUFBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQztTQUN4RDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUMsRUFBQyxDQUFDO0FBQ1AsQ0FBQzs7Ozs7O0FBRUQsU0FBUyxXQUFXLENBQUMsUUFBMEIsRUFBRSxJQUFZO0lBRXpELE9BQU8sUUFBUSxDQUFDLEdBQUc7Ozs7SUFBQyxVQUFBLE9BQU87O1lBRWpCLElBQUksd0JBQ0gsT0FBTyxDQUNiO1FBRUQsSUFBSSxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ2YsSUFBSSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSTs7OztZQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsWUFBWSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUFDO1lBQzdGLElBQUksQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDakQ7YUFBTTtZQUNILElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztTQUM5QztRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUMsRUFBQyxDQUFDO0FBQ1AsQ0FBQzs7Ozs7O0FBRUQsU0FBUyxZQUFZLENBQUMsSUFBb0IsRUFBRSxJQUFZO0lBQ3BELE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksTUFBTSxDQUFDLE9BQUssSUFBSSxDQUFDLElBQUksRUFBRSxPQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFFdEcsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7QWN0aW9uLCBjcmVhdGVSZWR1Y2VyLCBvbn0gZnJvbSAnQG5ncngvc3RvcmUnO1xyXG5pbXBvcnQgKiBhcyBOYXZpZ2F0aW9uQWN0aW9ucyBmcm9tICcuL25hdmlnYXRpb24uYWN0aW9ucyc7XHJcbmltcG9ydCB7TmF2aWdhdGlvbkl0ZW1zfSBmcm9tICcuLi8uLi9jb3JlLm5hdmlnYXRpb24nO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBOYXZpZ2F0aW9uSXRlbSB7XHJcbiAgICBiYWRnZTogYW55O1xyXG4gICAgaXRlbTogYW55O1xyXG4gICAgbmFtZTogc3RyaW5nO1xyXG4gICAgdGl0bGU6IHN0cmluZztcclxuICAgIGljb24/OiBzdHJpbmc7XHJcbiAgICB0YWdzPzogc3RyaW5nO1xyXG4gICAgcm91dGVyTGluaz86IHN0cmluZztcclxuICAgIHVybD86IHN0cmluZztcclxuICAgIGFjdGl2ZT86IGJvb2xlYW47XHJcbiAgICBvcGVuPzogYm9vbGVhbjtcclxuICAgIGl0ZW1zPzogTmF2aWdhdGlvbkl0ZW1bXTtcclxuICAgIG1hdGNoZWQ/OiBib29sZWFuO1xyXG4gICAgbmF2VGl0bGU/OiBib29sZWFuO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIE5hdmlnYXRpb25TdGF0ZSB7XHJcbiAgICBpdGVtczogTmF2aWdhdGlvbkl0ZW1bXTtcclxuICAgIHRvdGFsOiBudW1iZXI7XHJcbiAgICBmaWx0ZXJBY3RpdmU6IGJvb2xlYW47XHJcbiAgICBmaWx0ZXJUZXh0OiBzdHJpbmc7XHJcbiAgICBtYXRjaGVkOiBudW1iZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBpbml0aWFsU3RhdGU6IE5hdmlnYXRpb25TdGF0ZSA9IHtcclxuICAgIGl0ZW1zOiBkZWNvcmF0ZUl0ZW1zKE5hdmlnYXRpb25JdGVtcyksXHJcbiAgICB0b3RhbDogY291bnRUb3RhbChOYXZpZ2F0aW9uSXRlbXMpLFxyXG4gICAgZmlsdGVyQWN0aXZlOiBmYWxzZSxcclxuICAgIGZpbHRlclRleHQ6ICcnLFxyXG4gICAgbWF0Y2hlZDogMFxyXG59O1xyXG5cclxuXHJcbmNvbnN0IG5hdmlnYXRpb25SZWR1Y2VyID0gY3JlYXRlUmVkdWNlcihcclxuICAgIGluaXRpYWxTdGF0ZSxcclxuICAgIG9uKE5hdmlnYXRpb25BY3Rpb25zLmFjdGl2ZVVybCwgKHN0YXRlLCBhY3Rpb24pID0+ICh7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgaXRlbXM6IGRldGVjdEFjdGl2ZUl0ZW1zKHN0YXRlLml0ZW1zLCBhY3Rpb24udXJsKVxyXG4gICAgfSkpLFxyXG4gICAgb24oTmF2aWdhdGlvbkFjdGlvbnMudG9nZ2xlTmF2U2VjdGlvbiwgKHN0YXRlLCBhY3Rpb24pID0+ICh7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgaXRlbXM6IHRvZ2dsZUl0ZW1zKHN0YXRlLml0ZW1zLCBhY3Rpb24uaXRlbSlcclxuICAgIH0pKSxcclxuICAgIG9uKE5hdmlnYXRpb25BY3Rpb25zLnRvZ2dsZU5hdmlnYXRpb25GaWx0ZXIsIHN0YXRlID0+IHtcclxuICAgICAgICBpZiAoc3RhdGUuZmlsdGVyQWN0aXZlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIGZpbHRlckFjdGl2ZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBtYXRjaGVkOiAwLFxyXG4gICAgICAgICAgICAgICAgaXRlbXM6IHN0YXRlLml0ZW1zLm1hcChfID0+ICh7Li4uXywgbWF0Y2hlZDogbnVsbH0pKVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGl0ZW1zID0gZmlsdGVySXRlbXMoc3RhdGUuaXRlbXMsIHN0YXRlLmZpbHRlclRleHQpO1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJBY3RpdmU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBpdGVtcyxcclxuICAgICAgICAgICAgICAgIG1hdGNoZWQ6IGNvdW50TWF0Y2hlZChpdGVtcyksXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH0pLFxyXG4gICAgb24oTmF2aWdhdGlvbkFjdGlvbnMubmF2aWdhdGlvbkZpbHRlciwgKHN0YXRlLCBhY3Rpb24pID0+IHtcclxuICAgICAgICBjb25zdCBpdGVtcyA9IGZpbHRlckl0ZW1zKHN0YXRlLml0ZW1zLCBhY3Rpb24udGV4dCk7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgIGZpbHRlclRleHQ6IGFjdGlvbi50ZXh0LFxyXG4gICAgICAgICAgICBpdGVtcyxcclxuICAgICAgICAgICAgbWF0Y2hlZDogY291bnRNYXRjaGVkKGl0ZW1zKSxcclxuICAgICAgICB9O1xyXG4gICAgfSlcclxuKTtcclxuXHJcblxyXG5leHBvcnQgZnVuY3Rpb24gcmVkdWNlcihzdGF0ZTogTmF2aWdhdGlvblN0YXRlLCBhY3Rpb246IEFjdGlvbikge1xyXG4gICAgcmV0dXJuIG5hdmlnYXRpb25SZWR1Y2VyKHN0YXRlLCBhY3Rpb24pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBkZWNvcmF0ZUl0ZW1zKG5hdkl0ZW1zOiBOYXZpZ2F0aW9uSXRlbVtdKTogTmF2aWdhdGlvbkl0ZW1bXSB7XHJcbiAgICByZXR1cm4gbmF2SXRlbXMubWFwKG5hdkl0ZW0gPT4ge1xyXG4gICAgICAgIGNvbnN0IGl0ZW06IE5hdmlnYXRpb25JdGVtID0ge1xyXG4gICAgICAgICAgICAuLi5uYXZJdGVtLFxyXG4gICAgICAgICAgICBhY3RpdmU6IGZhbHNlLFxyXG4gICAgICAgICAgICBtYXRjaGVkOiBudWxsXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAobmF2SXRlbS5pdGVtcykge1xyXG4gICAgICAgICAgICBpdGVtLm9wZW4gPSBmYWxzZTtcclxuICAgICAgICAgICAgaXRlbS5pdGVtcyA9IGRlY29yYXRlSXRlbXMobmF2SXRlbS5pdGVtcyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpdGVtLm5hdlRpdGxlID0gIW5hdkl0ZW0uaXRlbXMgJiYgIW5hdkl0ZW0ucm91dGVyTGluayAmJiAhIW5hdkl0ZW0udGl0bGU7XHJcblxyXG4gICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNvdW50VG90YWwobmF2SXRlbXM6IE5hdmlnYXRpb25JdGVtW10pOiBudW1iZXIge1xyXG4gICAgbGV0IHRvdGFsID0gbmF2SXRlbXMubGVuZ3RoO1xyXG4gICAgbmF2SXRlbXMuZmlsdGVyKF8gPT4gISFfLml0ZW1zKS5mb3JFYWNoKF8gPT4ge1xyXG4gICAgICAgIHRvdGFsICs9IGNvdW50VG90YWwoXy5pdGVtcyk7XHJcbiAgICB9KTtcclxuICAgIHJldHVybiB0b3RhbDtcclxufVxyXG5cclxuZnVuY3Rpb24gY291bnRNYXRjaGVkKG5hdkl0ZW1zOiBOYXZpZ2F0aW9uSXRlbVtdKTogbnVtYmVyIHtcclxuICAgIGxldCBtYXRjaGVkID0gbmF2SXRlbXMuZmlsdGVyKF8gPT4gISFfLm1hdGNoZWQpLmxlbmd0aDtcclxuICAgIG5hdkl0ZW1zLmZpbHRlcihfID0+ICEhXy5pdGVtcykuZm9yRWFjaChfID0+IHtcclxuICAgICAgICBtYXRjaGVkICs9IGNvdW50TWF0Y2hlZChfLml0ZW1zKTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIG1hdGNoZWQ7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGRldGVjdEFjdGl2ZUl0ZW1zKG5hdkl0ZW1zOiBOYXZpZ2F0aW9uSXRlbVtdLCBhY3RpdmVVcmw6IHN0cmluZyk6IE5hdmlnYXRpb25JdGVtW10ge1xyXG4gICAgcmV0dXJuIG5hdkl0ZW1zLm1hcChuYXZJdGVtID0+IHtcclxuICAgICAgICBjb25zdCBpc0FjdGl2ZSA9IGl0ZW1Jc0FjdGl2ZShuYXZJdGVtLCBhY3RpdmVVcmwpO1xyXG4gICAgICAgIGNvbnN0IGl0ZW0gPSB7XHJcbiAgICAgICAgICAgIC4uLm5hdkl0ZW0sXHJcbiAgICAgICAgICAgIGFjdGl2ZTogaXNBY3RpdmVcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmIChuYXZJdGVtLml0ZW1zKSB7XHJcbiAgICAgICAgICAgIGl0ZW0ub3BlbiA9IGlzQWN0aXZlO1xyXG4gICAgICAgICAgICBpdGVtLml0ZW1zID0gZGV0ZWN0QWN0aXZlSXRlbXMobmF2SXRlbS5pdGVtcywgYWN0aXZlVXJsKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGl0ZW07XHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gaXRlbUlzQWN0aXZlKGl0ZW06IE5hdmlnYXRpb25JdGVtLCBhY3RpdmVVcmw6IHN0cmluZykge1xyXG4gICAgaWYgKGl0ZW0ucm91dGVyTGluayA9PT0gYWN0aXZlVXJsKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9IGVsc2UgaWYgKGl0ZW0uaXRlbXMpIHtcclxuICAgICAgICByZXR1cm4gaXRlbS5pdGVtcy5zb21lKF8gPT4gaXRlbUlzQWN0aXZlKF8sIGFjdGl2ZVVybCkpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRvZ2dsZUl0ZW1zKG5hdkl0ZW1zOiBOYXZpZ2F0aW9uSXRlbVtdLCB0b2dnbGVkSXRlbTogTmF2aWdhdGlvbkl0ZW0pOiBOYXZpZ2F0aW9uSXRlbVtdIHtcclxuXHJcbiAgICBjb25zdCBpc1RvZ2dsZWRJdGVtTGV2ZWwgPSBuYXZJdGVtcy5zb21lKF8gPT4gXyA9PT0gdG9nZ2xlZEl0ZW0pO1xyXG4gICAgcmV0dXJuIG5hdkl0ZW1zLm1hcChuYXZJdGVtID0+IHtcclxuXHJcbiAgICAgICAgY29uc3QgaXRlbSA9IHtcclxuICAgICAgICAgICAgLi4ubmF2SXRlbVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGlmIChpc1RvZ2dsZWRJdGVtTGV2ZWwgJiYgaXRlbS5pdGVtcyAmJiBuYXZJdGVtICE9PSB0b2dnbGVkSXRlbSkge1xyXG4gICAgICAgICAgICBpdGVtLm9wZW4gPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKG5hdkl0ZW0gPT09IHRvZ2dsZWRJdGVtKSB7XHJcbiAgICAgICAgICAgIGl0ZW0ub3BlbiA9ICFuYXZJdGVtLm9wZW47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChuYXZJdGVtLml0ZW1zKSB7XHJcbiAgICAgICAgICAgIGl0ZW0uaXRlbXMgPSB0b2dnbGVJdGVtcyhuYXZJdGVtLml0ZW1zLCB0b2dnbGVkSXRlbSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGZpbHRlckl0ZW1zKG5hdkl0ZW1zOiBOYXZpZ2F0aW9uSXRlbVtdLCB0ZXh0OiBzdHJpbmcpOiBOYXZpZ2F0aW9uSXRlbVtdIHtcclxuXHJcbiAgICByZXR1cm4gbmF2SXRlbXMubWFwKG5hdkl0ZW0gPT4ge1xyXG5cclxuICAgICAgICBjb25zdCBpdGVtID0ge1xyXG4gICAgICAgICAgICAuLi5uYXZJdGVtLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGlmIChuYXZJdGVtLml0ZW1zKSB7XHJcbiAgICAgICAgICAgIGl0ZW0ubWF0Y2hlZCA9IG5hdkl0ZW1NYXRjaChuYXZJdGVtLCB0ZXh0KSB8fCBuYXZJdGVtLml0ZW1zLnNvbWUoXyA9PiBuYXZJdGVtTWF0Y2goXywgdGV4dCkpO1xyXG4gICAgICAgICAgICBpdGVtLml0ZW1zID0gZmlsdGVySXRlbXMobmF2SXRlbS5pdGVtcywgdGV4dCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaXRlbS5tYXRjaGVkID0gbmF2SXRlbU1hdGNoKG5hdkl0ZW0sIHRleHQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBuYXZJdGVtTWF0Y2goaXRlbTogTmF2aWdhdGlvbkl0ZW0sIHRleHQ6IHN0cmluZykge1xyXG4gICAgcmV0dXJuICghdGV4dC50cmltKCkgfHwgKGl0ZW0udGFncyAmJiAhIWl0ZW0udGFncy5tYXRjaChuZXcgUmVnRXhwKGAuKiR7dGV4dC50cmltKCl9LipgLCAnZ2knKSkpKTtcclxuXHJcbn1cclxuIl19