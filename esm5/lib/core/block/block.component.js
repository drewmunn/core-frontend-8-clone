/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/block/block.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, ComponentFactoryResolver, Input, ViewContainerRef } from '@angular/core';
import { PageService } from '../pages/page.service';
import { Block } from './block-block';
import { BlockService } from '../pages/block.service';
var BlockComponent = /** @class */ (function () {
    function BlockComponent(pages, blocks, resolver, viewContainerRef) {
        this.pages = pages;
        this.blocks = blocks;
        this.resolver = resolver;
        this.viewContainerRef = viewContainerRef;
    }
    /**
     * @return {?}
     */
    BlockComponent.prototype.loadComponent = /**
     * @return {?}
     */
    function () {
        if (!this.blocks.hasBlock(this.blockWrapper.identifier)) {
            throw new Error(this.blockWrapper.id + " has not been mapped (" + this.blockWrapper.identifier + ")");
        }
        /** @type {?} */
        var componentFactory = this.resolver.resolveComponentFactory(this.blocks.getBlock(this.blockWrapper.identifier));
        /** @type {?} */
        var componentRef = this.viewContainerRef.createComponent(componentFactory);
        this.pages.sortBlocks(this.block);
        // @ts-ignore
        componentRef.instance.block = this.block;
        componentRef.changeDetectorRef.detectChanges();
    };
    /**
     * @return {?}
     */
    BlockComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.subscription = this.pages.getBlock(this.block.widgetId).then((/**
         * @param {?} block
         * @return {?}
         */
        function (block) {
            _this.blockWrapper = block;
            _this.loadComponent();
        }));
    };
    BlockComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block',
                    template: "<!-- BLOCK -->",
                    changeDetection: ChangeDetectionStrategy.Default
                }] }
    ];
    /** @nocollapse */
    BlockComponent.ctorParameters = function () { return [
        { type: PageService },
        { type: BlockService },
        { type: ComponentFactoryResolver },
        { type: ViewContainerRef }
    ]; };
    BlockComponent.propDecorators = {
        block: [{ type: Input }]
    };
    return BlockComponent;
}());
export { BlockComponent };
if (false) {
    /** @type {?} */
    BlockComponent.prototype.block;
    /** @type {?} */
    BlockComponent.prototype.blockWrapper;
    /** @type {?} */
    BlockComponent.prototype.subscription;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.pages;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.blocks;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.resolver;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.viewContainerRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2suY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9jay9ibG9jay5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQ0gsdUJBQXVCLEVBQ3ZCLFNBQVMsRUFDVCx3QkFBd0IsRUFDeEIsS0FBSyxFQUVMLGdCQUFnQixFQUNuQixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDbEQsT0FBTyxFQUFDLEtBQUssRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNwQyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFFcEQ7SUFZSSx3QkFDYyxLQUFrQixFQUNsQixNQUFvQixFQUNwQixRQUFrQyxFQUNsQyxnQkFBa0M7UUFIbEMsVUFBSyxHQUFMLEtBQUssQ0FBYTtRQUNsQixXQUFNLEdBQU4sTUFBTSxDQUFjO1FBQ3BCLGFBQVEsR0FBUixRQUFRLENBQTBCO1FBQ2xDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFFaEQsQ0FBQzs7OztJQUVELHNDQUFhOzs7SUFBYjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQ3JELE1BQU0sSUFBSSxLQUFLLENBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLDhCQUF5QixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsTUFBRyxDQUFDLENBQUM7U0FDcEc7O1lBQ0ssZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDOztZQUM1RyxZQUFZLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQztRQUU1RSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFbEMsYUFBYTtRQUNiLFlBQVksQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDekMsWUFBWSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ25ELENBQUM7Ozs7SUFFRCxpQ0FBUTs7O0lBQVI7UUFBQSxpQkFLQztRQUpHLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxLQUFZO1lBQzNFLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzFCLEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN6QixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7O2dCQXZDSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLDBCQUFxQztvQkFDckMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE9BQU87aUJBQ25EOzs7O2dCQVJPLFdBQVc7Z0JBRVgsWUFBWTtnQkFQaEIsd0JBQXdCO2dCQUd4QixnQkFBZ0I7Ozt3QkFjZixLQUFLOztJQWlDVixxQkFBQztDQUFBLEFBekNELElBeUNDO1NBbkNZLGNBQWM7OztJQUV2QiwrQkFBc0I7O0lBQ3RCLHNDQUFvQjs7SUFDcEIsc0NBQWtCOzs7OztJQUdkLCtCQUE0Qjs7Ozs7SUFDNUIsZ0NBQThCOzs7OztJQUM5QixrQ0FBNEM7Ozs7O0lBQzVDLDBDQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSxcclxuICAgIENvbXBvbmVudCxcclxuICAgIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcclxuICAgIElucHV0LFxyXG4gICAgT25Jbml0LFxyXG4gICAgVmlld0NvbnRhaW5lclJlZlxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1BhZ2VTZXJ2aWNlfSBmcm9tICcuLi9wYWdlcy9wYWdlLnNlcnZpY2UnO1xyXG5pbXBvcnQge0Jsb2NrfSBmcm9tICcuL2Jsb2NrLWJsb2NrJztcclxuaW1wb3J0IHtCbG9ja1NlcnZpY2V9IGZyb20gJy4uL3BhZ2VzL2Jsb2NrLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2snLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLmNvbXBvbmVudC5odG1sJyxcclxuICAgIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuRGVmYXVsdFxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEJsb2NrQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSBibG9jazogQmxvY2s7XHJcbiAgICBibG9ja1dyYXBwZXI6IEJsb2NrO1xyXG4gICAgc3Vic2NyaXB0aW9uOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJvdGVjdGVkIHBhZ2VzOiBQYWdlU2VydmljZSxcclxuICAgICAgICBwcm90ZWN0ZWQgYmxvY2tzOiBCbG9ja1NlcnZpY2UsXHJcbiAgICAgICAgcHJvdGVjdGVkIHJlc29sdmVyOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXHJcbiAgICAgICAgcHJvdGVjdGVkIHZpZXdDb250YWluZXJSZWY6IFZpZXdDb250YWluZXJSZWZcclxuICAgICkge1xyXG4gICAgfVxyXG5cclxuICAgIGxvYWRDb21wb25lbnQoKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmJsb2Nrcy5oYXNCbG9jayh0aGlzLmJsb2NrV3JhcHBlci5pZGVudGlmaWVyKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYCR7dGhpcy5ibG9ja1dyYXBwZXIuaWR9IGhhcyBub3QgYmVlbiBtYXBwZWQgKCR7dGhpcy5ibG9ja1dyYXBwZXIuaWRlbnRpZmllcn0pYCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGNvbXBvbmVudEZhY3RvcnkgPSB0aGlzLnJlc29sdmVyLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KHRoaXMuYmxvY2tzLmdldEJsb2NrKHRoaXMuYmxvY2tXcmFwcGVyLmlkZW50aWZpZXIpKTtcclxuICAgICAgICBjb25zdCBjb21wb25lbnRSZWYgPSB0aGlzLnZpZXdDb250YWluZXJSZWYuY3JlYXRlQ29tcG9uZW50KGNvbXBvbmVudEZhY3RvcnkpO1xyXG5cclxuICAgICAgICB0aGlzLnBhZ2VzLnNvcnRCbG9ja3ModGhpcy5ibG9jayk7XHJcblxyXG4gICAgICAgIC8vIEB0cy1pZ25vcmVcclxuICAgICAgICBjb21wb25lbnRSZWYuaW5zdGFuY2UuYmxvY2sgPSB0aGlzLmJsb2NrO1xyXG4gICAgICAgIGNvbXBvbmVudFJlZi5jaGFuZ2VEZXRlY3RvclJlZi5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb24gPSB0aGlzLnBhZ2VzLmdldEJsb2NrKHRoaXMuYmxvY2sud2lkZ2V0SWQpLnRoZW4oKGJsb2NrOiBCbG9jaykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmJsb2NrV3JhcHBlciA9IGJsb2NrO1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRDb21wb25lbnQoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19