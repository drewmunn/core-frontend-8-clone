/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/block/block-block.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var Block = /** @class */ (function () {
    function Block(component) {
        this.component = component;
    }
    return Block;
}());
export { Block };
if (false) {
    /** @type {?} */
    Block.prototype.id;
    /** @type {?} */
    Block.prototype.identifier;
    /** @type {?} */
    Block.prototype.widgetId;
    /** @type {?} */
    Block.prototype.content;
    /** @type {?} */
    Block.prototype.uid;
    /** @type {?} */
    Block.prototype.data;
    /** @type {?} */
    Block.prototype.component;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYmxvY2suanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2NrL2Jsb2NrLWJsb2NrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBRUE7SUFTSSxlQUFtQixTQUFvQjtRQUFwQixjQUFTLEdBQVQsU0FBUyxDQUFXO0lBQ3ZDLENBQUM7SUFDTCxZQUFDO0FBQUQsQ0FBQyxBQVhELElBV0M7Ozs7SUFURyxtQkFBVzs7SUFDWCwyQkFBbUI7O0lBQ25CLHlCQUFpQjs7SUFDakIsd0JBQWU7O0lBQ2Ysb0JBQVM7O0lBQ1QscUJBQVU7O0lBRUUsMEJBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtUeXBlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZXhwb3J0IGNsYXNzIEJsb2NrIHtcblxuICAgIGlkOiBudW1iZXI7XG4gICAgaWRlbnRpZmllcjogbnVtYmVyO1xuICAgIHdpZGdldElkOiBudW1iZXI7XG4gICAgY29udGVudDogYW55W107XG4gICAgdWlkOiBhbnk7XG4gICAgZGF0YTogYW55O1xuXG4gICAgY29uc3RydWN0b3IocHVibGljIGNvbXBvbmVudDogVHlwZTxhbnk+KSB7XG4gICAgfVxufVxuIl19