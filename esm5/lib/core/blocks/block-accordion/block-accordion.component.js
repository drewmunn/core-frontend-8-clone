/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-accordion/block-accordion.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
var BlockAccordionComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockAccordionComponent, _super);
    function BlockAccordionComponent(authService) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        return _this;
    }
    /**
     * @return {?}
     */
    BlockAccordionComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} idx
     * @return {?}
     */
    BlockAccordionComponent.prototype.trackByFn = /**
     * @param {?} idx
     * @return {?}
     */
    function (idx) {
        return idx;
    };
    BlockAccordionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-accordion',
                    template: "<mat-accordion>\n    <mat-expansion-panel *ngFor=\"let item of block.content trackBy: trackByFn\">\n        <mat-expansion-panel-header>\n            <mat-panel-title [innerHTML]=\"item.data.title\"></mat-panel-title>\n        </mat-expansion-panel-header>\n        <div class=\"{{block.data.title}} {{block.data.visibility}}\"\n             [ngClass]=\"{container: block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n                 'stickyDiv': block.data.specialType == 'sticky',\n                  'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n                  'col-sm-12': block.data.columnsSmall == 'full',\n                  'col-md-12': block.data.columnsNormal == 'full'}\"\n             [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n        >\n            <core-block\n                    *ngFor=\"let subBlock of item.content\"\n                    [block]=\"subBlock\"\n                    class=\"subWidget {{subBlock.data.class}}\">\n            </core-block>\n        </div>\n    </mat-expansion-panel>\n</mat-accordion>\n"
                }] }
    ];
    /** @nocollapse */
    BlockAccordionComponent.ctorParameters = function () { return [
        { type: AuthService }
    ]; };
    return BlockAccordionComponent;
}(BlockAbstractComponent));
export { BlockAccordionComponent };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockAccordionComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYWNjb3JkaW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYmxvY2tzL2Jsb2NrLWFjY29yZGlvbi9ibG9jay1hY2NvcmRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBRXBEO0lBSTZDLG1EQUFzQjtJQUUvRCxpQ0FDYyxXQUF3QjtRQUR0QyxZQUdJLGtCQUNJLFdBQVcsQ0FDZCxTQUNKO1FBTGEsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBS3RDLENBQUM7Ozs7SUFFRCwwQ0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7OztJQUVELDJDQUFTOzs7O0lBQVQsVUFBVSxHQUFXO1FBQ2pCLE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQzs7Z0JBbkJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsc0JBQXNCO29CQUNoQyx3bkNBQStDO2lCQUNsRDs7OztnQkFMTyxXQUFXOztJQXVCbkIsOEJBQUM7Q0FBQSxBQXJCRCxDQUk2QyxzQkFBc0IsR0FpQmxFO1NBakJZLHVCQUF1Qjs7Ozs7O0lBRzVCLDhDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vLi4vYXV0aC9hdXRoLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2stYWNjb3JkaW9uJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vYmxvY2stYWNjb3JkaW9uLmNvbXBvbmVudC5odG1sJyxcbn0pXG5leHBvcnQgY2xhc3MgQmxvY2tBY2NvcmRpb25Db21wb25lbnQgZXh0ZW5kcyBCbG9ja0Fic3RyYWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgfVxuXG4gICAgdHJhY2tCeUZuKGlkeDogbnVtYmVyKSB7XG4gICAgICAgIHJldHVybiBpZHg7XG4gICAgfVxuXG59XG4iXX0=