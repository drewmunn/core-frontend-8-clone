/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-blockquote/block-blockquote.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
var BlockBlockquoteComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockBlockquoteComponent, _super);
    function BlockBlockquoteComponent(authService) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        return _this;
    }
    /**
     * @return {?}
     */
    BlockBlockquoteComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        _super.prototype.ngOnInit.call(this);
        this.text = this.block.data.blockQuote;
    };
    BlockBlockquoteComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-blockquote',
                    template: "<blockquote>\n    <p>\n        <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n    </p>\n    <footer *ngIf=\"block.data.cite\">\n        <cite>\n            <a [href]=\"block.data.citeLink\" *ngIf=\"block.data.citeLink\">{{block.data.cite}}</a>\n            <span *ngIf=\"!block.data.citeLink\">{{block.data.cite}}</span>\n        </cite>\n    </footer>\n</blockquote>"
                }] }
    ];
    /** @nocollapse */
    BlockBlockquoteComponent.ctorParameters = function () { return [
        { type: AuthService }
    ]; };
    BlockBlockquoteComponent.propDecorators = {
        text: [{ type: Input }]
    };
    return BlockBlockquoteComponent;
}(BlockAbstractComponent));
export { BlockBlockquoteComponent };
if (false) {
    /** @type {?} */
    BlockBlockquoteComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockBlockquoteComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYmxvY2txdW90ZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2Nrcy9ibG9jay1ibG9ja3F1b3RlL2Jsb2NrLWJsb2NrcXVvdGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBQ25FLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRDtJQUs4QyxvREFBc0I7SUFJaEUsa0NBQ2MsV0FBd0I7UUFEdEMsWUFHSSxrQkFDSSxXQUFXLENBQ2QsU0FDSjtRQUxhLGlCQUFXLEdBQVgsV0FBVyxDQUFhOztJQUt0QyxDQUFDOzs7O0lBRUQsMkNBQVE7OztJQUFSO1FBQ0ksaUJBQU0sUUFBUSxXQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0MsQ0FBQzs7Z0JBcEJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsdUJBQXVCO29CQUNqQyxxWkFBZ0Q7aUJBQ25EOzs7O2dCQUxPLFdBQVc7Ozt1QkFTZCxLQUFLOztJQWVWLCtCQUFDO0NBQUEsQUF0QkQsQ0FLOEMsc0JBQXNCLEdBaUJuRTtTQWpCWSx3QkFBd0I7OztJQUVqQyx3Q0FBc0I7Ozs7O0lBR2xCLCtDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tBYnN0cmFjdENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcbmltcG9ydCB7QXV0aFNlcnZpY2V9IGZyb20gJy4uLy4uL2F1dGgvYXV0aC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdjb3JlLWJsb2NrLWJsb2NrcXVvdGUnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9ibG9jay1ibG9ja3F1b3RlLmNvbXBvbmVudC5odG1sJyxcbn0pXG5cbmV4cG9ydCBjbGFzcyBCbG9ja0Jsb2NrcXVvdGVDb21wb25lbnQgZXh0ZW5kcyBCbG9ja0Fic3RyYWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIEBJbnB1dCgpIHRleHQ6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgICAgIHRoaXMudGV4dCA9IHRoaXMuYmxvY2suZGF0YS5ibG9ja1F1b3RlO1xuICAgIH1cblxufVxuIl19