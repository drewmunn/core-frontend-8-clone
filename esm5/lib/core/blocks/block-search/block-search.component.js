/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-search/block-search.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { FormControl } from '@angular/forms';
import { SearchService } from '../../pages/search.service';
var BlockSearchComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockSearchComponent, _super);
    function BlockSearchComponent(search) {
        var _this = _super.call(this) || this;
        _this.search = search;
        _this.query = new FormControl();
        return _this;
    }
    /**
     * @return {?}
     */
    BlockSearchComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.search.refresh();
        this.query.setValue(this.search.getQuery());
    };
    /**
     * @return {?}
     */
    BlockSearchComponent.prototype.performSearch = /**
     * @return {?}
     */
    function () {
        this.search.performSearch(this.query.value);
    };
    BlockSearchComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-search',
                    template: "<form #searchForm=\"ngForm\" (ngSubmit)=\"performSearch()\">\n    <div class=\"form-group\">\n        <label class=\"form-label question-box\">\n            <span class=\"sr-only\">Search</span></label>\n        <input type=\"text\" class=\"form-control form-control-lg\" [formControl]=\"query\"\n               placeholder=\"Type your question...\"/>\n    </div>\n\n    <button class=\"btn btn-primary\" [ngClass]=\"{'selected': query.value && query.value.length > 2}\"\n            [disabled]=\"!query.value || query.value.length < 3\">Search\n    </button>\n</form>"
                }] }
    ];
    /** @nocollapse */
    BlockSearchComponent.ctorParameters = function () { return [
        { type: SearchService }
    ]; };
    return BlockSearchComponent;
}(BlockAbstractComponent));
export { BlockSearchComponent };
if (false) {
    /** @type {?} */
    BlockSearchComponent.prototype.query;
    /**
     * @type {?}
     * @protected
     */
    BlockSearchComponent.prototype.search;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stc2VhcmNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYmxvY2tzL2Jsb2NrLXNlYXJjaC9ibG9jay1zZWFyY2guY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSw0QkFBNEIsQ0FBQztBQUV6RDtJQUkwQyxnREFBc0I7SUFHNUQsOEJBQ2MsTUFBcUI7UUFEbkMsWUFHSSxpQkFBTyxTQUNWO1FBSGEsWUFBTSxHQUFOLE1BQU0sQ0FBZTtRQUhuQyxXQUFLLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQzs7SUFNMUIsQ0FBQzs7OztJQUVELHVDQUFROzs7SUFBUjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7Ozs7SUFFRCw0Q0FBYTs7O0lBQWI7UUFDSSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hELENBQUM7O2dCQXBCSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLG1CQUFtQjtvQkFDN0Isb2tCQUE0QztpQkFDL0M7Ozs7Z0JBTE8sYUFBYTs7SUF3QnJCLDJCQUFDO0NBQUEsQUF0QkQsQ0FJMEMsc0JBQXNCLEdBa0IvRDtTQWxCWSxvQkFBb0I7OztJQUM3QixxQ0FBMEI7Ozs7O0lBR3RCLHNDQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtTZWFyY2hTZXJ2aWNlfSBmcm9tICcuLi8uLi9wYWdlcy9zZWFyY2guc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay1zZWFyY2gnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9ibG9jay1zZWFyY2guY29tcG9uZW50Lmh0bWwnLFxufSlcbmV4cG9ydCBjbGFzcyBCbG9ja1NlYXJjaENvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIHF1ZXJ5ID0gbmV3IEZvcm1Db250cm9sKCk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIHNlYXJjaDogU2VhcmNoU2VydmljZVxuICAgICkge1xuICAgICAgICBzdXBlcigpO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnNlYXJjaC5yZWZyZXNoKCk7XG4gICAgICAgIHRoaXMucXVlcnkuc2V0VmFsdWUodGhpcy5zZWFyY2guZ2V0UXVlcnkoKSk7XG4gICAgfVxuXG4gICAgcGVyZm9ybVNlYXJjaCgpIHtcbiAgICAgICAgdGhpcy5zZWFyY2gucGVyZm9ybVNlYXJjaCh0aGlzLnF1ZXJ5LnZhbHVlKTtcbiAgICB9XG5cbn1cbiJdfQ==