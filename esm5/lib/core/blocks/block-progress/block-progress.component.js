/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-progress/block-progress.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
var BlockProgressComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockProgressComponent, _super);
    function BlockProgressComponent() {
        return _super.call(this) || this;
    }
    /**
     * @return {?}
     */
    BlockProgressComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        _super.prototype.ngOnInit.call(this);
    };
    BlockProgressComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-progress',
                    template: "\n        <mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\n    "
                }] }
    ];
    /** @nocollapse */
    BlockProgressComponent.ctorParameters = function () { return []; };
    return BlockProgressComponent;
}(BlockAbstractComponent));
export { BlockProgressComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stcHJvZ3Jlc3MuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stcHJvZ3Jlc3MvYmxvY2stcHJvZ3Jlc3MuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFFbkU7SUFNNEMsa0RBQXNCO0lBRTlEO2VBQ0ksaUJBQU87SUFDWCxDQUFDOzs7O0lBRUQseUNBQVE7OztJQUFSO1FBQ0ksaUJBQU0sUUFBUSxXQUFFLENBQUM7SUFDckIsQ0FBQzs7Z0JBZEosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxxQkFBcUI7b0JBQy9CLFFBQVEsRUFBRSw4RUFFVDtpQkFDSjs7OztJQVdELDZCQUFDO0NBQUEsQUFoQkQsQ0FNNEMsc0JBQXNCLEdBVWpFO1NBVlksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jsb2NrQWJzdHJhY3RDb21wb25lbnR9IGZyb20gJy4uL2Jsb2NrLWFic3RyYWN0LmNvbXBvbmVudCc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay1wcm9ncmVzcycsXG4gICAgdGVtcGxhdGU6IGBcbiAgICAgICAgPG1hdC1wcm9ncmVzcy1iYXIgbW9kZT1cImluZGV0ZXJtaW5hdGVcIj48L21hdC1wcm9ncmVzcy1iYXI+XG4gICAgYCxcbn0pXG5leHBvcnQgY2xhc3MgQmxvY2tQcm9ncmVzc0NvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgfVxuXG59XG4iXX0=