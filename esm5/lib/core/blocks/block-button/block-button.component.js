/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-button/block-button.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
var BlockButtonComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockButtonComponent, _super);
    function BlockButtonComponent(authService) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        return _this;
    }
    /**
     * @return {?}
     */
    BlockButtonComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        _super.prototype.ngOnInit.call(this);
        this.text = this.block.data.buttonPreText;
    };
    BlockButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-button',
                    template: "<div class=\"button-block\">\n    <p *ngIf=\"block.data.buttonPreText\">\n        <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n    </p>\n    <a [href]=\"block.data.documentId ? block.data.documentId : block.data.buttonLink\"\n       target=\"{{block.data.target}}\" class=\"btn {{block.data.buttonType}}\" role=\"button\">{{block.data.buttonText}}\n    </a>\n</div>\n"
                }] }
    ];
    /** @nocollapse */
    BlockButtonComponent.ctorParameters = function () { return [
        { type: AuthService }
    ]; };
    BlockButtonComponent.propDecorators = {
        text: [{ type: Input }]
    };
    return BlockButtonComponent;
}(BlockAbstractComponent));
export { BlockButtonComponent };
if (false) {
    /** @type {?} */
    BlockButtonComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockButtonComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYmxvY2tzL2Jsb2NrLWJ1dHRvbi9ibG9jay1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBQ25FLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRDtJQUswQyxnREFBc0I7SUFJNUQsOEJBQ2MsV0FBd0I7UUFEdEMsWUFHSSxrQkFDSSxXQUFXLENBQ2QsU0FDSjtRQUxhLGlCQUFXLEdBQVgsV0FBVyxDQUFhOztJQUt0QyxDQUFDOzs7O0lBRUQsdUNBQVE7OztJQUFSO1FBQ0ksaUJBQU0sUUFBUSxXQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUMsQ0FBQzs7Z0JBcEJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsbUJBQW1CO29CQUM3Qix1WkFBNEM7aUJBQy9DOzs7O2dCQUxPLFdBQVc7Ozt1QkFTZCxLQUFLOztJQWVWLDJCQUFDO0NBQUEsQUF0QkQsQ0FLMEMsc0JBQXNCLEdBaUIvRDtTQWpCWSxvQkFBb0I7OztJQUU3QixvQ0FBc0I7Ozs7O0lBR2xCLDJDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tBYnN0cmFjdENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcbmltcG9ydCB7QXV0aFNlcnZpY2V9IGZyb20gJy4uLy4uL2F1dGgvYXV0aC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdjb3JlLWJsb2NrLWJ1dHRvbicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLWJ1dHRvbi5jb21wb25lbnQuaHRtbCcsXG59KVxuXG5leHBvcnQgY2xhc3MgQmxvY2tCdXR0b25Db21wb25lbnQgZXh0ZW5kcyBCbG9ja0Fic3RyYWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIEBJbnB1dCgpIHRleHQ6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgICAgIHRoaXMudGV4dCA9IHRoaXMuYmxvY2suZGF0YS5idXR0b25QcmVUZXh0O1xuICAgIH1cblxufVxuIl19