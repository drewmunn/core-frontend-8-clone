/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-table/block-table.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
var BlockTableComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockTableComponent, _super);
    function BlockTableComponent(authService) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        return _this;
    }
    /**
     * @return {?}
     */
    BlockTableComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.header = this.block.data.tableData[0];
        this.data = this.block.data.tableData;
        this.data.shift();
    };
    /**
     * @param {?} idx
     * @return {?}
     */
    BlockTableComponent.prototype.trackByFn = /**
     * @param {?} idx
     * @return {?}
     */
    function (idx) {
        return idx;
    };
    BlockTableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-table',
                    template: "<div class=\"table-wrapper\">\n    <table class=\"table\">\n        <thead>\n        <tr>\n            <th *ngFor=\"let cell of header trackBy: trackByFn\" [innerHTML]=\"cell.content\"></th>\n        </tr>\n        </thead>\n        <tbody>\n        <tr *ngFor=\"let row of data trackBy: trackByFn\">\n            <td *ngFor=\"let cell of row trackBy: trackByFn\" [innerHTML]=\"cell.content\"></td>\n        </tr>\n        </tbody>\n    </table>\n</div>"
                }] }
    ];
    /** @nocollapse */
    BlockTableComponent.ctorParameters = function () { return [
        { type: AuthService }
    ]; };
    return BlockTableComponent;
}(BlockAbstractComponent));
export { BlockTableComponent };
if (false) {
    /** @type {?} */
    BlockTableComponent.prototype.header;
    /** @type {?} */
    BlockTableComponent.prototype.data;
    /**
     * @type {?}
     * @protected
     */
    BlockTableComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stdGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stdGFibGUvYmxvY2stdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBRXBEO0lBSXlDLCtDQUFzQjtJQUkzRCw2QkFDYyxXQUF3QjtRQUR0QyxZQUdJLGtCQUNJLFdBQVcsQ0FDZCxTQUNKO1FBTGEsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBS3RDLENBQUM7Ozs7SUFFRCxzQ0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsdUNBQVM7Ozs7SUFBVCxVQUFVLEdBQVc7UUFDakIsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDOztnQkF4QkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLGlkQUEyQztpQkFDOUM7Ozs7Z0JBTE8sV0FBVzs7SUE0Qm5CLDBCQUFDO0NBQUEsQUExQkQsQ0FJeUMsc0JBQXNCLEdBc0I5RDtTQXRCWSxtQkFBbUI7OztJQUM1QixxQ0FBVzs7SUFDWCxtQ0FBUzs7Ozs7SUFHTCwwQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tBYnN0cmFjdENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcbmltcG9ydCB7QXV0aFNlcnZpY2V9IGZyb20gJy4uLy4uL2F1dGgvYXV0aC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdjb3JlLWJsb2NrLXRhYmxlJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vYmxvY2stdGFibGUuY29tcG9uZW50Lmh0bWwnLFxufSlcbmV4cG9ydCBjbGFzcyBCbG9ja1RhYmxlQ29tcG9uZW50IGV4dGVuZHMgQmxvY2tBYnN0cmFjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgaGVhZGVyOiBbXTtcbiAgICBkYXRhOiBbXTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMuaGVhZGVyID0gdGhpcy5ibG9jay5kYXRhLnRhYmxlRGF0YVswXTtcbiAgICAgICAgdGhpcy5kYXRhID0gdGhpcy5ibG9jay5kYXRhLnRhYmxlRGF0YTtcbiAgICAgICAgdGhpcy5kYXRhLnNoaWZ0KCk7XG4gICAgfVxuXG4gICAgdHJhY2tCeUZuKGlkeDogbnVtYmVyKSB7XG4gICAgICAgIHJldHVybiBpZHg7XG4gICAgfVxuXG59XG4iXX0=