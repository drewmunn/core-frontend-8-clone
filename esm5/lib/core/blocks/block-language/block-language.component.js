/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-language/block-language.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { TranslateService } from '../../pages/translate.service';
import { TranslateService as TranslateProvider } from '@ngx-translate/core';
var BlockLanguageComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockLanguageComponent, _super);
    function BlockLanguageComponent(translate, translateProvider) {
        var _this = _super.call(this) || this;
        _this.translate = translate;
        _this.translateProvider = translateProvider;
        return _this;
    }
    /**
     * @return {?}
     */
    BlockLanguageComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.translate.getLanguages().subscribe((/**
         * @param {?} languages
         * @return {?}
         */
        function (languages) {
            _this.languages$ = languages;
        }));
    };
    /**
     * @param {?} languageCode
     * @return {?}
     */
    BlockLanguageComponent.prototype.setLanguage = /**
     * @param {?} languageCode
     * @return {?}
     */
    function (languageCode) {
        this.translateProvider.use(languageCode);
        this.translate.setDefaultLanguage(languageCode);
    };
    BlockLanguageComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-language',
                    template: "<mat-nav-list>\n    <mat-list-item *ngFor=\"let language of languages$\" [ngClass]=\"{'active' : language.code === ''}\">\n        <a matLine (click)=\"setLanguage(language.code)\">{{language.name}}</a>\n    </mat-list-item>\n</mat-nav-list>"
                }] }
    ];
    /** @nocollapse */
    BlockLanguageComponent.ctorParameters = function () { return [
        { type: TranslateService },
        { type: TranslateProvider }
    ]; };
    return BlockLanguageComponent;
}(BlockAbstractComponent));
export { BlockLanguageComponent };
if (false) {
    /** @type {?} */
    BlockLanguageComponent.prototype.languages$;
    /** @type {?} */
    BlockLanguageComponent.prototype.language;
    /**
     * @type {?}
     * @protected
     */
    BlockLanguageComponent.prototype.translate;
    /**
     * @type {?}
     * @protected
     */
    BlockLanguageComponent.prototype.translateProvider;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stbGFuZ3VhZ2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stbGFuZ3VhZ2UvYmxvY2stbGFuZ3VhZ2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sK0JBQStCLENBQUM7QUFDL0QsT0FBTyxFQUFDLGdCQUFnQixJQUFJLGlCQUFpQixFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFFMUU7SUFLNEMsa0RBQXNCO0lBSTlELGdDQUNjLFNBQTJCLEVBQzNCLGlCQUFvQztRQUZsRCxZQUlJLGlCQUFPLFNBQ1Y7UUFKYSxlQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMzQix1QkFBaUIsR0FBakIsaUJBQWlCLENBQW1COztJQUdsRCxDQUFDOzs7O0lBRUQseUNBQVE7OztJQUFSO1FBQUEsaUJBSUM7UUFIRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLFNBQVM7WUFDN0MsS0FBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7UUFDaEMsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELDRDQUFXOzs7O0lBQVgsVUFBWSxZQUFvQjtRQUM1QixJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDcEQsQ0FBQzs7Z0JBekJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQiw2UEFBOEM7aUJBQ2pEOzs7O2dCQU5PLGdCQUFnQjtnQkFDSSxpQkFBaUI7O0lBNkI3Qyw2QkFBQztDQUFBLEFBM0JELENBSzRDLHNCQUFzQixHQXNCakU7U0F0Qlksc0JBQXNCOzs7SUFDL0IsNENBQWdCOztJQUNoQiwwQ0FBYzs7Ozs7SUFHViwyQ0FBcUM7Ozs7O0lBQ3JDLG1EQUE4QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuaW1wb3J0IHtUcmFuc2xhdGVTZXJ2aWNlfSBmcm9tICcuLi8uLi9wYWdlcy90cmFuc2xhdGUuc2VydmljZSc7XG5pbXBvcnQge1RyYW5zbGF0ZVNlcnZpY2UgYXMgVHJhbnNsYXRlUHJvdmlkZXJ9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2stbGFuZ3VhZ2UnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9ibG9jay1sYW5ndWFnZS5jb21wb25lbnQuaHRtbCcsXG59KVxuXG5leHBvcnQgY2xhc3MgQmxvY2tMYW5ndWFnZUNvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIGxhbmd1YWdlcyQ6IGFueTtcbiAgICBsYW5ndWFnZTogYW55O1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXG4gICAgICAgIHByb3RlY3RlZCB0cmFuc2xhdGVQcm92aWRlcjogVHJhbnNsYXRlUHJvdmlkZXIsXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMudHJhbnNsYXRlLmdldExhbmd1YWdlcygpLnN1YnNjcmliZShsYW5ndWFnZXMgPT4ge1xuICAgICAgICAgICAgdGhpcy5sYW5ndWFnZXMkID0gbGFuZ3VhZ2VzO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzZXRMYW5ndWFnZShsYW5ndWFnZUNvZGU6IHN0cmluZykge1xuICAgICAgICB0aGlzLnRyYW5zbGF0ZVByb3ZpZGVyLnVzZShsYW5ndWFnZUNvZGUpO1xuICAgICAgICB0aGlzLnRyYW5zbGF0ZS5zZXREZWZhdWx0TGFuZ3VhZ2UobGFuZ3VhZ2VDb2RlKTtcbiAgICB9XG5cbn1cbiJdfQ==