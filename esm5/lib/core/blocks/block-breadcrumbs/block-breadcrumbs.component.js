/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-breadcrumbs/block-breadcrumbs.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { PageService } from '../../pages/page.service';
import { AuthService } from '../../auth/auth.service';
var BlockBreadcrumbsComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockBreadcrumbsComponent, _super);
    function BlockBreadcrumbsComponent(authService, pages) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        _this.pages = pages;
        _this.date = Date.now();
        return _this;
    }
    /**
     * @return {?}
     */
    BlockBreadcrumbsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.pages.getBreadcrumbs(this.pages.getCurrentPage().id).subscribe((/**
         * @param {?} breadcrumbs
         * @return {?}
         */
        function (breadcrumbs) {
            _this.breadcrumbs = breadcrumbs || [];
        }));
    };
    BlockBreadcrumbsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-breadcrumbs',
                    template: "<ol class=\"breadcrumb page-breadcrumb\">\n    <li class=\"breadcrumb-item\"><a href=\"#\" coreStubClick>Home</a></li>\n    <li class=\"breadcrumb-item\" *ngFor=\"let breadcrumb of breadcrumbs.parents\">\n        <a [routerLink]=\"breadcrumb.url\">{{breadcrumb.name}}</a>\n    </li>\n    <li class=\"breadcrumb-item\">{{breadcrumbs.name}}</li>\n    <li class=\"position-absolute pos-top pos-right d-none d-sm-block\">\n        <span>{{ date | date }}</span>\n    </li>\n</ol>"
                }] }
    ];
    /** @nocollapse */
    BlockBreadcrumbsComponent.ctorParameters = function () { return [
        { type: AuthService },
        { type: PageService }
    ]; };
    return BlockBreadcrumbsComponent;
}(BlockAbstractComponent));
export { BlockBreadcrumbsComponent };
if (false) {
    /** @type {?} */
    BlockBreadcrumbsComponent.prototype.breadcrumbs;
    /** @type {?} */
    BlockBreadcrumbsComponent.prototype.date;
    /**
     * @type {?}
     * @protected
     */
    BlockBreadcrumbsComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    BlockBreadcrumbsComponent.prototype.pages;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYnJlYWRjcnVtYnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stYnJlYWRjcnVtYnMvYmxvY2stYnJlYWRjcnVtYnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRDtJQUsrQyxxREFBc0I7SUFJakUsbUNBQ2MsV0FBd0IsRUFDeEIsS0FBa0I7UUFGaEMsWUFJSSxrQkFDSSxXQUFXLENBQ2QsU0FDSjtRQU5hLGlCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLFdBQUssR0FBTCxLQUFLLENBQWE7UUFKaEMsVUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQzs7SUFTbEIsQ0FBQzs7OztJQUVELDRDQUFROzs7SUFBUjtRQUFBLGlCQUlDO1FBSEcsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxXQUFXO1lBQzNFLEtBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxJQUFJLEVBQUUsQ0FBQztRQUN6QyxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7O2dCQXRCSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLHdCQUF3QjtvQkFDbEMsdWVBQWlEO2lCQUNwRDs7OztnQkFMTyxXQUFXO2dCQURYLFdBQVc7O0lBMkJuQixnQ0FBQztDQUFBLEFBeEJELENBSytDLHNCQUFzQixHQW1CcEU7U0FuQlkseUJBQXlCOzs7SUFDbEMsZ0RBQWlCOztJQUNqQix5Q0FBa0I7Ozs7O0lBR2QsZ0RBQWtDOzs7OztJQUNsQywwQ0FBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tBYnN0cmFjdENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcbmltcG9ydCB7UGFnZVNlcnZpY2V9IGZyb20gJy4uLy4uL3BhZ2VzL3BhZ2Uuc2VydmljZSc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi8uLi9hdXRoL2F1dGguc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay1icmVhZGNydW1icycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLWJyZWFkY3J1bWJzLmNvbXBvbmVudC5odG1sJyxcbn0pXG5cbmV4cG9ydCBjbGFzcyBCbG9ja0JyZWFkY3J1bWJzQ29tcG9uZW50IGV4dGVuZHMgQmxvY2tBYnN0cmFjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgYnJlYWRjcnVtYnM6IGFueTtcbiAgICBkYXRlID0gRGF0ZS5ub3coKTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgICAgICBwcm90ZWN0ZWQgcGFnZXM6IFBhZ2VTZXJ2aWNlLFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMucGFnZXMuZ2V0QnJlYWRjcnVtYnModGhpcy5wYWdlcy5nZXRDdXJyZW50UGFnZSgpLmlkKS5zdWJzY3JpYmUoYnJlYWRjcnVtYnMgPT4ge1xuICAgICAgICAgICAgdGhpcy5icmVhZGNydW1icyA9IGJyZWFkY3J1bWJzIHx8IFtdO1xuICAgICAgICB9KTtcbiAgICB9XG5cbn1cbiJdfQ==