/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-numbered/block-numbered.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
var BlockNumberedComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockNumberedComponent, _super);
    function BlockNumberedComponent() {
        return _super.call(this) || this;
    }
    /**
     * @return {?}
     */
    BlockNumberedComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        _super.prototype.ngOnInit.call(this);
    };
    /**
     * @param {?} idx
     * @return {?}
     */
    BlockNumberedComponent.prototype.trackByFn = /**
     * @param {?} idx
     * @return {?}
     */
    function (idx) {
        return idx;
    };
    BlockNumberedComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-numbered',
                    template: "<mat-list>\n    <mat-list-item *ngFor=\"let item of block.content trackBy: trackByFn\" [innerHTML]=\"item.data.text\"></mat-list-item>\n</mat-list>"
                }] }
    ];
    /** @nocollapse */
    BlockNumberedComponent.ctorParameters = function () { return []; };
    return BlockNumberedComponent;
}(BlockAbstractComponent));
export { BlockNumberedComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stbnVtYmVyZWQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stbnVtYmVyZWQvYmxvY2stbnVtYmVyZWQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFFbkU7SUFLNEMsa0RBQXNCO0lBRTlEO2VBQ0ksaUJBQU87SUFDWCxDQUFDOzs7O0lBRUQseUNBQVE7OztJQUFSO1FBQ0ksaUJBQU0sUUFBUSxXQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCwwQ0FBUzs7OztJQUFULFVBQVUsR0FBVztRQUNqQixPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7O2dCQWpCSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0IsK0pBQThDO2lCQUNqRDs7OztJQWdCRCw2QkFBQztDQUFBLEFBbkJELENBSzRDLHNCQUFzQixHQWNqRTtTQWRZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2stbnVtYmVyZWQnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9ibG9jay1udW1iZXJlZC5jb21wb25lbnQuaHRtbCcsXG59KVxuXG5leHBvcnQgY2xhc3MgQmxvY2tOdW1iZXJlZENvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgfVxuXG4gICAgdHJhY2tCeUZuKGlkeDogbnVtYmVyKSB7XG4gICAgICAgIHJldHVybiBpZHg7XG4gICAgfVxuXG59XG4iXX0=