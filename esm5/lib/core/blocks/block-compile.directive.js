/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-compile.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Compiler, Component, Directive, Input, NgModule, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { AuthService } from '../auth/auth.service';
var BlockCompileDirective = /** @class */ (function () {
    function BlockCompileDirective(vcRef, compiler) {
        this.vcRef = vcRef;
        this.compiler = compiler;
    }
    /**
     * @return {?}
     */
    BlockCompileDirective.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.coreBlockCompile) {
            if (this.compRef) {
                this.updateProperties();
                return;
            }
            throw Error('Template not specified');
        }
        this.vcRef.clear();
        this.compRef = null;
        /** @type {?} */
        var component = this.createDynamicComponent(this.coreBlockCompile);
        /** @type {?} */
        var module = this.createDynamicModule(component);
        this.compiler.compileModuleAndAllComponentsAsync(module)
            .then((/**
         * @param {?} moduleWithFactories
         * @return {?}
         */
        function (moduleWithFactories) {
            /** @type {?} */
            var compFactory = moduleWithFactories.componentFactories.find((/**
             * @param {?} a
             * @return {?}
             */
            function (a) { return a.componentType === component; }));
            _this.compRef = _this.vcRef.createComponent(compFactory);
            _this.updateProperties();
        }));
    };
    /**
     * @return {?}
     */
    BlockCompileDirective.prototype.updateProperties = /**
     * @return {?}
     */
    function () {
        var e_1, _a;
        try {
            for (var _b = tslib_1.__values(Object.values(this.coreBlockCompileContext)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var prop = _c.value;
                this.compRef.instance[prop] = this.coreBlockCompileContext[prop];
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    /**
     * @private
     * @param {?} template
     * @return {?}
     */
    BlockCompileDirective.prototype.createDynamicComponent = /**
     * @private
     * @param {?} template
     * @return {?}
     */
    function (template) {
        var DynamicComponent = /** @class */ (function () {
            function DynamicComponent(authService) {
                this.authService = authService;
            }
            /**
             * @return {?}
             */
            DynamicComponent.prototype.ngOnInit = /**
             * @return {?}
             */
            function () {
                this.user = this.authService.getUser();
            };
            DynamicComponent.decorators = [
                { type: Component, args: [{
                            selector: 'core-dynamic-block',
                            template: template,
                        },] },
            ];
            /** @nocollapse */
            DynamicComponent.ctorParameters = function () { return [
                { type: AuthService }
            ]; };
            return DynamicComponent;
        }());
        if (false) {
            /** @type {?} */
            DynamicComponent.prototype.user;
            /**
             * @type {?}
             * @protected
             */
            DynamicComponent.prototype.authService;
        }
        return DynamicComponent;
    };
    /**
     * @private
     * @param {?} component
     * @return {?}
     */
    BlockCompileDirective.prototype.createDynamicModule = /**
     * @private
     * @param {?} component
     * @return {?}
     */
    function (component) {
        var DynamicModule = /** @class */ (function () {
            function DynamicModule() {
            }
            DynamicModule.decorators = [
                { type: NgModule, args: [{
                            imports: [CommonModule, TranslateModule],
                            declarations: [component]
                        },] },
            ];
            return DynamicModule;
        }());
        return DynamicModule;
    };
    BlockCompileDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[coreBlockCompile]'
                },] }
    ];
    /** @nocollapse */
    BlockCompileDirective.ctorParameters = function () { return [
        { type: ViewContainerRef },
        { type: Compiler }
    ]; };
    BlockCompileDirective.propDecorators = {
        coreBlockCompile: [{ type: Input }],
        coreBlockCompileContext: [{ type: Input }]
    };
    return BlockCompileDirective;
}());
export { BlockCompileDirective };
if (false) {
    /** @type {?} */
    BlockCompileDirective.prototype.coreBlockCompile;
    /** @type {?} */
    BlockCompileDirective.prototype.coreBlockCompileContext;
    /** @type {?} */
    BlockCompileDirective.prototype.compRef;
    /**
     * @type {?}
     * @private
     */
    BlockCompileDirective.prototype.vcRef;
    /**
     * @type {?}
     * @private
     */
    BlockCompileDirective.prototype.compiler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stY29tcGlsZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2Nrcy9ibG9jay1jb21waWxlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxPQUFPLEVBQ0gsUUFBUSxFQUNSLFNBQVMsRUFFVCxTQUFTLEVBQ1QsS0FBSyxFQUVMLFFBQVEsRUFJUixnQkFBZ0IsRUFDbkIsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUNwRCxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sc0JBQXNCLENBQUM7QUFFakQ7SUFTSSwrQkFBb0IsS0FBdUIsRUFDdkIsUUFBa0I7UUFEbEIsVUFBSyxHQUFMLEtBQUssQ0FBa0I7UUFDdkIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtJQUN0QyxDQUFDOzs7O0lBRUQsMkNBQVc7OztJQUFYO1FBQUEsaUJBb0JDO1FBbkJHLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDeEIsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNkLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUN4QixPQUFPO2FBQ1Y7WUFDRCxNQUFNLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1NBQ3pDO1FBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7WUFFZCxTQUFTLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQzs7WUFDOUQsTUFBTSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUM7UUFDbEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxNQUFNLENBQUM7YUFDbkQsSUFBSTs7OztRQUFDLFVBQUMsbUJBQXNEOztnQkFDbkQsV0FBVyxHQUFHLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxhQUFhLEtBQUssU0FBUyxFQUE3QixDQUE2QixFQUFDO1lBQ25HLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdkQsS0FBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDNUIsQ0FBQyxFQUFDLENBQUM7SUFDWCxDQUFDOzs7O0lBRUQsZ0RBQWdCOzs7SUFBaEI7OztZQUNJLEtBQW1CLElBQUEsS0FBQSxpQkFBQSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBLGdCQUFBLDRCQUFFO2dCQUEzRCxJQUFNLElBQUksV0FBQTtnQkFDWCxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDcEU7Ozs7Ozs7OztJQUNMLENBQUM7Ozs7OztJQUVPLHNEQUFzQjs7Ozs7SUFBOUIsVUFBK0IsUUFBZ0I7UUFDM0M7WUFPSSwwQkFDYyxXQUF3QjtnQkFBeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7WUFFdEMsQ0FBQzs7OztZQUVELG1DQUFROzs7WUFBUjtnQkFDSSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDM0MsQ0FBQzs7d0JBZEosU0FBUyxTQUFDOzRCQUNQLFFBQVEsRUFBRSxvQkFBb0I7NEJBQzlCLFFBQVEsVUFBQTt5QkFDWDs7Ozt3QkEvQ0QsV0FBVzs7WUEyRFgsdUJBQUM7U0FBQSxBQWZELElBZUM7OztZQVZHLGdDQUFVOzs7OztZQUdOLHVDQUFrQzs7UUFTMUMsT0FBTyxnQkFBZ0IsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFTyxtREFBbUI7Ozs7O0lBQTNCLFVBQTRCLFNBQW9CO1FBQzVDO1lBQUE7WUFLQSxDQUFDOzt3QkFMQSxRQUFRLFNBQUM7NEJBQ04sT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLGVBQWUsQ0FBQzs0QkFDeEMsWUFBWSxFQUFFLENBQUMsU0FBUyxDQUFDO3lCQUM1Qjs7WUFFRCxvQkFBQztTQUFBLEFBTEQsSUFLQztRQUVELE9BQU8sYUFBYSxDQUFDO0lBQ3pCLENBQUM7O2dCQXZFSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLG9CQUFvQjtpQkFDakM7Ozs7Z0JBUkcsZ0JBQWdCO2dCQVZoQixRQUFROzs7bUNBb0JQLEtBQUs7MENBQ0wsS0FBSzs7SUFtRVYsNEJBQUM7Q0FBQSxBQXhFRCxJQXdFQztTQXJFWSxxQkFBcUI7OztJQUM5QixpREFBa0M7O0lBQ2xDLHdEQUF3Qzs7SUFFeEMsd0NBQTJCOzs7OztJQUVmLHNDQUErQjs7Ozs7SUFDL0IseUNBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgICBDb21waWxlcixcbiAgICBDb21wb25lbnQsXG4gICAgQ29tcG9uZW50UmVmLFxuICAgIERpcmVjdGl2ZSxcbiAgICBJbnB1dCxcbiAgICBNb2R1bGVXaXRoQ29tcG9uZW50RmFjdG9yaWVzLFxuICAgIE5nTW9kdWxlLFxuICAgIE9uQ2hhbmdlcyxcbiAgICBPbkluaXQsXG4gICAgVHlwZSxcbiAgICBWaWV3Q29udGFpbmVyUmVmXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge1RyYW5zbGF0ZU1vZHVsZX0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi9hdXRoL2F1dGguc2VydmljZSc7XG5cbkBEaXJlY3RpdmUoe1xuICAgIHNlbGVjdG9yOiAnW2NvcmVCbG9ja0NvbXBpbGVdJ1xufSlcbmV4cG9ydCBjbGFzcyBCbG9ja0NvbXBpbGVEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xuICAgIEBJbnB1dCgpIGNvcmVCbG9ja0NvbXBpbGU6IHN0cmluZztcbiAgICBASW5wdXQoKSBjb3JlQmxvY2tDb21waWxlQ29udGV4dDogYW55W107XG5cbiAgICBjb21wUmVmOiBDb21wb25lbnRSZWY8YW55PjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgdmNSZWY6IFZpZXdDb250YWluZXJSZWYsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBjb21waWxlcjogQ29tcGlsZXIpIHtcbiAgICB9XG5cbiAgICBuZ09uQ2hhbmdlcygpIHtcbiAgICAgICAgaWYgKCF0aGlzLmNvcmVCbG9ja0NvbXBpbGUpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmNvbXBSZWYpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVByb3BlcnRpZXMoKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aHJvdyBFcnJvcignVGVtcGxhdGUgbm90IHNwZWNpZmllZCcpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy52Y1JlZi5jbGVhcigpO1xuICAgICAgICB0aGlzLmNvbXBSZWYgPSBudWxsO1xuXG4gICAgICAgIGNvbnN0IGNvbXBvbmVudCA9IHRoaXMuY3JlYXRlRHluYW1pY0NvbXBvbmVudCh0aGlzLmNvcmVCbG9ja0NvbXBpbGUpO1xuICAgICAgICBjb25zdCBtb2R1bGUgPSB0aGlzLmNyZWF0ZUR5bmFtaWNNb2R1bGUoY29tcG9uZW50KTtcbiAgICAgICAgdGhpcy5jb21waWxlci5jb21waWxlTW9kdWxlQW5kQWxsQ29tcG9uZW50c0FzeW5jKG1vZHVsZSlcbiAgICAgICAgICAgIC50aGVuKChtb2R1bGVXaXRoRmFjdG9yaWVzOiBNb2R1bGVXaXRoQ29tcG9uZW50RmFjdG9yaWVzPGFueT4pID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBjb21wRmFjdG9yeSA9IG1vZHVsZVdpdGhGYWN0b3JpZXMuY29tcG9uZW50RmFjdG9yaWVzLmZpbmQoYSA9PiBhLmNvbXBvbmVudFR5cGUgPT09IGNvbXBvbmVudCk7XG4gICAgICAgICAgICAgICAgdGhpcy5jb21wUmVmID0gdGhpcy52Y1JlZi5jcmVhdGVDb21wb25lbnQoY29tcEZhY3RvcnkpO1xuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUHJvcGVydGllcygpO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdXBkYXRlUHJvcGVydGllcygpIHtcbiAgICAgICAgZm9yIChjb25zdCBwcm9wIG9mIE9iamVjdC52YWx1ZXModGhpcy5jb3JlQmxvY2tDb21waWxlQ29udGV4dCkpIHtcbiAgICAgICAgICAgIHRoaXMuY29tcFJlZi5pbnN0YW5jZVtwcm9wXSA9IHRoaXMuY29yZUJsb2NrQ29tcGlsZUNvbnRleHRbcHJvcF07XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGNyZWF0ZUR5bmFtaWNDb21wb25lbnQodGVtcGxhdGU6IHN0cmluZykge1xuICAgICAgICBAQ29tcG9uZW50KHtcbiAgICAgICAgICAgIHNlbGVjdG9yOiAnY29yZS1keW5hbWljLWJsb2NrJyxcbiAgICAgICAgICAgIHRlbXBsYXRlLFxuICAgICAgICB9KVxuICAgICAgICBjbGFzcyBEeW5hbWljQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICAgICAgICAgIHVzZXI6IGFueTtcblxuICAgICAgICAgICAgY29uc3RydWN0b3IoXG4gICAgICAgICAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXIgPSB0aGlzLmF1dGhTZXJ2aWNlLmdldFVzZXIoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBEeW5hbWljQ29tcG9uZW50O1xuICAgIH1cblxuICAgIHByaXZhdGUgY3JlYXRlRHluYW1pY01vZHVsZShjb21wb25lbnQ6IFR5cGU8YW55Pikge1xuICAgICAgICBATmdNb2R1bGUoe1xuICAgICAgICAgICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgVHJhbnNsYXRlTW9kdWxlXSxcbiAgICAgICAgICAgIGRlY2xhcmF0aW9uczogW2NvbXBvbmVudF1cbiAgICAgICAgfSlcbiAgICAgICAgY2xhc3MgRHluYW1pY01vZHVsZSB7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gRHluYW1pY01vZHVsZTtcbiAgICB9XG59Il19