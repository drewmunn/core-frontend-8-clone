/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-tabs/block-tabs.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
var BlockTabsComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockTabsComponent, _super);
    function BlockTabsComponent(authService) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        return _this;
    }
    /**
     * @return {?}
     */
    BlockTabsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} idx
     * @return {?}
     */
    BlockTabsComponent.prototype.trackByFn = /**
     * @param {?} idx
     * @return {?}
     */
    function (idx) {
        return idx;
    };
    BlockTabsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-tabs',
                    template: "<mat-tab-group>\n    <mat-tab *ngFor=\"let item of block.content trackBy: trackByFn\">\n        <ng-template mat-tab-label>\n            <ng-container>{{item.data.header}}</ng-container>\n        </ng-template>\n        <div [innerHTML]=\"item.data.content\"></div>\n    </mat-tab>\n</mat-tab-group>\n"
                }] }
    ];
    /** @nocollapse */
    BlockTabsComponent.ctorParameters = function () { return [
        { type: AuthService }
    ]; };
    return BlockTabsComponent;
}(BlockAbstractComponent));
export { BlockTabsComponent };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockTabsComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stdGFicy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2Nrcy9ibG9jay10YWJzL2Jsb2NrLXRhYnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBRXBEO0lBSXdDLDhDQUFzQjtJQUUxRCw0QkFDYyxXQUF3QjtRQUR0QyxZQUdJLGtCQUNJLFdBQVcsQ0FDZCxTQUNKO1FBTGEsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBS3RDLENBQUM7Ozs7SUFFRCxxQ0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7OztJQUVELHNDQUFTOzs7O0lBQVQsVUFBVSxHQUFXO1FBQ2pCLE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQzs7Z0JBbkJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQix5VEFBMEM7aUJBQzdDOzs7O2dCQUxPLFdBQVc7O0lBdUJuQix5QkFBQztDQUFBLEFBckJELENBSXdDLHNCQUFzQixHQWlCN0Q7U0FqQlksa0JBQWtCOzs7Ozs7SUFHdkIseUNBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jsb2NrQWJzdHJhY3RDb21wb25lbnR9IGZyb20gJy4uL2Jsb2NrLWFic3RyYWN0LmNvbXBvbmVudCc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi8uLi9hdXRoL2F1dGguc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay10YWJzJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vYmxvY2stdGFicy5jb21wb25lbnQuaHRtbCcsXG59KVxuZXhwb3J0IGNsYXNzIEJsb2NrVGFic0NvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UsXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKFxuICAgICAgICAgICAgYXV0aFNlcnZpY2VcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICB9XG5cbiAgICB0cmFja0J5Rm4oaWR4OiBudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIGlkeDtcbiAgICB9XG5cbn1cbiJdfQ==