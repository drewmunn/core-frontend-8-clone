/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-paragraph/block-paragraph.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
var BlockParagraphComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockParagraphComponent, _super);
    function BlockParagraphComponent(authService) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        return _this;
    }
    /**
     * @return {?}
     */
    BlockParagraphComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        _super.prototype.ngOnInit.call(this);
        this.text = this.block.data.text;
    };
    BlockParagraphComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-paragraph',
                    template: "\n        <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n    "
                }] }
    ];
    /** @nocollapse */
    BlockParagraphComponent.ctorParameters = function () { return [
        { type: AuthService }
    ]; };
    BlockParagraphComponent.propDecorators = {
        text: [{ type: Input }]
    };
    return BlockParagraphComponent;
}(BlockAbstractComponent));
export { BlockParagraphComponent };
if (false) {
    /** @type {?} */
    BlockParagraphComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockParagraphComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stcGFyYWdyYXBoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYmxvY2tzL2Jsb2NrLXBhcmFncmFwaC9ibG9jay1wYXJhZ3JhcGguY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBQ25FLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRDtJQU82QyxtREFBc0I7SUFJL0QsaUNBQ2MsV0FBd0I7UUFEdEMsWUFHSSxrQkFDSSxXQUFXLENBQ2QsU0FDSjtRQUxhLGlCQUFXLEdBQVgsV0FBVyxDQUFhOztJQUt0QyxDQUFDOzs7O0lBRUQsMENBQVE7OztJQUFSO1FBQ0ksaUJBQU0sUUFBUSxXQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckMsQ0FBQzs7Z0JBdEJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsc0JBQXNCO29CQUNoQyxRQUFRLEVBQUUseUZBRVQ7aUJBQ0o7Ozs7Z0JBUE8sV0FBVzs7O3VCQVdkLEtBQUs7O0lBZVYsOEJBQUM7Q0FBQSxBQXhCRCxDQU82QyxzQkFBc0IsR0FpQmxFO1NBakJZLHVCQUF1Qjs7O0lBRWhDLHVDQUFzQjs7Ozs7SUFHbEIsOENBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vLi4vYXV0aC9hdXRoLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2stcGFyYWdyYXBoJyxcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8bmctY29udGFpbmVyICpjb3JlQmxvY2tDb21waWxlPVwidGV4dDsgY29udGV4dDogdGhpc1wiPjwvbmctY29udGFpbmVyPlxuICAgIGBcbn0pXG5cbmV4cG9ydCBjbGFzcyBCbG9ja1BhcmFncmFwaENvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgQElucHV0KCkgdGV4dDogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UsXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKFxuICAgICAgICAgICAgYXV0aFNlcnZpY2VcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgc3VwZXIubmdPbkluaXQoKTtcbiAgICAgICAgdGhpcy50ZXh0ID0gdGhpcy5ibG9jay5kYXRhLnRleHQ7XG4gICAgfVxuXG59XG4iXX0=