/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-video/block-video.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
var BlockVideoComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockVideoComponent, _super);
    function BlockVideoComponent() {
        return _super.call(this) || this;
    }
    /**
     * @return {?}
     */
    BlockVideoComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    BlockVideoComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-video',
                    template: "VIDEO"
                }] }
    ];
    /** @nocollapse */
    BlockVideoComponent.ctorParameters = function () { return []; };
    return BlockVideoComponent;
}(BlockAbstractComponent));
export { BlockVideoComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stdmlkZW8uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stdmlkZW8vYmxvY2stdmlkZW8uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFFbkU7SUFLeUMsK0NBQXNCO0lBRTNEO2VBQ0ksaUJBQU87SUFDWCxDQUFDOzs7O0lBRUQsc0NBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7Z0JBWkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLGlCQUEyQztpQkFDOUM7Ozs7SUFXRCwwQkFBQztDQUFBLEFBZEQsQ0FLeUMsc0JBQXNCLEdBUzlEO1NBVFksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jsb2NrQWJzdHJhY3RDb21wb25lbnR9IGZyb20gJy4uL2Jsb2NrLWFic3RyYWN0LmNvbXBvbmVudCc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay12aWRlbycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLXZpZGVvLmNvbXBvbmVudC5odG1sJyxcbn0pXG5cbmV4cG9ydCBjbGFzcyBCbG9ja1ZpZGVvQ29tcG9uZW50IGV4dGVuZHMgQmxvY2tBYnN0cmFjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICB9XG5cbn1cbiJdfQ==