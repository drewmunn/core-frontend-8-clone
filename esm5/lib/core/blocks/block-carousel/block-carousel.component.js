/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-carousel/block-carousel.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
var BlockCarouselComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockCarouselComponent, _super);
    function BlockCarouselComponent(authService) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        return _this;
    }
    /**
     * @return {?}
     */
    BlockCarouselComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    BlockCarouselComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-carousel',
                    template: "CAROUSEL"
                }] }
    ];
    /** @nocollapse */
    BlockCarouselComponent.ctorParameters = function () { return [
        { type: AuthService }
    ]; };
    return BlockCarouselComponent;
}(BlockAbstractComponent));
export { BlockCarouselComponent };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockCarouselComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stY2Fyb3VzZWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stY2Fyb3VzZWwvYmxvY2stY2Fyb3VzZWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBRXBEO0lBSzRDLGtEQUFzQjtJQUU5RCxnQ0FDYyxXQUF3QjtRQUR0QyxZQUdJLGtCQUNJLFdBQVcsQ0FDZCxTQUNKO1FBTGEsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBS3RDLENBQUM7Ozs7SUFFRCx5Q0FBUTs7O0lBQVI7SUFDQSxDQUFDOztnQkFoQkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxxQkFBcUI7b0JBQy9CLG9CQUE4QztpQkFDakQ7Ozs7Z0JBTE8sV0FBVzs7SUFvQm5CLDZCQUFDO0NBQUEsQUFsQkQsQ0FLNEMsc0JBQXNCLEdBYWpFO1NBYlksc0JBQXNCOzs7Ozs7SUFHM0IsNkNBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jsb2NrQWJzdHJhY3RDb21wb25lbnR9IGZyb20gJy4uL2Jsb2NrLWFic3RyYWN0LmNvbXBvbmVudCc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi8uLi9hdXRoL2F1dGguc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay1jYXJvdXNlbCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLWNhcm91c2VsLmNvbXBvbmVudC5odG1sJyxcbn0pXG5cbmV4cG9ydCBjbGFzcyBCbG9ja0Nhcm91c2VsQ29tcG9uZW50IGV4dGVuZHMgQmxvY2tBYnN0cmFjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBhdXRoU2VydmljZVxuICAgICAgICApO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgIH1cblxufVxuIl19