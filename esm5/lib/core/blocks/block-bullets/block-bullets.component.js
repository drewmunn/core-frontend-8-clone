/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-bullets/block-bullets.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
var BlockBulletsComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockBulletsComponent, _super);
    function BlockBulletsComponent(authService) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        return _this;
    }
    /**
     * @return {?}
     */
    BlockBulletsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        _super.prototype.ngOnInit.call(this);
    };
    /**
     * @param {?} idx
     * @return {?}
     */
    BlockBulletsComponent.prototype.trackByFn = /**
     * @param {?} idx
     * @return {?}
     */
    function (idx) {
        return idx;
    };
    BlockBulletsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-bullets',
                    template: "<mat-list>\n    <mat-list-item *ngFor=\"let item of block.content trackBy: trackByFn\" [innerHTML]=\"item.data.text\"></mat-list-item>\n</mat-list>"
                }] }
    ];
    /** @nocollapse */
    BlockBulletsComponent.ctorParameters = function () { return [
        { type: AuthService }
    ]; };
    return BlockBulletsComponent;
}(BlockAbstractComponent));
export { BlockBulletsComponent };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockBulletsComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYnVsbGV0cy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2Nrcy9ibG9jay1idWxsZXRzL2Jsb2NrLWJ1bGxldHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBVyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDMUQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBRXBEO0lBSzJDLGlEQUFzQjtJQUU3RCwrQkFDYyxXQUF3QjtRQUR0QyxZQUdJLGtCQUNJLFdBQVcsQ0FDZCxTQUNKO1FBTGEsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBS3RDLENBQUM7Ozs7SUFFRCx3Q0FBUTs7O0lBQVI7UUFDSSxpQkFBTSxRQUFRLFdBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELHlDQUFTOzs7O0lBQVQsVUFBVSxHQUFXO1FBQ2pCLE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQzs7Z0JBckJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QiwrSkFBNkM7aUJBQ2hEOzs7O2dCQUxPLFdBQVc7O0lBeUJuQiw0QkFBQztDQUFBLEFBdkJELENBSzJDLHNCQUFzQixHQWtCaEU7U0FsQlkscUJBQXFCOzs7Ozs7SUFHMUIsNENBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21waWxlciwgQ29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vLi4vYXV0aC9hdXRoLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2stYnVsbGV0cycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLWJ1bGxldHMuY29tcG9uZW50Lmh0bWwnLFxufSlcblxuZXhwb3J0IGNsYXNzIEJsb2NrQnVsbGV0c0NvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UsXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKFxuICAgICAgICAgICAgYXV0aFNlcnZpY2VcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgc3VwZXIubmdPbkluaXQoKTtcbiAgICB9XG5cbiAgICB0cmFja0J5Rm4oaWR4OiBudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIGlkeDtcbiAgICB9XG5cbn1cbiJdfQ==