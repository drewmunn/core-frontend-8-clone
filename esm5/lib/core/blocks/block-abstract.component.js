/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-abstract.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Compiler, Component, ComponentFactoryResolver, Input, NgModule, ViewContainerRef } from '@angular/core';
import { PageService } from '../pages/page.service';
import { TranslateService } from '../pages/translate.service';
import { Block } from '../block/block-block';
import { AuthService } from '../auth/auth.service';
import { TranslateModule } from '@ngx-translate/core';
var BlockAbstractComponent = /** @class */ (function () {
    function BlockAbstractComponent(authService, pages, resolver, viewContainerRef, translate, compiler) {
        this.authService = authService;
        this.pages = pages;
        this.resolver = resolver;
        this.viewContainerRef = viewContainerRef;
        this.translate = translate;
        this.compiler = compiler;
    }
    /**
     * @return {?}
     */
    BlockAbstractComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.authService) {
            this.user = this.authService.getUser();
        }
    };
    /**
     * @protected
     * @param {?} contents
     * @return {?}
     */
    BlockAbstractComponent.prototype.createDynamicBlock = /**
     * @protected
     * @param {?} contents
     * @return {?}
     */
    function (contents) {
        this.dynamicComponent = this.createNewComponent(contents);
        this.dynamicModule = this.compiler.compileModuleSync(this.createComponentModule(this.dynamicComponent));
    };
    /**
     * @protected
     * @param {?} componentType
     * @return {?}
     */
    BlockAbstractComponent.prototype.createComponentModule = /**
     * @protected
     * @param {?} componentType
     * @return {?}
     */
    function (componentType) {
        var RuntimeComponentModule = /** @class */ (function () {
            function RuntimeComponentModule() {
            }
            RuntimeComponentModule.decorators = [
                { type: NgModule, args: [{
                            imports: [TranslateModule],
                            declarations: [componentType],
                            entryComponents: [componentType]
                        },] },
            ];
            return RuntimeComponentModule;
        }());
        return RuntimeComponentModule;
    };
    /**
     * @protected
     * @param {?} contents
     * @return {?}
     */
    BlockAbstractComponent.prototype.createNewComponent = /**
     * @protected
     * @param {?} contents
     * @return {?}
     */
    function (contents) {
        var DynamicComponent = /** @class */ (function () {
            function DynamicComponent(authService) {
                this.authService = authService;
            }
            /**
             * @return {?}
             */
            DynamicComponent.prototype.ngOnInit = /**
             * @return {?}
             */
            function () {
                this.contents = contents;
                this.user = this.authService.getUser();
            };
            DynamicComponent.decorators = [
                { type: Component, args: [{
                            selector: 'core-block-dynamic',
                            template: "" + contents,
                        },] },
            ];
            /** @nocollapse */
            DynamicComponent.ctorParameters = function () { return [
                { type: AuthService }
            ]; };
            return DynamicComponent;
        }());
        if (false) {
            /** @type {?} */
            DynamicComponent.prototype.contents;
            /** @type {?} */
            DynamicComponent.prototype.user;
            /**
             * @type {?}
             * @protected
             */
            DynamicComponent.prototype.authService;
        }
        return DynamicComponent;
    };
    /**
     * @param {?} idx
     * @param {?=} item
     * @return {?}
     */
    BlockAbstractComponent.prototype.trackByFn = /**
     * @param {?} idx
     * @param {?=} item
     * @return {?}
     */
    function (idx, item) {
        return idx;
    };
    BlockAbstractComponent.decorators = [
        { type: Component, args: [{
                    template: "<core-block class=\"{{block.data.class}} {{block.data.visibility}}\"\n            *ngFor=\"let subBlock of block.content trackBy: trackByFn\"\n            [block]=\"subBlock\"\n            [ngClass]=\"{ 'stickyDiv': block.data.specialType == 'sticky',\n            'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n             'col-sm-12': block.data.columnsSmall == 'full',\n             'col-md-12': block.data.columnsNormal == 'full'}\"\n            [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n>\n</core-block>",
                    changeDetection: ChangeDetectionStrategy.Default
                }] }
    ];
    /** @nocollapse */
    BlockAbstractComponent.ctorParameters = function () { return [
        { type: AuthService },
        { type: PageService },
        { type: ComponentFactoryResolver },
        { type: ViewContainerRef },
        { type: TranslateService },
        { type: Compiler }
    ]; };
    BlockAbstractComponent.propDecorators = {
        block: [{ type: Input }]
    };
    return BlockAbstractComponent;
}());
export { BlockAbstractComponent };
if (false) {
    /** @type {?} */
    BlockAbstractComponent.prototype.block;
    /** @type {?} */
    BlockAbstractComponent.prototype.user;
    /** @type {?} */
    BlockAbstractComponent.prototype.dynamicComponent;
    /** @type {?} */
    BlockAbstractComponent.prototype.dynamicModule;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.pages;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.resolver;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.translate;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.compiler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYWJzdHJhY3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stYWJzdHJhY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUNILHVCQUF1QixFQUN2QixRQUFRLEVBQ1IsU0FBUyxFQUNULHdCQUF3QixFQUN4QixLQUFLLEVBQ0wsUUFBUSxFQUdSLGdCQUFnQixFQUNuQixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDbEQsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFDLEtBQUssRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQzNDLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFFcEQ7SUFhSSxnQ0FDYyxXQUF5QixFQUN6QixLQUFtQixFQUNuQixRQUFtQyxFQUNuQyxnQkFBbUMsRUFDbkMsU0FBNEIsRUFDNUIsUUFBbUI7UUFMbkIsZ0JBQVcsR0FBWCxXQUFXLENBQWM7UUFDekIsVUFBSyxHQUFMLEtBQUssQ0FBYztRQUNuQixhQUFRLEdBQVIsUUFBUSxDQUEyQjtRQUNuQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQW1CO1FBQ25DLGNBQVMsR0FBVCxTQUFTLENBQW1CO1FBQzVCLGFBQVEsR0FBUixRQUFRLENBQVc7SUFFakMsQ0FBQzs7OztJQUVELHlDQUFROzs7SUFBUjtRQUNJLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNsQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDMUM7SUFDTCxDQUFDOzs7Ozs7SUFFUyxtREFBa0I7Ozs7O0lBQTVCLFVBQTZCLFFBQWE7UUFDdEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7SUFDNUcsQ0FBQzs7Ozs7O0lBRVMsc0RBQXFCOzs7OztJQUEvQixVQUFnQyxhQUFrQjtRQUM5QztZQUFBO1lBTUEsQ0FBQzs7d0JBTkEsUUFBUSxTQUFDOzRCQUNOLE9BQU8sRUFBRSxDQUFDLGVBQWUsQ0FBQzs0QkFDMUIsWUFBWSxFQUFFLENBQUMsYUFBYSxDQUFDOzRCQUM3QixlQUFlLEVBQUUsQ0FBQyxhQUFhLENBQUM7eUJBQ25DOztZQUVELDZCQUFDO1NBQUEsQUFORCxJQU1DO1FBRUQsT0FBTyxzQkFBc0IsQ0FBQztJQUNsQyxDQUFDOzs7Ozs7SUFFUyxtREFBa0I7Ozs7O0lBQTVCLFVBQTZCLFFBQWdCO1FBQ3pDO1lBUUksMEJBQ2MsV0FBd0I7Z0JBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1lBRXRDLENBQUM7Ozs7WUFFRCxtQ0FBUTs7O1lBQVI7Z0JBQ0ksSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUMzQyxDQUFDOzt3QkFoQkosU0FBUyxTQUFDOzRCQUNQLFFBQVEsRUFBRSxvQkFBb0I7NEJBQzlCLFFBQVEsRUFBRSxLQUFHLFFBQVU7eUJBQzFCOzs7O3dCQXJERCxXQUFXOztZQW1FWCx1QkFBQztTQUFBLEFBakJELElBaUJDOzs7WUFaRyxvQ0FBYzs7WUFDZCxnQ0FBVTs7Ozs7WUFHTix1Q0FBa0M7O1FBVTFDLE9BQU8sZ0JBQWdCLENBQUM7SUFDNUIsQ0FBQzs7Ozs7O0lBRUQsMENBQVM7Ozs7O0lBQVQsVUFBVSxHQUFXLEVBQUUsSUFBVTtRQUM3QixPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7O2dCQXZFSixTQUFTLFNBQUM7b0JBQ1Asa2xCQUEyRDtvQkFDM0QsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE9BQU87aUJBQ25EOzs7O2dCQU5PLFdBQVc7Z0JBSFgsV0FBVztnQkFQZix3QkFBd0I7Z0JBS3hCLGdCQUFnQjtnQkFHWixnQkFBZ0I7Z0JBVnBCLFFBQVE7Ozt3QkFzQlAsS0FBSzs7SUFrRVYsNkJBQUM7Q0FBQSxBQXpFRCxJQXlFQztTQXBFWSxzQkFBc0I7OztJQUUvQix1Q0FBc0I7O0lBQ3RCLHNDQUFvQjs7SUFFcEIsa0RBQXNCOztJQUN0QiwrQ0FBb0M7Ozs7O0lBR2hDLDZDQUFtQzs7Ozs7SUFDbkMsdUNBQTZCOzs7OztJQUM3QiwwQ0FBNkM7Ozs7O0lBQzdDLGtEQUE2Qzs7Ozs7SUFDN0MsMkNBQXNDOzs7OztJQUN0QywwQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICAgIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxuICAgIENvbXBpbGVyLFxuICAgIENvbXBvbmVudCxcbiAgICBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXG4gICAgSW5wdXQsXG4gICAgTmdNb2R1bGUsXG4gICAgTmdNb2R1bGVGYWN0b3J5LFxuICAgIE9uSW5pdCxcbiAgICBWaWV3Q29udGFpbmVyUmVmXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtQYWdlU2VydmljZX0gZnJvbSAnLi4vcGFnZXMvcGFnZS5zZXJ2aWNlJztcbmltcG9ydCB7VHJhbnNsYXRlU2VydmljZX0gZnJvbSAnLi4vcGFnZXMvdHJhbnNsYXRlLnNlcnZpY2UnO1xuaW1wb3J0IHtCbG9ja30gZnJvbSAnLi4vYmxvY2svYmxvY2stYmxvY2snO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vYXV0aC9hdXRoLnNlcnZpY2UnO1xuaW1wb3J0IHtUcmFuc2xhdGVNb2R1bGV9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICB0ZW1wbGF0ZVVybDogJy4vYmxvY2std3JhcHBlci9ibG9jay13cmFwcGVyLmNvbXBvbmVudC5odG1sJyxcbiAgICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5LkRlZmF1bHRcbn0pXG5cbmV4cG9ydCBjbGFzcyBCbG9ja0Fic3RyYWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIEBJbnB1dCgpIGJsb2NrOiBCbG9jaztcbiAgICB1c2VyOiB7IGRhdGE6IGFueSB9O1xuXG4gICAgZHluYW1pY0NvbXBvbmVudDogYW55O1xuICAgIGR5bmFtaWNNb2R1bGU6IE5nTW9kdWxlRmFjdG9yeTxhbnk+O1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCBhdXRoU2VydmljZT86IEF1dGhTZXJ2aWNlLFxuICAgICAgICBwcm90ZWN0ZWQgcGFnZXM/OiBQYWdlU2VydmljZSxcbiAgICAgICAgcHJvdGVjdGVkIHJlc29sdmVyPzogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxuICAgICAgICBwcm90ZWN0ZWQgdmlld0NvbnRhaW5lclJlZj86IFZpZXdDb250YWluZXJSZWYsXG4gICAgICAgIHByb3RlY3RlZCB0cmFuc2xhdGU/OiBUcmFuc2xhdGVTZXJ2aWNlLFxuICAgICAgICBwcm90ZWN0ZWQgY29tcGlsZXI/OiBDb21waWxlcixcbiAgICApIHtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgaWYgKHRoaXMuYXV0aFNlcnZpY2UpIHtcbiAgICAgICAgICAgIHRoaXMudXNlciA9IHRoaXMuYXV0aFNlcnZpY2UuZ2V0VXNlcigpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGNyZWF0ZUR5bmFtaWNCbG9jayhjb250ZW50czogYW55KSB7XG4gICAgICAgIHRoaXMuZHluYW1pY0NvbXBvbmVudCA9IHRoaXMuY3JlYXRlTmV3Q29tcG9uZW50KGNvbnRlbnRzKTtcbiAgICAgICAgdGhpcy5keW5hbWljTW9kdWxlID0gdGhpcy5jb21waWxlci5jb21waWxlTW9kdWxlU3luYyh0aGlzLmNyZWF0ZUNvbXBvbmVudE1vZHVsZSh0aGlzLmR5bmFtaWNDb21wb25lbnQpKTtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgY3JlYXRlQ29tcG9uZW50TW9kdWxlKGNvbXBvbmVudFR5cGU6IGFueSk6IGFueSB7XG4gICAgICAgIEBOZ01vZHVsZSh7XG4gICAgICAgICAgICBpbXBvcnRzOiBbVHJhbnNsYXRlTW9kdWxlXSxcbiAgICAgICAgICAgIGRlY2xhcmF0aW9uczogW2NvbXBvbmVudFR5cGVdLFxuICAgICAgICAgICAgZW50cnlDb21wb25lbnRzOiBbY29tcG9uZW50VHlwZV1cbiAgICAgICAgfSlcbiAgICAgICAgY2xhc3MgUnVudGltZUNvbXBvbmVudE1vZHVsZSB7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gUnVudGltZUNvbXBvbmVudE1vZHVsZTtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgY3JlYXRlTmV3Q29tcG9uZW50KGNvbnRlbnRzOiBzdHJpbmcpOiBhbnkge1xuICAgICAgICBAQ29tcG9uZW50KHtcbiAgICAgICAgICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay1keW5hbWljJyxcbiAgICAgICAgICAgIHRlbXBsYXRlOiBgJHtjb250ZW50c31gLFxuICAgICAgICB9KVxuICAgICAgICBjbGFzcyBEeW5hbWljQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICAgICAgICAgIGNvbnRlbnRzOiBhbnk7XG4gICAgICAgICAgICB1c2VyOiBhbnk7XG5cbiAgICAgICAgICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICAgICAgICAgIHByb3RlY3RlZCBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UsXG4gICAgICAgICAgICApIHtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbmdPbkluaXQoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb250ZW50cyA9IGNvbnRlbnRzO1xuICAgICAgICAgICAgICAgIHRoaXMudXNlciA9IHRoaXMuYXV0aFNlcnZpY2UuZ2V0VXNlcigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIER5bmFtaWNDb21wb25lbnQ7XG4gICAgfVxuXG4gICAgdHJhY2tCeUZuKGlkeDogbnVtYmVyLCBpdGVtPzogYW55KSB7XG4gICAgICAgIHJldHVybiBpZHg7XG4gICAgfVxuXG59XG4iXX0=