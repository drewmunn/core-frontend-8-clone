/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-search-results/block-search-results.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { SearchService } from '../../pages/search.service';
import { FormControl } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';
var BlockSearchResultsComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockSearchResultsComponent, _super);
    function BlockSearchResultsComponent(authService, search) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        _this.search = search;
        _this.feedback = new FormControl();
        return _this;
    }
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.reset();
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.reset = /**
     * @return {?}
     */
    function () {
        this.feedbackEnabled = this.block.data.showFeedback;
        this.eddieExpanded = false;
    };
    /**
     * @param {?} idx
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.trackByFn = /**
     * @param {?} idx
     * @return {?}
     */
    function (idx) {
        return idx;
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.getResults = /**
     * @return {?}
     */
    function () {
        return this.search.results;
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.hasResults = /**
     * @return {?}
     */
    function () {
        return this.search.results.length > 0;
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.getEddie = /**
     * @return {?}
     */
    function () {
        return this.search.eddie;
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.hasEddie = /**
     * @return {?}
     */
    function () {
        return this.search.eddie !== false;
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.hasSearchTerm = /**
     * @return {?}
     */
    function () {
        return true;
    };
    /**
     * @param {?=} type
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.hasFeedback = /**
     * @param {?=} type
     * @return {?}
     */
    function (type) {
        return false;
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.givePositiveFeedback = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.giveNegativeFeedback = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.closeFeedback = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} query
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.performSearch = /**
     * @param {?} query
     * @return {?}
     */
    function (query) {
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.loadMore = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    BlockSearchResultsComponent.prototype.getTotalResults = /**
     * @return {?}
     */
    function () {
        return this.search.getTotalResults();
    };
    BlockSearchResultsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-search-results',
                    template: "<div class=\"search-results-content\">\n\n    <core-spinner name=\"search\"></core-spinner>\n\n    <div class=\"row search-top\">\n        <div class=\"col-sm-12 text-center\">\n            <p>\n                <span *ngIf=\"!hasResults() && !hasEddie() && hasSearchTerm()\"\n                      [innerHTML]=\"noResultsText\"></span>\n                <span *ngIf=\"hasResults() && introText\" [innerHTML]=\"introText\"></span>\n            </p>\n        </div>\n    </div>\n\n    <h3 *ngIf=\"hasResults() || hasEddie()\">Results</h3>\n\n    <!-- Eddie -->\n    <div class=\"eddie\" *ngIf=\"hasEddie()\">\n        <h4 class=\"eddie__question\" [innerHTML]=\"getEddie().question\"></h4>\n\n        <div [hidden]=\"eddieExpanded\">\n            <div class=\"eddie__answer\" [innerHTML]=\"getEddie().answer | limitTo: 150\"></div>\n            <div class=\"eddie__ellipsis\" *ngIf=\"getEddie().answer.length > 150\">...</div>\n        </div>\n\n        <div class=\"eddie__answer-expanded\" [hidden]=\"!eddieExpanded\">\n            <div class=\"eddie__answer expanded\" [innerHTML]=\"getEddie().answer\"></div>\n            <div class=\"eddie__helpful-links\" *ngIf=\"getEddie().supplementalData.links.length\">\n                <h5>Helpful Links</h5>\n                <a *ngFor=\"let link of getEddie().supplementalData.links\" [href]=\"link.href\"\n                   target=\"_blank\">{{link.label}}</a>\n            </div>\n\n            <div class=\"eddie__feeback\">\n                <div class=\"eddie__feedback-question\">Did this answer your question?</div>\n                <div class=\"eddie__feedback-answers-block\">\n                    <button [disabled]=\"hasFeedback()\" (click)=\"givePositiveFeedback()\"\n                            [ngClass]=\"{'active' : hasFeedback('positive') }\" class=\"btn btn-feedback\">\n                        Yes\n                    </button>\n                    <button [disabled]=\"hasFeedback()\" (click)=\"giveNegativeFeedback()\"\n                            [ngClass]=\"{'active' : hasFeedback('negative') }\" class=\"btn btn-feedback\">\n                        No\n                    </button>\n                </div>\n            </div>\n\n            <div [hidden]=\"!supportSubmitted\" class=\"alert alert-success\">\n                <p><strong>Thank you, we really value your feedback.</strong>\n                    <br/>The search and support feature is evolving all the time \u2013 the more you use it the better we can\n                    make it!</p>\n            </div>\n\n            <div class=\"eddie__feedback-form\" *ngIf=\"feedbackEnabled\">\n                <button type=\"button\" class=\"eddie__feedback-close\"\n                        (click)=\"closeFeedback();\">\n                    <img src=\"/assets/img/close.svg\" alt=\"Close\"/>\n                </button>\n\n                <p><strong>We really value your feedback, please tell us how we could improve this answer.</strong></p>\n                <form #feedbackForm=\"ngForm\">\n                    <div class=\"form-group\">\n                        <label>\n                            <span class=\"sr-only\">Message</span>\n                            <textarea class=\"form-control\" [formControl]=\"feedback\"></textarea>\n                        </label>\n                    </div>\n\n                    <div class=\"form-button\">\n                        <button class=\"btn btn-primary\"\n                                [disabled]=\"!feedback || feedback.value.length < 5 || supportSubmitted\"\n                                (click)=\"giveNegativeFeedback()\">Leave feedback\n                        </button>\n                    </div>\n                </form>\n            </div>\n        </div>\n\n        <div class=\"eddie__expand-button\"\n             (click)=\"eddieExpanded = !eddieExpanded\">{{eddieExpanded ? 'Hide full answer' : 'Show full answer'}}</div>\n    </div>\n\n    <div *ngIf=\"getEddie() && getEddie().relatedQuestions.length > 0\">\n        <core-block-divider></core-block-divider>\n        <div class=\"eddie-related\">\n            <h5>Your colleagues are also asking</h5>\n            <ul>\n                <li *ngFor=\"let question of getEddie().relatedQuestions\">\n                    <a (click)=\"performSearch(question);\">{{ question }}</a>\n                </li>\n            </ul>\n        </div>\n    </div>\n\n    <core-block-divider *ngIf=\"hasResults() && hasEddie()\"></core-block-divider>\n\n    <!-- Regular results -->\n    <div class=\"additional-results\" *ngIf=\"hasResults()\">\n        <h5 *ngIf=\"hasEddie()\">We also found related content on these pages</h5>\n\n        <ul class=\"search-results-list no-bullet\">\n            <li class=\"search-results-list__item\" *ngFor=\"let result of getResults() trackBy: trackByFn\">\n                <div class=\"search-results-wrapper\">\n                    <a class=\"search-results-list__link\" *ngIf=\"result.url\" [routerLink]=\"'/page/' + result.url\">\n                        <p class=\"result-name\" [innerHTML]=\"result.name\"></p>\n                    </a>\n                    <a class=\"search-results-list__link\" *ngIf=\"result.nodeId\"\n                       [href]=\"'/api/1.0/download/' + result.nodeId\"\n                       [target]=\"'_blank'\">\n                        <p class=\"result-name\" [innerHTML]=\"result.name\"></p>\n                    </a>\n                    <!--                    <div core-breadcrumb page=\"result\" ng-if=\"result.pageId\"></div>-->\n                    <p>{{ result.description | limitTo: 100 }}<span *ngIf=\"result.description.length > 100\">...</span>\n                    </p>\n                </div>\n            </li>\n        </ul>\n\n        <div class=\"button-wrapper text-left\" *ngIf=\"hasResults() && getResults().length < getTotalResults()\">\n            <button class=\"btn btn-default\" (click)=\"loadMore()\">{{block.data.loadMoreText}}</button>\n        </div>\n    </div>\n\n    <div *ngIf=\"hasResults() || hasEddie()\">\n        <core-block-divider></core-block-divider>\n        <p><strong>Not what you were looking for? Try searching again.</strong></p>\n    </div>\n\n</div>"
                }] }
    ];
    /** @nocollapse */
    BlockSearchResultsComponent.ctorParameters = function () { return [
        { type: AuthService },
        { type: SearchService }
    ]; };
    return BlockSearchResultsComponent;
}(BlockAbstractComponent));
export { BlockSearchResultsComponent };
if (false) {
    /** @type {?} */
    BlockSearchResultsComponent.prototype.feedbackEnabled;
    /** @type {?} */
    BlockSearchResultsComponent.prototype.eddieExpanded;
    /** @type {?} */
    BlockSearchResultsComponent.prototype.supportSubmitted;
    /** @type {?} */
    BlockSearchResultsComponent.prototype.noResultsText;
    /** @type {?} */
    BlockSearchResultsComponent.prototype.introText;
    /** @type {?} */
    BlockSearchResultsComponent.prototype.feedback;
    /**
     * @type {?}
     * @protected
     */
    BlockSearchResultsComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    BlockSearchResultsComponent.prototype.search;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stc2VhcmNoLXJlc3VsdHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stc2VhcmNoLXJlc3VsdHMvYmxvY2stc2VhcmNoLXJlc3VsdHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLDRCQUE0QixDQUFDO0FBQ3pELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0seUJBQXlCLENBQUM7QUFFcEQ7SUFJaUQsdURBQXNCO0lBV25FLHFDQUNjLFdBQXdCLEVBQ3hCLE1BQXFCO1FBRm5DLFlBSUksa0JBQ0ksV0FBVyxDQUNkLFNBQ0o7UUFOYSxpQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixZQUFNLEdBQU4sTUFBTSxDQUFlO1FBSm5DLGNBQVEsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDOztJQVM3QixDQUFDOzs7O0lBRUQsOENBQVE7OztJQUFSO1FBQ0ksSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7Ozs7SUFFRCwyQ0FBSzs7O0lBQUw7UUFDSSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUNwRCxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDOzs7OztJQUVELCtDQUFTOzs7O0lBQVQsVUFBVSxHQUFXO1FBQ2pCLE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVELGdEQUFVOzs7SUFBVjtRQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDL0IsQ0FBQzs7OztJQUVELGdEQUFVOzs7SUFBVjtRQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7O0lBRUQsOENBQVE7OztJQUFSO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUM3QixDQUFDOzs7O0lBRUQsOENBQVE7OztJQUFSO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUM7SUFDdkMsQ0FBQzs7OztJQUVELG1EQUFhOzs7SUFBYjtRQUNJLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7O0lBRUQsaURBQVc7Ozs7SUFBWCxVQUFZLElBQWE7UUFDckIsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUVELDBEQUFvQjs7O0lBQXBCO0lBRUEsQ0FBQzs7OztJQUVELDBEQUFvQjs7O0lBQXBCO0lBRUEsQ0FBQzs7OztJQUVELG1EQUFhOzs7SUFBYjtJQUVBLENBQUM7Ozs7O0lBRUQsbURBQWE7Ozs7SUFBYixVQUFjLEtBQVU7SUFFeEIsQ0FBQzs7OztJQUVELDhDQUFROzs7SUFBUjtJQUVBLENBQUM7Ozs7SUFFRCxxREFBZTs7O0lBQWY7UUFDSSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDekMsQ0FBQzs7Z0JBbkZKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyw4a01BQW9EO2lCQUN2RDs7OztnQkFMTyxXQUFXO2dCQUZYLGFBQWE7O0lBeUZyQixrQ0FBQztDQUFBLEFBckZELENBSWlELHNCQUFzQixHQWlGdEU7U0FqRlksMkJBQTJCOzs7SUFFcEMsc0RBQXlCOztJQUN6QixvREFBdUI7O0lBQ3ZCLHVEQUEwQjs7SUFFMUIsb0RBQXNCOztJQUN0QixnREFBa0I7O0lBRWxCLCtDQUE2Qjs7Ozs7SUFHekIsa0RBQWtDOzs7OztJQUNsQyw2Q0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tBYnN0cmFjdENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcbmltcG9ydCB7U2VhcmNoU2VydmljZX0gZnJvbSAnLi4vLi4vcGFnZXMvc2VhcmNoLnNlcnZpY2UnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vLi4vYXV0aC9hdXRoLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2stc2VhcmNoLXJlc3VsdHMnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9ibG9jay1zZWFyY2gtcmVzdWx0cy5jb21wb25lbnQuaHRtbCcsXG59KVxuZXhwb3J0IGNsYXNzIEJsb2NrU2VhcmNoUmVzdWx0c0NvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgZmVlZGJhY2tFbmFibGVkOiBib29sZWFuO1xuICAgIGVkZGllRXhwYW5kZWQ6IGJvb2xlYW47XG4gICAgc3VwcG9ydFN1Ym1pdHRlZDogYm9vbGVhbjtcblxuICAgIG5vUmVzdWx0c1RleHQ6IHN0cmluZztcbiAgICBpbnRyb1RleHQ6IHN0cmluZztcblxuICAgIGZlZWRiYWNrID0gbmV3IEZvcm1Db250cm9sKCk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICAgICAgcHJvdGVjdGVkIHNlYXJjaDogU2VhcmNoU2VydmljZVxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMucmVzZXQoKTtcbiAgICB9XG5cbiAgICByZXNldCgpIHtcbiAgICAgICAgdGhpcy5mZWVkYmFja0VuYWJsZWQgPSB0aGlzLmJsb2NrLmRhdGEuc2hvd0ZlZWRiYWNrO1xuICAgICAgICB0aGlzLmVkZGllRXhwYW5kZWQgPSBmYWxzZTtcbiAgICB9XG5cbiAgICB0cmFja0J5Rm4oaWR4OiBudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIGlkeDtcbiAgICB9XG5cbiAgICBnZXRSZXN1bHRzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWFyY2gucmVzdWx0cztcbiAgICB9XG5cbiAgICBoYXNSZXN1bHRzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWFyY2gucmVzdWx0cy5sZW5ndGggPiAwO1xuICAgIH1cblxuICAgIGdldEVkZGllKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWFyY2guZWRkaWU7XG4gICAgfVxuXG4gICAgaGFzRWRkaWUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlYXJjaC5lZGRpZSAhPT0gZmFsc2U7XG4gICAgfVxuXG4gICAgaGFzU2VhcmNoVGVybSgpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgaGFzRmVlZGJhY2sodHlwZT86IHN0cmluZykge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgZ2l2ZVBvc2l0aXZlRmVlZGJhY2soKSB7XG5cbiAgICB9XG5cbiAgICBnaXZlTmVnYXRpdmVGZWVkYmFjaygpIHtcblxuICAgIH1cblxuICAgIGNsb3NlRmVlZGJhY2soKSB7XG5cbiAgICB9XG5cbiAgICBwZXJmb3JtU2VhcmNoKHF1ZXJ5OiBhbnkpIHtcblxuICAgIH1cblxuICAgIGxvYWRNb3JlKCkge1xuXG4gICAgfVxuXG4gICAgZ2V0VG90YWxSZXN1bHRzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWFyY2guZ2V0VG90YWxSZXN1bHRzKCk7XG4gICAgfVxuXG59XG4iXX0=