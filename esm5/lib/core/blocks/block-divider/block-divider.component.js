/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-divider/block-divider.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
var BlockDividerComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockDividerComponent, _super);
    function BlockDividerComponent() {
        return _super.call(this) || this;
    }
    /**
     * @return {?}
     */
    BlockDividerComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        _super.prototype.ngOnInit.call(this);
    };
    BlockDividerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-divider',
                    template: "\n        <hr class=\"divider\"/>\n    "
                }] }
    ];
    /** @nocollapse */
    BlockDividerComponent.ctorParameters = function () { return []; };
    return BlockDividerComponent;
}(BlockAbstractComponent));
export { BlockDividerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stZGl2aWRlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2Nrcy9ibG9jay1kaXZpZGVyL2Jsb2NrLWRpdmlkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFHbkU7SUFNMkMsaURBQXNCO0lBRTdEO2VBRUksaUJBQ0M7SUFDTCxDQUFDOzs7O0lBRUQsd0NBQVE7OztJQUFSO1FBQ0ksaUJBQU0sUUFBUSxXQUFFLENBQUM7SUFDckIsQ0FBQzs7Z0JBaEJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixRQUFRLEVBQUUseUNBRVQ7aUJBQ0o7Ozs7SUFhRCw0QkFBQztDQUFBLEFBbEJELENBTTJDLHNCQUFzQixHQVloRTtTQVpZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vLi4vYXV0aC9hdXRoLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2stZGl2aWRlcicsXG4gICAgdGVtcGxhdGU6IGBcbiAgICAgICAgPGhyIGNsYXNzPVwiZGl2aWRlclwiLz5cbiAgICBgLFxufSlcbmV4cG9ydCBjbGFzcyBCbG9ja0RpdmlkZXJDb21wb25lbnQgZXh0ZW5kcyBCbG9ja0Fic3RyYWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgc3VwZXIubmdPbkluaXQoKTtcbiAgICB9XG5cbn1cbiJdfQ==