/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-wrapper/block-wrapper.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
var BlockWrapperComponent = /** @class */ (function (_super) {
    tslib_1.__extends(BlockWrapperComponent, _super);
    function BlockWrapperComponent() {
        return _super.call(this) || this;
    }
    /**
     * @return {?}
     */
    BlockWrapperComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} idx
     * @param {?=} item
     * @return {?}
     */
    BlockWrapperComponent.prototype.trackByFn = /**
     * @param {?} idx
     * @param {?=} item
     * @return {?}
     */
    function (idx, item) {
        return idx;
    };
    BlockWrapperComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-wrapper',
                    template: "<core-block class=\"{{block.data.class}} {{block.data.visibility}}\"\n            *ngFor=\"let subBlock of block.content trackBy: trackByFn\"\n            [block]=\"subBlock\"\n            [ngClass]=\"{ 'stickyDiv': block.data.specialType == 'sticky',\n            'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n             'col-sm-12': block.data.columnsSmall == 'full',\n             'col-md-12': block.data.columnsNormal == 'full'}\"\n            [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n>\n</core-block>"
                }] }
    ];
    /** @nocollapse */
    BlockWrapperComponent.ctorParameters = function () { return []; };
    return BlockWrapperComponent;
}(BlockAbstractComponent));
export { BlockWrapperComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2std3JhcHBlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2Nrcy9ibG9jay13cmFwcGVyL2Jsb2NrLXdyYXBwZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDaEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFFbkU7SUFLMkMsaURBQXNCO0lBRTdEO2VBQ0ksaUJBQU87SUFDWCxDQUFDOzs7O0lBRUQsd0NBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7Ozs7O0lBRUQseUNBQVM7Ozs7O0lBQVQsVUFBVSxHQUFXLEVBQUUsSUFBVTtRQUM3QixPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7O2dCQWhCSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsa2xCQUE2QztpQkFDaEQ7Ozs7SUFlRCw0QkFBQztDQUFBLEFBbEJELENBSzJDLHNCQUFzQixHQWFoRTtTQWJZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2std3JhcHBlcicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLXdyYXBwZXIuY29tcG9uZW50Lmh0bWwnLFxufSlcblxuZXhwb3J0IGNsYXNzIEJsb2NrV3JhcHBlckNvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgfVxuXG4gICAgdHJhY2tCeUZuKGlkeDogbnVtYmVyLCBpdGVtPzogYW55KSB7XG4gICAgICAgIHJldHVybiBpZHg7XG4gICAgfVxuXG59XG4iXX0=