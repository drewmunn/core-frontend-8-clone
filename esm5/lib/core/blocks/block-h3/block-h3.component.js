/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-h3/block-h3.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
var BlockH3Component = /** @class */ (function (_super) {
    tslib_1.__extends(BlockH3Component, _super);
    function BlockH3Component(authService) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        return _this;
    }
    /**
     * @return {?}
     */
    BlockH3Component.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        _super.prototype.ngOnInit.call(this);
        this.text = this.block.data.text;
    };
    BlockH3Component.decorators = [
        { type: Component, args: [{
                    selector: 'core-block-h3',
                    template: "\n        <h3>\n            <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n        </h3>"
                }] }
    ];
    /** @nocollapse */
    BlockH3Component.ctorParameters = function () { return [
        { type: AuthService }
    ]; };
    BlockH3Component.propDecorators = {
        text: [{ type: Input }]
    };
    return BlockH3Component;
}(BlockAbstractComponent));
export { BlockH3Component };
if (false) {
    /** @type {?} */
    BlockH3Component.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockH3Component.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2staDMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2staDMvYmxvY2staDMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBQ25FLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUVwRDtJQU9zQyw0Q0FBc0I7SUFJeEQsMEJBQ2MsV0FBd0I7UUFEdEMsWUFHSSxrQkFDSSxXQUFXLENBQ2QsU0FDSjtRQUxhLGlCQUFXLEdBQVgsV0FBVyxDQUFhOztJQUt0QyxDQUFDOzs7O0lBRUQsbUNBQVE7OztJQUFSO1FBQ0ksaUJBQU0sUUFBUSxXQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckMsQ0FBQzs7Z0JBdEJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsUUFBUSxFQUFFLG9IQUdBO2lCQUNiOzs7O2dCQVJPLFdBQVc7Ozt1QkFXZCxLQUFLOztJQWVWLHVCQUFDO0NBQUEsQUF4QkQsQ0FPc0Msc0JBQXNCLEdBaUIzRDtTQWpCWSxnQkFBZ0I7OztJQUV6QixnQ0FBc0I7Ozs7O0lBR2xCLHVDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tBYnN0cmFjdENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcbmltcG9ydCB7QXV0aFNlcnZpY2V9IGZyb20gJy4uLy4uL2F1dGgvYXV0aC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdjb3JlLWJsb2NrLWgzJyxcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8aDM+XG4gICAgICAgICAgICA8bmctY29udGFpbmVyICpjb3JlQmxvY2tDb21waWxlPVwidGV4dDsgY29udGV4dDogdGhpc1wiPjwvbmctY29udGFpbmVyPlxuICAgICAgICA8L2gzPmAsXG59KVxuZXhwb3J0IGNsYXNzIEJsb2NrSDNDb21wb25lbnQgZXh0ZW5kcyBCbG9ja0Fic3RyYWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIEBJbnB1dCgpIHRleHQ6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgICAgIHRoaXMudGV4dCA9IHRoaXMuYmxvY2suZGF0YS50ZXh0O1xuICAgIH1cblxufVxuIl19