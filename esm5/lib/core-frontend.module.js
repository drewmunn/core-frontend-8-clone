/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-frontend.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CoreFrontendComponent } from './core-frontend.component';
import { CoreModule } from './core/core.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule } from '@angular/material/expansion';
import { TranslateModule } from '@ngx-translate/core';
import { AuthModule } from './core/auth/auth.module';
import { PanelsModule } from './core/utils/panels/panels.module';
import { SpinnerModule } from './core/utils/spinner/spinner.module';
import { PageModule } from './core/pages/page.module';
import { TranslateService } from './core/pages/translate.service';
import { PageService } from './core/pages/page.service';
import { CacheService } from './core/cache.service';
import { CoreRoutingModule } from './core-routing.module';
import { BlockService } from './core/pages/block.service';
var CoreFrontendModule = /** @class */ (function () {
    function CoreFrontendModule() {
    }
    /**
     * @return {?}
     */
    CoreFrontendModule.forRoot = /**
     * @return {?}
     */
    function () {
        return {
            ngModule: CoreFrontendModule,
            providers: [
                CacheService,
                PageService,
                BlockService,
                TranslateService,
            ]
        };
    };
    CoreFrontendModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        CoreFrontendComponent,
                    ],
                    imports: [
                        BrowserModule,
                        CoreModule,
                        AuthModule,
                        PanelsModule,
                        BrowserAnimationsModule,
                        MatExpansionModule,
                        TranslateModule,
                        PageModule,
                        SpinnerModule,
                        CoreRoutingModule,
                    ],
                    providers: [],
                    bootstrap: [CoreFrontendComponent],
                    exports: [
                        CoreModule,
                    ]
                },] }
    ];
    return CoreFrontendModule;
}());
export { CoreFrontendModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1mcm9udGVuZC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlLWZyb250ZW5kLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBc0IsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzVELE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQ2hFLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxvQkFBb0IsQ0FBQztBQUM5QyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sMkJBQTJCLENBQUM7QUFDeEQsT0FBTyxFQUFDLHVCQUF1QixFQUFDLE1BQU0sc0NBQXNDLENBQUM7QUFDN0UsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDL0QsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQ3BELE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUNuRCxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sbUNBQW1DLENBQUM7QUFDL0QsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHFDQUFxQyxDQUFDO0FBQ2xFLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUNwRCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sMkJBQTJCLENBQUM7QUFDdEQsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLHVCQUF1QixDQUFDO0FBQ3hELE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSw0QkFBNEIsQ0FBQztBQUV4RDtJQUFBO0lBbUNBLENBQUM7Ozs7SUFYVSwwQkFBTzs7O0lBQWQ7UUFDSSxPQUFPO1lBQ0gsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixTQUFTLEVBQUU7Z0JBQ1AsWUFBWTtnQkFDWixXQUFXO2dCQUNYLFlBQVk7Z0JBQ1osZ0JBQWdCO2FBQ25CO1NBQ0osQ0FBQztJQUNOLENBQUM7O2dCQWxDSixRQUFRLFNBQUM7b0JBQ04sWUFBWSxFQUFFO3dCQUNWLHFCQUFxQjtxQkFDeEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNMLGFBQWE7d0JBQ2IsVUFBVTt3QkFDVixVQUFVO3dCQUNWLFlBQVk7d0JBQ1osdUJBQXVCO3dCQUN2QixrQkFBa0I7d0JBQ2xCLGVBQWU7d0JBQ2YsVUFBVTt3QkFDVixhQUFhO3dCQUNiLGlCQUFpQjtxQkFDcEI7b0JBQ0QsU0FBUyxFQUFFLEVBQUU7b0JBQ2IsU0FBUyxFQUFFLENBQUMscUJBQXFCLENBQUM7b0JBQ2xDLE9BQU8sRUFBRTt3QkFDTCxVQUFVO3FCQUNiO2lCQUNKOztJQWNELHlCQUFDO0NBQUEsQUFuQ0QsSUFtQ0M7U0FaWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge01vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q29yZUZyb250ZW5kQ29tcG9uZW50fSBmcm9tICcuL2NvcmUtZnJvbnRlbmQuY29tcG9uZW50JztcbmltcG9ydCB7Q29yZU1vZHVsZX0gZnJvbSAnLi9jb3JlL2NvcmUubW9kdWxlJztcbmltcG9ydCB7QnJvd3Nlck1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic7XG5pbXBvcnQge0Jyb3dzZXJBbmltYXRpb25zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyL2FuaW1hdGlvbnMnO1xuaW1wb3J0IHtNYXRFeHBhbnNpb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2V4cGFuc2lvbic7XG5pbXBvcnQge1RyYW5zbGF0ZU1vZHVsZX0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQge0F1dGhNb2R1bGV9IGZyb20gJy4vY29yZS9hdXRoL2F1dGgubW9kdWxlJztcbmltcG9ydCB7UGFuZWxzTW9kdWxlfSBmcm9tICcuL2NvcmUvdXRpbHMvcGFuZWxzL3BhbmVscy5tb2R1bGUnO1xuaW1wb3J0IHtTcGlubmVyTW9kdWxlfSBmcm9tICcuL2NvcmUvdXRpbHMvc3Bpbm5lci9zcGlubmVyLm1vZHVsZSc7XG5pbXBvcnQge1BhZ2VNb2R1bGV9IGZyb20gJy4vY29yZS9wYWdlcy9wYWdlLm1vZHVsZSc7XG5pbXBvcnQge1RyYW5zbGF0ZVNlcnZpY2V9IGZyb20gJy4vY29yZS9wYWdlcy90cmFuc2xhdGUuc2VydmljZSc7XG5pbXBvcnQge1BhZ2VTZXJ2aWNlfSBmcm9tICcuL2NvcmUvcGFnZXMvcGFnZS5zZXJ2aWNlJztcbmltcG9ydCB7Q2FjaGVTZXJ2aWNlfSBmcm9tICcuL2NvcmUvY2FjaGUuc2VydmljZSc7XG5pbXBvcnQge0NvcmVSb3V0aW5nTW9kdWxlfSBmcm9tICcuL2NvcmUtcm91dGluZy5tb2R1bGUnO1xuaW1wb3J0IHtCbG9ja1NlcnZpY2V9IGZyb20gJy4vY29yZS9wYWdlcy9ibG9jay5zZXJ2aWNlJztcblxuQE5nTW9kdWxlKHtcbiAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgQ29yZUZyb250ZW5kQ29tcG9uZW50LFxuICAgIF0sXG4gICAgaW1wb3J0czogW1xuICAgICAgICBCcm93c2VyTW9kdWxlLFxuICAgICAgICBDb3JlTW9kdWxlLFxuICAgICAgICBBdXRoTW9kdWxlLFxuICAgICAgICBQYW5lbHNNb2R1bGUsXG4gICAgICAgIEJyb3dzZXJBbmltYXRpb25zTW9kdWxlLFxuICAgICAgICBNYXRFeHBhbnNpb25Nb2R1bGUsXG4gICAgICAgIFRyYW5zbGF0ZU1vZHVsZSxcbiAgICAgICAgUGFnZU1vZHVsZSxcbiAgICAgICAgU3Bpbm5lck1vZHVsZSxcbiAgICAgICAgQ29yZVJvdXRpbmdNb2R1bGUsXG4gICAgXSxcbiAgICBwcm92aWRlcnM6IFtdLFxuICAgIGJvb3RzdHJhcDogW0NvcmVGcm9udGVuZENvbXBvbmVudF0sXG4gICAgZXhwb3J0czogW1xuICAgICAgICBDb3JlTW9kdWxlLFxuICAgIF1cbn0pXG5cbmV4cG9ydCBjbGFzcyBDb3JlRnJvbnRlbmRNb2R1bGUge1xuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmdNb2R1bGU6IENvcmVGcm9udGVuZE1vZHVsZSxcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgICAgICAgIENhY2hlU2VydmljZSxcbiAgICAgICAgICAgICAgICBQYWdlU2VydmljZSxcbiAgICAgICAgICAgICAgICBCbG9ja1NlcnZpY2UsXG4gICAgICAgICAgICAgICAgVHJhbnNsYXRlU2VydmljZSxcbiAgICAgICAgICAgIF1cbiAgICAgICAgfTtcbiAgICB9XG59XG4iXX0=