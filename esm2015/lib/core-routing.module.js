/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-routing.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthService } from './core/auth/auth.service';
import { PageComponent } from './core/pages/page.component';
const ɵ0 = { breadcrumbs: ['Home'] };
/** @type {?} */
const routes = [
    {
        path: '',
        canActivate: [AuthService],
        children: [
            {
                path: '', component: PageComponent,
                data: ɵ0
            },
        ]
    },
];
export class CoreRoutingModule {
}
CoreRoutingModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    RouterModule.forRoot(routes)
                ],
                exports: [RouterModule]
            },] }
];
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1yb3V0aW5nLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUtcm91dGluZy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQVMsTUFBTSxpQkFBaUIsQ0FBQztBQUNyRCxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLDZCQUE2QixDQUFDO1dBVXBDLEVBQUMsV0FBVyxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQUM7O01BUnZDLE1BQU0sR0FBVztJQUVuQjtRQUNJLElBQUksRUFBRSxFQUFFO1FBQ1IsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDO1FBQzFCLFFBQVEsRUFBRTtZQUNOO2dCQUNJLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGFBQWE7Z0JBQ2xDLElBQUksSUFBeUI7YUFDaEM7U0FDSjtLQUNKO0NBQ0o7QUFRRCxNQUFNLE9BQU8saUJBQWlCOzs7WUFON0IsUUFBUSxTQUFDO2dCQUNOLE9BQU8sRUFBRTtvQkFDTCxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQztpQkFDL0I7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO2FBQzFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1JvdXRlck1vZHVsZSwgUm91dGVzfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi9jb3JlL2F1dGgvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7UGFnZUNvbXBvbmVudH0gZnJvbSAnLi9jb3JlL3BhZ2VzL3BhZ2UuY29tcG9uZW50JztcblxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXG5cbiAgICB7XG4gICAgICAgIHBhdGg6ICcnLFxuICAgICAgICBjYW5BY3RpdmF0ZTogW0F1dGhTZXJ2aWNlXSxcbiAgICAgICAgY2hpbGRyZW46IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBwYXRoOiAnJywgY29tcG9uZW50OiBQYWdlQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIGRhdGE6IHticmVhZGNydW1iczogWydIb21lJ119XG4gICAgICAgICAgICB9LFxuICAgICAgICBdXG4gICAgfSxcbl07XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW1xuICAgICAgICBSb3V0ZXJNb2R1bGUuZm9yUm9vdChyb3V0ZXMpXG4gICAgXSxcbiAgICBleHBvcnRzOiBbUm91dGVyTW9kdWxlXVxufSlcbmV4cG9ydCBjbGFzcyBDb3JlUm91dGluZ01vZHVsZSB7XG59XG4iXX0=