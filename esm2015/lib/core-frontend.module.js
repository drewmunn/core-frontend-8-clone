/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-frontend.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CoreFrontendComponent } from './core-frontend.component';
import { CoreModule } from './core/core.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule } from '@angular/material/expansion';
import { TranslateModule } from '@ngx-translate/core';
import { AuthModule } from './core/auth/auth.module';
import { PanelsModule } from './core/utils/panels/panels.module';
import { SpinnerModule } from './core/utils/spinner/spinner.module';
import { PageModule } from './core/pages/page.module';
import { TranslateService } from './core/pages/translate.service';
import { PageService } from './core/pages/page.service';
import { CacheService } from './core/cache.service';
import { CoreRoutingModule } from './core-routing.module';
import { BlockService } from './core/pages/block.service';
export class CoreFrontendModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return {
            ngModule: CoreFrontendModule,
            providers: [
                CacheService,
                PageService,
                BlockService,
                TranslateService,
            ]
        };
    }
}
CoreFrontendModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    CoreFrontendComponent,
                ],
                imports: [
                    BrowserModule,
                    CoreModule,
                    AuthModule,
                    PanelsModule,
                    BrowserAnimationsModule,
                    MatExpansionModule,
                    TranslateModule,
                    PageModule,
                    SpinnerModule,
                    CoreRoutingModule,
                ],
                providers: [],
                bootstrap: [CoreFrontendComponent],
                exports: [
                    CoreModule,
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1mcm9udGVuZC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlLWZyb250ZW5kLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBc0IsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzVELE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQ2hFLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxvQkFBb0IsQ0FBQztBQUM5QyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sMkJBQTJCLENBQUM7QUFDeEQsT0FBTyxFQUFDLHVCQUF1QixFQUFDLE1BQU0sc0NBQXNDLENBQUM7QUFDN0UsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDL0QsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQ3BELE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUNuRCxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sbUNBQW1DLENBQUM7QUFDL0QsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHFDQUFxQyxDQUFDO0FBQ2xFLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUNwRCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sMkJBQTJCLENBQUM7QUFDdEQsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLHVCQUF1QixDQUFDO0FBQ3hELE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSw0QkFBNEIsQ0FBQztBQXlCeEQsTUFBTSxPQUFPLGtCQUFrQjs7OztJQUMzQixNQUFNLENBQUMsT0FBTztRQUNWLE9BQU87WUFDSCxRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLFNBQVMsRUFBRTtnQkFDUCxZQUFZO2dCQUNaLFdBQVc7Z0JBQ1gsWUFBWTtnQkFDWixnQkFBZ0I7YUFDbkI7U0FDSixDQUFDO0lBQ04sQ0FBQzs7O1lBbENKLFFBQVEsU0FBQztnQkFDTixZQUFZLEVBQUU7b0JBQ1YscUJBQXFCO2lCQUN4QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ0wsYUFBYTtvQkFDYixVQUFVO29CQUNWLFVBQVU7b0JBQ1YsWUFBWTtvQkFDWix1QkFBdUI7b0JBQ3ZCLGtCQUFrQjtvQkFDbEIsZUFBZTtvQkFDZixVQUFVO29CQUNWLGFBQWE7b0JBQ2IsaUJBQWlCO2lCQUNwQjtnQkFDRCxTQUFTLEVBQUUsRUFBRTtnQkFDYixTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztnQkFDbEMsT0FBTyxFQUFFO29CQUNMLFVBQVU7aUJBQ2I7YUFDSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb3JlRnJvbnRlbmRDb21wb25lbnR9IGZyb20gJy4vY29yZS1mcm9udGVuZC5jb21wb25lbnQnO1xuaW1wb3J0IHtDb3JlTW9kdWxlfSBmcm9tICcuL2NvcmUvY29yZS5tb2R1bGUnO1xuaW1wb3J0IHtCcm93c2VyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcbmltcG9ydCB7QnJvd3NlckFuaW1hdGlvbnNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXIvYW5pbWF0aW9ucyc7XG5pbXBvcnQge01hdEV4cGFuc2lvbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZXhwYW5zaW9uJztcbmltcG9ydCB7VHJhbnNsYXRlTW9kdWxlfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7QXV0aE1vZHVsZX0gZnJvbSAnLi9jb3JlL2F1dGgvYXV0aC5tb2R1bGUnO1xuaW1wb3J0IHtQYW5lbHNNb2R1bGV9IGZyb20gJy4vY29yZS91dGlscy9wYW5lbHMvcGFuZWxzLm1vZHVsZSc7XG5pbXBvcnQge1NwaW5uZXJNb2R1bGV9IGZyb20gJy4vY29yZS91dGlscy9zcGlubmVyL3NwaW5uZXIubW9kdWxlJztcbmltcG9ydCB7UGFnZU1vZHVsZX0gZnJvbSAnLi9jb3JlL3BhZ2VzL3BhZ2UubW9kdWxlJztcbmltcG9ydCB7VHJhbnNsYXRlU2VydmljZX0gZnJvbSAnLi9jb3JlL3BhZ2VzL3RyYW5zbGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7UGFnZVNlcnZpY2V9IGZyb20gJy4vY29yZS9wYWdlcy9wYWdlLnNlcnZpY2UnO1xuaW1wb3J0IHtDYWNoZVNlcnZpY2V9IGZyb20gJy4vY29yZS9jYWNoZS5zZXJ2aWNlJztcbmltcG9ydCB7Q29yZVJvdXRpbmdNb2R1bGV9IGZyb20gJy4vY29yZS1yb3V0aW5nLm1vZHVsZSc7XG5pbXBvcnQge0Jsb2NrU2VydmljZX0gZnJvbSAnLi9jb3JlL3BhZ2VzL2Jsb2NrLnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICBDb3JlRnJvbnRlbmRDb21wb25lbnQsXG4gICAgXSxcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIEJyb3dzZXJNb2R1bGUsXG4gICAgICAgIENvcmVNb2R1bGUsXG4gICAgICAgIEF1dGhNb2R1bGUsXG4gICAgICAgIFBhbmVsc01vZHVsZSxcbiAgICAgICAgQnJvd3NlckFuaW1hdGlvbnNNb2R1bGUsXG4gICAgICAgIE1hdEV4cGFuc2lvbk1vZHVsZSxcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlLFxuICAgICAgICBQYWdlTW9kdWxlLFxuICAgICAgICBTcGlubmVyTW9kdWxlLFxuICAgICAgICBDb3JlUm91dGluZ01vZHVsZSxcbiAgICBdLFxuICAgIHByb3ZpZGVyczogW10sXG4gICAgYm9vdHN0cmFwOiBbQ29yZUZyb250ZW5kQ29tcG9uZW50XSxcbiAgICBleHBvcnRzOiBbXG4gICAgICAgIENvcmVNb2R1bGUsXG4gICAgXVxufSlcblxuZXhwb3J0IGNsYXNzIENvcmVGcm9udGVuZE1vZHVsZSB7XG4gICAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBuZ01vZHVsZTogQ29yZUZyb250ZW5kTW9kdWxlLFxuICAgICAgICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgICAgICAgICAgQ2FjaGVTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIFBhZ2VTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIEJsb2NrU2VydmljZSxcbiAgICAgICAgICAgICAgICBUcmFuc2xhdGVTZXJ2aWNlLFxuICAgICAgICAgICAgXVxuICAgICAgICB9O1xuICAgIH1cbn1cbiJdfQ==