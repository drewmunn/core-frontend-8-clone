/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/cache.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common/http";
export class CacheService {
    /**
     * @param {?=} router
     * @param {?=} api
     */
    constructor(router, api) {
        this.router = router;
        this.api = api;
        this.refresh();
    }
    /**
     * @return {?}
     */
    refresh() {
        this.cacheProbeTimestamp = {};
        this.objectLoadObservables$ = {};
        this.cacheProbePromises$ = {};
    }
    /**
     * @param {?} label
     * @return {?}
     */
    resetObjectLoad(label) {
        delete this.objectLoadObservables$[label];
    }
    /**
     * @param {?} inputs
     * @return {?}
     */
    createCacheKey(inputs) {
        return JSON.stringify(inputs);
    }
    /**
     * @param {?} item
     * @return {?}
     */
    getFromStorage(item) {
        try {
            if (localStorage.getItem(item)) {
                return localStorage.getItem(item);
            }
        }
        catch (e) {
        }
        try {
            if (sessionStorage.getItem(item)) {
                return sessionStorage.getItem(item);
            }
        }
        catch (e) {
        }
        return false;
    }
    /**
     * @param {?} item
     * @return {?}
     */
    removeFromStorage(item) {
        try {
            localStorage.removeItem(item);
        }
        catch (e) {
        }
        try {
            sessionStorage.removeItem(item);
        }
        catch (e) {
        }
    }
    /**
     * @param {?} route
     * @param {?} identifier
     * @param {?=} headers
     * @param {?=} cacheKey
     * @param {?=} query
     * @return {?}
     */
    getOneThroughCache(route, identifier, headers, cacheKey, query) {
        if (!cacheKey) {
            cacheKey = this.createCacheKey({
                route,
                identifier,
                headers,
                query,
            });
        }
        if (!this.objectLoadObservables$) {
            this.objectLoadObservables$ = {};
        }
        if (this.objectLoadObservables$[cacheKey]) {
            return this.objectLoadObservables$[cacheKey];
        }
        this.objectLoadObservables$[cacheKey] = new ReplaySubject();
        this.api.get(route + (identifier ? '/' + identifier : ''), {
            params: query || {},
            headers: headers || {}
        }).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            try {
                this.objectLoadObservables$[cacheKey].next(response);
            }
            catch (err) {
            }
        }));
        return this.objectLoadObservables$[cacheKey];
    }
    /**
     * @param {?} route
     * @param {?=} query
     * @param {?=} headers
     * @param {?=} cacheKey
     * @return {?}
     */
    getListThroughCache(route, query, headers, cacheKey) {
        if (!cacheKey) {
            cacheKey = this.createCacheKey({
                route,
                query,
                headers
            });
        }
        if (!this.objectLoadObservables$) {
            this.objectLoadObservables$ = {};
        }
        if (this.objectLoadObservables$[cacheKey]) {
            return this.objectLoadObservables$[cacheKey];
        }
        this.objectLoadObservables$[cacheKey] = new ReplaySubject();
        this.api.get(route, {
            params: query || {},
            headers: headers || {}
        })
            .subscribe((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            try {
                this.objectLoadObservables$[cacheKey].next(response);
            }
            catch (err) {
            }
        }));
        return this.objectLoadObservables$[cacheKey];
    }
}
CacheService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CacheService.ctorParameters = () => [
    { type: Router },
    { type: HttpClient }
];
/** @nocollapse */ CacheService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CacheService_Factory() { return new CacheService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.HttpClient)); }, token: CacheService, providedIn: "root" });
if (false) {
    /** @type {?} */
    CacheService.prototype.cacheProbeTimestamp;
    /** @type {?} */
    CacheService.prototype.cacheProbePromises$;
    /** @type {?} */
    CacheService.prototype.objectLoadObservables$;
    /**
     * @type {?}
     * @protected
     */
    CacheService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    CacheService.prototype.api;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FjaGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvY2FjaGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFDbkMsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLHNCQUFzQixDQUFDOzs7O0FBTWhELE1BQU0sT0FBTyxZQUFZOzs7OztJQUtyQixZQUNjLE1BQWUsRUFDZixHQUFnQjtRQURoQixXQUFNLEdBQU4sTUFBTSxDQUFTO1FBQ2YsUUFBRyxHQUFILEdBQUcsQ0FBYTtRQUUxQixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkIsQ0FBQzs7OztJQUVELE9BQU87UUFDSCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxFQUFFLENBQUM7UUFDakMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztJQUNsQyxDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxLQUFLO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzlDLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLE1BQU07UUFDakIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLElBQUk7UUFDZixJQUFJO1lBQ0EsSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUM1QixPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDckM7U0FDSjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1NBQ1g7UUFDRCxJQUFJO1lBQ0EsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUM5QixPQUFPLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdkM7U0FDSjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1NBQ1g7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7OztJQUVELGlCQUFpQixDQUFDLElBQUk7UUFDbEIsSUFBSTtZQUNBLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDakM7UUFBQyxPQUFPLENBQUMsRUFBRTtTQUNYO1FBQ0QsSUFBSTtZQUNBLGNBQWMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbkM7UUFBQyxPQUFPLENBQUMsRUFBRTtTQUNYO0lBQ0wsQ0FBQzs7Ozs7Ozs7O0lBRUQsa0JBQWtCLENBQUMsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFRLEVBQUUsUUFBUyxFQUFFLEtBQU07UUFDN0QsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNYLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO2dCQUMzQixLQUFLO2dCQUNMLFVBQVU7Z0JBQ1YsT0FBTztnQkFDUCxLQUFLO2FBQ1IsQ0FBQyxDQUFDO1NBQ047UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFO1lBQzlCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxFQUFFLENBQUM7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUN2QyxPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNoRDtRQUNELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLGFBQWEsRUFBWSxDQUFDO1FBQ3RFLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQ3JEO1lBQ0ksTUFBTSxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ25CLE9BQU8sRUFBRSxPQUFPLElBQUksRUFBRTtTQUN6QixDQUFDLENBQUMsU0FBUzs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3hCLElBQUk7Z0JBQ0EsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN4RDtZQUFDLE9BQU8sR0FBRyxFQUFFO2FBQ2I7UUFDTCxDQUFDLEVBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7Ozs7O0lBRUQsbUJBQW1CLENBQUMsS0FBSyxFQUFFLEtBQU0sRUFBRSxPQUFRLEVBQUUsUUFBUztRQUNsRCxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ1gsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQzNCLEtBQUs7Z0JBQ0wsS0FBSztnQkFDTCxPQUFPO2FBQ1YsQ0FBQyxDQUFDO1NBQ047UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFO1lBQzlCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxFQUFFLENBQUM7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUN2QyxPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNoRDtRQUNELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLGFBQWEsRUFBWSxDQUFDO1FBQ3RFLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEtBQUssRUFDZDtZQUNJLE1BQU0sRUFBRSxLQUFLLElBQUksRUFBRTtZQUNuQixPQUFPLEVBQUUsT0FBTyxJQUFJLEVBQUU7U0FDekIsQ0FBQzthQUNELFNBQVM7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNkLElBQUk7Z0JBQ0EsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN4RDtZQUFDLE9BQU8sR0FBRyxFQUFFO2FBQ2I7UUFDTCxDQUFDLEVBQ0osQ0FBQztRQUNOLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2pELENBQUM7OztZQWxISixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFOTyxNQUFNO1lBRU4sVUFBVTs7Ozs7SUFPZCwyQ0FBeUI7O0lBQ3pCLDJDQUF5Qjs7SUFDekIsOENBQTRCOzs7OztJQUd4Qiw4QkFBeUI7Ozs7O0lBQ3pCLDJCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1JvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7UmVwbGF5U3ViamVjdH0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0h0dHBDbGllbnR9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcblxuZXhwb3J0IGNsYXNzIENhY2hlU2VydmljZSB7XG4gICAgY2FjaGVQcm9iZVRpbWVzdGFtcDogYW55O1xuICAgIGNhY2hlUHJvYmVQcm9taXNlcyQ6IGFueTtcbiAgICBvYmplY3RMb2FkT2JzZXJ2YWJsZXMkOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIHJvdXRlcj86IFJvdXRlcixcbiAgICAgICAgcHJvdGVjdGVkIGFwaT86IEh0dHBDbGllbnQsXG4gICAgKSB7XG4gICAgICAgIHRoaXMucmVmcmVzaCgpO1xuICAgIH1cblxuICAgIHJlZnJlc2goKTogdm9pZCB7XG4gICAgICAgIHRoaXMuY2FjaGVQcm9iZVRpbWVzdGFtcCA9IHt9O1xuICAgICAgICB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyQgPSB7fTtcbiAgICAgICAgdGhpcy5jYWNoZVByb2JlUHJvbWlzZXMkID0ge307XG4gICAgfVxuXG4gICAgcmVzZXRPYmplY3RMb2FkKGxhYmVsKSB7XG4gICAgICAgIGRlbGV0ZSB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbbGFiZWxdO1xuICAgIH1cblxuICAgIGNyZWF0ZUNhY2hlS2V5KGlucHV0cykge1xuICAgICAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkoaW5wdXRzKTtcbiAgICB9XG5cbiAgICBnZXRGcm9tU3RvcmFnZShpdGVtKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBpZiAobG9jYWxTdG9yYWdlLmdldEl0ZW0oaXRlbSkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oaXRlbSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgfVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oaXRlbSkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShpdGVtKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICByZW1vdmVGcm9tU3RvcmFnZShpdGVtKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShpdGVtKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICB9XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5yZW1vdmVJdGVtKGl0ZW0pO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBnZXRPbmVUaHJvdWdoQ2FjaGUocm91dGUsIGlkZW50aWZpZXIsIGhlYWRlcnM/LCBjYWNoZUtleT8sIHF1ZXJ5Pykge1xuICAgICAgICBpZiAoIWNhY2hlS2V5KSB7XG4gICAgICAgICAgICBjYWNoZUtleSA9IHRoaXMuY3JlYXRlQ2FjaGVLZXkoe1xuICAgICAgICAgICAgICAgIHJvdXRlLFxuICAgICAgICAgICAgICAgIGlkZW50aWZpZXIsXG4gICAgICAgICAgICAgICAgaGVhZGVycyxcbiAgICAgICAgICAgICAgICBxdWVyeSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkKSB7XG4gICAgICAgICAgICB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyQgPSB7fTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XSkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFtjYWNoZUtleV07XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XSA9IG5ldyBSZXBsYXlTdWJqZWN0PFJlc3BvbnNlPigpO1xuICAgICAgICB0aGlzLmFwaS5nZXQocm91dGUgKyAoaWRlbnRpZmllciA/ICcvJyArIGlkZW50aWZpZXIgOiAnJyksXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcGFyYW1zOiBxdWVyeSB8fCB7fSxcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiBoZWFkZXJzIHx8IHt9XG4gICAgICAgICAgICB9KS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbY2FjaGVLZXldLm5leHQocmVzcG9uc2UpO1xuICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XTtcbiAgICB9XG5cbiAgICBnZXRMaXN0VGhyb3VnaENhY2hlKHJvdXRlLCBxdWVyeT8sIGhlYWRlcnM/LCBjYWNoZUtleT8pIHtcbiAgICAgICAgaWYgKCFjYWNoZUtleSkge1xuICAgICAgICAgICAgY2FjaGVLZXkgPSB0aGlzLmNyZWF0ZUNhY2hlS2V5KHtcbiAgICAgICAgICAgICAgICByb3V0ZSxcbiAgICAgICAgICAgICAgICBxdWVyeSxcbiAgICAgICAgICAgICAgICBoZWFkZXJzXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoIXRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJCkge1xuICAgICAgICAgICAgdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkID0ge307XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFtjYWNoZUtleV0pIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbY2FjaGVLZXldO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFtjYWNoZUtleV0gPSBuZXcgUmVwbGF5U3ViamVjdDxSZXNwb25zZT4oKTtcbiAgICAgICAgdGhpcy5hcGkuZ2V0KHJvdXRlLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHBhcmFtczogcXVlcnkgfHwge30sXG4gICAgICAgICAgICAgICAgaGVhZGVyczogaGVhZGVycyB8fCB7fVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XS5uZXh0KHJlc3BvbnNlKTtcbiAgICAgICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgICAgICByZXR1cm4gdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XTtcbiAgICB9XG5cbn1cbiJdfQ==