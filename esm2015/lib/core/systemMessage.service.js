/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/systemMessage.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class SystemMessageService {
    /**
     * @param {?} systemMessageService
     */
    constructor(systemMessageService) {
        this.systemMessageService = systemMessageService;
    }
    /**
     * @return {?}
     */
    clearInterceptMessage() {
        this.interceptMessage = null;
    }
    /**
     * @param {?} message
     * @return {?}
     */
    setInterceptMessage(message) {
        this.interceptMessage = message;
    }
    /**
     * @return {?}
     */
    getInterceptMessage() {
        return this.interceptMessage;
    }
}
SystemMessageService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SystemMessageService.ctorParameters = () => [
    { type: SystemMessageService }
];
/** @nocollapse */ SystemMessageService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SystemMessageService_Factory() { return new SystemMessageService(i0.ɵɵinject(SystemMessageService)); }, token: SystemMessageService, providedIn: "root" });
if (false) {
    /** @type {?} */
    SystemMessageService.prototype.interceptMessage;
    /**
     * @type {?}
     * @private
     */
    SystemMessageService.prototype.systemMessageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtTWVzc2FnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9zeXN0ZW1NZXNzYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDOztBQU16QyxNQUFNLE9BQU8sb0JBQW9COzs7O0lBRzdCLFlBQW9CLG9CQUEwQztRQUExQyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO0lBQzlELENBQUM7Ozs7SUFFRCxxQkFBcUI7UUFDakIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztJQUNqQyxDQUFDOzs7OztJQUVELG1CQUFtQixDQUFDLE9BQWU7UUFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE9BQU8sQ0FBQztJQUNwQyxDQUFDOzs7O0lBRUQsbUJBQW1CO1FBQ2YsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDakMsQ0FBQzs7O1lBcEJKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQUs2QyxvQkFBb0I7Ozs7O0lBRjlELGdEQUF5Qjs7Ozs7SUFFYixvREFBa0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5leHBvcnQgY2xhc3MgU3lzdGVtTWVzc2FnZVNlcnZpY2Uge1xuICAgIGludGVyY2VwdE1lc3NhZ2U6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgc3lzdGVtTWVzc2FnZVNlcnZpY2U6IFN5c3RlbU1lc3NhZ2VTZXJ2aWNlKSB7XG4gICAgfVxuXG4gICAgY2xlYXJJbnRlcmNlcHRNZXNzYWdlKCkge1xuICAgICAgICB0aGlzLmludGVyY2VwdE1lc3NhZ2UgPSBudWxsO1xuICAgIH1cblxuICAgIHNldEludGVyY2VwdE1lc3NhZ2UobWVzc2FnZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMuaW50ZXJjZXB0TWVzc2FnZSA9IG1lc3NhZ2U7XG4gICAgfVxuXG4gICAgZ2V0SW50ZXJjZXB0TWVzc2FnZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW50ZXJjZXB0TWVzc2FnZTtcbiAgICB9XG5cbn1cbiJdfQ==