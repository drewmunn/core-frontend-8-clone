/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/index.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { localStorageSync } from 'ngrx-store-localstorage';
import { reducer as SettingsReducer } from './settings/settings.reducer';
import { reducer as NavigationReducer } from './navigation/navigation.reducer';
import { reducer as RouterReducer } from './router/router.reducer';
import { settingsMetaReducer } from './settings/settings.meta';
import { NavigationEffects } from './navigation/navigation.effects';
/**
 * @record
 */
export function AppState() { }
if (false) {
    /** @type {?} */
    AppState.prototype.settings;
    /** @type {?} */
    AppState.prototype.navigation;
    /** @type {?} */
    AppState.prototype.router;
}
/** @type {?} */
export const reducers = {
    settings: SettingsReducer,
    navigation: NavigationReducer,
    router: RouterReducer
};
/**
 * @param {?} reducer
 * @return {?}
 */
export function localStorageSyncReducer(reducer) {
    return localStorageSync({
        keys: ['settings'],
        rehydrate: true,
    })(reducer);
}
/** @type {?} */
export const metaReducers = [
    localStorageSyncReducer,
    settingsMetaReducer
].concat([]);
/** @type {?} */
export const effects = [
    NavigationEffects
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBRUEsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFDLE9BQU8sSUFBSSxlQUFlLEVBQWdCLE1BQU0sNkJBQTZCLENBQUM7QUFDdEYsT0FBTyxFQUFrQixPQUFPLElBQUksaUJBQWlCLEVBQUMsTUFBTSxpQ0FBaUMsQ0FBQztBQUM5RixPQUFPLEVBQUMsT0FBTyxJQUFJLGFBQWEsRUFBaUIsTUFBTSx5QkFBeUIsQ0FBQztBQUNqRixPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUM3RCxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxpQ0FBaUMsQ0FBQzs7OztBQUVsRSw4QkFJQzs7O0lBSEcsNEJBQXdCOztJQUN4Qiw4QkFBNEI7O0lBQzVCLDBCQUEyQzs7O0FBRy9DLE1BQU0sT0FBTyxRQUFRLEdBQStCO0lBQ2hELFFBQVEsRUFBRSxlQUFlO0lBQ3pCLFVBQVUsRUFBRSxpQkFBaUI7SUFDN0IsTUFBTSxFQUFFLGFBQWE7Q0FDeEI7Ozs7O0FBRUQsTUFBTSxVQUFVLHVCQUF1QixDQUFDLE9BQTJCO0lBQy9ELE9BQU8sZ0JBQWdCLENBQUM7UUFDcEIsSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDO1FBQ2xCLFNBQVMsRUFBRSxJQUFJO0tBQ2xCLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUNoQixDQUFDOztBQUVELE1BQU0sT0FBTyxZQUFZLEdBQTRCO0lBQ2pELHVCQUF1QjtJQUN2QixtQkFBbUI7Q0FDdEIsQ0FBQyxNQUFNLENBQ0osRUFBRSxDQUNMOztBQUVELE1BQU0sT0FBTyxPQUFPLEdBQUc7SUFDbkIsaUJBQWlCO0NBQ3BCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBY3Rpb25SZWR1Y2VyLCBBY3Rpb25SZWR1Y2VyTWFwLCBNZXRhUmVkdWNlcn0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHtSb3V0ZXJSZWR1Y2VyU3RhdGV9IGZyb20gJ0BuZ3J4L3JvdXRlci1zdG9yZSc7XG5pbXBvcnQge2xvY2FsU3RvcmFnZVN5bmN9IGZyb20gJ25ncngtc3RvcmUtbG9jYWxzdG9yYWdlJztcbmltcG9ydCB7cmVkdWNlciBhcyBTZXR0aW5nc1JlZHVjZXIsIFNldHRpbmdzU3RhdGV9IGZyb20gJy4vc2V0dGluZ3Mvc2V0dGluZ3MucmVkdWNlcic7XG5pbXBvcnQge05hdmlnYXRpb25TdGF0ZSwgcmVkdWNlciBhcyBOYXZpZ2F0aW9uUmVkdWNlcn0gZnJvbSAnLi9uYXZpZ2F0aW9uL25hdmlnYXRpb24ucmVkdWNlcic7XG5pbXBvcnQge3JlZHVjZXIgYXMgUm91dGVyUmVkdWNlciwgUm91dGVyU3RhdGVVcmx9IGZyb20gJy4vcm91dGVyL3JvdXRlci5yZWR1Y2VyJztcbmltcG9ydCB7c2V0dGluZ3NNZXRhUmVkdWNlcn0gZnJvbSAnLi9zZXR0aW5ncy9zZXR0aW5ncy5tZXRhJztcbmltcG9ydCB7TmF2aWdhdGlvbkVmZmVjdHN9IGZyb20gJy4vbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLmVmZmVjdHMnO1xuXG5leHBvcnQgaW50ZXJmYWNlIEFwcFN0YXRlIHtcbiAgICBzZXR0aW5nczogU2V0dGluZ3NTdGF0ZTtcbiAgICBuYXZpZ2F0aW9uOiBOYXZpZ2F0aW9uU3RhdGU7XG4gICAgcm91dGVyOiBSb3V0ZXJSZWR1Y2VyU3RhdGU8Um91dGVyU3RhdGVVcmw+O1xufVxuXG5leHBvcnQgY29uc3QgcmVkdWNlcnM6IEFjdGlvblJlZHVjZXJNYXA8QXBwU3RhdGU+ID0ge1xuICAgIHNldHRpbmdzOiBTZXR0aW5nc1JlZHVjZXIsXG4gICAgbmF2aWdhdGlvbjogTmF2aWdhdGlvblJlZHVjZXIsXG4gICAgcm91dGVyOiBSb3V0ZXJSZWR1Y2VyXG59O1xuXG5leHBvcnQgZnVuY3Rpb24gbG9jYWxTdG9yYWdlU3luY1JlZHVjZXIocmVkdWNlcjogQWN0aW9uUmVkdWNlcjxhbnk+KTogQWN0aW9uUmVkdWNlcjxhbnk+IHtcbiAgICByZXR1cm4gbG9jYWxTdG9yYWdlU3luYyh7XG4gICAgICAgIGtleXM6IFsnc2V0dGluZ3MnXSxcbiAgICAgICAgcmVoeWRyYXRlOiB0cnVlLFxuICAgIH0pKHJlZHVjZXIpO1xufVxuXG5leHBvcnQgY29uc3QgbWV0YVJlZHVjZXJzOiBNZXRhUmVkdWNlcjxBcHBTdGF0ZT5bXSA9IFtcbiAgICBsb2NhbFN0b3JhZ2VTeW5jUmVkdWNlcixcbiAgICBzZXR0aW5nc01ldGFSZWR1Y2VyXG5dLmNvbmNhdChcbiAgICBbXVxuKTtcblxuZXhwb3J0IGNvbnN0IGVmZmVjdHMgPSBbXG4gICAgTmF2aWdhdGlvbkVmZmVjdHNcbl07XG4iXX0=