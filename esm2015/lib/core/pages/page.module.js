/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/page.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PageComponent } from './page.component';
import { BlockWrapperComponent } from '../blocks/block-wrapper/block-wrapper.component';
import { BlockLanguageComponent } from '../blocks/block-language/block-language.component';
import { BlockParagraphComponent } from '../blocks/block-paragraph/block-paragraph.component';
import { BlockParagraphLeadComponent } from '../blocks/block-paragraph-lead/block-paragraph-lead.component';
import { BlockTableComponent } from '../blocks/block-table/block-table.component';
import { BlockAccordionComponent } from '../blocks/block-accordion/block-accordion.component';
import { BlockH1Component } from '../blocks/block-h1/block-h1.component';
import { BlockH2Component } from '../blocks/block-h2/block-h2.component';
import { BlockH3Component } from '../blocks/block-h3/block-h3.component';
import { BlockH4Component } from '../blocks/block-h4/block-h4.component';
import { BlockH5Component } from '../blocks/block-h5/block-h5.component';
import { BlockH6Component } from '../blocks/block-h6/block-h6.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { BlockComponent } from '../block/block.component';
import { BlockAbstractComponent } from '../blocks/block-abstract.component';
import { BlockImageComponent } from '../blocks/block-image/block-image.component';
import { BlockBreadcrumbsComponent } from '../blocks/block-breadcrumbs/block-breadcrumbs.component';
import { MatListModule } from '@angular/material/list';
import { BlockCarouselComponent } from '../blocks/block-carousel/block-carousel.component';
import { BlockVideoComponent } from '../blocks/block-video/block-video.component';
import { BlockBulletsComponent } from '../blocks/block-bullets/block-bullets.component';
import { BlockNumberedComponent } from '../blocks/block-numbered/block-numbered.component';
import { BlockBlockquoteComponent } from '../blocks/block-blockquote/block-blockquote.component';
import { BlockDownloadComponent } from '../blocks/block-download/block-download.component';
import { BlockButtonComponent } from '../blocks/block-button/block-button.component';
import { BlockDividerComponent } from '../blocks/block-divider/block-divider.component';
import { BlockProgressComponent } from '../blocks/block-progress/block-progress.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { BlockTabsComponent } from '../blocks/block-tabs/block-tabs.component';
import { MatTabsModule } from '@angular/material/tabs';
import { SpinnerModule } from '../utils/spinner/spinner.module';
import { TruncatePipe } from '../utils/pipes/limitto.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlockSearchResultsComponent } from '../blocks/block-search-results/block-search-results.component';
import { BlockSearchComponent } from '../blocks/block-search/block-search.component';
import { BlockCompileDirective } from '../blocks/block-compile.directive';
const ɵ0 = { breadcrumbs: ['Page'] };
export class PageModule {
}
PageModule.decorators = [
    { type: NgModule, args: [{
                exports: [
                    BlockComponent,
                ],
                declarations: [
                    PageComponent,
                    TruncatePipe,
                    BlockComponent,
                    BlockAbstractComponent,
                    BlockWrapperComponent,
                    BlockLanguageComponent,
                    BlockParagraphComponent,
                    BlockParagraphLeadComponent,
                    BlockTableComponent,
                    BlockAccordionComponent,
                    BlockH1Component,
                    BlockH2Component,
                    BlockH3Component,
                    BlockH4Component,
                    BlockH5Component,
                    BlockH6Component,
                    BlockImageComponent,
                    BlockBreadcrumbsComponent,
                    BlockCarouselComponent,
                    BlockVideoComponent,
                    BlockBulletsComponent,
                    BlockNumberedComponent,
                    BlockBlockquoteComponent,
                    BlockDownloadComponent,
                    BlockButtonComponent,
                    BlockDividerComponent,
                    BlockProgressComponent,
                    BlockTabsComponent,
                    BlockSearchResultsComponent,
                    BlockSearchComponent,
                    BlockCompileDirective,
                ],
                imports: [
                    CommonModule,
                    RouterModule.forChild([
                        {
                            path: ':identifier', component: PageComponent,
                            data: ɵ0
                        }
                    ]),
                    MatExpansionModule,
                    MatListModule,
                    MatProgressBarModule,
                    MatTabsModule,
                    SpinnerModule,
                    ReactiveFormsModule,
                    FormsModule
                ],
                entryComponents: [
                    BlockAbstractComponent,
                    BlockWrapperComponent,
                    BlockLanguageComponent,
                    BlockParagraphComponent,
                    BlockParagraphLeadComponent,
                    BlockTableComponent,
                    BlockAccordionComponent,
                    BlockH1Component,
                    BlockH2Component,
                    BlockH3Component,
                    BlockH4Component,
                    BlockH5Component,
                    BlockH6Component,
                    BlockImageComponent,
                    BlockBreadcrumbsComponent,
                    BlockCarouselComponent,
                    BlockVideoComponent,
                    BlockBulletsComponent,
                    BlockNumberedComponent,
                    BlockBlockquoteComponent,
                    BlockDownloadComponent,
                    BlockButtonComponent,
                    BlockDividerComponent,
                    BlockProgressComponent,
                    BlockTabsComponent,
                    BlockSearchResultsComponent,
                    BlockSearchComponent,
                ],
            },] }
];
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3BhZ2VzL3BhZ2UubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxrQkFBa0IsQ0FBQztBQUMvQyxPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSxpREFBaUQsQ0FBQztBQUN0RixPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSxtREFBbUQsQ0FBQztBQUN6RixPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSxxREFBcUQsQ0FBQztBQUM1RixPQUFPLEVBQUMsMkJBQTJCLEVBQUMsTUFBTSwrREFBK0QsQ0FBQztBQUMxRyxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSw2Q0FBNkMsQ0FBQztBQUNoRixPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSxxREFBcUQsQ0FBQztBQUM1RixPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsa0JBQWtCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUMvRCxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDeEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sb0NBQW9DLENBQUM7QUFDMUUsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sNkNBQTZDLENBQUM7QUFDaEYsT0FBTyxFQUFDLHlCQUF5QixFQUFDLE1BQU0seURBQXlELENBQUM7QUFDbEcsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBQ3JELE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLG1EQUFtRCxDQUFDO0FBQ3pGLE9BQU8sRUFBQyxtQkFBbUIsRUFBQyxNQUFNLDZDQUE2QyxDQUFDO0FBQ2hGLE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLGlEQUFpRCxDQUFDO0FBQ3RGLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLG1EQUFtRCxDQUFDO0FBQ3pGLE9BQU8sRUFBQyx3QkFBd0IsRUFBQyxNQUFNLHVEQUF1RCxDQUFDO0FBQy9GLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLG1EQUFtRCxDQUFDO0FBQ3pGLE9BQU8sRUFBQyxvQkFBb0IsRUFBQyxNQUFNLCtDQUErQyxDQUFDO0FBQ25GLE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLGlEQUFpRCxDQUFDO0FBQ3RGLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLG1EQUFtRCxDQUFDO0FBQ3pGLE9BQU8sRUFBQyxvQkFBb0IsRUFBQyxNQUFNLGdDQUFnQyxDQUFDO0FBQ3BFLE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLDJDQUEyQyxDQUFDO0FBQzdFLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUNyRCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0saUNBQWlDLENBQUM7QUFDOUQsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBQ3pELE9BQU8sRUFBQyxXQUFXLEVBQUUsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNoRSxPQUFPLEVBQUMsMkJBQTJCLEVBQUMsTUFBTSwrREFBK0QsQ0FBQztBQUMxRyxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSwrQ0FBK0MsQ0FBQztBQUNuRixPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSxtQ0FBbUMsQ0FBQztXQTRDbEQsRUFBQyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUMsRUFBQztBQXlDN0MsTUFBTSxPQUFPLFVBQVU7OztZQW5GdEIsUUFBUSxTQUFDO2dCQUNOLE9BQU8sRUFBRTtvQkFDTCxjQUFjO2lCQUNqQjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1YsYUFBYTtvQkFDYixZQUFZO29CQUNaLGNBQWM7b0JBQ2Qsc0JBQXNCO29CQUN0QixxQkFBcUI7b0JBQ3JCLHNCQUFzQjtvQkFDdEIsdUJBQXVCO29CQUN2QiwyQkFBMkI7b0JBQzNCLG1CQUFtQjtvQkFDbkIsdUJBQXVCO29CQUN2QixnQkFBZ0I7b0JBQ2hCLGdCQUFnQjtvQkFDaEIsZ0JBQWdCO29CQUNoQixnQkFBZ0I7b0JBQ2hCLGdCQUFnQjtvQkFDaEIsZ0JBQWdCO29CQUNoQixtQkFBbUI7b0JBQ25CLHlCQUF5QjtvQkFDekIsc0JBQXNCO29CQUN0QixtQkFBbUI7b0JBQ25CLHFCQUFxQjtvQkFDckIsc0JBQXNCO29CQUN0Qix3QkFBd0I7b0JBQ3hCLHNCQUFzQjtvQkFDdEIsb0JBQW9CO29CQUNwQixxQkFBcUI7b0JBQ3JCLHNCQUFzQjtvQkFDdEIsa0JBQWtCO29CQUNsQiwyQkFBMkI7b0JBQzNCLG9CQUFvQjtvQkFDcEIscUJBQXFCO2lCQUN4QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ0wsWUFBWTtvQkFDWixZQUFZLENBQUMsUUFBUSxDQUFDO3dCQUNsQjs0QkFDSSxJQUFJLEVBQUUsYUFBYSxFQUFFLFNBQVMsRUFBRSxhQUFhOzRCQUM3QyxJQUFJLElBQXlCO3lCQUNoQztxQkFDSixDQUFDO29CQUNGLGtCQUFrQjtvQkFDbEIsYUFBYTtvQkFDYixvQkFBb0I7b0JBQ3BCLGFBQWE7b0JBQ2IsYUFBYTtvQkFDYixtQkFBbUI7b0JBQ25CLFdBQVc7aUJBQ2Q7Z0JBQ0QsZUFBZSxFQUFFO29CQUNiLHNCQUFzQjtvQkFDdEIscUJBQXFCO29CQUNyQixzQkFBc0I7b0JBQ3RCLHVCQUF1QjtvQkFDdkIsMkJBQTJCO29CQUMzQixtQkFBbUI7b0JBQ25CLHVCQUF1QjtvQkFDdkIsZ0JBQWdCO29CQUNoQixnQkFBZ0I7b0JBQ2hCLGdCQUFnQjtvQkFDaEIsZ0JBQWdCO29CQUNoQixnQkFBZ0I7b0JBQ2hCLGdCQUFnQjtvQkFDaEIsbUJBQW1CO29CQUNuQix5QkFBeUI7b0JBQ3pCLHNCQUFzQjtvQkFDdEIsbUJBQW1CO29CQUNuQixxQkFBcUI7b0JBQ3JCLHNCQUFzQjtvQkFDdEIsd0JBQXdCO29CQUN4QixzQkFBc0I7b0JBQ3RCLG9CQUFvQjtvQkFDcEIscUJBQXFCO29CQUNyQixzQkFBc0I7b0JBQ3RCLGtCQUFrQjtvQkFDbEIsMkJBQTJCO29CQUMzQixvQkFBb0I7aUJBQ3ZCO2FBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7Um91dGVyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQge1BhZ2VDb21wb25lbnR9IGZyb20gJy4vcGFnZS5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrV3JhcHBlckNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXdyYXBwZXIvYmxvY2std3JhcHBlci5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrTGFuZ3VhZ2VDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1sYW5ndWFnZS9ibG9jay1sYW5ndWFnZS5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrUGFyYWdyYXBoQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stcGFyYWdyYXBoL2Jsb2NrLXBhcmFncmFwaC5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrUGFyYWdyYXBoTGVhZENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXBhcmFncmFwaC1sZWFkL2Jsb2NrLXBhcmFncmFwaC1sZWFkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QmxvY2tUYWJsZUNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXRhYmxlL2Jsb2NrLXRhYmxlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QmxvY2tBY2NvcmRpb25Db21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1hY2NvcmRpb24vYmxvY2stYWNjb3JkaW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QmxvY2tIMUNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWgxL2Jsb2NrLWgxLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QmxvY2tIMkNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWgyL2Jsb2NrLWgyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QmxvY2tIM0NvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWgzL2Jsb2NrLWgzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QmxvY2tINENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWg0L2Jsb2NrLWg0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QmxvY2tINUNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWg1L2Jsb2NrLWg1LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QmxvY2tINkNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWg2L2Jsb2NrLWg2LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7TWF0RXhwYW5zaW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9leHBhbnNpb24nO1xyXG5pbXBvcnQge0Jsb2NrQ29tcG9uZW50fSBmcm9tICcuLi9ibG9jay9ibG9jay5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrQWJzdHJhY3RDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrSW1hZ2VDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1pbWFnZS9ibG9jay1pbWFnZS5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrQnJlYWRjcnVtYnNDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1icmVhZGNydW1icy9ibG9jay1icmVhZGNydW1icy5jb21wb25lbnQnO1xyXG5pbXBvcnQge01hdExpc3RNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2xpc3QnO1xyXG5pbXBvcnQge0Jsb2NrQ2Fyb3VzZWxDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1jYXJvdXNlbC9ibG9jay1jYXJvdXNlbC5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrVmlkZW9Db21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay12aWRlby9ibG9jay12aWRlby5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrQnVsbGV0c0NvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWJ1bGxldHMvYmxvY2stYnVsbGV0cy5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrTnVtYmVyZWRDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1udW1iZXJlZC9ibG9jay1udW1iZXJlZC5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrQmxvY2txdW90ZUNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWJsb2NrcXVvdGUvYmxvY2stYmxvY2txdW90ZS5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrRG93bmxvYWRDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1kb3dubG9hZC9ibG9jay1kb3dubG9hZC5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrQnV0dG9uQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stYnV0dG9uL2Jsb2NrLWJ1dHRvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrRGl2aWRlckNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWRpdmlkZXIvYmxvY2stZGl2aWRlci5jb21wb25lbnQnO1xyXG5pbXBvcnQge0Jsb2NrUHJvZ3Jlc3NDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1wcm9ncmVzcy9ibG9jay1wcm9ncmVzcy5jb21wb25lbnQnO1xyXG5pbXBvcnQge01hdFByb2dyZXNzQmFyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9wcm9ncmVzcy1iYXInO1xyXG5pbXBvcnQge0Jsb2NrVGFic0NvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXRhYnMvYmxvY2stdGFicy5jb21wb25lbnQnO1xyXG5pbXBvcnQge01hdFRhYnNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RhYnMnO1xyXG5pbXBvcnQge1NwaW5uZXJNb2R1bGV9IGZyb20gJy4uL3V0aWxzL3NwaW5uZXIvc3Bpbm5lci5tb2R1bGUnO1xyXG5pbXBvcnQge1RydW5jYXRlUGlwZX0gZnJvbSAnLi4vdXRpbHMvcGlwZXMvbGltaXR0by5waXBlJztcclxuaW1wb3J0IHtGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQge0Jsb2NrU2VhcmNoUmVzdWx0c0NvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXNlYXJjaC1yZXN1bHRzL2Jsb2NrLXNlYXJjaC1yZXN1bHRzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QmxvY2tTZWFyY2hDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1zZWFyY2gvYmxvY2stc2VhcmNoLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7QmxvY2tDb21waWxlRGlyZWN0aXZlfSBmcm9tICcuLi9ibG9ja3MvYmxvY2stY29tcGlsZS5kaXJlY3RpdmUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBCbG9ja0NvbXBvbmVudCxcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBQYWdlQ29tcG9uZW50LFxyXG4gICAgICAgIFRydW5jYXRlUGlwZSxcclxuICAgICAgICBCbG9ja0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0Fic3RyYWN0Q29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrV3JhcHBlckNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0xhbmd1YWdlQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrUGFyYWdyYXBoQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrUGFyYWdyYXBoTGVhZENvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1RhYmxlQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrQWNjb3JkaW9uQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrSDFDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tIMkNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0gzQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrSDRDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tINUNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0g2Q29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrSW1hZ2VDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tCcmVhZGNydW1ic0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0Nhcm91c2VsQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrVmlkZW9Db21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tCdWxsZXRzQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrTnVtYmVyZWRDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tCbG9ja3F1b3RlQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrRG93bmxvYWRDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tCdXR0b25Db21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tEaXZpZGVyQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrUHJvZ3Jlc3NDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tUYWJzQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrU2VhcmNoUmVzdWx0c0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1NlYXJjaENvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0NvbXBpbGVEaXJlY3RpdmUsXHJcbiAgICBdLFxyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQoW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBwYXRoOiAnOmlkZW50aWZpZXInLCBjb21wb25lbnQ6IFBhZ2VDb21wb25lbnQsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7YnJlYWRjcnVtYnM6IFsnUGFnZSddfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXSksXHJcbiAgICAgICAgTWF0RXhwYW5zaW9uTW9kdWxlLFxyXG4gICAgICAgIE1hdExpc3RNb2R1bGUsXHJcbiAgICAgICAgTWF0UHJvZ3Jlc3NCYXJNb2R1bGUsXHJcbiAgICAgICAgTWF0VGFic01vZHVsZSxcclxuICAgICAgICBTcGlubmVyTW9kdWxlLFxyXG4gICAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgRm9ybXNNb2R1bGVcclxuICAgIF0sXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtcclxuICAgICAgICBCbG9ja0Fic3RyYWN0Q29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrV3JhcHBlckNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0xhbmd1YWdlQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrUGFyYWdyYXBoQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrUGFyYWdyYXBoTGVhZENvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1RhYmxlQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrQWNjb3JkaW9uQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrSDFDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tIMkNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0gzQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrSDRDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tINUNvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0g2Q29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrSW1hZ2VDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tCcmVhZGNydW1ic0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja0Nhcm91c2VsQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrVmlkZW9Db21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tCdWxsZXRzQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrTnVtYmVyZWRDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tCbG9ja3F1b3RlQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrRG93bmxvYWRDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tCdXR0b25Db21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tEaXZpZGVyQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrUHJvZ3Jlc3NDb21wb25lbnQsXHJcbiAgICAgICAgQmxvY2tUYWJzQ29tcG9uZW50LFxyXG4gICAgICAgIEJsb2NrU2VhcmNoUmVzdWx0c0NvbXBvbmVudCxcclxuICAgICAgICBCbG9ja1NlYXJjaENvbXBvbmVudCxcclxuICAgIF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQYWdlTW9kdWxlIHtcclxufVxyXG4iXX0=