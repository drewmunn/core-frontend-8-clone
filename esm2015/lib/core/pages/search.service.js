/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/search.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { PageService } from './page.service';
import { SpinnerService } from '../utils/spinner/spinner.service';
import { TranslateService } from './translate.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common/http";
import * as i3 from "../utils/spinner/spinner.service";
import * as i4 from "./translate.service";
/** @type {?} */
const EXCERPT_SIZE = 500;
export class SearchService extends PageService {
    /**
     * @param {?} router
     * @param {?} api
     * @param {?} spinner
     * @param {?} translate
     */
    constructor(router, api, spinner, translate) {
        super(router, api);
        this.router = router;
        this.api = api;
        this.spinner = spinner;
        this.translate = translate;
    }
    /**
     * @return {?}
     */
    refresh() {
        this.limit = 10;
        this.offset = 0;
        this.results = [];
        this.eddie = false;
        this.totalResults = 0;
    }
    /**
     * @param {?} query
     * @return {?}
     */
    setQuery(query) {
        this.query = query;
    }
    /**
     * @return {?}
     */
    getQuery() {
        return this.query;
    }
    /**
     * @param {?} query
     * @return {?}
     */
    performSearch(query) {
        this.refresh();
        this.setQuery(query);
        this.spinner.show('search');
        return this.getOneThroughCache('search', null, {}, false, {
            q: query,
            limit: this.limit,
            offset: this.offset,
            excerpt: EXCERPT_SIZE
        }).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            if (response.response.docs) {
                /** @type {?} */
                const searchTermRegex = new RegExp(query, 'gi');
                this.results = [...response.response.docs];
                this.totalResults = response.response.numFound;
                for (const result of this.results) {
                    if (result.content) {
                        result.content = result.content[0].substring(0, 1000).replace(searchTermRegex, '<em>$&</em>');
                        if (response.highlighting[result.uniqueId].content) {
                            result.content = response.highlighting[result.uniqueId].content[0];
                        }
                        else if (response.highlighting[result.uniqueId].text && response.highlighting[result.uniqueId].text[0]) {
                            result.content = response.highlighting[result.uniqueId].text[0];
                        }
                        result.content = result.content.replace(/\|\|/g, ', ').replace(/\ufffd/g, ' ') + '&hellip;';
                        if (result.categories) {
                            result.category = result.categories[0];
                        }
                        // result.name = LanguageService.getPageSpecific('name', results);
                        // result.url = LanguageService.getPageSpecific('url', results);
                        // result.description = LanguageService.getPageSpecific('description', results);
                    }
                }
            }
        }));
    }
    /**
     * @return {?}
     */
    getTotalResults() {
        return this.totalResults;
    }
}
SearchService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchService.ctorParameters = () => [
    { type: Router },
    { type: HttpClient },
    { type: SpinnerService },
    { type: TranslateService }
];
/** @nocollapse */ SearchService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SearchService_Factory() { return new SearchService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.HttpClient), i0.ɵɵinject(i3.SpinnerService), i0.ɵɵinject(i4.TranslateService)); }, token: SearchService, providedIn: "root" });
if (false) {
    /** @type {?} */
    SearchService.prototype.query;
    /** @type {?} */
    SearchService.prototype.limit;
    /** @type {?} */
    SearchService.prototype.offset;
    /** @type {?} */
    SearchService.prototype.results;
    /** @type {?} */
    SearchService.prototype.totalResults;
    /** @type {?} */
    SearchService.prototype.eddie;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.api;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.spinner;
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3BhZ2VzL3NlYXJjaC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsTUFBTSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDdkMsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ2hELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sa0NBQWtDLENBQUM7QUFDaEUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0scUJBQXFCLENBQUM7Ozs7Ozs7TUFFL0MsWUFBWSxHQUFHLEdBQUc7QUFNeEIsTUFBTSxPQUFPLGFBQWMsU0FBUSxXQUFXOzs7Ozs7O0lBWTFDLFlBQ2MsTUFBYyxFQUNkLEdBQWUsRUFDZixPQUF1QixFQUN2QixTQUEyQjtRQUVyQyxLQUFLLENBQ0QsTUFBTSxFQUNOLEdBQUcsQ0FDTixDQUFDO1FBUlEsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLFFBQUcsR0FBSCxHQUFHLENBQVk7UUFDZixZQUFPLEdBQVAsT0FBTyxDQUFnQjtRQUN2QixjQUFTLEdBQVQsU0FBUyxDQUFrQjtJQU16QyxDQUFDOzs7O0lBRUQsT0FBTztRQUNILElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLEtBQWE7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsS0FBYTtRQUN2QixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDZixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRTVCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRTtZQUN0RCxDQUFDLEVBQUUsS0FBSztZQUNSLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDbkIsT0FBTyxFQUFFLFlBQVk7U0FDeEIsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNwQixJQUFJLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFOztzQkFDbEIsZUFBZSxHQUFHLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUM7Z0JBQy9DLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzNDLElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7Z0JBQy9DLEtBQUssTUFBTSxNQUFNLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtvQkFDL0IsSUFBSSxNQUFNLENBQUMsT0FBTyxFQUFFO3dCQUNoQixNQUFNLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFLGFBQWEsQ0FBQyxDQUFDO3dCQUM5RixJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sRUFBRTs0QkFDaEQsTUFBTSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQ3RFOzZCQUFNLElBQUksUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRTs0QkFDdEcsTUFBTSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQ25FO3dCQUNELE1BQU0sQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLEdBQUcsVUFBVSxDQUFDO3dCQUM1RixJQUFJLE1BQU0sQ0FBQyxVQUFVLEVBQUU7NEJBQ25CLE1BQU0sQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQzt5QkFDMUM7d0JBQ0Qsa0VBQWtFO3dCQUNsRSxnRUFBZ0U7d0JBQ2hFLGdGQUFnRjtxQkFDbkY7aUJBQ0o7YUFDSjtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELGVBQWU7UUFDWCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDN0IsQ0FBQzs7O1lBbEZKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQVZPLE1BQU07WUFDTixVQUFVO1lBRVYsY0FBYztZQUNkLGdCQUFnQjs7Ozs7SUFVcEIsOEJBQWM7O0lBRWQsOEJBQWM7O0lBQ2QsK0JBQWU7O0lBRWYsZ0NBQWU7O0lBQ2YscUNBQXFCOztJQUVyQiw4QkFBVzs7Ozs7SUFHUCwrQkFBd0I7Ozs7O0lBQ3hCLDRCQUF5Qjs7Ozs7SUFDekIsZ0NBQWlDOzs7OztJQUNqQyxrQ0FBcUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQge0h0dHBDbGllbnR9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7UGFnZVNlcnZpY2V9IGZyb20gJy4vcGFnZS5zZXJ2aWNlJztcbmltcG9ydCB7U3Bpbm5lclNlcnZpY2V9IGZyb20gJy4uL3V0aWxzL3NwaW5uZXIvc3Bpbm5lci5zZXJ2aWNlJztcbmltcG9ydCB7VHJhbnNsYXRlU2VydmljZX0gZnJvbSAnLi90cmFuc2xhdGUuc2VydmljZSc7XG5cbmNvbnN0IEVYQ0VSUFRfU0laRSA9IDUwMDtcblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcblxuZXhwb3J0IGNsYXNzIFNlYXJjaFNlcnZpY2UgZXh0ZW5kcyBQYWdlU2VydmljZSB7XG5cbiAgICBxdWVyeTogc3RyaW5nO1xuXG4gICAgbGltaXQ6IG51bWJlcjtcbiAgICBvZmZzZXQ6IG51bWJlcjtcblxuICAgIHJlc3VsdHM6IGFueVtdO1xuICAgIHRvdGFsUmVzdWx0czogbnVtYmVyO1xuXG4gICAgZWRkaWU6IGFueTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgICAgIHByb3RlY3RlZCBhcGk6IEh0dHBDbGllbnQsXG4gICAgICAgIHByb3RlY3RlZCBzcGlubmVyOiBTcGlubmVyU2VydmljZSxcbiAgICAgICAgcHJvdGVjdGVkIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICByb3V0ZXIsXG4gICAgICAgICAgICBhcGksXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcmVmcmVzaCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5saW1pdCA9IDEwO1xuICAgICAgICB0aGlzLm9mZnNldCA9IDA7XG4gICAgICAgIHRoaXMucmVzdWx0cyA9IFtdO1xuICAgICAgICB0aGlzLmVkZGllID0gZmFsc2U7XG4gICAgICAgIHRoaXMudG90YWxSZXN1bHRzID0gMDtcbiAgICB9XG5cbiAgICBzZXRRdWVyeShxdWVyeTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMucXVlcnkgPSBxdWVyeTtcbiAgICB9XG5cbiAgICBnZXRRdWVyeSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucXVlcnk7XG4gICAgfVxuXG4gICAgcGVyZm9ybVNlYXJjaChxdWVyeTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMucmVmcmVzaCgpO1xuICAgICAgICB0aGlzLnNldFF1ZXJ5KHF1ZXJ5KTtcbiAgICAgICAgdGhpcy5zcGlubmVyLnNob3coJ3NlYXJjaCcpO1xuXG4gICAgICAgIHJldHVybiB0aGlzLmdldE9uZVRocm91Z2hDYWNoZSgnc2VhcmNoJywgbnVsbCwge30sIGZhbHNlLCB7XG4gICAgICAgICAgICBxOiBxdWVyeSxcbiAgICAgICAgICAgIGxpbWl0OiB0aGlzLmxpbWl0LFxuICAgICAgICAgICAgb2Zmc2V0OiB0aGlzLm9mZnNldCxcbiAgICAgICAgICAgIGV4Y2VycHQ6IEVYQ0VSUFRfU0laRVxuICAgICAgICB9KS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnJlc3BvbnNlLmRvY3MpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBzZWFyY2hUZXJtUmVnZXggPSBuZXcgUmVnRXhwKHF1ZXJ5LCAnZ2knKTtcbiAgICAgICAgICAgICAgICB0aGlzLnJlc3VsdHMgPSBbLi4ucmVzcG9uc2UucmVzcG9uc2UuZG9jc107XG4gICAgICAgICAgICAgICAgdGhpcy50b3RhbFJlc3VsdHMgPSByZXNwb25zZS5yZXNwb25zZS5udW1Gb3VuZDtcbiAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IHJlc3VsdCBvZiB0aGlzLnJlc3VsdHMpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdC5jb250ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuY29udGVudCA9IHJlc3VsdC5jb250ZW50WzBdLnN1YnN0cmluZygwLCAxMDAwKS5yZXBsYWNlKHNlYXJjaFRlcm1SZWdleCwgJzxlbT4kJjwvZW0+Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuaGlnaGxpZ2h0aW5nW3Jlc3VsdC51bmlxdWVJZF0uY29udGVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5jb250ZW50ID0gcmVzcG9uc2UuaGlnaGxpZ2h0aW5nW3Jlc3VsdC51bmlxdWVJZF0uY29udGVudFswXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UuaGlnaGxpZ2h0aW5nW3Jlc3VsdC51bmlxdWVJZF0udGV4dCAmJiByZXNwb25zZS5oaWdobGlnaHRpbmdbcmVzdWx0LnVuaXF1ZUlkXS50ZXh0WzBdKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbnRlbnQgPSByZXNwb25zZS5oaWdobGlnaHRpbmdbcmVzdWx0LnVuaXF1ZUlkXS50ZXh0WzBdO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbnRlbnQgPSByZXN1bHQuY29udGVudC5yZXBsYWNlKC9cXHxcXHwvZywgJywgJykucmVwbGFjZSgvXFx1ZmZmZC9nLCAnICcpICsgJyZoZWxsaXA7JztcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQuY2F0ZWdvcmllcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5jYXRlZ29yeSA9IHJlc3VsdC5jYXRlZ29yaWVzWzBdO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gcmVzdWx0Lm5hbWUgPSBMYW5ndWFnZVNlcnZpY2UuZ2V0UGFnZVNwZWNpZmljKCduYW1lJywgcmVzdWx0cyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyByZXN1bHQudXJsID0gTGFuZ3VhZ2VTZXJ2aWNlLmdldFBhZ2VTcGVjaWZpYygndXJsJywgcmVzdWx0cyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyByZXN1bHQuZGVzY3JpcHRpb24gPSBMYW5ndWFnZVNlcnZpY2UuZ2V0UGFnZVNwZWNpZmljKCdkZXNjcmlwdGlvbicsIHJlc3VsdHMpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBnZXRUb3RhbFJlc3VsdHMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnRvdGFsUmVzdWx0cztcbiAgICB9XG5cbn1cbiJdfQ==