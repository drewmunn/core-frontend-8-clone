/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/page.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { APP_CONFIG } from '../../app.config';
import { PageService } from './page.service';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
export class PageComponent {
    /**
     * @param {?} route
     * @param {?} router
     * @param {?} pages
     */
    constructor(route, router, pages) {
        this.route = route;
        this.router = router;
        this.pages = pages;
        this.appName = APP_CONFIG.appName;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.route.paramMap.subscribe((/**
         * @param {?} params
         * @return {?}
         */
        params => {
            this.subscription = this.pages.getPage(params.get('identifier') || 'default').pipe(map((/**
             * @param {?} page
             * @return {?}
             */
            (page) => {
                this.pages.setCurrentPage(page);
                this.page$ = of(page);
            }))).subscribe();
        }));
    }
    /**
     * @param {?} idx
     * @param {?} item
     * @return {?}
     */
    trackByFn(idx, item) {
        return item.uid + '_' + idx;
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        console.log('Page destroyed');
        this.subscription.unsubscribe();
    }
}
PageComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-home',
                template: "<div *ngIf=\"page$ | async as page\">\n    <core-block *ngFor=\"let block of page.widgets trackBy: trackByFn\" [block]=\"block\" class=\"core-block-l1\"></core-block>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.Default
            }] }
];
/** @nocollapse */
PageComponent.ctorParameters = () => [
    { type: ActivatedRoute },
    { type: Router },
    { type: PageService }
];
if (false) {
    /** @type {?} */
    PageComponent.prototype.appName;
    /** @type {?} */
    PageComponent.prototype.page$;
    /** @type {?} */
    PageComponent.prototype.subscription;
    /**
     * @type {?}
     * @private
     */
    PageComponent.prototype.route;
    /**
     * @type {?}
     * @private
     */
    PageComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    PageComponent.prototype.pages;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3BhZ2VzL3BhZ2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLHVCQUF1QixFQUFFLFNBQVMsRUFBb0IsTUFBTSxlQUFlLENBQUM7QUFDcEYsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGtCQUFrQixDQUFDO0FBQzVDLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQWEsRUFBRSxFQUFDLE1BQU0sTUFBTSxDQUFDO0FBQ3BDLE9BQU8sRUFBQyxjQUFjLEVBQUUsTUFBTSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDdkQsT0FBTyxFQUFDLEdBQUcsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBUW5DLE1BQU0sT0FBTyxhQUFhOzs7Ozs7SUFNdEIsWUFDWSxLQUFxQixFQUNyQixNQUFjLEVBQ2QsS0FBa0I7UUFGbEIsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFDckIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLFVBQUssR0FBTCxLQUFLLENBQWE7UUFSOUIsWUFBTyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7SUFVN0IsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUU7WUFDbkMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FDOUUsR0FBRzs7OztZQUFDLENBQUMsSUFBb0MsRUFBRSxFQUFFO2dCQUN6QyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUIsQ0FBQyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUN4QixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVELFNBQVMsQ0FBQyxHQUFXLEVBQUUsSUFBUztRQUM1QixPQUFPLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQztJQUNoQyxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3BDLENBQUM7OztZQXBDSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLDRMQUFvQztnQkFDcEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE9BQU87YUFDbkQ7Ozs7WUFQTyxjQUFjO1lBQUUsTUFBTTtZQUZ0QixXQUFXOzs7O0lBWWYsZ0NBQTZCOztJQUU3Qiw4QkFBa0Q7O0lBQ2xELHFDQUFrQjs7Ozs7SUFHZCw4QkFBNkI7Ozs7O0lBQzdCLCtCQUFzQjs7Ozs7SUFDdEIsOEJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSwgQ29tcG9uZW50LCBPbkRlc3Ryb3ksIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7QVBQX0NPTkZJR30gZnJvbSAnLi4vLi4vYXBwLmNvbmZpZyc7XHJcbmltcG9ydCB7UGFnZVNlcnZpY2V9IGZyb20gJy4vcGFnZS5zZXJ2aWNlJztcclxuaW1wb3J0IHtPYnNlcnZhYmxlLCBvZn0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7QWN0aXZhdGVkUm91dGUsIFJvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHttYXB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdjb3JlLWhvbWUnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3BhZ2UuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5EZWZhdWx0XHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUGFnZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIGFwcE5hbWUgPSBBUFBfQ09ORklHLmFwcE5hbWU7XHJcblxyXG4gICAgcGFnZSQ6IE9ic2VydmFibGU8eyBuYW1lOyBkZXNjcmlwdGlvbjsgd2lkZ2V0cyB9PjtcclxuICAgIHN1YnNjcmlwdGlvbjogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBwYWdlczogUGFnZVNlcnZpY2UsXHJcbiAgICApIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnJvdXRlLnBhcmFtTWFwLnN1YnNjcmliZShwYXJhbXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbiA9IHRoaXMucGFnZXMuZ2V0UGFnZShwYXJhbXMuZ2V0KCdpZGVudGlmaWVyJykgfHwgJ2RlZmF1bHQnKS5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKChwYWdlOiB7IG5hbWUsIGRlc2NyaXB0aW9uLCB3aWRnZXRzIH0pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnBhZ2VzLnNldEN1cnJlbnRQYWdlKHBhZ2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucGFnZSQgPSBvZihwYWdlKTtcclxuICAgICAgICAgICAgICAgIH0pKS5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB0cmFja0J5Rm4oaWR4OiBudW1iZXIsIGl0ZW06IGFueSkge1xyXG4gICAgICAgIHJldHVybiBpdGVtLnVpZCArICdfJyArIGlkeDtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnUGFnZSBkZXN0cm95ZWQnKTtcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=