/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/page.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { CacheService } from '../cache.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs';
import * as _ from 'lodash';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common/http";
export class PageService extends CacheService {
    /**
     * @param {?} router
     * @param {?} api
     */
    constructor(router, api) {
        super(router, api);
        this.router = router;
        this.api = api;
    }
    /**
     * @return {?}
     */
    refresh() {
        super.refresh();
        this.page = false;
        this.breadcrumbs = {};
    }
    /**
     * @param {?} page
     * @return {?}
     */
    setCurrentPage(page) {
        this.page = page;
    }
    /**
     * @return {?}
     */
    getCurrentPage() {
        return this.page;
    }
    /**
     * @param {?=} headers
     * @return {?}
     */
    getAll(headers) {
        return this.getListThroughCache('pages', {}, headers || {});
    }
    /**
     * @param {?} identifier
     * @param {?=} blockInteraction
     * @param {?=} headers
     * @return {?}
     */
    getPage(identifier, blockInteraction, headers) {
        /** @type {?} */
        const cacheKey = this.createCacheKey(['page', identifier, headers]);
        if (!blockInteraction && this.objectLoadObservables$[cacheKey]) {
            this.api.get('page/' + identifier, { headers: headers || {} }).subscribe(); // Trigger interaction
            return this.objectLoadObservables$[cacheKey];
        }
        else if (blockInteraction) {
            headers = Object.assign({}, (headers || {}), { 'x-service-info': JSON.stringify({ blockInteraction: true }) });
        }
        this.objectLoadObservables$[cacheKey] = this.getOneThroughCache('page', identifier, headers, cacheKey);
        return this.objectLoadObservables$[cacheKey];
    }
    /**
     * @param {?=} headers
     * @return {?}
     */
    getPages(headers) {
        return this.getListThroughCache('pages', {}, headers || {});
    }
    /**
     * @param {?} blockId
     * @return {?}
     */
    getBlock(blockId) {
        try {
            /** @type {?} */
            const observables = [];
            if (this.getFromStorage('block:' + blockId) !== false) {
                /** @type {?} */
                const block = JSON.parse((/** @type {?} */ (this.getFromStorage('block:' + blockId))));
                if (!block.timestamp) {
                    return this.loadBlock(blockId);
                }
                if (!this.cacheProbeTimestamp.blocks) {
                    this.cacheProbePromises$.blocks = this.api.get('cache-probe', { params: { q: 'widget' } }).pipe(map((/**
                     * @param {?} response
                     * @return {?}
                     */
                    (response) => {
                        this.cacheProbeTimestamp.blocks = (/** @type {?} */ (response));
                    })));
                    observables.push(this.cacheProbePromises$.blocks);
                }
                return forkJoin(observables).toPromise().then((/**
                 * @param {?} response
                 * @return {?}
                 */
                response => {
                    if (!this.cacheProbeTimestamp.blocks || block.timestamp < this.cacheProbeTimestamp.blocks) {
                        return this.loadBlock(blockId);
                    }
                    return block;
                }));
            }
        }
        catch (err) {
        }
        return this.loadBlock(blockId);
    }
    /**
     * @param {?} blockId
     * @return {?}
     */
    loadBlock(blockId) {
        if (this.objectLoadObservables$['block:' + blockId]) {
            return this.objectLoadObservables$['block:' + blockId];
        }
        this.objectLoadObservables$['block:' + blockId] = this.api.get('widget/' + blockId)
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            /** @type {?} */
            const block = response;
            try {
                localStorage.setItem('block:' + blockId, JSON.stringify(block));
            }
            catch (e) {
            }
            try {
                sessionStorage.setItem('block:' + blockId, JSON.stringify(block));
            }
            catch (e) {
            }
            return block;
        }))).toPromise();
        return this.objectLoadObservables$['block:' + blockId];
    }
    /**
     * @param {?} blocks
     * @return {?}
     */
    sortBlocks(blocks) {
        blocks.content.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        (a, b) => (a.data.row < b.data.row) ? 1 : -1))
            .sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        (a, b) => (a.data.col < b.data.col) ? 1 : -1));
        for (const subBlocks of blocks.content) {
            this.sortBlocks(subBlocks);
        }
    }
    /**
     * @param {?} requestedPageId
     * @return {?}
     */
    getBreadcrumbs(requestedPageId) {
        return this.getPages().pipe(map((/**
         * @param {?} pages
         * @return {?}
         */
        (pages) => {
            if (_.isEmpty(this.breadcrumbs)) {
                /** @type {?} */
                const extractIds = (/**
                 * @param {?} page
                 * @param {?} parents
                 * @return {?}
                 */
                (page, parents) => {
                    /** @type {?} */
                    const pageMin = _.omit(page, ['children', 'createdBy', 'dateCreated', 'displayOrder', 'groups',
                        'hidden', 'lastUpdated', 'permissions', 'tags', 'widgets']);
                    pageMin.parents = parents;
                    this.breadcrumbs[page.id] = page;
                    for (const child of page.children) {
                        extractIds(child, parents.concat([pageMin]));
                    }
                });
                for (const page of pages) {
                    extractIds(page, []);
                }
            }
            return this.breadcrumbs[requestedPageId] ? this.breadcrumbs[requestedPageId] : [];
        })));
    }
    /**
     * @param {?} query
     * @return {?}
     */
    performSearch(query) {
        console.log(query);
    }
}
PageService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PageService.ctorParameters = () => [
    { type: Router },
    { type: HttpClient }
];
/** @nocollapse */ PageService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PageService_Factory() { return new PageService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.HttpClient)); }, token: PageService, providedIn: "root" });
if (false) {
    /** @type {?} */
    PageService.prototype.page;
    /** @type {?} */
    PageService.prototype.breadcrumbs;
    /**
     * @type {?}
     * @protected
     */
    PageService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    PageService.prototype.api;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9wYWdlcy9wYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxrQkFBa0IsQ0FBQztBQUM5QyxPQUFPLEVBQUMsR0FBRyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbkMsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNoRCxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sTUFBTSxDQUFDO0FBQzlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDOzs7O0FBTTVCLE1BQU0sT0FBTyxXQUFZLFNBQVEsWUFBWTs7Ozs7SUFLekMsWUFDYyxNQUFjLEVBQ2QsR0FBZTtRQUV6QixLQUFLLENBQ0QsTUFBTSxFQUNOLEdBQUcsQ0FDTixDQUFDO1FBTlEsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLFFBQUcsR0FBSCxHQUFHLENBQVk7SUFNN0IsQ0FBQzs7OztJQUVELE9BQU87UUFDSCxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7UUFDbEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsSUFBUztRQUNwQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUNyQixDQUFDOzs7O0lBRUQsY0FBYztRQUNWLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxPQUFZO1FBQ2YsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLEVBQUUsRUFBRSxPQUFPLElBQUksRUFBRSxDQUFDLENBQUM7SUFDaEUsQ0FBQzs7Ozs7OztJQUVELE9BQU8sQ0FBQyxVQUFVLEVBQUUsZ0JBQTBCLEVBQUUsT0FBWTs7Y0FDbEQsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxNQUFNLEVBQUUsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDNUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLFVBQVUsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLElBQUksRUFBRSxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLHNCQUFzQjtZQUNoRyxPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNoRDthQUFNLElBQUksZ0JBQWdCLEVBQUU7WUFDekIsT0FBTyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFDLGdCQUFnQixFQUFFLElBQUksRUFBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDO1NBQzlHO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN2RyxPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNqRCxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxPQUFZO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxFQUFFLEVBQUUsT0FBTyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLE9BQU87UUFDWixJQUFJOztrQkFDTSxXQUFXLEdBQUcsRUFBRTtZQUN0QixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxLQUFLLEtBQUssRUFBRTs7c0JBQzdDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFBLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxFQUFVLENBQUM7Z0JBQzNFLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFO29CQUNsQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ2xDO2dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFO29CQUNsQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxFQUFDLE1BQU0sRUFBRSxFQUFDLENBQUMsRUFBRSxRQUFRLEVBQUMsRUFBQyxDQUFDLENBQUMsSUFBSSxDQUN2RixHQUFHOzs7O29CQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7d0JBQ2IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxtQkFBQSxRQUFRLEVBQVUsQ0FBQztvQkFDekQsQ0FBQyxFQUFDLENBQ0wsQ0FBQztvQkFDRixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDckQ7Z0JBQ0QsT0FBTyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSTs7OztnQkFBQyxRQUFRLENBQUMsRUFBRTtvQkFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFO3dCQUN2RixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ2xDO29CQUNELE9BQU8sS0FBSyxDQUFDO2dCQUNqQixDQUFDLEVBQUMsQ0FBQzthQUNOO1NBQ0o7UUFBQyxPQUFPLEdBQUcsRUFBRTtTQUNiO1FBQ0QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsU0FBUyxDQUFDLE9BQU87UUFDYixJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLEVBQUU7WUFDakQsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxDQUFDO1NBQzFEO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO2FBQzlFLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTs7a0JBQ1AsS0FBSyxHQUFHLFFBQVE7WUFDdEIsSUFBSTtnQkFDQSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2FBQ25FO1lBQUMsT0FBTyxDQUFDLEVBQUU7YUFDWDtZQUNELElBQUk7Z0JBQ0EsY0FBYyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzthQUNyRTtZQUFDLE9BQU8sQ0FBQyxFQUFFO2FBQ1g7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDLEVBQUMsQ0FDTCxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsQ0FBQztJQUMzRCxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxNQUFNO1FBQ2IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7OztRQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFDO2FBQzVELElBQUk7Ozs7O1FBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQztRQUN4RCxLQUFLLE1BQU0sU0FBUyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7WUFDcEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUM5QjtJQUNMLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLGVBQXVCO1FBQ2xDLE9BQU8sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FDdkIsR0FBRzs7OztRQUFDLENBQUMsS0FBUyxFQUFFLEVBQUU7WUFDZCxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFOztzQkFDdkIsVUFBVTs7Ozs7Z0JBQUcsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQUU7OzBCQUMzQixPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxVQUFVLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRSxjQUFjLEVBQUUsUUFBUTt3QkFDMUYsUUFBUSxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO29CQUMvRCxPQUFPLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztvQkFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUNqQyxLQUFLLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7d0JBQy9CLFVBQVUsQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDaEQ7Z0JBQ0wsQ0FBQyxDQUFBO2dCQUNELEtBQUssTUFBTSxJQUFJLElBQUksS0FBSyxFQUFFO29CQUN0QixVQUFVLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2lCQUN4QjthQUNKO1lBQ0QsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDdEYsQ0FBQyxFQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLEtBQWE7UUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QixDQUFDOzs7WUF2SUosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBUE8sTUFBTTtZQUNOLFVBQVU7Ozs7O0lBVWQsMkJBQVU7O0lBQ1Ysa0NBQWdCOzs7OztJQUdaLDZCQUF3Qjs7Ozs7SUFDeEIsMEJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q2FjaGVTZXJ2aWNlfSBmcm9tICcuLi9jYWNoZS5zZXJ2aWNlJztcbmltcG9ydCB7bWFwfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQge1JvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7SHR0cENsaWVudH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHtmb3JrSm9pbn0gZnJvbSAncnhqcyc7XG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5cbmV4cG9ydCBjbGFzcyBQYWdlU2VydmljZSBleHRlbmRzIENhY2hlU2VydmljZSB7XG5cbiAgICBwYWdlOiBhbnk7XG4gICAgYnJlYWRjcnVtYnM6IHt9O1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCByb3V0ZXI6IFJvdXRlcixcbiAgICAgICAgcHJvdGVjdGVkIGFwaTogSHR0cENsaWVudCxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICByb3V0ZXIsXG4gICAgICAgICAgICBhcGksXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcmVmcmVzaCgpOiB2b2lkIHtcbiAgICAgICAgc3VwZXIucmVmcmVzaCgpO1xuICAgICAgICB0aGlzLnBhZ2UgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5icmVhZGNydW1icyA9IHt9O1xuICAgIH1cblxuICAgIHNldEN1cnJlbnRQYWdlKHBhZ2U6IGFueSkge1xuICAgICAgICB0aGlzLnBhZ2UgPSBwYWdlO1xuICAgIH1cblxuICAgIGdldEN1cnJlbnRQYWdlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5wYWdlO1xuICAgIH1cblxuICAgIGdldEFsbChoZWFkZXJzPzoge30pIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0TGlzdFRocm91Z2hDYWNoZSgncGFnZXMnLCB7fSwgaGVhZGVycyB8fCB7fSk7XG4gICAgfVxuXG4gICAgZ2V0UGFnZShpZGVudGlmaWVyLCBibG9ja0ludGVyYWN0aW9uPzogYm9vbGVhbiwgaGVhZGVycz86IHt9KSB7XG4gICAgICAgIGNvbnN0IGNhY2hlS2V5ID0gdGhpcy5jcmVhdGVDYWNoZUtleShbJ3BhZ2UnLCBpZGVudGlmaWVyLCBoZWFkZXJzXSk7XG4gICAgICAgIGlmICghYmxvY2tJbnRlcmFjdGlvbiAmJiB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbY2FjaGVLZXldKSB7XG4gICAgICAgICAgICB0aGlzLmFwaS5nZXQoJ3BhZ2UvJyArIGlkZW50aWZpZXIsIHtoZWFkZXJzOiBoZWFkZXJzIHx8IHt9fSkuc3Vic2NyaWJlKCk7IC8vIFRyaWdnZXIgaW50ZXJhY3Rpb25cbiAgICAgICAgICAgIHJldHVybiB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbY2FjaGVLZXldO1xuICAgICAgICB9IGVsc2UgaWYgKGJsb2NrSW50ZXJhY3Rpb24pIHtcbiAgICAgICAgICAgIGhlYWRlcnMgPSBPYmplY3QuYXNzaWduKHt9LCAoaGVhZGVycyB8fCB7fSksIHsneC1zZXJ2aWNlLWluZm8nOiBKU09OLnN0cmluZ2lmeSh7YmxvY2tJbnRlcmFjdGlvbjogdHJ1ZX0pfSk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XSA9IHRoaXMuZ2V0T25lVGhyb3VnaENhY2hlKCdwYWdlJywgaWRlbnRpZmllciwgaGVhZGVycywgY2FjaGVLZXkpO1xuICAgICAgICByZXR1cm4gdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkW2NhY2hlS2V5XTtcbiAgICB9XG5cbiAgICBnZXRQYWdlcyhoZWFkZXJzPzoge30pIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0TGlzdFRocm91Z2hDYWNoZSgncGFnZXMnLCB7fSwgaGVhZGVycyB8fCB7fSk7XG4gICAgfVxuXG4gICAgZ2V0QmxvY2soYmxvY2tJZCkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3Qgb2JzZXJ2YWJsZXMgPSBbXTtcbiAgICAgICAgICAgIGlmICh0aGlzLmdldEZyb21TdG9yYWdlKCdibG9jazonICsgYmxvY2tJZCkgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgYmxvY2sgPSBKU09OLnBhcnNlKHRoaXMuZ2V0RnJvbVN0b3JhZ2UoJ2Jsb2NrOicgKyBibG9ja0lkKSBhcyBzdHJpbmcpO1xuICAgICAgICAgICAgICAgIGlmICghYmxvY2sudGltZXN0YW1wKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmxvYWRCbG9jayhibG9ja0lkKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLmNhY2hlUHJvYmVUaW1lc3RhbXAuYmxvY2tzKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2FjaGVQcm9iZVByb21pc2VzJC5ibG9ja3MgPSB0aGlzLmFwaS5nZXQoJ2NhY2hlLXByb2JlJywge3BhcmFtczoge3E6ICd3aWRnZXQnfX0pLnBpcGUoXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXAoKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jYWNoZVByb2JlVGltZXN0YW1wLmJsb2NrcyA9IHJlc3BvbnNlIGFzIG51bWJlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmFibGVzLnB1c2godGhpcy5jYWNoZVByb2JlUHJvbWlzZXMkLmJsb2Nrcyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiBmb3JrSm9pbihvYnNlcnZhYmxlcykudG9Qcm9taXNlKCkudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghdGhpcy5jYWNoZVByb2JlVGltZXN0YW1wLmJsb2NrcyB8fCBibG9jay50aW1lc3RhbXAgPCB0aGlzLmNhY2hlUHJvYmVUaW1lc3RhbXAuYmxvY2tzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5sb2FkQmxvY2soYmxvY2tJZCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGJsb2NrO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5sb2FkQmxvY2soYmxvY2tJZCk7XG4gICAgfVxuXG4gICAgbG9hZEJsb2NrKGJsb2NrSWQpIHtcbiAgICAgICAgaWYgKHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFsnYmxvY2s6JyArIGJsb2NrSWRdKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkWydibG9jazonICsgYmxvY2tJZF07XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkWydibG9jazonICsgYmxvY2tJZF0gPSB0aGlzLmFwaS5nZXQoJ3dpZGdldC8nICsgYmxvY2tJZClcbiAgICAgICAgICAgIC5waXBlKFxuICAgICAgICAgICAgICAgIG1hcCgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYmxvY2sgPSByZXNwb25zZTtcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdibG9jazonICsgYmxvY2tJZCwgSlNPTi5zdHJpbmdpZnkoYmxvY2spKTtcbiAgICAgICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdibG9jazonICsgYmxvY2tJZCwgSlNPTi5zdHJpbmdpZnkoYmxvY2spKTtcbiAgICAgICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBibG9jaztcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgKS50b1Byb21pc2UoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFsnYmxvY2s6JyArIGJsb2NrSWRdO1xuICAgIH1cblxuICAgIHNvcnRCbG9ja3MoYmxvY2tzKSB7XG4gICAgICAgIGJsb2Nrcy5jb250ZW50LnNvcnQoKGEsIGIpID0+IChhLmRhdGEucm93IDwgYi5kYXRhLnJvdykgPyAxIDogLTEpXG4gICAgICAgICAgICAuc29ydCgoYSwgYikgPT4gKGEuZGF0YS5jb2wgPCBiLmRhdGEuY29sKSA/IDEgOiAtMSk7XG4gICAgICAgIGZvciAoY29uc3Qgc3ViQmxvY2tzIG9mIGJsb2Nrcy5jb250ZW50KSB7XG4gICAgICAgICAgICB0aGlzLnNvcnRCbG9ja3Moc3ViQmxvY2tzKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldEJyZWFkY3J1bWJzKHJlcXVlc3RlZFBhZ2VJZDogbnVtYmVyKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldFBhZ2VzKCkucGlwZShcbiAgICAgICAgICAgIG1hcCgocGFnZXM6IFtdKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKF8uaXNFbXB0eSh0aGlzLmJyZWFkY3J1bWJzKSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBleHRyYWN0SWRzID0gKHBhZ2UsIHBhcmVudHMpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBhZ2VNaW4gPSBfLm9taXQocGFnZSwgWydjaGlsZHJlbicsICdjcmVhdGVkQnknLCAnZGF0ZUNyZWF0ZWQnLCAnZGlzcGxheU9yZGVyJywgJ2dyb3VwcycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2hpZGRlbicsICdsYXN0VXBkYXRlZCcsICdwZXJtaXNzaW9ucycsICd0YWdzJywgJ3dpZGdldHMnXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlTWluLnBhcmVudHMgPSBwYXJlbnRzO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5icmVhZGNydW1ic1twYWdlLmlkXSA9IHBhZ2U7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IGNoaWxkIG9mIHBhZ2UuY2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHRyYWN0SWRzKGNoaWxkLCBwYXJlbnRzLmNvbmNhdChbcGFnZU1pbl0pKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBwYWdlIG9mIHBhZ2VzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBleHRyYWN0SWRzKHBhZ2UsIFtdKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5icmVhZGNydW1ic1tyZXF1ZXN0ZWRQYWdlSWRdID8gdGhpcy5icmVhZGNydW1ic1tyZXF1ZXN0ZWRQYWdlSWRdIDogW107XG4gICAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgIH1cblxuICAgIHBlcmZvcm1TZWFyY2gocXVlcnk6IHN0cmluZykge1xuICAgICAgICBjb25zb2xlLmxvZyhxdWVyeSk7XG4gICAgfVxuXG59XG4iXX0=