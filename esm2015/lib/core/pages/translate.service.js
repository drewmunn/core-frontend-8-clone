/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/translate.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { CacheService } from '../cache.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common/http";
export class TranslateService extends CacheService {
    /**
     * @param {?} router
     * @param {?} api
     */
    constructor(router, api) {
        super();
        this.router = router;
        this.api = api;
        this.defaultLanguage = 'en_GB';
        this.refresh();
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    getTranslation(lang) {
        this.translations = this.getTranslations(lang).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            return response.data;
        })));
        return (/** @type {?} */ (this.translations));
    }
    /**
     * @return {?}
     */
    getLanguages() {
        if (this.objectLoadObservables$.languages) {
            return this.objectLoadObservables$.languages;
        }
        this.objectLoadObservables$.languages = new ReplaySubject();
        this.api.get('languages').subscribe((/**
         * @param {?} languages
         * @return {?}
         */
        languages => {
            sessionStorage.setItem('languages', JSON.stringify(languages));
            this.objectLoadObservables$.languages.next(languages);
        }));
        return this.objectLoadObservables$.languages;
    }
    /**
     * @param {?} languageCode
     * @return {?}
     */
    getTranslations(languageCode) {
        // if (this.getFromStorage('translations:' + languageCode)) {
        //     const translations = JSON.parse(this.getFromStorage('translations:' + languageCode) as string);
        //     if (!translations.timestamp) {
        //         return this.loadTranslations(languageCode);
        //     }
        //     let observables = [];
        //     if (!this.cacheProbeTimestamp.translations) {
        //         if (!this.cacheProbePromises$.translations) {
        //             this.cacheProbePromises$.translations = this.restangular.one('cache-probe').get({q: 'translations'}).pipe(
        //                 map((response) => {
        //                     this.cacheProbeTimestamp.translations = response as number;
        //                 })
        //             );
        //         }
        //     }
        //     observables.push(this.cacheProbePromises$.translations);
        //     return forkJoin(observables).subscribe(response => {
        //         if (!this.cacheProbeTimestamp.translations || translation.timestamp < this.cacheProbeTimestamp.translations) {
        //             return this.loadTranslations(languageCode);
        //         }
        //
        //         return deferred.promise;
        //     });
        // }
        return this.loadTranslations(languageCode);
    }
    /**
     * @param {?} languageCode
     * @return {?}
     */
    loadTranslations(languageCode) {
        if (this.objectLoadObservables$['translations:' + languageCode]) {
            return this.objectLoadObservables$['translations:' + languageCode];
        }
        this.objectLoadObservables$['translations:' + languageCode] = this.api.get('translations', {
            params: { languageCode },
            observe: 'response'
        }).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            /** @type {?} */
            const newServiceInfo = JSON.parse(response.headers.get('x-service-info'));
            /** @type {?} */
            const translations = {
                timestamp: this.cacheProbeTimestamp.translations || newServiceInfo.timestamp,
                data: {}
            };
            for (const translation of (/** @type {?} */ (response.body))) {
                translations.data[translation.dataKey] = translation.dataValue;
            }
            try {
                sessionStorage.setItem('translations:' + languageCode, JSON.stringify(translations));
            }
            catch (e) {
            }
            try {
                localStorage.setItem('translations:' + languageCode, JSON.stringify(translations));
            }
            catch (e) {
            }
            return translations;
        })));
        return this.objectLoadObservables$['translations:' + languageCode];
    }
    /**
     * @return {?}
     */
    getDefaultLanguage() {
        if (sessionStorage.getItem('defaultLanguage')) {
            return sessionStorage.getItem('defaultLanguage');
        }
        if (localStorage.getItem('defaultLanguage')) {
            return localStorage.getItem('defaultLanguage');
        }
        return this.defaultLanguage;
    }
    /**
     * @param {?} languageCode
     * @return {?}
     */
    setDefaultLanguage(languageCode) {
        try {
            sessionStorage.setItem('defaultLanguage', languageCode);
            localStorage.setItem('defaultLanguage', languageCode);
        }
        catch (_a) {
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        console.log('Destroy translate');
    }
}
TranslateService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TranslateService.ctorParameters = () => [
    { type: Router },
    { type: HttpClient }
];
/** @nocollapse */ TranslateService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TranslateService_Factory() { return new TranslateService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.HttpClient)); }, token: TranslateService, providedIn: "root" });
if (false) {
    /** @type {?} */
    TranslateService.prototype.translations;
    /** @type {?} */
    TranslateService.prototype.defaultLanguage;
    /**
     * @type {?}
     * @protected
     */
    TranslateService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    TranslateService.prototype.api;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3BhZ2VzL3RyYW5zbGF0ZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBWSxNQUFNLGVBQWUsQ0FBQztBQUVwRCxPQUFPLEVBQWEsYUFBYSxFQUFDLE1BQU0sTUFBTSxDQUFDO0FBQy9DLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxrQkFBa0IsQ0FBQztBQUM5QyxPQUFPLEVBQUMsR0FBRyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbkMsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQzs7OztBQU1oRCxNQUFNLE9BQU8sZ0JBQWlCLFNBQVEsWUFBWTs7Ozs7SUFFOUMsWUFDYyxNQUFjLEVBQ2QsR0FBZTtRQUV6QixLQUFLLEVBQUUsQ0FBQztRQUhFLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxRQUFHLEdBQUgsR0FBRyxDQUFZO1FBTzdCLG9CQUFlLEdBQUcsT0FBTyxDQUFDO1FBSnRCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDOzs7OztJQUtELGNBQWMsQ0FBQyxJQUFZO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQy9DLEdBQUc7Ozs7UUFBQyxDQUFDLFFBQXVCLEVBQUUsRUFBRTtZQUM1QixPQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDekIsQ0FBQyxFQUFDLENBQUMsQ0FBQztRQUVSLE9BQU8sbUJBQUEsSUFBSSxDQUFDLFlBQVksRUFBbUIsQ0FBQztJQUNoRCxDQUFDOzs7O0lBRUQsWUFBWTtRQUNSLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRTtZQUN2QyxPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7U0FDaEQ7UUFDRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxHQUFHLElBQUksYUFBYSxFQUFZLENBQUM7UUFDdEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUzs7OztRQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQzVDLGNBQWMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxRCxDQUFDLEVBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQztJQUNqRCxDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxZQUFZO1FBQ3hCLDZEQUE2RDtRQUM3RCxzR0FBc0c7UUFDdEcscUNBQXFDO1FBQ3JDLHNEQUFzRDtRQUN0RCxRQUFRO1FBQ1IsNEJBQTRCO1FBQzVCLG9EQUFvRDtRQUNwRCx3REFBd0Q7UUFDeEQseUhBQXlIO1FBQ3pILHNDQUFzQztRQUN0QyxrRkFBa0Y7UUFDbEYscUJBQXFCO1FBQ3JCLGlCQUFpQjtRQUNqQixZQUFZO1FBQ1osUUFBUTtRQUNSLCtEQUErRDtRQUMvRCwyREFBMkQ7UUFDM0QseUhBQXlIO1FBQ3pILDBEQUEwRDtRQUMxRCxZQUFZO1FBQ1osRUFBRTtRQUNGLG1DQUFtQztRQUNuQyxVQUFVO1FBQ1YsSUFBSTtRQUVKLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQy9DLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsWUFBWTtRQUN6QixJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLEdBQUcsWUFBWSxDQUFDLEVBQUU7WUFDN0QsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxHQUFHLFlBQVksQ0FBQyxDQUFDO1NBQ3RFO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsR0FBRyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQ3JGO1lBQ0ksTUFBTSxFQUFFLEVBQUMsWUFBWSxFQUFDO1lBQ3RCLE9BQU8sRUFBRSxVQUFVO1NBQ3RCLENBQUMsQ0FBQyxJQUFJLENBQ1AsR0FBRzs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFOztrQkFDTCxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOztrQkFDbkUsWUFBWSxHQUFHO2dCQUNqQixTQUFTLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksSUFBSSxjQUFjLENBQUMsU0FBUztnQkFDNUUsSUFBSSxFQUFFLEVBQUU7YUFDWDtZQUNELEtBQUssTUFBTSxXQUFXLElBQUksbUJBQUEsUUFBUSxDQUFDLElBQUksRUFBTyxFQUFFO2dCQUM1QyxZQUFZLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDO2FBQ2xFO1lBQ0QsSUFBSTtnQkFDQSxjQUFjLENBQUMsT0FBTyxDQUFDLGVBQWUsR0FBRyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2FBQ3hGO1lBQUMsT0FBTyxDQUFDLEVBQUU7YUFDWDtZQUNELElBQUk7Z0JBQ0EsWUFBWSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEdBQUcsWUFBWSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzthQUN0RjtZQUFDLE9BQU8sQ0FBQyxFQUFFO2FBQ1g7WUFFRCxPQUFPLFlBQVksQ0FBQztRQUN4QixDQUFDLEVBQUMsQ0FDTCxDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxHQUFHLFlBQVksQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7Ozs7SUFFRCxrQkFBa0I7UUFDZCxJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsRUFBRTtZQUMzQyxPQUFPLGNBQWMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUNwRDtRQUNELElBQUksWUFBWSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO1lBQ3pDLE9BQU8sWUFBWSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQ2xEO1FBQ0QsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQ2hDLENBQUM7Ozs7O0lBRUQsa0JBQWtCLENBQUMsWUFBb0I7UUFDbkMsSUFBSTtZQUNBLGNBQWMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsWUFBWSxDQUFDLENBQUM7WUFDeEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxZQUFZLENBQUMsQ0FBQztTQUN6RDtRQUFDLFdBQU07U0FDUDtJQUNMLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7OztZQXpISixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFMTyxNQUFNO1lBQ04sVUFBVTs7Ozs7SUFnQmQsd0NBQThCOztJQUM5QiwyQ0FBMEI7Ozs7O0lBUnRCLGtDQUF3Qjs7Ozs7SUFDeEIsK0JBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlLCBPbkRlc3Ryb3l9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtUcmFuc2xhdGVMb2FkZXJ9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHtPYnNlcnZhYmxlLCBSZXBsYXlTdWJqZWN0fSBmcm9tICdyeGpzJztcbmltcG9ydCB7Q2FjaGVTZXJ2aWNlfSBmcm9tICcuLi9jYWNoZS5zZXJ2aWNlJztcbmltcG9ydCB7bWFwfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQge1JvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7SHR0cENsaWVudH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5leHBvcnQgY2xhc3MgVHJhbnNsYXRlU2VydmljZSBleHRlbmRzIENhY2hlU2VydmljZSBpbXBsZW1lbnRzIFRyYW5zbGF0ZUxvYWRlciwgT25EZXN0cm95IHtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgICAgIHByb3RlY3RlZCBhcGk6IEh0dHBDbGllbnQsXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgICAgIHRoaXMucmVmcmVzaCgpO1xuICAgIH1cblxuICAgIHRyYW5zbGF0aW9uczogT2JzZXJ2YWJsZTxhbnk+O1xuICAgIGRlZmF1bHRMYW5ndWFnZSA9ICdlbl9HQic7XG5cbiAgICBnZXRUcmFuc2xhdGlvbihsYW5nOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgICAgICB0aGlzLnRyYW5zbGF0aW9ucyA9IHRoaXMuZ2V0VHJhbnNsYXRpb25zKGxhbmcpLnBpcGUoXG4gICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiB7IGRhdGE6IGFueSB9KSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB9KSk7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRpb25zIGFzIE9ic2VydmFibGU8YW55PjtcbiAgICB9XG5cbiAgICBnZXRMYW5ndWFnZXMoKSB7XG4gICAgICAgIGlmICh0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyQubGFuZ3VhZ2VzKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkLmxhbmd1YWdlcztcbiAgICAgICAgfVxuICAgICAgICB0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyQubGFuZ3VhZ2VzID0gbmV3IFJlcGxheVN1YmplY3Q8UmVzcG9uc2U+KCk7XG4gICAgICAgIHRoaXMuYXBpLmdldCgnbGFuZ3VhZ2VzJykuc3Vic2NyaWJlKGxhbmd1YWdlcyA9PiB7XG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdsYW5ndWFnZXMnLCBKU09OLnN0cmluZ2lmeShsYW5ndWFnZXMpKTtcbiAgICAgICAgICAgIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJC5sYW5ndWFnZXMubmV4dChsYW5ndWFnZXMpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJC5sYW5ndWFnZXM7XG4gICAgfVxuXG4gICAgZ2V0VHJhbnNsYXRpb25zKGxhbmd1YWdlQ29kZSkge1xuICAgICAgICAvLyBpZiAodGhpcy5nZXRGcm9tU3RvcmFnZSgndHJhbnNsYXRpb25zOicgKyBsYW5ndWFnZUNvZGUpKSB7XG4gICAgICAgIC8vICAgICBjb25zdCB0cmFuc2xhdGlvbnMgPSBKU09OLnBhcnNlKHRoaXMuZ2V0RnJvbVN0b3JhZ2UoJ3RyYW5zbGF0aW9uczonICsgbGFuZ3VhZ2VDb2RlKSBhcyBzdHJpbmcpO1xuICAgICAgICAvLyAgICAgaWYgKCF0cmFuc2xhdGlvbnMudGltZXN0YW1wKSB7XG4gICAgICAgIC8vICAgICAgICAgcmV0dXJuIHRoaXMubG9hZFRyYW5zbGF0aW9ucyhsYW5ndWFnZUNvZGUpO1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyAgICAgbGV0IG9ic2VydmFibGVzID0gW107XG4gICAgICAgIC8vICAgICBpZiAoIXRoaXMuY2FjaGVQcm9iZVRpbWVzdGFtcC50cmFuc2xhdGlvbnMpIHtcbiAgICAgICAgLy8gICAgICAgICBpZiAoIXRoaXMuY2FjaGVQcm9iZVByb21pc2VzJC50cmFuc2xhdGlvbnMpIHtcbiAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5jYWNoZVByb2JlUHJvbWlzZXMkLnRyYW5zbGF0aW9ucyA9IHRoaXMucmVzdGFuZ3VsYXIub25lKCdjYWNoZS1wcm9iZScpLmdldCh7cTogJ3RyYW5zbGF0aW9ucyd9KS5waXBlKFxuICAgICAgICAvLyAgICAgICAgICAgICAgICAgbWFwKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAvLyAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2FjaGVQcm9iZVRpbWVzdGFtcC50cmFuc2xhdGlvbnMgPSByZXNwb25zZSBhcyBudW1iZXI7XG4gICAgICAgIC8vICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAvLyAgICAgICAgICAgICApO1xuICAgICAgICAvLyAgICAgICAgIH1cbiAgICAgICAgLy8gICAgIH1cbiAgICAgICAgLy8gICAgIG9ic2VydmFibGVzLnB1c2godGhpcy5jYWNoZVByb2JlUHJvbWlzZXMkLnRyYW5zbGF0aW9ucyk7XG4gICAgICAgIC8vICAgICByZXR1cm4gZm9ya0pvaW4ob2JzZXJ2YWJsZXMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XG4gICAgICAgIC8vICAgICAgICAgaWYgKCF0aGlzLmNhY2hlUHJvYmVUaW1lc3RhbXAudHJhbnNsYXRpb25zIHx8IHRyYW5zbGF0aW9uLnRpbWVzdGFtcCA8IHRoaXMuY2FjaGVQcm9iZVRpbWVzdGFtcC50cmFuc2xhdGlvbnMpIHtcbiAgICAgICAgLy8gICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9hZFRyYW5zbGF0aW9ucyhsYW5ndWFnZUNvZGUpO1xuICAgICAgICAvLyAgICAgICAgIH1cbiAgICAgICAgLy9cbiAgICAgICAgLy8gICAgICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICAgICAgLy8gICAgIH0pO1xuICAgICAgICAvLyB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMubG9hZFRyYW5zbGF0aW9ucyhsYW5ndWFnZUNvZGUpO1xuICAgIH1cblxuICAgIGxvYWRUcmFuc2xhdGlvbnMobGFuZ3VhZ2VDb2RlKSB7XG4gICAgICAgIGlmICh0aGlzLm9iamVjdExvYWRPYnNlcnZhYmxlcyRbJ3RyYW5zbGF0aW9uczonICsgbGFuZ3VhZ2VDb2RlXSkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFsndHJhbnNsYXRpb25zOicgKyBsYW5ndWFnZUNvZGVdO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub2JqZWN0TG9hZE9ic2VydmFibGVzJFsndHJhbnNsYXRpb25zOicgKyBsYW5ndWFnZUNvZGVdID0gdGhpcy5hcGkuZ2V0KCd0cmFuc2xhdGlvbnMnLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHBhcmFtczoge2xhbmd1YWdlQ29kZX0sXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZTogJ3Jlc3BvbnNlJ1xuICAgICAgICAgICAgfSkucGlwZShcbiAgICAgICAgICAgIG1hcChyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgbmV3U2VydmljZUluZm8gPSBKU09OLnBhcnNlKHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCd4LXNlcnZpY2UtaW5mbycpKTtcbiAgICAgICAgICAgICAgICBjb25zdCB0cmFuc2xhdGlvbnMgPSB7XG4gICAgICAgICAgICAgICAgICAgIHRpbWVzdGFtcDogdGhpcy5jYWNoZVByb2JlVGltZXN0YW1wLnRyYW5zbGF0aW9ucyB8fCBuZXdTZXJ2aWNlSW5mby50aW1lc3RhbXAsXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHt9XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IHRyYW5zbGF0aW9uIG9mIHJlc3BvbnNlLmJvZHkgYXMgYW55KSB7XG4gICAgICAgICAgICAgICAgICAgIHRyYW5zbGF0aW9ucy5kYXRhW3RyYW5zbGF0aW9uLmRhdGFLZXldID0gdHJhbnNsYXRpb24uZGF0YVZhbHVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCd0cmFuc2xhdGlvbnM6JyArIGxhbmd1YWdlQ29kZSwgSlNPTi5zdHJpbmdpZnkodHJhbnNsYXRpb25zKSk7XG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgndHJhbnNsYXRpb25zOicgKyBsYW5ndWFnZUNvZGUsIEpTT04uc3RyaW5naWZ5KHRyYW5zbGF0aW9ucykpO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gdHJhbnNsYXRpb25zO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgKTtcblxuICAgICAgICByZXR1cm4gdGhpcy5vYmplY3RMb2FkT2JzZXJ2YWJsZXMkWyd0cmFuc2xhdGlvbnM6JyArIGxhbmd1YWdlQ29kZV07XG4gICAgfVxuXG4gICAgZ2V0RGVmYXVsdExhbmd1YWdlKCkge1xuICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSgnZGVmYXVsdExhbmd1YWdlJykpIHtcbiAgICAgICAgICAgIHJldHVybiBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKCdkZWZhdWx0TGFuZ3VhZ2UnKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2RlZmF1bHRMYW5ndWFnZScpKSB7XG4gICAgICAgICAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2RlZmF1bHRMYW5ndWFnZScpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHRMYW5ndWFnZTtcbiAgICB9XG5cbiAgICBzZXREZWZhdWx0TGFuZ3VhZ2UobGFuZ3VhZ2VDb2RlOiBzdHJpbmcpIHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oJ2RlZmF1bHRMYW5ndWFnZScsIGxhbmd1YWdlQ29kZSk7XG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnZGVmYXVsdExhbmd1YWdlJywgbGFuZ3VhZ2VDb2RlKTtcbiAgICAgICAgfSBjYXRjaCB7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0Rlc3Ryb3kgdHJhbnNsYXRlJyk7XG4gICAgfVxuXG59XG4iXX0=