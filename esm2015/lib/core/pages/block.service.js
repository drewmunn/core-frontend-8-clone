/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/pages/block.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BlockWrapperComponent } from '../blocks/block-wrapper/block-wrapper.component';
import { BlockLanguageComponent } from '../blocks/block-language/block-language.component';
import { BlockParagraphComponent } from '../blocks/block-paragraph/block-paragraph.component';
import { BlockParagraphLeadComponent } from '../blocks/block-paragraph-lead/block-paragraph-lead.component';
import { BlockTableComponent } from '../blocks/block-table/block-table.component';
import { BlockAccordionComponent } from '../blocks/block-accordion/block-accordion.component';
import { BlockH1Component } from '../blocks/block-h1/block-h1.component';
import { BlockH2Component } from '../blocks/block-h2/block-h2.component';
import { BlockH3Component } from '../blocks/block-h3/block-h3.component';
import { BlockH4Component } from '../blocks/block-h4/block-h4.component';
import { BlockH5Component } from '../blocks/block-h5/block-h5.component';
import { BlockH6Component } from '../blocks/block-h6/block-h6.component';
import { BlockImageComponent } from '../blocks/block-image/block-image.component';
import { BlockBreadcrumbsComponent } from '../blocks/block-breadcrumbs/block-breadcrumbs.component';
import { BlockCarouselComponent } from '../blocks/block-carousel/block-carousel.component';
import { BlockVideoComponent } from '../blocks/block-video/block-video.component';
import { BlockBulletsComponent } from '../blocks/block-bullets/block-bullets.component';
import { BlockNumberedComponent } from '../blocks/block-numbered/block-numbered.component';
import { BlockBlockquoteComponent } from '../blocks/block-blockquote/block-blockquote.component';
import { BlockDownloadComponent } from '../blocks/block-download/block-download.component';
import { BlockButtonComponent } from '../blocks/block-button/block-button.component';
import { BlockDividerComponent } from '../blocks/block-divider/block-divider.component';
import { BlockProgressComponent } from '../blocks/block-progress/block-progress.component';
import { BlockTabsComponent } from '../blocks/block-tabs/block-tabs.component';
import { BlockSearchResultsComponent } from '../blocks/block-search-results/block-search-results.component';
import { BlockSearchComponent } from '../blocks/block-search/block-search.component';
import * as i0 from "@angular/core";
export class BlockService {
    constructor() {
        this.replaceBlocks({
            WRAPPER: BlockWrapperComponent,
            LANGUAGE: BlockLanguageComponent,
            PARAGRAPH: BlockParagraphComponent,
            'PARAGRAPH-LEAD': BlockParagraphLeadComponent,
            TABLE: BlockTableComponent,
            ACCORDION: BlockAccordionComponent,
            H1: BlockH1Component,
            H2: BlockH2Component,
            H3: BlockH3Component,
            H4: BlockH4Component,
            H5: BlockH5Component,
            H6: BlockH6Component,
            IMAGE: BlockImageComponent,
            BREADCRUMBS: BlockBreadcrumbsComponent,
            CAROUSEL: BlockCarouselComponent,
            VIDEO: BlockVideoComponent,
            BULLETS: BlockBulletsComponent,
            NUMBERED: BlockNumberedComponent,
            BLOCKQUOTE: BlockBlockquoteComponent,
            DOWNLOAD: BlockDownloadComponent,
            BUTTON: BlockButtonComponent,
            DIVIDER: BlockDividerComponent,
            PROGRESS: BlockProgressComponent,
            TABS: BlockTabsComponent,
            'SEARCH-RESULTS': BlockSearchResultsComponent,
            SEARCH: BlockSearchComponent,
        });
    }
    /**
     * @param {?} blocks
     * @return {?}
     */
    replaceBlocks(blocks) {
        this.blocks = blocks;
    }
    /**
     * @return {?}
     */
    getBlocks() {
        return this.blocks;
    }
    /**
     * @param {?} identifier
     * @return {?}
     */
    hasBlock(identifier) {
        return this.blocks.hasOwnProperty(identifier);
    }
    /**
     * @param {?} identifier
     * @return {?}
     */
    getBlock(identifier) {
        return this.blocks[identifier];
    }
    /**
     * @param {?} identifier
     * @param {?} component
     * @return {?}
     */
    addBlock(identifier, component) {
        this.blocks[identifier] = component;
    }
}
BlockService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
BlockService.ctorParameters = () => [];
/** @nocollapse */ BlockService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function BlockService_Factory() { return new BlockService(); }, token: BlockService, providedIn: "root" });
if (false) {
    /** @type {?} */
    BlockService.prototype.blocks;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2suc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvcGFnZXMvYmxvY2suc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0saURBQWlELENBQUM7QUFDdEYsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFDekYsT0FBTyxFQUFDLHVCQUF1QixFQUFDLE1BQU0scURBQXFELENBQUM7QUFDNUYsT0FBTyxFQUFDLDJCQUEyQixFQUFDLE1BQU0sK0RBQStELENBQUM7QUFDMUcsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sNkNBQTZDLENBQUM7QUFDaEYsT0FBTyxFQUFDLHVCQUF1QixFQUFDLE1BQU0scURBQXFELENBQUM7QUFDNUYsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sNkNBQTZDLENBQUM7QUFDaEYsT0FBTyxFQUFDLHlCQUF5QixFQUFDLE1BQU0seURBQXlELENBQUM7QUFDbEcsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFDekYsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sNkNBQTZDLENBQUM7QUFDaEYsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0saURBQWlELENBQUM7QUFDdEYsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFDekYsT0FBTyxFQUFDLHdCQUF3QixFQUFDLE1BQU0sdURBQXVELENBQUM7QUFDL0YsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFDekYsT0FBTyxFQUFDLG9CQUFvQixFQUFDLE1BQU0sK0NBQStDLENBQUM7QUFDbkYsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0saURBQWlELENBQUM7QUFDdEYsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sbURBQW1ELENBQUM7QUFDekYsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sMkNBQTJDLENBQUM7QUFDN0UsT0FBTyxFQUFDLDJCQUEyQixFQUFDLE1BQU0sK0RBQStELENBQUM7QUFDMUcsT0FBTyxFQUFDLG9CQUFvQixFQUFDLE1BQU0sK0NBQStDLENBQUM7O0FBTW5GLE1BQU0sT0FBTyxZQUFZO0lBSXJCO1FBQ0ksSUFBSSxDQUFDLGFBQWEsQ0FDZDtZQUNJLE9BQU8sRUFBRSxxQkFBcUI7WUFDOUIsUUFBUSxFQUFFLHNCQUFzQjtZQUNoQyxTQUFTLEVBQUUsdUJBQXVCO1lBQ2xDLGdCQUFnQixFQUFFLDJCQUEyQjtZQUM3QyxLQUFLLEVBQUUsbUJBQW1CO1lBQzFCLFNBQVMsRUFBRSx1QkFBdUI7WUFDbEMsRUFBRSxFQUFFLGdCQUFnQjtZQUNwQixFQUFFLEVBQUUsZ0JBQWdCO1lBQ3BCLEVBQUUsRUFBRSxnQkFBZ0I7WUFDcEIsRUFBRSxFQUFFLGdCQUFnQjtZQUNwQixFQUFFLEVBQUUsZ0JBQWdCO1lBQ3BCLEVBQUUsRUFBRSxnQkFBZ0I7WUFDcEIsS0FBSyxFQUFFLG1CQUFtQjtZQUMxQixXQUFXLEVBQUUseUJBQXlCO1lBQ3RDLFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMsS0FBSyxFQUFFLG1CQUFtQjtZQUMxQixPQUFPLEVBQUUscUJBQXFCO1lBQzlCLFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMsVUFBVSxFQUFFLHdCQUF3QjtZQUNwQyxRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLE1BQU0sRUFBRSxvQkFBb0I7WUFDNUIsT0FBTyxFQUFFLHFCQUFxQjtZQUM5QixRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLElBQUksRUFBRSxrQkFBa0I7WUFDeEIsZ0JBQWdCLEVBQUUsMkJBQTJCO1lBQzdDLE1BQU0sRUFBRSxvQkFBb0I7U0FDL0IsQ0FDSixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsTUFBTTtRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsU0FBUztRQUNMLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxVQUFVO1FBQ2YsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNsRCxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxVQUFVO1FBQ2YsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7OztJQUVELFFBQVEsQ0FBQyxVQUFVLEVBQUUsU0FBUztRQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLFNBQVMsQ0FBQztJQUN4QyxDQUFDOzs7WUEzREosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7Ozs7O0lBSUcsOEJBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja1dyYXBwZXJDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay13cmFwcGVyL2Jsb2NrLXdyYXBwZXIuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tMYW5ndWFnZUNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWxhbmd1YWdlL2Jsb2NrLWxhbmd1YWdlLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrUGFyYWdyYXBoQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stcGFyYWdyYXBoL2Jsb2NrLXBhcmFncmFwaC5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja1BhcmFncmFwaExlYWRDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1wYXJhZ3JhcGgtbGVhZC9ibG9jay1wYXJhZ3JhcGgtbGVhZC5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja1RhYmxlQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stdGFibGUvYmxvY2stdGFibGUuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tBY2NvcmRpb25Db21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1hY2NvcmRpb24vYmxvY2stYWNjb3JkaW9uLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrSDFDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1oMS9ibG9jay1oMS5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja0gyQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2staDIvYmxvY2staDIuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tIM0NvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWgzL2Jsb2NrLWgzLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrSDRDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1oNC9ibG9jay1oNC5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja0g1Q29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2staDUvYmxvY2staDUuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tINkNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWg2L2Jsb2NrLWg2LmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrSW1hZ2VDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1pbWFnZS9ibG9jay1pbWFnZS5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja0JyZWFkY3J1bWJzQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stYnJlYWRjcnVtYnMvYmxvY2stYnJlYWRjcnVtYnMuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tDYXJvdXNlbENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWNhcm91c2VsL2Jsb2NrLWNhcm91c2VsLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrVmlkZW9Db21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay12aWRlby9ibG9jay12aWRlby5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja0J1bGxldHNDb21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1idWxsZXRzL2Jsb2NrLWJ1bGxldHMuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tOdW1iZXJlZENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLW51bWJlcmVkL2Jsb2NrLW51bWJlcmVkLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrQmxvY2txdW90ZUNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWJsb2NrcXVvdGUvYmxvY2stYmxvY2txdW90ZS5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja0Rvd25sb2FkQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stZG93bmxvYWQvYmxvY2stZG93bmxvYWQuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tCdXR0b25Db21wb25lbnR9IGZyb20gJy4uL2Jsb2Nrcy9ibG9jay1idXR0b24vYmxvY2stYnV0dG9uLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrRGl2aWRlckNvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLWRpdmlkZXIvYmxvY2stZGl2aWRlci5jb21wb25lbnQnO1xuaW1wb3J0IHtCbG9ja1Byb2dyZXNzQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stcHJvZ3Jlc3MvYmxvY2stcHJvZ3Jlc3MuY29tcG9uZW50JztcbmltcG9ydCB7QmxvY2tUYWJzQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stdGFicy9ibG9jay10YWJzLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrU2VhcmNoUmVzdWx0c0NvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2tzL2Jsb2NrLXNlYXJjaC1yZXN1bHRzL2Jsb2NrLXNlYXJjaC1yZXN1bHRzLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jsb2NrU2VhcmNoQ29tcG9uZW50fSBmcm9tICcuLi9ibG9ja3MvYmxvY2stc2VhcmNoL2Jsb2NrLXNlYXJjaC5jb21wb25lbnQnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5leHBvcnQgY2xhc3MgQmxvY2tTZXJ2aWNlIHtcblxuICAgIGJsb2NrczogYW55O1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHRoaXMucmVwbGFjZUJsb2NrcyhcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBXUkFQUEVSOiBCbG9ja1dyYXBwZXJDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgTEFOR1VBR0U6IEJsb2NrTGFuZ3VhZ2VDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgUEFSQUdSQVBIOiBCbG9ja1BhcmFncmFwaENvbXBvbmVudCxcbiAgICAgICAgICAgICAgICAnUEFSQUdSQVBILUxFQUQnOiBCbG9ja1BhcmFncmFwaExlYWRDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgVEFCTEU6IEJsb2NrVGFibGVDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgQUNDT1JESU9OOiBCbG9ja0FjY29yZGlvbkNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBIMTogQmxvY2tIMUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBIMjogQmxvY2tIMkNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBIMzogQmxvY2tIM0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBINDogQmxvY2tINENvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBINTogQmxvY2tINUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBINjogQmxvY2tINkNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBJTUFHRTogQmxvY2tJbWFnZUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBCUkVBRENSVU1CUzogQmxvY2tCcmVhZGNydW1ic0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBDQVJPVVNFTDogQmxvY2tDYXJvdXNlbENvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBWSURFTzogQmxvY2tWaWRlb0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBCVUxMRVRTOiBCbG9ja0J1bGxldHNDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgTlVNQkVSRUQ6IEJsb2NrTnVtYmVyZWRDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgQkxPQ0tRVU9URTogQmxvY2tCbG9ja3F1b3RlQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIERPV05MT0FEOiBCbG9ja0Rvd25sb2FkQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIEJVVFRPTjogQmxvY2tCdXR0b25Db21wb25lbnQsXG4gICAgICAgICAgICAgICAgRElWSURFUjogQmxvY2tEaXZpZGVyQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIFBST0dSRVNTOiBCbG9ja1Byb2dyZXNzQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIFRBQlM6IEJsb2NrVGFic0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICAnU0VBUkNILVJFU1VMVFMnOiBCbG9ja1NlYXJjaFJlc3VsdHNDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgU0VBUkNIOiBCbG9ja1NlYXJjaENvbXBvbmVudCxcbiAgICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICByZXBsYWNlQmxvY2tzKGJsb2Nrcykge1xuICAgICAgICB0aGlzLmJsb2NrcyA9IGJsb2NrcztcbiAgICB9XG5cbiAgICBnZXRCbG9ja3MoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmJsb2NrcztcbiAgICB9XG5cbiAgICBoYXNCbG9jayhpZGVudGlmaWVyKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmJsb2Nrcy5oYXNPd25Qcm9wZXJ0eShpZGVudGlmaWVyKTtcbiAgICB9XG5cbiAgICBnZXRCbG9jayhpZGVudGlmaWVyKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmJsb2Nrc1tpZGVudGlmaWVyXTtcbiAgICB9XG5cbiAgICBhZGRCbG9jayhpZGVudGlmaWVyLCBjb21wb25lbnQpIHtcbiAgICAgICAgdGhpcy5ibG9ja3NbaWRlbnRpZmllcl0gPSBjb21wb25lbnQ7XG4gICAgfVxuXG59XG4iXX0=