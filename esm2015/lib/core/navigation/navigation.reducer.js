/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/navigation/navigation.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { createReducer, on } from '@ngrx/store';
import * as NavigationActions from './navigation.actions';
import { NavigationItems } from '../../core.navigation';
/**
 * @record
 */
export function NavigationItem() { }
if (false) {
    /** @type {?} */
    NavigationItem.prototype.badge;
    /** @type {?} */
    NavigationItem.prototype.item;
    /** @type {?} */
    NavigationItem.prototype.name;
    /** @type {?} */
    NavigationItem.prototype.title;
    /** @type {?|undefined} */
    NavigationItem.prototype.icon;
    /** @type {?|undefined} */
    NavigationItem.prototype.tags;
    /** @type {?|undefined} */
    NavigationItem.prototype.routerLink;
    /** @type {?|undefined} */
    NavigationItem.prototype.url;
    /** @type {?|undefined} */
    NavigationItem.prototype.active;
    /** @type {?|undefined} */
    NavigationItem.prototype.open;
    /** @type {?|undefined} */
    NavigationItem.prototype.items;
    /** @type {?|undefined} */
    NavigationItem.prototype.matched;
    /** @type {?|undefined} */
    NavigationItem.prototype.navTitle;
}
/**
 * @record
 */
export function NavigationState() { }
if (false) {
    /** @type {?} */
    NavigationState.prototype.items;
    /** @type {?} */
    NavigationState.prototype.total;
    /** @type {?} */
    NavigationState.prototype.filterActive;
    /** @type {?} */
    NavigationState.prototype.filterText;
    /** @type {?} */
    NavigationState.prototype.matched;
}
/** @type {?} */
export const initialState = {
    items: decorateItems(NavigationItems),
    total: countTotal(NavigationItems),
    filterActive: false,
    filterText: '',
    matched: 0
};
const ɵ0 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
(state, action) => (Object.assign({}, state, { items: detectActiveItems(state.items, action.url) })), ɵ1 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
(state, action) => (Object.assign({}, state, { items: toggleItems(state.items, action.item) })), ɵ2 = /**
 * @param {?} state
 * @return {?}
 */
state => {
    if (state.filterActive) {
        return Object.assign({}, state, { filterActive: false, matched: 0, items: state.items.map((/**
             * @param {?} _
             * @return {?}
             */
            _ => (Object.assign({}, _, { matched: null })))) });
    }
    else {
        /** @type {?} */
        const items = filterItems(state.items, state.filterText);
        return Object.assign({}, state, { filterActive: true, items, matched: countMatched(items) });
    }
}, ɵ3 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
(state, action) => {
    /** @type {?} */
    const items = filterItems(state.items, action.text);
    return Object.assign({}, state, { filterText: action.text, items, matched: countMatched(items) });
};
/** @type {?} */
const navigationReducer = createReducer(initialState, on(NavigationActions.activeUrl, (ɵ0)), on(NavigationActions.toggleNavSection, (ɵ1)), on(NavigationActions.toggleNavigationFilter, (ɵ2)), on(NavigationActions.navigationFilter, (ɵ3)));
/**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
export function reducer(state, action) {
    return navigationReducer(state, action);
}
/**
 * @param {?} navItems
 * @return {?}
 */
function decorateItems(navItems) {
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    navItem => {
        /** @type {?} */
        const item = Object.assign({}, navItem, { active: false, matched: null });
        if (navItem.items) {
            item.open = false;
            item.items = decorateItems(navItem.items);
        }
        item.navTitle = !navItem.items && !navItem.routerLink && !!navItem.title;
        return item;
    }));
}
/**
 * @param {?} navItems
 * @return {?}
 */
function countTotal(navItems) {
    /** @type {?} */
    let total = navItems.length;
    navItems.filter((/**
     * @param {?} _
     * @return {?}
     */
    _ => !!_.items)).forEach((/**
     * @param {?} _
     * @return {?}
     */
    _ => {
        total += countTotal(_.items);
    }));
    return total;
}
/**
 * @param {?} navItems
 * @return {?}
 */
function countMatched(navItems) {
    /** @type {?} */
    let matched = navItems.filter((/**
     * @param {?} _
     * @return {?}
     */
    _ => !!_.matched)).length;
    navItems.filter((/**
     * @param {?} _
     * @return {?}
     */
    _ => !!_.items)).forEach((/**
     * @param {?} _
     * @return {?}
     */
    _ => {
        matched += countMatched(_.items);
    }));
    return matched;
}
/**
 * @param {?} navItems
 * @param {?} activeUrl
 * @return {?}
 */
function detectActiveItems(navItems, activeUrl) {
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    navItem => {
        /** @type {?} */
        const isActive = itemIsActive(navItem, activeUrl);
        /** @type {?} */
        const item = Object.assign({}, navItem, { active: isActive });
        if (navItem.items) {
            item.open = isActive;
            item.items = detectActiveItems(navItem.items, activeUrl);
        }
        return item;
    }));
}
/**
 * @param {?} item
 * @param {?} activeUrl
 * @return {?}
 */
function itemIsActive(item, activeUrl) {
    if (item.routerLink === activeUrl) {
        return true;
    }
    else if (item.items) {
        return item.items.some((/**
         * @param {?} _
         * @return {?}
         */
        _ => itemIsActive(_, activeUrl)));
    }
    else {
        return false;
    }
}
/**
 * @param {?} navItems
 * @param {?} toggledItem
 * @return {?}
 */
function toggleItems(navItems, toggledItem) {
    /** @type {?} */
    const isToggledItemLevel = navItems.some((/**
     * @param {?} _
     * @return {?}
     */
    _ => _ === toggledItem));
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    navItem => {
        /** @type {?} */
        const item = Object.assign({}, navItem);
        if (isToggledItemLevel && item.items && navItem !== toggledItem) {
            item.open = false;
        }
        if (navItem === toggledItem) {
            item.open = !navItem.open;
        }
        if (navItem.items) {
            item.items = toggleItems(navItem.items, toggledItem);
        }
        return item;
    }));
}
/**
 * @param {?} navItems
 * @param {?} text
 * @return {?}
 */
function filterItems(navItems, text) {
    return navItems.map((/**
     * @param {?} navItem
     * @return {?}
     */
    navItem => {
        /** @type {?} */
        const item = Object.assign({}, navItem);
        if (navItem.items) {
            item.matched = navItemMatch(navItem, text) || navItem.items.some((/**
             * @param {?} _
             * @return {?}
             */
            _ => navItemMatch(_, text)));
            item.items = filterItems(navItem.items, text);
        }
        else {
            item.matched = navItemMatch(navItem, text);
        }
        return item;
    }));
}
/**
 * @param {?} item
 * @param {?} text
 * @return {?}
 */
function navItemMatch(item, text) {
    return (!text.trim() || (item.tags && !!item.tags.match(new RegExp(`.*${text.trim()}.*`, 'gi'))));
}
export { ɵ0, ɵ1, ɵ2, ɵ3 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5yZWR1Y2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9uYXZpZ2F0aW9uL25hdmlnYXRpb24ucmVkdWNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBUyxhQUFhLEVBQUUsRUFBRSxFQUFDLE1BQU0sYUFBYSxDQUFDO0FBQ3RELE9BQU8sS0FBSyxpQkFBaUIsTUFBTSxzQkFBc0IsQ0FBQztBQUMxRCxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sdUJBQXVCLENBQUM7Ozs7QUFFdEQsb0NBY0M7OztJQWJHLCtCQUFXOztJQUNYLDhCQUFVOztJQUNWLDhCQUFhOztJQUNiLCtCQUFjOztJQUNkLDhCQUFjOztJQUNkLDhCQUFjOztJQUNkLG9DQUFvQjs7SUFDcEIsNkJBQWE7O0lBQ2IsZ0NBQWlCOztJQUNqQiw4QkFBZTs7SUFDZiwrQkFBeUI7O0lBQ3pCLGlDQUFrQjs7SUFDbEIsa0NBQW1COzs7OztBQUd2QixxQ0FNQzs7O0lBTEcsZ0NBQXdCOztJQUN4QixnQ0FBYzs7SUFDZCx1Q0FBc0I7O0lBQ3RCLHFDQUFtQjs7SUFDbkIsa0NBQWdCOzs7QUFHcEIsTUFBTSxPQUFPLFlBQVksR0FBb0I7SUFDekMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxlQUFlLENBQUM7SUFDckMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxlQUFlLENBQUM7SUFDbEMsWUFBWSxFQUFFLEtBQUs7SUFDbkIsVUFBVSxFQUFFLEVBQUU7SUFDZCxPQUFPLEVBQUUsQ0FBQztDQUNiOzs7Ozs7QUFLbUMsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEVBQUUsQ0FBQyxtQkFDNUMsS0FBSyxJQUNSLEtBQUssRUFBRSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFDbkQ7Ozs7O0FBQ3FDLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxFQUFFLENBQUMsbUJBQ25ELEtBQUssSUFDUixLQUFLLEVBQUUsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUM5Qzs7OztBQUMyQyxLQUFLLENBQUMsRUFBRTtJQUNqRCxJQUFJLEtBQUssQ0FBQyxZQUFZLEVBQUU7UUFDcEIseUJBQ08sS0FBSyxJQUNSLFlBQVksRUFBRSxLQUFLLEVBQ25CLE9BQU8sRUFBRSxDQUFDLEVBQ1YsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsbUJBQUssQ0FBQyxJQUFFLE9BQU8sRUFBRSxJQUFJLElBQUUsRUFBQyxJQUN0RDtLQUNMO1NBQU07O2NBQ0csS0FBSyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUM7UUFDeEQseUJBQ08sS0FBSyxJQUNSLFlBQVksRUFBRSxJQUFJLEVBQ2xCLEtBQUssRUFDTCxPQUFPLEVBQUUsWUFBWSxDQUFDLEtBQUssQ0FBQyxJQUM5QjtLQUNMO0FBRUwsQ0FBQzs7Ozs7QUFDc0MsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEVBQUU7O1VBQy9DLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ25ELHlCQUNPLEtBQUssSUFDUixVQUFVLEVBQUUsTUFBTSxDQUFDLElBQUksRUFDdkIsS0FBSyxFQUNMLE9BQU8sRUFBRSxZQUFZLENBQUMsS0FBSyxDQUFDLElBQzlCO0FBQ04sQ0FBQzs7TUFyQ0MsaUJBQWlCLEdBQUcsYUFBYSxDQUNuQyxZQUFZLEVBQ1osRUFBRSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsT0FHM0IsRUFDSCxFQUFFLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLE9BR2xDLEVBQ0gsRUFBRSxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixPQWtCekMsRUFDRixFQUFFLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLE9BUW5DLENBQ0w7Ozs7OztBQUdELE1BQU0sVUFBVSxPQUFPLENBQUMsS0FBc0IsRUFBRSxNQUFjO0lBQzFELE9BQU8saUJBQWlCLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQzVDLENBQUM7Ozs7O0FBRUQsU0FBUyxhQUFhLENBQUMsUUFBMEI7SUFDN0MsT0FBTyxRQUFRLENBQUMsR0FBRzs7OztJQUFDLE9BQU8sQ0FBQyxFQUFFOztjQUNwQixJQUFJLHFCQUNILE9BQU8sSUFDVixNQUFNLEVBQUUsS0FBSyxFQUNiLE9BQU8sRUFBRSxJQUFJLEdBQ2hCO1FBQ0QsSUFBSSxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ2YsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7WUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzdDO1FBRUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO1FBRXpFLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUMsRUFBQyxDQUFDO0FBQ1AsQ0FBQzs7Ozs7QUFFRCxTQUFTLFVBQVUsQ0FBQyxRQUEwQjs7UUFDdEMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxNQUFNO0lBQzNCLFFBQVEsQ0FBQyxNQUFNOzs7O0lBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBQyxDQUFDLE9BQU87Ozs7SUFBQyxDQUFDLENBQUMsRUFBRTtRQUN4QyxLQUFLLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDLEVBQUMsQ0FBQztJQUNILE9BQU8sS0FBSyxDQUFDO0FBQ2pCLENBQUM7Ozs7O0FBRUQsU0FBUyxZQUFZLENBQUMsUUFBMEI7O1FBQ3hDLE9BQU8sR0FBRyxRQUFRLENBQUMsTUFBTTs7OztJQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUMsQ0FBQyxNQUFNO0lBQ3RELFFBQVEsQ0FBQyxNQUFNOzs7O0lBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBQyxDQUFDLE9BQU87Ozs7SUFBQyxDQUFDLENBQUMsRUFBRTtRQUN4QyxPQUFPLElBQUksWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyQyxDQUFDLEVBQUMsQ0FBQztJQUNILE9BQU8sT0FBTyxDQUFDO0FBQ25CLENBQUM7Ozs7OztBQUVELFNBQVMsaUJBQWlCLENBQUMsUUFBMEIsRUFBRSxTQUFpQjtJQUNwRSxPQUFPLFFBQVEsQ0FBQyxHQUFHOzs7O0lBQUMsT0FBTyxDQUFDLEVBQUU7O2NBQ3BCLFFBQVEsR0FBRyxZQUFZLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQzs7Y0FDM0MsSUFBSSxxQkFDSCxPQUFPLElBQ1YsTUFBTSxFQUFFLFFBQVEsR0FDbkI7UUFDRCxJQUFJLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDZixJQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQztZQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7U0FDNUQ7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDLEVBQUMsQ0FBQztBQUNQLENBQUM7Ozs7OztBQUVELFNBQVMsWUFBWSxDQUFDLElBQW9CLEVBQUUsU0FBaUI7SUFDekQsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsRUFBRTtRQUMvQixPQUFPLElBQUksQ0FBQztLQUNmO1NBQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1FBQ25CLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJOzs7O1FBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxFQUFDLENBQUM7S0FDM0Q7U0FBTTtRQUNILE9BQU8sS0FBSyxDQUFDO0tBQ2hCO0FBQ0wsQ0FBQzs7Ozs7O0FBRUQsU0FBUyxXQUFXLENBQUMsUUFBMEIsRUFBRSxXQUEyQjs7VUFFbEUsa0JBQWtCLEdBQUcsUUFBUSxDQUFDLElBQUk7Ozs7SUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxXQUFXLEVBQUM7SUFDaEUsT0FBTyxRQUFRLENBQUMsR0FBRzs7OztJQUFDLE9BQU8sQ0FBQyxFQUFFOztjQUVwQixJQUFJLHFCQUNILE9BQU8sQ0FDYjtRQUVELElBQUksa0JBQWtCLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxPQUFPLEtBQUssV0FBVyxFQUFFO1lBQzdELElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1NBQ3JCO1FBQ0QsSUFBSSxPQUFPLEtBQUssV0FBVyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1NBQzdCO1FBQ0QsSUFBSSxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ2YsSUFBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQztTQUN4RDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUMsRUFBQyxDQUFDO0FBQ1AsQ0FBQzs7Ozs7O0FBRUQsU0FBUyxXQUFXLENBQUMsUUFBMEIsRUFBRSxJQUFZO0lBRXpELE9BQU8sUUFBUSxDQUFDLEdBQUc7Ozs7SUFBQyxPQUFPLENBQUMsRUFBRTs7Y0FFcEIsSUFBSSxxQkFDSCxPQUFPLENBQ2I7UUFFRCxJQUFJLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDZixJQUFJLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxFQUFDLENBQUM7WUFDN0YsSUFBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztTQUNqRDthQUFNO1lBQ0gsSUFBSSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQzlDO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQyxFQUFDLENBQUM7QUFDUCxDQUFDOzs7Ozs7QUFFRCxTQUFTLFlBQVksQ0FBQyxJQUFvQixFQUFFLElBQVk7SUFDcEQsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxNQUFNLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUV0RyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBY3Rpb24sIGNyZWF0ZVJlZHVjZXIsIG9ufSBmcm9tICdAbmdyeC9zdG9yZSc7XHJcbmltcG9ydCAqIGFzIE5hdmlnYXRpb25BY3Rpb25zIGZyb20gJy4vbmF2aWdhdGlvbi5hY3Rpb25zJztcclxuaW1wb3J0IHtOYXZpZ2F0aW9uSXRlbXN9IGZyb20gJy4uLy4uL2NvcmUubmF2aWdhdGlvbic7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIE5hdmlnYXRpb25JdGVtIHtcclxuICAgIGJhZGdlOiBhbnk7XHJcbiAgICBpdGVtOiBhbnk7XHJcbiAgICBuYW1lOiBzdHJpbmc7XHJcbiAgICB0aXRsZTogc3RyaW5nO1xyXG4gICAgaWNvbj86IHN0cmluZztcclxuICAgIHRhZ3M/OiBzdHJpbmc7XHJcbiAgICByb3V0ZXJMaW5rPzogc3RyaW5nO1xyXG4gICAgdXJsPzogc3RyaW5nO1xyXG4gICAgYWN0aXZlPzogYm9vbGVhbjtcclxuICAgIG9wZW4/OiBib29sZWFuO1xyXG4gICAgaXRlbXM/OiBOYXZpZ2F0aW9uSXRlbVtdO1xyXG4gICAgbWF0Y2hlZD86IGJvb2xlYW47XHJcbiAgICBuYXZUaXRsZT86IGJvb2xlYW47XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgTmF2aWdhdGlvblN0YXRlIHtcclxuICAgIGl0ZW1zOiBOYXZpZ2F0aW9uSXRlbVtdO1xyXG4gICAgdG90YWw6IG51bWJlcjtcclxuICAgIGZpbHRlckFjdGl2ZTogYm9vbGVhbjtcclxuICAgIGZpbHRlclRleHQ6IHN0cmluZztcclxuICAgIG1hdGNoZWQ6IG51bWJlcjtcclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IGluaXRpYWxTdGF0ZTogTmF2aWdhdGlvblN0YXRlID0ge1xyXG4gICAgaXRlbXM6IGRlY29yYXRlSXRlbXMoTmF2aWdhdGlvbkl0ZW1zKSxcclxuICAgIHRvdGFsOiBjb3VudFRvdGFsKE5hdmlnYXRpb25JdGVtcyksXHJcbiAgICBmaWx0ZXJBY3RpdmU6IGZhbHNlLFxyXG4gICAgZmlsdGVyVGV4dDogJycsXHJcbiAgICBtYXRjaGVkOiAwXHJcbn07XHJcblxyXG5cclxuY29uc3QgbmF2aWdhdGlvblJlZHVjZXIgPSBjcmVhdGVSZWR1Y2VyKFxyXG4gICAgaW5pdGlhbFN0YXRlLFxyXG4gICAgb24oTmF2aWdhdGlvbkFjdGlvbnMuYWN0aXZlVXJsLCAoc3RhdGUsIGFjdGlvbikgPT4gKHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICBpdGVtczogZGV0ZWN0QWN0aXZlSXRlbXMoc3RhdGUuaXRlbXMsIGFjdGlvbi51cmwpXHJcbiAgICB9KSksXHJcbiAgICBvbihOYXZpZ2F0aW9uQWN0aW9ucy50b2dnbGVOYXZTZWN0aW9uLCAoc3RhdGUsIGFjdGlvbikgPT4gKHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICBpdGVtczogdG9nZ2xlSXRlbXMoc3RhdGUuaXRlbXMsIGFjdGlvbi5pdGVtKVxyXG4gICAgfSkpLFxyXG4gICAgb24oTmF2aWdhdGlvbkFjdGlvbnMudG9nZ2xlTmF2aWdhdGlvbkZpbHRlciwgc3RhdGUgPT4ge1xyXG4gICAgICAgIGlmIChzdGF0ZS5maWx0ZXJBY3RpdmUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyQWN0aXZlOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIG1hdGNoZWQ6IDAsXHJcbiAgICAgICAgICAgICAgICBpdGVtczogc3RhdGUuaXRlbXMubWFwKF8gPT4gKHsuLi5fLCBtYXRjaGVkOiBudWxsfSkpXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgaXRlbXMgPSBmaWx0ZXJJdGVtcyhzdGF0ZS5pdGVtcywgc3RhdGUuZmlsdGVyVGV4dCk7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIGZpbHRlckFjdGl2ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGl0ZW1zLFxyXG4gICAgICAgICAgICAgICAgbWF0Y2hlZDogY291bnRNYXRjaGVkKGl0ZW1zKSxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfSksXHJcbiAgICBvbihOYXZpZ2F0aW9uQWN0aW9ucy5uYXZpZ2F0aW9uRmlsdGVyLCAoc3RhdGUsIGFjdGlvbikgPT4ge1xyXG4gICAgICAgIGNvbnN0IGl0ZW1zID0gZmlsdGVySXRlbXMoc3RhdGUuaXRlbXMsIGFjdGlvbi50ZXh0KTtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgZmlsdGVyVGV4dDogYWN0aW9uLnRleHQsXHJcbiAgICAgICAgICAgIGl0ZW1zLFxyXG4gICAgICAgICAgICBtYXRjaGVkOiBjb3VudE1hdGNoZWQoaXRlbXMpLFxyXG4gICAgICAgIH07XHJcbiAgICB9KVxyXG4pO1xyXG5cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiByZWR1Y2VyKHN0YXRlOiBOYXZpZ2F0aW9uU3RhdGUsIGFjdGlvbjogQWN0aW9uKSB7XHJcbiAgICByZXR1cm4gbmF2aWdhdGlvblJlZHVjZXIoc3RhdGUsIGFjdGlvbik7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGRlY29yYXRlSXRlbXMobmF2SXRlbXM6IE5hdmlnYXRpb25JdGVtW10pOiBOYXZpZ2F0aW9uSXRlbVtdIHtcclxuICAgIHJldHVybiBuYXZJdGVtcy5tYXAobmF2SXRlbSA9PiB7XHJcbiAgICAgICAgY29uc3QgaXRlbTogTmF2aWdhdGlvbkl0ZW0gPSB7XHJcbiAgICAgICAgICAgIC4uLm5hdkl0ZW0sXHJcbiAgICAgICAgICAgIGFjdGl2ZTogZmFsc2UsXHJcbiAgICAgICAgICAgIG1hdGNoZWQ6IG51bGxcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmIChuYXZJdGVtLml0ZW1zKSB7XHJcbiAgICAgICAgICAgIGl0ZW0ub3BlbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpdGVtLml0ZW1zID0gZGVjb3JhdGVJdGVtcyhuYXZJdGVtLml0ZW1zKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGl0ZW0ubmF2VGl0bGUgPSAhbmF2SXRlbS5pdGVtcyAmJiAhbmF2SXRlbS5yb3V0ZXJMaW5rICYmICEhbmF2SXRlbS50aXRsZTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGl0ZW07XHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gY291bnRUb3RhbChuYXZJdGVtczogTmF2aWdhdGlvbkl0ZW1bXSk6IG51bWJlciB7XHJcbiAgICBsZXQgdG90YWwgPSBuYXZJdGVtcy5sZW5ndGg7XHJcbiAgICBuYXZJdGVtcy5maWx0ZXIoXyA9PiAhIV8uaXRlbXMpLmZvckVhY2goXyA9PiB7XHJcbiAgICAgICAgdG90YWwgKz0gY291bnRUb3RhbChfLml0ZW1zKTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIHRvdGFsO1xyXG59XHJcblxyXG5mdW5jdGlvbiBjb3VudE1hdGNoZWQobmF2SXRlbXM6IE5hdmlnYXRpb25JdGVtW10pOiBudW1iZXIge1xyXG4gICAgbGV0IG1hdGNoZWQgPSBuYXZJdGVtcy5maWx0ZXIoXyA9PiAhIV8ubWF0Y2hlZCkubGVuZ3RoO1xyXG4gICAgbmF2SXRlbXMuZmlsdGVyKF8gPT4gISFfLml0ZW1zKS5mb3JFYWNoKF8gPT4ge1xyXG4gICAgICAgIG1hdGNoZWQgKz0gY291bnRNYXRjaGVkKF8uaXRlbXMpO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gbWF0Y2hlZDtcclxufVxyXG5cclxuZnVuY3Rpb24gZGV0ZWN0QWN0aXZlSXRlbXMobmF2SXRlbXM6IE5hdmlnYXRpb25JdGVtW10sIGFjdGl2ZVVybDogc3RyaW5nKTogTmF2aWdhdGlvbkl0ZW1bXSB7XHJcbiAgICByZXR1cm4gbmF2SXRlbXMubWFwKG5hdkl0ZW0gPT4ge1xyXG4gICAgICAgIGNvbnN0IGlzQWN0aXZlID0gaXRlbUlzQWN0aXZlKG5hdkl0ZW0sIGFjdGl2ZVVybCk7XHJcbiAgICAgICAgY29uc3QgaXRlbSA9IHtcclxuICAgICAgICAgICAgLi4ubmF2SXRlbSxcclxuICAgICAgICAgICAgYWN0aXZlOiBpc0FjdGl2ZVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgaWYgKG5hdkl0ZW0uaXRlbXMpIHtcclxuICAgICAgICAgICAgaXRlbS5vcGVuID0gaXNBY3RpdmU7XHJcbiAgICAgICAgICAgIGl0ZW0uaXRlbXMgPSBkZXRlY3RBY3RpdmVJdGVtcyhuYXZJdGVtLml0ZW1zLCBhY3RpdmVVcmwpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBpdGVtSXNBY3RpdmUoaXRlbTogTmF2aWdhdGlvbkl0ZW0sIGFjdGl2ZVVybDogc3RyaW5nKSB7XHJcbiAgICBpZiAoaXRlbS5yb3V0ZXJMaW5rID09PSBhY3RpdmVVcmwpIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAoaXRlbS5pdGVtcykge1xyXG4gICAgICAgIHJldHVybiBpdGVtLml0ZW1zLnNvbWUoXyA9PiBpdGVtSXNBY3RpdmUoXywgYWN0aXZlVXJsKSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxufVxyXG5cclxuZnVuY3Rpb24gdG9nZ2xlSXRlbXMobmF2SXRlbXM6IE5hdmlnYXRpb25JdGVtW10sIHRvZ2dsZWRJdGVtOiBOYXZpZ2F0aW9uSXRlbSk6IE5hdmlnYXRpb25JdGVtW10ge1xyXG5cclxuICAgIGNvbnN0IGlzVG9nZ2xlZEl0ZW1MZXZlbCA9IG5hdkl0ZW1zLnNvbWUoXyA9PiBfID09PSB0b2dnbGVkSXRlbSk7XHJcbiAgICByZXR1cm4gbmF2SXRlbXMubWFwKG5hdkl0ZW0gPT4ge1xyXG5cclxuICAgICAgICBjb25zdCBpdGVtID0ge1xyXG4gICAgICAgICAgICAuLi5uYXZJdGVtXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKGlzVG9nZ2xlZEl0ZW1MZXZlbCAmJiBpdGVtLml0ZW1zICYmIG5hdkl0ZW0gIT09IHRvZ2dsZWRJdGVtKSB7XHJcbiAgICAgICAgICAgIGl0ZW0ub3BlbiA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAobmF2SXRlbSA9PT0gdG9nZ2xlZEl0ZW0pIHtcclxuICAgICAgICAgICAgaXRlbS5vcGVuID0gIW5hdkl0ZW0ub3BlbjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKG5hdkl0ZW0uaXRlbXMpIHtcclxuICAgICAgICAgICAgaXRlbS5pdGVtcyA9IHRvZ2dsZUl0ZW1zKG5hdkl0ZW0uaXRlbXMsIHRvZ2dsZWRJdGVtKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGl0ZW07XHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gZmlsdGVySXRlbXMobmF2SXRlbXM6IE5hdmlnYXRpb25JdGVtW10sIHRleHQ6IHN0cmluZyk6IE5hdmlnYXRpb25JdGVtW10ge1xyXG5cclxuICAgIHJldHVybiBuYXZJdGVtcy5tYXAobmF2SXRlbSA9PiB7XHJcblxyXG4gICAgICAgIGNvbnN0IGl0ZW0gPSB7XHJcbiAgICAgICAgICAgIC4uLm5hdkl0ZW0sXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKG5hdkl0ZW0uaXRlbXMpIHtcclxuICAgICAgICAgICAgaXRlbS5tYXRjaGVkID0gbmF2SXRlbU1hdGNoKG5hdkl0ZW0sIHRleHQpIHx8IG5hdkl0ZW0uaXRlbXMuc29tZShfID0+IG5hdkl0ZW1NYXRjaChfLCB0ZXh0KSk7XHJcbiAgICAgICAgICAgIGl0ZW0uaXRlbXMgPSBmaWx0ZXJJdGVtcyhuYXZJdGVtLml0ZW1zLCB0ZXh0KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpdGVtLm1hdGNoZWQgPSBuYXZJdGVtTWF0Y2gobmF2SXRlbSwgdGV4dCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIG5hdkl0ZW1NYXRjaChpdGVtOiBOYXZpZ2F0aW9uSXRlbSwgdGV4dDogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gKCF0ZXh0LnRyaW0oKSB8fCAoaXRlbS50YWdzICYmICEhaXRlbS50YWdzLm1hdGNoKG5ldyBSZWdFeHAoYC4qJHt0ZXh0LnRyaW0oKX0uKmAsICdnaScpKSkpO1xyXG5cclxufVxyXG4iXX0=