/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/navigation/navigation.effects.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { BreakpointObserver } from '@angular/cdk/layout';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { activeUrl, mobileNavigation } from './navigation.actions';
import { handleClassCondition } from '../utils/utils.functions';
export class NavigationEffects {
    /**
     * @param {?} actions$
     * @param {?} breakpointObserver
     */
    constructor(actions$, breakpointObserver) {
        this.actions$ = actions$;
        this.mapToActiveUrl$ = createEffect((/**
         * @return {?}
         */
        () => this.actions$.pipe(ofType(ROUTER_NAVIGATED), map((/**
         * @param {?} action
         * @return {?}
         */
        (action) => activeUrl({ url: action.payload.event.url }))), tap((/**
         * @param {?} action
         * @return {?}
         */
        action => handleClassCondition(false, 'mobile-nav-on', document.querySelector('body')))))));
        this.mobileNavigation$ = createEffect((/**
         * @return {?}
         */
        () => this.actions$.pipe(ofType(mobileNavigation), tap((/**
         * @param {?} action
         * @return {?}
         */
        action => handleClassCondition(action.open, 'mobile-nav-on', document.querySelector('body')))))), { dispatch: false });
        breakpointObserver.observe('(max-width: 600px)').subscribe((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            handleClassCondition(result.matches, 'mobile-view-activated', document.querySelector('body'));
        }));
    }
}
NavigationEffects.decorators = [
    { type: Injectable }
];
/** @nocollapse */
NavigationEffects.ctorParameters = () => [
    { type: Actions },
    { type: BreakpointObserver }
];
if (false) {
    /** @type {?} */
    NavigationEffects.prototype.mapToActiveUrl$;
    /** @type {?} */
    NavigationEffects.prototype.mobileNavigation$;
    /**
     * @type {?}
     * @private
     */
    NavigationEffects.prototype.actions$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5lZmZlY3RzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9uYXZpZ2F0aW9uL25hdmlnYXRpb24uZWZmZWN0cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUN4QyxPQUFPLEVBQUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDNUQsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxFQUFDLGdCQUFnQixFQUF3QixNQUFNLG9CQUFvQixDQUFDO0FBQzNFLE9BQU8sRUFBQyxTQUFTLEVBQUUsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRSxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUc5RCxNQUFNLE9BQU8saUJBQWlCOzs7OztJQWExQixZQUFvQixRQUFpQixFQUFFLGtCQUFzQztRQUF6RCxhQUFRLEdBQVIsUUFBUSxDQUFTO1FBWHJDLG9CQUFlLEdBQUcsWUFBWTs7O1FBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQ25ELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxFQUN4QixHQUFHOzs7O1FBQUMsQ0FBQyxNQUE2QixFQUFFLEVBQUUsQ0FBQyxTQUFTLENBQUMsRUFBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxFQUNsRixHQUFHOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEVBQUUsZUFBZSxFQUFFLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxDQUM5RixFQUFDLENBQUM7UUFFSCxzQkFBaUIsR0FBRyxZQUFZOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FDckQsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEVBQ3hCLEdBQUc7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsZUFBZSxFQUFFLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsRUFDL0YsQ0FBQyxHQUFFLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxDQUFDLENBQUM7UUFHdkIsa0JBQWtCLENBQUMsT0FBTyxDQUN0QixvQkFBb0IsQ0FDdkIsQ0FBQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUU7WUFDakIsb0JBQW9CLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDbEcsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7WUFwQkosVUFBVTs7OztZQU5ILE9BQU87WUFDUCxrQkFBa0I7Ozs7SUFRdEIsNENBSUc7O0lBRUgsOENBRzJCOzs7OztJQUVmLHFDQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7bWFwLCB0YXB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHtBY3Rpb25zLCBjcmVhdGVFZmZlY3QsIG9mVHlwZX0gZnJvbSAnQG5ncngvZWZmZWN0cyc7XHJcbmltcG9ydCB7QnJlYWtwb2ludE9ic2VydmVyfSBmcm9tICdAYW5ndWxhci9jZGsvbGF5b3V0JztcclxuaW1wb3J0IHtST1VURVJfTkFWSUdBVEVELCBSb3V0ZXJOYXZpZ2F0ZWRBY3Rpb259IGZyb20gJ0BuZ3J4L3JvdXRlci1zdG9yZSc7XHJcbmltcG9ydCB7YWN0aXZlVXJsLCBtb2JpbGVOYXZpZ2F0aW9ufSBmcm9tICcuL25hdmlnYXRpb24uYWN0aW9ucyc7XHJcbmltcG9ydCB7aGFuZGxlQ2xhc3NDb25kaXRpb259IGZyb20gJy4uL3V0aWxzL3V0aWxzLmZ1bmN0aW9ucyc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBOYXZpZ2F0aW9uRWZmZWN0cyB7XHJcblxyXG4gICAgbWFwVG9BY3RpdmVVcmwkID0gY3JlYXRlRWZmZWN0KCgpID0+IHRoaXMuYWN0aW9ucyQucGlwZShcclxuICAgICAgICBvZlR5cGUoUk9VVEVSX05BVklHQVRFRCksXHJcbiAgICAgICAgbWFwKChhY3Rpb246IFJvdXRlck5hdmlnYXRlZEFjdGlvbikgPT4gYWN0aXZlVXJsKHt1cmw6IGFjdGlvbi5wYXlsb2FkLmV2ZW50LnVybH0pKSxcclxuICAgICAgICB0YXAoYWN0aW9uID0+IGhhbmRsZUNsYXNzQ29uZGl0aW9uKGZhbHNlLCAnbW9iaWxlLW5hdi1vbicsIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2JvZHknKSkpXHJcbiAgICApKTtcclxuXHJcbiAgICBtb2JpbGVOYXZpZ2F0aW9uJCA9IGNyZWF0ZUVmZmVjdCgoKSA9PiB0aGlzLmFjdGlvbnMkLnBpcGUoXHJcbiAgICAgICAgb2ZUeXBlKG1vYmlsZU5hdmlnYXRpb24pLFxyXG4gICAgICAgIHRhcChhY3Rpb24gPT4gaGFuZGxlQ2xhc3NDb25kaXRpb24oYWN0aW9uLm9wZW4sICdtb2JpbGUtbmF2LW9uJywgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignYm9keScpKVxyXG4gICAgICAgICkpLCB7ZGlzcGF0Y2g6IGZhbHNlfSk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhY3Rpb25zJDogQWN0aW9ucywgYnJlYWtwb2ludE9ic2VydmVyOiBCcmVha3BvaW50T2JzZXJ2ZXIpIHtcclxuICAgICAgICBicmVha3BvaW50T2JzZXJ2ZXIub2JzZXJ2ZShcclxuICAgICAgICAgICAgJyhtYXgtd2lkdGg6IDYwMHB4KScsXHJcbiAgICAgICAgKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgaGFuZGxlQ2xhc3NDb25kaXRpb24ocmVzdWx0Lm1hdGNoZXMsICdtb2JpbGUtdmlldy1hY3RpdmF0ZWQnLCBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdib2R5JykpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuXHJcblxyXG4iXX0=