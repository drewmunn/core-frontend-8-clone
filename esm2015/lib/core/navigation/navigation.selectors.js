/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/navigation/navigation.selectors.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { createFeatureSelector, createSelector } from '@ngrx/store';
/** @type {?} */
export const selectNavigationState = createFeatureSelector('navigation');
const ɵ0 = /**
 * @param {?} state
 * @return {?}
 */
state => state.items;
/** @type {?} */
export const selectNavigationItems = createSelector(selectNavigationState, (ɵ0));
const ɵ1 = /**
 * @param {?} state
 * @return {?}
 */
state => ({
    active: state.filterActive,
    text: state.filterText
});
/** @type {?} */
export const selectFilter = createSelector(selectNavigationState, (ɵ1));
const ɵ2 = /**
 * @param {?} state
 * @return {?}
 */
state => ({
    active: state.filterActive && !!state.filterText.trim(),
    total: state.total,
    matched: state.matched
});
/** @type {?} */
export const selectResult = createSelector(selectNavigationState, (ɵ2));
export { ɵ0, ɵ1, ɵ2 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5zZWxlY3RvcnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL25hdmlnYXRpb24vbmF2aWdhdGlvbi5zZWxlY3RvcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMscUJBQXFCLEVBQUUsY0FBYyxFQUFDLE1BQU0sYUFBYSxDQUFDOztBQUdsRSxNQUFNLE9BQU8scUJBQXFCLEdBQUcscUJBQXFCLENBQWtCLFlBQVksQ0FBQzs7Ozs7QUFFZCxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLOztBQUEvRixNQUFNLE9BQU8scUJBQXFCLEdBQUcsY0FBYyxDQUFDLHFCQUFxQixPQUF1Qjs7Ozs7QUFDOUIsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3hFLE1BQU0sRUFBRSxLQUFLLENBQUMsWUFBWTtJQUMxQixJQUFJLEVBQUUsS0FBSyxDQUFDLFVBQVU7Q0FDekIsQ0FBQzs7QUFIRixNQUFNLE9BQU8sWUFBWSxHQUFHLGNBQWMsQ0FBQyxxQkFBcUIsT0FHN0Q7Ozs7O0FBRytELEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN4RSxNQUFNLEVBQUUsS0FBSyxDQUFDLFlBQVksSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUU7SUFDdkQsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLO0lBQ2xCLE9BQU8sRUFBRSxLQUFLLENBQUMsT0FBTztDQUN6QixDQUFDOztBQUpGLE1BQU0sT0FBTyxZQUFZLEdBQUcsY0FBYyxDQUFDLHFCQUFxQixPQUk3RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Y3JlYXRlRmVhdHVyZVNlbGVjdG9yLCBjcmVhdGVTZWxlY3Rvcn0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHtOYXZpZ2F0aW9uU3RhdGV9IGZyb20gJy4vbmF2aWdhdGlvbi5yZWR1Y2VyJztcblxuZXhwb3J0IGNvbnN0IHNlbGVjdE5hdmlnYXRpb25TdGF0ZSA9IGNyZWF0ZUZlYXR1cmVTZWxlY3RvcjxOYXZpZ2F0aW9uU3RhdGU+KCduYXZpZ2F0aW9uJyk7XG5cbmV4cG9ydCBjb25zdCBzZWxlY3ROYXZpZ2F0aW9uSXRlbXMgPSBjcmVhdGVTZWxlY3RvcihzZWxlY3ROYXZpZ2F0aW9uU3RhdGUsIHN0YXRlID0+IHN0YXRlLml0ZW1zKTtcbmV4cG9ydCBjb25zdCBzZWxlY3RGaWx0ZXIgPSBjcmVhdGVTZWxlY3RvcihzZWxlY3ROYXZpZ2F0aW9uU3RhdGUsIHN0YXRlID0+ICh7XG4gICAgYWN0aXZlOiBzdGF0ZS5maWx0ZXJBY3RpdmUsXG4gICAgdGV4dDogc3RhdGUuZmlsdGVyVGV4dFxufSkpO1xuXG5cbmV4cG9ydCBjb25zdCBzZWxlY3RSZXN1bHQgPSBjcmVhdGVTZWxlY3RvcihzZWxlY3ROYXZpZ2F0aW9uU3RhdGUsIHN0YXRlID0+ICh7XG4gICAgYWN0aXZlOiBzdGF0ZS5maWx0ZXJBY3RpdmUgJiYgISFzdGF0ZS5maWx0ZXJUZXh0LnRyaW0oKSxcbiAgICB0b3RhbDogc3RhdGUudG90YWwsXG4gICAgbWF0Y2hlZDogc3RhdGUubWF0Y2hlZFxufSkpO1xuIl19