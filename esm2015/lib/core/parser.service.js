/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/parser.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class ParserService {
    /**
     * @param {?} text
     * @param {?} find
     * @param {?} replace
     * @return {?}
     */
    replaceAll(text, find, replace) {
        /**
         * @param {?} textString
         * @return {?}
         */
        function escape(textString) {
            return textString.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
        }
        return text.replace(new RegExp(escape(find), 'g'), replace);
    }
    /**
     * @param {?} obj
     * @param {?=} prefix
     * @return {?}
     */
    serializeRgQuery(obj, prefix) {
        /** @type {?} */
        const str = [];
        /** @type {?} */
        const keys = [];
        for (const k in obj) {
            if (obj.hasOwnProperty(k)) {
                keys.push(k);
            }
        }
        keys.sort();
        for (let i = 0; i < keys.length; i++) {
            /** @type {?} */
            let k = prefix ? prefix + '[' + keys[i] + ']' : keys[i];
            /** @type {?} */
            let v = obj[keys[i]];
            str.push(typeof v == 'object' ?
                encodeURIComponent(k) + '=' + encodeURIComponent(JSON.stringify(v)).replace(/%3A/g, ':') :
                encodeURIComponent(k) + '=' + encodeURIComponent(v));
        }
        /** @type {?} */
        let finalStr = str.join('&');
        finalStr = this.replaceAll(finalStr, '%40', '@');
        finalStr = this.replaceAll(finalStr, '%3A', ':');
        finalStr = this.replaceAll(finalStr, '%2C', ',');
        finalStr = this.replaceAll(finalStr, '%20', '+');
        return finalStr;
    }
    /**
     * @param {?} cname
     * @return {?}
     */
    getCookie(cname) {
        /** @type {?} */
        const name = cname + '=';
        /** @type {?} */
        const decodedCookie = decodeURIComponent(document.cookie);
        /** @type {?} */
        const ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            /** @type {?} */
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return false;
    }
    /**
     * @param {?} name
     * @param {?} path
     * @param {?=} domain
     * @return {?}
     */
    deleteCookie(name, path, domain) {
        if (this.getCookie(name)) {
            try {
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') +
                    ((domain) ? ';domain=' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') +
                    ((domain) ? ';domain=.' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') + ';domain=' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((path) ? ';path=' + path : '') + ';domain=.' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((domain) ? ';domain=' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ((domain) ? ';domain=.' + domain : '') +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ';domain=' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                document.cookie = name + '=' +
                    ';domain=.' + window.location.hostname +
                    ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
            }
            catch (err) {
            }
        }
    }
}
ParserService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ ParserService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ParserService_Factory() { return new ParserService(); }, token: ParserService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFyc2VyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3BhcnNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQzs7QUFNekMsTUFBTSxPQUFPLGFBQWE7Ozs7Ozs7SUFFdEIsVUFBVSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsT0FBTzs7Ozs7UUFDMUIsU0FBUyxNQUFNLENBQUMsVUFBVTtZQUN0QixPQUFPLFVBQVUsQ0FBQyxPQUFPLENBQUMsNkJBQTZCLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDckUsQ0FBQztRQUVELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDaEUsQ0FBQzs7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLE1BQU87O2NBQ25CLEdBQUcsR0FBRyxFQUFFOztjQUNSLElBQUksR0FBRyxFQUFFO1FBQ2YsS0FBSyxNQUFNLENBQUMsSUFBSSxHQUFHLEVBQUU7WUFDakIsSUFBSSxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2hCO1NBQ0o7UUFDRCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDWixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7Z0JBQzlCLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs7Z0JBQ25ELENBQUMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksUUFBUSxDQUFDLENBQUM7Z0JBQzNCLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUMxRixrQkFBa0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUM1RDs7WUFDRyxRQUFRLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDNUIsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNqRCxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDakQsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztRQUVqRCxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDOzs7OztJQUVELFNBQVMsQ0FBQyxLQUFLOztjQUNMLElBQUksR0FBRyxLQUFLLEdBQUcsR0FBRzs7Y0FDbEIsYUFBYSxHQUFHLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7O2NBQ25ELEVBQUUsR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUNuQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7Z0JBQzVCLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2IsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtnQkFDeEIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdEI7WUFDRCxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUN2QixPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDN0M7U0FDSjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7Ozs7SUFFRCxZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFPO1FBQzVCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN0QixJQUFJO2dCQUNBLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLEdBQUc7b0JBQ3hCLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUMvQixDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDckMsd0NBQXdDLENBQUM7Z0JBQzdDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLEdBQUc7b0JBQ3hCLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUMvQixDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDdEMsd0NBQXdDLENBQUM7Z0JBRTdDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLEdBQUc7b0JBQ3hCLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsVUFBVSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUTtvQkFDdkUsd0NBQXdDLENBQUM7Z0JBQzdDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLEdBQUc7b0JBQ3hCLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsV0FBVyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUTtvQkFDeEUsd0NBQXdDLENBQUM7Z0JBRTdDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLEdBQUc7b0JBQ3hCLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUNyQyx3Q0FBd0MsQ0FBQztnQkFDN0MsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsR0FBRztvQkFDeEIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ3RDLHdDQUF3QyxDQUFDO2dCQUU3QyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksR0FBRyxHQUFHO29CQUN4QixVQUFVLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNyQyx3Q0FBd0MsQ0FBQztnQkFDN0MsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsR0FBRztvQkFDeEIsV0FBVyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUTtvQkFDdEMsd0NBQXdDLENBQUM7YUFFaEQ7WUFBQyxPQUFPLEdBQUcsRUFBRTthQUNiO1NBQ0o7SUFDTCxDQUFDOzs7WUEzRkosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcblxuZXhwb3J0IGNsYXNzIFBhcnNlclNlcnZpY2Uge1xuXG4gICAgcmVwbGFjZUFsbCh0ZXh0LCBmaW5kLCByZXBsYWNlKSB7XG4gICAgICAgIGZ1bmN0aW9uIGVzY2FwZSh0ZXh0U3RyaW5nKSB7XG4gICAgICAgICAgICByZXR1cm4gdGV4dFN0cmluZy5yZXBsYWNlKC8oWy4qKz9ePSE6JHt9KCl8XFxbXFxdXFwvXFxcXF0pL2csICdcXFxcJDEnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0ZXh0LnJlcGxhY2UobmV3IFJlZ0V4cChlc2NhcGUoZmluZCksICdnJyksIHJlcGxhY2UpO1xuICAgIH1cblxuICAgIHNlcmlhbGl6ZVJnUXVlcnkob2JqLCBwcmVmaXg/KSB7XG4gICAgICAgIGNvbnN0IHN0ciA9IFtdO1xuICAgICAgICBjb25zdCBrZXlzID0gW107XG4gICAgICAgIGZvciAoY29uc3QgayBpbiBvYmopIHtcbiAgICAgICAgICAgIGlmIChvYmouaGFzT3duUHJvcGVydHkoaykpIHtcbiAgICAgICAgICAgICAgICBrZXlzLnB1c2goayk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAga2V5cy5zb3J0KCk7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IGsgPSBwcmVmaXggPyBwcmVmaXggKyAnWycgKyBrZXlzW2ldICsgJ10nIDoga2V5c1tpXSxcbiAgICAgICAgICAgICAgICB2ID0gb2JqW2tleXNbaV1dO1xuICAgICAgICAgICAgc3RyLnB1c2godHlwZW9mIHYgPT0gJ29iamVjdCcgP1xuICAgICAgICAgICAgICAgIGVuY29kZVVSSUNvbXBvbmVudChrKSArICc9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeSh2KSkucmVwbGFjZSgvJTNBL2csICc6JykgOlxuICAgICAgICAgICAgICAgIGVuY29kZVVSSUNvbXBvbmVudChrKSArICc9JyArIGVuY29kZVVSSUNvbXBvbmVudCh2KSk7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGZpbmFsU3RyID0gc3RyLmpvaW4oJyYnKTtcbiAgICAgICAgZmluYWxTdHIgPSB0aGlzLnJlcGxhY2VBbGwoZmluYWxTdHIsICclNDAnLCAnQCcpO1xuICAgICAgICBmaW5hbFN0ciA9IHRoaXMucmVwbGFjZUFsbChmaW5hbFN0ciwgJyUzQScsICc6Jyk7XG4gICAgICAgIGZpbmFsU3RyID0gdGhpcy5yZXBsYWNlQWxsKGZpbmFsU3RyLCAnJTJDJywgJywnKTtcbiAgICAgICAgZmluYWxTdHIgPSB0aGlzLnJlcGxhY2VBbGwoZmluYWxTdHIsICclMjAnLCAnKycpO1xuXG4gICAgICAgIHJldHVybiBmaW5hbFN0cjtcbiAgICB9XG5cbiAgICBnZXRDb29raWUoY25hbWUpIHtcbiAgICAgICAgY29uc3QgbmFtZSA9IGNuYW1lICsgJz0nO1xuICAgICAgICBjb25zdCBkZWNvZGVkQ29va2llID0gZGVjb2RlVVJJQ29tcG9uZW50KGRvY3VtZW50LmNvb2tpZSk7XG4gICAgICAgIGNvbnN0IGNhID0gZGVjb2RlZENvb2tpZS5zcGxpdCgnOycpO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNhLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgYyA9IGNhW2ldO1xuICAgICAgICAgICAgd2hpbGUgKGMuY2hhckF0KDApID09PSAnICcpIHtcbiAgICAgICAgICAgICAgICBjID0gYy5zdWJzdHJpbmcoMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoYy5pbmRleE9mKG5hbWUpID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGMuc3Vic3RyaW5nKG5hbWUubGVuZ3RoLCBjLmxlbmd0aCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGRlbGV0ZUNvb2tpZShuYW1lLCBwYXRoLCBkb21haW4/KSB7XG4gICAgICAgIGlmICh0aGlzLmdldENvb2tpZShuYW1lKSkge1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSBuYW1lICsgJz0nICtcbiAgICAgICAgICAgICAgICAgICAgKChwYXRoKSA/ICc7cGF0aD0nICsgcGF0aCA6ICcnKSArXG4gICAgICAgICAgICAgICAgICAgICgoZG9tYWluKSA/ICc7ZG9tYWluPScgKyBkb21haW4gOiAnJykgK1xuICAgICAgICAgICAgICAgICAgICAnO2V4cGlyZXM9VGh1LCAwMSBKYW4gMTk3MCAwMDowMDowMSBHTVQnO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LmNvb2tpZSA9IG5hbWUgKyAnPScgK1xuICAgICAgICAgICAgICAgICAgICAoKHBhdGgpID8gJztwYXRoPScgKyBwYXRoIDogJycpICtcbiAgICAgICAgICAgICAgICAgICAgKChkb21haW4pID8gJztkb21haW49LicgKyBkb21haW4gOiAnJykgK1xuICAgICAgICAgICAgICAgICAgICAnO2V4cGlyZXM9VGh1LCAwMSBKYW4gMTk3MCAwMDowMDowMSBHTVQnO1xuXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuY29va2llID0gbmFtZSArICc9JyArXG4gICAgICAgICAgICAgICAgICAgICgocGF0aCkgPyAnO3BhdGg9JyArIHBhdGggOiAnJykgKyAnO2RvbWFpbj0nICsgd2luZG93LmxvY2F0aW9uLmhvc3RuYW1lICtcbiAgICAgICAgICAgICAgICAgICAgJztleHBpcmVzPVRodSwgMDEgSmFuIDE5NzAgMDA6MDA6MDEgR01UJztcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSBuYW1lICsgJz0nICtcbiAgICAgICAgICAgICAgICAgICAgKChwYXRoKSA/ICc7cGF0aD0nICsgcGF0aCA6ICcnKSArICc7ZG9tYWluPS4nICsgd2luZG93LmxvY2F0aW9uLmhvc3RuYW1lICtcbiAgICAgICAgICAgICAgICAgICAgJztleHBpcmVzPVRodSwgMDEgSmFuIDE5NzAgMDA6MDA6MDEgR01UJztcblxuICAgICAgICAgICAgICAgIGRvY3VtZW50LmNvb2tpZSA9IG5hbWUgKyAnPScgK1xuICAgICAgICAgICAgICAgICAgICAoKGRvbWFpbikgPyAnO2RvbWFpbj0nICsgZG9tYWluIDogJycpICtcbiAgICAgICAgICAgICAgICAgICAgJztleHBpcmVzPVRodSwgMDEgSmFuIDE5NzAgMDA6MDA6MDEgR01UJztcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSBuYW1lICsgJz0nICtcbiAgICAgICAgICAgICAgICAgICAgKChkb21haW4pID8gJztkb21haW49LicgKyBkb21haW4gOiAnJykgK1xuICAgICAgICAgICAgICAgICAgICAnO2V4cGlyZXM9VGh1LCAwMSBKYW4gMTk3MCAwMDowMDowMSBHTVQnO1xuXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuY29va2llID0gbmFtZSArICc9JyArXG4gICAgICAgICAgICAgICAgICAgICc7ZG9tYWluPScgKyB3aW5kb3cubG9jYXRpb24uaG9zdG5hbWUgK1xuICAgICAgICAgICAgICAgICAgICAnO2V4cGlyZXM9VGh1LCAwMSBKYW4gMTk3MCAwMDowMDowMSBHTVQnO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LmNvb2tpZSA9IG5hbWUgKyAnPScgK1xuICAgICAgICAgICAgICAgICAgICAnO2RvbWFpbj0uJyArIHdpbmRvdy5sb2NhdGlvbi5ob3N0bmFtZSArXG4gICAgICAgICAgICAgICAgICAgICc7ZXhwaXJlcz1UaHUsIDAxIEphbiAxOTcwIDAwOjAwOjAxIEdNVCc7XG5cbiAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG59XG4iXX0=