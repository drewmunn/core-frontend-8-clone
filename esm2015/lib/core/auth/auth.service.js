/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/auth/auth.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, forkJoin, throwError } from 'rxjs';
import { APP_CONFIG } from '../../app.config';
import { TabsModule } from 'ngx-bootstrap';
import { Angulartics2 } from 'angulartics2';
import { ParserService } from '../parser.service';
import { CacheService } from '../cache.service';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { PageService } from '../pages/page.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common/http";
import * as i3 from "angulartics2";
import * as i4 from "../parser.service";
import * as i5 from "../cache.service";
import * as i6 from "../pages/page.service";
export class AuthService {
    /**
     * @param {?} router
     * @param {?} api
     * @param {?} angulartics
     * @param {?} parser
     * @param {?} cache
     * @param {?} pages
     */
    constructor(router, api, angulartics, parser, cache, pages) {
        this.router = router;
        this.api = api;
        this.angulartics = angulartics;
        this.parser = parser;
        this.cache = cache;
        this.pages = pages;
        this.authChangeEmit = new BehaviorSubject(true);
        this.failedLogins = 0;
        this.loginLimit = 4;
    }
    /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    canActivate(next, state) {
        if (!this.authResumed) {
            this.resume();
        }
        if (this.isAuthenticated()) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }
    /**
     * @return {?}
     */
    isAuthenticated() {
        return this.getUser() || this.cookiesExist();
    }
    /**
     * @return {?}
     */
    getUser() {
        return this.user;
    }
    /**
     * @param {?} username
     * @param {?} password
     * @param {?=} recaptchaResponse
     * @param {?=} authToken
     * @return {?}
     */
    login(username, password, recaptchaResponse, authToken) {
        this.reset();
        /** @type {?} */
        const user = {
            username,
            password,
            recaptchaResponse,
            authToken,
            mfa_token: this.mfaToken,
            google2step: this.google2step
        };
        return this.api.post('auth', user, { observe: 'response' })
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            this.user = response.body;
            APP_CONFIG.authHeaders.xUserName = response.headers.get('x-service-user-name');
            APP_CONFIG.authHeaders.xUserToken = response.headers.get('x-service-user-token');
            document.cookie = 'x-service-user-name=' + response.headers.get('x-service-user-name') + ';path=/';
            document.cookie = 'x-service-user-token=' + response.headers.get('x-service-user-token') + ';path=/';
            if (response.headers.get('x-service-info')) {
                /** @type {?} */
                const serviceInfo = JSON.parse(response.headers.get('x-service-info'));
                this.thirdPartyOnline = serviceInfo.thirdPartyOnline || false;
            }
            return this.initialise().subscribe((/**
             * @return {?}
             */
            () => {
                this.angulartics.eventTrack.next({
                    action: 'Auth succeeded',
                });
                return this.router.navigate(['/']);
            }), (/**
             * @param {?} resp
             * @return {?}
             */
            (resp) => {
                this.angulartics.eventTrack.next({
                    action: 'Auth failed',
                });
                if (this.parser.getCookie('x-service-login-token')) {
                    if ((/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))))
                        > this.failedLogins) {
                        this.failedLogins = (/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))));
                    }
                }
                document.cookie = 'x-service-login-token=' + btoa((/** @type {?} */ ((/** @type {?} */ ((this.failedLogins + 1)))))) + ';path=/';
                if (this.failedLogins >= this.loginLimit) {
                    this.recaptchaEnabled = true;
                }
                return throwError('Failed to authenticate');
            }));
        })));
    }
    /**
     * @return {?}
     */
    resume() {
        this.authResumed = true;
        if (this.parser.getCookie('x-service-user-name') || this.parser.getCookie('x-service-emulated-user-name')) {
            APP_CONFIG.authHeaders.xUserName = this.parser.getCookie('x-service-user-name');
            APP_CONFIG.authHeaders.xUserToken = this.parser.getCookie('x-service-user-token');
            this.isEmulated = this.parser.getCookie('x-service-emulated-user-name') !== false;
            if (this.parser.getCookie('x-service-emulated-user-name')) {
                APP_CONFIG.authHeaders.xUserName = this.parser.getCookie('x-service-emulated-user-name');
            }
            return this.api.get('auth', { observe: 'response' }).toPromise().then((/**
             * @param {?} response
             * @return {?}
             */
            (response) => {
                this.user = response.body;
                if (response.headers.get('x-service-info')) {
                    /** @type {?} */
                    const serviceInfo = JSON.parse(response.headers.get('x-service-info'));
                    this.thirdPartyOnline = serviceInfo.thirdPartyOnline || false;
                }
                this.authChangeEmit.next(true);
                return this.initialise().subscribe();
            })).catch((/**
             * @return {?}
             */
            () => {
                this.reset();
                return this.router.navigate(['/login']);
            }));
        }
        else {
            if (this.parser.getCookie('x-service-login-token')) {
                if ((/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))))
                    > this.failedLogins) {
                    this.failedLogins = (/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))));
                }
            }
            if ((/** @type {?} */ (this.failedLogins)) >= this.loginLimit) {
                this.recaptchaEnabled = true;
            }
        }
        return false;
    }
    /**
     * @private
     * @return {?}
     */
    getServiceProvider() {
        return this.cache.getOneThroughCache('service-provider', 2, {}, false).subscribe((/**
         * @param {?} provider
         * @return {?}
         */
        provider => {
            this.serviceProvider = provider;
            if (this.serviceProvider.status !== 'ACTIVE') {
                throwError('Service provider offline');
                return this.router.navigate(['/maintenance']);
            }
            this.recaptchaEnabled = this.serviceProvider.data.recaptchaEnabled;
        }));
    }
    /**
     * @return {?}
     */
    initialise() {
        /** @type {?} */
        const observables = [];
        if (this.authCheckTimer) {
            clearInterval(this.authCheckTimer);
        }
        // @TODO: await
        this.getServiceProvider();
        if (this.isAuthenticated()) {
            if (!this.analyticsLoaded) {
                observables.push(this.api.get('analytics-custom-vars').toPromise().then((/**
                 * @param {?} response
                 * @return {?}
                 */
                response => {
                    for (const dimension of (/** @type {?} */ (response))) {
                        for (const [key, value] of Object.entries(dimension)) {
                            try {
                                ga('set', key, value);
                                if (key === 'dimension1') {
                                    try {
                                        ga('set', 'userId', value);
                                        if (gclog) {
                                            gclog.setCustomAttribute('userId', value);
                                        }
                                    }
                                    catch (e) {
                                    }
                                    this.angulartics.eventTrack.next({
                                        action: 'Analytics initialised',
                                    });
                                }
                            }
                            catch (err) {
                            }
                        }
                    }
                })));
            }
            this.authCheckTimer = setInterval((/**
             * @return {?}
             */
            () => {
                return this.triggerPing();
            }), 2500);
        }
        return forkJoin(observables);
    }
    /**
     * @return {?}
     */
    cookiesExist() {
        return this.parser.getCookie('x-service-user-name') !== false
            || this.parser.getCookie('x-service-emulated-user-name') !== false;
    }
    /**
     * @return {?}
     */
    triggerPing() {
        if (!this.cookiesExist()) {
            this.reset();
            return this.router.navigate(['/login']);
        }
    }
    /**
     * @return {?}
     */
    reset() {
        this.user = false;
        this.cache.refresh();
        this.pages.refresh();
        this.analyticsLoaded = false;
        this.thirdPartyOnline = false;
        this.parser.deleteCookie('x-service-user-name', '/');
        this.parser.deleteCookie('x-service-emulated-user-name', '/');
        this.parser.deleteCookie('x-service-user-token', '/');
        this.parser.deleteCookie('TPSESSION', '/');
        APP_CONFIG.authHeaders = Object.assign({}, APP_CONFIG.authHeaders, {
            xUserName: null,
            xUserToken: null,
        });
        this.authChangeEmit.next(true);
        if (this.authCheckTimer) {
            clearInterval(this.authCheckTimer);
        }
    }
    /**
     * @return {?}
     */
    logout() {
        this.reset();
        this.router.navigate(['/login']);
        return this.api.delete('auth').subscribe((/**
         * @return {?}
         */
        () => {
            this.angulartics.eventTrack.next({
                action: 'Auth closed',
            });
        }), (/**
         * @return {?}
         */
        () => {
            this.angulartics.eventTrack.next({
                action: 'Auth closure failed',
            });
        }));
    }
}
AuthService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
    { type: NgModule, args: [{
                imports: [
                    TabsModule,
                ],
            },] }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: Router },
    { type: HttpClient },
    { type: Angulartics2 },
    { type: ParserService },
    { type: CacheService },
    { type: PageService }
];
/** @nocollapse */ AuthService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthService_Factory() { return new AuthService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.HttpClient), i0.ɵɵinject(i3.Angulartics2), i0.ɵɵinject(i4.ParserService), i0.ɵɵinject(i5.CacheService), i0.ɵɵinject(i6.PageService)); }, token: AuthService, providedIn: "root" });
if (false) {
    /** @type {?} */
    AuthService.prototype.authChangeEmit;
    /** @type {?} */
    AuthService.prototype.authResumed;
    /** @type {?} */
    AuthService.prototype.user;
    /** @type {?} */
    AuthService.prototype.mfaToken;
    /** @type {?} */
    AuthService.prototype.google2step;
    /** @type {?} */
    AuthService.prototype.analyticsLoaded;
    /** @type {?} */
    AuthService.prototype.thirdPartyOnline;
    /** @type {?} */
    AuthService.prototype.recaptchaEnabled;
    /** @type {?} */
    AuthService.prototype.serviceProvider;
    /** @type {?} */
    AuthService.prototype.authCheckTimer;
    /** @type {?} */
    AuthService.prototype.isEmulated;
    /** @type {?} */
    AuthService.prototype.failedLogins;
    /** @type {?} */
    AuthService.prototype.loginLimit;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.api;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.angulartics;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.parser;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.cache;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.pages;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9hdXRoL2F1dGguc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUUsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBc0MsTUFBTSxFQUFzQixNQUFNLGlCQUFpQixDQUFDO0FBQ2pHLE9BQU8sRUFBQyxlQUFlLEVBQUUsUUFBUSxFQUE0QixVQUFVLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFDckYsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGtCQUFrQixDQUFDO0FBQzVDLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGNBQWMsQ0FBQztBQUMxQyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFDaEQsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGtCQUFrQixDQUFDO0FBQzlDLE9BQU8sRUFBQyxHQUFHLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNuQyxPQUFPLEVBQUMsVUFBVSxFQUFlLE1BQU0sc0JBQXNCLENBQUM7QUFDOUQsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHVCQUF1QixDQUFDOzs7Ozs7OztBQWVsRCxNQUFNLE9BQU8sV0FBVzs7Ozs7Ozs7O0lBdUJwQixZQUNZLE1BQWMsRUFDZCxHQUFlLEVBQ2YsV0FBeUIsRUFDekIsTUFBcUIsRUFDckIsS0FBbUIsRUFDbkIsS0FBa0I7UUFMbEIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLFFBQUcsR0FBSCxHQUFHLENBQVk7UUFDZixnQkFBVyxHQUFYLFdBQVcsQ0FBYztRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFlO1FBQ3JCLFVBQUssR0FBTCxLQUFLLENBQWM7UUFDbkIsVUFBSyxHQUFMLEtBQUssQ0FBYTtRQTNCOUIsbUJBQWMsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQWtCM0MsaUJBQVksR0FBRyxDQUFDLENBQUM7UUFDakIsZUFBVSxHQUFHLENBQUMsQ0FBQztJQVVmLENBQUM7Ozs7OztJQUVELFdBQVcsQ0FBQyxJQUE0QixFQUFFLEtBQTBCO1FBQ2hFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ25CLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUNqQjtRQUNELElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRSxFQUFFO1lBQ3hCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDakMsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUVELGVBQWU7UUFDWCxPQUFPLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDakQsQ0FBQzs7OztJQUVELE9BQU87UUFDSCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQzs7Ozs7Ozs7SUFFRCxLQUFLLENBQUMsUUFBZ0IsRUFBRSxRQUFnQixFQUFFLGlCQUEwQixFQUFFLFNBQWtCO1FBQ3BGLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7Y0FFUCxJQUFJLEdBQUc7WUFDVCxRQUFRO1lBQ1IsUUFBUTtZQUNSLGlCQUFpQjtZQUNqQixTQUFTO1lBQ1QsU0FBUyxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3hCLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVztTQUNoQztRQUVELE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQXVCLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBQyxPQUFPLEVBQUUsVUFBVSxFQUFDLENBQUM7YUFDMUUsSUFBSSxDQUNELEdBQUc7Ozs7UUFDQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQ1QsSUFBSSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBRTFCLFVBQVUsQ0FBQyxXQUFXLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDL0UsVUFBVSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUVqRixRQUFRLENBQUMsTUFBTSxHQUFHLHNCQUFzQixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLEdBQUcsU0FBUyxDQUFDO1lBQ25HLFFBQVEsQ0FBQyxNQUFNLEdBQUcsdUJBQXVCLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsR0FBRyxTQUFTLENBQUM7WUFFckcsSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFOztzQkFDbEMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDdEUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQyxnQkFBZ0IsSUFBSSxLQUFLLENBQUM7YUFDakU7WUFFRCxPQUFPLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxTQUFTOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztvQkFDN0IsTUFBTSxFQUFFLGdCQUFnQjtpQkFDM0IsQ0FBQyxDQUFDO2dCQUNILE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3ZDLENBQUM7Ozs7WUFBRSxDQUFDLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztvQkFDN0IsTUFBTSxFQUFFLGFBQWE7aUJBQ3hCLENBQUMsQ0FBQztnQkFFSCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLEVBQUU7b0JBQ2hELElBQUksbUJBQUEsbUJBQUEsSUFBSSxDQUFDLG1CQUFBLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLEVBQVUsQ0FBQyxFQUFXLEVBQVU7MEJBQ2pGLElBQUksQ0FBQyxZQUFZLEVBQUU7d0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsbUJBQUEsbUJBQUEsSUFBSSxDQUFDLG1CQUFBLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLEVBQVUsQ0FBQyxFQUFXLEVBQVUsQ0FBQztxQkFDM0c7aUJBQ0o7Z0JBQ0QsUUFBUSxDQUFDLE1BQU0sR0FBRyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsbUJBQUEsbUJBQUEsQ0FBQyxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxFQUFXLEVBQVUsQ0FBQyxHQUFHLFNBQVMsQ0FBQztnQkFFNUcsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ3RDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7aUJBQ2hDO2dCQUVELE9BQU8sVUFBVSxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDaEQsQ0FBQyxFQUFDLENBQUM7UUFFUCxDQUFDLEVBQ0osQ0FDSixDQUFDO0lBQ1YsQ0FBQzs7OztJQUVELE1BQU07UUFDRixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsOEJBQThCLENBQUMsRUFBRTtZQUV2RyxVQUFVLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ2hGLFVBQVUsQ0FBQyxXQUFXLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDbEYsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyw4QkFBOEIsQ0FBQyxLQUFLLEtBQUssQ0FBQztZQUVsRixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUFDLEVBQUU7Z0JBQ3ZELFVBQVUsQ0FBQyxXQUFXLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUFDLENBQUM7YUFDNUY7WUFFRCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxFQUFDLE9BQU8sRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO2dCQUM3RSxJQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7Z0JBRTFCLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsRUFBRTs7MEJBQ2xDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7b0JBQ3RFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxXQUFXLENBQUMsZ0JBQWdCLElBQUksS0FBSyxDQUFDO2lCQUNqRTtnQkFFRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFL0IsT0FBTyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7WUFFekMsQ0FBQyxFQUFDLENBQUMsS0FBSzs7O1lBQUMsR0FBRyxFQUFFO2dCQUNWLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDYixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUM1QyxDQUFDLEVBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLEVBQUU7Z0JBQ2hELElBQUksbUJBQUEsbUJBQUEsSUFBSSxDQUFDLG1CQUFBLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLEVBQVUsQ0FBQyxFQUFXLEVBQVU7c0JBQ2pGLElBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsbUJBQUEsbUJBQUEsSUFBSSxDQUFDLG1CQUFBLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLEVBQVUsQ0FBQyxFQUFXLEVBQVUsQ0FBQztpQkFDM0c7YUFDSjtZQUNELElBQUksbUJBQUEsSUFBSSxDQUFDLFlBQVksRUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7YUFDaEM7U0FDSjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7O0lBRU8sa0JBQWtCO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUN4RixJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQztZQUNoQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxLQUFLLFFBQVEsRUFBRTtnQkFDMUMsVUFBVSxDQUFDLDBCQUEwQixDQUFDLENBQUM7Z0JBQ3ZDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2FBQ2pEO1lBQ0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQ3ZFLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELFVBQVU7O2NBQ0EsV0FBVyxHQUFHLEVBQUU7UUFFdEIsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3JCLGFBQWEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDdEM7UUFFRCxlQUFlO1FBQ2YsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFFMUIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFLEVBQUU7WUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3ZCLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJOzs7O2dCQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUMvRSxLQUFLLE1BQU0sU0FBUyxJQUFJLG1CQUFBLFFBQVEsRUFBTyxFQUFFO3dCQUNyQyxLQUFLLE1BQU0sQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRTs0QkFDbEQsSUFBSTtnQ0FDQSxFQUFFLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztnQ0FDdEIsSUFBSSxHQUFHLEtBQUssWUFBWSxFQUFFO29DQUN0QixJQUFJO3dDQUNBLEVBQUUsQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO3dDQUMzQixJQUFJLEtBQUssRUFBRTs0Q0FDUCxLQUFLLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO3lDQUM3QztxQ0FDSjtvQ0FBQyxPQUFPLENBQUMsRUFBRTtxQ0FDWDtvQ0FDRCxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7d0NBQzdCLE1BQU0sRUFBRSx1QkFBdUI7cUNBQ2xDLENBQUMsQ0FBQztpQ0FDTjs2QkFDSjs0QkFBQyxPQUFPLEdBQUcsRUFBRTs2QkFDYjt5QkFDSjtxQkFDSjtnQkFDTCxDQUFDLEVBQUMsQ0FBQyxDQUFDO2FBQ1A7WUFFRCxJQUFJLENBQUMsY0FBYyxHQUFHLFdBQVc7OztZQUFDLEdBQUcsRUFBRTtnQkFDbkMsT0FBTyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUIsQ0FBQyxHQUFFLElBQUksQ0FBQyxDQUFDO1NBQ1o7UUFFRCxPQUFPLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7O0lBRUQsWUFBWTtRQUNSLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsS0FBSyxLQUFLO2VBQ3RELElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUFDLEtBQUssS0FBSyxDQUFDO0lBQzNFLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFBRTtZQUN0QixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDYixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztTQUMzQztJQUNMLENBQUM7Ozs7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7UUFFbEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBRXJCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFFOUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMscUJBQXFCLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsOEJBQThCLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsc0JBQXNCLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRTNDLFVBQVUsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLFdBQVcsRUFBRTtZQUMvRCxTQUFTLEVBQUUsSUFBSTtZQUNmLFVBQVUsRUFBRSxJQUFJO1NBQ25CLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRS9CLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNyQixhQUFhLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ3RDO0lBQ0wsQ0FBQzs7OztJQUVELE1BQU07UUFDRixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDYixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDakMsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUU7WUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO2dCQUM3QixNQUFNLEVBQUUsYUFBYTthQUN4QixDQUFDLENBQUM7UUFDUCxDQUFDOzs7UUFBRSxHQUFHLEVBQUU7WUFDSixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7Z0JBQzdCLE1BQU0sRUFBRSxxQkFBcUI7YUFDaEMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7WUE1UUosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCO1lBRUEsUUFBUSxTQUFDO2dCQUNOLE9BQU8sRUFBRTtvQkFDTCxVQUFVO2lCQUNiO2FBQ0o7Ozs7WUF0QjRDLE1BQU07WUFRM0MsVUFBVTtZQUpWLFlBQVk7WUFDWixhQUFhO1lBQ2IsWUFBWTtZQUdaLFdBQVc7Ozs7O0lBaUJmLHFDQUEyQzs7SUFFM0Msa0NBQXFCOztJQUNyQiwyQkFBVTs7SUFDViwrQkFBaUI7O0lBQ2pCLGtDQUFxQjs7SUFDckIsc0NBQXlCOztJQUN6Qix1Q0FBMEI7O0lBQzFCLHVDQUEwQjs7SUFDMUIsc0NBTUU7O0lBQ0YscUNBQW9COztJQUNwQixpQ0FBb0I7O0lBQ3BCLG1DQUFpQjs7SUFDakIsaUNBQWU7Ozs7O0lBR1gsNkJBQXNCOzs7OztJQUN0QiwwQkFBdUI7Ozs7O0lBQ3ZCLGtDQUFpQzs7Ozs7SUFDakMsNkJBQTZCOzs7OztJQUM3Qiw0QkFBMkI7Ozs7O0lBQzNCLDRCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZSwgTmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBDYW5BY3RpdmF0ZSwgUm91dGVyLCBSb3V0ZXJTdGF0ZVNuYXBzaG90fSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHtCZWhhdmlvclN1YmplY3QsIGZvcmtKb2luLCBPYnNlcnZhYmxlLCBTdWJzY3JpcHRpb24sIHRocm93RXJyb3J9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtBUFBfQ09ORklHfSBmcm9tICcuLi8uLi9hcHAuY29uZmlnJztcbmltcG9ydCB7VGFic01vZHVsZX0gZnJvbSAnbmd4LWJvb3RzdHJhcCc7XG5pbXBvcnQge0FuZ3VsYXJ0aWNzMn0gZnJvbSAnYW5ndWxhcnRpY3MyJztcbmltcG9ydCB7UGFyc2VyU2VydmljZX0gZnJvbSAnLi4vcGFyc2VyLnNlcnZpY2UnO1xuaW1wb3J0IHtDYWNoZVNlcnZpY2V9IGZyb20gJy4uL2NhY2hlLnNlcnZpY2UnO1xuaW1wb3J0IHttYXB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7SHR0cENsaWVudCwgSHR0cFJlc3BvbnNlfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge1BhZ2VTZXJ2aWNlfSBmcm9tICcuLi9wYWdlcy9wYWdlLnNlcnZpY2UnO1xuXG5kZWNsYXJlIGxldCBnYTogKHA6IHN0cmluZywgcGFnZTogc3RyaW5nLCB1cmxBZnRlclJlZGlyZWN0cz86IHVua25vd24pID0+IHZvaWQ7XG5kZWNsYXJlIGxldCBnY2xvZzogYW55O1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtcbiAgICAgICAgVGFic01vZHVsZSxcbiAgICBdLFxufSlcblxuZXhwb3J0IGNsYXNzIEF1dGhTZXJ2aWNlIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUge1xuXG4gICAgYXV0aENoYW5nZUVtaXQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KHRydWUpO1xuXG4gICAgYXV0aFJlc3VtZWQ6IGJvb2xlYW47XG4gICAgdXNlcjogYW55O1xuICAgIG1mYVRva2VuOiBzdHJpbmc7XG4gICAgZ29vZ2xlMnN0ZXA6IGJvb2xlYW47XG4gICAgYW5hbHl0aWNzTG9hZGVkOiBib29sZWFuO1xuICAgIHRoaXJkUGFydHlPbmxpbmU6IGJvb2xlYW47XG4gICAgcmVjYXB0Y2hhRW5hYmxlZDogYm9vbGVhbjtcbiAgICBzZXJ2aWNlUHJvdmlkZXI6IHtcbiAgICAgICAgbmFtZTogc3RyaW5nO1xuICAgICAgICBkYXRhOiBhbnk7XG4gICAgICAgIHN0YXR1czogc3RyaW5nO1xuICAgICAgICBsYW5ndWFnZXM6IGFueTtcbiAgICAgICAgY29uZmlndXJhdGlvbjogYW55O1xuICAgIH07XG4gICAgYXV0aENoZWNrVGltZXI6IGFueTtcbiAgICBpc0VtdWxhdGVkOiBib29sZWFuO1xuICAgIGZhaWxlZExvZ2lucyA9IDA7XG4gICAgbG9naW5MaW1pdCA9IDQ7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICAgICAgcHJpdmF0ZSBhcGk6IEh0dHBDbGllbnQsXG4gICAgICAgIHByaXZhdGUgYW5ndWxhcnRpY3M6IEFuZ3VsYXJ0aWNzMixcbiAgICAgICAgcHJpdmF0ZSBwYXJzZXI6IFBhcnNlclNlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgY2FjaGU6IENhY2hlU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBwYWdlczogUGFnZVNlcnZpY2VcbiAgICApIHtcbiAgICB9XG5cbiAgICBjYW5BY3RpdmF0ZShuZXh0OiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IE9ic2VydmFibGU8Ym9vbGVhbj4gfCBQcm9taXNlPGJvb2xlYW4+IHwgYm9vbGVhbiB7XG4gICAgICAgIGlmICghdGhpcy5hdXRoUmVzdW1lZCkge1xuICAgICAgICAgICAgdGhpcy5yZXN1bWUoKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5pc0F1dGhlbnRpY2F0ZWQoKSkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbG9naW4nXSk7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBpc0F1dGhlbnRpY2F0ZWQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldFVzZXIoKSB8fCB0aGlzLmNvb2tpZXNFeGlzdCgpO1xuICAgIH1cblxuICAgIGdldFVzZXIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnVzZXI7XG4gICAgfVxuXG4gICAgbG9naW4odXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZywgcmVjYXB0Y2hhUmVzcG9uc2U/OiBzdHJpbmcsIGF1dGhUb2tlbj86IHN0cmluZyk6IE9ic2VydmFibGU8U3Vic2NyaXB0aW9uPiB7XG4gICAgICAgIHRoaXMucmVzZXQoKTtcblxuICAgICAgICBjb25zdCB1c2VyID0ge1xuICAgICAgICAgICAgdXNlcm5hbWUsXG4gICAgICAgICAgICBwYXNzd29yZCxcbiAgICAgICAgICAgIHJlY2FwdGNoYVJlc3BvbnNlLFxuICAgICAgICAgICAgYXV0aFRva2VuLFxuICAgICAgICAgICAgbWZhX3Rva2VuOiB0aGlzLm1mYVRva2VuLFxuICAgICAgICAgICAgZ29vZ2xlMnN0ZXA6IHRoaXMuZ29vZ2xlMnN0ZXBcbiAgICAgICAgfTtcblxuICAgICAgICByZXR1cm4gdGhpcy5hcGkucG9zdDxIdHRwUmVzcG9uc2U8b2JqZWN0Pj4oJ2F1dGgnLCB1c2VyLCB7b2JzZXJ2ZTogJ3Jlc3BvbnNlJ30pXG4gICAgICAgICAgICAucGlwZShcbiAgICAgICAgICAgICAgICBtYXAoXG4gICAgICAgICAgICAgICAgICAgIChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyID0gcmVzcG9uc2UuYm9keTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgQVBQX0NPTkZJRy5hdXRoSGVhZGVycy54VXNlck5hbWUgPSByZXNwb25zZS5oZWFkZXJzLmdldCgneC1zZXJ2aWNlLXVzZXItbmFtZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgQVBQX0NPTkZJRy5hdXRoSGVhZGVycy54VXNlclRva2VuID0gcmVzcG9uc2UuaGVhZGVycy5nZXQoJ3gtc2VydmljZS11c2VyLXRva2VuJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LmNvb2tpZSA9ICd4LXNlcnZpY2UtdXNlci1uYW1lPScgKyByZXNwb25zZS5oZWFkZXJzLmdldCgneC1zZXJ2aWNlLXVzZXItbmFtZScpICsgJztwYXRoPS8nO1xuICAgICAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuY29va2llID0gJ3gtc2VydmljZS11c2VyLXRva2VuPScgKyByZXNwb25zZS5oZWFkZXJzLmdldCgneC1zZXJ2aWNlLXVzZXItdG9rZW4nKSArICc7cGF0aD0vJztcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCd4LXNlcnZpY2UtaW5mbycpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VydmljZUluZm8gPSBKU09OLnBhcnNlKHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCd4LXNlcnZpY2UtaW5mbycpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRoaXJkUGFydHlPbmxpbmUgPSBzZXJ2aWNlSW5mby50aGlyZFBhcnR5T25saW5lIHx8IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5pbml0aWFsaXNlKCkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFuZ3VsYXJ0aWNzLmV2ZW50VHJhY2submV4dCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ0F1dGggc3VjY2VlZGVkJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvJ10pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSwgKHJlc3ApID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFuZ3VsYXJ0aWNzLmV2ZW50VHJhY2submV4dCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ0F1dGggZmFpbGVkJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnBhcnNlci5nZXRDb29raWUoJ3gtc2VydmljZS1sb2dpbi10b2tlbicpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhdG9iKHRoaXMucGFyc2VyLmdldENvb2tpZSgneC1zZXJ2aWNlLWxvZ2luLXRva2VuJykgYXMgc3RyaW5nKSBhcyB1bmtub3duIGFzIG51bWJlclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPiB0aGlzLmZhaWxlZExvZ2lucykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5mYWlsZWRMb2dpbnMgPSBhdG9iKHRoaXMucGFyc2VyLmdldENvb2tpZSgneC1zZXJ2aWNlLWxvZ2luLXRva2VuJykgYXMgc3RyaW5nKSBhcyB1bmtub3duIGFzIG51bWJlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSAneC1zZXJ2aWNlLWxvZ2luLXRva2VuPScgKyBidG9hKCh0aGlzLmZhaWxlZExvZ2lucyArIDEpIGFzIHVua25vd24gYXMgc3RyaW5nKSArICc7cGF0aD0vJztcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmZhaWxlZExvZ2lucyA+PSB0aGlzLmxvZ2luTGltaXQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWNhcHRjaGFFbmFibGVkID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcignRmFpbGVkIHRvIGF1dGhlbnRpY2F0ZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICApO1xuICAgIH1cblxuICAgIHJlc3VtZSgpIHtcbiAgICAgICAgdGhpcy5hdXRoUmVzdW1lZCA9IHRydWU7XG4gICAgICAgIGlmICh0aGlzLnBhcnNlci5nZXRDb29raWUoJ3gtc2VydmljZS11c2VyLW5hbWUnKSB8fCB0aGlzLnBhcnNlci5nZXRDb29raWUoJ3gtc2VydmljZS1lbXVsYXRlZC11c2VyLW5hbWUnKSkge1xuXG4gICAgICAgICAgICBBUFBfQ09ORklHLmF1dGhIZWFkZXJzLnhVc2VyTmFtZSA9IHRoaXMucGFyc2VyLmdldENvb2tpZSgneC1zZXJ2aWNlLXVzZXItbmFtZScpO1xuICAgICAgICAgICAgQVBQX0NPTkZJRy5hdXRoSGVhZGVycy54VXNlclRva2VuID0gdGhpcy5wYXJzZXIuZ2V0Q29va2llKCd4LXNlcnZpY2UtdXNlci10b2tlbicpO1xuICAgICAgICAgICAgdGhpcy5pc0VtdWxhdGVkID0gdGhpcy5wYXJzZXIuZ2V0Q29va2llKCd4LXNlcnZpY2UtZW11bGF0ZWQtdXNlci1uYW1lJykgIT09IGZhbHNlO1xuXG4gICAgICAgICAgICBpZiAodGhpcy5wYXJzZXIuZ2V0Q29va2llKCd4LXNlcnZpY2UtZW11bGF0ZWQtdXNlci1uYW1lJykpIHtcbiAgICAgICAgICAgICAgICBBUFBfQ09ORklHLmF1dGhIZWFkZXJzLnhVc2VyTmFtZSA9IHRoaXMucGFyc2VyLmdldENvb2tpZSgneC1zZXJ2aWNlLWVtdWxhdGVkLXVzZXItbmFtZScpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hcGkuZ2V0KCdhdXRoJywge29ic2VydmU6ICdyZXNwb25zZSd9KS50b1Byb21pc2UoKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMudXNlciA9IHJlc3BvbnNlLmJvZHk7XG5cbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuaGVhZGVycy5nZXQoJ3gtc2VydmljZS1pbmZvJykpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VydmljZUluZm8gPSBKU09OLnBhcnNlKHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCd4LXNlcnZpY2UtaW5mbycpKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50aGlyZFBhcnR5T25saW5lID0gc2VydmljZUluZm8udGhpcmRQYXJ0eU9ubGluZSB8fCBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB0aGlzLmF1dGhDaGFuZ2VFbWl0Lm5leHQodHJ1ZSk7XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5pbml0aWFsaXNlKCkuc3Vic2NyaWJlKCk7XG5cbiAgICAgICAgICAgIH0pLmNhdGNoKCgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnJlc2V0KCk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAodGhpcy5wYXJzZXIuZ2V0Q29va2llKCd4LXNlcnZpY2UtbG9naW4tdG9rZW4nKSkge1xuICAgICAgICAgICAgICAgIGlmIChhdG9iKHRoaXMucGFyc2VyLmdldENvb2tpZSgneC1zZXJ2aWNlLWxvZ2luLXRva2VuJykgYXMgc3RyaW5nKSBhcyB1bmtub3duIGFzIG51bWJlclxuICAgICAgICAgICAgICAgICAgICA+IHRoaXMuZmFpbGVkTG9naW5zKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmFpbGVkTG9naW5zID0gYXRvYih0aGlzLnBhcnNlci5nZXRDb29raWUoJ3gtc2VydmljZS1sb2dpbi10b2tlbicpIGFzIHN0cmluZykgYXMgdW5rbm93biBhcyBudW1iZXI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRoaXMuZmFpbGVkTG9naW5zIGFzIG51bWJlciA+PSB0aGlzLmxvZ2luTGltaXQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnJlY2FwdGNoYUVuYWJsZWQgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldFNlcnZpY2VQcm92aWRlcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FjaGUuZ2V0T25lVGhyb3VnaENhY2hlKCdzZXJ2aWNlLXByb3ZpZGVyJywgMiwge30sIGZhbHNlKS5zdWJzY3JpYmUocHJvdmlkZXIgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZXJ2aWNlUHJvdmlkZXIgPSBwcm92aWRlcjtcbiAgICAgICAgICAgIGlmICh0aGlzLnNlcnZpY2VQcm92aWRlci5zdGF0dXMgIT09ICdBQ1RJVkUnKSB7XG4gICAgICAgICAgICAgICAgdGhyb3dFcnJvcignU2VydmljZSBwcm92aWRlciBvZmZsaW5lJyk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL21haW50ZW5hbmNlJ10pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5yZWNhcHRjaGFFbmFibGVkID0gdGhpcy5zZXJ2aWNlUHJvdmlkZXIuZGF0YS5yZWNhcHRjaGFFbmFibGVkO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBpbml0aWFsaXNlKCkge1xuICAgICAgICBjb25zdCBvYnNlcnZhYmxlcyA9IFtdO1xuXG4gICAgICAgIGlmICh0aGlzLmF1dGhDaGVja1RpbWVyKSB7XG4gICAgICAgICAgICBjbGVhckludGVydmFsKHRoaXMuYXV0aENoZWNrVGltZXIpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gQFRPRE86IGF3YWl0XG4gICAgICAgIHRoaXMuZ2V0U2VydmljZVByb3ZpZGVyKCk7XG5cbiAgICAgICAgaWYgKHRoaXMuaXNBdXRoZW50aWNhdGVkKCkpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5hbmFseXRpY3NMb2FkZWQpIHtcbiAgICAgICAgICAgICAgICBvYnNlcnZhYmxlcy5wdXNoKHRoaXMuYXBpLmdldCgnYW5hbHl0aWNzLWN1c3RvbS12YXJzJykudG9Qcm9taXNlKCkudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgZGltZW5zaW9uIG9mIHJlc3BvbnNlIGFzIGFueSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBba2V5LCB2YWx1ZV0gb2YgT2JqZWN0LmVudHJpZXMoZGltZW5zaW9uKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhKCdzZXQnLCBrZXksIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGtleSA9PT0gJ2RpbWVuc2lvbjEnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhKCdzZXQnLCAndXNlcklkJywgdmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChnY2xvZykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBnY2xvZy5zZXRDdXN0b21BdHRyaWJ1dGUoJ3VzZXJJZCcsIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFuZ3VsYXJ0aWNzLmV2ZW50VHJhY2submV4dCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnQW5hbHl0aWNzIGluaXRpYWxpc2VkJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSkpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmF1dGhDaGVja1RpbWVyID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnRyaWdnZXJQaW5nKCk7XG4gICAgICAgICAgICB9LCAyNTAwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmb3JrSm9pbihvYnNlcnZhYmxlcyk7XG4gICAgfVxuXG4gICAgY29va2llc0V4aXN0KCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5wYXJzZXIuZ2V0Q29va2llKCd4LXNlcnZpY2UtdXNlci1uYW1lJykgIT09IGZhbHNlXG4gICAgICAgICAgICB8fCB0aGlzLnBhcnNlci5nZXRDb29raWUoJ3gtc2VydmljZS1lbXVsYXRlZC11c2VyLW5hbWUnKSAhPT0gZmFsc2U7XG4gICAgfVxuXG4gICAgdHJpZ2dlclBpbmcoKSB7XG4gICAgICAgIGlmICghdGhpcy5jb29raWVzRXhpc3QoKSkge1xuICAgICAgICAgICAgdGhpcy5yZXNldCgpO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmVzZXQoKSB7XG4gICAgICAgIHRoaXMudXNlciA9IGZhbHNlO1xuXG4gICAgICAgIHRoaXMuY2FjaGUucmVmcmVzaCgpO1xuICAgICAgICB0aGlzLnBhZ2VzLnJlZnJlc2goKTtcblxuICAgICAgICB0aGlzLmFuYWx5dGljc0xvYWRlZCA9IGZhbHNlO1xuICAgICAgICB0aGlzLnRoaXJkUGFydHlPbmxpbmUgPSBmYWxzZTtcblxuICAgICAgICB0aGlzLnBhcnNlci5kZWxldGVDb29raWUoJ3gtc2VydmljZS11c2VyLW5hbWUnLCAnLycpO1xuICAgICAgICB0aGlzLnBhcnNlci5kZWxldGVDb29raWUoJ3gtc2VydmljZS1lbXVsYXRlZC11c2VyLW5hbWUnLCAnLycpO1xuICAgICAgICB0aGlzLnBhcnNlci5kZWxldGVDb29raWUoJ3gtc2VydmljZS11c2VyLXRva2VuJywgJy8nKTtcbiAgICAgICAgdGhpcy5wYXJzZXIuZGVsZXRlQ29va2llKCdUUFNFU1NJT04nLCAnLycpO1xuXG4gICAgICAgIEFQUF9DT05GSUcuYXV0aEhlYWRlcnMgPSBPYmplY3QuYXNzaWduKHt9LCBBUFBfQ09ORklHLmF1dGhIZWFkZXJzLCB7XG4gICAgICAgICAgICB4VXNlck5hbWU6IG51bGwsXG4gICAgICAgICAgICB4VXNlclRva2VuOiBudWxsLFxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmF1dGhDaGFuZ2VFbWl0Lm5leHQodHJ1ZSk7XG5cbiAgICAgICAgaWYgKHRoaXMuYXV0aENoZWNrVGltZXIpIHtcbiAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5hdXRoQ2hlY2tUaW1lcik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBsb2dvdXQoKSB7XG4gICAgICAgIHRoaXMucmVzZXQoKTtcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbG9naW4nXSk7XG4gICAgICAgIHJldHVybiB0aGlzLmFwaS5kZWxldGUoJ2F1dGgnKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5hbmd1bGFydGljcy5ldmVudFRyYWNrLm5leHQoe1xuICAgICAgICAgICAgICAgIGFjdGlvbjogJ0F1dGggY2xvc2VkJyxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmFuZ3VsYXJ0aWNzLmV2ZW50VHJhY2submV4dCh7XG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnQXV0aCBjbG9zdXJlIGZhaWxlZCcsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG59XG4iXX0=