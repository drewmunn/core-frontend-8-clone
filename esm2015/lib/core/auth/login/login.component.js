/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/auth/login/login.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { APP_CONFIG } from '../../../app.config';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { SpinnerService } from '../../utils/spinner/spinner.service';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
export class LoginComponent {
    /**
     * @param {?} authService
     * @param {?} spinner
     */
    constructor(authService, spinner) {
        this.authService = authService;
        this.spinner = spinner;
        this.loginForm = new FormGroup({
            username: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
        this.appName = APP_CONFIG.appName;
        this.copyright = APP_CONFIG.copyright;
    }
    /**
     * @return {?}
     */
    login() {
        this.loading = true;
        this.errorMessage = false;
        this.spinner.show('login');
        return this.authService.login(this.loginForm.get('username').value, this.loginForm.get('password').value).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            this.spinner.hide('login');
            this.loading = false;
            this.errorMessage = `<p>Please check your email address and password are correct, and try again.</p>
                                    <p>Error reference: ${error.headers.get('x-service-request-id')}</small></p>`;
            return throwError(`Error Code: ${error.status}\\nMessage: ${error.message}`);
        }))).subscribe((/**
         * @return {?}
         */
        () => {
            this.spinner.hide('login');
            this.loading = false;
        }));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
LoginComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-login',
                template: "<div class=\"page-wrapper\">\n    <div class=\"page-inner bg-brand-gradient\">\n        <div class=\"page-content-wrapper bg-transparent m-0\">\n            <div class=\"height-10 w-100 shadow-lg px-4 bg-brand-gradient\">\n                <div class=\"d-flex align-items-center container p-0\">\n                    <div class=\"page-logo width-mobile-auto m-0 align-items-center justify-content-center p-0 bg-transparent bg-img-none shadow-0 height-9\">\n                        <a href=\"javascript:void(0)\" class=\"page-logo-link press-scale-down d-flex align-items-center\">\n                            <span class=\"page-logo-text mr-1\">{{appName}}</span>\n                        </a>\n                    </div>\n                </div>\n            </div>\n            <div class=\"flex-1\"\n                 style=\"background: url(assets/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover;\">\n                <div class=\"container py-4 py-lg-5 my-lg-5 px-4 px-sm-0\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12 col-md-6 col-lg-5 col-xl-4 offset-xl-4 offset-md-3\">\n                            <h1 class=\"text-white fw-300 mb-3 d-sm-block d-md-none\">\n                                Secure login\n                            </h1>\n\n                            <div class=\"card p-4 rounded-plus bg-faded\">\n\n                                <form [formGroup]=\"loginForm\" (ngSubmit)=\"login()\">\n                                    <div class=\"form-group\">\n                                        <label class=\"form-label\">Username</label>\n                                        <input type=\"email\"\n                                               formControlName=\"username\"\n                                               name=\"username\"\n                                               class=\"form-control form-control-lg\"\n                                               required=\"true\"\n                                               [required]=\"true\"/>\n                                        <div class=\"invalid-feedback\">No, you missed this one.</div>\n                                        <div class=\"help-block\">Your username</div>\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label class=\"form-label\">Password</label>\n                                        <input type=\"password\"\n                                               formControlName=\"password\"\n                                               name=\"password\"\n                                               class=\"form-control form-control-lg\"\n                                               required=\"true\"\n                                               [required]=\"true\"/>\n                                        <div class=\"invalid-feedback\">Sorry, you missed this one.</div>\n                                        <div class=\"help-block\">Your password</div>\n                                    </div>\n\n                                    <div class=\"row no-gutters\">\n                                        <div class=\"col-lg-12 pl-lg-1 my-2\">\n                                            <button id=\"js-login-btn\" type=\"submit\"\n                                                    class=\"btn btn-primary btn-block btn-lg\"\n                                                    [class.spinner]=\"loading\"\n                                                    [disabled]=\"loading || !loginForm.valid\">\n                                                <core-spinner name=\"login\"></core-spinner>\n                                                <span>Secure Login</span>\n                                            </button>\n                                        </div>\n                                    </div>\n                                </form>\n\n                                <alert *ngIf=\"errorMessage\" class=\"alert alert-danger\" [innerHTML]=\"errorMessage\"></alert>\n\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"position-absolute pos-bottom pos-left pos-right p-3 text-center text-white\"\n                         [innerHTML]=\"copyright\"></div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"
            }] }
];
/** @nocollapse */
LoginComponent.ctorParameters = () => [
    { type: AuthService },
    { type: SpinnerService }
];
if (false) {
    /** @type {?} */
    LoginComponent.prototype.loading;
    /** @type {?} */
    LoginComponent.prototype.errorMessage;
    /** @type {?} */
    LoginComponent.prototype.loginForm;
    /** @type {?} */
    LoginComponent.prototype.appName;
    /** @type {?} */
    LoginComponent.prototype.copyright;
    /**
     * @type {?}
     * @protected
     */
    LoginComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    LoginComponent.prototype.spinner;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9hdXRoL2xvZ2luL2xvZ2luLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFFaEQsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQy9DLE9BQU8sRUFBQyxXQUFXLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBQ2xFLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM1QyxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0scUNBQXFDLENBQUM7QUFFbkUsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBQzFDLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFNaEMsTUFBTSxPQUFPLGNBQWM7Ozs7O0lBYXZCLFlBQ2MsV0FBd0IsRUFDeEIsT0FBdUI7UUFEdkIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFYckMsY0FBUyxHQUFHLElBQUksU0FBUyxDQUFDO1lBQ2xCLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN0RSxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7U0FDckQsQ0FDSixDQUFDO1FBRUYsWUFBTyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7UUFDN0IsY0FBUyxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUM7SUFNakMsQ0FBQzs7OztJQUVELEtBQUs7UUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMzQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQzFHLFVBQVU7Ozs7UUFBQyxDQUFDLEtBQXdCLEVBQUUsRUFBRTtZQUNwQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHOzBEQUNzQixLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxjQUFjLENBQUM7WUFDbEcsT0FBTyxVQUFVLENBQUMsZUFBZSxLQUFLLENBQUMsTUFBTSxlQUFlLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBQ2pGLENBQUMsRUFBQyxDQUNMLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFO1lBQ2IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDekIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsUUFBUTtJQUNSLENBQUM7OztZQTFDSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLHM0SUFBcUM7YUFDeEM7Ozs7WUFUTyxXQUFXO1lBQ1gsY0FBYzs7OztJQVVsQixpQ0FBaUI7O0lBQ2pCLHNDQUErQjs7SUFFL0IsbUNBSUU7O0lBRUYsaUNBQTZCOztJQUM3QixtQ0FBaUM7Ozs7O0lBRzdCLHFDQUFrQzs7Ozs7SUFDbEMsaUNBQWlDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQge0FQUF9DT05GSUd9IGZyb20gJy4uLy4uLy4uL2FwcC5jb25maWcnO1xyXG5pbXBvcnQge0Zvcm1Db250cm9sLCBGb3JtR3JvdXAsIFZhbGlkYXRvcnN9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vYXV0aC5zZXJ2aWNlJztcclxuaW1wb3J0IHtTcGlubmVyU2VydmljZX0gZnJvbSAnLi4vLi4vdXRpbHMvc3Bpbm5lci9zcGlubmVyLnNlcnZpY2UnO1xyXG5pbXBvcnQge0h0dHBFcnJvclJlc3BvbnNlfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7Y2F0Y2hFcnJvcn0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQge3Rocm93RXJyb3J9IGZyb20gJ3J4anMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2NvcmUtbG9naW4nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2xvZ2luLmNvbXBvbmVudC5odG1sJyxcclxufSlcclxuZXhwb3J0IGNsYXNzIExvZ2luQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGxvYWRpbmc6IGJvb2xlYW47XHJcbiAgICBlcnJvck1lc3NhZ2U6IHN0cmluZyB8IGJvb2xlYW47XHJcblxyXG4gICAgbG9naW5Gb3JtID0gbmV3IEZvcm1Hcm91cCh7XHJcbiAgICAgICAgICAgIHVzZXJuYW1lOiBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLmVtYWlsXSksXHJcbiAgICAgICAgICAgIHBhc3N3b3JkOiBuZXcgRm9ybUNvbnRyb2woJycsIFZhbGlkYXRvcnMucmVxdWlyZWQpXHJcbiAgICAgICAgfVxyXG4gICAgKTtcclxuXHJcbiAgICBhcHBOYW1lID0gQVBQX0NPTkZJRy5hcHBOYW1lO1xyXG4gICAgY29weXJpZ2h0ID0gQVBQX0NPTkZJRy5jb3B5cmlnaHQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcclxuICAgICAgICBwcm90ZWN0ZWQgc3Bpbm5lcjogU3Bpbm5lclNlcnZpY2VcclxuICAgICkge1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ2luKCkge1xyXG4gICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5lcnJvck1lc3NhZ2UgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnNwaW5uZXIuc2hvdygnbG9naW4nKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5hdXRoU2VydmljZS5sb2dpbih0aGlzLmxvZ2luRm9ybS5nZXQoJ3VzZXJuYW1lJykudmFsdWUsIHRoaXMubG9naW5Gb3JtLmdldCgncGFzc3dvcmQnKS52YWx1ZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3I6IEh0dHBFcnJvclJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNwaW5uZXIuaGlkZSgnbG9naW4nKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lcnJvck1lc3NhZ2UgPSBgPHA+UGxlYXNlIGNoZWNrIHlvdXIgZW1haWwgYWRkcmVzcyBhbmQgcGFzc3dvcmQgYXJlIGNvcnJlY3QsIGFuZCB0cnkgYWdhaW4uPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD5FcnJvciByZWZlcmVuY2U6ICR7ZXJyb3IuaGVhZGVycy5nZXQoJ3gtc2VydmljZS1yZXF1ZXN0LWlkJyl9PC9zbWFsbD48L3A+YDtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGBFcnJvciBDb2RlOiAke2Vycm9yLnN0YXR1c31cXFxcbk1lc3NhZ2U6ICR7ZXJyb3IubWVzc2FnZX1gKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICApLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc3Bpbm5lci5oaWRlKCdsb2dpbicpO1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgIH1cclxuXHJcbn1cclxuIl19