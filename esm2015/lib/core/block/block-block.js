/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/block/block-block.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class Block {
    /**
     * @param {?} component
     */
    constructor(component) {
        this.component = component;
    }
}
if (false) {
    /** @type {?} */
    Block.prototype.id;
    /** @type {?} */
    Block.prototype.identifier;
    /** @type {?} */
    Block.prototype.widgetId;
    /** @type {?} */
    Block.prototype.content;
    /** @type {?} */
    Block.prototype.uid;
    /** @type {?} */
    Block.prototype.data;
    /** @type {?} */
    Block.prototype.component;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYmxvY2suanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2NrL2Jsb2NrLWJsb2NrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBRUEsTUFBTSxPQUFPLEtBQUs7Ozs7SUFTZCxZQUFtQixTQUFvQjtRQUFwQixjQUFTLEdBQVQsU0FBUyxDQUFXO0lBQ3ZDLENBQUM7Q0FDSjs7O0lBVEcsbUJBQVc7O0lBQ1gsMkJBQW1COztJQUNuQix5QkFBaUI7O0lBQ2pCLHdCQUFlOztJQUNmLG9CQUFTOztJQUNULHFCQUFVOztJQUVFLDBCQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7VHlwZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBjbGFzcyBCbG9jayB7XG5cbiAgICBpZDogbnVtYmVyO1xuICAgIGlkZW50aWZpZXI6IG51bWJlcjtcbiAgICB3aWRnZXRJZDogbnVtYmVyO1xuICAgIGNvbnRlbnQ6IGFueVtdO1xuICAgIHVpZDogYW55O1xuICAgIGRhdGE6IGFueTtcblxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBjb21wb25lbnQ6IFR5cGU8YW55Pikge1xuICAgIH1cbn1cbiJdfQ==