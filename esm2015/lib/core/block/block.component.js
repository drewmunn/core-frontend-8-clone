/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/block/block.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, ComponentFactoryResolver, Input, ViewContainerRef } from '@angular/core';
import { PageService } from '../pages/page.service';
import { Block } from './block-block';
import { BlockService } from '../pages/block.service';
export class BlockComponent {
    /**
     * @param {?} pages
     * @param {?} blocks
     * @param {?} resolver
     * @param {?} viewContainerRef
     */
    constructor(pages, blocks, resolver, viewContainerRef) {
        this.pages = pages;
        this.blocks = blocks;
        this.resolver = resolver;
        this.viewContainerRef = viewContainerRef;
    }
    /**
     * @return {?}
     */
    loadComponent() {
        if (!this.blocks.hasBlock(this.blockWrapper.identifier)) {
            throw new Error(`${this.blockWrapper.id} has not been mapped (${this.blockWrapper.identifier})`);
        }
        /** @type {?} */
        const componentFactory = this.resolver.resolveComponentFactory(this.blocks.getBlock(this.blockWrapper.identifier));
        /** @type {?} */
        const componentRef = this.viewContainerRef.createComponent(componentFactory);
        this.pages.sortBlocks(this.block);
        // @ts-ignore
        componentRef.instance.block = this.block;
        componentRef.changeDetectorRef.detectChanges();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscription = this.pages.getBlock(this.block.widgetId).then((/**
         * @param {?} block
         * @return {?}
         */
        (block) => {
            this.blockWrapper = block;
            this.loadComponent();
        }));
    }
}
BlockComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block',
                template: "<!-- BLOCK -->",
                changeDetection: ChangeDetectionStrategy.Default
            }] }
];
/** @nocollapse */
BlockComponent.ctorParameters = () => [
    { type: PageService },
    { type: BlockService },
    { type: ComponentFactoryResolver },
    { type: ViewContainerRef }
];
BlockComponent.propDecorators = {
    block: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockComponent.prototype.block;
    /** @type {?} */
    BlockComponent.prototype.blockWrapper;
    /** @type {?} */
    BlockComponent.prototype.subscription;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.pages;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.blocks;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.resolver;
    /**
     * @type {?}
     * @protected
     */
    BlockComponent.prototype.viewContainerRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2suY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9jay9ibG9jay5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQ0gsdUJBQXVCLEVBQ3ZCLFNBQVMsRUFDVCx3QkFBd0IsRUFDeEIsS0FBSyxFQUVMLGdCQUFnQixFQUNuQixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDbEQsT0FBTyxFQUFDLEtBQUssRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNwQyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFRcEQsTUFBTSxPQUFPLGNBQWM7Ozs7Ozs7SUFNdkIsWUFDYyxLQUFrQixFQUNsQixNQUFvQixFQUNwQixRQUFrQyxFQUNsQyxnQkFBa0M7UUFIbEMsVUFBSyxHQUFMLEtBQUssQ0FBYTtRQUNsQixXQUFNLEdBQU4sTUFBTSxDQUFjO1FBQ3BCLGFBQVEsR0FBUixRQUFRLENBQTBCO1FBQ2xDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFFaEQsQ0FBQzs7OztJQUVELGFBQWE7UUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUNyRCxNQUFNLElBQUksS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLHlCQUF5QixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7U0FDcEc7O2NBQ0ssZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDOztjQUM1RyxZQUFZLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQztRQUU1RSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFbEMsYUFBYTtRQUNiLFlBQVksQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDekMsWUFBWSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ25ELENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLEtBQVksRUFBRSxFQUFFO1lBQy9FLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN6QixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7OztZQXZDSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLDBCQUFxQztnQkFDckMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE9BQU87YUFDbkQ7Ozs7WUFSTyxXQUFXO1lBRVgsWUFBWTtZQVBoQix3QkFBd0I7WUFHeEIsZ0JBQWdCOzs7b0JBY2YsS0FBSzs7OztJQUFOLCtCQUFzQjs7SUFDdEIsc0NBQW9COztJQUNwQixzQ0FBa0I7Ozs7O0lBR2QsK0JBQTRCOzs7OztJQUM1QixnQ0FBOEI7Ozs7O0lBQzlCLGtDQUE0Qzs7Ozs7SUFDNUMsMENBQTRDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICAgIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxyXG4gICAgSW5wdXQsXHJcbiAgICBPbkluaXQsXHJcbiAgICBWaWV3Q29udGFpbmVyUmVmXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7UGFnZVNlcnZpY2V9IGZyb20gJy4uL3BhZ2VzL3BhZ2Uuc2VydmljZSc7XHJcbmltcG9ydCB7QmxvY2t9IGZyb20gJy4vYmxvY2stYmxvY2snO1xyXG5pbXBvcnQge0Jsb2NrU2VydmljZX0gZnJvbSAnLi4vcGFnZXMvYmxvY2suc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jaycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vYmxvY2suY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5EZWZhdWx0XHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQmxvY2tDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIGJsb2NrOiBCbG9jaztcclxuICAgIGJsb2NrV3JhcHBlcjogQmxvY2s7XHJcbiAgICBzdWJzY3JpcHRpb246IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcm90ZWN0ZWQgcGFnZXM6IFBhZ2VTZXJ2aWNlLFxyXG4gICAgICAgIHByb3RlY3RlZCBibG9ja3M6IEJsb2NrU2VydmljZSxcclxuICAgICAgICBwcm90ZWN0ZWQgcmVzb2x2ZXI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcclxuICAgICAgICBwcm90ZWN0ZWQgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZlxyXG4gICAgKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbG9hZENvbXBvbmVudCgpIHtcclxuICAgICAgICBpZiAoIXRoaXMuYmxvY2tzLmhhc0Jsb2NrKHRoaXMuYmxvY2tXcmFwcGVyLmlkZW50aWZpZXIpKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgJHt0aGlzLmJsb2NrV3JhcHBlci5pZH0gaGFzIG5vdCBiZWVuIG1hcHBlZCAoJHt0aGlzLmJsb2NrV3JhcHBlci5pZGVudGlmaWVyfSlgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgY29tcG9uZW50RmFjdG9yeSA9IHRoaXMucmVzb2x2ZXIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkodGhpcy5ibG9ja3MuZ2V0QmxvY2sodGhpcy5ibG9ja1dyYXBwZXIuaWRlbnRpZmllcikpO1xyXG4gICAgICAgIGNvbnN0IGNvbXBvbmVudFJlZiA9IHRoaXMudmlld0NvbnRhaW5lclJlZi5jcmVhdGVDb21wb25lbnQoY29tcG9uZW50RmFjdG9yeSk7XHJcblxyXG4gICAgICAgIHRoaXMucGFnZXMuc29ydEJsb2Nrcyh0aGlzLmJsb2NrKTtcclxuXHJcbiAgICAgICAgLy8gQHRzLWlnbm9yZVxyXG4gICAgICAgIGNvbXBvbmVudFJlZi5pbnN0YW5jZS5ibG9jayA9IHRoaXMuYmxvY2s7XHJcbiAgICAgICAgY29tcG9uZW50UmVmLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbiA9IHRoaXMucGFnZXMuZ2V0QmxvY2sodGhpcy5ibG9jay53aWRnZXRJZCkudGhlbigoYmxvY2s6IEJsb2NrKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYmxvY2tXcmFwcGVyID0gYmxvY2s7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZENvbXBvbmVudCgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=