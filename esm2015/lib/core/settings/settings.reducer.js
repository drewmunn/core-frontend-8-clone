/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/settings/settings.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { createReducer, on } from '@ngrx/store';
import * as SettingsActions from './settings.actions';
/**
 * @record
 */
export function SettingsState() { }
if (false) {
    /** @type {?} */
    SettingsState.prototype.fixedHeader;
    /** @type {?} */
    SettingsState.prototype.fixedNavigation;
    /** @type {?} */
    SettingsState.prototype.minifyNavigation;
    /** @type {?} */
    SettingsState.prototype.hideNavigation;
    /** @type {?} */
    SettingsState.prototype.topNavigation;
    /** @type {?} */
    SettingsState.prototype.boxedLayout;
    /** @type {?} */
    SettingsState.prototype.pushContent;
    /** @type {?} */
    SettingsState.prototype.noOverlay;
    /** @type {?} */
    SettingsState.prototype.offCanvas;
    /** @type {?} */
    SettingsState.prototype.biggerContentFont;
    /** @type {?} */
    SettingsState.prototype.highContrastText;
    /** @type {?} */
    SettingsState.prototype.daltonism;
    /** @type {?} */
    SettingsState.prototype.preloaderInside;
    /** @type {?} */
    SettingsState.prototype.rtl;
    /** @type {?} */
    SettingsState.prototype.cleanPageBackground;
    /** @type {?} */
    SettingsState.prototype.hideNavigationIcons;
    /** @type {?} */
    SettingsState.prototype.disableCSSAnimation;
    /** @type {?} */
    SettingsState.prototype.hideInfoCard;
    /** @type {?} */
    SettingsState.prototype.leanSubheader;
    /** @type {?} */
    SettingsState.prototype.hierarchicalNavigation;
    /** @type {?} */
    SettingsState.prototype.globalFontSize;
}
// here you can configure initial state of your app
// for all your users
/** @type {?} */
export const initialState = {
    // app layout
    fixedHeader: true,
    fixedNavigation: false,
    minifyNavigation: false,
    hideNavigation: false,
    topNavigation: false,
    boxedLayout: false,
    // mobile menu
    pushContent: false,
    noOverlay: false,
    offCanvas: false,
    // accessibility
    biggerContentFont: false,
    highContrastText: false,
    daltonism: false,
    preloaderInside: false,
    rtl: false,
    // global modifications
    cleanPageBackground: false,
    hideNavigationIcons: false,
    disableCSSAnimation: false,
    hideInfoCard: false,
    leanSubheader: false,
    hierarchicalNavigation: false,
    // global font size
    globalFontSize: 'md',
};
const ɵ0 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { fixedHeader: !state.fixedHeader })), ɵ1 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { fixedNavigation: !state.fixedNavigation })), ɵ2 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { minifyNavigation: !state.minifyNavigation })), ɵ3 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { hideNavigation: !state.hideNavigation })), ɵ4 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { topNavigation: !state.topNavigation })), ɵ5 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { boxedLayout: !state.boxedLayout })), ɵ6 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { pushContent: !state.pushContent })), ɵ7 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { noOverlay: !state.noOverlay })), ɵ8 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { offCanvas: !state.offCanvas })), ɵ9 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { biggerContentFont: !state.biggerContentFont })), ɵ10 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { highContrastText: !state.highContrastText })), ɵ11 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { daltonism: !state.daltonism })), ɵ12 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { rtl: !state.rtl })), ɵ13 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { preloaderInside: !state.preloaderInside })), ɵ14 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { cleanPageBackground: !state.cleanPageBackground })), ɵ15 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { hideNavigationIcons: !state.hideNavigationIcons })), ɵ16 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { disableCSSAnimation: !state.disableCSSAnimation })), ɵ17 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { hideInfoCard: !state.hideInfoCard })), ɵ18 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { leanSubheader: !state.leanSubheader })), ɵ19 = /**
 * @param {?} state
 * @return {?}
 */
state => (Object.assign({}, state, { hierarchicalNavigation: !state.hierarchicalNavigation })), ɵ20 = /**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
(state, action) => (Object.assign({}, state, { globalFontSize: action.size })), ɵ21 = /**
 * @return {?}
 */
() => (Object.assign({}, initialState));
/** @type {?} */
const settingsReducer = createReducer(initialState, on(SettingsActions.toggleFixedHeader, (ɵ0)), on(SettingsActions.toggleFixedNavigation, (ɵ1)), on(SettingsActions.toggleMinifyNavigation, (ɵ2)), on(SettingsActions.toggleHideNavigation, (ɵ3)), on(SettingsActions.toggleTopNavigation, (ɵ4)), on(SettingsActions.toggleBoxedLayout, (ɵ5)), on(SettingsActions.togglePushContent, (ɵ6)), on(SettingsActions.toggleNoOverlay, (ɵ7)), on(SettingsActions.toggleOffCanvas, (ɵ8)), on(SettingsActions.toggleBiggerContentFont, (ɵ9)), on(SettingsActions.toggleHighContrastText, (ɵ10)), on(SettingsActions.toggleDaltonism, (ɵ11)), on(SettingsActions.toggleRtl, (ɵ12)), on(SettingsActions.togglePreloaderInsise, (ɵ13)), on(SettingsActions.toggleCleanPageBackground, (ɵ14)), on(SettingsActions.toggleHideNavigationIcons, (ɵ15)), on(SettingsActions.toggleDisableCSSAnimation, (ɵ16)), on(SettingsActions.toggleHideInfoCard, (ɵ17)), on(SettingsActions.toggleLeanSubheader, (ɵ18)), on(SettingsActions.toggleHierarchicalNavigation, (ɵ19)), on(SettingsActions.setGlobalFontSize, (ɵ20)), on(SettingsActions.appReset, (ɵ21)));
/**
 * @param {?} state
 * @param {?} action
 * @return {?}
 */
export function reducer(state, action) {
    return settingsReducer(state, action);
}
export { ɵ0, ɵ1, ɵ2, ɵ3, ɵ4, ɵ5, ɵ6, ɵ7, ɵ8, ɵ9, ɵ10, ɵ11, ɵ12, ɵ13, ɵ14, ɵ15, ɵ16, ɵ17, ɵ18, ɵ19, ɵ20, ɵ21 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MucmVkdWNlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvc2V0dGluZ3Mvc2V0dGluZ3MucmVkdWNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBUyxhQUFhLEVBQUUsRUFBRSxFQUFDLE1BQU0sYUFBYSxDQUFDO0FBQ3RELE9BQU8sS0FBSyxlQUFlLE1BQU0sb0JBQW9CLENBQUM7Ozs7QUFFdEQsbUNBK0JDOzs7SUE3Qkcsb0NBQXFCOztJQUNyQix3Q0FBeUI7O0lBQ3pCLHlDQUEwQjs7SUFDMUIsdUNBQXdCOztJQUN4QixzQ0FBdUI7O0lBQ3ZCLG9DQUFxQjs7SUFHckIsb0NBQXFCOztJQUNyQixrQ0FBbUI7O0lBQ25CLGtDQUFtQjs7SUFHbkIsMENBQTJCOztJQUMzQix5Q0FBMEI7O0lBQzFCLGtDQUFtQjs7SUFDbkIsd0NBQXlCOztJQUN6Qiw0QkFBYTs7SUFHYiw0Q0FBNkI7O0lBQzdCLDRDQUE2Qjs7SUFDN0IsNENBQTZCOztJQUM3QixxQ0FBc0I7O0lBQ3RCLHNDQUF1Qjs7SUFDdkIsK0NBQWdDOztJQUdoQyx1Q0FBdUI7Ozs7O0FBSzNCLE1BQU0sT0FBTyxZQUFZLEdBQWtCOztJQUV2QyxXQUFXLEVBQUUsSUFBSTtJQUNqQixlQUFlLEVBQUUsS0FBSztJQUN0QixnQkFBZ0IsRUFBRSxLQUFLO0lBQ3ZCLGNBQWMsRUFBRSxLQUFLO0lBQ3JCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLFdBQVcsRUFBRSxLQUFLOztJQUdsQixXQUFXLEVBQUUsS0FBSztJQUNsQixTQUFTLEVBQUUsS0FBSztJQUNoQixTQUFTLEVBQUUsS0FBSzs7SUFHaEIsaUJBQWlCLEVBQUUsS0FBSztJQUN4QixnQkFBZ0IsRUFBRSxLQUFLO0lBQ3ZCLFNBQVMsRUFBRSxLQUFLO0lBQ2hCLGVBQWUsRUFBRSxLQUFLO0lBQ3RCLEdBQUcsRUFBRSxLQUFLOztJQUdWLG1CQUFtQixFQUFFLEtBQUs7SUFDMUIsbUJBQW1CLEVBQUUsS0FBSztJQUMxQixtQkFBbUIsRUFBRSxLQUFLO0lBQzFCLFlBQVksRUFBRSxLQUFLO0lBQ25CLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLHNCQUFzQixFQUFFLEtBQUs7O0lBRzdCLGNBQWMsRUFBRSxJQUFJO0NBRXZCOzs7OztBQUt5QyxLQUFLLENBQUMsRUFBRSxDQUFDLG1CQUFLLEtBQUssSUFBRSxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxJQUFFOzs7O0FBQ2xELEtBQUssQ0FBQyxFQUFFLENBQUMsbUJBQUssS0FBSyxJQUFFLGVBQWUsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLElBQUU7Ozs7QUFDN0QsS0FBSyxDQUFDLEVBQUUsQ0FBQyxtQkFBSyxLQUFLLElBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLElBQUU7Ozs7QUFDbEUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxtQkFBSyxLQUFLLElBQUUsY0FBYyxFQUFFLENBQUMsS0FBSyxDQUFDLGNBQWMsSUFBRTs7OztBQUM3RCxLQUFLLENBQUMsRUFBRSxDQUFDLG1CQUFLLEtBQUssSUFBRSxhQUFhLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBYSxJQUFFOzs7O0FBQzVELEtBQUssQ0FBQyxFQUFFLENBQUMsbUJBQUssS0FBSyxJQUFFLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLElBQUU7Ozs7QUFDdEQsS0FBSyxDQUFDLEVBQUUsQ0FBQyxtQkFBSyxLQUFLLElBQUUsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsSUFBRTs7OztBQUN4RCxLQUFLLENBQUMsRUFBRSxDQUFDLG1CQUFLLEtBQUssSUFBRSxTQUFTLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxJQUFFOzs7O0FBQ2xELEtBQUssQ0FBQyxFQUFFLENBQUMsbUJBQUssS0FBSyxJQUFFLFNBQVMsRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUU7Ozs7QUFDMUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxtQkFBSyxLQUFLLElBQUUsaUJBQWlCLEVBQUUsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLElBQUU7Ozs7QUFDbkUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxtQkFBSyxLQUFLLElBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLElBQUU7Ozs7QUFDdkUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxtQkFBSyxLQUFLLElBQUUsU0FBUyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBRTs7OztBQUN4RCxLQUFLLENBQUMsRUFBRSxDQUFDLG1CQUFLLEtBQUssSUFBRSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFFOzs7O0FBQzFCLEtBQUssQ0FBQyxFQUFFLENBQUMsbUJBQUssS0FBSyxJQUFFLGVBQWUsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLElBQUU7Ozs7QUFDMUQsS0FBSyxDQUFDLEVBQUUsQ0FBQyxtQkFDaEQsS0FBSyxJQUNSLG1CQUFtQixFQUFFLENBQUMsS0FBSyxDQUFDLG1CQUFtQixJQUNqRDs7OztBQUM0QyxLQUFLLENBQUMsRUFBRSxDQUFDLG1CQUNoRCxLQUFLLElBQ1IsbUJBQW1CLEVBQUUsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLElBQ2pEOzs7O0FBQzRDLEtBQUssQ0FBQyxFQUFFLENBQUMsbUJBQ2hELEtBQUssSUFDUixtQkFBbUIsRUFBRSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsSUFDakQ7Ozs7QUFDcUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxtQkFBSyxLQUFLLElBQUUsWUFBWSxFQUFFLENBQUMsS0FBSyxDQUFDLFlBQVksSUFBRTs7OztBQUN2RCxLQUFLLENBQUMsRUFBRSxDQUFDLG1CQUFLLEtBQUssSUFBRSxhQUFhLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBYSxJQUFFOzs7O0FBQ2pELEtBQUssQ0FBQyxFQUFFLENBQUMsbUJBQ25ELEtBQUssSUFDUixzQkFBc0IsRUFBRSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsSUFDdkQ7Ozs7O0FBQ29DLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxFQUFFLENBQUMsbUJBQUssS0FBSyxJQUFFLGNBQWMsRUFBRSxNQUFNLENBQUMsSUFBSSxJQUFFOzs7QUFFckUsR0FBRyxFQUFFLENBQUMsbUJBQUssWUFBWSxFQUFFOztNQXJDcEQsZUFBZSxHQUFHLGFBQWEsQ0FDakMsWUFBWSxFQUVaLEVBQUUsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLE9BQXlELEVBQzdGLEVBQUUsQ0FBQyxlQUFlLENBQUMscUJBQXFCLE9BQWlFLEVBQ3pHLEVBQUUsQ0FBQyxlQUFlLENBQUMsc0JBQXNCLE9BQW1FLEVBQzVHLEVBQUUsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLE9BQStELEVBQ3RHLEVBQUUsQ0FBQyxlQUFlLENBQUMsbUJBQW1CLE9BQTZELEVBQ25HLEVBQUUsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLE9BQXlELEVBQzdGLEVBQUUsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLE9BQXlELEVBQzdGLEVBQUUsQ0FBQyxlQUFlLENBQUMsZUFBZSxPQUFxRCxFQUN2RixFQUFFLENBQUMsZUFBZSxDQUFDLGVBQWUsT0FBcUQsRUFDdkYsRUFBRSxDQUFDLGVBQWUsQ0FBQyx1QkFBdUIsT0FBcUUsRUFDL0csRUFBRSxDQUFDLGVBQWUsQ0FBQyxzQkFBc0IsUUFBbUUsRUFDNUcsRUFBRSxDQUFDLGVBQWUsQ0FBQyxlQUFlLFFBQXFELEVBQ3ZGLEVBQUUsQ0FBQyxlQUFlLENBQUMsU0FBUyxRQUF5QyxFQUNyRSxFQUFFLENBQUMsZUFBZSxDQUFDLHFCQUFxQixRQUFpRSxFQUN6RyxFQUFFLENBQUMsZUFBZSxDQUFDLHlCQUF5QixRQUd6QyxFQUNILEVBQUUsQ0FBQyxlQUFlLENBQUMseUJBQXlCLFFBR3pDLEVBQ0gsRUFBRSxDQUFDLGVBQWUsQ0FBQyx5QkFBeUIsUUFHekMsRUFDSCxFQUFFLENBQUMsZUFBZSxDQUFDLGtCQUFrQixRQUEyRCxFQUNoRyxFQUFFLENBQUMsZUFBZSxDQUFDLG1CQUFtQixRQUE2RCxFQUNuRyxFQUFFLENBQUMsZUFBZSxDQUFDLDRCQUE0QixRQUc1QyxFQUNILEVBQUUsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLFFBQStELEVBRW5HLEVBQUUsQ0FBQyxlQUFlLENBQUMsUUFBUSxRQUE0QixDQUMxRDs7Ozs7O0FBRUQsTUFBTSxVQUFVLE9BQU8sQ0FBQyxLQUFvQixFQUFFLE1BQWM7SUFDeEQsT0FBTyxlQUFlLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQzFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0FjdGlvbiwgY3JlYXRlUmVkdWNlciwgb259IGZyb20gJ0BuZ3J4L3N0b3JlJztcclxuaW1wb3J0ICogYXMgU2V0dGluZ3NBY3Rpb25zIGZyb20gJy4vc2V0dGluZ3MuYWN0aW9ucyc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFNldHRpbmdzU3RhdGUge1xyXG4gICAgLy8gYXBwIGxheW91dFxyXG4gICAgZml4ZWRIZWFkZXI6IGJvb2xlYW47XHJcbiAgICBmaXhlZE5hdmlnYXRpb246IGJvb2xlYW47XHJcbiAgICBtaW5pZnlOYXZpZ2F0aW9uOiBib29sZWFuO1xyXG4gICAgaGlkZU5hdmlnYXRpb246IGJvb2xlYW47XHJcbiAgICB0b3BOYXZpZ2F0aW9uOiBib29sZWFuO1xyXG4gICAgYm94ZWRMYXlvdXQ6IGJvb2xlYW47XHJcblxyXG4gICAgLy8gbW9iaWxlIG1lbnVcclxuICAgIHB1c2hDb250ZW50OiBib29sZWFuO1xyXG4gICAgbm9PdmVybGF5OiBib29sZWFuO1xyXG4gICAgb2ZmQ2FudmFzOiBib29sZWFuO1xyXG5cclxuICAgIC8vIGFjY2Vzc2liaWxpdHlcclxuICAgIGJpZ2dlckNvbnRlbnRGb250OiBib29sZWFuO1xyXG4gICAgaGlnaENvbnRyYXN0VGV4dDogYm9vbGVhbjtcclxuICAgIGRhbHRvbmlzbTogYm9vbGVhbjtcclxuICAgIHByZWxvYWRlckluc2lkZTogYm9vbGVhbjtcclxuICAgIHJ0bDogYm9vbGVhbjtcclxuXHJcbiAgICAvLyBnbG9iYWwgbW9kaWZpY2F0aW9uc1xyXG4gICAgY2xlYW5QYWdlQmFja2dyb3VuZDogYm9vbGVhbjtcclxuICAgIGhpZGVOYXZpZ2F0aW9uSWNvbnM6IGJvb2xlYW47XHJcbiAgICBkaXNhYmxlQ1NTQW5pbWF0aW9uOiBib29sZWFuO1xyXG4gICAgaGlkZUluZm9DYXJkOiBib29sZWFuO1xyXG4gICAgbGVhblN1YmhlYWRlcjogYm9vbGVhbjtcclxuICAgIGhpZXJhcmNoaWNhbE5hdmlnYXRpb246IGJvb2xlYW47XHJcblxyXG4gICAgLy8gZ2xvYmFsIGZvbnQgc2l6ZVxyXG4gICAgZ2xvYmFsRm9udFNpemU6IHN0cmluZztcclxufVxyXG5cclxuLy8gaGVyZSB5b3UgY2FuIGNvbmZpZ3VyZSBpbml0aWFsIHN0YXRlIG9mIHlvdXIgYXBwXHJcbi8vIGZvciBhbGwgeW91ciB1c2Vyc1xyXG5leHBvcnQgY29uc3QgaW5pdGlhbFN0YXRlOiBTZXR0aW5nc1N0YXRlID0ge1xyXG4gICAgLy8gYXBwIGxheW91dFxyXG4gICAgZml4ZWRIZWFkZXI6IHRydWUsXHJcbiAgICBmaXhlZE5hdmlnYXRpb246IGZhbHNlLFxyXG4gICAgbWluaWZ5TmF2aWdhdGlvbjogZmFsc2UsXHJcbiAgICBoaWRlTmF2aWdhdGlvbjogZmFsc2UsXHJcbiAgICB0b3BOYXZpZ2F0aW9uOiBmYWxzZSxcclxuICAgIGJveGVkTGF5b3V0OiBmYWxzZSxcclxuXHJcbiAgICAvLyBtb2JpbGUgbWVudVxyXG4gICAgcHVzaENvbnRlbnQ6IGZhbHNlLFxyXG4gICAgbm9PdmVybGF5OiBmYWxzZSxcclxuICAgIG9mZkNhbnZhczogZmFsc2UsXHJcblxyXG4gICAgLy8gYWNjZXNzaWJpbGl0eVxyXG4gICAgYmlnZ2VyQ29udGVudEZvbnQ6IGZhbHNlLFxyXG4gICAgaGlnaENvbnRyYXN0VGV4dDogZmFsc2UsXHJcbiAgICBkYWx0b25pc206IGZhbHNlLFxyXG4gICAgcHJlbG9hZGVySW5zaWRlOiBmYWxzZSxcclxuICAgIHJ0bDogZmFsc2UsXHJcblxyXG4gICAgLy8gZ2xvYmFsIG1vZGlmaWNhdGlvbnNcclxuICAgIGNsZWFuUGFnZUJhY2tncm91bmQ6IGZhbHNlLFxyXG4gICAgaGlkZU5hdmlnYXRpb25JY29uczogZmFsc2UsXHJcbiAgICBkaXNhYmxlQ1NTQW5pbWF0aW9uOiBmYWxzZSxcclxuICAgIGhpZGVJbmZvQ2FyZDogZmFsc2UsXHJcbiAgICBsZWFuU3ViaGVhZGVyOiBmYWxzZSxcclxuICAgIGhpZXJhcmNoaWNhbE5hdmlnYXRpb246IGZhbHNlLFxyXG5cclxuICAgIC8vIGdsb2JhbCBmb250IHNpemVcclxuICAgIGdsb2JhbEZvbnRTaXplOiAnbWQnLFxyXG5cclxufTtcclxuXHJcbmNvbnN0IHNldHRpbmdzUmVkdWNlciA9IGNyZWF0ZVJlZHVjZXIoXHJcbiAgICBpbml0aWFsU3RhdGUsXHJcblxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZUZpeGVkSGVhZGVyLCBzdGF0ZSA9PiAoey4uLnN0YXRlLCBmaXhlZEhlYWRlcjogIXN0YXRlLmZpeGVkSGVhZGVyfSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZUZpeGVkTmF2aWdhdGlvbiwgc3RhdGUgPT4gKHsuLi5zdGF0ZSwgZml4ZWROYXZpZ2F0aW9uOiAhc3RhdGUuZml4ZWROYXZpZ2F0aW9ufSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZU1pbmlmeU5hdmlnYXRpb24sIHN0YXRlID0+ICh7Li4uc3RhdGUsIG1pbmlmeU5hdmlnYXRpb246ICFzdGF0ZS5taW5pZnlOYXZpZ2F0aW9ufSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZUhpZGVOYXZpZ2F0aW9uLCBzdGF0ZSA9PiAoey4uLnN0YXRlLCBoaWRlTmF2aWdhdGlvbjogIXN0YXRlLmhpZGVOYXZpZ2F0aW9ufSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZVRvcE5hdmlnYXRpb24sIHN0YXRlID0+ICh7Li4uc3RhdGUsIHRvcE5hdmlnYXRpb246ICFzdGF0ZS50b3BOYXZpZ2F0aW9ufSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZUJveGVkTGF5b3V0LCBzdGF0ZSA9PiAoey4uLnN0YXRlLCBib3hlZExheW91dDogIXN0YXRlLmJveGVkTGF5b3V0fSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZVB1c2hDb250ZW50LCBzdGF0ZSA9PiAoey4uLnN0YXRlLCBwdXNoQ29udGVudDogIXN0YXRlLnB1c2hDb250ZW50fSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZU5vT3ZlcmxheSwgc3RhdGUgPT4gKHsuLi5zdGF0ZSwgbm9PdmVybGF5OiAhc3RhdGUubm9PdmVybGF5fSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZU9mZkNhbnZhcywgc3RhdGUgPT4gKHsuLi5zdGF0ZSwgb2ZmQ2FudmFzOiAhc3RhdGUub2ZmQ2FudmFzfSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZUJpZ2dlckNvbnRlbnRGb250LCBzdGF0ZSA9PiAoey4uLnN0YXRlLCBiaWdnZXJDb250ZW50Rm9udDogIXN0YXRlLmJpZ2dlckNvbnRlbnRGb250fSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZUhpZ2hDb250cmFzdFRleHQsIHN0YXRlID0+ICh7Li4uc3RhdGUsIGhpZ2hDb250cmFzdFRleHQ6ICFzdGF0ZS5oaWdoQ29udHJhc3RUZXh0fSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZURhbHRvbmlzbSwgc3RhdGUgPT4gKHsuLi5zdGF0ZSwgZGFsdG9uaXNtOiAhc3RhdGUuZGFsdG9uaXNtfSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZVJ0bCwgc3RhdGUgPT4gKHsuLi5zdGF0ZSwgcnRsOiAhc3RhdGUucnRsfSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZVByZWxvYWRlckluc2lzZSwgc3RhdGUgPT4gKHsuLi5zdGF0ZSwgcHJlbG9hZGVySW5zaWRlOiAhc3RhdGUucHJlbG9hZGVySW5zaWRlfSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZUNsZWFuUGFnZUJhY2tncm91bmQsIHN0YXRlID0+ICh7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgY2xlYW5QYWdlQmFja2dyb3VuZDogIXN0YXRlLmNsZWFuUGFnZUJhY2tncm91bmRcclxuICAgIH0pKSxcclxuICAgIG9uKFNldHRpbmdzQWN0aW9ucy50b2dnbGVIaWRlTmF2aWdhdGlvbkljb25zLCBzdGF0ZSA9PiAoe1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIGhpZGVOYXZpZ2F0aW9uSWNvbnM6ICFzdGF0ZS5oaWRlTmF2aWdhdGlvbkljb25zXHJcbiAgICB9KSksXHJcbiAgICBvbihTZXR0aW5nc0FjdGlvbnMudG9nZ2xlRGlzYWJsZUNTU0FuaW1hdGlvbiwgc3RhdGUgPT4gKHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICBkaXNhYmxlQ1NTQW5pbWF0aW9uOiAhc3RhdGUuZGlzYWJsZUNTU0FuaW1hdGlvblxyXG4gICAgfSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZUhpZGVJbmZvQ2FyZCwgc3RhdGUgPT4gKHsuLi5zdGF0ZSwgaGlkZUluZm9DYXJkOiAhc3RhdGUuaGlkZUluZm9DYXJkfSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZUxlYW5TdWJoZWFkZXIsIHN0YXRlID0+ICh7Li4uc3RhdGUsIGxlYW5TdWJoZWFkZXI6ICFzdGF0ZS5sZWFuU3ViaGVhZGVyfSkpLFxyXG4gICAgb24oU2V0dGluZ3NBY3Rpb25zLnRvZ2dsZUhpZXJhcmNoaWNhbE5hdmlnYXRpb24sIHN0YXRlID0+ICh7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgaGllcmFyY2hpY2FsTmF2aWdhdGlvbjogIXN0YXRlLmhpZXJhcmNoaWNhbE5hdmlnYXRpb25cclxuICAgIH0pKSxcclxuICAgIG9uKFNldHRpbmdzQWN0aW9ucy5zZXRHbG9iYWxGb250U2l6ZSwgKHN0YXRlLCBhY3Rpb24pID0+ICh7Li4uc3RhdGUsIGdsb2JhbEZvbnRTaXplOiBhY3Rpb24uc2l6ZX0pKSxcclxuXHJcbiAgICBvbihTZXR0aW5nc0FjdGlvbnMuYXBwUmVzZXQsICgpID0+ICh7Li4uaW5pdGlhbFN0YXRlfSkpLFxyXG4pO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHJlZHVjZXIoc3RhdGU6IFNldHRpbmdzU3RhdGUsIGFjdGlvbjogQWN0aW9uKSB7XHJcbiAgICByZXR1cm4gc2V0dGluZ3NSZWR1Y2VyKHN0YXRlLCBhY3Rpb24pO1xyXG59XHJcbiJdfQ==