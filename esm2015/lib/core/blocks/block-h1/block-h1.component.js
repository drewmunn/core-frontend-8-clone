/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-h1/block-h1.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
export class BlockH1Component extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockH1Component.decorators = [
    { type: Component, args: [{
                selector: 'core-block-h1',
                template: `
        <h1>
            <ng-container *coreBlockCompile="text; context: this"></ng-container>
        </h1>`
            }] }
];
/** @nocollapse */
BlockH1Component.ctorParameters = () => [
    { type: AuthService }
];
BlockH1Component.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockH1Component.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockH1Component.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2staDEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2staDEvYmxvY2staDEuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDdkQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBU3BELE1BQU0sT0FBTyxnQkFBaUIsU0FBUSxzQkFBc0I7Ozs7SUFJeEQsWUFDYyxXQUF3QjtRQUVsQyxLQUFLLENBQ0QsV0FBVyxDQUNkLENBQUM7UUFKUSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUt0QyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztJQUNyQyxDQUFDOzs7WUF0QkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxlQUFlO2dCQUN6QixRQUFRLEVBQUU7OztjQUdBO2FBQ2I7Ozs7WUFSTyxXQUFXOzs7bUJBV2QsS0FBSzs7OztJQUFOLGdDQUFzQjs7Ozs7SUFHbEIsdUNBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vLi4vYXV0aC9hdXRoLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2staDEnLFxuICAgIHRlbXBsYXRlOiBgXG4gICAgICAgIDxoMT5cbiAgICAgICAgICAgIDxuZy1jb250YWluZXIgKmNvcmVCbG9ja0NvbXBpbGU9XCJ0ZXh0OyBjb250ZXh0OiB0aGlzXCI+PC9uZy1jb250YWluZXI+XG4gICAgICAgIDwvaDE+YCxcbn0pXG5leHBvcnQgY2xhc3MgQmxvY2tIMUNvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgQElucHV0KCkgdGV4dDogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UsXG4gICAgKSB7XG4gICAgICAgIHN1cGVyKFxuICAgICAgICAgICAgYXV0aFNlcnZpY2VcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgc3VwZXIubmdPbkluaXQoKTtcbiAgICAgICAgdGhpcy50ZXh0ID0gdGhpcy5ibG9jay5kYXRhLnRleHQ7XG4gICAgfVxuXG59XG4iXX0=