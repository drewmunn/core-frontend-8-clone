/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-table/block-table.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
export class BlockTableComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.header = this.block.data.tableData[0];
        this.data = this.block.data.tableData;
        this.data.shift();
    }
    /**
     * @param {?} idx
     * @return {?}
     */
    trackByFn(idx) {
        return idx;
    }
}
BlockTableComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-table',
                template: "<div class=\"table-wrapper\">\n    <table class=\"table\">\n        <thead>\n        <tr>\n            <th *ngFor=\"let cell of header trackBy: trackByFn\" [innerHTML]=\"cell.content\"></th>\n        </tr>\n        </thead>\n        <tbody>\n        <tr *ngFor=\"let row of data trackBy: trackByFn\">\n            <td *ngFor=\"let cell of row trackBy: trackByFn\" [innerHTML]=\"cell.content\"></td>\n        </tr>\n        </tbody>\n    </table>\n</div>"
            }] }
];
/** @nocollapse */
BlockTableComponent.ctorParameters = () => [
    { type: AuthService }
];
if (false) {
    /** @type {?} */
    BlockTableComponent.prototype.header;
    /** @type {?} */
    BlockTableComponent.prototype.data;
    /**
     * @type {?}
     * @protected
     */
    BlockTableComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stdGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stdGFibGUvYmxvY2stdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUNoRCxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUNuRSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0seUJBQXlCLENBQUM7QUFNcEQsTUFBTSxPQUFPLG1CQUFvQixTQUFRLHNCQUFzQjs7OztJQUkzRCxZQUNjLFdBQXdCO1FBRWxDLEtBQUssQ0FDRCxXQUFXLENBQ2QsQ0FBQztRQUpRLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBS3RDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELFNBQVMsQ0FBQyxHQUFXO1FBQ2pCLE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQzs7O1lBeEJKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixpZEFBMkM7YUFDOUM7Ozs7WUFMTyxXQUFXOzs7O0lBT2YscUNBQVc7O0lBQ1gsbUNBQVM7Ozs7O0lBR0wsMENBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jsb2NrQWJzdHJhY3RDb21wb25lbnR9IGZyb20gJy4uL2Jsb2NrLWFic3RyYWN0LmNvbXBvbmVudCc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi8uLi9hdXRoL2F1dGguc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay10YWJsZScsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLXRhYmxlLmNvbXBvbmVudC5odG1sJyxcbn0pXG5leHBvcnQgY2xhc3MgQmxvY2tUYWJsZUNvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIGhlYWRlcjogW107XG4gICAgZGF0YTogW107XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBhdXRoU2VydmljZVxuICAgICAgICApO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLmhlYWRlciA9IHRoaXMuYmxvY2suZGF0YS50YWJsZURhdGFbMF07XG4gICAgICAgIHRoaXMuZGF0YSA9IHRoaXMuYmxvY2suZGF0YS50YWJsZURhdGE7XG4gICAgICAgIHRoaXMuZGF0YS5zaGlmdCgpO1xuICAgIH1cblxuICAgIHRyYWNrQnlGbihpZHg6IG51bWJlcikge1xuICAgICAgICByZXR1cm4gaWR4O1xuICAgIH1cblxufVxuIl19