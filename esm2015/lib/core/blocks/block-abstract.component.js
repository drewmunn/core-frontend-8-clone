/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-abstract.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Compiler, Component, ComponentFactoryResolver, Input, NgModule, ViewContainerRef } from '@angular/core';
import { PageService } from '../pages/page.service';
import { TranslateService } from '../pages/translate.service';
import { Block } from '../block/block-block';
import { AuthService } from '../auth/auth.service';
import { TranslateModule } from '@ngx-translate/core';
export class BlockAbstractComponent {
    /**
     * @param {?=} authService
     * @param {?=} pages
     * @param {?=} resolver
     * @param {?=} viewContainerRef
     * @param {?=} translate
     * @param {?=} compiler
     */
    constructor(authService, pages, resolver, viewContainerRef, translate, compiler) {
        this.authService = authService;
        this.pages = pages;
        this.resolver = resolver;
        this.viewContainerRef = viewContainerRef;
        this.translate = translate;
        this.compiler = compiler;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.authService) {
            this.user = this.authService.getUser();
        }
    }
    /**
     * @protected
     * @param {?} contents
     * @return {?}
     */
    createDynamicBlock(contents) {
        this.dynamicComponent = this.createNewComponent(contents);
        this.dynamicModule = this.compiler.compileModuleSync(this.createComponentModule(this.dynamicComponent));
    }
    /**
     * @protected
     * @param {?} componentType
     * @return {?}
     */
    createComponentModule(componentType) {
        class RuntimeComponentModule {
        }
        RuntimeComponentModule.decorators = [
            { type: NgModule, args: [{
                        imports: [TranslateModule],
                        declarations: [componentType],
                        entryComponents: [componentType]
                    },] },
        ];
        return RuntimeComponentModule;
    }
    /**
     * @protected
     * @param {?} contents
     * @return {?}
     */
    createNewComponent(contents) {
        class DynamicComponent {
            /**
             * @param {?} authService
             */
            constructor(authService) {
                this.authService = authService;
            }
            /**
             * @return {?}
             */
            ngOnInit() {
                this.contents = contents;
                this.user = this.authService.getUser();
            }
        }
        DynamicComponent.decorators = [
            { type: Component, args: [{
                        selector: 'core-block-dynamic',
                        template: `${contents}`,
                    },] },
        ];
        /** @nocollapse */
        DynamicComponent.ctorParameters = () => [
            { type: AuthService }
        ];
        if (false) {
            /** @type {?} */
            DynamicComponent.prototype.contents;
            /** @type {?} */
            DynamicComponent.prototype.user;
            /**
             * @type {?}
             * @protected
             */
            DynamicComponent.prototype.authService;
        }
        return DynamicComponent;
    }
    /**
     * @param {?} idx
     * @param {?=} item
     * @return {?}
     */
    trackByFn(idx, item) {
        return idx;
    }
}
BlockAbstractComponent.decorators = [
    { type: Component, args: [{
                template: "<core-block class=\"{{block.data.class}} {{block.data.visibility}}\"\n            *ngFor=\"let subBlock of block.content trackBy: trackByFn\"\n            [block]=\"subBlock\"\n            [ngClass]=\"{ 'stickyDiv': block.data.specialType == 'sticky',\n            'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n             'col-sm-12': block.data.columnsSmall == 'full',\n             'col-md-12': block.data.columnsNormal == 'full'}\"\n            [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n>\n</core-block>",
                changeDetection: ChangeDetectionStrategy.Default
            }] }
];
/** @nocollapse */
BlockAbstractComponent.ctorParameters = () => [
    { type: AuthService },
    { type: PageService },
    { type: ComponentFactoryResolver },
    { type: ViewContainerRef },
    { type: TranslateService },
    { type: Compiler }
];
BlockAbstractComponent.propDecorators = {
    block: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockAbstractComponent.prototype.block;
    /** @type {?} */
    BlockAbstractComponent.prototype.user;
    /** @type {?} */
    BlockAbstractComponent.prototype.dynamicComponent;
    /** @type {?} */
    BlockAbstractComponent.prototype.dynamicModule;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.pages;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.resolver;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.translate;
    /**
     * @type {?}
     * @protected
     */
    BlockAbstractComponent.prototype.compiler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYWJzdHJhY3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stYWJzdHJhY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUNILHVCQUF1QixFQUN2QixRQUFRLEVBQ1IsU0FBUyxFQUNULHdCQUF3QixFQUN4QixLQUFLLEVBQ0wsUUFBUSxFQUdSLGdCQUFnQixFQUNuQixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDbEQsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFDLEtBQUssRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQzNDLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFPcEQsTUFBTSxPQUFPLHNCQUFzQjs7Ozs7Ozs7O0lBUS9CLFlBQ2MsV0FBeUIsRUFDekIsS0FBbUIsRUFDbkIsUUFBbUMsRUFDbkMsZ0JBQW1DLEVBQ25DLFNBQTRCLEVBQzVCLFFBQW1CO1FBTG5CLGdCQUFXLEdBQVgsV0FBVyxDQUFjO1FBQ3pCLFVBQUssR0FBTCxLQUFLLENBQWM7UUFDbkIsYUFBUSxHQUFSLFFBQVEsQ0FBMkI7UUFDbkMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFtQjtRQUNuQyxjQUFTLEdBQVQsU0FBUyxDQUFtQjtRQUM1QixhQUFRLEdBQVIsUUFBUSxDQUFXO0lBRWpDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUMxQztJQUNMLENBQUM7Ozs7OztJQUVTLGtCQUFrQixDQUFDLFFBQWE7UUFDdEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7SUFDNUcsQ0FBQzs7Ozs7O0lBRVMscUJBQXFCLENBQUMsYUFBa0I7UUFDOUMsTUFLTSxzQkFBc0I7OztvQkFMM0IsUUFBUSxTQUFDO3dCQUNOLE9BQU8sRUFBRSxDQUFDLGVBQWUsQ0FBQzt3QkFDMUIsWUFBWSxFQUFFLENBQUMsYUFBYSxDQUFDO3dCQUM3QixlQUFlLEVBQUUsQ0FBQyxhQUFhLENBQUM7cUJBQ25DOztRQUlELE9BQU8sc0JBQXNCLENBQUM7SUFDbEMsQ0FBQzs7Ozs7O0lBRVMsa0JBQWtCLENBQUMsUUFBZ0I7UUFDekMsTUFJTSxnQkFBZ0I7Ozs7WUFJbEIsWUFDYyxXQUF3QjtnQkFBeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7WUFFdEMsQ0FBQzs7OztZQUVELFFBQVE7Z0JBQ0osSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUMzQyxDQUFDOzs7b0JBaEJKLFNBQVMsU0FBQzt3QkFDUCxRQUFRLEVBQUUsb0JBQW9CO3dCQUM5QixRQUFRLEVBQUUsR0FBRyxRQUFRLEVBQUU7cUJBQzFCOzs7O29CQXJERCxXQUFXOzs7O1lBdURQLG9DQUFjOztZQUNkLGdDQUFVOzs7OztZQUdOLHVDQUFrQzs7UUFVMUMsT0FBTyxnQkFBZ0IsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBVyxFQUFFLElBQVU7UUFDN0IsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDOzs7WUF2RUosU0FBUyxTQUFDO2dCQUNQLGtsQkFBMkQ7Z0JBQzNELGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxPQUFPO2FBQ25EOzs7O1lBTk8sV0FBVztZQUhYLFdBQVc7WUFQZix3QkFBd0I7WUFLeEIsZ0JBQWdCO1lBR1osZ0JBQWdCO1lBVnBCLFFBQVE7OztvQkFzQlAsS0FBSzs7OztJQUFOLHVDQUFzQjs7SUFDdEIsc0NBQW9COztJQUVwQixrREFBc0I7O0lBQ3RCLCtDQUFvQzs7Ozs7SUFHaEMsNkNBQW1DOzs7OztJQUNuQyx1Q0FBNkI7Ozs7O0lBQzdCLDBDQUE2Qzs7Ozs7SUFDN0Msa0RBQTZDOzs7OztJQUM3QywyQ0FBc0M7Ozs7O0lBQ3RDLDBDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gICAgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksXG4gICAgQ29tcGlsZXIsXG4gICAgQ29tcG9uZW50LFxuICAgIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcbiAgICBJbnB1dCxcbiAgICBOZ01vZHVsZSxcbiAgICBOZ01vZHVsZUZhY3RvcnksXG4gICAgT25Jbml0LFxuICAgIFZpZXdDb250YWluZXJSZWZcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1BhZ2VTZXJ2aWNlfSBmcm9tICcuLi9wYWdlcy9wYWdlLnNlcnZpY2UnO1xuaW1wb3J0IHtUcmFuc2xhdGVTZXJ2aWNlfSBmcm9tICcuLi9wYWdlcy90cmFuc2xhdGUuc2VydmljZSc7XG5pbXBvcnQge0Jsb2NrfSBmcm9tICcuLi9ibG9jay9ibG9jay1ibG9jayc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi9hdXRoL2F1dGguc2VydmljZSc7XG5pbXBvcnQge1RyYW5zbGF0ZU1vZHVsZX0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHRlbXBsYXRlVXJsOiAnLi9ibG9jay13cmFwcGVyL2Jsb2NrLXdyYXBwZXIuY29tcG9uZW50Lmh0bWwnLFxuICAgIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuRGVmYXVsdFxufSlcblxuZXhwb3J0IGNsYXNzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgQElucHV0KCkgYmxvY2s6IEJsb2NrO1xuICAgIHVzZXI6IHsgZGF0YTogYW55IH07XG5cbiAgICBkeW5hbWljQ29tcG9uZW50OiBhbnk7XG4gICAgZHluYW1pY01vZHVsZTogTmdNb2R1bGVGYWN0b3J5PGFueT47XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlPzogQXV0aFNlcnZpY2UsXG4gICAgICAgIHByb3RlY3RlZCBwYWdlcz86IFBhZ2VTZXJ2aWNlLFxuICAgICAgICBwcm90ZWN0ZWQgcmVzb2x2ZXI/OiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXG4gICAgICAgIHByb3RlY3RlZCB2aWV3Q29udGFpbmVyUmVmPzogVmlld0NvbnRhaW5lclJlZixcbiAgICAgICAgcHJvdGVjdGVkIHRyYW5zbGF0ZT86IFRyYW5zbGF0ZVNlcnZpY2UsXG4gICAgICAgIHByb3RlY3RlZCBjb21waWxlcj86IENvbXBpbGVyLFxuICAgICkge1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICBpZiAodGhpcy5hdXRoU2VydmljZSkge1xuICAgICAgICAgICAgdGhpcy51c2VyID0gdGhpcy5hdXRoU2VydmljZS5nZXRVc2VyKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgY3JlYXRlRHluYW1pY0Jsb2NrKGNvbnRlbnRzOiBhbnkpIHtcbiAgICAgICAgdGhpcy5keW5hbWljQ29tcG9uZW50ID0gdGhpcy5jcmVhdGVOZXdDb21wb25lbnQoY29udGVudHMpO1xuICAgICAgICB0aGlzLmR5bmFtaWNNb2R1bGUgPSB0aGlzLmNvbXBpbGVyLmNvbXBpbGVNb2R1bGVTeW5jKHRoaXMuY3JlYXRlQ29tcG9uZW50TW9kdWxlKHRoaXMuZHluYW1pY0NvbXBvbmVudCkpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBjcmVhdGVDb21wb25lbnRNb2R1bGUoY29tcG9uZW50VHlwZTogYW55KTogYW55IHtcbiAgICAgICAgQE5nTW9kdWxlKHtcbiAgICAgICAgICAgIGltcG9ydHM6IFtUcmFuc2xhdGVNb2R1bGVdLFxuICAgICAgICAgICAgZGVjbGFyYXRpb25zOiBbY29tcG9uZW50VHlwZV0sXG4gICAgICAgICAgICBlbnRyeUNvbXBvbmVudHM6IFtjb21wb25lbnRUeXBlXVxuICAgICAgICB9KVxuICAgICAgICBjbGFzcyBSdW50aW1lQ29tcG9uZW50TW9kdWxlIHtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBSdW50aW1lQ29tcG9uZW50TW9kdWxlO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBjcmVhdGVOZXdDb21wb25lbnQoY29udGVudHM6IHN0cmluZyk6IGFueSB7XG4gICAgICAgIEBDb21wb25lbnQoe1xuICAgICAgICAgICAgc2VsZWN0b3I6ICdjb3JlLWJsb2NrLWR5bmFtaWMnLFxuICAgICAgICAgICAgdGVtcGxhdGU6IGAke2NvbnRlbnRzfWAsXG4gICAgICAgIH0pXG4gICAgICAgIGNsYXNzIER5bmFtaWNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgICAgICAgICAgY29udGVudHM6IGFueTtcbiAgICAgICAgICAgIHVzZXI6IGFueTtcblxuICAgICAgICAgICAgY29uc3RydWN0b3IoXG4gICAgICAgICAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnRzID0gY29udGVudHM7XG4gICAgICAgICAgICAgICAgdGhpcy51c2VyID0gdGhpcy5hdXRoU2VydmljZS5nZXRVc2VyKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gRHluYW1pY0NvbXBvbmVudDtcbiAgICB9XG5cbiAgICB0cmFja0J5Rm4oaWR4OiBudW1iZXIsIGl0ZW0/OiBhbnkpIHtcbiAgICAgICAgcmV0dXJuIGlkeDtcbiAgICB9XG5cbn1cbiJdfQ==