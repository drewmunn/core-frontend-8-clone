/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-breadcrumbs/block-breadcrumbs.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { PageService } from '../../pages/page.service';
import { AuthService } from '../../auth/auth.service';
export class BlockBreadcrumbsComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     * @param {?} pages
     */
    constructor(authService, pages) {
        super(authService);
        this.authService = authService;
        this.pages = pages;
        this.date = Date.now();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.pages.getBreadcrumbs(this.pages.getCurrentPage().id).subscribe((/**
         * @param {?} breadcrumbs
         * @return {?}
         */
        breadcrumbs => {
            this.breadcrumbs = breadcrumbs || [];
        }));
    }
}
BlockBreadcrumbsComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-breadcrumbs',
                template: "<ol class=\"breadcrumb page-breadcrumb\">\n    <li class=\"breadcrumb-item\"><a href=\"#\" coreStubClick>Home</a></li>\n    <li class=\"breadcrumb-item\" *ngFor=\"let breadcrumb of breadcrumbs.parents\">\n        <a [routerLink]=\"breadcrumb.url\">{{breadcrumb.name}}</a>\n    </li>\n    <li class=\"breadcrumb-item\">{{breadcrumbs.name}}</li>\n    <li class=\"position-absolute pos-top pos-right d-none d-sm-block\">\n        <span>{{ date | date }}</span>\n    </li>\n</ol>"
            }] }
];
/** @nocollapse */
BlockBreadcrumbsComponent.ctorParameters = () => [
    { type: AuthService },
    { type: PageService }
];
if (false) {
    /** @type {?} */
    BlockBreadcrumbsComponent.prototype.breadcrumbs;
    /** @type {?} */
    BlockBreadcrumbsComponent.prototype.date;
    /**
     * @type {?}
     * @protected
     */
    BlockBreadcrumbsComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    BlockBreadcrumbsComponent.prototype.pages;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYnJlYWRjcnVtYnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stYnJlYWRjcnVtYnMvYmxvY2stYnJlYWRjcnVtYnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUNoRCxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUNuRSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBT3BELE1BQU0sT0FBTyx5QkFBMEIsU0FBUSxzQkFBc0I7Ozs7O0lBSWpFLFlBQ2MsV0FBd0IsRUFDeEIsS0FBa0I7UUFFNUIsS0FBSyxDQUNELFdBQVcsQ0FDZCxDQUFDO1FBTFEsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsVUFBSyxHQUFMLEtBQUssQ0FBYTtRQUpoQyxTQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBU2xCLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsV0FBVyxDQUFDLEVBQUU7WUFDOUUsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLElBQUksRUFBRSxDQUFDO1FBQ3pDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7O1lBdEJKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsd0JBQXdCO2dCQUNsQyx1ZUFBaUQ7YUFDcEQ7Ozs7WUFMTyxXQUFXO1lBRFgsV0FBVzs7OztJQVNmLGdEQUFpQjs7SUFDakIseUNBQWtCOzs7OztJQUdkLGdEQUFrQzs7Ozs7SUFDbEMsMENBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jsb2NrQWJzdHJhY3RDb21wb25lbnR9IGZyb20gJy4uL2Jsb2NrLWFic3RyYWN0LmNvbXBvbmVudCc7XG5pbXBvcnQge1BhZ2VTZXJ2aWNlfSBmcm9tICcuLi8uLi9wYWdlcy9wYWdlLnNlcnZpY2UnO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vLi4vYXV0aC9hdXRoLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2stYnJlYWRjcnVtYnMnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9ibG9jay1icmVhZGNydW1icy5jb21wb25lbnQuaHRtbCcsXG59KVxuXG5leHBvcnQgY2xhc3MgQmxvY2tCcmVhZGNydW1ic0NvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIGJyZWFkY3J1bWJzOiBhbnk7XG4gICAgZGF0ZSA9IERhdGUubm93KCk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICAgICAgcHJvdGVjdGVkIHBhZ2VzOiBQYWdlU2VydmljZSxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBhdXRoU2VydmljZVxuICAgICAgICApO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnBhZ2VzLmdldEJyZWFkY3J1bWJzKHRoaXMucGFnZXMuZ2V0Q3VycmVudFBhZ2UoKS5pZCkuc3Vic2NyaWJlKGJyZWFkY3J1bWJzID0+IHtcbiAgICAgICAgICAgIHRoaXMuYnJlYWRjcnVtYnMgPSBicmVhZGNydW1icyB8fCBbXTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG59XG4iXX0=