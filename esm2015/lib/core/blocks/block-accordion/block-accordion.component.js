/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-accordion/block-accordion.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
export class BlockAccordionComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} idx
     * @return {?}
     */
    trackByFn(idx) {
        return idx;
    }
}
BlockAccordionComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-accordion',
                template: "<mat-accordion>\n    <mat-expansion-panel *ngFor=\"let item of block.content trackBy: trackByFn\">\n        <mat-expansion-panel-header>\n            <mat-panel-title [innerHTML]=\"item.data.title\"></mat-panel-title>\n        </mat-expansion-panel-header>\n        <div class=\"{{block.data.title}} {{block.data.visibility}}\"\n             [ngClass]=\"{container: block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n                 'stickyDiv': block.data.specialType == 'sticky',\n                  'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n                  'col-sm-12': block.data.columnsSmall == 'full',\n                  'col-md-12': block.data.columnsNormal == 'full'}\"\n             [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n        >\n            <core-block\n                    *ngFor=\"let subBlock of item.content\"\n                    [block]=\"subBlock\"\n                    class=\"subWidget {{subBlock.data.class}}\">\n            </core-block>\n        </div>\n    </mat-expansion-panel>\n</mat-accordion>\n"
            }] }
];
/** @nocollapse */
BlockAccordionComponent.ctorParameters = () => [
    { type: AuthService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockAccordionComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYWNjb3JkaW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYmxvY2tzL2Jsb2NrLWFjY29yZGlvbi9ibG9jay1hY2NvcmRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUNoRCxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUNuRSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0seUJBQXlCLENBQUM7QUFNcEQsTUFBTSxPQUFPLHVCQUF3QixTQUFRLHNCQUFzQjs7OztJQUUvRCxZQUNjLFdBQXdCO1FBRWxDLEtBQUssQ0FDRCxXQUFXLENBQ2QsQ0FBQztRQUpRLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBS3RDLENBQUM7Ozs7SUFFRCxRQUFRO0lBQ1IsQ0FBQzs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBVztRQUNqQixPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7OztZQW5CSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHNCQUFzQjtnQkFDaEMsd25DQUErQzthQUNsRDs7OztZQUxPLFdBQVc7Ozs7Ozs7SUFTWCw4Q0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tBYnN0cmFjdENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcbmltcG9ydCB7QXV0aFNlcnZpY2V9IGZyb20gJy4uLy4uL2F1dGgvYXV0aC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdjb3JlLWJsb2NrLWFjY29yZGlvbicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLWFjY29yZGlvbi5jb21wb25lbnQuaHRtbCcsXG59KVxuZXhwb3J0IGNsYXNzIEJsb2NrQWNjb3JkaW9uQ29tcG9uZW50IGV4dGVuZHMgQmxvY2tBYnN0cmFjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBhdXRoU2VydmljZVxuICAgICAgICApO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgIH1cblxuICAgIHRyYWNrQnlGbihpZHg6IG51bWJlcikge1xuICAgICAgICByZXR1cm4gaWR4O1xuICAgIH1cblxufVxuIl19