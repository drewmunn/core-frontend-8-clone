/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-paragraph/block-paragraph.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
export class BlockParagraphComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockParagraphComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-paragraph',
                template: `
        <ng-container *coreBlockCompile="text; context: this"></ng-container>
    `
            }] }
];
/** @nocollapse */
BlockParagraphComponent.ctorParameters = () => [
    { type: AuthService }
];
BlockParagraphComponent.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockParagraphComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockParagraphComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stcGFyYWdyYXBoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYmxvY2tzL2Jsb2NrLXBhcmFncmFwaC9ibG9jay1wYXJhZ3JhcGguY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDdkQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBU3BELE1BQU0sT0FBTyx1QkFBd0IsU0FBUSxzQkFBc0I7Ozs7SUFJL0QsWUFDYyxXQUF3QjtRQUVsQyxLQUFLLENBQ0QsV0FBVyxDQUNkLENBQUM7UUFKUSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUt0QyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztJQUNyQyxDQUFDOzs7WUF0QkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxzQkFBc0I7Z0JBQ2hDLFFBQVEsRUFBRTs7S0FFVDthQUNKOzs7O1lBUE8sV0FBVzs7O21CQVdkLEtBQUs7Ozs7SUFBTix1Q0FBc0I7Ozs7O0lBR2xCLDhDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tBYnN0cmFjdENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcbmltcG9ydCB7QXV0aFNlcnZpY2V9IGZyb20gJy4uLy4uL2F1dGgvYXV0aC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdjb3JlLWJsb2NrLXBhcmFncmFwaCcsXG4gICAgdGVtcGxhdGU6IGBcbiAgICAgICAgPG5nLWNvbnRhaW5lciAqY29yZUJsb2NrQ29tcGlsZT1cInRleHQ7IGNvbnRleHQ6IHRoaXNcIj48L25nLWNvbnRhaW5lcj5cbiAgICBgXG59KVxuXG5leHBvcnQgY2xhc3MgQmxvY2tQYXJhZ3JhcGhDb21wb25lbnQgZXh0ZW5kcyBCbG9ja0Fic3RyYWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIEBJbnB1dCgpIHRleHQ6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgICAgIHRoaXMudGV4dCA9IHRoaXMuYmxvY2suZGF0YS50ZXh0O1xuICAgIH1cblxufVxuIl19