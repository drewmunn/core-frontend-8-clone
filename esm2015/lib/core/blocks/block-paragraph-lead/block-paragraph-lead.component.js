/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-paragraph-lead/block-paragraph-lead.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
export class BlockParagraphLeadComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.text;
    }
}
BlockParagraphLeadComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-paragraph-lead',
                template: `
        <ng-container *coreBlockCompile="text; context: this" class="lead"></ng-container>
    `
            }] }
];
/** @nocollapse */
BlockParagraphLeadComponent.ctorParameters = () => [
    { type: AuthService }
];
BlockParagraphLeadComponent.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockParagraphLeadComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockParagraphLeadComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stcGFyYWdyYXBoLWxlYWQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stcGFyYWdyYXBoLWxlYWQvYmxvY2stcGFyYWdyYXBoLWxlYWQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDdkQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBU3BELE1BQU0sT0FBTywyQkFBNEIsU0FBUSxzQkFBc0I7Ozs7SUFJbkUsWUFDYyxXQUF3QjtRQUVsQyxLQUFLLENBQ0QsV0FBVyxDQUNkLENBQUM7UUFKUSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUt0QyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztJQUNyQyxDQUFDOzs7WUF0QkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLFFBQVEsRUFBRTs7S0FFVDthQUNKOzs7O1lBUE8sV0FBVzs7O21CQVdkLEtBQUs7Ozs7SUFBTiwyQ0FBc0I7Ozs7O0lBR2xCLGtEQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tBYnN0cmFjdENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcbmltcG9ydCB7QXV0aFNlcnZpY2V9IGZyb20gJy4uLy4uL2F1dGgvYXV0aC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdjb3JlLWJsb2NrLXBhcmFncmFwaC1sZWFkJyxcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8bmctY29udGFpbmVyICpjb3JlQmxvY2tDb21waWxlPVwidGV4dDsgY29udGV4dDogdGhpc1wiIGNsYXNzPVwibGVhZFwiPjwvbmctY29udGFpbmVyPlxuICAgIGBcbn0pXG5cbmV4cG9ydCBjbGFzcyBCbG9ja1BhcmFncmFwaExlYWRDb21wb25lbnQgZXh0ZW5kcyBCbG9ja0Fic3RyYWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIEBJbnB1dCgpIHRleHQ6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgICAgIHRoaXMudGV4dCA9IHRoaXMuYmxvY2suZGF0YS50ZXh0O1xuICAgIH1cblxufVxuIl19