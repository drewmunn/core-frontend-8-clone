/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-image/block-image.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
export class BlockImageComponent extends BlockAbstractComponent {
    constructor() {
        super();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.url = this.block.data.image_url;
        this.alt = this.block.data.alt_text;
    }
}
BlockImageComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-image',
                template: `
        <img class="img-responsive image-block" [src]="url" alt="{{alt}}"/>`
            }] }
];
/** @nocollapse */
BlockImageComponent.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    BlockImageComponent.prototype.url;
    /** @type {?} */
    BlockImageComponent.prototype.alt;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2staW1hZ2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2staW1hZ2UvYmxvY2staW1hZ2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUNoRCxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQU9uRSxNQUFNLE9BQU8sbUJBQW9CLFNBQVEsc0JBQXNCO0lBSzNEO1FBQ0ksS0FBSyxFQUFFLENBQUM7SUFDWixDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3hDLENBQUM7OztZQWpCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsUUFBUSxFQUFFOzRFQUM4RDthQUMzRTs7Ozs7O0lBR0csa0NBQVk7O0lBQ1osa0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QmxvY2tBYnN0cmFjdENvbXBvbmVudH0gZnJvbSAnLi4vYmxvY2stYWJzdHJhY3QuY29tcG9uZW50JztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdjb3JlLWJsb2NrLWltYWdlJyxcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8aW1nIGNsYXNzPVwiaW1nLXJlc3BvbnNpdmUgaW1hZ2UtYmxvY2tcIiBbc3JjXT1cInVybFwiIGFsdD1cInt7YWx0fX1cIi8+YFxufSlcbmV4cG9ydCBjbGFzcyBCbG9ja0ltYWdlQ29tcG9uZW50IGV4dGVuZHMgQmxvY2tBYnN0cmFjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICB1cmw6IHN0cmluZztcbiAgICBhbHQ6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlcigpO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnVybCA9IHRoaXMuYmxvY2suZGF0YS5pbWFnZV91cmw7XG4gICAgICAgIHRoaXMuYWx0ID0gdGhpcy5ibG9jay5kYXRhLmFsdF90ZXh0O1xuICAgIH1cblxufVxuIl19