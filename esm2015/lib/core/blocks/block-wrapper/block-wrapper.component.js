/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-wrapper/block-wrapper.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
export class BlockWrapperComponent extends BlockAbstractComponent {
    constructor() {
        super();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} idx
     * @param {?=} item
     * @return {?}
     */
    trackByFn(idx, item) {
        return idx;
    }
}
BlockWrapperComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-wrapper',
                template: "<core-block class=\"{{block.data.class}} {{block.data.visibility}}\"\n            *ngFor=\"let subBlock of block.content trackBy: trackByFn\"\n            [block]=\"subBlock\"\n            [ngClass]=\"{ 'stickyDiv': block.data.specialType == 'sticky',\n            'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n             'col-sm-12': block.data.columnsSmall == 'full',\n             'col-md-12': block.data.columnsNormal == 'full'}\"\n            [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n>\n</core-block>"
            }] }
];
/** @nocollapse */
BlockWrapperComponent.ctorParameters = () => [];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2std3JhcHBlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2Nrcy9ibG9jay13cmFwcGVyL2Jsb2NrLXdyYXBwZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUNoRCxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQU9uRSxNQUFNLE9BQU8scUJBQXNCLFNBQVEsc0JBQXNCO0lBRTdEO1FBQ0ksS0FBSyxFQUFFLENBQUM7SUFDWixDQUFDOzs7O0lBRUQsUUFBUTtJQUNSLENBQUM7Ozs7OztJQUVELFNBQVMsQ0FBQyxHQUFXLEVBQUUsSUFBVTtRQUM3QixPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7OztZQWhCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsa2xCQUE2QzthQUNoRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2std3JhcHBlcicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLXdyYXBwZXIuY29tcG9uZW50Lmh0bWwnLFxufSlcblxuZXhwb3J0IGNsYXNzIEJsb2NrV3JhcHBlckNvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgfVxuXG4gICAgdHJhY2tCeUZuKGlkeDogbnVtYmVyLCBpdGVtPzogYW55KSB7XG4gICAgICAgIHJldHVybiBpZHg7XG4gICAgfVxuXG59XG4iXX0=