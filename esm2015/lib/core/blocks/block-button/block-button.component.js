/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-button/block-button.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
export class BlockButtonComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.buttonPreText;
    }
}
BlockButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-button',
                template: "<div class=\"button-block\">\n    <p *ngIf=\"block.data.buttonPreText\">\n        <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n    </p>\n    <a [href]=\"block.data.documentId ? block.data.documentId : block.data.buttonLink\"\n       target=\"{{block.data.target}}\" class=\"btn {{block.data.buttonType}}\" role=\"button\">{{block.data.buttonText}}\n    </a>\n</div>\n"
            }] }
];
/** @nocollapse */
BlockButtonComponent.ctorParameters = () => [
    { type: AuthService }
];
BlockButtonComponent.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockButtonComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockButtonComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYmxvY2tzL2Jsb2NrLWJ1dHRvbi9ibG9jay1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDdkQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBT3BELE1BQU0sT0FBTyxvQkFBcUIsU0FBUSxzQkFBc0I7Ozs7SUFJNUQsWUFDYyxXQUF3QjtRQUVsQyxLQUFLLENBQ0QsV0FBVyxDQUNkLENBQUM7UUFKUSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUt0QyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QyxDQUFDOzs7WUFwQkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxtQkFBbUI7Z0JBQzdCLHVaQUE0QzthQUMvQzs7OztZQUxPLFdBQVc7OzttQkFTZCxLQUFLOzs7O0lBQU4sb0NBQXNCOzs7OztJQUdsQiwyQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jsb2NrQWJzdHJhY3RDb21wb25lbnR9IGZyb20gJy4uL2Jsb2NrLWFic3RyYWN0LmNvbXBvbmVudCc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi8uLi9hdXRoL2F1dGguc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay1idXR0b24nLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9ibG9jay1idXR0b24uY29tcG9uZW50Lmh0bWwnLFxufSlcblxuZXhwb3J0IGNsYXNzIEJsb2NrQnV0dG9uQ29tcG9uZW50IGV4dGVuZHMgQmxvY2tBYnN0cmFjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBASW5wdXQoKSB0ZXh0OiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBhdXRoU2VydmljZVxuICAgICAgICApO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICBzdXBlci5uZ09uSW5pdCgpO1xuICAgICAgICB0aGlzLnRleHQgPSB0aGlzLmJsb2NrLmRhdGEuYnV0dG9uUHJlVGV4dDtcbiAgICB9XG5cbn1cbiJdfQ==