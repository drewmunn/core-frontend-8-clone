/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-tabs/block-tabs.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
export class BlockTabsComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} idx
     * @return {?}
     */
    trackByFn(idx) {
        return idx;
    }
}
BlockTabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-tabs',
                template: "<mat-tab-group>\n    <mat-tab *ngFor=\"let item of block.content trackBy: trackByFn\">\n        <ng-template mat-tab-label>\n            <ng-container>{{item.data.header}}</ng-container>\n        </ng-template>\n        <div [innerHTML]=\"item.data.content\"></div>\n    </mat-tab>\n</mat-tab-group>\n"
            }] }
];
/** @nocollapse */
BlockTabsComponent.ctorParameters = () => [
    { type: AuthService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BlockTabsComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stdGFicy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2Nrcy9ibG9jay10YWJzL2Jsb2NrLXRhYnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUNoRCxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUNuRSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0seUJBQXlCLENBQUM7QUFNcEQsTUFBTSxPQUFPLGtCQUFtQixTQUFRLHNCQUFzQjs7OztJQUUxRCxZQUNjLFdBQXdCO1FBRWxDLEtBQUssQ0FDRCxXQUFXLENBQ2QsQ0FBQztRQUpRLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBS3RDLENBQUM7Ozs7SUFFRCxRQUFRO0lBQ1IsQ0FBQzs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBVztRQUNqQixPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7OztZQW5CSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGlCQUFpQjtnQkFDM0IseVRBQTBDO2FBQzdDOzs7O1lBTE8sV0FBVzs7Ozs7OztJQVNYLHlDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vLi4vYXV0aC9hdXRoLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtYmxvY2stdGFicycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLXRhYnMuY29tcG9uZW50Lmh0bWwnLFxufSlcbmV4cG9ydCBjbGFzcyBCbG9ja1RhYnNDb21wb25lbnQgZXh0ZW5kcyBCbG9ja0Fic3RyYWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgICkge1xuICAgICAgICBzdXBlcihcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgfVxuXG4gICAgdHJhY2tCeUZuKGlkeDogbnVtYmVyKSB7XG4gICAgICAgIHJldHVybiBpZHg7XG4gICAgfVxuXG59XG4iXX0=