/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-search/block-search.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { FormControl } from '@angular/forms';
import { SearchService } from '../../pages/search.service';
export class BlockSearchComponent extends BlockAbstractComponent {
    /**
     * @param {?} search
     */
    constructor(search) {
        super();
        this.search = search;
        this.query = new FormControl();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.search.refresh();
        this.query.setValue(this.search.getQuery());
    }
    /**
     * @return {?}
     */
    performSearch() {
        this.search.performSearch(this.query.value);
    }
}
BlockSearchComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-search',
                template: "<form #searchForm=\"ngForm\" (ngSubmit)=\"performSearch()\">\n    <div class=\"form-group\">\n        <label class=\"form-label question-box\">\n            <span class=\"sr-only\">Search</span></label>\n        <input type=\"text\" class=\"form-control form-control-lg\" [formControl]=\"query\"\n               placeholder=\"Type your question...\"/>\n    </div>\n\n    <button class=\"btn btn-primary\" [ngClass]=\"{'selected': query.value && query.value.length > 2}\"\n            [disabled]=\"!query.value || query.value.length < 3\">Search\n    </button>\n</form>"
            }] }
];
/** @nocollapse */
BlockSearchComponent.ctorParameters = () => [
    { type: SearchService }
];
if (false) {
    /** @type {?} */
    BlockSearchComponent.prototype.query;
    /**
     * @type {?}
     * @protected
     */
    BlockSearchComponent.prototype.search;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stc2VhcmNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYmxvY2tzL2Jsb2NrLXNlYXJjaC9ibG9jay1zZWFyY2guY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUNoRCxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUNuRSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDM0MsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLDRCQUE0QixDQUFDO0FBTXpELE1BQU0sT0FBTyxvQkFBcUIsU0FBUSxzQkFBc0I7Ozs7SUFHNUQsWUFDYyxNQUFxQjtRQUUvQixLQUFLLEVBQUUsQ0FBQztRQUZFLFdBQU0sR0FBTixNQUFNLENBQWU7UUFIbkMsVUFBSyxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7SUFNMUIsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUNoRCxDQUFDOzs7O0lBRUQsYUFBYTtRQUNULElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7O1lBcEJKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsbUJBQW1CO2dCQUM3Qixva0JBQTRDO2FBQy9DOzs7O1lBTE8sYUFBYTs7OztJQU9qQixxQ0FBMEI7Ozs7O0lBR3RCLHNDQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCbG9ja0Fic3RyYWN0Q29tcG9uZW50fSBmcm9tICcuLi9ibG9jay1hYnN0cmFjdC5jb21wb25lbnQnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtTZWFyY2hTZXJ2aWNlfSBmcm9tICcuLi8uLi9wYWdlcy9zZWFyY2guc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay1zZWFyY2gnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9ibG9jay1zZWFyY2guY29tcG9uZW50Lmh0bWwnLFxufSlcbmV4cG9ydCBjbGFzcyBCbG9ja1NlYXJjaENvbXBvbmVudCBleHRlbmRzIEJsb2NrQWJzdHJhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIHF1ZXJ5ID0gbmV3IEZvcm1Db250cm9sKCk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIHNlYXJjaDogU2VhcmNoU2VydmljZVxuICAgICkge1xuICAgICAgICBzdXBlcigpO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnNlYXJjaC5yZWZyZXNoKCk7XG4gICAgICAgIHRoaXMucXVlcnkuc2V0VmFsdWUodGhpcy5zZWFyY2guZ2V0UXVlcnkoKSk7XG4gICAgfVxuXG4gICAgcGVyZm9ybVNlYXJjaCgpIHtcbiAgICAgICAgdGhpcy5zZWFyY2gucGVyZm9ybVNlYXJjaCh0aGlzLnF1ZXJ5LnZhbHVlKTtcbiAgICB9XG5cbn1cbiJdfQ==