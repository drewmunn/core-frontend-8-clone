/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-compile.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Compiler, Component, Directive, Input, NgModule, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { AuthService } from '../auth/auth.service';
export class BlockCompileDirective {
    /**
     * @param {?} vcRef
     * @param {?} compiler
     */
    constructor(vcRef, compiler) {
        this.vcRef = vcRef;
        this.compiler = compiler;
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        if (!this.coreBlockCompile) {
            if (this.compRef) {
                this.updateProperties();
                return;
            }
            throw Error('Template not specified');
        }
        this.vcRef.clear();
        this.compRef = null;
        /** @type {?} */
        const component = this.createDynamicComponent(this.coreBlockCompile);
        /** @type {?} */
        const module = this.createDynamicModule(component);
        this.compiler.compileModuleAndAllComponentsAsync(module)
            .then((/**
         * @param {?} moduleWithFactories
         * @return {?}
         */
        (moduleWithFactories) => {
            /** @type {?} */
            const compFactory = moduleWithFactories.componentFactories.find((/**
             * @param {?} a
             * @return {?}
             */
            a => a.componentType === component));
            this.compRef = this.vcRef.createComponent(compFactory);
            this.updateProperties();
        }));
    }
    /**
     * @return {?}
     */
    updateProperties() {
        for (const prop of Object.values(this.coreBlockCompileContext)) {
            this.compRef.instance[prop] = this.coreBlockCompileContext[prop];
        }
    }
    /**
     * @private
     * @param {?} template
     * @return {?}
     */
    createDynamicComponent(template) {
        class DynamicComponent {
            /**
             * @param {?} authService
             */
            constructor(authService) {
                this.authService = authService;
            }
            /**
             * @return {?}
             */
            ngOnInit() {
                this.user = this.authService.getUser();
            }
        }
        DynamicComponent.decorators = [
            { type: Component, args: [{
                        selector: 'core-dynamic-block',
                        template,
                    },] },
        ];
        /** @nocollapse */
        DynamicComponent.ctorParameters = () => [
            { type: AuthService }
        ];
        if (false) {
            /** @type {?} */
            DynamicComponent.prototype.user;
            /**
             * @type {?}
             * @protected
             */
            DynamicComponent.prototype.authService;
        }
        return DynamicComponent;
    }
    /**
     * @private
     * @param {?} component
     * @return {?}
     */
    createDynamicModule(component) {
        class DynamicModule {
        }
        DynamicModule.decorators = [
            { type: NgModule, args: [{
                        imports: [CommonModule, TranslateModule],
                        declarations: [component]
                    },] },
        ];
        return DynamicModule;
    }
}
BlockCompileDirective.decorators = [
    { type: Directive, args: [{
                selector: '[coreBlockCompile]'
            },] }
];
/** @nocollapse */
BlockCompileDirective.ctorParameters = () => [
    { type: ViewContainerRef },
    { type: Compiler }
];
BlockCompileDirective.propDecorators = {
    coreBlockCompile: [{ type: Input }],
    coreBlockCompileContext: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockCompileDirective.prototype.coreBlockCompile;
    /** @type {?} */
    BlockCompileDirective.prototype.coreBlockCompileContext;
    /** @type {?} */
    BlockCompileDirective.prototype.compRef;
    /**
     * @type {?}
     * @private
     */
    BlockCompileDirective.prototype.vcRef;
    /**
     * @type {?}
     * @private
     */
    BlockCompileDirective.prototype.compiler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stY29tcGlsZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2Nrcy9ibG9jay1jb21waWxlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFDSCxRQUFRLEVBQ1IsU0FBUyxFQUVULFNBQVMsRUFDVCxLQUFLLEVBRUwsUUFBUSxFQUlSLGdCQUFnQixFQUNuQixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQ3BELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUtqRCxNQUFNLE9BQU8scUJBQXFCOzs7OztJQU05QixZQUFvQixLQUF1QixFQUN2QixRQUFrQjtRQURsQixVQUFLLEdBQUwsS0FBSyxDQUFrQjtRQUN2QixhQUFRLEdBQVIsUUFBUSxDQUFVO0lBQ3RDLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN4QixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQ3hCLE9BQU87YUFDVjtZQUNELE1BQU0sS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7U0FDekM7UUFFRCxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDOztjQUVkLFNBQVMsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDOztjQUM5RCxNQUFNLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQztRQUNsRCxJQUFJLENBQUMsUUFBUSxDQUFDLGtDQUFrQyxDQUFDLE1BQU0sQ0FBQzthQUNuRCxJQUFJOzs7O1FBQUMsQ0FBQyxtQkFBc0QsRUFBRSxFQUFFOztrQkFDdkQsV0FBVyxHQUFHLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxhQUFhLEtBQUssU0FBUyxFQUFDO1lBQ25HLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDNUIsQ0FBQyxFQUFDLENBQUM7SUFDWCxDQUFDOzs7O0lBRUQsZ0JBQWdCO1FBQ1osS0FBSyxNQUFNLElBQUksSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFO1lBQzVELElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNwRTtJQUNMLENBQUM7Ozs7OztJQUVPLHNCQUFzQixDQUFDLFFBQWdCO1FBQzNDLE1BSU0sZ0JBQWdCOzs7O1lBR2xCLFlBQ2MsV0FBd0I7Z0JBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1lBRXRDLENBQUM7Ozs7WUFFRCxRQUFRO2dCQUNKLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUMzQyxDQUFDOzs7b0JBZEosU0FBUyxTQUFDO3dCQUNQLFFBQVEsRUFBRSxvQkFBb0I7d0JBQzlCLFFBQVE7cUJBQ1g7Ozs7b0JBL0NELFdBQVc7Ozs7WUFpRFAsZ0NBQVU7Ozs7O1lBR04sdUNBQWtDOztRQVMxQyxPQUFPLGdCQUFnQixDQUFDO0lBQzVCLENBQUM7Ozs7OztJQUVPLG1CQUFtQixDQUFDLFNBQW9CO1FBQzVDLE1BSU0sYUFBYTs7O29CQUpsQixRQUFRLFNBQUM7d0JBQ04sT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLGVBQWUsQ0FBQzt3QkFDeEMsWUFBWSxFQUFFLENBQUMsU0FBUyxDQUFDO3FCQUM1Qjs7UUFJRCxPQUFPLGFBQWEsQ0FBQztJQUN6QixDQUFDOzs7WUF2RUosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxvQkFBb0I7YUFDakM7Ozs7WUFSRyxnQkFBZ0I7WUFWaEIsUUFBUTs7OytCQW9CUCxLQUFLO3NDQUNMLEtBQUs7Ozs7SUFETixpREFBa0M7O0lBQ2xDLHdEQUF3Qzs7SUFFeEMsd0NBQTJCOzs7OztJQUVmLHNDQUErQjs7Ozs7SUFDL0IseUNBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgICBDb21waWxlcixcbiAgICBDb21wb25lbnQsXG4gICAgQ29tcG9uZW50UmVmLFxuICAgIERpcmVjdGl2ZSxcbiAgICBJbnB1dCxcbiAgICBNb2R1bGVXaXRoQ29tcG9uZW50RmFjdG9yaWVzLFxuICAgIE5nTW9kdWxlLFxuICAgIE9uQ2hhbmdlcyxcbiAgICBPbkluaXQsXG4gICAgVHlwZSxcbiAgICBWaWV3Q29udGFpbmVyUmVmXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge1RyYW5zbGF0ZU1vZHVsZX0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi9hdXRoL2F1dGguc2VydmljZSc7XG5cbkBEaXJlY3RpdmUoe1xuICAgIHNlbGVjdG9yOiAnW2NvcmVCbG9ja0NvbXBpbGVdJ1xufSlcbmV4cG9ydCBjbGFzcyBCbG9ja0NvbXBpbGVEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xuICAgIEBJbnB1dCgpIGNvcmVCbG9ja0NvbXBpbGU6IHN0cmluZztcbiAgICBASW5wdXQoKSBjb3JlQmxvY2tDb21waWxlQ29udGV4dDogYW55W107XG5cbiAgICBjb21wUmVmOiBDb21wb25lbnRSZWY8YW55PjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgdmNSZWY6IFZpZXdDb250YWluZXJSZWYsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBjb21waWxlcjogQ29tcGlsZXIpIHtcbiAgICB9XG5cbiAgICBuZ09uQ2hhbmdlcygpIHtcbiAgICAgICAgaWYgKCF0aGlzLmNvcmVCbG9ja0NvbXBpbGUpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmNvbXBSZWYpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVByb3BlcnRpZXMoKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aHJvdyBFcnJvcignVGVtcGxhdGUgbm90IHNwZWNpZmllZCcpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy52Y1JlZi5jbGVhcigpO1xuICAgICAgICB0aGlzLmNvbXBSZWYgPSBudWxsO1xuXG4gICAgICAgIGNvbnN0IGNvbXBvbmVudCA9IHRoaXMuY3JlYXRlRHluYW1pY0NvbXBvbmVudCh0aGlzLmNvcmVCbG9ja0NvbXBpbGUpO1xuICAgICAgICBjb25zdCBtb2R1bGUgPSB0aGlzLmNyZWF0ZUR5bmFtaWNNb2R1bGUoY29tcG9uZW50KTtcbiAgICAgICAgdGhpcy5jb21waWxlci5jb21waWxlTW9kdWxlQW5kQWxsQ29tcG9uZW50c0FzeW5jKG1vZHVsZSlcbiAgICAgICAgICAgIC50aGVuKChtb2R1bGVXaXRoRmFjdG9yaWVzOiBNb2R1bGVXaXRoQ29tcG9uZW50RmFjdG9yaWVzPGFueT4pID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBjb21wRmFjdG9yeSA9IG1vZHVsZVdpdGhGYWN0b3JpZXMuY29tcG9uZW50RmFjdG9yaWVzLmZpbmQoYSA9PiBhLmNvbXBvbmVudFR5cGUgPT09IGNvbXBvbmVudCk7XG4gICAgICAgICAgICAgICAgdGhpcy5jb21wUmVmID0gdGhpcy52Y1JlZi5jcmVhdGVDb21wb25lbnQoY29tcEZhY3RvcnkpO1xuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUHJvcGVydGllcygpO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdXBkYXRlUHJvcGVydGllcygpIHtcbiAgICAgICAgZm9yIChjb25zdCBwcm9wIG9mIE9iamVjdC52YWx1ZXModGhpcy5jb3JlQmxvY2tDb21waWxlQ29udGV4dCkpIHtcbiAgICAgICAgICAgIHRoaXMuY29tcFJlZi5pbnN0YW5jZVtwcm9wXSA9IHRoaXMuY29yZUJsb2NrQ29tcGlsZUNvbnRleHRbcHJvcF07XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGNyZWF0ZUR5bmFtaWNDb21wb25lbnQodGVtcGxhdGU6IHN0cmluZykge1xuICAgICAgICBAQ29tcG9uZW50KHtcbiAgICAgICAgICAgIHNlbGVjdG9yOiAnY29yZS1keW5hbWljLWJsb2NrJyxcbiAgICAgICAgICAgIHRlbXBsYXRlLFxuICAgICAgICB9KVxuICAgICAgICBjbGFzcyBEeW5hbWljQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICAgICAgICAgIHVzZXI6IGFueTtcblxuICAgICAgICAgICAgY29uc3RydWN0b3IoXG4gICAgICAgICAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXIgPSB0aGlzLmF1dGhTZXJ2aWNlLmdldFVzZXIoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBEeW5hbWljQ29tcG9uZW50O1xuICAgIH1cblxuICAgIHByaXZhdGUgY3JlYXRlRHluYW1pY01vZHVsZShjb21wb25lbnQ6IFR5cGU8YW55Pikge1xuICAgICAgICBATmdNb2R1bGUoe1xuICAgICAgICAgICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgVHJhbnNsYXRlTW9kdWxlXSxcbiAgICAgICAgICAgIGRlY2xhcmF0aW9uczogW2NvbXBvbmVudF1cbiAgICAgICAgfSlcbiAgICAgICAgY2xhc3MgRHluYW1pY01vZHVsZSB7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gRHluYW1pY01vZHVsZTtcbiAgICB9XG59Il19