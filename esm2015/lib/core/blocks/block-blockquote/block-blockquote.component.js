/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-blockquote/block-blockquote.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { AuthService } from '../../auth/auth.service';
export class BlockBlockquoteComponent extends BlockAbstractComponent {
    /**
     * @param {?} authService
     */
    constructor(authService) {
        super(authService);
        this.authService = authService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        super.ngOnInit();
        this.text = this.block.data.blockQuote;
    }
}
BlockBlockquoteComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-blockquote',
                template: "<blockquote>\n    <p>\n        <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n    </p>\n    <footer *ngIf=\"block.data.cite\">\n        <cite>\n            <a [href]=\"block.data.citeLink\" *ngIf=\"block.data.citeLink\">{{block.data.cite}}</a>\n            <span *ngIf=\"!block.data.citeLink\">{{block.data.cite}}</span>\n        </cite>\n    </footer>\n</blockquote>"
            }] }
];
/** @nocollapse */
BlockBlockquoteComponent.ctorParameters = () => [
    { type: AuthService }
];
BlockBlockquoteComponent.propDecorators = {
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BlockBlockquoteComponent.prototype.text;
    /**
     * @type {?}
     * @protected
     */
    BlockBlockquoteComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stYmxvY2txdW90ZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2Jsb2Nrcy9ibG9jay1ibG9ja3F1b3RlL2Jsb2NrLWJsb2NrcXVvdGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFDdkQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDbkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBT3BELE1BQU0sT0FBTyx3QkFBeUIsU0FBUSxzQkFBc0I7Ozs7SUFJaEUsWUFDYyxXQUF3QjtRQUVsQyxLQUFLLENBQ0QsV0FBVyxDQUNkLENBQUM7UUFKUSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUt0QyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQyxDQUFDOzs7WUFwQkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSx1QkFBdUI7Z0JBQ2pDLHFaQUFnRDthQUNuRDs7OztZQUxPLFdBQVc7OzttQkFTZCxLQUFLOzs7O0lBQU4sd0NBQXNCOzs7OztJQUdsQiwrQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jsb2NrQWJzdHJhY3RDb21wb25lbnR9IGZyb20gJy4uL2Jsb2NrLWFic3RyYWN0LmNvbXBvbmVudCc7XG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi8uLi9hdXRoL2F1dGguc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay1ibG9ja3F1b3RlJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vYmxvY2stYmxvY2txdW90ZS5jb21wb25lbnQuaHRtbCcsXG59KVxuXG5leHBvcnQgY2xhc3MgQmxvY2tCbG9ja3F1b3RlQ29tcG9uZW50IGV4dGVuZHMgQmxvY2tBYnN0cmFjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBASW5wdXQoKSB0ZXh0OiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICApIHtcbiAgICAgICAgc3VwZXIoXG4gICAgICAgICAgICBhdXRoU2VydmljZVxuICAgICAgICApO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICBzdXBlci5uZ09uSW5pdCgpO1xuICAgICAgICB0aGlzLnRleHQgPSB0aGlzLmJsb2NrLmRhdGEuYmxvY2tRdW90ZTtcbiAgICB9XG5cbn1cbiJdfQ==