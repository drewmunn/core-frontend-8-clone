/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/blocks/block-language/block-language.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BlockAbstractComponent } from '../block-abstract.component';
import { TranslateService } from '../../pages/translate.service';
import { TranslateService as TranslateProvider } from '@ngx-translate/core';
export class BlockLanguageComponent extends BlockAbstractComponent {
    /**
     * @param {?} translate
     * @param {?} translateProvider
     */
    constructor(translate, translateProvider) {
        super();
        this.translate = translate;
        this.translateProvider = translateProvider;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.translate.getLanguages().subscribe((/**
         * @param {?} languages
         * @return {?}
         */
        languages => {
            this.languages$ = languages;
        }));
    }
    /**
     * @param {?} languageCode
     * @return {?}
     */
    setLanguage(languageCode) {
        this.translateProvider.use(languageCode);
        this.translate.setDefaultLanguage(languageCode);
    }
}
BlockLanguageComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-block-language',
                template: "<mat-nav-list>\n    <mat-list-item *ngFor=\"let language of languages$\" [ngClass]=\"{'active' : language.code === ''}\">\n        <a matLine (click)=\"setLanguage(language.code)\">{{language.name}}</a>\n    </mat-list-item>\n</mat-nav-list>"
            }] }
];
/** @nocollapse */
BlockLanguageComponent.ctorParameters = () => [
    { type: TranslateService },
    { type: TranslateProvider }
];
if (false) {
    /** @type {?} */
    BlockLanguageComponent.prototype.languages$;
    /** @type {?} */
    BlockLanguageComponent.prototype.language;
    /**
     * @type {?}
     * @protected
     */
    BlockLanguageComponent.prototype.translate;
    /**
     * @type {?}
     * @protected
     */
    BlockLanguageComponent.prototype.translateProvider;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxvY2stbGFuZ3VhZ2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS9ibG9ja3MvYmxvY2stbGFuZ3VhZ2UvYmxvY2stbGFuZ3VhZ2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUNoRCxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUNuRSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQUMvRCxPQUFPLEVBQUMsZ0JBQWdCLElBQUksaUJBQWlCLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQU8xRSxNQUFNLE9BQU8sc0JBQXVCLFNBQVEsc0JBQXNCOzs7OztJQUk5RCxZQUNjLFNBQTJCLEVBQzNCLGlCQUFvQztRQUU5QyxLQUFLLEVBQUUsQ0FBQztRQUhFLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7SUFHbEQsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDLFNBQVM7Ozs7UUFBQyxTQUFTLENBQUMsRUFBRTtZQUNoRCxJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztRQUNoQyxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLFlBQW9CO1FBQzVCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNwRCxDQUFDOzs7WUF6QkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxxQkFBcUI7Z0JBQy9CLDZQQUE4QzthQUNqRDs7OztZQU5PLGdCQUFnQjtZQUNJLGlCQUFpQjs7OztJQVF6Qyw0Q0FBZ0I7O0lBQ2hCLDBDQUFjOzs7OztJQUdWLDJDQUFxQzs7Ozs7SUFDckMsbURBQThDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jsb2NrQWJzdHJhY3RDb21wb25lbnR9IGZyb20gJy4uL2Jsb2NrLWFic3RyYWN0LmNvbXBvbmVudCc7XG5pbXBvcnQge1RyYW5zbGF0ZVNlcnZpY2V9IGZyb20gJy4uLy4uL3BhZ2VzL3RyYW5zbGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7VHJhbnNsYXRlU2VydmljZSBhcyBUcmFuc2xhdGVQcm92aWRlcn0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1ibG9jay1sYW5ndWFnZScsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jsb2NrLWxhbmd1YWdlLmNvbXBvbmVudC5odG1sJyxcbn0pXG5cbmV4cG9ydCBjbGFzcyBCbG9ja0xhbmd1YWdlQ29tcG9uZW50IGV4dGVuZHMgQmxvY2tBYnN0cmFjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgbGFuZ3VhZ2VzJDogYW55O1xuICAgIGxhbmd1YWdlOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSxcbiAgICAgICAgcHJvdGVjdGVkIHRyYW5zbGF0ZVByb3ZpZGVyOiBUcmFuc2xhdGVQcm92aWRlcixcbiAgICApIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy50cmFuc2xhdGUuZ2V0TGFuZ3VhZ2VzKCkuc3Vic2NyaWJlKGxhbmd1YWdlcyA9PiB7XG4gICAgICAgICAgICB0aGlzLmxhbmd1YWdlcyQgPSBsYW5ndWFnZXM7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHNldExhbmd1YWdlKGxhbmd1YWdlQ29kZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMudHJhbnNsYXRlUHJvdmlkZXIudXNlKGxhbmd1YWdlQ29kZSk7XG4gICAgICAgIHRoaXMudHJhbnNsYXRlLnNldERlZmF1bHRMYW5ndWFnZShsYW5ndWFnZUNvZGUpO1xuICAgIH1cblxufVxuIl19