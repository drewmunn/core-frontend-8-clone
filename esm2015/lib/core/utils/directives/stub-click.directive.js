/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/directives/stub-click.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener } from '@angular/core';
export class StubClickDirective {
    /**
     * @param {?} event
     * @return {?}
     */
    onMouseDown(event) {
        event.preventDefault();
    }
}
StubClickDirective.decorators = [
    { type: Directive, args: [{
                selector: '[coreStubClick]'
            },] }
];
StubClickDirective.propDecorators = {
    onMouseDown: [{ type: HostListener, args: ['click', ['$event'],] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R1Yi1jbGljay5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3V0aWxzL2RpcmVjdGl2ZXMvc3R1Yi1jbGljay5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUt4RCxNQUFNLE9BQU8sa0JBQWtCOzs7OztJQUc3QixXQUFXLENBQUMsS0FBaUI7UUFDM0IsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3pCLENBQUM7OztZQVJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsaUJBQWlCO2FBQzVCOzs7MEJBR0UsWUFBWSxTQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICBzZWxlY3RvcjogJ1tjb3JlU3R1YkNsaWNrXSdcclxufSlcclxuZXhwb3J0IGNsYXNzIFN0dWJDbGlja0RpcmVjdGl2ZSB7XHJcblxyXG4gIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJywgWyckZXZlbnQnXSlcclxuICBvbk1vdXNlRG93bihldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICB9XHJcbn1cclxuIl19