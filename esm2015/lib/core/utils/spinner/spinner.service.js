/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/spinner/spinner.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class SpinnerService {
    constructor() {
        this.spinnerCache = new Set();
    }
    /**
     * @param {?} spinner
     * @return {?}
     */
    _register(spinner) {
        this.spinnerCache.add(spinner);
    }
    /**
     * @param {?} spinnerToRemove
     * @return {?}
     */
    _unregister(spinnerToRemove) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner === spinnerToRemove) {
                this.spinnerCache.delete(spinner);
            }
        }));
    }
    /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    _unregisterGroup(spinnerGroup) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.group === spinnerGroup) {
                this.spinnerCache.delete(spinner);
            }
        }));
    }
    /**
     * @return {?}
     */
    _unregisterAll() {
        this.spinnerCache.clear();
    }
    /**
     * @param {?} spinnerName
     * @return {?}
     */
    show(spinnerName) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.name === spinnerName) {
                spinner.show = true;
            }
        }));
    }
    /**
     * @param {?} spinnerName
     * @return {?}
     */
    hide(spinnerName) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.name === spinnerName) {
                spinner.show = false;
            }
        }));
    }
    /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    showGroup(spinnerGroup) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.group === spinnerGroup) {
                spinner.show = true;
            }
        }));
    }
    /**
     * @param {?} spinnerGroup
     * @return {?}
     */
    hideGroup(spinnerGroup) {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.group === spinnerGroup) {
                spinner.show = false;
            }
        }));
    }
    /**
     * @return {?}
     */
    showAll() {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => spinner.show = true));
    }
    /**
     * @return {?}
     */
    hideAll() {
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => spinner.show = false));
    }
    /**
     * @param {?} spinnerName
     * @return {?}
     */
    isShowing(spinnerName) {
        /** @type {?} */
        let showing = undefined;
        this.spinnerCache.forEach((/**
         * @param {?} spinner
         * @return {?}
         */
        spinner => {
            if (spinner.name === spinnerName) {
                showing = spinner.show;
            }
        }));
        return showing;
    }
}
SpinnerService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ SpinnerService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SpinnerService_Factory() { return new SpinnerService(); }, token: SpinnerService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    SpinnerService.prototype.spinnerCache;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Bpbm5lci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy9zcGlubmVyL3NwaW5uZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7O0FBTXpDLE1BQU0sT0FBTyxjQUFjO0lBSDNCO1FBSVksaUJBQVksR0FBRyxJQUFJLEdBQUcsRUFBb0IsQ0FBQztLQTJFdEQ7Ozs7O0lBekVHLFNBQVMsQ0FBQyxPQUF5QjtRQUMvQixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxlQUFpQztRQUN6QyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU87Ozs7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNoQyxJQUFJLE9BQU8sS0FBSyxlQUFlLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ3JDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLFlBQW9CO1FBQ2pDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTzs7OztRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ2hDLElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxZQUFZLEVBQUU7Z0JBQ2hDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ3JDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsY0FBYztRQUNWLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDOUIsQ0FBQzs7Ozs7SUFFRCxJQUFJLENBQUMsV0FBbUI7UUFDcEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPOzs7O1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDaEMsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFBRTtnQkFDOUIsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7YUFDdkI7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsSUFBSSxDQUFDLFdBQW1CO1FBQ3BCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTzs7OztRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ2hDLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxXQUFXLEVBQUU7Z0JBQzlCLE9BQU8sQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO2FBQ3hCO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELFNBQVMsQ0FBQyxZQUFvQjtRQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU87Ozs7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNoQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLEtBQUssWUFBWSxFQUFFO2dCQUNoQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzthQUN2QjtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxTQUFTLENBQUMsWUFBb0I7UUFDMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPOzs7O1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDaEMsSUFBSSxPQUFPLENBQUMsS0FBSyxLQUFLLFlBQVksRUFBRTtnQkFDaEMsT0FBTyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7YUFDeEI7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPOzs7O1FBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksRUFBQyxDQUFDO0lBQzlELENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPOzs7O1FBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLEtBQUssRUFBQyxDQUFDO0lBQy9ELENBQUM7Ozs7O0lBRUQsU0FBUyxDQUFDLFdBQW1COztZQUNyQixPQUFPLEdBQUcsU0FBUztRQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU87Ozs7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNoQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssV0FBVyxFQUFFO2dCQUM5QixPQUFPLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQzthQUMxQjtRQUNMLENBQUMsRUFBQyxDQUFDO1FBQ0gsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQzs7O1lBOUVKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7Ozs7Ozs7SUFFRyxzQ0FBbUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtTcGlubmVyQ29tcG9uZW50fSBmcm9tICcuL3NwaW5uZXIuY29tcG9uZW50JztcblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBTcGlubmVyU2VydmljZSB7XG4gICAgcHJpdmF0ZSBzcGlubmVyQ2FjaGUgPSBuZXcgU2V0PFNwaW5uZXJDb21wb25lbnQ+KCk7XG5cbiAgICBfcmVnaXN0ZXIoc3Bpbm5lcjogU3Bpbm5lckNvbXBvbmVudCk6IHZvaWQge1xuICAgICAgICB0aGlzLnNwaW5uZXJDYWNoZS5hZGQoc3Bpbm5lcik7XG4gICAgfVxuXG4gICAgX3VucmVnaXN0ZXIoc3Bpbm5lclRvUmVtb3ZlOiBTcGlubmVyQ29tcG9uZW50KTogdm9pZCB7XG4gICAgICAgIHRoaXMuc3Bpbm5lckNhY2hlLmZvckVhY2goc3Bpbm5lciA9PiB7XG4gICAgICAgICAgICBpZiAoc3Bpbm5lciA9PT0gc3Bpbm5lclRvUmVtb3ZlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZGVsZXRlKHNwaW5uZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBfdW5yZWdpc3Rlckdyb3VwKHNwaW5uZXJHcm91cDogc3RyaW5nKTogdm9pZCB7XG4gICAgICAgIHRoaXMuc3Bpbm5lckNhY2hlLmZvckVhY2goc3Bpbm5lciA9PiB7XG4gICAgICAgICAgICBpZiAoc3Bpbm5lci5ncm91cCA9PT0gc3Bpbm5lckdyb3VwKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZGVsZXRlKHNwaW5uZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBfdW5yZWdpc3RlckFsbCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuY2xlYXIoKTtcbiAgICB9XG5cbiAgICBzaG93KHNwaW5uZXJOYW1lOiBzdHJpbmcpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZm9yRWFjaChzcGlubmVyID0+IHtcbiAgICAgICAgICAgIGlmIChzcGlubmVyLm5hbWUgPT09IHNwaW5uZXJOYW1lKSB7XG4gICAgICAgICAgICAgICAgc3Bpbm5lci5zaG93ID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgaGlkZShzcGlubmVyTmFtZTogc3RyaW5nKTogdm9pZCB7XG4gICAgICAgIHRoaXMuc3Bpbm5lckNhY2hlLmZvckVhY2goc3Bpbm5lciA9PiB7XG4gICAgICAgICAgICBpZiAoc3Bpbm5lci5uYW1lID09PSBzcGlubmVyTmFtZSkge1xuICAgICAgICAgICAgICAgIHNwaW5uZXIuc2hvdyA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzaG93R3JvdXAoc3Bpbm5lckdyb3VwOiBzdHJpbmcpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZm9yRWFjaChzcGlubmVyID0+IHtcbiAgICAgICAgICAgIGlmIChzcGlubmVyLmdyb3VwID09PSBzcGlubmVyR3JvdXApIHtcbiAgICAgICAgICAgICAgICBzcGlubmVyLnNob3cgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBoaWRlR3JvdXAoc3Bpbm5lckdyb3VwOiBzdHJpbmcpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZm9yRWFjaChzcGlubmVyID0+IHtcbiAgICAgICAgICAgIGlmIChzcGlubmVyLmdyb3VwID09PSBzcGlubmVyR3JvdXApIHtcbiAgICAgICAgICAgICAgICBzcGlubmVyLnNob3cgPSBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc2hvd0FsbCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyQ2FjaGUuZm9yRWFjaChzcGlubmVyID0+IHNwaW5uZXIuc2hvdyA9IHRydWUpO1xuICAgIH1cblxuICAgIGhpZGVBbGwoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuc3Bpbm5lckNhY2hlLmZvckVhY2goc3Bpbm5lciA9PiBzcGlubmVyLnNob3cgPSBmYWxzZSk7XG4gICAgfVxuXG4gICAgaXNTaG93aW5nKHNwaW5uZXJOYW1lOiBzdHJpbmcpOiBib29sZWFuIHwgdW5kZWZpbmVkIHtcbiAgICAgICAgbGV0IHNob3dpbmcgPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMuc3Bpbm5lckNhY2hlLmZvckVhY2goc3Bpbm5lciA9PiB7XG4gICAgICAgICAgICBpZiAoc3Bpbm5lci5uYW1lID09PSBzcGlubmVyTmFtZSkge1xuICAgICAgICAgICAgICAgIHNob3dpbmcgPSBzcGlubmVyLnNob3c7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gc2hvd2luZztcbiAgICB9XG59Il19