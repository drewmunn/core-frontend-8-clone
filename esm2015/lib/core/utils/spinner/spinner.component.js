/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/spinner/spinner.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SpinnerService } from './spinner.service';
export class SpinnerComponent {
    /**
     * @param {?} spinnerService
     */
    constructor(spinnerService) {
        this.spinnerService = spinnerService;
        this.isShowing = false;
        this.showChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    get show() {
        return this.isShowing;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set show(val) {
        this.isShowing = val;
        this.showChange.emit(this.isShowing);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.name) {
            throw new Error('Spinner must have a \'name\' attribute.');
        }
        this.spinnerService._register(this);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.spinnerService._unregister(this);
    }
}
SpinnerComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-spinner',
                template: `
        <mat-spinner color="primary" diameter="30" *ngIf="show"></mat-spinner>
    `
            }] }
];
/** @nocollapse */
SpinnerComponent.ctorParameters = () => [
    { type: SpinnerService }
];
SpinnerComponent.propDecorators = {
    name: [{ type: Input }],
    group: [{ type: Input }],
    loadingImage: [{ type: Input }],
    show: [{ type: Input }],
    showChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    SpinnerComponent.prototype.name;
    /** @type {?} */
    SpinnerComponent.prototype.group;
    /** @type {?} */
    SpinnerComponent.prototype.loadingImage;
    /**
     * @type {?}
     * @private
     */
    SpinnerComponent.prototype.isShowing;
    /** @type {?} */
    SpinnerComponent.prototype.showChange;
    /**
     * @type {?}
     * @private
     */
    SpinnerComponent.prototype.spinnerService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Bpbm5lci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3V0aWxzL3NwaW5uZXIvc3Bpbm5lci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN4RixPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFRakQsTUFBTSxPQUFPLGdCQUFnQjs7OztJQUN6QixZQUFvQixjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFPMUMsY0FBUyxHQUFHLEtBQUssQ0FBQztRQU9oQixlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQWIxQyxDQUFDOzs7O0lBUUQsSUFDSSxJQUFJO1FBQ0osT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBSUQsSUFBSSxJQUFJLENBQUMsR0FBWTtRQUNqQixJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDekMsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNaLE1BQU0sSUFBSSxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQTtTQUM3RDtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3hDLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7O1lBckNKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsY0FBYztnQkFDeEIsUUFBUSxFQUFFOztLQUVUO2FBQ0o7Ozs7WUFQTyxjQUFjOzs7bUJBWWpCLEtBQUs7b0JBQ0wsS0FBSzsyQkFDTCxLQUFLO21CQUlMLEtBQUs7eUJBS0wsTUFBTTs7OztJQVhQLGdDQUFzQjs7SUFDdEIsaUNBQXVCOztJQUN2Qix3Q0FBOEI7Ozs7O0lBRTlCLHFDQUEwQjs7SUFPMUIsc0NBQTBDOzs7OztJQWQ5QiwwQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25EZXN0cm95LCBPbkluaXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1NwaW5uZXJTZXJ2aWNlfSBmcm9tICcuL3NwaW5uZXIuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnY29yZS1zcGlubmVyJyxcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8bWF0LXNwaW5uZXIgY29sb3I9XCJwcmltYXJ5XCIgZGlhbWV0ZXI9XCIzMFwiICpuZ0lmPVwic2hvd1wiPjwvbWF0LXNwaW5uZXI+XG4gICAgYFxufSlcbmV4cG9ydCBjbGFzcyBTcGlubmVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgc3Bpbm5lclNlcnZpY2U6IFNwaW5uZXJTZXJ2aWNlKSB7XG4gICAgfVxuXG4gICAgQElucHV0KCkgbmFtZTogc3RyaW5nO1xuICAgIEBJbnB1dCgpIGdyb3VwOiBzdHJpbmc7XG4gICAgQElucHV0KCkgbG9hZGluZ0ltYWdlOiBzdHJpbmc7XG5cbiAgICBwcml2YXRlIGlzU2hvd2luZyA9IGZhbHNlO1xuXG4gICAgQElucHV0KClcbiAgICBnZXQgc2hvdygpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTaG93aW5nO1xuICAgIH1cblxuICAgIEBPdXRwdXQoKSBzaG93Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgc2V0IHNob3codmFsOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuaXNTaG93aW5nID0gdmFsO1xuICAgICAgICB0aGlzLnNob3dDaGFuZ2UuZW1pdCh0aGlzLmlzU2hvd2luZyk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgICAgIGlmICghdGhpcy5uYW1lKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1NwaW5uZXIgbXVzdCBoYXZlIGEgXFwnbmFtZVxcJyBhdHRyaWJ1dGUuJylcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnNwaW5uZXJTZXJ2aWNlLl9yZWdpc3Rlcih0aGlzKTtcbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zcGlubmVyU2VydmljZS5fdW5yZWdpc3Rlcih0aGlzKTtcbiAgICB9XG5cbn1cbiJdfQ==