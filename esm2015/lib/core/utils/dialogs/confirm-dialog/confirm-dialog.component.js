/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/dialogs/confirm-dialog/confirm-dialog.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
export class ConfirmDialogComponent {
    /**
     * @param {?} bsModalRef
     */
    constructor(bsModalRef) {
        this.bsModalRef = bsModalRef;
        this.onClose = new Subject();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    confirm($event) {
        this.onClose.next(true);
        this.onClose.complete();
        this.bsModalRef.hide();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    cancel($event) {
        this.onClose.next(false);
        this.onClose.complete();
        this.bsModalRef.hide();
    }
}
ConfirmDialogComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-confirm-dialog',
                template: `
        <div class="modal-header">
            <div class="modal-title" [innerHTML]="title">
            </div>
        </div>
        <div class="modal-body" [innerHTML]="message"></div>
        <div class="modal-footer" *ngIf="buttons">
            <button (click)="confirm($event)" type="button" class="btn bootbox-accept {{buttons.confirm.className}}">
                {{buttons.confirm.label}}</button>
            <button (click)="cancel($event)" type="button" class="btn bootbox-cancel {{buttons.cancel.className}}">
                {{buttons.cancel.label}}</button>
        </div>
    `
            }] }
];
/** @nocollapse */
ConfirmDialogComponent.ctorParameters = () => [
    { type: BsModalRef }
];
if (false) {
    /** @type {?} */
    ConfirmDialogComponent.prototype.title;
    /** @type {?} */
    ConfirmDialogComponent.prototype.message;
    /** @type {?} */
    ConfirmDialogComponent.prototype.buttons;
    /** @type {?} */
    ConfirmDialogComponent.prototype.onClose;
    /** @type {?} */
    ConfirmDialogComponent.prototype.bsModalRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybS1kaWFsb2cuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy9kaWFsb2dzL2NvbmZpcm0tZGlhbG9nL2NvbmZpcm0tZGlhbG9nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDeEMsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUMsT0FBTyxFQUFDLE1BQU0sTUFBTSxDQUFDO0FBa0I3QixNQUFNLE9BQU8sc0JBQXNCOzs7O0lBTS9CLFlBQW1CLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFGekMsWUFBTyxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFHeEIsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsTUFBa0I7UUFDdEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQWtCO1FBQ3JCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7WUFuQ0osU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxxQkFBcUI7Z0JBQy9CLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7O0tBWVQ7YUFDSjs7OztZQW5CTyxVQUFVOzs7O0lBcUJkLHVDQUFjOztJQUNkLHlDQUFnQjs7SUFDaEIseUNBQXVCOztJQUN2Qix5Q0FBd0I7O0lBRVosNENBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge0JzTW9kYWxSZWZ9IGZyb20gJ25neC1ib290c3RyYXAnO1xyXG5pbXBvcnQge0RpYWxvZ0J1dHRvbnN9IGZyb20gJy4uL2RpYWxvZ3Muc2VydmljZSc7XHJcbmltcG9ydCB7U3ViamVjdH0gZnJvbSAncnhqcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY29yZS1jb25maXJtLWRpYWxvZycsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1oZWFkZXJcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLXRpdGxlXCIgW2lubmVySFRNTF09XCJ0aXRsZVwiPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiIFtpbm5lckhUTUxdPVwibWVzc2FnZVwiPjwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIiAqbmdJZj1cImJ1dHRvbnNcIj5cclxuICAgICAgICAgICAgPGJ1dHRvbiAoY2xpY2spPVwiY29uZmlybSgkZXZlbnQpXCIgdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJvb3Rib3gtYWNjZXB0IHt7YnV0dG9ucy5jb25maXJtLmNsYXNzTmFtZX19XCI+XHJcbiAgICAgICAgICAgICAgICB7e2J1dHRvbnMuY29uZmlybS5sYWJlbH19PC9idXR0b24+XHJcbiAgICAgICAgICAgIDxidXR0b24gKGNsaWNrKT1cImNhbmNlbCgkZXZlbnQpXCIgdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJvb3Rib3gtY2FuY2VsIHt7YnV0dG9ucy5jYW5jZWwuY2xhc3NOYW1lfX1cIj5cclxuICAgICAgICAgICAgICAgIHt7YnV0dG9ucy5jYW5jZWwubGFiZWx9fTwvYnV0dG9uPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgYCxcclxufSlcclxuZXhwb3J0IGNsYXNzIENvbmZpcm1EaWFsb2dDb21wb25lbnQge1xyXG4gICAgdGl0bGU6IHN0cmluZztcclxuICAgIG1lc3NhZ2U6IHN0cmluZztcclxuICAgIGJ1dHRvbnM6IERpYWxvZ0J1dHRvbnM7XHJcbiAgICBvbkNsb3NlID0gbmV3IFN1YmplY3QoKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgYnNNb2RhbFJlZjogQnNNb2RhbFJlZikge1xyXG4gICAgfVxyXG5cclxuICAgIGNvbmZpcm0oJGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5vbkNsb3NlLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5vbkNsb3NlLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgdGhpcy5ic01vZGFsUmVmLmhpZGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBjYW5jZWwoJGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5vbkNsb3NlLm5leHQoZmFsc2UpO1xyXG4gICAgICAgIHRoaXMub25DbG9zZS5jb21wbGV0ZSgpO1xyXG4gICAgICAgIHRoaXMuYnNNb2RhbFJlZi5oaWRlKCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==