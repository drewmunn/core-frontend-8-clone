/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/dialogs/dialogs.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, RendererFactory2 } from '@angular/core';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { BsModalService } from 'ngx-bootstrap';
export class DialogsService {
    /**
     * @param {?} rendererFactory
     * @param {?} modalService
     */
    constructor(rendererFactory, modalService) {
        this.modalService = modalService;
        this.renderer = rendererFactory.createRenderer(null, null);
    }
    /**
     * @param {?} initialState
     * @return {?}
     */
    confirm(initialState) {
        this.playSound('messagebox');
        this.bsModalRef = this.modalService.show(ConfirmDialogComponent, {
            initialState,
            backdrop: 'static',
            keyboard: false,
            class: 'modal-dialog-centered'
        });
        this.renderer.addClass(document.querySelector('.modal'), 'modal-alert');
        return (/** @type {?} */ (this.bsModalRef.content.onClose));
    }
    /**
     * @param {?} sound
     * @param {?=} path
     * @return {?}
     */
    playSound(sound, path = 'assets/media/sound') {
        /** @type {?} */
        const audioElement = document.createElement('audio');
        if (navigator.userAgent.match('Firefox/')) {
            audioElement.setAttribute('src', path + '/' + sound + '.ogg');
        }
        else {
            audioElement.setAttribute('src', path + '/' + sound + '.mp3');
        }
        audioElement.addEventListener('load', (/**
         * @return {?}
         */
        () => {
            audioElement.play();
        }), true);
        audioElement.pause();
        audioElement.play();
    }
}
DialogsService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
DialogsService.ctorParameters = () => [
    { type: RendererFactory2 },
    { type: BsModalService }
];
if (false) {
    /** @type {?} */
    DialogsService.prototype.bsModalRef;
    /** @type {?} */
    DialogsService.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    DialogsService.prototype.modalService;
}
/**
 * @record
 */
export function DialogOptions() { }
if (false) {
    /** @type {?} */
    DialogOptions.prototype.title;
    /** @type {?} */
    DialogOptions.prototype.message;
    /** @type {?} */
    DialogOptions.prototype.buttons;
}
/**
 * @record
 */
export function DialogButton() { }
if (false) {
    /** @type {?} */
    DialogButton.prototype.label;
    /** @type {?} */
    DialogButton.prototype.className;
}
/**
 * @record
 */
export function DialogButtons() { }
if (false) {
    /** @type {?|undefined} */
    DialogButtons.prototype.confirm;
    /** @type {?|undefined} */
    DialogButtons.prototype.cancel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9ncy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy9kaWFsb2dzL2RpYWxvZ3Muc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQWEsZ0JBQWdCLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDdEUsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sMkNBQTJDLENBQUM7QUFDakYsT0FBTyxFQUFhLGNBQWMsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUl6RCxNQUFNLE9BQU8sY0FBYzs7Ozs7SUFJdkIsWUFDSSxlQUFpQyxFQUN6QixZQUE0QjtRQUE1QixpQkFBWSxHQUFaLFlBQVksQ0FBZ0I7UUFDcEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxlQUFlLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMvRCxDQUFDOzs7OztJQUVNLE9BQU8sQ0FBQyxZQUEyQjtRQUN0QyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzdCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQzNEO1lBQ0ksWUFBWTtZQUNaLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFFBQVEsRUFBRSxLQUFLO1lBQ2YsS0FBSyxFQUFFLHVCQUF1QjtTQUNqQyxDQUFDLENBQUM7UUFDUCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3hFLE9BQU8sbUJBQUEsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUF1QixDQUFDO0lBQ2xFLENBQUM7Ozs7OztJQUVNLFNBQVMsQ0FBQyxLQUFhLEVBQUUsSUFBSSxHQUFHLG9CQUFvQjs7Y0FDakQsWUFBWSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDO1FBQ3BELElBQUksU0FBUyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDdkMsWUFBWSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsSUFBSSxHQUFHLEdBQUcsR0FBRyxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUM7U0FDakU7YUFBTTtZQUNILFlBQVksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLElBQUksR0FBRyxHQUFHLEdBQUcsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDO1NBQ2pFO1FBRUQsWUFBWSxDQUFDLGdCQUFnQixDQUFDLE1BQU07OztRQUFFLEdBQUcsRUFBRTtZQUN2QyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEIsQ0FBQyxHQUFFLElBQUksQ0FBQyxDQUFDO1FBQ1QsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JCLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN4QixDQUFDOzs7WUFyQ0osVUFBVTs7OztZQUxvQixnQkFBZ0I7WUFFM0IsY0FBYzs7OztJQUs5QixvQ0FBdUI7O0lBQ3ZCLGtDQUFvQjs7Ozs7SUFJaEIsc0NBQW9DOzs7OztBQWtDNUMsbUNBSUM7OztJQUhHLDhCQUFjOztJQUNkLGdDQUFnQjs7SUFDaEIsZ0NBQXVCOzs7OztBQUczQixrQ0FHQzs7O0lBRkcsNkJBQWM7O0lBQ2QsaUNBQWtCOzs7OztBQUd0QixtQ0FHQzs7O0lBRkcsZ0NBQXVCOztJQUN2QiwrQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGUsIFJlbmRlcmVyMiwgUmVuZGVyZXJGYWN0b3J5Mn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7Q29uZmlybURpYWxvZ0NvbXBvbmVudH0gZnJvbSAnLi9jb25maXJtLWRpYWxvZy9jb25maXJtLWRpYWxvZy5jb21wb25lbnQnO1xyXG5pbXBvcnQge0JzTW9kYWxSZWYsIEJzTW9kYWxTZXJ2aWNlfSBmcm9tICduZ3gtYm9vdHN0cmFwJztcclxuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tICdyeGpzJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIERpYWxvZ3NTZXJ2aWNlIHtcclxuICAgIGJzTW9kYWxSZWY6IEJzTW9kYWxSZWY7XHJcbiAgICByZW5kZXJlcjogUmVuZGVyZXIyO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHJlbmRlcmVyRmFjdG9yeTogUmVuZGVyZXJGYWN0b3J5MixcclxuICAgICAgICBwcml2YXRlIG1vZGFsU2VydmljZTogQnNNb2RhbFNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLnJlbmRlcmVyID0gcmVuZGVyZXJGYWN0b3J5LmNyZWF0ZVJlbmRlcmVyKG51bGwsIG51bGwpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBjb25maXJtKGluaXRpYWxTdGF0ZTogRGlhbG9nT3B0aW9ucykge1xyXG4gICAgICAgIHRoaXMucGxheVNvdW5kKCdtZXNzYWdlYm94Jyk7XHJcbiAgICAgICAgdGhpcy5ic01vZGFsUmVmID0gdGhpcy5tb2RhbFNlcnZpY2Uuc2hvdyhDb25maXJtRGlhbG9nQ29tcG9uZW50LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBpbml0aWFsU3RhdGUsXHJcbiAgICAgICAgICAgICAgICBiYWNrZHJvcDogJ3N0YXRpYycsXHJcbiAgICAgICAgICAgICAgICBrZXlib2FyZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBjbGFzczogJ21vZGFsLWRpYWxvZy1jZW50ZXJlZCdcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyhkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubW9kYWwnKSwgJ21vZGFsLWFsZXJ0Jyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYnNNb2RhbFJlZi5jb250ZW50Lm9uQ2xvc2UgYXMgT2JzZXJ2YWJsZTxib29sZWFuPjtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcGxheVNvdW5kKHNvdW5kOiBzdHJpbmcsIHBhdGggPSAnYXNzZXRzL21lZGlhL3NvdW5kJykge1xyXG4gICAgICAgIGNvbnN0IGF1ZGlvRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2F1ZGlvJyk7XHJcbiAgICAgICAgaWYgKG5hdmlnYXRvci51c2VyQWdlbnQubWF0Y2goJ0ZpcmVmb3gvJykpIHtcclxuICAgICAgICAgICAgYXVkaW9FbGVtZW50LnNldEF0dHJpYnV0ZSgnc3JjJywgcGF0aCArICcvJyArIHNvdW5kICsgJy5vZ2cnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBhdWRpb0VsZW1lbnQuc2V0QXR0cmlidXRlKCdzcmMnLCBwYXRoICsgJy8nICsgc291bmQgKyAnLm1wMycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYXVkaW9FbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIGF1ZGlvRWxlbWVudC5wbGF5KCk7XHJcbiAgICAgICAgfSwgdHJ1ZSk7XHJcbiAgICAgICAgYXVkaW9FbGVtZW50LnBhdXNlKCk7XHJcbiAgICAgICAgYXVkaW9FbGVtZW50LnBsYXkoKTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRGlhbG9nT3B0aW9ucyB7XHJcbiAgICB0aXRsZTogc3RyaW5nO1xyXG4gICAgbWVzc2FnZTogc3RyaW5nO1xyXG4gICAgYnV0dG9uczogRGlhbG9nQnV0dG9ucztcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBEaWFsb2dCdXR0b24ge1xyXG4gICAgbGFiZWw6IHN0cmluZztcclxuICAgIGNsYXNzTmFtZTogc3RyaW5nO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIERpYWxvZ0J1dHRvbnMge1xyXG4gICAgY29uZmlybT86IERpYWxvZ0J1dHRvbjtcclxuICAgIGNhbmNlbD86IERpYWxvZ0J1dHRvbjtcclxufVxyXG4iXX0=