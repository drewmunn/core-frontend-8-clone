/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/utils.functions.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} scrollDuration
 * @return {?}
 */
export function scrollToTop(scrollDuration) {
    /** @type {?} */
    const cosParameter = window.scrollY / 2;
    /** @type {?} */
    let scrollCount = 0;
    /** @type {?} */
    let oldTimestamp = performance.now();
    /**
     * @param {?} newTimestamp
     * @return {?}
     */
    function step(newTimestamp) {
        scrollCount += Math.PI / (scrollDuration / (newTimestamp - oldTimestamp));
        if (scrollCount >= Math.PI) {
            window.scrollTo(0, 0);
        }
        if (window.scrollY === 0) {
            return;
        }
        window.scrollTo(0, Math.round(cosParameter + cosParameter * Math.cos(scrollCount)));
        oldTimestamp = newTimestamp;
        window.requestAnimationFrame(step);
    }
    window.requestAnimationFrame(step);
}
/* tslint:disable */
/**
 * @return {?}
 */
export function toggleFullscreen() {
    if (!document.fullscreenElement /* Standard browsers */
        && !document['msFullscreenElement'] /* Internet Explorer */
        && !document['mozFullScreenElement'] /* Firefox */
        && !document['webkitFullscreenElement'] /* Chrome */) {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        }
        else if (document.documentElement['msRequestFullscreen']) {
            document.documentElement['msRequestFullscreen']();
        }
        else if (document.documentElement['mozRequestFullScreen']) {
            document.documentElement['mozRequestFullScreen']();
        }
        else if (document.documentElement['webkitRequestFullscreen']) {
            document.documentElement['webkitRequestFullscreen'](Element['ALLOW_KEYBOARD_INPUT']);
        }
    }
    else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document['msExitFullscreen']) {
            document['msExitFullscreen']();
        }
        else if (document['mozCancelFullScreen']) {
            document['mozCancelFullScreen']();
        }
        else if (document['webkitExitFullscreen']) {
            document['webkitExitFullscreen']();
        }
    }
}
/* tslint:enable */
// conditionaly apply css class to target
/**
 * @param {?} condition
 * @param {?} className
 * @param {?} el
 * @return {?}
 */
export function handleClassCondition(condition, className, el) {
    if (!condition && el.classList.contains(className)) {
        el.classList.remove(className);
    }
    if (condition && !el.classList.contains(className)) {
        el.classList.add(className);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuZnVuY3Rpb25zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy91dGlscy5mdW5jdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsTUFBTSxVQUFVLFdBQVcsQ0FBQyxjQUFzQjs7VUFDeEMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxPQUFPLEdBQUcsQ0FBQzs7UUFDbkMsV0FBVyxHQUFHLENBQUM7O1FBQ2YsWUFBWSxHQUFHLFdBQVcsQ0FBQyxHQUFHLEVBQUU7Ozs7O0lBRXBDLFNBQVMsSUFBSSxDQUFDLFlBQVk7UUFDdEIsV0FBVyxJQUFJLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUMxRSxJQUFJLFdBQVcsSUFBSSxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ3hCLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3pCO1FBQ0QsSUFBSSxNQUFNLENBQUMsT0FBTyxLQUFLLENBQUMsRUFBRTtZQUN0QixPQUFPO1NBQ1Y7UUFDRCxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxZQUFZLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEYsWUFBWSxHQUFHLFlBQVksQ0FBQztRQUM1QixNQUFNLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN2QyxDQUFDOzs7OztBQUlELE1BQU0sVUFBVSxnQkFBZ0I7SUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBYSx1QkFBdUI7V0FDNUQsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBTyx1QkFBdUI7V0FDOUQsQ0FBQyxRQUFRLENBQUMsc0JBQXNCLENBQUMsQ0FBTSxhQUFhO1dBQ3BELENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDLENBQUcsWUFBWSxFQUN4RDtRQUNFLElBQUksUUFBUSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsRUFBRTtZQUM1QyxRQUFRLENBQUMsZUFBZSxDQUFDLGlCQUFpQixFQUFFLENBQUM7U0FDaEQ7YUFBTSxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsRUFBRTtZQUN4RCxRQUFRLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQztTQUNyRDthQUFNLElBQUksUUFBUSxDQUFDLGVBQWUsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFO1lBQ3pELFFBQVEsQ0FBQyxlQUFlLENBQUMsc0JBQXNCLENBQUMsRUFBRSxDQUFDO1NBQ3REO2FBQU0sSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLHlCQUF5QixDQUFDLEVBQUU7WUFDNUQsUUFBUSxDQUFDLGVBQWUsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7U0FDeEY7S0FDSjtTQUFNO1FBQ0gsSUFBSSxRQUFRLENBQUMsY0FBYyxFQUFFO1lBQ3pCLFFBQVEsQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUM3QjthQUFNLElBQUksUUFBUSxDQUFDLGtCQUFrQixDQUFDLEVBQUU7WUFDckMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsQ0FBQztTQUNsQzthQUFNLElBQUksUUFBUSxDQUFDLHFCQUFxQixDQUFDLEVBQUU7WUFDeEMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQztTQUNyQzthQUFNLElBQUksUUFBUSxDQUFDLHNCQUFzQixDQUFDLEVBQUU7WUFDekMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLEVBQUUsQ0FBQztTQUN0QztLQUNKO0FBQ0wsQ0FBQzs7Ozs7Ozs7O0FBTUQsTUFBTSxVQUFVLG9CQUFvQixDQUNoQyxTQUFrQixFQUNsQixTQUFpQixFQUNqQixFQUFlO0lBRWYsSUFBSSxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRTtRQUNoRCxFQUFFLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUNsQztJQUNELElBQUksU0FBUyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUU7UUFDaEQsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7S0FDL0I7QUFDTCxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIHNjcm9sbFRvVG9wKHNjcm9sbER1cmF0aW9uOiBudW1iZXIpIHtcclxuICAgIGNvbnN0IGNvc1BhcmFtZXRlciA9IHdpbmRvdy5zY3JvbGxZIC8gMjtcclxuICAgIGxldCBzY3JvbGxDb3VudCA9IDA7XHJcbiAgICBsZXQgb2xkVGltZXN0YW1wID0gcGVyZm9ybWFuY2Uubm93KCk7XHJcblxyXG4gICAgZnVuY3Rpb24gc3RlcChuZXdUaW1lc3RhbXApIHtcclxuICAgICAgICBzY3JvbGxDb3VudCArPSBNYXRoLlBJIC8gKHNjcm9sbER1cmF0aW9uIC8gKG5ld1RpbWVzdGFtcCAtIG9sZFRpbWVzdGFtcCkpO1xyXG4gICAgICAgIGlmIChzY3JvbGxDb3VudCA+PSBNYXRoLlBJKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5zY3JvbGxUbygwLCAwKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHdpbmRvdy5zY3JvbGxZID09PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgd2luZG93LnNjcm9sbFRvKDAsIE1hdGgucm91bmQoY29zUGFyYW1ldGVyICsgY29zUGFyYW1ldGVyICogTWF0aC5jb3Moc2Nyb2xsQ291bnQpKSk7XHJcbiAgICAgICAgb2xkVGltZXN0YW1wID0gbmV3VGltZXN0YW1wO1xyXG4gICAgICAgIHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoc3RlcCk7XHJcbiAgICB9XHJcblxyXG4gICAgd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZShzdGVwKTtcclxufVxyXG5cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlICovXHJcbmV4cG9ydCBmdW5jdGlvbiB0b2dnbGVGdWxsc2NyZWVuKCkge1xyXG4gICAgaWYgKCFkb2N1bWVudC5mdWxsc2NyZWVuRWxlbWVudCAgICAgICAgICAgICAvKiBTdGFuZGFyZCBicm93c2VycyAqL1xyXG4gICAgICAgICYmICFkb2N1bWVudFsnbXNGdWxsc2NyZWVuRWxlbWVudCddICAgICAgIC8qIEludGVybmV0IEV4cGxvcmVyICovXHJcbiAgICAgICAgJiYgIWRvY3VtZW50Wydtb3pGdWxsU2NyZWVuRWxlbWVudCddICAgICAgLyogRmlyZWZveCAqL1xyXG4gICAgICAgICYmICFkb2N1bWVudFsnd2Via2l0RnVsbHNjcmVlbkVsZW1lbnQnXSAgIC8qIENocm9tZSAqL1xyXG4gICAgKSB7XHJcbiAgICAgICAgaWYgKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5yZXF1ZXN0RnVsbHNjcmVlbikge1xyXG4gICAgICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQucmVxdWVzdEZ1bGxzY3JlZW4oKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudFsnbXNSZXF1ZXN0RnVsbHNjcmVlbiddKSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudFsnbXNSZXF1ZXN0RnVsbHNjcmVlbiddKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChkb2N1bWVudC5kb2N1bWVudEVsZW1lbnRbJ21velJlcXVlc3RGdWxsU2NyZWVuJ10pIHtcclxuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50Wydtb3pSZXF1ZXN0RnVsbFNjcmVlbiddKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChkb2N1bWVudC5kb2N1bWVudEVsZW1lbnRbJ3dlYmtpdFJlcXVlc3RGdWxsc2NyZWVuJ10pIHtcclxuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50Wyd3ZWJraXRSZXF1ZXN0RnVsbHNjcmVlbiddKEVsZW1lbnRbJ0FMTE9XX0tFWUJPQVJEX0lOUFVUJ10pO1xyXG4gICAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKGRvY3VtZW50LmV4aXRGdWxsc2NyZWVuKSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmV4aXRGdWxsc2NyZWVuKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChkb2N1bWVudFsnbXNFeGl0RnVsbHNjcmVlbiddKSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50Wydtc0V4aXRGdWxsc2NyZWVuJ10oKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGRvY3VtZW50Wydtb3pDYW5jZWxGdWxsU2NyZWVuJ10pIHtcclxuICAgICAgICAgICAgZG9jdW1lbnRbJ21vekNhbmNlbEZ1bGxTY3JlZW4nXSgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZG9jdW1lbnRbJ3dlYmtpdEV4aXRGdWxsc2NyZWVuJ10pIHtcclxuICAgICAgICAgICAgZG9jdW1lbnRbJ3dlYmtpdEV4aXRGdWxsc2NyZWVuJ10oKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qIHRzbGludDplbmFibGUgKi9cclxuXHJcblxyXG4vLyBjb25kaXRpb25hbHkgYXBwbHkgY3NzIGNsYXNzIHRvIHRhcmdldFxyXG5leHBvcnQgZnVuY3Rpb24gaGFuZGxlQ2xhc3NDb25kaXRpb24oXHJcbiAgICBjb25kaXRpb246IGJvb2xlYW4sXHJcbiAgICBjbGFzc05hbWU6IHN0cmluZyxcclxuICAgIGVsOiBIVE1MRWxlbWVudFxyXG4pIHtcclxuICAgIGlmICghY29uZGl0aW9uICYmIGVsLmNsYXNzTGlzdC5jb250YWlucyhjbGFzc05hbWUpKSB7XHJcbiAgICAgICAgZWwuY2xhc3NMaXN0LnJlbW92ZShjbGFzc05hbWUpO1xyXG4gICAgfVxyXG4gICAgaWYgKGNvbmRpdGlvbiAmJiAhZWwuY2xhc3NMaXN0LmNvbnRhaW5zKGNsYXNzTmFtZSkpIHtcclxuICAgICAgICBlbC5jbGFzc0xpc3QuYWRkKGNsYXNzTmFtZSk7XHJcbiAgICB9XHJcbn1cclxuIl19