/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/panels/panels.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './panel/panel.component';
import { TooltipModule } from 'ngx-bootstrap';
import { DialogsModule } from '../dialogs/dialogs.module';
export class PanelsModule {
}
PanelsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [PanelComponent],
                imports: [
                    TooltipModule,
                    CommonModule,
                    DialogsModule
                ],
                exports: [PanelComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWxzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvdXRpbHMvcGFuZWxzL3BhbmVscy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFXMUQsTUFBTSxPQUFPLFlBQVk7OztZQVR4QixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsY0FBYyxDQUFDO2dCQUM5QixPQUFPLEVBQUU7b0JBQ1AsYUFBYTtvQkFDYixZQUFZO29CQUNaLGFBQWE7aUJBQ2Q7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsY0FBYyxDQUFDO2FBQzFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgUGFuZWxDb21wb25lbnQgfSBmcm9tICcuL3BhbmVsL3BhbmVsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFRvb2x0aXBNb2R1bGUgfSBmcm9tICduZ3gtYm9vdHN0cmFwJztcclxuaW1wb3J0IHsgRGlhbG9nc01vZHVsZSB9IGZyb20gJy4uL2RpYWxvZ3MvZGlhbG9ncy5tb2R1bGUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtQYW5lbENvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgVG9vbHRpcE1vZHVsZSxcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIERpYWxvZ3NNb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtQYW5lbENvbXBvbmVudF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFBhbmVsc01vZHVsZSB7IH1cclxuIl19