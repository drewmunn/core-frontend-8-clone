/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/panels/panel/panel.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Attribute, Component, ContentChild, ElementRef, Input, Renderer2 } from '@angular/core';
import { makeSlideInOut } from '../../animations';
import { handleClassCondition } from '../../utils.functions';
import { DialogsService } from '../../dialogs/dialogs.service';
export class PanelComponent {
    /**
     * @param {?} headerClass
     * @param {?} dialogs
     * @param {?} el
     * @param {?} renderer
     */
    constructor(headerClass, dialogs, el, renderer) {
        this.dialogs = dialogs;
        this.el = el;
        this.renderer = renderer;
        this.hasPannel = false;
        this.collapsible = false;
        this.collapsed = false;
        this.fullscreenable = false;
        this.headerClass = headerClass;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (typeof this.collapsed !== 'undefined') {
            this.collapsible = true;
        }
        if (typeof this.fullscreenIn !== 'undefined') {
            this.fullscreenable = true;
        }
        if (typeof this.closed !== 'undefined') {
            this.clossable = true;
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        // console.log(22, changes);
        // if (typeof changes.fullscreenIn !== 'undefined') {
        //   console.log('111', changes.fullscreenIn.currentValue, this.fullscreenable);
        // }
    }
    /**
     * @return {?}
     */
    get pannelClasses() {
        /** @type {?} */
        const classes = ['panel'];
        classes.push(this.collapsed ? 'panel-collapsed' : '');
        classes.push(this.fullscreenIn ? 'panel-fullscreen' : '');
        return classes;
    }
    /**
     * @return {?}
     */
    get pannelContainerClasses() {
        /** @type {?} */
        const classes = ['panel-container'];
        if (this.collapsible) {
            // classes.push(this.collapsed ? 'collapse' : 'show');
            classes.push(this.collapsed ? '' : 'show');
        }
        return classes;
    }
    /**
     * @return {?}
     */
    get pannelContentClasses() {
        /** @type {?} */
        const classes = ['panel-content'];
        return classes;
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    toggleCollapse($event) {
        $event.preventDefault();
        this.collapsed = !this.collapsed;
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    toggleFullscreen($event) {
        $event.preventDefault();
        this.fullscreenIn = !this.fullscreenIn;
        handleClassCondition(this.fullscreenIn, 'panel-fullscreen', document.querySelector('body'));
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    closePanel($event) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            $event.preventDefault();
            /** @type {?} */
            const titleEl = this.el.nativeElement.querySelector('h1')
                || this.el.nativeElement.querySelector('h2')
                || this.el.nativeElement.querySelector('h3');
            /** @type {?} */
            const title = titleEl ? titleEl.innerText : '';
            /** @type {?} */
            const result = yield this.dialogs.confirm({
                title: `<i class='fal fa-times-circle text-danger mr-2'></i>
      Do you wish to delete panel <span class='fw-500'>&nbsp;'${title}'&nbsp;</span>?`,
                message: `<span><strong>Warning:</strong> This action cannot be undone!</span>`,
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-danger shadow-0'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-default'
                    }
                }
            }).toPromise();
            if (result) {
                this.renderer.addClass(this.el.nativeElement, 'd-none');
            }
        });
    }
    /**
     * @param {?} headerClass
     * @return {?}
     */
    setHeaderClass(headerClass) {
        this.headerClass = headerClass;
    }
}
PanelComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-panel',
                template: "<div class=\"panel\" [ngClass]=\"pannelClasses\">\r\n  <div class=\"panel-hdr {{ headerClass }}\">\r\n    <ng-content select=\"[panelTitle]\" #panelTitle></ng-content>\r\n    <ng-content select=\"[panelToolbar]\"> </ng-content>\r\n    <div class=\"panel-toolbar\">\r\n      <button\r\n        *ngIf=\"collapsible\"\r\n        class=\"btn btn-panel\"\r\n        data-action=\"panel-collapse\"\r\n        (click)=\"toggleCollapse($event)\"\r\n        tooltip=\"Collapse\"\r\n      ></button>\r\n      <button\r\n        class=\"btn btn-panel\"\r\n        *ngIf=\"fullscreenable\"\r\n        data-action=\"panel-fullscreen\"\r\n        (click)=\"toggleFullscreen($event)\"\r\n        tooltip=\"Fullscreen\"\r\n      ></button>\r\n      <button\r\n        class=\"btn btn-panel\"\r\n        *ngIf=\"clossable\"\r\n        data-action=\"panel-close\"\r\n        (click)=\"closePanel($event)\"\r\n        tooltip=\"Close\"\r\n      ></button>      \r\n    </div>    \r\n  </div>\r\n\r\n  <div class=\"panel-container\" [ngClass]=\"pannelContainerClasses\">\r\n    <div\r\n      class=\"panel-content\"\r\n      [ngClass]=\"pannelContentClasses\"\r\n      *ngIf=\"!collapsed\"\r\n      [@slideInOut]\r\n    >\r\n      <ng-content select=\"[panelContent]\"> </ng-content>\r\n    </div>\r\n\r\n    <ng-content select=\"[panelFooter]\"> </ng-content>\r\n  </div>\r\n</div>\r\n",
                animations: [makeSlideInOut()]
            }] }
];
/** @nocollapse */
PanelComponent.ctorParameters = () => [
    { type: String, decorators: [{ type: Attribute, args: ['headerClass',] }] },
    { type: DialogsService },
    { type: ElementRef },
    { type: Renderer2 }
];
PanelComponent.propDecorators = {
    collapsible: [{ type: Input }],
    collapsed: [{ type: Input }],
    fullscreenable: [{ type: Input }],
    fullscreenIn: [{ type: Input }],
    clossable: [{ type: Input }],
    closed: [{ type: Input }],
    panelTitle: [{ type: ContentChild, args: ['panelTitle', { static: true },] }]
};
if (false) {
    /** @type {?} */
    PanelComponent.prototype.hasPannel;
    /** @type {?} */
    PanelComponent.prototype.collapsible;
    /** @type {?} */
    PanelComponent.prototype.collapsed;
    /** @type {?} */
    PanelComponent.prototype.fullscreenable;
    /** @type {?} */
    PanelComponent.prototype.fullscreenIn;
    /** @type {?} */
    PanelComponent.prototype.clossable;
    /** @type {?} */
    PanelComponent.prototype.closed;
    /** @type {?} */
    PanelComponent.prototype.headerClass;
    /** @type {?} */
    PanelComponent.prototype.panelTitle;
    /**
     * @type {?}
     * @private
     */
    PanelComponent.prototype.dialogs;
    /**
     * @type {?}
     * @private
     */
    PanelComponent.prototype.el;
    /**
     * @type {?}
     * @private
     */
    PanelComponent.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy9wYW5lbHMvcGFuZWwvcGFuZWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFDSCxTQUFTLEVBQ1QsU0FBUyxFQUNULFlBQVksRUFDWixVQUFVLEVBQ1YsS0FBSyxFQUdMLFNBQVMsRUFFWixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDaEQsT0FBTyxFQUFDLG9CQUFvQixFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDM0QsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLCtCQUErQixDQUFDO0FBTzdELE1BQU0sT0FBTyxjQUFjOzs7Ozs7O0lBYXZCLFlBQzhCLFdBQW1CLEVBQ3JDLE9BQXVCLEVBQ3ZCLEVBQWMsRUFDZCxRQUFtQjtRQUZuQixZQUFPLEdBQVAsT0FBTyxDQUFnQjtRQUN2QixPQUFFLEdBQUYsRUFBRSxDQUFZO1FBQ2QsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQWYvQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ1QsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixtQkFBYyxHQUFHLEtBQUssQ0FBQztRQWM1QixJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztJQUNuQyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksT0FBTyxJQUFJLENBQUMsU0FBUyxLQUFLLFdBQVcsRUFBRTtZQUN2QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztTQUMzQjtRQUNELElBQUksT0FBTyxJQUFJLENBQUMsWUFBWSxLQUFLLFdBQVcsRUFBRTtZQUMxQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM5QjtRQUNELElBQUksT0FBTyxJQUFJLENBQUMsTUFBTSxLQUFLLFdBQVcsRUFBRTtZQUNwQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztTQUN6QjtJQUNMLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQzlCLDRCQUE0QjtRQUM1QixxREFBcUQ7UUFDckQsZ0ZBQWdGO1FBQ2hGLElBQUk7SUFDUixDQUFDOzs7O0lBRUQsSUFBSSxhQUFhOztjQUNQLE9BQU8sR0FBRyxDQUFDLE9BQU8sQ0FBQztRQUN6QixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN0RCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMxRCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDOzs7O0lBRUQsSUFBSSxzQkFBc0I7O2NBQ2hCLE9BQU8sR0FBRyxDQUFDLGlCQUFpQixDQUFDO1FBQ25DLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNsQixzREFBc0Q7WUFDdEQsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzlDO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQzs7OztJQUVELElBQUksb0JBQW9COztjQUNkLE9BQU8sR0FBRyxDQUFDLGVBQWUsQ0FBQztRQUNqQyxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxNQUFrQjtRQUM3QixNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDckMsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxNQUFrQjtRQUMvQixNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDdkMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxrQkFBa0IsRUFBRSxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDaEcsQ0FBQzs7Ozs7SUFFSyxVQUFVLENBQUMsTUFBa0I7O1lBQy9CLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQzs7a0JBQ2xCLE9BQU8sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO21CQUNsRCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO21CQUN6QyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDOztrQkFFMUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRTs7a0JBRXhDLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO2dCQUN0QyxLQUFLLEVBQUU7Z0VBQzZDLEtBQUssaUJBQWlCO2dCQUMxRSxPQUFPLEVBQUUsc0VBQXNFO2dCQUMvRSxPQUFPLEVBQUU7b0JBQ0wsT0FBTyxFQUFFO3dCQUNMLEtBQUssRUFBRSxLQUFLO3dCQUNaLFNBQVMsRUFBRSxxQkFBcUI7cUJBQ25DO29CQUNELE1BQU0sRUFBRTt3QkFDSixLQUFLLEVBQUUsSUFBSTt3QkFDWCxTQUFTLEVBQUUsYUFBYTtxQkFDM0I7aUJBQ0o7YUFDSixDQUFDLENBQUMsU0FBUyxFQUFFO1lBRWQsSUFBSSxNQUFNLEVBQUU7Z0JBQ1IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsUUFBUSxDQUFDLENBQUM7YUFDM0Q7UUFFTCxDQUFDO0tBQUE7Ozs7O0lBRUQsY0FBYyxDQUFDLFdBQW1CO1FBQzlCLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO0lBQ25DLENBQUM7OztZQTlHSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLGsyQ0FBcUM7Z0JBQ3JDLFVBQVUsRUFBRSxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ2pDOzs7O3lDQWVRLFNBQVMsU0FBQyxhQUFhO1lBckJ4QixjQUFjO1lBVGxCLFVBQVU7WUFJVixTQUFTOzs7MEJBZVIsS0FBSzt3QkFDTCxLQUFLOzZCQUNMLEtBQUs7MkJBQ0wsS0FBSzt3QkFDTCxLQUFLO3FCQUNMLEtBQUs7eUJBR0wsWUFBWSxTQUFDLFlBQVksRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7Ozs7SUFUMUMsbUNBQWtCOztJQUNsQixxQ0FBNkI7O0lBQzdCLG1DQUEyQjs7SUFDM0Isd0NBQWdDOztJQUNoQyxzQ0FBK0I7O0lBQy9CLG1DQUE0Qjs7SUFDNUIsZ0NBQXlCOztJQUN6QixxQ0FBb0I7O0lBRXBCLG9DQUF1RDs7Ozs7SUFJbkQsaUNBQStCOzs7OztJQUMvQiw0QkFBc0I7Ozs7O0lBQ3RCLGtDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBBdHRyaWJ1dGUsXHJcbiAgICBDb21wb25lbnQsXHJcbiAgICBDb250ZW50Q2hpbGQsXHJcbiAgICBFbGVtZW50UmVmLFxyXG4gICAgSW5wdXQsXHJcbiAgICBPbkNoYW5nZXMsXHJcbiAgICBPbkluaXQsXHJcbiAgICBSZW5kZXJlcjIsXHJcbiAgICBTaW1wbGVDaGFuZ2VzXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7bWFrZVNsaWRlSW5PdXR9IGZyb20gJy4uLy4uL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQge2hhbmRsZUNsYXNzQ29uZGl0aW9ufSBmcm9tICcuLi8uLi91dGlscy5mdW5jdGlvbnMnO1xyXG5pbXBvcnQge0RpYWxvZ3NTZXJ2aWNlfSBmcm9tICcuLi8uLi9kaWFsb2dzL2RpYWxvZ3Muc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY29yZS1wYW5lbCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vcGFuZWwuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgYW5pbWF0aW9uczogW21ha2VTbGlkZUluT3V0KCldLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGFuZWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgaGFzUGFubmVsID0gZmFsc2U7XHJcbiAgICBASW5wdXQoKSBjb2xsYXBzaWJsZSA9IGZhbHNlO1xyXG4gICAgQElucHV0KCkgY29sbGFwc2VkID0gZmFsc2U7XHJcbiAgICBASW5wdXQoKSBmdWxsc2NyZWVuYWJsZSA9IGZhbHNlO1xyXG4gICAgQElucHV0KCkgZnVsbHNjcmVlbkluOiBib29sZWFuO1xyXG4gICAgQElucHV0KCkgY2xvc3NhYmxlOiBib29sZWFuO1xyXG4gICAgQElucHV0KCkgY2xvc2VkOiBib29sZWFuO1xyXG4gICAgaGVhZGVyQ2xhc3M6IHN0cmluZztcclxuXHJcbiAgICBAQ29udGVudENoaWxkKCdwYW5lbFRpdGxlJywge3N0YXRpYzogdHJ1ZX0pIHBhbmVsVGl0bGU7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgQEF0dHJpYnV0ZSgnaGVhZGVyQ2xhc3MnKSBoZWFkZXJDbGFzczogc3RyaW5nLFxyXG4gICAgICAgIHByaXZhdGUgZGlhbG9nczogRGlhbG9nc1NlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBlbDogRWxlbWVudFJlZixcclxuICAgICAgICBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsXHJcbiAgICApIHtcclxuICAgICAgICB0aGlzLmhlYWRlckNsYXNzID0gaGVhZGVyQ2xhc3M7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiB0aGlzLmNvbGxhcHNlZCAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgdGhpcy5jb2xsYXBzaWJsZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0eXBlb2YgdGhpcy5mdWxsc2NyZWVuSW4gIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZnVsbHNjcmVlbmFibGUgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodHlwZW9mIHRoaXMuY2xvc2VkICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICB0aGlzLmNsb3NzYWJsZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygyMiwgY2hhbmdlcyk7XHJcbiAgICAgICAgLy8gaWYgKHR5cGVvZiBjaGFuZ2VzLmZ1bGxzY3JlZW5JbiAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAvLyAgIGNvbnNvbGUubG9nKCcxMTEnLCBjaGFuZ2VzLmZ1bGxzY3JlZW5Jbi5jdXJyZW50VmFsdWUsIHRoaXMuZnVsbHNjcmVlbmFibGUpO1xyXG4gICAgICAgIC8vIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXQgcGFubmVsQ2xhc3NlcygpIHtcclxuICAgICAgICBjb25zdCBjbGFzc2VzID0gWydwYW5lbCddO1xyXG4gICAgICAgIGNsYXNzZXMucHVzaCh0aGlzLmNvbGxhcHNlZCA/ICdwYW5lbC1jb2xsYXBzZWQnIDogJycpO1xyXG4gICAgICAgIGNsYXNzZXMucHVzaCh0aGlzLmZ1bGxzY3JlZW5JbiA/ICdwYW5lbC1mdWxsc2NyZWVuJyA6ICcnKTtcclxuICAgICAgICByZXR1cm4gY2xhc3NlcztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcGFubmVsQ29udGFpbmVyQ2xhc3NlcygpIHtcclxuICAgICAgICBjb25zdCBjbGFzc2VzID0gWydwYW5lbC1jb250YWluZXInXTtcclxuICAgICAgICBpZiAodGhpcy5jb2xsYXBzaWJsZSkge1xyXG4gICAgICAgICAgICAvLyBjbGFzc2VzLnB1c2godGhpcy5jb2xsYXBzZWQgPyAnY29sbGFwc2UnIDogJ3Nob3cnKTtcclxuICAgICAgICAgICAgY2xhc3Nlcy5wdXNoKHRoaXMuY29sbGFwc2VkID8gJycgOiAnc2hvdycpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gY2xhc3NlcztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcGFubmVsQ29udGVudENsYXNzZXMoKSB7XHJcbiAgICAgICAgY29uc3QgY2xhc3NlcyA9IFsncGFuZWwtY29udGVudCddO1xyXG4gICAgICAgIHJldHVybiBjbGFzc2VzO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZUNvbGxhcHNlKCRldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHRoaXMuY29sbGFwc2VkID0gIXRoaXMuY29sbGFwc2VkO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZUZ1bGxzY3JlZW4oJGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgdGhpcy5mdWxsc2NyZWVuSW4gPSAhdGhpcy5mdWxsc2NyZWVuSW47XHJcbiAgICAgICAgaGFuZGxlQ2xhc3NDb25kaXRpb24odGhpcy5mdWxsc2NyZWVuSW4sICdwYW5lbC1mdWxsc2NyZWVuJywgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignYm9keScpKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBjbG9zZVBhbmVsKCRldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGNvbnN0IHRpdGxlRWwgPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaDEnKVxyXG4gICAgICAgICAgICB8fCB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaDInKVxyXG4gICAgICAgICAgICB8fCB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaDMnKTtcclxuXHJcbiAgICAgICAgY29uc3QgdGl0bGUgPSB0aXRsZUVsID8gdGl0bGVFbC5pbm5lclRleHQgOiAnJztcclxuXHJcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgdGhpcy5kaWFsb2dzLmNvbmZpcm0oe1xyXG4gICAgICAgICAgICB0aXRsZTogYDxpIGNsYXNzPSdmYWwgZmEtdGltZXMtY2lyY2xlIHRleHQtZGFuZ2VyIG1yLTInPjwvaT5cclxuICAgICAgRG8geW91IHdpc2ggdG8gZGVsZXRlIHBhbmVsIDxzcGFuIGNsYXNzPSdmdy01MDAnPiZuYnNwOycke3RpdGxlfScmbmJzcDs8L3NwYW4+P2AsXHJcbiAgICAgICAgICAgIG1lc3NhZ2U6IGA8c3Bhbj48c3Ryb25nPldhcm5pbmc6PC9zdHJvbmc+IFRoaXMgYWN0aW9uIGNhbm5vdCBiZSB1bmRvbmUhPC9zcGFuPmAsXHJcbiAgICAgICAgICAgIGJ1dHRvbnM6IHtcclxuICAgICAgICAgICAgICAgIGNvbmZpcm06IHtcclxuICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ1llcycsXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnYnRuLWRhbmdlciBzaGFkb3ctMCdcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBjYW5jZWw6IHtcclxuICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ05vJyxcclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdidG4tZGVmYXVsdCdcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLnRvUHJvbWlzZSgpO1xyXG5cclxuICAgICAgICBpZiAocmVzdWx0KSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5lbC5uYXRpdmVFbGVtZW50LCAnZC1ub25lJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBzZXRIZWFkZXJDbGFzcyhoZWFkZXJDbGFzczogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5oZWFkZXJDbGFzcyA9IGhlYWRlckNsYXNzO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=