/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/pipes/limitto.pipe.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class TruncatePipe {
    /**
     * @param {?} value
     * @param {?} args
     * @return {?}
     */
    transform(value, args) {
        /** @type {?} */
        const limit = args || 10;
        /** @type {?} */
        const trail = '...';
        return value.length > limit ? value.substring(0, limit) + trail : value;
    }
}
TruncatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'limitTo'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGltaXR0by5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGdjL2NvcmUtZnJvbnRlbmQvIiwic291cmNlcyI6WyJsaWIvY29yZS91dGlscy9waXBlcy9saW1pdHRvLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsSUFBSSxFQUFnQixNQUFNLGVBQWUsQ0FBQztBQU1sRCxNQUFNLE9BQU8sWUFBWTs7Ozs7O0lBRXJCLFNBQVMsQ0FBQyxLQUFhLEVBQUUsSUFBWTs7Y0FDM0IsS0FBSyxHQUFHLElBQUksSUFBSSxFQUFFOztjQUNsQixLQUFLLEdBQUcsS0FBSztRQUVuQixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUM1RSxDQUFDOzs7WUFYSixJQUFJLFNBQUM7Z0JBQ0YsSUFBSSxFQUFFLFNBQVM7YUFDbEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1BpcGUsIFBpcGVUcmFuc2Zvcm19IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gICAgbmFtZTogJ2xpbWl0VG8nXG59KVxuXG5leHBvcnQgY2xhc3MgVHJ1bmNhdGVQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgICB0cmFuc2Zvcm0odmFsdWU6IHN0cmluZywgYXJnczogbnVtYmVyKTogc3RyaW5nIHtcbiAgICAgICAgY29uc3QgbGltaXQgPSBhcmdzIHx8IDEwO1xuICAgICAgICBjb25zdCB0cmFpbCA9ICcuLi4nO1xuXG4gICAgICAgIHJldHVybiB2YWx1ZS5sZW5ndGggPiBsaW1pdCA/IHZhbHVlLnN1YnN0cmluZygwLCBsaW1pdCkgKyB0cmFpbCA6IHZhbHVlO1xuICAgIH1cblxufVxuIl19