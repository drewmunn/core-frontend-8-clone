/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/utils/ui/on-off/on-off.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
export class OnOffComponent {
    constructor() {
        this.checked = false;
        this.checkedChange = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (!changes.checked || changes.checked.currentValue === this.checked) {
            return;
        }
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onCheck($event) {
        $event.preventDefault();
        this.checked = !this.checked;
        this.checkedChange.emit(this.checked);
    }
}
OnOffComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-on-off',
                template: `
        <a
                href="#"
                (click)="onCheck($event)"
                class="btn btn-switch {{class}}"
                [class.active]="checked"></a>
    `,
                host: {
                    class: 'd-inline-block'
                },
                changeDetection: ChangeDetectionStrategy.OnPush
            }] }
];
OnOffComponent.propDecorators = {
    checked: [{ type: Input }],
    class: [{ type: Input }],
    checkedChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    OnOffComponent.prototype.checked;
    /** @type {?} */
    OnOffComponent.prototype.class;
    /** @type {?} */
    OnOffComponent.prototype.checkedChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib24tb2ZmLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2NvcmUvdXRpbHMvdWkvb24tb2ZmL29uLW9mZi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsdUJBQXVCLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQWEsTUFBTSxFQUFnQixNQUFNLGVBQWUsQ0FBQztBQWlCeEgsTUFBTSxPQUFPLGNBQWM7SUFkM0I7UUFnQmEsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUVmLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQWNqRCxDQUFDOzs7OztJQVpHLFdBQVcsQ0FBQyxPQUFzQjtRQUM5QixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLFlBQVksS0FBSyxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ25FLE9BQU87U0FDVjtJQUNMLENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLE1BQWtCO1FBQ3RCLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUU3QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7O1lBL0JKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsYUFBYTtnQkFDdkIsUUFBUSxFQUFFOzs7Ozs7S0FNVDtnQkFDRCxJQUFJLEVBQUU7b0JBQ0YsS0FBSyxFQUFFLGdCQUFnQjtpQkFDMUI7Z0JBQ0QsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07YUFDbEQ7OztzQkFHSSxLQUFLO29CQUNMLEtBQUs7NEJBQ0wsTUFBTTs7OztJQUZQLGlDQUF5Qjs7SUFDekIsK0JBQXVCOztJQUN2Qix1Q0FBNkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uQ2hhbmdlcywgT3V0cHV0LCBTaW1wbGVDaGFuZ2VzfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY29yZS1vbi1vZmYnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgICAgICAgKGNsaWNrKT1cIm9uQ2hlY2soJGV2ZW50KVwiXHJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImJ0biBidG4tc3dpdGNoIHt7Y2xhc3N9fVwiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuYWN0aXZlXT1cImNoZWNrZWRcIj48L2E+XHJcbiAgICBgLFxyXG4gICAgaG9zdDoge1xyXG4gICAgICAgIGNsYXNzOiAnZC1pbmxpbmUtYmxvY2snXHJcbiAgICB9LFxyXG4gICAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2hcclxufSlcclxuZXhwb3J0IGNsYXNzIE9uT2ZmQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuXHJcbiAgICBASW5wdXQoKSBjaGVja2VkID0gZmFsc2U7XHJcbiAgICBASW5wdXQoKSBjbGFzczogc3RyaW5nO1xyXG4gICAgQE91dHB1dCgpIGNoZWNrZWRDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgICAgIGlmICghY2hhbmdlcy5jaGVja2VkIHx8IGNoYW5nZXMuY2hlY2tlZC5jdXJyZW50VmFsdWUgPT09IHRoaXMuY2hlY2tlZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2hlY2soJGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgdGhpcy5jaGVja2VkID0gIXRoaXMuY2hlY2tlZDtcclxuXHJcbiAgICAgICAgdGhpcy5jaGVja2VkQ2hhbmdlLmVtaXQodGhpcy5jaGVja2VkKTtcclxuICAgIH1cclxufVxyXG4iXX0=