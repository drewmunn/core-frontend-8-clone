/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/router/router.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as fromRouter from '@ngrx/router-store';
/**
 * @record
 */
export function RouterStateUrl() { }
if (false) {
    /** @type {?} */
    RouterStateUrl.prototype.url;
    /** @type {?} */
    RouterStateUrl.prototype.queryParams;
    /** @type {?} */
    RouterStateUrl.prototype.params;
    /** @type {?} */
    RouterStateUrl.prototype.data;
}
/** @type {?} */
export const reducer = fromRouter.routerReducer;
export class CustomSerializer {
    /**
     * @param {?} routerState
     * @return {?}
     */
    serialize(routerState) {
        const { url } = routerState;
        const { queryParams } = routerState.root;
        /** @type {?} */
        let state = routerState.root;
        while (state.firstChild) {
            state = state.firstChild;
        }
        const { params, data } = state;
        return { url, queryParams, params, data };
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGVyLnJlZHVjZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL3JvdXRlci9yb3V0ZXIucmVkdWNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQU1BLE9BQU8sS0FBSyxVQUFVLE1BQU0sb0JBQW9CLENBQUM7Ozs7QUFFakQsb0NBS0M7OztJQUpDLDZCQUFZOztJQUNaLHFDQUFvQjs7SUFDcEIsZ0NBQWU7O0lBQ2YsOEJBQVU7OztBQUdaLE1BQU0sT0FBTyxPQUFPLEdBQUcsVUFBVSxDQUFDLGFBQWE7QUFFL0MsTUFBTSxPQUFPLGdCQUFnQjs7Ozs7SUFFcEIsU0FBUyxDQUFDLFdBQWdDO2NBQ3pDLEVBQUUsR0FBRyxFQUFFLEdBQUcsV0FBVztjQUNyQixFQUFFLFdBQVcsRUFBRSxHQUFHLFdBQVcsQ0FBQyxJQUFJOztZQUVwQyxLQUFLLEdBQTJCLFdBQVcsQ0FBQyxJQUFJO1FBQ3BELE9BQU8sS0FBSyxDQUFDLFVBQVUsRUFBRTtZQUN2QixLQUFLLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQztTQUMxQjtjQUVLLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLEtBQUs7UUFFOUIsT0FBTyxFQUFFLEdBQUcsRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO0lBQzVDLENBQUM7Q0FDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICBQYXJhbXMsXHJcbiAgUm91dGVyU3RhdGVTbmFwc2hvdCxcclxufSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0ICogYXMgZnJvbVJvdXRlciBmcm9tICdAbmdyeC9yb3V0ZXItc3RvcmUnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBSb3V0ZXJTdGF0ZVVybCB7XHJcbiAgdXJsOiBzdHJpbmc7XHJcbiAgcXVlcnlQYXJhbXM6IFBhcmFtcztcclxuICBwYXJhbXM6IFBhcmFtcztcclxuICBkYXRhOiBhbnk7XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCByZWR1Y2VyID0gZnJvbVJvdXRlci5yb3V0ZXJSZWR1Y2VyO1xyXG5cclxuZXhwb3J0IGNsYXNzIEN1c3RvbVNlcmlhbGl6ZXJcclxuICBpbXBsZW1lbnRzIGZyb21Sb3V0ZXIuUm91dGVyU3RhdGVTZXJpYWxpemVyPFJvdXRlclN0YXRlVXJsPiB7XHJcbiAgcHVibGljIHNlcmlhbGl6ZShyb3V0ZXJTdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IFJvdXRlclN0YXRlVXJsIHtcclxuICAgIGNvbnN0IHsgdXJsIH0gPSByb3V0ZXJTdGF0ZTtcclxuICAgIGNvbnN0IHsgcXVlcnlQYXJhbXMgfSA9IHJvdXRlclN0YXRlLnJvb3Q7XHJcblxyXG4gICAgbGV0IHN0YXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90ID0gcm91dGVyU3RhdGUucm9vdDtcclxuICAgIHdoaWxlIChzdGF0ZS5maXJzdENoaWxkKSB7XHJcbiAgICAgIHN0YXRlID0gc3RhdGUuZmlyc3RDaGlsZDtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCB7IHBhcmFtcywgZGF0YSB9ID0gc3RhdGU7XHJcblxyXG4gICAgcmV0dXJuIHsgdXJsLCBxdWVyeVBhcmFtcywgcGFyYW1zLCBkYXRhIH07XHJcbiAgfVxyXG59XHJcbiJdfQ==