/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/api.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, NgModule } from '@angular/core';
import { APP_CONFIG } from '../app.config';
import { ParserService } from './parser.service';
import sha1 from 'js-sha1';
import * as i0 from "@angular/core";
export class APIInterceptor {
    /**
     * @param {?} parser
     */
    constructor(parser) {
        this.parser = parser;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        // const pURL = (req.url + (this.parser.serializeRgQuery(req.params) ? '?' + this.parser.serializeRgQuery(req.params) : ''));
        /** @type {?} */
        const pURL = APP_CONFIG.api + '/' + req.urlWithParams;
        /** @type {?} */
        let responseHash;
        if (req.method.substr(0, 3) === 'GET') {
            responseHash = (pURL + APP_CONFIG.authHeaders.xUserToken);
        }
        else {
            responseHash = (pURL + (req.body ? JSON.stringify(req.body) : '') + APP_CONFIG.authHeaders.xUserToken);
        }
        /** @type {?} */
        const apiReq = req.clone({
            url: `${APP_CONFIG.api}/${req.url}`,
            headers: req.headers
                .set('x-service-provider', APP_CONFIG.authHeaders.xServiceProvider || '')
                .set('x-service-user-name', APP_CONFIG.authHeaders.xUserName || '')
                .set('Cache-Control', 'no-cache')
                .set('Pragma', 'no-cache')
                .set('x-service-request-hash', sha1(responseHash))
        });
        return next.handle(apiReq);
        // .pipe(
        //     catchError((error: HttpErrorResponse) => {
        //         let errorMessage: string;
        //         if (error.error instanceof ErrorEvent) {
        //             errorMessage = `Error: ${error.error.message}`;
        //         } else {
        //             errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        //         }
        //         return throwError(errorMessage);
        //     })
        // );
    }
}
APIInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
APIInterceptor.ctorParameters = () => [
    { type: ParserService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    APIInterceptor.prototype.parser;
}
export class ApiService {
    /**
     * @param {?} response
     * @return {?}
     */
    setInterceptedResponse(response) {
        this.interceptedResponse = response;
    }
    /**
     * @return {?}
     */
    getInterceptedResponse() {
        return this.interceptedResponse;
    }
}
ApiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
    { type: NgModule, args: [{
                imports: [],
            },] }
];
/** @nocollapse */ ApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ApiService_Factory() { return new ApiService(); }, token: ApiService, providedIn: "root" });
if (false) {
    /** @type {?} */
    ApiService.prototype.interceptedResponse;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2FwaS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBRSxRQUFRLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDL0MsT0FBTyxJQUFJLE1BQU0sU0FBUyxDQUFDOztBQUszQixNQUFNLE9BQU8sY0FBYzs7OztJQUV2QixZQUNjLE1BQXFCO1FBQXJCLFdBQU0sR0FBTixNQUFNLENBQWU7SUFFbkMsQ0FBQzs7Ozs7O0lBRUQsU0FBUyxDQUFDLEdBQXFCLEVBQUUsSUFBaUI7OztjQUV4QyxJQUFJLEdBQUcsVUFBVSxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDLGFBQWE7O1lBQ2pELFlBQW9CO1FBQ3hCLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssRUFBRTtZQUNuQyxZQUFZLEdBQUcsQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUM3RDthQUFNO1lBQ0gsWUFBWSxHQUFHLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDMUc7O2NBRUssTUFBTSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDckIsR0FBRyxFQUFFLEdBQUcsVUFBVSxDQUFDLEdBQUcsSUFBSSxHQUFHLENBQUMsR0FBRyxFQUFFO1lBQ25DLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTztpQkFDZixHQUFHLENBQUMsb0JBQW9CLEVBQUUsVUFBVSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsSUFBSSxFQUFFLENBQUM7aUJBQ3hFLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxVQUFVLENBQUMsV0FBVyxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUM7aUJBQ2xFLEdBQUcsQ0FBQyxlQUFlLEVBQUUsVUFBVSxDQUFDO2lCQUNoQyxHQUFHLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQztpQkFDekIsR0FBRyxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN6RCxDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzNCLFNBQVM7UUFDVCxpREFBaUQ7UUFDakQsb0NBQW9DO1FBQ3BDLG1EQUFtRDtRQUNuRCw4REFBOEQ7UUFDOUQsbUJBQW1CO1FBQ25CLHVGQUF1RjtRQUN2RixZQUFZO1FBQ1osMkNBQTJDO1FBQzNDLFNBQVM7UUFDVCxLQUFLO0lBQ1QsQ0FBQzs7O1lBeENKLFVBQVU7Ozs7WUFMSCxhQUFhOzs7Ozs7O0lBU2IsZ0NBQStCOztBQStDdkMsTUFBTSxPQUFPLFVBQVU7Ozs7O0lBR25CLHNCQUFzQixDQUFDLFFBQVk7UUFDL0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFFBQVEsQ0FBQztJQUN4QyxDQUFDOzs7O0lBRUQsc0JBQXNCO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDO0lBQ3BDLENBQUM7OztZQWpCSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7WUFFQSxRQUFRLFNBQUM7Z0JBQ04sT0FBTyxFQUFFLEVBQUU7YUFDZDs7Ozs7SUFHRyx5Q0FBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGUsIE5nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QVBQX0NPTkZJR30gZnJvbSAnLi4vYXBwLmNvbmZpZyc7XG5pbXBvcnQge1BhcnNlclNlcnZpY2V9IGZyb20gJy4vcGFyc2VyLnNlcnZpY2UnO1xuaW1wb3J0IHNoYTEgZnJvbSAnanMtc2hhMSc7XG5pbXBvcnQge0h0dHBFdmVudCwgSHR0cEhhbmRsZXIsIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlcXVlc3R9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBUElJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIHBhcnNlcjogUGFyc2VyU2VydmljZVxuICAgICkge1xuICAgIH1cblxuICAgIGludGVyY2VwdChyZXE6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgICAgICAvLyBjb25zdCBwVVJMID0gKHJlcS51cmwgKyAodGhpcy5wYXJzZXIuc2VyaWFsaXplUmdRdWVyeShyZXEucGFyYW1zKSA/ICc/JyArIHRoaXMucGFyc2VyLnNlcmlhbGl6ZVJnUXVlcnkocmVxLnBhcmFtcykgOiAnJykpO1xuICAgICAgICBjb25zdCBwVVJMID0gQVBQX0NPTkZJRy5hcGkgKyAnLycgKyByZXEudXJsV2l0aFBhcmFtcztcbiAgICAgICAgbGV0IHJlc3BvbnNlSGFzaDogc3RyaW5nO1xuICAgICAgICBpZiAocmVxLm1ldGhvZC5zdWJzdHIoMCwgMykgPT09ICdHRVQnKSB7XG4gICAgICAgICAgICByZXNwb25zZUhhc2ggPSAocFVSTCArIEFQUF9DT05GSUcuYXV0aEhlYWRlcnMueFVzZXJUb2tlbik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXNwb25zZUhhc2ggPSAocFVSTCArIChyZXEuYm9keSA/IEpTT04uc3RyaW5naWZ5KHJlcS5ib2R5KSA6ICcnKSArIEFQUF9DT05GSUcuYXV0aEhlYWRlcnMueFVzZXJUb2tlbik7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBhcGlSZXEgPSByZXEuY2xvbmUoe1xuICAgICAgICAgICAgdXJsOiBgJHtBUFBfQ09ORklHLmFwaX0vJHtyZXEudXJsfWAsXG4gICAgICAgICAgICBoZWFkZXJzOiByZXEuaGVhZGVyc1xuICAgICAgICAgICAgICAgIC5zZXQoJ3gtc2VydmljZS1wcm92aWRlcicsIEFQUF9DT05GSUcuYXV0aEhlYWRlcnMueFNlcnZpY2VQcm92aWRlciB8fCAnJylcbiAgICAgICAgICAgICAgICAuc2V0KCd4LXNlcnZpY2UtdXNlci1uYW1lJywgQVBQX0NPTkZJRy5hdXRoSGVhZGVycy54VXNlck5hbWUgfHwgJycpXG4gICAgICAgICAgICAgICAgLnNldCgnQ2FjaGUtQ29udHJvbCcsICduby1jYWNoZScpXG4gICAgICAgICAgICAgICAgLnNldCgnUHJhZ21hJywgJ25vLWNhY2hlJylcbiAgICAgICAgICAgICAgICAuc2V0KCd4LXNlcnZpY2UtcmVxdWVzdC1oYXNoJywgc2hhMShyZXNwb25zZUhhc2gpKVxuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gbmV4dC5oYW5kbGUoYXBpUmVxKTtcbiAgICAgICAgLy8gLnBpcGUoXG4gICAgICAgIC8vICAgICBjYXRjaEVycm9yKChlcnJvcjogSHR0cEVycm9yUmVzcG9uc2UpID0+IHtcbiAgICAgICAgLy8gICAgICAgICBsZXQgZXJyb3JNZXNzYWdlOiBzdHJpbmc7XG4gICAgICAgIC8vICAgICAgICAgaWYgKGVycm9yLmVycm9yIGluc3RhbmNlb2YgRXJyb3JFdmVudCkge1xuICAgICAgICAvLyAgICAgICAgICAgICBlcnJvck1lc3NhZ2UgPSBgRXJyb3I6ICR7ZXJyb3IuZXJyb3IubWVzc2FnZX1gO1xuICAgICAgICAvLyAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vICAgICAgICAgICAgIGVycm9yTWVzc2FnZSA9IGBFcnJvciBDb2RlOiAke2Vycm9yLnN0YXR1c31cXG5NZXNzYWdlOiAke2Vycm9yLm1lc3NhZ2V9YDtcbiAgICAgICAgLy8gICAgICAgICB9XG4gICAgICAgIC8vICAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3JNZXNzYWdlKTtcbiAgICAgICAgLy8gICAgIH0pXG4gICAgICAgIC8vICk7XG4gICAgfVxufVxuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtdLFxufSlcblxuZXhwb3J0IGNsYXNzIEFwaVNlcnZpY2Uge1xuICAgIGludGVyY2VwdGVkUmVzcG9uc2U6IHt9O1xuXG4gICAgc2V0SW50ZXJjZXB0ZWRSZXNwb25zZShyZXNwb25zZToge30pIHtcbiAgICAgICAgdGhpcy5pbnRlcmNlcHRlZFJlc3BvbnNlID0gcmVzcG9uc2U7XG4gICAgfVxuXG4gICAgZ2V0SW50ZXJjZXB0ZWRSZXNwb25zZSgpOiB7fSB7XG4gICAgICAgIHJldHVybiB0aGlzLmludGVyY2VwdGVkUmVzcG9uc2U7XG4gICAgfVxuXG59XG4iXX0=