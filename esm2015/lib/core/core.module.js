/**
 * @fileoverview added by tsickle
 * Generated from: lib/core/core.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { environment } from '../environments/environment';
import { DialogsModule } from './utils/dialogs/dialogs.module';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PopoverModule } from 'ngx-bootstrap';
import { APIInterceptor, ApiService } from './api.service';
import { Angulartics2Module } from 'angulartics2';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateService } from './pages/translate.service';
import { effects, metaReducers, reducers } from './index';
import { CustomSerializer } from './router/router.reducer';
export class CoreModule {
    /**
     * @param {?} parentModule
     * @param {?} http
     */
    constructor(parentModule, http) {
        this.http = http;
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
}
CoreModule.decorators = [
    { type: NgModule, args: [{
                declarations: [],
                imports: [
                    CommonModule,
                    BrowserAnimationsModule,
                    HttpClientModule,
                    StoreModule.forRoot(reducers, {
                        metaReducers,
                        runtimeChecks: {
                            strictStateImmutability: false,
                            strictActionImmutability: false,
                            strictStateSerializability: false,
                            strictActionSerializability: false,
                        },
                    }),
                    EffectsModule.forRoot([...effects]),
                    StoreDevtoolsModule.instrument({
                        maxAge: 25, logOnly: environment.production,
                        actionsBlocklist: ['@ngrx/router*']
                    }),
                    StoreRouterConnectingModule.forRoot(),
                    AccordionModule.forRoot(),
                    AlertModule.forRoot(),
                    BsDropdownModule.forRoot(),
                    ButtonsModule.forRoot(),
                    CollapseModule.forRoot(),
                    ModalModule.forRoot(),
                    TooltipModule.forRoot(),
                    TabsModule.forRoot(),
                    PopoverModule.forRoot(),
                    DialogsModule,
                    ApiService,
                    Angulartics2Module.forRoot(),
                    TranslateModule.forRoot({
                        loader: {
                            provide: TranslateLoader,
                            useClass: TranslateService,
                        }
                    }),
                ],
                providers: [
                    {
                        provide: RouterStateSerializer,
                        useClass: CustomSerializer
                    },
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: APIInterceptor,
                        multi: true
                    },
                ]
            },] }
];
/** @nocollapse */
CoreModule.ctorParameters = () => [
    { type: CoreModule, decorators: [{ type: Optional }, { type: SkipSelf }] },
    { type: HttpClient }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoreModule.prototype.http;
}
/**
 * @param {?} parentModule
 * @param {?} moduleName
 * @return {?}
 */
export function throwIfAlreadyLoaded(parentModule, moduleName) {
    if (parentModule) {
        throw new Error(`${moduleName} has already been loaded. Import ${moduleName} modules in the AppModule only.`);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2NvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzNELE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSxzQ0FBc0MsQ0FBQztBQUM3RSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sYUFBYSxDQUFDO0FBQ3hDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDNUMsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFDLHFCQUFxQixFQUFFLDJCQUEyQixFQUFDLE1BQU0sb0JBQW9CLENBQUM7QUFDdEYsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBQ3hELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUNoRCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUN4RCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDcEQsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBQ3RELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUNoRCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDcEQsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBQ3hELE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUM3RCxPQUFPLEVBQUMsaUJBQWlCLEVBQUUsVUFBVSxFQUFFLGdCQUFnQixFQUFDLE1BQU0sc0JBQXNCLENBQUM7QUFDckYsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLG9CQUFvQixDQUFDO0FBQzlDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDNUMsT0FBTyxFQUFDLGNBQWMsRUFBRSxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sY0FBYyxDQUFDO0FBQ2hELE9BQU8sRUFBQyxlQUFlLEVBQUUsZUFBZSxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFDckUsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sMkJBQTJCLENBQUM7QUFDM0QsT0FBTyxFQUFDLE9BQU8sRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFDLE1BQU0sU0FBUyxDQUFDO0FBQ3hELE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBNkR6RCxNQUFNLE9BQU8sVUFBVTs7Ozs7SUFDbkIsWUFBb0MsWUFBd0IsRUFBVSxJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2xGLG9CQUFvQixDQUFDLFlBQVksRUFBRSxZQUFZLENBQUMsQ0FBQztJQUNyRCxDQUFDOzs7WUE5REosUUFBUSxTQUFDO2dCQUNOLFlBQVksRUFBRSxFQUFFO2dCQUNoQixPQUFPLEVBQUU7b0JBQ0wsWUFBWTtvQkFDWix1QkFBdUI7b0JBQ3ZCLGdCQUFnQjtvQkFFaEIsV0FBVyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUU7d0JBQzFCLFlBQVk7d0JBQ1osYUFBYSxFQUFFOzRCQUNYLHVCQUF1QixFQUFFLEtBQUs7NEJBQzlCLHdCQUF3QixFQUFFLEtBQUs7NEJBQy9CLDBCQUEwQixFQUFFLEtBQUs7NEJBQ2pDLDJCQUEyQixFQUFFLEtBQUs7eUJBQ3JDO3FCQUNKLENBQUM7b0JBQ0YsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUM7b0JBQ25DLG1CQUFtQixDQUFDLFVBQVUsQ0FBQzt3QkFDM0IsTUFBTSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsV0FBVyxDQUFDLFVBQVU7d0JBQzNDLGdCQUFnQixFQUFFLENBQUMsZUFBZSxDQUFDO3FCQUN0QyxDQUFDO29CQUVGLDJCQUEyQixDQUFDLE9BQU8sRUFBRTtvQkFFckMsZUFBZSxDQUFDLE9BQU8sRUFBRTtvQkFDekIsV0FBVyxDQUFDLE9BQU8sRUFBRTtvQkFDckIsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO29CQUMxQixhQUFhLENBQUMsT0FBTyxFQUFFO29CQUN2QixjQUFjLENBQUMsT0FBTyxFQUFFO29CQUN4QixXQUFXLENBQUMsT0FBTyxFQUFFO29CQUNyQixhQUFhLENBQUMsT0FBTyxFQUFFO29CQUN2QixVQUFVLENBQUMsT0FBTyxFQUFFO29CQUNwQixhQUFhLENBQUMsT0FBTyxFQUFFO29CQUV2QixhQUFhO29CQUViLFVBQVU7b0JBRVYsa0JBQWtCLENBQUMsT0FBTyxFQUFFO29CQUM1QixlQUFlLENBQUMsT0FBTyxDQUFDO3dCQUNwQixNQUFNLEVBQUU7NEJBQ0osT0FBTyxFQUFFLGVBQWU7NEJBQ3hCLFFBQVEsRUFBRSxnQkFBZ0I7eUJBQzdCO3FCQUNKLENBQUM7aUJBQ0w7Z0JBQ0QsU0FBUyxFQUFFO29CQUNQO3dCQUNJLE9BQU8sRUFBRSxxQkFBcUI7d0JBQzlCLFFBQVEsRUFBRSxnQkFBZ0I7cUJBQzdCO29CQUNEO3dCQUNJLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLFFBQVEsRUFBRSxjQUFjO3dCQUN4QixLQUFLLEVBQUUsSUFBSTtxQkFDZDtpQkFDSjthQUNKOzs7O1lBR3FELFVBQVUsdUJBQS9DLFFBQVEsWUFBSSxRQUFRO1lBdEVWLFVBQVU7Ozs7Ozs7SUFzRTZCLDBCQUF3Qjs7Ozs7OztBQUsxRixNQUFNLFVBQVUsb0JBQW9CLENBQUMsWUFBaUIsRUFBRSxVQUFrQjtJQUN0RSxJQUFJLFlBQVksRUFBRTtRQUNkLE1BQU0sSUFBSSxLQUFLLENBQUMsR0FBRyxVQUFVLG9DQUFvQyxVQUFVLGlDQUFpQyxDQUFDLENBQUM7S0FDakg7QUFDTCxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZSwgT3B0aW9uYWwsIFNraXBTZWxmfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7QnJvd3NlckFuaW1hdGlvbnNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXIvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7U3RvcmVNb2R1bGV9IGZyb20gJ0BuZ3J4L3N0b3JlJztcclxuaW1wb3J0IHtFZmZlY3RzTW9kdWxlfSBmcm9tICdAbmdyeC9lZmZlY3RzJztcclxuaW1wb3J0IHtTdG9yZURldnRvb2xzTW9kdWxlfSBmcm9tICdAbmdyeC9zdG9yZS1kZXZ0b29scyc7XHJcbmltcG9ydCB7Um91dGVyU3RhdGVTZXJpYWxpemVyLCBTdG9yZVJvdXRlckNvbm5lY3RpbmdNb2R1bGV9IGZyb20gJ0BuZ3J4L3JvdXRlci1zdG9yZSc7XHJcbmltcG9ydCB7QWNjb3JkaW9uTW9kdWxlfSBmcm9tICduZ3gtYm9vdHN0cmFwL2FjY29yZGlvbic7XHJcbmltcG9ydCB7QWxlcnRNb2R1bGV9IGZyb20gJ25neC1ib290c3RyYXAvYWxlcnQnO1xyXG5pbXBvcnQge0JzRHJvcGRvd25Nb2R1bGV9IGZyb20gJ25neC1ib290c3RyYXAvZHJvcGRvd24nO1xyXG5pbXBvcnQge0J1dHRvbnNNb2R1bGV9IGZyb20gJ25neC1ib290c3RyYXAvYnV0dG9ucyc7XHJcbmltcG9ydCB7Q29sbGFwc2VNb2R1bGV9IGZyb20gJ25neC1ib290c3RyYXAvY29sbGFwc2UnO1xyXG5pbXBvcnQge01vZGFsTW9kdWxlfSBmcm9tICduZ3gtYm9vdHN0cmFwL21vZGFsJztcclxuaW1wb3J0IHtUb29sdGlwTW9kdWxlfSBmcm9tICduZ3gtYm9vdHN0cmFwL3Rvb2x0aXAnO1xyXG5pbXBvcnQge2Vudmlyb25tZW50fSBmcm9tICcuLi9lbnZpcm9ubWVudHMvZW52aXJvbm1lbnQnO1xyXG5pbXBvcnQge0RpYWxvZ3NNb2R1bGV9IGZyb20gJy4vdXRpbHMvZGlhbG9ncy9kaWFsb2dzLm1vZHVsZSc7XHJcbmltcG9ydCB7SFRUUF9JTlRFUkNFUFRPUlMsIEh0dHBDbGllbnQsIEh0dHBDbGllbnRNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHtUYWJzTW9kdWxlfSBmcm9tICduZ3gtYm9vdHN0cmFwL3RhYnMnO1xyXG5pbXBvcnQge1BvcG92ZXJNb2R1bGV9IGZyb20gJ25neC1ib290c3RyYXAnO1xyXG5pbXBvcnQge0FQSUludGVyY2VwdG9yLCBBcGlTZXJ2aWNlfSBmcm9tICcuL2FwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHtBbmd1bGFydGljczJNb2R1bGV9IGZyb20gJ2FuZ3VsYXJ0aWNzMic7XHJcbmltcG9ydCB7VHJhbnNsYXRlTG9hZGVyLCBUcmFuc2xhdGVNb2R1bGV9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5pbXBvcnQge1RyYW5zbGF0ZVNlcnZpY2V9IGZyb20gJy4vcGFnZXMvdHJhbnNsYXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQge2VmZmVjdHMsIG1ldGFSZWR1Y2VycywgcmVkdWNlcnN9IGZyb20gJy4vaW5kZXgnO1xyXG5pbXBvcnQge0N1c3RvbVNlcmlhbGl6ZXJ9IGZyb20gJy4vcm91dGVyL3JvdXRlci5yZWR1Y2VyJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtdLFxyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBCcm93c2VyQW5pbWF0aW9uc01vZHVsZSxcclxuICAgICAgICBIdHRwQ2xpZW50TW9kdWxlLFxyXG5cclxuICAgICAgICBTdG9yZU1vZHVsZS5mb3JSb290KHJlZHVjZXJzLCB7XHJcbiAgICAgICAgICAgIG1ldGFSZWR1Y2VycyxcclxuICAgICAgICAgICAgcnVudGltZUNoZWNrczoge1xyXG4gICAgICAgICAgICAgICAgc3RyaWN0U3RhdGVJbW11dGFiaWxpdHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgc3RyaWN0QWN0aW9uSW1tdXRhYmlsaXR5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHN0cmljdFN0YXRlU2VyaWFsaXphYmlsaXR5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHN0cmljdEFjdGlvblNlcmlhbGl6YWJpbGl0eTogZmFsc2UsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgRWZmZWN0c01vZHVsZS5mb3JSb290KFsuLi5lZmZlY3RzXSksXHJcbiAgICAgICAgU3RvcmVEZXZ0b29sc01vZHVsZS5pbnN0cnVtZW50KHtcclxuICAgICAgICAgICAgbWF4QWdlOiAyNSwgbG9nT25seTogZW52aXJvbm1lbnQucHJvZHVjdGlvbixcclxuICAgICAgICAgICAgYWN0aW9uc0Jsb2NrbGlzdDogWydAbmdyeC9yb3V0ZXIqJ11cclxuICAgICAgICB9KSxcclxuXHJcbiAgICAgICAgU3RvcmVSb3V0ZXJDb25uZWN0aW5nTW9kdWxlLmZvclJvb3QoKSxcclxuXHJcbiAgICAgICAgQWNjb3JkaW9uTW9kdWxlLmZvclJvb3QoKSxcclxuICAgICAgICBBbGVydE1vZHVsZS5mb3JSb290KCksXHJcbiAgICAgICAgQnNEcm9wZG93bk1vZHVsZS5mb3JSb290KCksXHJcbiAgICAgICAgQnV0dG9uc01vZHVsZS5mb3JSb290KCksXHJcbiAgICAgICAgQ29sbGFwc2VNb2R1bGUuZm9yUm9vdCgpLFxyXG4gICAgICAgIE1vZGFsTW9kdWxlLmZvclJvb3QoKSxcclxuICAgICAgICBUb29sdGlwTW9kdWxlLmZvclJvb3QoKSxcclxuICAgICAgICBUYWJzTW9kdWxlLmZvclJvb3QoKSxcclxuICAgICAgICBQb3BvdmVyTW9kdWxlLmZvclJvb3QoKSxcclxuXHJcbiAgICAgICAgRGlhbG9nc01vZHVsZSxcclxuXHJcbiAgICAgICAgQXBpU2VydmljZSxcclxuXHJcbiAgICAgICAgQW5ndWxhcnRpY3MyTW9kdWxlLmZvclJvb3QoKSxcclxuICAgICAgICBUcmFuc2xhdGVNb2R1bGUuZm9yUm9vdCh7XHJcbiAgICAgICAgICAgIGxvYWRlcjoge1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZTogVHJhbnNsYXRlTG9hZGVyLFxyXG4gICAgICAgICAgICAgICAgdXNlQ2xhc3M6IFRyYW5zbGF0ZVNlcnZpY2UsXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KSxcclxuICAgIF0sXHJcbiAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHByb3ZpZGU6IFJvdXRlclN0YXRlU2VyaWFsaXplcixcclxuICAgICAgICAgICAgdXNlQ2xhc3M6IEN1c3RvbVNlcmlhbGl6ZXJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXHJcbiAgICAgICAgICAgIHVzZUNsYXNzOiBBUElJbnRlcmNlcHRvcixcclxuICAgICAgICAgICAgbXVsdGk6IHRydWVcclxuICAgICAgICB9LFxyXG4gICAgXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENvcmVNb2R1bGUge1xyXG4gICAgY29uc3RydWN0b3IoQE9wdGlvbmFsKCkgQFNraXBTZWxmKCkgcGFyZW50TW9kdWxlOiBDb3JlTW9kdWxlLCBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHtcclxuICAgICAgICB0aHJvd0lmQWxyZWFkeUxvYWRlZChwYXJlbnRNb2R1bGUsICdDb3JlTW9kdWxlJyk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiB0aHJvd0lmQWxyZWFkeUxvYWRlZChwYXJlbnRNb2R1bGU6IGFueSwgbW9kdWxlTmFtZTogc3RyaW5nKSB7XHJcbiAgICBpZiAocGFyZW50TW9kdWxlKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKGAke21vZHVsZU5hbWV9IGhhcyBhbHJlYWR5IGJlZW4gbG9hZGVkLiBJbXBvcnQgJHttb2R1bGVOYW1lfSBtb2R1bGVzIGluIHRoZSBBcHBNb2R1bGUgb25seS5gKTtcclxuICAgIH1cclxufVxyXG5cclxuIl19