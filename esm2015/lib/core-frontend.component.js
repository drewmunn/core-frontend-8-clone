/**
 * @fileoverview added by tsickle
 * Generated from: lib/core-frontend.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Angulartics2GoogleTagManager } from 'angulartics2/gtm';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService as TranslateProvider } from '@ngx-translate/core';
import { TranslateService } from './core/pages/translate.service';
import { AngularticsGCLOG } from './core/gclog/gclogAngulartics';
export class CoreFrontendComponent {
    /**
     * @param {?} translate
     * @param {?} translateProvider
     * @param {?} router
     * @param {?} gtm
     * @param {?} gclog
     */
    constructor(translate, translateProvider, router, gtm, gclog) {
        this.translate = translate;
        this.translateProvider = translateProvider;
        this.router = router;
        this.gtm = gtm;
        this.gclog = gclog;
        this.title = 'core-frontend';
        gtm.startTracking();
        gclog.startTracking();
        translateProvider.setDefaultLang(translate.getDefaultLanguage());
        translate.getLanguages().subscribe((/**
         * @param {?} languages
         * @return {?}
         */
        languages => {
            translateProvider.addLangs(languages.map((/**
             * @param {?} language
             * @return {?}
             */
            language => {
                return language.code;
            })));
        }));
        this.router.events.subscribe((/**
         * @param {?} event
         * @return {?}
         */
        event => {
            if (event instanceof NavigationEnd) {
                try {
                    gtm.pageTrack(event.urlAfterRedirects);
                    gclog.pageTrack(event.urlAfterRedirects);
                }
                catch (err) {
                    console.log(err);
                }
            }
        }));
    }
}
CoreFrontendComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-root',
                template: '<router-outlet></router-outlet>'
            }] }
];
/** @nocollapse */
CoreFrontendComponent.ctorParameters = () => [
    { type: TranslateService },
    { type: TranslateProvider },
    { type: Router },
    { type: Angulartics2GoogleTagManager },
    { type: AngularticsGCLOG }
];
if (false) {
    /** @type {?} */
    CoreFrontendComponent.prototype.title;
    /**
     * @type {?}
     * @private
     */
    CoreFrontendComponent.prototype.translate;
    /**
     * @type {?}
     * @private
     */
    CoreFrontendComponent.prototype.translateProvider;
    /** @type {?} */
    CoreFrontendComponent.prototype.router;
    /** @type {?} */
    CoreFrontendComponent.prototype.gtm;
    /** @type {?} */
    CoreFrontendComponent.prototype.gclog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1mcm9udGVuZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZ2MvY29yZS1mcm9udGVuZC8iLCJzb3VyY2VzIjpbImxpYi9jb3JlLWZyb250ZW5kLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDeEMsT0FBTyxFQUFDLDRCQUE0QixFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDOUQsT0FBTyxFQUFDLGFBQWEsRUFBRSxNQUFNLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUN0RCxPQUFPLEVBQUMsZ0JBQWdCLElBQUksaUJBQWlCLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUMxRSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQU8vRCxNQUFNLE9BQU8scUJBQXFCOzs7Ozs7OztJQUc5QixZQUNZLFNBQTJCLEVBQzNCLGlCQUFvQyxFQUNyQyxNQUFjLEVBQ2QsR0FBaUMsRUFDakMsS0FBdUI7UUFKdEIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0Isc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNyQyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsUUFBRyxHQUFILEdBQUcsQ0FBOEI7UUFDakMsVUFBSyxHQUFMLEtBQUssQ0FBa0I7UUFQbEMsVUFBSyxHQUFHLGVBQWUsQ0FBQztRQVNwQixHQUFHLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDcEIsS0FBSyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRXRCLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDO1FBQ2pFLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxTQUFTOzs7O1FBQUMsU0FBUyxDQUFDLEVBQUU7WUFDM0MsaUJBQWlCLENBQUMsUUFBUSxDQUN0QixTQUFTLENBQUMsR0FBRzs7OztZQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNyQixPQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDekIsQ0FBQyxFQUFDLENBQ0wsQ0FBQztRQUNOLENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2pDLElBQUksS0FBSyxZQUFZLGFBQWEsRUFBRTtnQkFDaEMsSUFBSTtvQkFDQSxHQUFHLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO29CQUN2QyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2lCQUM1QztnQkFBQyxPQUFPLEdBQUcsRUFBRTtvQkFDVixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUNwQjthQUNKO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFFUCxDQUFDOzs7WUF0Q0osU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxXQUFXO2dCQUNyQixRQUFRLEVBQUUsaUNBQWlDO2FBQzlDOzs7O1lBTk8sZ0JBQWdCO1lBREksaUJBQWlCO1lBRHRCLE1BQU07WUFEckIsNEJBQTRCO1lBSTVCLGdCQUFnQjs7OztJQVFwQixzQ0FBd0I7Ozs7O0lBR3BCLDBDQUFtQzs7Ozs7SUFDbkMsa0RBQTRDOztJQUM1Qyx1Q0FBcUI7O0lBQ3JCLG9DQUF3Qzs7SUFDeEMsc0NBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtBbmd1bGFydGljczJHb29nbGVUYWdNYW5hZ2VyfSBmcm9tICdhbmd1bGFydGljczIvZ3RtJztcbmltcG9ydCB7TmF2aWdhdGlvbkVuZCwgUm91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHtUcmFuc2xhdGVTZXJ2aWNlIGFzIFRyYW5zbGF0ZVByb3ZpZGVyfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7VHJhbnNsYXRlU2VydmljZX0gZnJvbSAnLi9jb3JlL3BhZ2VzL3RyYW5zbGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7QW5ndWxhcnRpY3NHQ0xPR30gZnJvbSAnLi9jb3JlL2djbG9nL2djbG9nQW5ndWxhcnRpY3MnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvcmUtcm9vdCcsXG4gICAgdGVtcGxhdGU6ICc8cm91dGVyLW91dGxldD48L3JvdXRlci1vdXRsZXQ+J1xufSlcblxuZXhwb3J0IGNsYXNzIENvcmVGcm9udGVuZENvbXBvbmVudCB7XG4gICAgdGl0bGUgPSAnY29yZS1mcm9udGVuZCc7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgdHJhbnNsYXRlUHJvdmlkZXI6IFRyYW5zbGF0ZVByb3ZpZGVyLFxuICAgICAgICBwdWJsaWMgcm91dGVyOiBSb3V0ZXIsXG4gICAgICAgIHB1YmxpYyBndG06IEFuZ3VsYXJ0aWNzMkdvb2dsZVRhZ01hbmFnZXIsXG4gICAgICAgIHB1YmxpYyBnY2xvZzogQW5ndWxhcnRpY3NHQ0xPRyxcbiAgICApIHtcbiAgICAgICAgZ3RtLnN0YXJ0VHJhY2tpbmcoKTtcbiAgICAgICAgZ2Nsb2cuc3RhcnRUcmFja2luZygpO1xuXG4gICAgICAgIHRyYW5zbGF0ZVByb3ZpZGVyLnNldERlZmF1bHRMYW5nKHRyYW5zbGF0ZS5nZXREZWZhdWx0TGFuZ3VhZ2UoKSk7XG4gICAgICAgIHRyYW5zbGF0ZS5nZXRMYW5ndWFnZXMoKS5zdWJzY3JpYmUobGFuZ3VhZ2VzID0+IHtcbiAgICAgICAgICAgIHRyYW5zbGF0ZVByb3ZpZGVyLmFkZExhbmdzKFxuICAgICAgICAgICAgICAgIGxhbmd1YWdlcy5tYXAobGFuZ3VhZ2UgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbGFuZ3VhZ2UuY29kZTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy5yb3V0ZXIuZXZlbnRzLnN1YnNjcmliZShldmVudCA9PiB7XG4gICAgICAgICAgICBpZiAoZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSB7XG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgZ3RtLnBhZ2VUcmFjayhldmVudC51cmxBZnRlclJlZGlyZWN0cyk7XG4gICAgICAgICAgICAgICAgICAgIGdjbG9nLnBhZ2VUcmFjayhldmVudC51cmxBZnRlclJlZGlyZWN0cyk7XG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgIH1cblxufVxuIl19