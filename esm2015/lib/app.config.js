/**
 * @fileoverview added by tsickle
 * Generated from: lib/app.config.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
export const APP_CONFIG = {
    appName: 'CoreFrontend',
    api: '',
    authHeaders: {
        xUserName: null,
        xUserToken: null,
        xServiceProvider: null,
    },
    provider: null,
    user: 'Matt Dixon',
    email: 'core@builtoncore.com',
    twitter: 'builtoncore',
    avatar: 'avatar-admin.png',
    version: '1.0.0',
    bs4v: '4.3',
    logo: 'logo.svg',
    logoM: 'logo.svg',
    copyright: new Date().getFullYear() + ' © <a href="https://builtoncore.com" class="text-primary fw-500" ' +
        'title="Builtoncore" target="_blank">Gallagher Communication</a>',
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BnYy9jb3JlLWZyb250ZW5kLyIsInNvdXJjZXMiOlsibGliL2FwcC5jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsTUFBTSxPQUFPLFVBQVUsR0FBRztJQUN0QixPQUFPLEVBQUUsY0FBYztJQUN2QixHQUFHLEVBQUUsRUFBRTtJQUNQLFdBQVcsRUFBRTtRQUNULFNBQVMsRUFBRSxJQUFJO1FBQ2YsVUFBVSxFQUFFLElBQUk7UUFDaEIsZ0JBQWdCLEVBQUUsSUFBSTtLQUN6QjtJQUNELFFBQVEsRUFBRSxJQUFJO0lBQ2QsSUFBSSxFQUFFLFlBQVk7SUFDbEIsS0FBSyxFQUFFLHNCQUFzQjtJQUM3QixPQUFPLEVBQUUsYUFBYTtJQUN0QixNQUFNLEVBQUUsa0JBQWtCO0lBQzFCLE9BQU8sRUFBRSxPQUFPO0lBQ2hCLElBQUksRUFBRSxLQUFLO0lBQ1gsSUFBSSxFQUFFLFVBQVU7SUFDaEIsS0FBSyxFQUFFLFVBQVU7SUFDakIsU0FBUyxFQUFFLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLEdBQUcsbUVBQW1FO1FBQ3JHLGlFQUFpRTtDQUN4RSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBBUFBfQ09ORklHID0ge1xyXG4gICAgYXBwTmFtZTogJ0NvcmVGcm9udGVuZCcsXHJcbiAgICBhcGk6ICcnLFxyXG4gICAgYXV0aEhlYWRlcnM6IHtcclxuICAgICAgICB4VXNlck5hbWU6IG51bGwsXHJcbiAgICAgICAgeFVzZXJUb2tlbjogbnVsbCxcclxuICAgICAgICB4U2VydmljZVByb3ZpZGVyOiBudWxsLFxyXG4gICAgfSxcclxuICAgIHByb3ZpZGVyOiBudWxsLFxyXG4gICAgdXNlcjogJ01hdHQgRGl4b24nLFxyXG4gICAgZW1haWw6ICdjb3JlQGJ1aWx0b25jb3JlLmNvbScsXHJcbiAgICB0d2l0dGVyOiAnYnVpbHRvbmNvcmUnLFxyXG4gICAgYXZhdGFyOiAnYXZhdGFyLWFkbWluLnBuZycsXHJcbiAgICB2ZXJzaW9uOiAnMS4wLjAnLFxyXG4gICAgYnM0djogJzQuMycsXHJcbiAgICBsb2dvOiAnbG9nby5zdmcnLFxyXG4gICAgbG9nb006ICdsb2dvLnN2ZycsXHJcbiAgICBjb3B5cmlnaHQ6IG5ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKSArICcgwqkgPGEgaHJlZj1cImh0dHBzOi8vYnVpbHRvbmNvcmUuY29tXCIgY2xhc3M9XCJ0ZXh0LXByaW1hcnkgZnctNTAwXCIgJyArXHJcbiAgICAgICAgJ3RpdGxlPVwiQnVpbHRvbmNvcmVcIiB0YXJnZXQ9XCJfYmxhbmtcIj5HYWxsYWdoZXIgQ29tbXVuaWNhdGlvbjwvYT4nLFxyXG59O1xyXG4iXX0=