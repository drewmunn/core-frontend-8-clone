(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('@angular/platform-browser/animations'), require('@ngrx/store'), require('@ngrx/effects'), require('@ngrx/store-devtools'), require('@ngrx/router-store'), require('ngx-bootstrap/accordion'), require('ngx-bootstrap/alert'), require('ngx-bootstrap/dropdown'), require('ngx-bootstrap/buttons'), require('ngx-bootstrap/collapse'), require('ngx-bootstrap/modal'), require('ngx-bootstrap/tooltip'), require('ngx-bootstrap'), require('rxjs'), require('@angular/common/http'), require('ngx-bootstrap/tabs'), require('js-sha1'), require('angulartics2'), require('@ngx-translate/core'), require('@angular/router'), require('rxjs/operators'), require('ngrx-store-localstorage'), require('@angular/cdk/layout'), require('@angular/forms'), require('lodash'), require('@angular/material/progress-bar'), require('@angular/material/progress-spinner'), require('@angular/material/button'), require('@angular/material/icon'), require('@angular/material/expansion'), require('@angular/material/list'), require('@angular/material/tabs'), require('@angular/animations'), require('angulartics2/gtm'), require('@angular/platform-browser')) :
    typeof define === 'function' && define.amd ? define('@gc/core-frontend', ['exports', '@angular/core', '@angular/common', '@angular/platform-browser/animations', '@ngrx/store', '@ngrx/effects', '@ngrx/store-devtools', '@ngrx/router-store', 'ngx-bootstrap/accordion', 'ngx-bootstrap/alert', 'ngx-bootstrap/dropdown', 'ngx-bootstrap/buttons', 'ngx-bootstrap/collapse', 'ngx-bootstrap/modal', 'ngx-bootstrap/tooltip', 'ngx-bootstrap', 'rxjs', '@angular/common/http', 'ngx-bootstrap/tabs', 'js-sha1', 'angulartics2', '@ngx-translate/core', '@angular/router', 'rxjs/operators', 'ngrx-store-localstorage', '@angular/cdk/layout', '@angular/forms', 'lodash', '@angular/material/progress-bar', '@angular/material/progress-spinner', '@angular/material/button', '@angular/material/icon', '@angular/material/expansion', '@angular/material/list', '@angular/material/tabs', '@angular/animations', 'angulartics2/gtm', '@angular/platform-browser'], factory) :
    (global = global || self, factory((global.gc = global.gc || {}, global.gc['core-frontend'] = {}), global.ng.core, global.ng.common, global.ng.platformBrowser.animations, global.store, global.effects$1, global.storeDevtools, global.routerStore, global.accordion, global.alert, global.dropdown, global.buttons, global.collapse, global.modal, global.tooltip, global.ngxBootstrap, global.rxjs, global.ng.common.http, global.tabs, global.sha1, global.angulartics2, global.core$1, global.ng.router, global.rxjs.operators, global.ngrxStoreLocalstorage, global.ng.cdk.layout, global.ng.forms, global.lodash, global.ng.material['progress-bar'], global.ng.material['progress-spinner'], global.ng.material.button, global.ng.material.icon, global.ng.material.expansion, global.ng.material.list, global.ng.material.tabs, global.ng.animations, global.gtm, global.ng.platformBrowser));
}(this, (function (exports, core, common, animations, store, effects$1, storeDevtools, routerStore, accordion, alert, dropdown, buttons, collapse, modal, tooltip, ngxBootstrap, rxjs, http, tabs, sha1, angulartics2, core$1, router, operators, ngrxStoreLocalstorage, layout, forms, lodash, progressBar, progressSpinner, button, icon, expansion, list, tabs$1, animations$1, gtm, platformBrowser) { 'use strict';

    sha1 = sha1 && sha1.hasOwnProperty('default') ? sha1['default'] : sha1;

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/environments/environment.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var environment = {
        production: false,
    };

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/dialogs/confirm-dialog/confirm-dialog.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ConfirmDialogComponent = /** @class */ (function () {
        function ConfirmDialogComponent(bsModalRef) {
            this.bsModalRef = bsModalRef;
            this.onClose = new rxjs.Subject();
        }
        /**
         * @param {?} $event
         * @return {?}
         */
        ConfirmDialogComponent.prototype.confirm = /**
         * @param {?} $event
         * @return {?}
         */
        function ($event) {
            this.onClose.next(true);
            this.onClose.complete();
            this.bsModalRef.hide();
        };
        /**
         * @param {?} $event
         * @return {?}
         */
        ConfirmDialogComponent.prototype.cancel = /**
         * @param {?} $event
         * @return {?}
         */
        function ($event) {
            this.onClose.next(false);
            this.onClose.complete();
            this.bsModalRef.hide();
        };
        ConfirmDialogComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-confirm-dialog',
                        template: "\n        <div class=\"modal-header\">\n            <div class=\"modal-title\" [innerHTML]=\"title\">\n            </div>\n        </div>\n        <div class=\"modal-body\" [innerHTML]=\"message\"></div>\n        <div class=\"modal-footer\" *ngIf=\"buttons\">\n            <button (click)=\"confirm($event)\" type=\"button\" class=\"btn bootbox-accept {{buttons.confirm.className}}\">\n                {{buttons.confirm.label}}</button>\n            <button (click)=\"cancel($event)\" type=\"button\" class=\"btn bootbox-cancel {{buttons.cancel.className}}\">\n                {{buttons.cancel.label}}</button>\n        </div>\n    "
                    }] }
        ];
        /** @nocollapse */
        ConfirmDialogComponent.ctorParameters = function () { return [
            { type: ngxBootstrap.BsModalRef }
        ]; };
        return ConfirmDialogComponent;
    }());
    if (false) {
        /** @type {?} */
        ConfirmDialogComponent.prototype.title;
        /** @type {?} */
        ConfirmDialogComponent.prototype.message;
        /** @type {?} */
        ConfirmDialogComponent.prototype.buttons;
        /** @type {?} */
        ConfirmDialogComponent.prototype.onClose;
        /** @type {?} */
        ConfirmDialogComponent.prototype.bsModalRef;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/dialogs/dialogs.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DialogsService = /** @class */ (function () {
        function DialogsService(rendererFactory, modalService) {
            this.modalService = modalService;
            this.renderer = rendererFactory.createRenderer(null, null);
        }
        /**
         * @param {?} initialState
         * @return {?}
         */
        DialogsService.prototype.confirm = /**
         * @param {?} initialState
         * @return {?}
         */
        function (initialState) {
            this.playSound('messagebox');
            this.bsModalRef = this.modalService.show(ConfirmDialogComponent, {
                initialState: initialState,
                backdrop: 'static',
                keyboard: false,
                class: 'modal-dialog-centered'
            });
            this.renderer.addClass(document.querySelector('.modal'), 'modal-alert');
            return (/** @type {?} */ (this.bsModalRef.content.onClose));
        };
        /**
         * @param {?} sound
         * @param {?=} path
         * @return {?}
         */
        DialogsService.prototype.playSound = /**
         * @param {?} sound
         * @param {?=} path
         * @return {?}
         */
        function (sound, path) {
            if (path === void 0) { path = 'assets/media/sound'; }
            /** @type {?} */
            var audioElement = document.createElement('audio');
            if (navigator.userAgent.match('Firefox/')) {
                audioElement.setAttribute('src', path + '/' + sound + '.ogg');
            }
            else {
                audioElement.setAttribute('src', path + '/' + sound + '.mp3');
            }
            audioElement.addEventListener('load', (/**
             * @return {?}
             */
            function () {
                audioElement.play();
            }), true);
            audioElement.pause();
            audioElement.play();
        };
        DialogsService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        DialogsService.ctorParameters = function () { return [
            { type: core.RendererFactory2 },
            { type: ngxBootstrap.BsModalService }
        ]; };
        return DialogsService;
    }());
    if (false) {
        /** @type {?} */
        DialogsService.prototype.bsModalRef;
        /** @type {?} */
        DialogsService.prototype.renderer;
        /**
         * @type {?}
         * @private
         */
        DialogsService.prototype.modalService;
    }
    /**
     * @record
     */
    function DialogOptions() { }
    if (false) {
        /** @type {?} */
        DialogOptions.prototype.title;
        /** @type {?} */
        DialogOptions.prototype.message;
        /** @type {?} */
        DialogOptions.prototype.buttons;
    }
    /**
     * @record
     */
    function DialogButton() { }
    if (false) {
        /** @type {?} */
        DialogButton.prototype.label;
        /** @type {?} */
        DialogButton.prototype.className;
    }
    /**
     * @record
     */
    function DialogButtons() { }
    if (false) {
        /** @type {?|undefined} */
        DialogButtons.prototype.confirm;
        /** @type {?|undefined} */
        DialogButtons.prototype.cancel;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/dialogs/dialogs.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DialogsModule = /** @class */ (function () {
        function DialogsModule() {
        }
        DialogsModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [ConfirmDialogComponent],
                        entryComponents: [ConfirmDialogComponent],
                        imports: [
                            common.CommonModule,
                            ngxBootstrap.ModalModule
                        ],
                        providers: [DialogsService]
                    },] }
        ];
        return DialogsModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/app.config.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var APP_CONFIG = {
        appName: 'CoreFrontend',
        api: '',
        authHeaders: {
            xUserName: null,
            xUserToken: null,
            xServiceProvider: null,
        },
        provider: null,
        user: 'Matt Dixon',
        email: 'core@builtoncore.com',
        twitter: 'builtoncore',
        avatar: 'avatar-admin.png',
        version: '1.0.0',
        bs4v: '4.3',
        logo: 'logo.svg',
        logoM: 'logo.svg',
        copyright: new Date().getFullYear() + ' © <a href="https://builtoncore.com" class="text-primary fw-500" ' +
            'title="Builtoncore" target="_blank">Gallagher Communication</a>',
    };

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/parser.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ParserService = /** @class */ (function () {
        function ParserService() {
        }
        /**
         * @param {?} text
         * @param {?} find
         * @param {?} replace
         * @return {?}
         */
        ParserService.prototype.replaceAll = /**
         * @param {?} text
         * @param {?} find
         * @param {?} replace
         * @return {?}
         */
        function (text, find, replace) {
            /**
             * @param {?} textString
             * @return {?}
             */
            function escape(textString) {
                return textString.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
            }
            return text.replace(new RegExp(escape(find), 'g'), replace);
        };
        /**
         * @param {?} obj
         * @param {?=} prefix
         * @return {?}
         */
        ParserService.prototype.serializeRgQuery = /**
         * @param {?} obj
         * @param {?=} prefix
         * @return {?}
         */
        function (obj, prefix) {
            /** @type {?} */
            var str = [];
            /** @type {?} */
            var keys = [];
            for (var k in obj) {
                if (obj.hasOwnProperty(k)) {
                    keys.push(k);
                }
            }
            keys.sort();
            for (var i = 0; i < keys.length; i++) {
                /** @type {?} */
                var k = prefix ? prefix + '[' + keys[i] + ']' : keys[i];
                /** @type {?} */
                var v = obj[keys[i]];
                str.push(typeof v == 'object' ?
                    encodeURIComponent(k) + '=' + encodeURIComponent(JSON.stringify(v)).replace(/%3A/g, ':') :
                    encodeURIComponent(k) + '=' + encodeURIComponent(v));
            }
            /** @type {?} */
            var finalStr = str.join('&');
            finalStr = this.replaceAll(finalStr, '%40', '@');
            finalStr = this.replaceAll(finalStr, '%3A', ':');
            finalStr = this.replaceAll(finalStr, '%2C', ',');
            finalStr = this.replaceAll(finalStr, '%20', '+');
            return finalStr;
        };
        /**
         * @param {?} cname
         * @return {?}
         */
        ParserService.prototype.getCookie = /**
         * @param {?} cname
         * @return {?}
         */
        function (cname) {
            /** @type {?} */
            var name = cname + '=';
            /** @type {?} */
            var decodedCookie = decodeURIComponent(document.cookie);
            /** @type {?} */
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                /** @type {?} */
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return false;
        };
        /**
         * @param {?} name
         * @param {?} path
         * @param {?=} domain
         * @return {?}
         */
        ParserService.prototype.deleteCookie = /**
         * @param {?} name
         * @param {?} path
         * @param {?=} domain
         * @return {?}
         */
        function (name, path, domain) {
            if (this.getCookie(name)) {
                try {
                    document.cookie = name + '=' +
                        ((path) ? ';path=' + path : '') +
                        ((domain) ? ';domain=' + domain : '') +
                        ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                    document.cookie = name + '=' +
                        ((path) ? ';path=' + path : '') +
                        ((domain) ? ';domain=.' + domain : '') +
                        ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                    document.cookie = name + '=' +
                        ((path) ? ';path=' + path : '') + ';domain=' + window.location.hostname +
                        ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                    document.cookie = name + '=' +
                        ((path) ? ';path=' + path : '') + ';domain=.' + window.location.hostname +
                        ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                    document.cookie = name + '=' +
                        ((domain) ? ';domain=' + domain : '') +
                        ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                    document.cookie = name + '=' +
                        ((domain) ? ';domain=.' + domain : '') +
                        ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                    document.cookie = name + '=' +
                        ';domain=' + window.location.hostname +
                        ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                    document.cookie = name + '=' +
                        ';domain=.' + window.location.hostname +
                        ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
                }
                catch (err) {
                }
            }
        };
        ParserService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */ ParserService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function ParserService_Factory() { return new ParserService(); }, token: ParserService, providedIn: "root" });
        return ParserService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/api.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var APIInterceptor = /** @class */ (function () {
        function APIInterceptor(parser) {
            this.parser = parser;
        }
        /**
         * @param {?} req
         * @param {?} next
         * @return {?}
         */
        APIInterceptor.prototype.intercept = /**
         * @param {?} req
         * @param {?} next
         * @return {?}
         */
        function (req, next) {
            // const pURL = (req.url + (this.parser.serializeRgQuery(req.params) ? '?' + this.parser.serializeRgQuery(req.params) : ''));
            /** @type {?} */
            var pURL = APP_CONFIG.api + '/' + req.urlWithParams;
            /** @type {?} */
            var responseHash;
            if (req.method.substr(0, 3) === 'GET') {
                responseHash = (pURL + APP_CONFIG.authHeaders.xUserToken);
            }
            else {
                responseHash = (pURL + (req.body ? JSON.stringify(req.body) : '') + APP_CONFIG.authHeaders.xUserToken);
            }
            /** @type {?} */
            var apiReq = req.clone({
                url: APP_CONFIG.api + "/" + req.url,
                headers: req.headers
                    .set('x-service-provider', APP_CONFIG.authHeaders.xServiceProvider || '')
                    .set('x-service-user-name', APP_CONFIG.authHeaders.xUserName || '')
                    .set('Cache-Control', 'no-cache')
                    .set('Pragma', 'no-cache')
                    .set('x-service-request-hash', sha1(responseHash))
            });
            return next.handle(apiReq);
            // .pipe(
            //     catchError((error: HttpErrorResponse) => {
            //         let errorMessage: string;
            //         if (error.error instanceof ErrorEvent) {
            //             errorMessage = `Error: ${error.error.message}`;
            //         } else {
            //             errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            //         }
            //         return throwError(errorMessage);
            //     })
            // );
        };
        APIInterceptor.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        APIInterceptor.ctorParameters = function () { return [
            { type: ParserService }
        ]; };
        return APIInterceptor;
    }());
    if (false) {
        /**
         * @type {?}
         * @protected
         */
        APIInterceptor.prototype.parser;
    }
    var ApiService = /** @class */ (function () {
        function ApiService() {
        }
        /**
         * @param {?} response
         * @return {?}
         */
        ApiService.prototype.setInterceptedResponse = /**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            this.interceptedResponse = response;
        };
        /**
         * @return {?}
         */
        ApiService.prototype.getInterceptedResponse = /**
         * @return {?}
         */
        function () {
            return this.interceptedResponse;
        };
        ApiService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] },
            { type: core.NgModule, args: [{
                        imports: [],
                    },] }
        ];
        /** @nocollapse */ ApiService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function ApiService_Factory() { return new ApiService(); }, token: ApiService, providedIn: "root" });
        return ApiService;
    }());
    if (false) {
        /** @type {?} */
        ApiService.prototype.interceptedResponse;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/cache.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CacheService = /** @class */ (function () {
        function CacheService(router, api) {
            this.router = router;
            this.api = api;
            this.refresh();
        }
        /**
         * @return {?}
         */
        CacheService.prototype.refresh = /**
         * @return {?}
         */
        function () {
            this.cacheProbeTimestamp = {};
            this.objectLoadObservables$ = {};
            this.cacheProbePromises$ = {};
        };
        /**
         * @param {?} label
         * @return {?}
         */
        CacheService.prototype.resetObjectLoad = /**
         * @param {?} label
         * @return {?}
         */
        function (label) {
            delete this.objectLoadObservables$[label];
        };
        /**
         * @param {?} inputs
         * @return {?}
         */
        CacheService.prototype.createCacheKey = /**
         * @param {?} inputs
         * @return {?}
         */
        function (inputs) {
            return JSON.stringify(inputs);
        };
        /**
         * @param {?} item
         * @return {?}
         */
        CacheService.prototype.getFromStorage = /**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            try {
                if (localStorage.getItem(item)) {
                    return localStorage.getItem(item);
                }
            }
            catch (e) {
            }
            try {
                if (sessionStorage.getItem(item)) {
                    return sessionStorage.getItem(item);
                }
            }
            catch (e) {
            }
            return false;
        };
        /**
         * @param {?} item
         * @return {?}
         */
        CacheService.prototype.removeFromStorage = /**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            try {
                localStorage.removeItem(item);
            }
            catch (e) {
            }
            try {
                sessionStorage.removeItem(item);
            }
            catch (e) {
            }
        };
        /**
         * @param {?} route
         * @param {?} identifier
         * @param {?=} headers
         * @param {?=} cacheKey
         * @param {?=} query
         * @return {?}
         */
        CacheService.prototype.getOneThroughCache = /**
         * @param {?} route
         * @param {?} identifier
         * @param {?=} headers
         * @param {?=} cacheKey
         * @param {?=} query
         * @return {?}
         */
        function (route, identifier, headers, cacheKey, query) {
            var _this = this;
            if (!cacheKey) {
                cacheKey = this.createCacheKey({
                    route: route,
                    identifier: identifier,
                    headers: headers,
                    query: query,
                });
            }
            if (!this.objectLoadObservables$) {
                this.objectLoadObservables$ = {};
            }
            if (this.objectLoadObservables$[cacheKey]) {
                return this.objectLoadObservables$[cacheKey];
            }
            this.objectLoadObservables$[cacheKey] = new rxjs.ReplaySubject();
            this.api.get(route + (identifier ? '/' + identifier : ''), {
                params: query || {},
                headers: headers || {}
            }).subscribe((/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                try {
                    _this.objectLoadObservables$[cacheKey].next(response);
                }
                catch (err) {
                }
            }));
            return this.objectLoadObservables$[cacheKey];
        };
        /**
         * @param {?} route
         * @param {?=} query
         * @param {?=} headers
         * @param {?=} cacheKey
         * @return {?}
         */
        CacheService.prototype.getListThroughCache = /**
         * @param {?} route
         * @param {?=} query
         * @param {?=} headers
         * @param {?=} cacheKey
         * @return {?}
         */
        function (route, query, headers, cacheKey) {
            var _this = this;
            if (!cacheKey) {
                cacheKey = this.createCacheKey({
                    route: route,
                    query: query,
                    headers: headers
                });
            }
            if (!this.objectLoadObservables$) {
                this.objectLoadObservables$ = {};
            }
            if (this.objectLoadObservables$[cacheKey]) {
                return this.objectLoadObservables$[cacheKey];
            }
            this.objectLoadObservables$[cacheKey] = new rxjs.ReplaySubject();
            this.api.get(route, {
                params: query || {},
                headers: headers || {}
            })
                .subscribe((/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                try {
                    _this.objectLoadObservables$[cacheKey].next(response);
                }
                catch (err) {
                }
            }));
            return this.objectLoadObservables$[cacheKey];
        };
        CacheService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        CacheService.ctorParameters = function () { return [
            { type: router.Router },
            { type: http.HttpClient }
        ]; };
        /** @nocollapse */ CacheService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function CacheService_Factory() { return new CacheService(core.ɵɵinject(router.Router), core.ɵɵinject(http.HttpClient)); }, token: CacheService, providedIn: "root" });
        return CacheService;
    }());
    if (false) {
        /** @type {?} */
        CacheService.prototype.cacheProbeTimestamp;
        /** @type {?} */
        CacheService.prototype.cacheProbePromises$;
        /** @type {?} */
        CacheService.prototype.objectLoadObservables$;
        /**
         * @type {?}
         * @protected
         */
        CacheService.prototype.router;
        /**
         * @type {?}
         * @protected
         */
        CacheService.prototype.api;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/pages/translate.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TranslateService = /** @class */ (function (_super) {
        __extends(TranslateService, _super);
        function TranslateService(router, api) {
            var _this = _super.call(this) || this;
            _this.router = router;
            _this.api = api;
            _this.defaultLanguage = 'en_GB';
            _this.refresh();
            return _this;
        }
        /**
         * @param {?} lang
         * @return {?}
         */
        TranslateService.prototype.getTranslation = /**
         * @param {?} lang
         * @return {?}
         */
        function (lang) {
            this.translations = this.getTranslations(lang).pipe(operators.map((/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                return response.data;
            })));
            return (/** @type {?} */ (this.translations));
        };
        /**
         * @return {?}
         */
        TranslateService.prototype.getLanguages = /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (this.objectLoadObservables$.languages) {
                return this.objectLoadObservables$.languages;
            }
            this.objectLoadObservables$.languages = new rxjs.ReplaySubject();
            this.api.get('languages').subscribe((/**
             * @param {?} languages
             * @return {?}
             */
            function (languages) {
                sessionStorage.setItem('languages', JSON.stringify(languages));
                _this.objectLoadObservables$.languages.next(languages);
            }));
            return this.objectLoadObservables$.languages;
        };
        /**
         * @param {?} languageCode
         * @return {?}
         */
        TranslateService.prototype.getTranslations = /**
         * @param {?} languageCode
         * @return {?}
         */
        function (languageCode) {
            // if (this.getFromStorage('translations:' + languageCode)) {
            //     const translations = JSON.parse(this.getFromStorage('translations:' + languageCode) as string);
            //     if (!translations.timestamp) {
            //         return this.loadTranslations(languageCode);
            //     }
            //     let observables = [];
            //     if (!this.cacheProbeTimestamp.translations) {
            //         if (!this.cacheProbePromises$.translations) {
            //             this.cacheProbePromises$.translations = this.restangular.one('cache-probe').get({q: 'translations'}).pipe(
            //                 map((response) => {
            //                     this.cacheProbeTimestamp.translations = response as number;
            //                 })
            //             );
            //         }
            //     }
            //     observables.push(this.cacheProbePromises$.translations);
            //     return forkJoin(observables).subscribe(response => {
            //         if (!this.cacheProbeTimestamp.translations || translation.timestamp < this.cacheProbeTimestamp.translations) {
            //             return this.loadTranslations(languageCode);
            //         }
            //
            //         return deferred.promise;
            //     });
            // }
            return this.loadTranslations(languageCode);
        };
        /**
         * @param {?} languageCode
         * @return {?}
         */
        TranslateService.prototype.loadTranslations = /**
         * @param {?} languageCode
         * @return {?}
         */
        function (languageCode) {
            var _this = this;
            if (this.objectLoadObservables$['translations:' + languageCode]) {
                return this.objectLoadObservables$['translations:' + languageCode];
            }
            this.objectLoadObservables$['translations:' + languageCode] = this.api.get('translations', {
                params: { languageCode: languageCode },
                observe: 'response'
            }).pipe(operators.map((/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                var e_1, _a;
                /** @type {?} */
                var newServiceInfo = JSON.parse(response.headers.get('x-service-info'));
                /** @type {?} */
                var translations = {
                    timestamp: _this.cacheProbeTimestamp.translations || newServiceInfo.timestamp,
                    data: {}
                };
                try {
                    for (var _b = __values((/** @type {?} */ (response.body))), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var translation = _c.value;
                        translations.data[translation.dataKey] = translation.dataValue;
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                try {
                    sessionStorage.setItem('translations:' + languageCode, JSON.stringify(translations));
                }
                catch (e) {
                }
                try {
                    localStorage.setItem('translations:' + languageCode, JSON.stringify(translations));
                }
                catch (e) {
                }
                return translations;
            })));
            return this.objectLoadObservables$['translations:' + languageCode];
        };
        /**
         * @return {?}
         */
        TranslateService.prototype.getDefaultLanguage = /**
         * @return {?}
         */
        function () {
            if (sessionStorage.getItem('defaultLanguage')) {
                return sessionStorage.getItem('defaultLanguage');
            }
            if (localStorage.getItem('defaultLanguage')) {
                return localStorage.getItem('defaultLanguage');
            }
            return this.defaultLanguage;
        };
        /**
         * @param {?} languageCode
         * @return {?}
         */
        TranslateService.prototype.setDefaultLanguage = /**
         * @param {?} languageCode
         * @return {?}
         */
        function (languageCode) {
            try {
                sessionStorage.setItem('defaultLanguage', languageCode);
                localStorage.setItem('defaultLanguage', languageCode);
            }
            catch (_a) {
            }
        };
        /**
         * @return {?}
         */
        TranslateService.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            console.log('Destroy translate');
        };
        TranslateService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        TranslateService.ctorParameters = function () { return [
            { type: router.Router },
            { type: http.HttpClient }
        ]; };
        /** @nocollapse */ TranslateService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function TranslateService_Factory() { return new TranslateService(core.ɵɵinject(router.Router), core.ɵɵinject(http.HttpClient)); }, token: TranslateService, providedIn: "root" });
        return TranslateService;
    }(CacheService));
    if (false) {
        /** @type {?} */
        TranslateService.prototype.translations;
        /** @type {?} */
        TranslateService.prototype.defaultLanguage;
        /**
         * @type {?}
         * @protected
         */
        TranslateService.prototype.router;
        /**
         * @type {?}
         * @protected
         */
        TranslateService.prototype.api;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/settings/settings.actions.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var toggleFixedHeader = store.createAction('[Settings] Toggle Fixed Header');
    /** @type {?} */
    var toggleFixedNavigation = store.createAction('[Settings] Toggle Fixed Navigation');
    /** @type {?} */
    var toggleMinifyNavigation = store.createAction('[Settings] Toggle Minify Navigation');
    /** @type {?} */
    var toggleHideNavigation = store.createAction('[Settings] Toggle Hide Navigation');
    /** @type {?} */
    var toggleTopNavigation = store.createAction('[Settings] Toggle Top Navigation');
    /** @type {?} */
    var toggleBoxedLayout = store.createAction('[Settings] Toggle Boxed Layout');
    /** @type {?} */
    var togglePushContent = store.createAction('[Settings] Toggle Push Content');
    /** @type {?} */
    var toggleNoOverlay = store.createAction('[Settings] Toggle No Overlay');
    /** @type {?} */
    var toggleOffCanvas = store.createAction('[Settings] Toggle Off Canvas');
    /** @type {?} */
    var toggleBiggerContentFont = store.createAction('[Settings] Toggle Bigger Content Font');
    /** @type {?} */
    var toggleHighContrastText = store.createAction('[Settings] Toggle High Contrast Text');
    /** @type {?} */
    var toggleDaltonism = store.createAction('[Settings] Toggle Daltonism');
    /** @type {?} */
    var toggleRtl = store.createAction('[Settings] Toggle RTL');
    /** @type {?} */
    var togglePreloaderInsise = store.createAction('[Settings] Toggle Preloader Insise');
    /** @type {?} */
    var toggleCleanPageBackground = store.createAction('[Settings] Toggle Clean Page Background');
    /** @type {?} */
    var toggleHideNavigationIcons = store.createAction('[Settings] Toggle Hide Navigation Icons');
    /** @type {?} */
    var toggleDisableCSSAnimation = store.createAction('[Settings] Toggle Disable CSS Animation');
    /** @type {?} */
    var toggleHideInfoCard = store.createAction('[Settings] Toggle Hide Info Card');
    /** @type {?} */
    var toggleLeanSubheader = store.createAction('[Settings] Toggle Lean Subheader');
    /** @type {?} */
    var toggleHierarchicalNavigation = store.createAction('[Settings] Toggle Hierarchical Navigation');
    /** @type {?} */
    var setGlobalFontSize = store.createAction('[Settings] Set Global Font Size', store.props());
    /** @type {?} */
    var appReset = store.createAction('[Settings] App Reset');
    /** @type {?} */
    var factoryReset = store.createAction('[Settings] Factory Reset');
    /** @type {?} */
    var SettingsActionTypes = [
        toggleFixedHeader.type,
        toggleFixedNavigation.type,
        toggleMinifyNavigation.type,
        toggleHideNavigation.type,
        toggleTopNavigation.type,
        toggleBoxedLayout.type,
        togglePushContent.type,
        toggleNoOverlay.type,
        toggleOffCanvas.type,
        toggleBiggerContentFont.type,
        toggleHighContrastText.type,
        toggleDaltonism.type,
        toggleRtl.type,
        togglePreloaderInsise.type,
        toggleCleanPageBackground.type,
        toggleHideNavigationIcons.type,
        toggleDisableCSSAnimation.type,
        toggleHideInfoCard.type,
        toggleLeanSubheader.type,
        toggleHierarchicalNavigation.type,
        setGlobalFontSize.type,
        appReset.type,
        factoryReset.type,
    ];

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/settings/settings.reducer.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function SettingsState() { }
    if (false) {
        /** @type {?} */
        SettingsState.prototype.fixedHeader;
        /** @type {?} */
        SettingsState.prototype.fixedNavigation;
        /** @type {?} */
        SettingsState.prototype.minifyNavigation;
        /** @type {?} */
        SettingsState.prototype.hideNavigation;
        /** @type {?} */
        SettingsState.prototype.topNavigation;
        /** @type {?} */
        SettingsState.prototype.boxedLayout;
        /** @type {?} */
        SettingsState.prototype.pushContent;
        /** @type {?} */
        SettingsState.prototype.noOverlay;
        /** @type {?} */
        SettingsState.prototype.offCanvas;
        /** @type {?} */
        SettingsState.prototype.biggerContentFont;
        /** @type {?} */
        SettingsState.prototype.highContrastText;
        /** @type {?} */
        SettingsState.prototype.daltonism;
        /** @type {?} */
        SettingsState.prototype.preloaderInside;
        /** @type {?} */
        SettingsState.prototype.rtl;
        /** @type {?} */
        SettingsState.prototype.cleanPageBackground;
        /** @type {?} */
        SettingsState.prototype.hideNavigationIcons;
        /** @type {?} */
        SettingsState.prototype.disableCSSAnimation;
        /** @type {?} */
        SettingsState.prototype.hideInfoCard;
        /** @type {?} */
        SettingsState.prototype.leanSubheader;
        /** @type {?} */
        SettingsState.prototype.hierarchicalNavigation;
        /** @type {?} */
        SettingsState.prototype.globalFontSize;
    }
    // here you can configure initial state of your app
    // for all your users
    /** @type {?} */
    var initialState = {
        // app layout
        fixedHeader: true,
        fixedNavigation: false,
        minifyNavigation: false,
        hideNavigation: false,
        topNavigation: false,
        boxedLayout: false,
        // mobile menu
        pushContent: false,
        noOverlay: false,
        offCanvas: false,
        // accessibility
        biggerContentFont: false,
        highContrastText: false,
        daltonism: false,
        preloaderInside: false,
        rtl: false,
        // global modifications
        cleanPageBackground: false,
        hideNavigationIcons: false,
        disableCSSAnimation: false,
        hideInfoCard: false,
        leanSubheader: false,
        hierarchicalNavigation: false,
        // global font size
        globalFontSize: 'md',
    };
    var ɵ0 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { fixedHeader: !state.fixedHeader })); }, ɵ1 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { fixedNavigation: !state.fixedNavigation })); }, ɵ2 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { minifyNavigation: !state.minifyNavigation })); }, ɵ3 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { hideNavigation: !state.hideNavigation })); }, ɵ4 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { topNavigation: !state.topNavigation })); }, ɵ5 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { boxedLayout: !state.boxedLayout })); }, ɵ6 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { pushContent: !state.pushContent })); }, ɵ7 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { noOverlay: !state.noOverlay })); }, ɵ8 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { offCanvas: !state.offCanvas })); }, ɵ9 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { biggerContentFont: !state.biggerContentFont })); }, ɵ10 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { highContrastText: !state.highContrastText })); }, ɵ11 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { daltonism: !state.daltonism })); }, ɵ12 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { rtl: !state.rtl })); }, ɵ13 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { preloaderInside: !state.preloaderInside })); }, ɵ14 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { cleanPageBackground: !state.cleanPageBackground })); }, ɵ15 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { hideNavigationIcons: !state.hideNavigationIcons })); }, ɵ16 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { disableCSSAnimation: !state.disableCSSAnimation })); }, ɵ17 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { hideInfoCard: !state.hideInfoCard })); }, ɵ18 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { leanSubheader: !state.leanSubheader })); }, ɵ19 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return (__assign({}, state, { hierarchicalNavigation: !state.hierarchicalNavigation })); }, ɵ20 = /**
     * @param {?} state
     * @param {?} action
     * @return {?}
     */
    function (state, action) { return (__assign({}, state, { globalFontSize: action.size })); }, ɵ21 = /**
     * @return {?}
     */
    function () { return (__assign({}, initialState)); };
    /** @type {?} */
    var settingsReducer = store.createReducer(initialState, store.on(toggleFixedHeader, (ɵ0)), store.on(toggleFixedNavigation, (ɵ1)), store.on(toggleMinifyNavigation, (ɵ2)), store.on(toggleHideNavigation, (ɵ3)), store.on(toggleTopNavigation, (ɵ4)), store.on(toggleBoxedLayout, (ɵ5)), store.on(togglePushContent, (ɵ6)), store.on(toggleNoOverlay, (ɵ7)), store.on(toggleOffCanvas, (ɵ8)), store.on(toggleBiggerContentFont, (ɵ9)), store.on(toggleHighContrastText, (ɵ10)), store.on(toggleDaltonism, (ɵ11)), store.on(toggleRtl, (ɵ12)), store.on(togglePreloaderInsise, (ɵ13)), store.on(toggleCleanPageBackground, (ɵ14)), store.on(toggleHideNavigationIcons, (ɵ15)), store.on(toggleDisableCSSAnimation, (ɵ16)), store.on(toggleHideInfoCard, (ɵ17)), store.on(toggleLeanSubheader, (ɵ18)), store.on(toggleHierarchicalNavigation, (ɵ19)), store.on(setGlobalFontSize, (ɵ20)), store.on(appReset, (ɵ21)));
    /**
     * @param {?} state
     * @param {?} action
     * @return {?}
     */
    function reducer(state, action) {
        return settingsReducer(state, action);
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/navigation/navigation.actions.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var toggleNavSection = store.createAction('[Navigation] Toggle Nav Section', store.props());
    /** @type {?} */
    var activeUrl = store.createAction('[Navigation] Active Url', store.props());
    /** @type {?} */
    var toggleNavigationFilter = store.createAction('[Navigation] Toggle Filter');
    /** @type {?} */
    var navigationFilter = store.createAction('[Navigation] Navigation Filter', store.props());
    /** @type {?} */
    var mobileNavigation = store.createAction('[Navigation] Mobile Navigation', store.props());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core.navigation.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NavigationItems = [];

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/navigation/navigation.reducer.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function NavigationItem() { }
    if (false) {
        /** @type {?} */
        NavigationItem.prototype.badge;
        /** @type {?} */
        NavigationItem.prototype.item;
        /** @type {?} */
        NavigationItem.prototype.name;
        /** @type {?} */
        NavigationItem.prototype.title;
        /** @type {?|undefined} */
        NavigationItem.prototype.icon;
        /** @type {?|undefined} */
        NavigationItem.prototype.tags;
        /** @type {?|undefined} */
        NavigationItem.prototype.routerLink;
        /** @type {?|undefined} */
        NavigationItem.prototype.url;
        /** @type {?|undefined} */
        NavigationItem.prototype.active;
        /** @type {?|undefined} */
        NavigationItem.prototype.open;
        /** @type {?|undefined} */
        NavigationItem.prototype.items;
        /** @type {?|undefined} */
        NavigationItem.prototype.matched;
        /** @type {?|undefined} */
        NavigationItem.prototype.navTitle;
    }
    /**
     * @record
     */
    function NavigationState() { }
    if (false) {
        /** @type {?} */
        NavigationState.prototype.items;
        /** @type {?} */
        NavigationState.prototype.total;
        /** @type {?} */
        NavigationState.prototype.filterActive;
        /** @type {?} */
        NavigationState.prototype.filterText;
        /** @type {?} */
        NavigationState.prototype.matched;
    }
    /** @type {?} */
    var initialState$1 = {
        items: decorateItems(NavigationItems),
        total: countTotal(NavigationItems),
        filterActive: false,
        filterText: '',
        matched: 0
    };
    var ɵ0$1 = /**
     * @param {?} state
     * @param {?} action
     * @return {?}
     */
    function (state, action) { return (__assign({}, state, { items: detectActiveItems(state.items, action.url) })); }, ɵ1$1 = /**
     * @param {?} state
     * @param {?} action
     * @return {?}
     */
    function (state, action) { return (__assign({}, state, { items: toggleItems(state.items, action.item) })); }, ɵ2$1 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) {
        if (state.filterActive) {
            return __assign({}, state, { filterActive: false, matched: 0, items: state.items.map((/**
                 * @param {?} _
                 * @return {?}
                 */
                function (_) { return (__assign({}, _, { matched: null })); })) });
        }
        else {
            /** @type {?} */
            var items = filterItems(state.items, state.filterText);
            return __assign({}, state, { filterActive: true, items: items, matched: countMatched(items) });
        }
    }, ɵ3$1 = /**
     * @param {?} state
     * @param {?} action
     * @return {?}
     */
    function (state, action) {
        /** @type {?} */
        var items = filterItems(state.items, action.text);
        return __assign({}, state, { filterText: action.text, items: items, matched: countMatched(items) });
    };
    /** @type {?} */
    var navigationReducer = store.createReducer(initialState$1, store.on(activeUrl, (ɵ0$1)), store.on(toggleNavSection, (ɵ1$1)), store.on(toggleNavigationFilter, (ɵ2$1)), store.on(navigationFilter, (ɵ3$1)));
    /**
     * @param {?} state
     * @param {?} action
     * @return {?}
     */
    function reducer$1(state, action) {
        return navigationReducer(state, action);
    }
    /**
     * @param {?} navItems
     * @return {?}
     */
    function decorateItems(navItems) {
        return navItems.map((/**
         * @param {?} navItem
         * @return {?}
         */
        function (navItem) {
            /** @type {?} */
            var item = __assign({}, navItem, { active: false, matched: null });
            if (navItem.items) {
                item.open = false;
                item.items = decorateItems(navItem.items);
            }
            item.navTitle = !navItem.items && !navItem.routerLink && !!navItem.title;
            return item;
        }));
    }
    /**
     * @param {?} navItems
     * @return {?}
     */
    function countTotal(navItems) {
        /** @type {?} */
        var total = navItems.length;
        navItems.filter((/**
         * @param {?} _
         * @return {?}
         */
        function (_) { return !!_.items; })).forEach((/**
         * @param {?} _
         * @return {?}
         */
        function (_) {
            total += countTotal(_.items);
        }));
        return total;
    }
    /**
     * @param {?} navItems
     * @return {?}
     */
    function countMatched(navItems) {
        /** @type {?} */
        var matched = navItems.filter((/**
         * @param {?} _
         * @return {?}
         */
        function (_) { return !!_.matched; })).length;
        navItems.filter((/**
         * @param {?} _
         * @return {?}
         */
        function (_) { return !!_.items; })).forEach((/**
         * @param {?} _
         * @return {?}
         */
        function (_) {
            matched += countMatched(_.items);
        }));
        return matched;
    }
    /**
     * @param {?} navItems
     * @param {?} activeUrl
     * @return {?}
     */
    function detectActiveItems(navItems, activeUrl) {
        return navItems.map((/**
         * @param {?} navItem
         * @return {?}
         */
        function (navItem) {
            /** @type {?} */
            var isActive = itemIsActive(navItem, activeUrl);
            /** @type {?} */
            var item = __assign({}, navItem, { active: isActive });
            if (navItem.items) {
                item.open = isActive;
                item.items = detectActiveItems(navItem.items, activeUrl);
            }
            return item;
        }));
    }
    /**
     * @param {?} item
     * @param {?} activeUrl
     * @return {?}
     */
    function itemIsActive(item, activeUrl) {
        if (item.routerLink === activeUrl) {
            return true;
        }
        else if (item.items) {
            return item.items.some((/**
             * @param {?} _
             * @return {?}
             */
            function (_) { return itemIsActive(_, activeUrl); }));
        }
        else {
            return false;
        }
    }
    /**
     * @param {?} navItems
     * @param {?} toggledItem
     * @return {?}
     */
    function toggleItems(navItems, toggledItem) {
        /** @type {?} */
        var isToggledItemLevel = navItems.some((/**
         * @param {?} _
         * @return {?}
         */
        function (_) { return _ === toggledItem; }));
        return navItems.map((/**
         * @param {?} navItem
         * @return {?}
         */
        function (navItem) {
            /** @type {?} */
            var item = __assign({}, navItem);
            if (isToggledItemLevel && item.items && navItem !== toggledItem) {
                item.open = false;
            }
            if (navItem === toggledItem) {
                item.open = !navItem.open;
            }
            if (navItem.items) {
                item.items = toggleItems(navItem.items, toggledItem);
            }
            return item;
        }));
    }
    /**
     * @param {?} navItems
     * @param {?} text
     * @return {?}
     */
    function filterItems(navItems, text) {
        return navItems.map((/**
         * @param {?} navItem
         * @return {?}
         */
        function (navItem) {
            /** @type {?} */
            var item = __assign({}, navItem);
            if (navItem.items) {
                item.matched = navItemMatch(navItem, text) || navItem.items.some((/**
                 * @param {?} _
                 * @return {?}
                 */
                function (_) { return navItemMatch(_, text); }));
                item.items = filterItems(navItem.items, text);
            }
            else {
                item.matched = navItemMatch(navItem, text);
            }
            return item;
        }));
    }
    /**
     * @param {?} item
     * @param {?} text
     * @return {?}
     */
    function navItemMatch(item, text) {
        return (!text.trim() || (item.tags && !!item.tags.match(new RegExp(".*" + text.trim() + ".*", 'gi'))));
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/router/router.reducer.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function RouterStateUrl() { }
    if (false) {
        /** @type {?} */
        RouterStateUrl.prototype.url;
        /** @type {?} */
        RouterStateUrl.prototype.queryParams;
        /** @type {?} */
        RouterStateUrl.prototype.params;
        /** @type {?} */
        RouterStateUrl.prototype.data;
    }
    /** @type {?} */
    var reducer$2 = routerStore.routerReducer;
    var CustomSerializer = /** @class */ (function () {
        function CustomSerializer() {
        }
        /**
         * @param {?} routerState
         * @return {?}
         */
        CustomSerializer.prototype.serialize = /**
         * @param {?} routerState
         * @return {?}
         */
        function (routerState) {
            var url = routerState.url;
            var queryParams = routerState.root.queryParams;
            /** @type {?} */
            var state = routerState.root;
            while (state.firstChild) {
                state = state.firstChild;
            }
            var params = state.params, data = state.data;
            return { url: url, queryParams: queryParams, params: params, data: data };
        };
        return CustomSerializer;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/utils.functions.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} scrollDuration
     * @return {?}
     */
    function scrollToTop(scrollDuration) {
        /** @type {?} */
        var cosParameter = window.scrollY / 2;
        /** @type {?} */
        var scrollCount = 0;
        /** @type {?} */
        var oldTimestamp = performance.now();
        /**
         * @param {?} newTimestamp
         * @return {?}
         */
        function step(newTimestamp) {
            scrollCount += Math.PI / (scrollDuration / (newTimestamp - oldTimestamp));
            if (scrollCount >= Math.PI) {
                window.scrollTo(0, 0);
            }
            if (window.scrollY === 0) {
                return;
            }
            window.scrollTo(0, Math.round(cosParameter + cosParameter * Math.cos(scrollCount)));
            oldTimestamp = newTimestamp;
            window.requestAnimationFrame(step);
        }
        window.requestAnimationFrame(step);
    }
    /* tslint:disable */
    /**
     * @return {?}
     */
    function toggleFullscreen() {
        if (!document.fullscreenElement /* Standard browsers */
            && !document['msFullscreenElement'] /* Internet Explorer */
            && !document['mozFullScreenElement'] /* Firefox */
            && !document['webkitFullscreenElement'] /* Chrome */) {
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            }
            else if (document.documentElement['msRequestFullscreen']) {
                document.documentElement['msRequestFullscreen']();
            }
            else if (document.documentElement['mozRequestFullScreen']) {
                document.documentElement['mozRequestFullScreen']();
            }
            else if (document.documentElement['webkitRequestFullscreen']) {
                document.documentElement['webkitRequestFullscreen'](Element['ALLOW_KEYBOARD_INPUT']);
            }
        }
        else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            }
            else if (document['msExitFullscreen']) {
                document['msExitFullscreen']();
            }
            else if (document['mozCancelFullScreen']) {
                document['mozCancelFullScreen']();
            }
            else if (document['webkitExitFullscreen']) {
                document['webkitExitFullscreen']();
            }
        }
    }
    /* tslint:enable */
    // conditionaly apply css class to target
    /**
     * @param {?} condition
     * @param {?} className
     * @param {?} el
     * @return {?}
     */
    function handleClassCondition(condition, className, el) {
        if (!condition && el.classList.contains(className)) {
            el.classList.remove(className);
        }
        if (condition && !el.classList.contains(className)) {
            el.classList.add(className);
        }
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/settings/settings.meta.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var html = document.querySelector('html');
    /** @type {?} */
    var body = document.querySelector('body');
    // meta reducer that applies layout classes based on settings reducer
    /**
     * @param {?} reducer
     * @return {?}
     */
    function settingsMetaReducer(reducer) {
        return (/**
         * @param {?} state
         * @param {?} action
         * @return {?}
         */
        function (state, action) {
            // build new state
            /** @type {?} */
            var result = reducer(state, action);
            // use our middleware only for INIT action and for Settings actions
            if (action.type === store.INIT || SettingsActionTypes.includes(action.type)) {
                handleCssClasses(result.settings, action);
            }
            // pass state into next chain
            return result;
        });
    }
    /**
     * @param {?} state
     * @param {?} action
     * @return {?}
     */
    function handleCssClasses(state, action) {
        handleClassCondition(state.fixedHeader, 'header-function-fixed', body);
        handleClassCondition(state.fixedNavigation, 'nav-function-fixed', body);
        handleClassCondition(state.minifyNavigation, 'nav-function-minify', body);
        handleClassCondition(state.hideNavigation, 'nav-function-hidden', body);
        handleClassCondition(state.topNavigation, 'nav-function-top', body);
        handleClassCondition(state.boxedLayout, 'mod-main-boxed', body);
        handleClassCondition(state.pushContent, 'nav-mobile-push', body);
        handleClassCondition(state.noOverlay, 'nav-mobile-no-overlay', body);
        handleClassCondition(state.offCanvas, 'nav-mobile-slide-out', body);
        handleClassCondition(state.biggerContentFont, 'mod-bigger-font', body);
        handleClassCondition(state.highContrastText, 'mod-high-contrast', body);
        handleClassCondition(state.daltonism, 'mod-color-blind', body);
        handleClassCondition(state.cleanPageBackground, 'mod-clean-page-bg', body);
        handleClassCondition(state.hideNavigationIcons, 'mod-hide-nav-icons', body);
        handleClassCondition(state.disableCSSAnimation, 'mod-disable-animation', body);
        handleClassCondition(state.hideInfoCard, 'mod-hide-info-card', body);
        handleClassCondition(state.leanSubheader, 'mod-lean-subheader', body);
        handleClassCondition(state.hierarchicalNavigation, 'mod-nav-link', body);
        handleClassCondition(state.globalFontSize === 'sm', 'root-text-sm', html);
        handleClassCondition(state.globalFontSize === 'md', 'root-text', html);
        handleClassCondition(state.globalFontSize === 'lg', 'root-text-lg', html);
        handleClassCondition(state.globalFontSize === 'xl', 'root-text-xl', html);
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/navigation/navigation.effects.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NavigationEffects = /** @class */ (function () {
        function NavigationEffects(actions$, breakpointObserver) {
            var _this = this;
            this.actions$ = actions$;
            this.mapToActiveUrl$ = effects$1.createEffect((/**
             * @return {?}
             */
            function () { return _this.actions$.pipe(effects$1.ofType(routerStore.ROUTER_NAVIGATED), operators.map((/**
             * @param {?} action
             * @return {?}
             */
            function (action) { return activeUrl({ url: action.payload.event.url }); })), operators.tap((/**
             * @param {?} action
             * @return {?}
             */
            function (action) { return handleClassCondition(false, 'mobile-nav-on', document.querySelector('body')); }))); }));
            this.mobileNavigation$ = effects$1.createEffect((/**
             * @return {?}
             */
            function () { return _this.actions$.pipe(effects$1.ofType(mobileNavigation), operators.tap((/**
             * @param {?} action
             * @return {?}
             */
            function (action) { return handleClassCondition(action.open, 'mobile-nav-on', document.querySelector('body')); }))); }), { dispatch: false });
            breakpointObserver.observe('(max-width: 600px)').subscribe((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                handleClassCondition(result.matches, 'mobile-view-activated', document.querySelector('body'));
            }));
        }
        NavigationEffects.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        NavigationEffects.ctorParameters = function () { return [
            { type: effects$1.Actions },
            { type: layout.BreakpointObserver }
        ]; };
        return NavigationEffects;
    }());
    if (false) {
        /** @type {?} */
        NavigationEffects.prototype.mapToActiveUrl$;
        /** @type {?} */
        NavigationEffects.prototype.mobileNavigation$;
        /**
         * @type {?}
         * @private
         */
        NavigationEffects.prototype.actions$;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/index.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function AppState() { }
    if (false) {
        /** @type {?} */
        AppState.prototype.settings;
        /** @type {?} */
        AppState.prototype.navigation;
        /** @type {?} */
        AppState.prototype.router;
    }
    /** @type {?} */
    var reducers = {
        settings: reducer,
        navigation: reducer$1,
        router: reducer$2
    };
    /**
     * @param {?} reducer
     * @return {?}
     */
    function localStorageSyncReducer(reducer) {
        return ngrxStoreLocalstorage.localStorageSync({
            keys: ['settings'],
            rehydrate: true,
        })(reducer);
    }
    /** @type {?} */
    var metaReducers = [
        localStorageSyncReducer,
        settingsMetaReducer
    ].concat([]);
    /** @type {?} */
    var effects = [
        NavigationEffects
    ];

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/core.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoreModule = /** @class */ (function () {
        function CoreModule(parentModule, http) {
            this.http = http;
            throwIfAlreadyLoaded(parentModule, 'CoreModule');
        }
        CoreModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [],
                        imports: [
                            common.CommonModule,
                            animations.BrowserAnimationsModule,
                            http.HttpClientModule,
                            store.StoreModule.forRoot(reducers, {
                                metaReducers: metaReducers,
                                runtimeChecks: {
                                    strictStateImmutability: false,
                                    strictActionImmutability: false,
                                    strictStateSerializability: false,
                                    strictActionSerializability: false,
                                },
                            }),
                            effects$1.EffectsModule.forRoot(__spread(effects)),
                            storeDevtools.StoreDevtoolsModule.instrument({
                                maxAge: 25, logOnly: environment.production,
                                actionsBlocklist: ['@ngrx/router*']
                            }),
                            routerStore.StoreRouterConnectingModule.forRoot(),
                            accordion.AccordionModule.forRoot(),
                            alert.AlertModule.forRoot(),
                            dropdown.BsDropdownModule.forRoot(),
                            buttons.ButtonsModule.forRoot(),
                            collapse.CollapseModule.forRoot(),
                            modal.ModalModule.forRoot(),
                            tooltip.TooltipModule.forRoot(),
                            tabs.TabsModule.forRoot(),
                            ngxBootstrap.PopoverModule.forRoot(),
                            DialogsModule,
                            ApiService,
                            angulartics2.Angulartics2Module.forRoot(),
                            core$1.TranslateModule.forRoot({
                                loader: {
                                    provide: core$1.TranslateLoader,
                                    useClass: TranslateService,
                                }
                            }),
                        ],
                        providers: [
                            {
                                provide: routerStore.RouterStateSerializer,
                                useClass: CustomSerializer
                            },
                            {
                                provide: http.HTTP_INTERCEPTORS,
                                useClass: APIInterceptor,
                                multi: true
                            },
                        ]
                    },] }
        ];
        /** @nocollapse */
        CoreModule.ctorParameters = function () { return [
            { type: CoreModule, decorators: [{ type: core.Optional }, { type: core.SkipSelf }] },
            { type: http.HttpClient }
        ]; };
        return CoreModule;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        CoreModule.prototype.http;
    }
    /**
     * @param {?} parentModule
     * @param {?} moduleName
     * @return {?}
     */
    function throwIfAlreadyLoaded(parentModule, moduleName) {
        if (parentModule) {
            throw new Error(moduleName + " has already been loaded. Import " + moduleName + " modules in the AppModule only.");
        }
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/pages/page.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PageService = /** @class */ (function (_super) {
        __extends(PageService, _super);
        function PageService(router, api) {
            var _this = _super.call(this, router, api) || this;
            _this.router = router;
            _this.api = api;
            return _this;
        }
        /**
         * @return {?}
         */
        PageService.prototype.refresh = /**
         * @return {?}
         */
        function () {
            _super.prototype.refresh.call(this);
            this.page = false;
            this.breadcrumbs = {};
        };
        /**
         * @param {?} page
         * @return {?}
         */
        PageService.prototype.setCurrentPage = /**
         * @param {?} page
         * @return {?}
         */
        function (page) {
            this.page = page;
        };
        /**
         * @return {?}
         */
        PageService.prototype.getCurrentPage = /**
         * @return {?}
         */
        function () {
            return this.page;
        };
        /**
         * @param {?=} headers
         * @return {?}
         */
        PageService.prototype.getAll = /**
         * @param {?=} headers
         * @return {?}
         */
        function (headers) {
            return this.getListThroughCache('pages', {}, headers || {});
        };
        /**
         * @param {?} identifier
         * @param {?=} blockInteraction
         * @param {?=} headers
         * @return {?}
         */
        PageService.prototype.getPage = /**
         * @param {?} identifier
         * @param {?=} blockInteraction
         * @param {?=} headers
         * @return {?}
         */
        function (identifier, blockInteraction, headers) {
            /** @type {?} */
            var cacheKey = this.createCacheKey(['page', identifier, headers]);
            if (!blockInteraction && this.objectLoadObservables$[cacheKey]) {
                this.api.get('page/' + identifier, { headers: headers || {} }).subscribe(); // Trigger interaction
                return this.objectLoadObservables$[cacheKey];
            }
            else if (blockInteraction) {
                headers = Object.assign({}, (headers || {}), { 'x-service-info': JSON.stringify({ blockInteraction: true }) });
            }
            this.objectLoadObservables$[cacheKey] = this.getOneThroughCache('page', identifier, headers, cacheKey);
            return this.objectLoadObservables$[cacheKey];
        };
        /**
         * @param {?=} headers
         * @return {?}
         */
        PageService.prototype.getPages = /**
         * @param {?=} headers
         * @return {?}
         */
        function (headers) {
            return this.getListThroughCache('pages', {}, headers || {});
        };
        /**
         * @param {?} blockId
         * @return {?}
         */
        PageService.prototype.getBlock = /**
         * @param {?} blockId
         * @return {?}
         */
        function (blockId) {
            var _this = this;
            try {
                /** @type {?} */
                var observables = [];
                if (this.getFromStorage('block:' + blockId) !== false) {
                    /** @type {?} */
                    var block_1 = JSON.parse((/** @type {?} */ (this.getFromStorage('block:' + blockId))));
                    if (!block_1.timestamp) {
                        return this.loadBlock(blockId);
                    }
                    if (!this.cacheProbeTimestamp.blocks) {
                        this.cacheProbePromises$.blocks = this.api.get('cache-probe', { params: { q: 'widget' } }).pipe(operators.map((/**
                         * @param {?} response
                         * @return {?}
                         */
                        function (response) {
                            _this.cacheProbeTimestamp.blocks = (/** @type {?} */ (response));
                        })));
                        observables.push(this.cacheProbePromises$.blocks);
                    }
                    return rxjs.forkJoin(observables).toPromise().then((/**
                     * @param {?} response
                     * @return {?}
                     */
                    function (response) {
                        if (!_this.cacheProbeTimestamp.blocks || block_1.timestamp < _this.cacheProbeTimestamp.blocks) {
                            return _this.loadBlock(blockId);
                        }
                        return block_1;
                    }));
                }
            }
            catch (err) {
            }
            return this.loadBlock(blockId);
        };
        /**
         * @param {?} blockId
         * @return {?}
         */
        PageService.prototype.loadBlock = /**
         * @param {?} blockId
         * @return {?}
         */
        function (blockId) {
            if (this.objectLoadObservables$['block:' + blockId]) {
                return this.objectLoadObservables$['block:' + blockId];
            }
            this.objectLoadObservables$['block:' + blockId] = this.api.get('widget/' + blockId)
                .pipe(operators.map((/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                /** @type {?} */
                var block = response;
                try {
                    localStorage.setItem('block:' + blockId, JSON.stringify(block));
                }
                catch (e) {
                }
                try {
                    sessionStorage.setItem('block:' + blockId, JSON.stringify(block));
                }
                catch (e) {
                }
                return block;
            }))).toPromise();
            return this.objectLoadObservables$['block:' + blockId];
        };
        /**
         * @param {?} blocks
         * @return {?}
         */
        PageService.prototype.sortBlocks = /**
         * @param {?} blocks
         * @return {?}
         */
        function (blocks) {
            var e_1, _a;
            blocks.content.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) { return (a.data.row < b.data.row) ? 1 : -1; }))
                .sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) { return (a.data.col < b.data.col) ? 1 : -1; }));
            try {
                for (var _b = __values(blocks.content), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var subBlocks = _c.value;
                    this.sortBlocks(subBlocks);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
        };
        /**
         * @param {?} requestedPageId
         * @return {?}
         */
        PageService.prototype.getBreadcrumbs = /**
         * @param {?} requestedPageId
         * @return {?}
         */
        function (requestedPageId) {
            var _this = this;
            return this.getPages().pipe(operators.map((/**
             * @param {?} pages
             * @return {?}
             */
            function (pages) {
                var e_2, _a;
                if (lodash.isEmpty(_this.breadcrumbs)) {
                    /** @type {?} */
                    var extractIds_1 = (/**
                     * @param {?} page
                     * @param {?} parents
                     * @return {?}
                     */
                    function (page, parents) {
                        var e_3, _a;
                        /** @type {?} */
                        var pageMin = lodash.omit(page, ['children', 'createdBy', 'dateCreated', 'displayOrder', 'groups',
                            'hidden', 'lastUpdated', 'permissions', 'tags', 'widgets']);
                        pageMin.parents = parents;
                        _this.breadcrumbs[page.id] = page;
                        try {
                            for (var _b = __values(page.children), _c = _b.next(); !_c.done; _c = _b.next()) {
                                var child = _c.value;
                                extractIds_1(child, parents.concat([pageMin]));
                            }
                        }
                        catch (e_3_1) { e_3 = { error: e_3_1 }; }
                        finally {
                            try {
                                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                            }
                            finally { if (e_3) throw e_3.error; }
                        }
                    });
                    try {
                        for (var pages_1 = __values(pages), pages_1_1 = pages_1.next(); !pages_1_1.done; pages_1_1 = pages_1.next()) {
                            var page = pages_1_1.value;
                            extractIds_1(page, []);
                        }
                    }
                    catch (e_2_1) { e_2 = { error: e_2_1 }; }
                    finally {
                        try {
                            if (pages_1_1 && !pages_1_1.done && (_a = pages_1.return)) _a.call(pages_1);
                        }
                        finally { if (e_2) throw e_2.error; }
                    }
                }
                return _this.breadcrumbs[requestedPageId] ? _this.breadcrumbs[requestedPageId] : [];
            })));
        };
        /**
         * @param {?} query
         * @return {?}
         */
        PageService.prototype.performSearch = /**
         * @param {?} query
         * @return {?}
         */
        function (query) {
            console.log(query);
        };
        PageService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        PageService.ctorParameters = function () { return [
            { type: router.Router },
            { type: http.HttpClient }
        ]; };
        /** @nocollapse */ PageService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function PageService_Factory() { return new PageService(core.ɵɵinject(router.Router), core.ɵɵinject(http.HttpClient)); }, token: PageService, providedIn: "root" });
        return PageService;
    }(CacheService));
    if (false) {
        /** @type {?} */
        PageService.prototype.page;
        /** @type {?} */
        PageService.prototype.breadcrumbs;
        /**
         * @type {?}
         * @protected
         */
        PageService.prototype.router;
        /**
         * @type {?}
         * @protected
         */
        PageService.prototype.api;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/auth/auth.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AuthService = /** @class */ (function () {
        function AuthService(router, api, angulartics, parser, cache, pages) {
            this.router = router;
            this.api = api;
            this.angulartics = angulartics;
            this.parser = parser;
            this.cache = cache;
            this.pages = pages;
            this.authChangeEmit = new rxjs.BehaviorSubject(true);
            this.failedLogins = 0;
            this.loginLimit = 4;
        }
        /**
         * @param {?} next
         * @param {?} state
         * @return {?}
         */
        AuthService.prototype.canActivate = /**
         * @param {?} next
         * @param {?} state
         * @return {?}
         */
        function (next, state) {
            if (!this.authResumed) {
                this.resume();
            }
            if (this.isAuthenticated()) {
                return true;
            }
            this.router.navigate(['/login']);
            return false;
        };
        /**
         * @return {?}
         */
        AuthService.prototype.isAuthenticated = /**
         * @return {?}
         */
        function () {
            return this.getUser() || this.cookiesExist();
        };
        /**
         * @return {?}
         */
        AuthService.prototype.getUser = /**
         * @return {?}
         */
        function () {
            return this.user;
        };
        /**
         * @param {?} username
         * @param {?} password
         * @param {?=} recaptchaResponse
         * @param {?=} authToken
         * @return {?}
         */
        AuthService.prototype.login = /**
         * @param {?} username
         * @param {?} password
         * @param {?=} recaptchaResponse
         * @param {?=} authToken
         * @return {?}
         */
        function (username, password, recaptchaResponse, authToken) {
            var _this = this;
            this.reset();
            /** @type {?} */
            var user = {
                username: username,
                password: password,
                recaptchaResponse: recaptchaResponse,
                authToken: authToken,
                mfa_token: this.mfaToken,
                google2step: this.google2step
            };
            return this.api.post('auth', user, { observe: 'response' })
                .pipe(operators.map((/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                _this.user = response.body;
                APP_CONFIG.authHeaders.xUserName = response.headers.get('x-service-user-name');
                APP_CONFIG.authHeaders.xUserToken = response.headers.get('x-service-user-token');
                document.cookie = 'x-service-user-name=' + response.headers.get('x-service-user-name') + ';path=/';
                document.cookie = 'x-service-user-token=' + response.headers.get('x-service-user-token') + ';path=/';
                if (response.headers.get('x-service-info')) {
                    /** @type {?} */
                    var serviceInfo = JSON.parse(response.headers.get('x-service-info'));
                    _this.thirdPartyOnline = serviceInfo.thirdPartyOnline || false;
                }
                return _this.initialise().subscribe((/**
                 * @return {?}
                 */
                function () {
                    _this.angulartics.eventTrack.next({
                        action: 'Auth succeeded',
                    });
                    return _this.router.navigate(['/']);
                }), (/**
                 * @param {?} resp
                 * @return {?}
                 */
                function (resp) {
                    _this.angulartics.eventTrack.next({
                        action: 'Auth failed',
                    });
                    if (_this.parser.getCookie('x-service-login-token')) {
                        if ((/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (_this.parser.getCookie('x-service-login-token'))))))))
                            > _this.failedLogins) {
                            _this.failedLogins = (/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (_this.parser.getCookie('x-service-login-token'))))))));
                        }
                    }
                    document.cookie = 'x-service-login-token=' + btoa((/** @type {?} */ ((/** @type {?} */ ((_this.failedLogins + 1)))))) + ';path=/';
                    if (_this.failedLogins >= _this.loginLimit) {
                        _this.recaptchaEnabled = true;
                    }
                    return rxjs.throwError('Failed to authenticate');
                }));
            })));
        };
        /**
         * @return {?}
         */
        AuthService.prototype.resume = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this.authResumed = true;
            if (this.parser.getCookie('x-service-user-name') || this.parser.getCookie('x-service-emulated-user-name')) {
                APP_CONFIG.authHeaders.xUserName = this.parser.getCookie('x-service-user-name');
                APP_CONFIG.authHeaders.xUserToken = this.parser.getCookie('x-service-user-token');
                this.isEmulated = this.parser.getCookie('x-service-emulated-user-name') !== false;
                if (this.parser.getCookie('x-service-emulated-user-name')) {
                    APP_CONFIG.authHeaders.xUserName = this.parser.getCookie('x-service-emulated-user-name');
                }
                return this.api.get('auth', { observe: 'response' }).toPromise().then((/**
                 * @param {?} response
                 * @return {?}
                 */
                function (response) {
                    _this.user = response.body;
                    if (response.headers.get('x-service-info')) {
                        /** @type {?} */
                        var serviceInfo = JSON.parse(response.headers.get('x-service-info'));
                        _this.thirdPartyOnline = serviceInfo.thirdPartyOnline || false;
                    }
                    _this.authChangeEmit.next(true);
                    return _this.initialise().subscribe();
                })).catch((/**
                 * @return {?}
                 */
                function () {
                    _this.reset();
                    return _this.router.navigate(['/login']);
                }));
            }
            else {
                if (this.parser.getCookie('x-service-login-token')) {
                    if ((/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))))
                        > this.failedLogins) {
                        this.failedLogins = (/** @type {?} */ ((/** @type {?} */ (atob((/** @type {?} */ (this.parser.getCookie('x-service-login-token'))))))));
                    }
                }
                if ((/** @type {?} */ (this.failedLogins)) >= this.loginLimit) {
                    this.recaptchaEnabled = true;
                }
            }
            return false;
        };
        /**
         * @private
         * @return {?}
         */
        AuthService.prototype.getServiceProvider = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            return this.cache.getOneThroughCache('service-provider', 2, {}, false).subscribe((/**
             * @param {?} provider
             * @return {?}
             */
            function (provider) {
                _this.serviceProvider = provider;
                if (_this.serviceProvider.status !== 'ACTIVE') {
                    rxjs.throwError('Service provider offline');
                    return _this.router.navigate(['/maintenance']);
                }
                _this.recaptchaEnabled = _this.serviceProvider.data.recaptchaEnabled;
            }));
        };
        /**
         * @return {?}
         */
        AuthService.prototype.initialise = /**
         * @return {?}
         */
        function () {
            var _this = this;
            /** @type {?} */
            var observables = [];
            if (this.authCheckTimer) {
                clearInterval(this.authCheckTimer);
            }
            // @TODO: await
            this.getServiceProvider();
            if (this.isAuthenticated()) {
                if (!this.analyticsLoaded) {
                    observables.push(this.api.get('analytics-custom-vars').toPromise().then((/**
                     * @param {?} response
                     * @return {?}
                     */
                    function (response) {
                        var e_1, _a, e_2, _b;
                        try {
                            for (var _c = __values((/** @type {?} */ (response))), _d = _c.next(); !_d.done; _d = _c.next()) {
                                var dimension = _d.value;
                                try {
                                    for (var _e = __values(Object.entries(dimension)), _f = _e.next(); !_f.done; _f = _e.next()) {
                                        var _g = __read(_f.value, 2), key = _g[0], value = _g[1];
                                        try {
                                            ga('set', key, value);
                                            if (key === 'dimension1') {
                                                try {
                                                    ga('set', 'userId', value);
                                                    if (gclog) {
                                                        gclog.setCustomAttribute('userId', value);
                                                    }
                                                }
                                                catch (e) {
                                                }
                                                _this.angulartics.eventTrack.next({
                                                    action: 'Analytics initialised',
                                                });
                                            }
                                        }
                                        catch (err) {
                                        }
                                    }
                                }
                                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                                finally {
                                    try {
                                        if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                                    }
                                    finally { if (e_2) throw e_2.error; }
                                }
                            }
                        }
                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                        finally {
                            try {
                                if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                            }
                            finally { if (e_1) throw e_1.error; }
                        }
                    })));
                }
                this.authCheckTimer = setInterval((/**
                 * @return {?}
                 */
                function () {
                    return _this.triggerPing();
                }), 2500);
            }
            return rxjs.forkJoin(observables);
        };
        /**
         * @return {?}
         */
        AuthService.prototype.cookiesExist = /**
         * @return {?}
         */
        function () {
            return this.parser.getCookie('x-service-user-name') !== false
                || this.parser.getCookie('x-service-emulated-user-name') !== false;
        };
        /**
         * @return {?}
         */
        AuthService.prototype.triggerPing = /**
         * @return {?}
         */
        function () {
            if (!this.cookiesExist()) {
                this.reset();
                return this.router.navigate(['/login']);
            }
        };
        /**
         * @return {?}
         */
        AuthService.prototype.reset = /**
         * @return {?}
         */
        function () {
            this.user = false;
            this.cache.refresh();
            this.pages.refresh();
            this.analyticsLoaded = false;
            this.thirdPartyOnline = false;
            this.parser.deleteCookie('x-service-user-name', '/');
            this.parser.deleteCookie('x-service-emulated-user-name', '/');
            this.parser.deleteCookie('x-service-user-token', '/');
            this.parser.deleteCookie('TPSESSION', '/');
            APP_CONFIG.authHeaders = Object.assign({}, APP_CONFIG.authHeaders, {
                xUserName: null,
                xUserToken: null,
            });
            this.authChangeEmit.next(true);
            if (this.authCheckTimer) {
                clearInterval(this.authCheckTimer);
            }
        };
        /**
         * @return {?}
         */
        AuthService.prototype.logout = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this.reset();
            this.router.navigate(['/login']);
            return this.api.delete('auth').subscribe((/**
             * @return {?}
             */
            function () {
                _this.angulartics.eventTrack.next({
                    action: 'Auth closed',
                });
            }), (/**
             * @return {?}
             */
            function () {
                _this.angulartics.eventTrack.next({
                    action: 'Auth closure failed',
                });
            }));
        };
        AuthService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] },
            { type: core.NgModule, args: [{
                        imports: [
                            ngxBootstrap.TabsModule,
                        ],
                    },] }
        ];
        /** @nocollapse */
        AuthService.ctorParameters = function () { return [
            { type: router.Router },
            { type: http.HttpClient },
            { type: angulartics2.Angulartics2 },
            { type: ParserService },
            { type: CacheService },
            { type: PageService }
        ]; };
        /** @nocollapse */ AuthService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function AuthService_Factory() { return new AuthService(core.ɵɵinject(router.Router), core.ɵɵinject(http.HttpClient), core.ɵɵinject(angulartics2.Angulartics2), core.ɵɵinject(ParserService), core.ɵɵinject(CacheService), core.ɵɵinject(PageService)); }, token: AuthService, providedIn: "root" });
        return AuthService;
    }());
    if (false) {
        /** @type {?} */
        AuthService.prototype.authChangeEmit;
        /** @type {?} */
        AuthService.prototype.authResumed;
        /** @type {?} */
        AuthService.prototype.user;
        /** @type {?} */
        AuthService.prototype.mfaToken;
        /** @type {?} */
        AuthService.prototype.google2step;
        /** @type {?} */
        AuthService.prototype.analyticsLoaded;
        /** @type {?} */
        AuthService.prototype.thirdPartyOnline;
        /** @type {?} */
        AuthService.prototype.recaptchaEnabled;
        /** @type {?} */
        AuthService.prototype.serviceProvider;
        /** @type {?} */
        AuthService.prototype.authCheckTimer;
        /** @type {?} */
        AuthService.prototype.isEmulated;
        /** @type {?} */
        AuthService.prototype.failedLogins;
        /** @type {?} */
        AuthService.prototype.loginLimit;
        /**
         * @type {?}
         * @private
         */
        AuthService.prototype.router;
        /**
         * @type {?}
         * @private
         */
        AuthService.prototype.api;
        /**
         * @type {?}
         * @private
         */
        AuthService.prototype.angulartics;
        /**
         * @type {?}
         * @private
         */
        AuthService.prototype.parser;
        /**
         * @type {?}
         * @private
         */
        AuthService.prototype.cache;
        /**
         * @type {?}
         * @private
         */
        AuthService.prototype.pages;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/spinner/spinner.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SpinnerService = /** @class */ (function () {
        function SpinnerService() {
            this.spinnerCache = new Set();
        }
        /**
         * @param {?} spinner
         * @return {?}
         */
        SpinnerService.prototype._register = /**
         * @param {?} spinner
         * @return {?}
         */
        function (spinner) {
            this.spinnerCache.add(spinner);
        };
        /**
         * @param {?} spinnerToRemove
         * @return {?}
         */
        SpinnerService.prototype._unregister = /**
         * @param {?} spinnerToRemove
         * @return {?}
         */
        function (spinnerToRemove) {
            var _this = this;
            this.spinnerCache.forEach((/**
             * @param {?} spinner
             * @return {?}
             */
            function (spinner) {
                if (spinner === spinnerToRemove) {
                    _this.spinnerCache.delete(spinner);
                }
            }));
        };
        /**
         * @param {?} spinnerGroup
         * @return {?}
         */
        SpinnerService.prototype._unregisterGroup = /**
         * @param {?} spinnerGroup
         * @return {?}
         */
        function (spinnerGroup) {
            var _this = this;
            this.spinnerCache.forEach((/**
             * @param {?} spinner
             * @return {?}
             */
            function (spinner) {
                if (spinner.group === spinnerGroup) {
                    _this.spinnerCache.delete(spinner);
                }
            }));
        };
        /**
         * @return {?}
         */
        SpinnerService.prototype._unregisterAll = /**
         * @return {?}
         */
        function () {
            this.spinnerCache.clear();
        };
        /**
         * @param {?} spinnerName
         * @return {?}
         */
        SpinnerService.prototype.show = /**
         * @param {?} spinnerName
         * @return {?}
         */
        function (spinnerName) {
            this.spinnerCache.forEach((/**
             * @param {?} spinner
             * @return {?}
             */
            function (spinner) {
                if (spinner.name === spinnerName) {
                    spinner.show = true;
                }
            }));
        };
        /**
         * @param {?} spinnerName
         * @return {?}
         */
        SpinnerService.prototype.hide = /**
         * @param {?} spinnerName
         * @return {?}
         */
        function (spinnerName) {
            this.spinnerCache.forEach((/**
             * @param {?} spinner
             * @return {?}
             */
            function (spinner) {
                if (spinner.name === spinnerName) {
                    spinner.show = false;
                }
            }));
        };
        /**
         * @param {?} spinnerGroup
         * @return {?}
         */
        SpinnerService.prototype.showGroup = /**
         * @param {?} spinnerGroup
         * @return {?}
         */
        function (spinnerGroup) {
            this.spinnerCache.forEach((/**
             * @param {?} spinner
             * @return {?}
             */
            function (spinner) {
                if (spinner.group === spinnerGroup) {
                    spinner.show = true;
                }
            }));
        };
        /**
         * @param {?} spinnerGroup
         * @return {?}
         */
        SpinnerService.prototype.hideGroup = /**
         * @param {?} spinnerGroup
         * @return {?}
         */
        function (spinnerGroup) {
            this.spinnerCache.forEach((/**
             * @param {?} spinner
             * @return {?}
             */
            function (spinner) {
                if (spinner.group === spinnerGroup) {
                    spinner.show = false;
                }
            }));
        };
        /**
         * @return {?}
         */
        SpinnerService.prototype.showAll = /**
         * @return {?}
         */
        function () {
            this.spinnerCache.forEach((/**
             * @param {?} spinner
             * @return {?}
             */
            function (spinner) { return spinner.show = true; }));
        };
        /**
         * @return {?}
         */
        SpinnerService.prototype.hideAll = /**
         * @return {?}
         */
        function () {
            this.spinnerCache.forEach((/**
             * @param {?} spinner
             * @return {?}
             */
            function (spinner) { return spinner.show = false; }));
        };
        /**
         * @param {?} spinnerName
         * @return {?}
         */
        SpinnerService.prototype.isShowing = /**
         * @param {?} spinnerName
         * @return {?}
         */
        function (spinnerName) {
            /** @type {?} */
            var showing = undefined;
            this.spinnerCache.forEach((/**
             * @param {?} spinner
             * @return {?}
             */
            function (spinner) {
                if (spinner.name === spinnerName) {
                    showing = spinner.show;
                }
            }));
            return showing;
        };
        SpinnerService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */ SpinnerService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function SpinnerService_Factory() { return new SpinnerService(); }, token: SpinnerService, providedIn: "root" });
        return SpinnerService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        SpinnerService.prototype.spinnerCache;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/auth/login/login.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoginComponent = /** @class */ (function () {
        function LoginComponent(authService, spinner) {
            this.authService = authService;
            this.spinner = spinner;
            this.loginForm = new forms.FormGroup({
                username: new forms.FormControl('', [forms.Validators.required, forms.Validators.email]),
                password: new forms.FormControl('', forms.Validators.required)
            });
            this.appName = APP_CONFIG.appName;
            this.copyright = APP_CONFIG.copyright;
        }
        /**
         * @return {?}
         */
        LoginComponent.prototype.login = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this.loading = true;
            this.errorMessage = false;
            this.spinner.show('login');
            return this.authService.login(this.loginForm.get('username').value, this.loginForm.get('password').value).pipe(operators.catchError((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                _this.spinner.hide('login');
                _this.loading = false;
                _this.errorMessage = "<p>Please check your email address and password are correct, and try again.</p>\n                                    <p>Error reference: " + error.headers.get('x-service-request-id') + "</small></p>";
                return rxjs.throwError("Error Code: " + error.status + "\\nMessage: " + error.message);
            }))).subscribe((/**
             * @return {?}
             */
            function () {
                _this.spinner.hide('login');
                _this.loading = false;
            }));
        };
        /**
         * @return {?}
         */
        LoginComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        LoginComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-login',
                        template: "<div class=\"page-wrapper\">\n    <div class=\"page-inner bg-brand-gradient\">\n        <div class=\"page-content-wrapper bg-transparent m-0\">\n            <div class=\"height-10 w-100 shadow-lg px-4 bg-brand-gradient\">\n                <div class=\"d-flex align-items-center container p-0\">\n                    <div class=\"page-logo width-mobile-auto m-0 align-items-center justify-content-center p-0 bg-transparent bg-img-none shadow-0 height-9\">\n                        <a href=\"javascript:void(0)\" class=\"page-logo-link press-scale-down d-flex align-items-center\">\n                            <span class=\"page-logo-text mr-1\">{{appName}}</span>\n                        </a>\n                    </div>\n                </div>\n            </div>\n            <div class=\"flex-1\"\n                 style=\"background: url(assets/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover;\">\n                <div class=\"container py-4 py-lg-5 my-lg-5 px-4 px-sm-0\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12 col-md-6 col-lg-5 col-xl-4 offset-xl-4 offset-md-3\">\n                            <h1 class=\"text-white fw-300 mb-3 d-sm-block d-md-none\">\n                                Secure login\n                            </h1>\n\n                            <div class=\"card p-4 rounded-plus bg-faded\">\n\n                                <form [formGroup]=\"loginForm\" (ngSubmit)=\"login()\">\n                                    <div class=\"form-group\">\n                                        <label class=\"form-label\">Username</label>\n                                        <input type=\"email\"\n                                               formControlName=\"username\"\n                                               name=\"username\"\n                                               class=\"form-control form-control-lg\"\n                                               required=\"true\"\n                                               [required]=\"true\"/>\n                                        <div class=\"invalid-feedback\">No, you missed this one.</div>\n                                        <div class=\"help-block\">Your username</div>\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label class=\"form-label\">Password</label>\n                                        <input type=\"password\"\n                                               formControlName=\"password\"\n                                               name=\"password\"\n                                               class=\"form-control form-control-lg\"\n                                               required=\"true\"\n                                               [required]=\"true\"/>\n                                        <div class=\"invalid-feedback\">Sorry, you missed this one.</div>\n                                        <div class=\"help-block\">Your password</div>\n                                    </div>\n\n                                    <div class=\"row no-gutters\">\n                                        <div class=\"col-lg-12 pl-lg-1 my-2\">\n                                            <button id=\"js-login-btn\" type=\"submit\"\n                                                    class=\"btn btn-primary btn-block btn-lg\"\n                                                    [class.spinner]=\"loading\"\n                                                    [disabled]=\"loading || !loginForm.valid\">\n                                                <core-spinner name=\"login\"></core-spinner>\n                                                <span>Secure Login</span>\n                                            </button>\n                                        </div>\n                                    </div>\n                                </form>\n\n                                <alert *ngIf=\"errorMessage\" class=\"alert alert-danger\" [innerHTML]=\"errorMessage\"></alert>\n\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"position-absolute pos-bottom pos-left pos-right p-3 text-center text-white\"\n                         [innerHTML]=\"copyright\"></div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"
                    }] }
        ];
        /** @nocollapse */
        LoginComponent.ctorParameters = function () { return [
            { type: AuthService },
            { type: SpinnerService }
        ]; };
        return LoginComponent;
    }());
    if (false) {
        /** @type {?} */
        LoginComponent.prototype.loading;
        /** @type {?} */
        LoginComponent.prototype.errorMessage;
        /** @type {?} */
        LoginComponent.prototype.loginForm;
        /** @type {?} */
        LoginComponent.prototype.appName;
        /** @type {?} */
        LoginComponent.prototype.copyright;
        /**
         * @type {?}
         * @protected
         */
        LoginComponent.prototype.authService;
        /**
         * @type {?}
         * @protected
         */
        LoginComponent.prototype.spinner;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/spinner/spinner.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SpinnerComponent = /** @class */ (function () {
        function SpinnerComponent(spinnerService) {
            this.spinnerService = spinnerService;
            this.isShowing = false;
            this.showChange = new core.EventEmitter();
        }
        Object.defineProperty(SpinnerComponent.prototype, "show", {
            get: /**
             * @return {?}
             */
            function () {
                return this.isShowing;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */
            function (val) {
                this.isShowing = val;
                this.showChange.emit(this.isShowing);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        SpinnerComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            if (!this.name) {
                throw new Error('Spinner must have a \'name\' attribute.');
            }
            this.spinnerService._register(this);
        };
        /**
         * @return {?}
         */
        SpinnerComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this.spinnerService._unregister(this);
        };
        SpinnerComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-spinner',
                        template: "\n        <mat-spinner color=\"primary\" diameter=\"30\" *ngIf=\"show\"></mat-spinner>\n    "
                    }] }
        ];
        /** @nocollapse */
        SpinnerComponent.ctorParameters = function () { return [
            { type: SpinnerService }
        ]; };
        SpinnerComponent.propDecorators = {
            name: [{ type: core.Input }],
            group: [{ type: core.Input }],
            loadingImage: [{ type: core.Input }],
            show: [{ type: core.Input }],
            showChange: [{ type: core.Output }]
        };
        return SpinnerComponent;
    }());
    if (false) {
        /** @type {?} */
        SpinnerComponent.prototype.name;
        /** @type {?} */
        SpinnerComponent.prototype.group;
        /** @type {?} */
        SpinnerComponent.prototype.loadingImage;
        /**
         * @type {?}
         * @private
         */
        SpinnerComponent.prototype.isShowing;
        /** @type {?} */
        SpinnerComponent.prototype.showChange;
        /**
         * @type {?}
         * @private
         */
        SpinnerComponent.prototype.spinnerService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/spinner/spinner.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SpinnerModule = /** @class */ (function () {
        function SpinnerModule() {
        }
        SpinnerModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [SpinnerComponent],
                        imports: [common.CommonModule, progressSpinner.MatProgressSpinnerModule],
                        exports: [SpinnerComponent],
                        providers: [SpinnerService]
                    },] }
        ];
        return SpinnerModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/systemMessage.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SystemMessageService = /** @class */ (function () {
        function SystemMessageService(systemMessageService) {
            this.systemMessageService = systemMessageService;
        }
        /**
         * @return {?}
         */
        SystemMessageService.prototype.clearInterceptMessage = /**
         * @return {?}
         */
        function () {
            this.interceptMessage = null;
        };
        /**
         * @param {?} message
         * @return {?}
         */
        SystemMessageService.prototype.setInterceptMessage = /**
         * @param {?} message
         * @return {?}
         */
        function (message) {
            this.interceptMessage = message;
        };
        /**
         * @return {?}
         */
        SystemMessageService.prototype.getInterceptMessage = /**
         * @return {?}
         */
        function () {
            return this.interceptMessage;
        };
        SystemMessageService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        SystemMessageService.ctorParameters = function () { return [
            { type: SystemMessageService }
        ]; };
        /** @nocollapse */ SystemMessageService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function SystemMessageService_Factory() { return new SystemMessageService(core.ɵɵinject(SystemMessageService)); }, token: SystemMessageService, providedIn: "root" });
        return SystemMessageService;
    }());
    if (false) {
        /** @type {?} */
        SystemMessageService.prototype.interceptMessage;
        /**
         * @type {?}
         * @private
         */
        SystemMessageService.prototype.systemMessageService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/auth/mfa-modal/mfa-modal.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MfaModalComponent = /** @class */ (function () {
        function MfaModalComponent(bsModalRef, messages, api, router) {
            this.bsModalRef = bsModalRef;
            this.messages = messages;
            this.api = api;
            this.router = router;
            this.mfaToken = new forms.FormControl('');
            this.mfaToken2 = new forms.FormControl('');
            this.googleSecret = new forms.FormControl('');
            this.secondaryUsername = new forms.FormControl('');
            this.secondaryPassword = new forms.FormControl('');
            this.mobile = new forms.FormControl('');
            this.mfa = {
                type: 'google',
                state: 418,
            };
        }
        /**
         * @return {?}
         */
        MfaModalComponent.prototype.submit = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var newData = Object.assign({}, this.interceptedResponse.response.request.body, {
                mfa_token: this.mfaToken.value,
                mfa_token2: this.mfaToken2.value,
                mfaType: this.mfa.type,
                google2step: this.getState() === 419,
                googleSecret: this.googleSecret.value,
                secondary_username: this.secondaryUsername.value,
                secondary_password: this.secondaryPassword.value,
                mobile: this.mobile.value
            });
            /** @type {?} */
            var responseHash = this.router.url + JSON.stringify(newData) + APP_CONFIG.authHeaders.xUserToken;
            /** @type {?} */
            var newHeaders = this.interceptedResponse.response.request.headers.set('x-service-request-hash', sha1(responseHash));
            /** @type {?} */
            var newRequest = this.interceptedResponse.response.request.clone({
                headers: newHeaders,
                body: newData,
                observe: (/** @type {?} */ ('response')),
            });
            console.log(newRequest);
            // return this.api.post('auth')(newRequest.body, null, {
            //         headers: newHeaders,
            //         // @TODO: Headers not set
            //     }).subscribe(
            //         this.interceptedResponse.subject,
            //         response => {
            //             this.mfa.mfaResponse = response.headers.get('x-service-mfa');
            //             this.setState(response.status);
            //
            //             switch (this.getState()) {
            //                 // case 417:
            //                 //     $rootScope.successMessage = {text: response.data};
            //                 //     $modalInstance.close();
            //                 //     break;
            //                 case 418:
            //                     return this._messages.setInterceptMessage('Invalid Authentication Code');
            //                 // case 419:
            //                 //     $rootScope.interceptMessage = {text: 'Enable Multi-Factor Authentication'};
            //                 //     $('div.modal-lg').addClass('modal-lg').removeClass('modal-sm');
            //                 //     break;
            //                 // case 421:
            //                 //     $rootScope.passwordPolicy = {
            //                 //         response: response,
            //                 //         deferred: $rootScope.twoStep.deferred
            //                 //     };
            //                 //     $rootScope.interceptMessage = {text: "Your password does not comply with the latest Core password policy, enforced in September 2019. Please update your password to ensure the utmost security of this application."};
            //                 //     $injector.invoke(['passwordPolicyModal', function (passwordPolicyModal) {
            //                 //         passwordPolicyModal.show();
            //                 //     }]);
            //                 //     return false;
            //             }
            //
            //         }
            //     );
        };
        /**
         * @return {?}
         */
        MfaModalComponent.prototype.getInterceptMessage = /**
         * @return {?}
         */
        function () {
            return this.messages.getInterceptMessage();
        };
        /**
         * @param {?} $event
         * @return {?}
         */
        MfaModalComponent.prototype.closeModal = /**
         * @param {?} $event
         * @return {?}
         */
        function ($event) {
            this.bsModalRef.hide();
        };
        /**
         * @param {?} type
         * @return {?}
         */
        MfaModalComponent.prototype.setType = /**
         * @param {?} type
         * @return {?}
         */
        function (type) {
            this.mfa.type = type;
        };
        /**
         * @return {?}
         */
        MfaModalComponent.prototype.getType = /**
         * @return {?}
         */
        function () {
            return this.mfa.type;
        };
        /**
         * @param {?} state
         * @return {?}
         */
        MfaModalComponent.prototype.setState = /**
         * @param {?} state
         * @return {?}
         */
        function (state) {
            this.mfa.state = state;
        };
        /**
         * @return {?}
         */
        MfaModalComponent.prototype.getState = /**
         * @return {?}
         */
        function () {
            return this.mfa.state;
        };
        /**
         * @param {?} google
         * @return {?}
         */
        MfaModalComponent.prototype.isMFAEnabled = /**
         * @param {?} google
         * @return {?}
         */
        function (google) {
            return false;
        };
        MfaModalComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-mfa-modal',
                        template: "<div class=\"modal-header\">\r\n    <h3 class=\"modal-title\">Multi-Factor Authentication</h3>\r\n</div>\r\n<div class=\"modal-content\">\r\n    <div class=\"modal-body\">\r\n        <div class=\"modal-body\">\r\n\r\n            <div (show)=\"getInterceptMessage() && getState() !== 421\">\r\n                <div class=\"new-item alert alert-danger\">\r\n                    <span class=\"fal fa-exclamation-circle\"></span>\r\n                    {{getInterceptMessage()}}\r\n                </div>\r\n            </div>\r\n\r\n            <div [ngSwitch]=\"mfa.state\">\r\n                <div class=\"form-group\" *ngSwitchCase=\"419\">\r\n\r\n                    <tabset>\r\n\r\n                        <tab heading=\"Google Authenticator Setup\" (click)=\"setType('google')\"\r\n                             *ngIf=\"!isMFAEnabled('google')\">\r\n                            <div class=\"box box-default\">\r\n                                <div class=\"box-body\">\r\n\r\n                                    <p>To setup Multi-Factor Authentication with Google, you will need to install the\r\n                                        free\r\n                                        Google Authenticator application on your mobile device, adding an additional\r\n                                        layer\r\n                                        of\r\n                                        security to this application. Please select your mobile phone operating system\r\n                                        below:</p>\r\n\r\n                                    <tabset>\r\n                                        <tab heading=\"Apple iOS\">\r\n                                            <div class=\"box box-default\">\r\n                                                <div class=\"box-body\">\r\n                                                    <h4>Downloading the app</h4>\r\n                                                    <ol>\r\n                                                        <li>Visit the iTunes App Store</li>\r\n                                                        <li>Search for Google Authenticator</li>\r\n                                                        <li>Download and install the application</li>\r\n                                                    </ol>\r\n\r\n                                                    <h4>Setting up the app</h4>\r\n                                                    <ol>\r\n                                                        <li>On your phone, open the Google Authenticator application\r\n                                                        </li>\r\n                                                        <li>Tap the plus icon</li>\r\n                                                        <li>You can add your account using the QR code below or\r\n                                                            manually:\r\n                                                            <ul>\r\n                                                                <li>Using Barcode: Tap \"Scan Barcode\" and then point\r\n                                                                    your\r\n                                                                    camera\r\n                                                                    at the QR code below\r\n                                                                </li>\r\n                                                                <li>Using Manual Entry: Tap \"Manual Entry\" and enter the\r\n                                                                    secret\r\n                                                                    key:\r\n                                                                    <!--                                                                <strong>{{twoStep.googleSecret}}</strong></li>-->\r\n                                                            </ul>\r\n                                                        </li>\r\n                                                        <li>The first response code will now show in your application,\r\n                                                            enter\r\n                                                            it\r\n                                                            into the box below to complete setup\r\n                                                        </li>\r\n                                                    </ol>\r\n                                                </div>\r\n                                            </div>\r\n                                        </tab>\r\n                                        <tab heading=\"Google Android\">\r\n                                            <div class=\"box box-default\">\r\n                                                <div class=\"box-body\">\r\n                                                    <h4>Downloading the app</h4>\r\n                                                    <ol>\r\n                                                        <li>Visit Google Play</li>\r\n                                                        <li>Search for Google Authenticator</li>\r\n                                                        <li>Download and install the application</li>\r\n                                                    </ol>\r\n\r\n                                                    <h4>Setting up the app</h4>\r\n                                                    <ol>\r\n                                                        <li>On your phone, open the Google Authenticator application\r\n                                                        </li>\r\n                                                        <li>If this is the first time you have used Authenticator, click\r\n                                                            the\r\n                                                            Add\r\n                                                            an account button. If you are adding a new account, choose\r\n                                                            \u201CAdd\r\n                                                            an\r\n                                                            account\u201D from the app\u2019s menu\r\n                                                        </li>\r\n                                                        <li>You can add your account using the QR code below or\r\n                                                            manually:\r\n                                                            <ul>\r\n                                                                <li>Using Barcode: Tap \"Scan Barcode\". If the\r\n                                                                    Authenticator\r\n                                                                    app\r\n                                                                    cannot locate a barcode scanner app on your phone,\r\n                                                                    you\r\n                                                                    might\r\n                                                                    be prompted to download and install one,\r\n                                                                    alternatively\r\n                                                                    use\r\n                                                                    the manual entry option below. If you want to\r\n                                                                    install a\r\n                                                                    barcode scanner app so you can complete the setup\r\n                                                                    process,\r\n                                                                    click install and then point your camera at the QR\r\n                                                                    code\r\n                                                                    below\r\n                                                                </li>\r\n                                                                <li>Using Manual Entry: Tap \"Manual Entry\" and enter the\r\n                                                                    secret\r\n                                                                    key:\r\n                                                                    <!--                                                                <strong>{{twoStep.googleSecret}}</strong><br/>Make sure-->\r\n                                                                    you've chosen to make the key Time based and press\r\n                                                                    'Save'\r\n                                                                </li>\r\n                                                            </ul>\r\n                                                        </li>\r\n                                                        <li>The first response code will now show in your application,\r\n                                                            enter\r\n                                                            it\r\n                                                            into the box below to complete setup\r\n                                                        </li>\r\n                                                    </ol>\r\n                                                </div>\r\n                                            </div>\r\n                                        </tab>\r\n                                        <tab heading=\"Microsoft Windows\">\r\n                                            <div class=\"box box-default\">\r\n                                                <div class=\"box-body\">\r\n                                                    <h4>Downloading the app</h4>\r\n                                                    <ol>\r\n                                                        <li>Visit the Windows App Store</li>\r\n                                                        <li>Search for Microsoft Authenticator</li>\r\n                                                        <li>Download and install the application</li>\r\n                                                    </ol>\r\n\r\n                                                    <h4>Setting up the app</h4>\r\n                                                    <ol>\r\n                                                        <li>On your phone, open the Microsoft Authenticator\r\n                                                            application\r\n                                                        </li>\r\n                                                        <li>Tap the Add (+) icon</li>\r\n                                                        <li>You can add your account using the QR code below or\r\n                                                            manually:\r\n                                                            <ul>\r\n                                                                <li>Using Barcode: Tap \"Scan Barcode\" and then point\r\n                                                                    your\r\n                                                                    camera\r\n                                                                    at the QR code below\r\n                                                                </li>\r\n                                                                <li>Using Manual Entry: Tap \"Manual Entry\" and enter the\r\n                                                                    secret\r\n                                                                    key:\r\n                                                                    <!--                                                                <strong>{{twoStep.googleSecret}}</strong></li>-->\r\n                                                            </ul>\r\n                                                        </li>\r\n                                                        <li>The first response code will now show in your application,\r\n                                                            enter\r\n                                                            it\r\n                                                            into the box below to complete setup\r\n                                                        </li>\r\n                                                    </ol>\r\n                                                </div>\r\n                                            </div>\r\n                                        </tab>\r\n                                    </tabset>\r\n\r\n                                    <p>This setup process needs to completed only once, subsequent access to this\r\n                                        application\r\n                                        will request the latest code shown in your authenticator application to be\r\n                                        entered\r\n                                        to\r\n                                        ensure maximum application security.</p>\r\n\r\n                                    <div class=\"input-group text-center\" style=\"width:100%; margin:14px 0;\">\r\n                                        <!--                                    <img ng-src=\"{{twoStep.qrcode}}\" alt=\"QR Code\"/>-->\r\n                                    </div>\r\n\r\n                                    <div class=\"row\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter Authentication Code</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"6\" name=\"mfa_token\"\r\n                                                       ng-model=\"twoStep.mfa_token\" class=\"form-control input-lg\"\r\n                                                       autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                                                       ng-required=\"twoStep.mfaType === 'google'\"/>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <div class=\"row\" ng-if=\"isMFAEnabled('sms')\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter your SMS Authentication Code</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"13\" name=\"mfa_token2\"\r\n                                                       ng-model=\"twoStep.mfa_token2\" class=\"form-control input-lg\"\r\n                                                       autocomplete=\"off\" style=\"letter-spacing: 15px;\"\r\n                                                       ng-required=\"twoStep.mfaType === 'google'\"/>\r\n                                            </div>\r\n                                            <small class=\"pull-right\">This is required to validate setup</small>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n                            </div>\r\n                        </tab>\r\n\r\n                        <tab heading=\"SMS Setup\" ng-click=\"twoStep.mfaType = 'sms'\" ng-if=\"!isMFAEnabled('sms')\">\r\n                            <div class=\"box box-default\">\r\n                                <div class=\"box-body\">\r\n\r\n                                    <p>To setup Multi-Factor Authentication with SMS, you will need you to enter your\r\n                                        mobile\r\n                                        phone number with country code.</p>\r\n\r\n                                    <div class=\"row\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter your mobile phone number</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-phone\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"13\" name=\"mobile\"\r\n                                                       ng-model=\"twoStep.mobile\"\r\n                                                       class=\"form-control input-lg\" autocomplete=\"off\"\r\n                                                       style=\"letter-spacing: 15px;\" placeholder=\"+44\" auto-focus\r\n                                                       ng-required=\"twoStep.mfaType === 'sms'\"/>\r\n                                            </div>\r\n                                            <small class=\"pull-right\">E.g. +44771234567</small>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <div class=\"row\" ng-if=\"isMFAEnabled('google')\">\r\n                                        <div class=\"form-group col-sm-6 col-sm-offset-3\">\r\n                                            <label>Enter your Google Authentication Code</label>\r\n                                            <div class=\"input-group\">\r\n                                                <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                                                <input type=\"text\" maxlength=\"6\" name=\"mfa_token2\"\r\n                                                       ng-model=\"twoStep.mfa_token2\" class=\"form-control input-lg\"\r\n                                                       autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                                                       ng-required=\"twoStep.mfaType === 'sms'\"/>\r\n                                            </div>\r\n                                            <small class=\"pull-right\">This is required to validate setup</small>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n                            </div>\r\n                        </tab>\r\n\r\n                    </tabset>\r\n\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngSwitchCase=\"418\">\r\n                    <div class=\"input-group\" *ngIf=\"mfa.type === 'google'\">\r\n                        <div class=\"input-group-prepend\"><i class=\"fal fa-lock\"></i></div>\r\n                        <input type=\"text\" maxlength=\"6\" [formControl]=\"mfaToken\"\r\n                               class=\"form-control form-control-lg\" autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                               [required]=\"true\"/>\r\n                    </div>\r\n\r\n                    <div *ngIf=\"mfa.type === 'sms'\">\r\n                        <p class=\"small\">An SMS with a 6-digit verification code has been sent to\r\n                            <!--                                                    {{mfa.mfaDetails.number}}-->\r\n                        </p>\r\n                        <div class=\"input-group\">\r\n                            <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                            <input type=\"text\" maxlength=\"6\" name=\"mfaToken\" ngModel\r\n                                   class=\"form-control input-lg\" autocomplete=\"off\" style=\"letter-spacing: 20px;\"\r\n                                   [required]=\"true\"/>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <a class=\"pull-right small\" href=\"javascript:void(0)\" (click)=\"setState(421)\">More\r\n                        options</a>\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngSwitchCase=\"420\">\r\n                    <div class=\"input-group\">\r\n                        <div class=\"input-group-addon\"><i class=\"fal fa-lock\"></i></div>\r\n                        <input type=\"text\" name=\"username\" placeholder=\"Username\" ng-model=\"twoStep.secondary_username\"\r\n                               class=\"form-control input-lg\" autocomplete=\"off\" readonly\r\n                               onfocus=\"this.removeAttribute('readonly');\" ng-required=\"true\"/>\r\n                        <input type=\"password\" placeholder=\"Password\" ng-model=\"twoStep.secondary_password\"\r\n                               class=\"form-control input-lg\" autocomplete=\"off\" readonly\r\n                               onfocus=\"this.removeAttribute('readonly');\" ng-required=\"true\"/>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngSwitchCase=\"421\">\r\n                    <h4>Try another way to sign in</h4>\r\n                    <ul class=\"list-group\">\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFAType('google')\"\r\n                            ng-show=\"isMFAEnabled('google')\">\r\n                            <i class=\"fal pull-left ga\"></i>Get a verification code from the\r\n                            <strong>Google Authenticator</strong> app\r\n                        </li>\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFATypeSetup('google')\"\r\n                            ng-show=\"!isMFAEnabled('google')\">\r\n                            <i class=\"fal pull-left ga\"></i>Setup <strong>Google Authenticator</strong>\r\n                        </li>\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFAType('sms')\"\r\n                            ng-show=\"isMFAEnabled('sms')\">\r\n                            <!--                        <i class=\"fal pull-left sms\"></i>Get an SMS verification code at {{twoStep.mfaDetails.number}}-->\r\n                        </li>\r\n                        <li class=\"list-group-item two-step\" ng-click=\"selectMFATypeSetup('sms')\"\r\n                            ng-show=\"!isMFAEnabled('sms')\">\r\n                            <i class=\"fal pull-left sms\"></i>Setup SMS verification\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"modal-footer\" (show)=\"getInterceptMessage() && getState() !== 421\">\r\n            <button class=\"btn btn-warning pull-right\" (click)=\"submit()\" ng-disabled=\"mfaForm.$invalid\">Submit</button>\r\n        </div>\r\n\r\n    </div>\r\n</div>"
                    }] }
        ];
        /** @nocollapse */
        MfaModalComponent.ctorParameters = function () { return [
            { type: modal.BsModalRef },
            { type: SystemMessageService },
            { type: http.HttpClient },
            { type: router.Router }
        ]; };
        return MfaModalComponent;
    }());
    if (false) {
        /** @type {?} */
        MfaModalComponent.prototype.mfaToken;
        /** @type {?} */
        MfaModalComponent.prototype.mfaToken2;
        /** @type {?} */
        MfaModalComponent.prototype.googleSecret;
        /** @type {?} */
        MfaModalComponent.prototype.secondaryUsername;
        /** @type {?} */
        MfaModalComponent.prototype.secondaryPassword;
        /** @type {?} */
        MfaModalComponent.prototype.mobile;
        /** @type {?} */
        MfaModalComponent.prototype.interceptedResponse;
        /** @type {?} */
        MfaModalComponent.prototype.mfa;
        /** @type {?} */
        MfaModalComponent.prototype.bsModalRef;
        /**
         * @type {?}
         * @private
         */
        MfaModalComponent.prototype.messages;
        /**
         * @type {?}
         * @private
         */
        MfaModalComponent.prototype.api;
        /**
         * @type {?}
         * @private
         */
        MfaModalComponent.prototype.router;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/auth/auth.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ɵ0$2 = { breadcrumbs: ['Login'] };
    var AuthModule = /** @class */ (function () {
        function AuthModule() {
        }
        AuthModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [LoginComponent, MfaModalComponent],
                        imports: [
                            common.CommonModule,
                            router.RouterModule.forChild([
                                { path: '', pathMatch: 'full', redirectTo: 'login' },
                                {
                                    path: 'login', component: LoginComponent,
                                    data: ɵ0$2
                                },
                            ]),
                            forms.FormsModule,
                            forms.ReactiveFormsModule,
                            progressBar.MatProgressBarModule,
                            progressSpinner.MatProgressSpinnerModule,
                            button.MatButtonModule,
                            icon.MatIconModule,
                            SpinnerModule,
                            ngxBootstrap.AlertModule,
                            ngxBootstrap.TabsModule
                        ]
                    },] }
        ];
        return AuthModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/block/block-block.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var Block = /** @class */ (function () {
        function Block(component) {
            this.component = component;
        }
        return Block;
    }());
    if (false) {
        /** @type {?} */
        Block.prototype.id;
        /** @type {?} */
        Block.prototype.identifier;
        /** @type {?} */
        Block.prototype.widgetId;
        /** @type {?} */
        Block.prototype.content;
        /** @type {?} */
        Block.prototype.uid;
        /** @type {?} */
        Block.prototype.data;
        /** @type {?} */
        Block.prototype.component;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-compile.directive.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockCompileDirective = /** @class */ (function () {
        function BlockCompileDirective(vcRef, compiler) {
            this.vcRef = vcRef;
            this.compiler = compiler;
        }
        /**
         * @return {?}
         */
        BlockCompileDirective.prototype.ngOnChanges = /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (!this.coreBlockCompile) {
                if (this.compRef) {
                    this.updateProperties();
                    return;
                }
                throw Error('Template not specified');
            }
            this.vcRef.clear();
            this.compRef = null;
            /** @type {?} */
            var component = this.createDynamicComponent(this.coreBlockCompile);
            /** @type {?} */
            var module = this.createDynamicModule(component);
            this.compiler.compileModuleAndAllComponentsAsync(module)
                .then((/**
             * @param {?} moduleWithFactories
             * @return {?}
             */
            function (moduleWithFactories) {
                /** @type {?} */
                var compFactory = moduleWithFactories.componentFactories.find((/**
                 * @param {?} a
                 * @return {?}
                 */
                function (a) { return a.componentType === component; }));
                _this.compRef = _this.vcRef.createComponent(compFactory);
                _this.updateProperties();
            }));
        };
        /**
         * @return {?}
         */
        BlockCompileDirective.prototype.updateProperties = /**
         * @return {?}
         */
        function () {
            var e_1, _a;
            try {
                for (var _b = __values(Object.values(this.coreBlockCompileContext)), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var prop = _c.value;
                    this.compRef.instance[prop] = this.coreBlockCompileContext[prop];
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
        };
        /**
         * @private
         * @param {?} template
         * @return {?}
         */
        BlockCompileDirective.prototype.createDynamicComponent = /**
         * @private
         * @param {?} template
         * @return {?}
         */
        function (template) {
            var DynamicComponent = /** @class */ (function () {
                function DynamicComponent(authService) {
                    this.authService = authService;
                }
                /**
                 * @return {?}
                 */
                DynamicComponent.prototype.ngOnInit = /**
                 * @return {?}
                 */
                function () {
                    this.user = this.authService.getUser();
                };
                DynamicComponent.decorators = [
                    { type: core.Component, args: [{
                                selector: 'core-dynamic-block',
                                template: template,
                            },] },
                ];
                /** @nocollapse */
                DynamicComponent.ctorParameters = function () { return [
                    { type: AuthService }
                ]; };
                return DynamicComponent;
            }());
            if (false) {
                /** @type {?} */
                DynamicComponent.prototype.user;
                /**
                 * @type {?}
                 * @protected
                 */
                DynamicComponent.prototype.authService;
            }
            return DynamicComponent;
        };
        /**
         * @private
         * @param {?} component
         * @return {?}
         */
        BlockCompileDirective.prototype.createDynamicModule = /**
         * @private
         * @param {?} component
         * @return {?}
         */
        function (component) {
            var DynamicModule = /** @class */ (function () {
                function DynamicModule() {
                }
                DynamicModule.decorators = [
                    { type: core.NgModule, args: [{
                                imports: [common.CommonModule, core$1.TranslateModule],
                                declarations: [component]
                            },] },
                ];
                return DynamicModule;
            }());
            return DynamicModule;
        };
        BlockCompileDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: '[coreBlockCompile]'
                    },] }
        ];
        /** @nocollapse */
        BlockCompileDirective.ctorParameters = function () { return [
            { type: core.ViewContainerRef },
            { type: core.Compiler }
        ]; };
        BlockCompileDirective.propDecorators = {
            coreBlockCompile: [{ type: core.Input }],
            coreBlockCompileContext: [{ type: core.Input }]
        };
        return BlockCompileDirective;
    }());
    if (false) {
        /** @type {?} */
        BlockCompileDirective.prototype.coreBlockCompile;
        /** @type {?} */
        BlockCompileDirective.prototype.coreBlockCompileContext;
        /** @type {?} */
        BlockCompileDirective.prototype.compRef;
        /**
         * @type {?}
         * @private
         */
        BlockCompileDirective.prototype.vcRef;
        /**
         * @type {?}
         * @private
         */
        BlockCompileDirective.prototype.compiler;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-abstract.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockAbstractComponent = /** @class */ (function () {
        function BlockAbstractComponent(authService, pages, resolver, viewContainerRef, translate, compiler) {
            this.authService = authService;
            this.pages = pages;
            this.resolver = resolver;
            this.viewContainerRef = viewContainerRef;
            this.translate = translate;
            this.compiler = compiler;
        }
        /**
         * @return {?}
         */
        BlockAbstractComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            if (this.authService) {
                this.user = this.authService.getUser();
            }
        };
        /**
         * @protected
         * @param {?} contents
         * @return {?}
         */
        BlockAbstractComponent.prototype.createDynamicBlock = /**
         * @protected
         * @param {?} contents
         * @return {?}
         */
        function (contents) {
            this.dynamicComponent = this.createNewComponent(contents);
            this.dynamicModule = this.compiler.compileModuleSync(this.createComponentModule(this.dynamicComponent));
        };
        /**
         * @protected
         * @param {?} componentType
         * @return {?}
         */
        BlockAbstractComponent.prototype.createComponentModule = /**
         * @protected
         * @param {?} componentType
         * @return {?}
         */
        function (componentType) {
            var RuntimeComponentModule = /** @class */ (function () {
                function RuntimeComponentModule() {
                }
                RuntimeComponentModule.decorators = [
                    { type: core.NgModule, args: [{
                                imports: [core$1.TranslateModule],
                                declarations: [componentType],
                                entryComponents: [componentType]
                            },] },
                ];
                return RuntimeComponentModule;
            }());
            return RuntimeComponentModule;
        };
        /**
         * @protected
         * @param {?} contents
         * @return {?}
         */
        BlockAbstractComponent.prototype.createNewComponent = /**
         * @protected
         * @param {?} contents
         * @return {?}
         */
        function (contents) {
            var DynamicComponent = /** @class */ (function () {
                function DynamicComponent(authService) {
                    this.authService = authService;
                }
                /**
                 * @return {?}
                 */
                DynamicComponent.prototype.ngOnInit = /**
                 * @return {?}
                 */
                function () {
                    this.contents = contents;
                    this.user = this.authService.getUser();
                };
                DynamicComponent.decorators = [
                    { type: core.Component, args: [{
                                selector: 'core-block-dynamic',
                                template: "" + contents,
                            },] },
                ];
                /** @nocollapse */
                DynamicComponent.ctorParameters = function () { return [
                    { type: AuthService }
                ]; };
                return DynamicComponent;
            }());
            if (false) {
                /** @type {?} */
                DynamicComponent.prototype.contents;
                /** @type {?} */
                DynamicComponent.prototype.user;
                /**
                 * @type {?}
                 * @protected
                 */
                DynamicComponent.prototype.authService;
            }
            return DynamicComponent;
        };
        /**
         * @param {?} idx
         * @param {?=} item
         * @return {?}
         */
        BlockAbstractComponent.prototype.trackByFn = /**
         * @param {?} idx
         * @param {?=} item
         * @return {?}
         */
        function (idx, item) {
            return idx;
        };
        BlockAbstractComponent.decorators = [
            { type: core.Component, args: [{
                        template: "<core-block class=\"{{block.data.class}} {{block.data.visibility}}\"\n            *ngFor=\"let subBlock of block.content trackBy: trackByFn\"\n            [block]=\"subBlock\"\n            [ngClass]=\"{ 'stickyDiv': block.data.specialType == 'sticky',\n            'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n             'col-sm-12': block.data.columnsSmall == 'full',\n             'col-md-12': block.data.columnsNormal == 'full'}\"\n            [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n>\n</core-block>",
                        changeDetection: core.ChangeDetectionStrategy.Default
                    }] }
        ];
        /** @nocollapse */
        BlockAbstractComponent.ctorParameters = function () { return [
            { type: AuthService },
            { type: PageService },
            { type: core.ComponentFactoryResolver },
            { type: core.ViewContainerRef },
            { type: TranslateService },
            { type: core.Compiler }
        ]; };
        BlockAbstractComponent.propDecorators = {
            block: [{ type: core.Input }]
        };
        return BlockAbstractComponent;
    }());
    if (false) {
        /** @type {?} */
        BlockAbstractComponent.prototype.block;
        /** @type {?} */
        BlockAbstractComponent.prototype.user;
        /** @type {?} */
        BlockAbstractComponent.prototype.dynamicComponent;
        /** @type {?} */
        BlockAbstractComponent.prototype.dynamicModule;
        /**
         * @type {?}
         * @protected
         */
        BlockAbstractComponent.prototype.authService;
        /**
         * @type {?}
         * @protected
         */
        BlockAbstractComponent.prototype.pages;
        /**
         * @type {?}
         * @protected
         */
        BlockAbstractComponent.prototype.resolver;
        /**
         * @type {?}
         * @protected
         */
        BlockAbstractComponent.prototype.viewContainerRef;
        /**
         * @type {?}
         * @protected
         */
        BlockAbstractComponent.prototype.translate;
        /**
         * @type {?}
         * @protected
         */
        BlockAbstractComponent.prototype.compiler;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/pages/search.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var EXCERPT_SIZE = 500;
    var SearchService = /** @class */ (function (_super) {
        __extends(SearchService, _super);
        function SearchService(router, api, spinner, translate) {
            var _this = _super.call(this, router, api) || this;
            _this.router = router;
            _this.api = api;
            _this.spinner = spinner;
            _this.translate = translate;
            return _this;
        }
        /**
         * @return {?}
         */
        SearchService.prototype.refresh = /**
         * @return {?}
         */
        function () {
            this.limit = 10;
            this.offset = 0;
            this.results = [];
            this.eddie = false;
            this.totalResults = 0;
        };
        /**
         * @param {?} query
         * @return {?}
         */
        SearchService.prototype.setQuery = /**
         * @param {?} query
         * @return {?}
         */
        function (query) {
            this.query = query;
        };
        /**
         * @return {?}
         */
        SearchService.prototype.getQuery = /**
         * @return {?}
         */
        function () {
            return this.query;
        };
        /**
         * @param {?} query
         * @return {?}
         */
        SearchService.prototype.performSearch = /**
         * @param {?} query
         * @return {?}
         */
        function (query) {
            var _this = this;
            this.refresh();
            this.setQuery(query);
            this.spinner.show('search');
            return this.getOneThroughCache('search', null, {}, false, {
                q: query,
                limit: this.limit,
                offset: this.offset,
                excerpt: EXCERPT_SIZE
            }).subscribe((/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                var e_1, _a;
                if (response.response.docs) {
                    /** @type {?} */
                    var searchTermRegex = new RegExp(query, 'gi');
                    _this.results = __spread(response.response.docs);
                    _this.totalResults = response.response.numFound;
                    try {
                        for (var _b = __values(_this.results), _c = _b.next(); !_c.done; _c = _b.next()) {
                            var result = _c.value;
                            if (result.content) {
                                result.content = result.content[0].substring(0, 1000).replace(searchTermRegex, '<em>$&</em>');
                                if (response.highlighting[result.uniqueId].content) {
                                    result.content = response.highlighting[result.uniqueId].content[0];
                                }
                                else if (response.highlighting[result.uniqueId].text && response.highlighting[result.uniqueId].text[0]) {
                                    result.content = response.highlighting[result.uniqueId].text[0];
                                }
                                result.content = result.content.replace(/\|\|/g, ', ').replace(/\ufffd/g, ' ') + '&hellip;';
                                if (result.categories) {
                                    result.category = result.categories[0];
                                }
                                // result.name = LanguageService.getPageSpecific('name', results);
                                // result.url = LanguageService.getPageSpecific('url', results);
                                // result.description = LanguageService.getPageSpecific('description', results);
                            }
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                }
            }));
        };
        /**
         * @return {?}
         */
        SearchService.prototype.getTotalResults = /**
         * @return {?}
         */
        function () {
            return this.totalResults;
        };
        SearchService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        SearchService.ctorParameters = function () { return [
            { type: router.Router },
            { type: http.HttpClient },
            { type: SpinnerService },
            { type: TranslateService }
        ]; };
        /** @nocollapse */ SearchService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function SearchService_Factory() { return new SearchService(core.ɵɵinject(router.Router), core.ɵɵinject(http.HttpClient), core.ɵɵinject(SpinnerService), core.ɵɵinject(TranslateService)); }, token: SearchService, providedIn: "root" });
        return SearchService;
    }(PageService));
    if (false) {
        /** @type {?} */
        SearchService.prototype.query;
        /** @type {?} */
        SearchService.prototype.limit;
        /** @type {?} */
        SearchService.prototype.offset;
        /** @type {?} */
        SearchService.prototype.results;
        /** @type {?} */
        SearchService.prototype.totalResults;
        /** @type {?} */
        SearchService.prototype.eddie;
        /**
         * @type {?}
         * @protected
         */
        SearchService.prototype.router;
        /**
         * @type {?}
         * @protected
         */
        SearchService.prototype.api;
        /**
         * @type {?}
         * @protected
         */
        SearchService.prototype.spinner;
        /**
         * @type {?}
         * @protected
         */
        SearchService.prototype.translate;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-wrapper/block-wrapper.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockWrapperComponent = /** @class */ (function (_super) {
        __extends(BlockWrapperComponent, _super);
        function BlockWrapperComponent() {
            return _super.call(this) || this;
        }
        /**
         * @return {?}
         */
        BlockWrapperComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @param {?} idx
         * @param {?=} item
         * @return {?}
         */
        BlockWrapperComponent.prototype.trackByFn = /**
         * @param {?} idx
         * @param {?=} item
         * @return {?}
         */
        function (idx, item) {
            return idx;
        };
        BlockWrapperComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-wrapper',
                        template: "<core-block class=\"{{block.data.class}} {{block.data.visibility}}\"\n            *ngFor=\"let subBlock of block.content trackBy: trackByFn\"\n            [block]=\"subBlock\"\n            [ngClass]=\"{ 'stickyDiv': block.data.specialType == 'sticky',\n            'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n             'col-sm-12': block.data.columnsSmall == 'full',\n             'col-md-12': block.data.columnsNormal == 'full'}\"\n            [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n>\n</core-block>"
                    }] }
        ];
        /** @nocollapse */
        BlockWrapperComponent.ctorParameters = function () { return []; };
        return BlockWrapperComponent;
    }(BlockAbstractComponent));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-language/block-language.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockLanguageComponent = /** @class */ (function (_super) {
        __extends(BlockLanguageComponent, _super);
        function BlockLanguageComponent(translate, translateProvider) {
            var _this = _super.call(this) || this;
            _this.translate = translate;
            _this.translateProvider = translateProvider;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockLanguageComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this.translate.getLanguages().subscribe((/**
             * @param {?} languages
             * @return {?}
             */
            function (languages) {
                _this.languages$ = languages;
            }));
        };
        /**
         * @param {?} languageCode
         * @return {?}
         */
        BlockLanguageComponent.prototype.setLanguage = /**
         * @param {?} languageCode
         * @return {?}
         */
        function (languageCode) {
            this.translateProvider.use(languageCode);
            this.translate.setDefaultLanguage(languageCode);
        };
        BlockLanguageComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-language',
                        template: "<mat-nav-list>\n    <mat-list-item *ngFor=\"let language of languages$\" [ngClass]=\"{'active' : language.code === ''}\">\n        <a matLine (click)=\"setLanguage(language.code)\">{{language.name}}</a>\n    </mat-list-item>\n</mat-nav-list>"
                    }] }
        ];
        /** @nocollapse */
        BlockLanguageComponent.ctorParameters = function () { return [
            { type: TranslateService },
            { type: core$1.TranslateService }
        ]; };
        return BlockLanguageComponent;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockLanguageComponent.prototype.languages$;
        /** @type {?} */
        BlockLanguageComponent.prototype.language;
        /**
         * @type {?}
         * @protected
         */
        BlockLanguageComponent.prototype.translate;
        /**
         * @type {?}
         * @protected
         */
        BlockLanguageComponent.prototype.translateProvider;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-paragraph/block-paragraph.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockParagraphComponent = /** @class */ (function (_super) {
        __extends(BlockParagraphComponent, _super);
        function BlockParagraphComponent(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockParagraphComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
            this.text = this.block.data.text;
        };
        BlockParagraphComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-paragraph',
                        template: "\n        <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n    "
                    }] }
        ];
        /** @nocollapse */
        BlockParagraphComponent.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        BlockParagraphComponent.propDecorators = {
            text: [{ type: core.Input }]
        };
        return BlockParagraphComponent;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockParagraphComponent.prototype.text;
        /**
         * @type {?}
         * @protected
         */
        BlockParagraphComponent.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-paragraph-lead/block-paragraph-lead.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockParagraphLeadComponent = /** @class */ (function (_super) {
        __extends(BlockParagraphLeadComponent, _super);
        function BlockParagraphLeadComponent(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockParagraphLeadComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
            this.text = this.block.data.text;
        };
        BlockParagraphLeadComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-paragraph-lead',
                        template: "\n        <ng-container *coreBlockCompile=\"text; context: this\" class=\"lead\"></ng-container>\n    "
                    }] }
        ];
        /** @nocollapse */
        BlockParagraphLeadComponent.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        BlockParagraphLeadComponent.propDecorators = {
            text: [{ type: core.Input }]
        };
        return BlockParagraphLeadComponent;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockParagraphLeadComponent.prototype.text;
        /**
         * @type {?}
         * @protected
         */
        BlockParagraphLeadComponent.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-table/block-table.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockTableComponent = /** @class */ (function (_super) {
        __extends(BlockTableComponent, _super);
        function BlockTableComponent(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockTableComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.header = this.block.data.tableData[0];
            this.data = this.block.data.tableData;
            this.data.shift();
        };
        /**
         * @param {?} idx
         * @return {?}
         */
        BlockTableComponent.prototype.trackByFn = /**
         * @param {?} idx
         * @return {?}
         */
        function (idx) {
            return idx;
        };
        BlockTableComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-table',
                        template: "<div class=\"table-wrapper\">\n    <table class=\"table\">\n        <thead>\n        <tr>\n            <th *ngFor=\"let cell of header trackBy: trackByFn\" [innerHTML]=\"cell.content\"></th>\n        </tr>\n        </thead>\n        <tbody>\n        <tr *ngFor=\"let row of data trackBy: trackByFn\">\n            <td *ngFor=\"let cell of row trackBy: trackByFn\" [innerHTML]=\"cell.content\"></td>\n        </tr>\n        </tbody>\n    </table>\n</div>"
                    }] }
        ];
        /** @nocollapse */
        BlockTableComponent.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        return BlockTableComponent;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockTableComponent.prototype.header;
        /** @type {?} */
        BlockTableComponent.prototype.data;
        /**
         * @type {?}
         * @protected
         */
        BlockTableComponent.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-accordion/block-accordion.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockAccordionComponent = /** @class */ (function (_super) {
        __extends(BlockAccordionComponent, _super);
        function BlockAccordionComponent(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockAccordionComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @param {?} idx
         * @return {?}
         */
        BlockAccordionComponent.prototype.trackByFn = /**
         * @param {?} idx
         * @return {?}
         */
        function (idx) {
            return idx;
        };
        BlockAccordionComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-accordion',
                        template: "<mat-accordion>\n    <mat-expansion-panel *ngFor=\"let item of block.content trackBy: trackByFn\">\n        <mat-expansion-panel-header>\n            <mat-panel-title [innerHTML]=\"item.data.title\"></mat-panel-title>\n        </mat-expansion-panel-header>\n        <div class=\"{{block.data.title}} {{block.data.visibility}}\"\n             [ngClass]=\"{container: block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n                 'stickyDiv': block.data.specialType == 'sticky',\n                  'full-width': block.data.columnsSmall == 'full' ||  block.data.columnsNormal == 'full',\n                  'col-sm-12': block.data.columnsSmall == 'full',\n                  'col-md-12': block.data.columnsNormal == 'full'}\"\n             [ngStyle]=\"{padding: block.data.padding, margin: block.data.margin }\"\n        >\n            <core-block\n                    *ngFor=\"let subBlock of item.content\"\n                    [block]=\"subBlock\"\n                    class=\"subWidget {{subBlock.data.class}}\">\n            </core-block>\n        </div>\n    </mat-expansion-panel>\n</mat-accordion>\n"
                    }] }
        ];
        /** @nocollapse */
        BlockAccordionComponent.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        return BlockAccordionComponent;
    }(BlockAbstractComponent));
    if (false) {
        /**
         * @type {?}
         * @protected
         */
        BlockAccordionComponent.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-h1/block-h1.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockH1Component = /** @class */ (function (_super) {
        __extends(BlockH1Component, _super);
        function BlockH1Component(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockH1Component.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
            this.text = this.block.data.text;
        };
        BlockH1Component.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-h1',
                        template: "\n        <h1>\n            <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n        </h1>"
                    }] }
        ];
        /** @nocollapse */
        BlockH1Component.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        BlockH1Component.propDecorators = {
            text: [{ type: core.Input }]
        };
        return BlockH1Component;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockH1Component.prototype.text;
        /**
         * @type {?}
         * @protected
         */
        BlockH1Component.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-h2/block-h2.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockH2Component = /** @class */ (function (_super) {
        __extends(BlockH2Component, _super);
        function BlockH2Component(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockH2Component.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
            this.text = this.block.data.text;
        };
        BlockH2Component.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-h2',
                        template: "\n        <h2>\n            <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n        </h2>"
                    }] }
        ];
        /** @nocollapse */
        BlockH2Component.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        BlockH2Component.propDecorators = {
            text: [{ type: core.Input }]
        };
        return BlockH2Component;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockH2Component.prototype.text;
        /**
         * @type {?}
         * @protected
         */
        BlockH2Component.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-h3/block-h3.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockH3Component = /** @class */ (function (_super) {
        __extends(BlockH3Component, _super);
        function BlockH3Component(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockH3Component.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
            this.text = this.block.data.text;
        };
        BlockH3Component.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-h3',
                        template: "\n        <h3>\n            <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n        </h3>"
                    }] }
        ];
        /** @nocollapse */
        BlockH3Component.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        BlockH3Component.propDecorators = {
            text: [{ type: core.Input }]
        };
        return BlockH3Component;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockH3Component.prototype.text;
        /**
         * @type {?}
         * @protected
         */
        BlockH3Component.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-h4/block-h4.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockH4Component = /** @class */ (function (_super) {
        __extends(BlockH4Component, _super);
        function BlockH4Component(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockH4Component.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
            this.text = this.block.data.text;
        };
        BlockH4Component.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-h4',
                        template: "\n        <h4>\n            <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n        </h4>"
                    }] }
        ];
        /** @nocollapse */
        BlockH4Component.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        BlockH4Component.propDecorators = {
            text: [{ type: core.Input }]
        };
        return BlockH4Component;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockH4Component.prototype.text;
        /**
         * @type {?}
         * @protected
         */
        BlockH4Component.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-h5/block-h5.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockH5Component = /** @class */ (function (_super) {
        __extends(BlockH5Component, _super);
        function BlockH5Component(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockH5Component.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
            this.text = this.block.data.text;
        };
        BlockH5Component.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-h5',
                        template: "\n        <h5>\n            <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n        </h5>"
                    }] }
        ];
        /** @nocollapse */
        BlockH5Component.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        BlockH5Component.propDecorators = {
            text: [{ type: core.Input }]
        };
        return BlockH5Component;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockH5Component.prototype.text;
        /**
         * @type {?}
         * @protected
         */
        BlockH5Component.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-h6/block-h6.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockH6Component = /** @class */ (function (_super) {
        __extends(BlockH6Component, _super);
        function BlockH6Component(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockH6Component.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
            this.text = this.block.data.text;
        };
        BlockH6Component.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-h6',
                        template: "\n        <h6>\n            <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n        </h6>"
                    }] }
        ];
        /** @nocollapse */
        BlockH6Component.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        BlockH6Component.propDecorators = {
            text: [{ type: core.Input }]
        };
        return BlockH6Component;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockH6Component.prototype.text;
        /**
         * @type {?}
         * @protected
         */
        BlockH6Component.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-image/block-image.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockImageComponent = /** @class */ (function (_super) {
        __extends(BlockImageComponent, _super);
        function BlockImageComponent() {
            return _super.call(this) || this;
        }
        /**
         * @return {?}
         */
        BlockImageComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.url = this.block.data.image_url;
            this.alt = this.block.data.alt_text;
        };
        BlockImageComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-image',
                        template: "\n        <img class=\"img-responsive image-block\" [src]=\"url\" alt=\"{{alt}}\"/>"
                    }] }
        ];
        /** @nocollapse */
        BlockImageComponent.ctorParameters = function () { return []; };
        return BlockImageComponent;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockImageComponent.prototype.url;
        /** @type {?} */
        BlockImageComponent.prototype.alt;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-breadcrumbs/block-breadcrumbs.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockBreadcrumbsComponent = /** @class */ (function (_super) {
        __extends(BlockBreadcrumbsComponent, _super);
        function BlockBreadcrumbsComponent(authService, pages) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            _this.pages = pages;
            _this.date = Date.now();
            return _this;
        }
        /**
         * @return {?}
         */
        BlockBreadcrumbsComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this.pages.getBreadcrumbs(this.pages.getCurrentPage().id).subscribe((/**
             * @param {?} breadcrumbs
             * @return {?}
             */
            function (breadcrumbs) {
                _this.breadcrumbs = breadcrumbs || [];
            }));
        };
        BlockBreadcrumbsComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-breadcrumbs',
                        template: "<ol class=\"breadcrumb page-breadcrumb\">\n    <li class=\"breadcrumb-item\"><a href=\"#\" coreStubClick>Home</a></li>\n    <li class=\"breadcrumb-item\" *ngFor=\"let breadcrumb of breadcrumbs.parents\">\n        <a [routerLink]=\"breadcrumb.url\">{{breadcrumb.name}}</a>\n    </li>\n    <li class=\"breadcrumb-item\">{{breadcrumbs.name}}</li>\n    <li class=\"position-absolute pos-top pos-right d-none d-sm-block\">\n        <span>{{ date | date }}</span>\n    </li>\n</ol>"
                    }] }
        ];
        /** @nocollapse */
        BlockBreadcrumbsComponent.ctorParameters = function () { return [
            { type: AuthService },
            { type: PageService }
        ]; };
        return BlockBreadcrumbsComponent;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockBreadcrumbsComponent.prototype.breadcrumbs;
        /** @type {?} */
        BlockBreadcrumbsComponent.prototype.date;
        /**
         * @type {?}
         * @protected
         */
        BlockBreadcrumbsComponent.prototype.authService;
        /**
         * @type {?}
         * @protected
         */
        BlockBreadcrumbsComponent.prototype.pages;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-carousel/block-carousel.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockCarouselComponent = /** @class */ (function (_super) {
        __extends(BlockCarouselComponent, _super);
        function BlockCarouselComponent(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockCarouselComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        BlockCarouselComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-carousel',
                        template: "CAROUSEL"
                    }] }
        ];
        /** @nocollapse */
        BlockCarouselComponent.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        return BlockCarouselComponent;
    }(BlockAbstractComponent));
    if (false) {
        /**
         * @type {?}
         * @protected
         */
        BlockCarouselComponent.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-video/block-video.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockVideoComponent = /** @class */ (function (_super) {
        __extends(BlockVideoComponent, _super);
        function BlockVideoComponent() {
            return _super.call(this) || this;
        }
        /**
         * @return {?}
         */
        BlockVideoComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        BlockVideoComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-video',
                        template: "VIDEO"
                    }] }
        ];
        /** @nocollapse */
        BlockVideoComponent.ctorParameters = function () { return []; };
        return BlockVideoComponent;
    }(BlockAbstractComponent));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-bullets/block-bullets.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockBulletsComponent = /** @class */ (function (_super) {
        __extends(BlockBulletsComponent, _super);
        function BlockBulletsComponent(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockBulletsComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
        };
        /**
         * @param {?} idx
         * @return {?}
         */
        BlockBulletsComponent.prototype.trackByFn = /**
         * @param {?} idx
         * @return {?}
         */
        function (idx) {
            return idx;
        };
        BlockBulletsComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-bullets',
                        template: "<mat-list>\n    <mat-list-item *ngFor=\"let item of block.content trackBy: trackByFn\" [innerHTML]=\"item.data.text\"></mat-list-item>\n</mat-list>"
                    }] }
        ];
        /** @nocollapse */
        BlockBulletsComponent.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        return BlockBulletsComponent;
    }(BlockAbstractComponent));
    if (false) {
        /**
         * @type {?}
         * @protected
         */
        BlockBulletsComponent.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-numbered/block-numbered.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockNumberedComponent = /** @class */ (function (_super) {
        __extends(BlockNumberedComponent, _super);
        function BlockNumberedComponent() {
            return _super.call(this) || this;
        }
        /**
         * @return {?}
         */
        BlockNumberedComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
        };
        /**
         * @param {?} idx
         * @return {?}
         */
        BlockNumberedComponent.prototype.trackByFn = /**
         * @param {?} idx
         * @return {?}
         */
        function (idx) {
            return idx;
        };
        BlockNumberedComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-numbered',
                        template: "<mat-list>\n    <mat-list-item *ngFor=\"let item of block.content trackBy: trackByFn\" [innerHTML]=\"item.data.text\"></mat-list-item>\n</mat-list>"
                    }] }
        ];
        /** @nocollapse */
        BlockNumberedComponent.ctorParameters = function () { return []; };
        return BlockNumberedComponent;
    }(BlockAbstractComponent));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-blockquote/block-blockquote.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockBlockquoteComponent = /** @class */ (function (_super) {
        __extends(BlockBlockquoteComponent, _super);
        function BlockBlockquoteComponent(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockBlockquoteComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
            this.text = this.block.data.blockQuote;
        };
        BlockBlockquoteComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-blockquote',
                        template: "<blockquote>\n    <p>\n        <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n    </p>\n    <footer *ngIf=\"block.data.cite\">\n        <cite>\n            <a [href]=\"block.data.citeLink\" *ngIf=\"block.data.citeLink\">{{block.data.cite}}</a>\n            <span *ngIf=\"!block.data.citeLink\">{{block.data.cite}}</span>\n        </cite>\n    </footer>\n</blockquote>"
                    }] }
        ];
        /** @nocollapse */
        BlockBlockquoteComponent.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        BlockBlockquoteComponent.propDecorators = {
            text: [{ type: core.Input }]
        };
        return BlockBlockquoteComponent;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockBlockquoteComponent.prototype.text;
        /**
         * @type {?}
         * @protected
         */
        BlockBlockquoteComponent.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-download/block-download.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockDownloadComponent = /** @class */ (function (_super) {
        __extends(BlockDownloadComponent, _super);
        function BlockDownloadComponent(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockDownloadComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
        };
        BlockDownloadComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-download',
                        template: "<div class=\"download\">\n    <p *ngIf=\"block.data.downloadText\" [innerHTML]=\"block.data.downloadText\"></p>\n    <a [href]=\"block.data.documentId ? block.data.documentId : block.data.buttonLink\"\n       target=\"{{block.data.target}}\" class=\"btn btn-primary\" role=\"button\">{{block.data.buttonText}}</a>\n</div>\n"
                    }] }
        ];
        /** @nocollapse */
        BlockDownloadComponent.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        return BlockDownloadComponent;
    }(BlockAbstractComponent));
    if (false) {
        /**
         * @type {?}
         * @protected
         */
        BlockDownloadComponent.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-button/block-button.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockButtonComponent = /** @class */ (function (_super) {
        __extends(BlockButtonComponent, _super);
        function BlockButtonComponent(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockButtonComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
            this.text = this.block.data.buttonPreText;
        };
        BlockButtonComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-button',
                        template: "<div class=\"button-block\">\n    <p *ngIf=\"block.data.buttonPreText\">\n        <ng-container *coreBlockCompile=\"text; context: this\"></ng-container>\n    </p>\n    <a [href]=\"block.data.documentId ? block.data.documentId : block.data.buttonLink\"\n       target=\"{{block.data.target}}\" class=\"btn {{block.data.buttonType}}\" role=\"button\">{{block.data.buttonText}}\n    </a>\n</div>\n"
                    }] }
        ];
        /** @nocollapse */
        BlockButtonComponent.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        BlockButtonComponent.propDecorators = {
            text: [{ type: core.Input }]
        };
        return BlockButtonComponent;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockButtonComponent.prototype.text;
        /**
         * @type {?}
         * @protected
         */
        BlockButtonComponent.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-divider/block-divider.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockDividerComponent = /** @class */ (function (_super) {
        __extends(BlockDividerComponent, _super);
        function BlockDividerComponent() {
            return _super.call(this) || this;
        }
        /**
         * @return {?}
         */
        BlockDividerComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
        };
        BlockDividerComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-divider',
                        template: "\n        <hr class=\"divider\"/>\n    "
                    }] }
        ];
        /** @nocollapse */
        BlockDividerComponent.ctorParameters = function () { return []; };
        return BlockDividerComponent;
    }(BlockAbstractComponent));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-progress/block-progress.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockProgressComponent = /** @class */ (function (_super) {
        __extends(BlockProgressComponent, _super);
        function BlockProgressComponent() {
            return _super.call(this) || this;
        }
        /**
         * @return {?}
         */
        BlockProgressComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            _super.prototype.ngOnInit.call(this);
        };
        BlockProgressComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-progress',
                        template: "\n        <mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\n    "
                    }] }
        ];
        /** @nocollapse */
        BlockProgressComponent.ctorParameters = function () { return []; };
        return BlockProgressComponent;
    }(BlockAbstractComponent));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-tabs/block-tabs.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockTabsComponent = /** @class */ (function (_super) {
        __extends(BlockTabsComponent, _super);
        function BlockTabsComponent(authService) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            return _this;
        }
        /**
         * @return {?}
         */
        BlockTabsComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @param {?} idx
         * @return {?}
         */
        BlockTabsComponent.prototype.trackByFn = /**
         * @param {?} idx
         * @return {?}
         */
        function (idx) {
            return idx;
        };
        BlockTabsComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-tabs',
                        template: "<mat-tab-group>\n    <mat-tab *ngFor=\"let item of block.content trackBy: trackByFn\">\n        <ng-template mat-tab-label>\n            <ng-container>{{item.data.header}}</ng-container>\n        </ng-template>\n        <div [innerHTML]=\"item.data.content\"></div>\n    </mat-tab>\n</mat-tab-group>\n"
                    }] }
        ];
        /** @nocollapse */
        BlockTabsComponent.ctorParameters = function () { return [
            { type: AuthService }
        ]; };
        return BlockTabsComponent;
    }(BlockAbstractComponent));
    if (false) {
        /**
         * @type {?}
         * @protected
         */
        BlockTabsComponent.prototype.authService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-search-results/block-search-results.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockSearchResultsComponent = /** @class */ (function (_super) {
        __extends(BlockSearchResultsComponent, _super);
        function BlockSearchResultsComponent(authService, search) {
            var _this = _super.call(this, authService) || this;
            _this.authService = authService;
            _this.search = search;
            _this.feedback = new forms.FormControl();
            return _this;
        }
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.reset();
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.reset = /**
         * @return {?}
         */
        function () {
            this.feedbackEnabled = this.block.data.showFeedback;
            this.eddieExpanded = false;
        };
        /**
         * @param {?} idx
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.trackByFn = /**
         * @param {?} idx
         * @return {?}
         */
        function (idx) {
            return idx;
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.getResults = /**
         * @return {?}
         */
        function () {
            return this.search.results;
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.hasResults = /**
         * @return {?}
         */
        function () {
            return this.search.results.length > 0;
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.getEddie = /**
         * @return {?}
         */
        function () {
            return this.search.eddie;
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.hasEddie = /**
         * @return {?}
         */
        function () {
            return this.search.eddie !== false;
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.hasSearchTerm = /**
         * @return {?}
         */
        function () {
            return true;
        };
        /**
         * @param {?=} type
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.hasFeedback = /**
         * @param {?=} type
         * @return {?}
         */
        function (type) {
            return false;
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.givePositiveFeedback = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.giveNegativeFeedback = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.closeFeedback = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @param {?} query
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.performSearch = /**
         * @param {?} query
         * @return {?}
         */
        function (query) {
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.loadMore = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @return {?}
         */
        BlockSearchResultsComponent.prototype.getTotalResults = /**
         * @return {?}
         */
        function () {
            return this.search.getTotalResults();
        };
        BlockSearchResultsComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-search-results',
                        template: "<div class=\"search-results-content\">\n\n    <core-spinner name=\"search\"></core-spinner>\n\n    <div class=\"row search-top\">\n        <div class=\"col-sm-12 text-center\">\n            <p>\n                <span *ngIf=\"!hasResults() && !hasEddie() && hasSearchTerm()\"\n                      [innerHTML]=\"noResultsText\"></span>\n                <span *ngIf=\"hasResults() && introText\" [innerHTML]=\"introText\"></span>\n            </p>\n        </div>\n    </div>\n\n    <h3 *ngIf=\"hasResults() || hasEddie()\">Results</h3>\n\n    <!-- Eddie -->\n    <div class=\"eddie\" *ngIf=\"hasEddie()\">\n        <h4 class=\"eddie__question\" [innerHTML]=\"getEddie().question\"></h4>\n\n        <div [hidden]=\"eddieExpanded\">\n            <div class=\"eddie__answer\" [innerHTML]=\"getEddie().answer | limitTo: 150\"></div>\n            <div class=\"eddie__ellipsis\" *ngIf=\"getEddie().answer.length > 150\">...</div>\n        </div>\n\n        <div class=\"eddie__answer-expanded\" [hidden]=\"!eddieExpanded\">\n            <div class=\"eddie__answer expanded\" [innerHTML]=\"getEddie().answer\"></div>\n            <div class=\"eddie__helpful-links\" *ngIf=\"getEddie().supplementalData.links.length\">\n                <h5>Helpful Links</h5>\n                <a *ngFor=\"let link of getEddie().supplementalData.links\" [href]=\"link.href\"\n                   target=\"_blank\">{{link.label}}</a>\n            </div>\n\n            <div class=\"eddie__feeback\">\n                <div class=\"eddie__feedback-question\">Did this answer your question?</div>\n                <div class=\"eddie__feedback-answers-block\">\n                    <button [disabled]=\"hasFeedback()\" (click)=\"givePositiveFeedback()\"\n                            [ngClass]=\"{'active' : hasFeedback('positive') }\" class=\"btn btn-feedback\">\n                        Yes\n                    </button>\n                    <button [disabled]=\"hasFeedback()\" (click)=\"giveNegativeFeedback()\"\n                            [ngClass]=\"{'active' : hasFeedback('negative') }\" class=\"btn btn-feedback\">\n                        No\n                    </button>\n                </div>\n            </div>\n\n            <div [hidden]=\"!supportSubmitted\" class=\"alert alert-success\">\n                <p><strong>Thank you, we really value your feedback.</strong>\n                    <br/>The search and support feature is evolving all the time \u2013 the more you use it the better we can\n                    make it!</p>\n            </div>\n\n            <div class=\"eddie__feedback-form\" *ngIf=\"feedbackEnabled\">\n                <button type=\"button\" class=\"eddie__feedback-close\"\n                        (click)=\"closeFeedback();\">\n                    <img src=\"/assets/img/close.svg\" alt=\"Close\"/>\n                </button>\n\n                <p><strong>We really value your feedback, please tell us how we could improve this answer.</strong></p>\n                <form #feedbackForm=\"ngForm\">\n                    <div class=\"form-group\">\n                        <label>\n                            <span class=\"sr-only\">Message</span>\n                            <textarea class=\"form-control\" [formControl]=\"feedback\"></textarea>\n                        </label>\n                    </div>\n\n                    <div class=\"form-button\">\n                        <button class=\"btn btn-primary\"\n                                [disabled]=\"!feedback || feedback.value.length < 5 || supportSubmitted\"\n                                (click)=\"giveNegativeFeedback()\">Leave feedback\n                        </button>\n                    </div>\n                </form>\n            </div>\n        </div>\n\n        <div class=\"eddie__expand-button\"\n             (click)=\"eddieExpanded = !eddieExpanded\">{{eddieExpanded ? 'Hide full answer' : 'Show full answer'}}</div>\n    </div>\n\n    <div *ngIf=\"getEddie() && getEddie().relatedQuestions.length > 0\">\n        <core-block-divider></core-block-divider>\n        <div class=\"eddie-related\">\n            <h5>Your colleagues are also asking</h5>\n            <ul>\n                <li *ngFor=\"let question of getEddie().relatedQuestions\">\n                    <a (click)=\"performSearch(question);\">{{ question }}</a>\n                </li>\n            </ul>\n        </div>\n    </div>\n\n    <core-block-divider *ngIf=\"hasResults() && hasEddie()\"></core-block-divider>\n\n    <!-- Regular results -->\n    <div class=\"additional-results\" *ngIf=\"hasResults()\">\n        <h5 *ngIf=\"hasEddie()\">We also found related content on these pages</h5>\n\n        <ul class=\"search-results-list no-bullet\">\n            <li class=\"search-results-list__item\" *ngFor=\"let result of getResults() trackBy: trackByFn\">\n                <div class=\"search-results-wrapper\">\n                    <a class=\"search-results-list__link\" *ngIf=\"result.url\" [routerLink]=\"'/page/' + result.url\">\n                        <p class=\"result-name\" [innerHTML]=\"result.name\"></p>\n                    </a>\n                    <a class=\"search-results-list__link\" *ngIf=\"result.nodeId\"\n                       [href]=\"'/api/1.0/download/' + result.nodeId\"\n                       [target]=\"'_blank'\">\n                        <p class=\"result-name\" [innerHTML]=\"result.name\"></p>\n                    </a>\n                    <!--                    <div core-breadcrumb page=\"result\" ng-if=\"result.pageId\"></div>-->\n                    <p>{{ result.description | limitTo: 100 }}<span *ngIf=\"result.description.length > 100\">...</span>\n                    </p>\n                </div>\n            </li>\n        </ul>\n\n        <div class=\"button-wrapper text-left\" *ngIf=\"hasResults() && getResults().length < getTotalResults()\">\n            <button class=\"btn btn-default\" (click)=\"loadMore()\">{{block.data.loadMoreText}}</button>\n        </div>\n    </div>\n\n    <div *ngIf=\"hasResults() || hasEddie()\">\n        <core-block-divider></core-block-divider>\n        <p><strong>Not what you were looking for? Try searching again.</strong></p>\n    </div>\n\n</div>"
                    }] }
        ];
        /** @nocollapse */
        BlockSearchResultsComponent.ctorParameters = function () { return [
            { type: AuthService },
            { type: SearchService }
        ]; };
        return BlockSearchResultsComponent;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockSearchResultsComponent.prototype.feedbackEnabled;
        /** @type {?} */
        BlockSearchResultsComponent.prototype.eddieExpanded;
        /** @type {?} */
        BlockSearchResultsComponent.prototype.supportSubmitted;
        /** @type {?} */
        BlockSearchResultsComponent.prototype.noResultsText;
        /** @type {?} */
        BlockSearchResultsComponent.prototype.introText;
        /** @type {?} */
        BlockSearchResultsComponent.prototype.feedback;
        /**
         * @type {?}
         * @protected
         */
        BlockSearchResultsComponent.prototype.authService;
        /**
         * @type {?}
         * @protected
         */
        BlockSearchResultsComponent.prototype.search;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/blocks/block-search/block-search.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockSearchComponent = /** @class */ (function (_super) {
        __extends(BlockSearchComponent, _super);
        function BlockSearchComponent(search) {
            var _this = _super.call(this) || this;
            _this.search = search;
            _this.query = new forms.FormControl();
            return _this;
        }
        /**
         * @return {?}
         */
        BlockSearchComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.search.refresh();
            this.query.setValue(this.search.getQuery());
        };
        /**
         * @return {?}
         */
        BlockSearchComponent.prototype.performSearch = /**
         * @return {?}
         */
        function () {
            this.search.performSearch(this.query.value);
        };
        BlockSearchComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block-search',
                        template: "<form #searchForm=\"ngForm\" (ngSubmit)=\"performSearch()\">\n    <div class=\"form-group\">\n        <label class=\"form-label question-box\">\n            <span class=\"sr-only\">Search</span></label>\n        <input type=\"text\" class=\"form-control form-control-lg\" [formControl]=\"query\"\n               placeholder=\"Type your question...\"/>\n    </div>\n\n    <button class=\"btn btn-primary\" [ngClass]=\"{'selected': query.value && query.value.length > 2}\"\n            [disabled]=\"!query.value || query.value.length < 3\">Search\n    </button>\n</form>"
                    }] }
        ];
        /** @nocollapse */
        BlockSearchComponent.ctorParameters = function () { return [
            { type: SearchService }
        ]; };
        return BlockSearchComponent;
    }(BlockAbstractComponent));
    if (false) {
        /** @type {?} */
        BlockSearchComponent.prototype.query;
        /**
         * @type {?}
         * @protected
         */
        BlockSearchComponent.prototype.search;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/pages/block.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockService = /** @class */ (function () {
        function BlockService() {
            this.replaceBlocks({
                WRAPPER: BlockWrapperComponent,
                LANGUAGE: BlockLanguageComponent,
                PARAGRAPH: BlockParagraphComponent,
                'PARAGRAPH-LEAD': BlockParagraphLeadComponent,
                TABLE: BlockTableComponent,
                ACCORDION: BlockAccordionComponent,
                H1: BlockH1Component,
                H2: BlockH2Component,
                H3: BlockH3Component,
                H4: BlockH4Component,
                H5: BlockH5Component,
                H6: BlockH6Component,
                IMAGE: BlockImageComponent,
                BREADCRUMBS: BlockBreadcrumbsComponent,
                CAROUSEL: BlockCarouselComponent,
                VIDEO: BlockVideoComponent,
                BULLETS: BlockBulletsComponent,
                NUMBERED: BlockNumberedComponent,
                BLOCKQUOTE: BlockBlockquoteComponent,
                DOWNLOAD: BlockDownloadComponent,
                BUTTON: BlockButtonComponent,
                DIVIDER: BlockDividerComponent,
                PROGRESS: BlockProgressComponent,
                TABS: BlockTabsComponent,
                'SEARCH-RESULTS': BlockSearchResultsComponent,
                SEARCH: BlockSearchComponent,
            });
        }
        /**
         * @param {?} blocks
         * @return {?}
         */
        BlockService.prototype.replaceBlocks = /**
         * @param {?} blocks
         * @return {?}
         */
        function (blocks) {
            this.blocks = blocks;
        };
        /**
         * @return {?}
         */
        BlockService.prototype.getBlocks = /**
         * @return {?}
         */
        function () {
            return this.blocks;
        };
        /**
         * @param {?} identifier
         * @return {?}
         */
        BlockService.prototype.hasBlock = /**
         * @param {?} identifier
         * @return {?}
         */
        function (identifier) {
            return this.blocks.hasOwnProperty(identifier);
        };
        /**
         * @param {?} identifier
         * @return {?}
         */
        BlockService.prototype.getBlock = /**
         * @param {?} identifier
         * @return {?}
         */
        function (identifier) {
            return this.blocks[identifier];
        };
        /**
         * @param {?} identifier
         * @param {?} component
         * @return {?}
         */
        BlockService.prototype.addBlock = /**
         * @param {?} identifier
         * @param {?} component
         * @return {?}
         */
        function (identifier, component) {
            this.blocks[identifier] = component;
        };
        BlockService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        BlockService.ctorParameters = function () { return []; };
        /** @nocollapse */ BlockService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function BlockService_Factory() { return new BlockService(); }, token: BlockService, providedIn: "root" });
        return BlockService;
    }());
    if (false) {
        /** @type {?} */
        BlockService.prototype.blocks;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/pages/page.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PageComponent = /** @class */ (function () {
        function PageComponent(route, router, pages) {
            this.route = route;
            this.router = router;
            this.pages = pages;
            this.appName = APP_CONFIG.appName;
        }
        /**
         * @return {?}
         */
        PageComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this.route.paramMap.subscribe((/**
             * @param {?} params
             * @return {?}
             */
            function (params) {
                _this.subscription = _this.pages.getPage(params.get('identifier') || 'default').pipe(operators.map((/**
                 * @param {?} page
                 * @return {?}
                 */
                function (page) {
                    _this.pages.setCurrentPage(page);
                    _this.page$ = rxjs.of(page);
                }))).subscribe();
            }));
        };
        /**
         * @param {?} idx
         * @param {?} item
         * @return {?}
         */
        PageComponent.prototype.trackByFn = /**
         * @param {?} idx
         * @param {?} item
         * @return {?}
         */
        function (idx, item) {
            return item.uid + '_' + idx;
        };
        /**
         * @return {?}
         */
        PageComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            console.log('Page destroyed');
            this.subscription.unsubscribe();
        };
        PageComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-home',
                        template: "<div *ngIf=\"page$ | async as page\">\n    <core-block *ngFor=\"let block of page.widgets trackBy: trackByFn\" [block]=\"block\" class=\"core-block-l1\"></core-block>\n</div>\n",
                        changeDetection: core.ChangeDetectionStrategy.Default
                    }] }
        ];
        /** @nocollapse */
        PageComponent.ctorParameters = function () { return [
            { type: router.ActivatedRoute },
            { type: router.Router },
            { type: PageService }
        ]; };
        return PageComponent;
    }());
    if (false) {
        /** @type {?} */
        PageComponent.prototype.appName;
        /** @type {?} */
        PageComponent.prototype.page$;
        /** @type {?} */
        PageComponent.prototype.subscription;
        /**
         * @type {?}
         * @private
         */
        PageComponent.prototype.route;
        /**
         * @type {?}
         * @private
         */
        PageComponent.prototype.router;
        /**
         * @type {?}
         * @private
         */
        PageComponent.prototype.pages;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/block/block.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BlockComponent = /** @class */ (function () {
        function BlockComponent(pages, blocks, resolver, viewContainerRef) {
            this.pages = pages;
            this.blocks = blocks;
            this.resolver = resolver;
            this.viewContainerRef = viewContainerRef;
        }
        /**
         * @return {?}
         */
        BlockComponent.prototype.loadComponent = /**
         * @return {?}
         */
        function () {
            if (!this.blocks.hasBlock(this.blockWrapper.identifier)) {
                throw new Error(this.blockWrapper.id + " has not been mapped (" + this.blockWrapper.identifier + ")");
            }
            /** @type {?} */
            var componentFactory = this.resolver.resolveComponentFactory(this.blocks.getBlock(this.blockWrapper.identifier));
            /** @type {?} */
            var componentRef = this.viewContainerRef.createComponent(componentFactory);
            this.pages.sortBlocks(this.block);
            // @ts-ignore
            componentRef.instance.block = this.block;
            componentRef.changeDetectorRef.detectChanges();
        };
        /**
         * @return {?}
         */
        BlockComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this.subscription = this.pages.getBlock(this.block.widgetId).then((/**
             * @param {?} block
             * @return {?}
             */
            function (block) {
                _this.blockWrapper = block;
                _this.loadComponent();
            }));
        };
        BlockComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-block',
                        template: "<!-- BLOCK -->",
                        changeDetection: core.ChangeDetectionStrategy.Default
                    }] }
        ];
        /** @nocollapse */
        BlockComponent.ctorParameters = function () { return [
            { type: PageService },
            { type: BlockService },
            { type: core.ComponentFactoryResolver },
            { type: core.ViewContainerRef }
        ]; };
        BlockComponent.propDecorators = {
            block: [{ type: core.Input }]
        };
        return BlockComponent;
    }());
    if (false) {
        /** @type {?} */
        BlockComponent.prototype.block;
        /** @type {?} */
        BlockComponent.prototype.blockWrapper;
        /** @type {?} */
        BlockComponent.prototype.subscription;
        /**
         * @type {?}
         * @protected
         */
        BlockComponent.prototype.pages;
        /**
         * @type {?}
         * @protected
         */
        BlockComponent.prototype.blocks;
        /**
         * @type {?}
         * @protected
         */
        BlockComponent.prototype.resolver;
        /**
         * @type {?}
         * @protected
         */
        BlockComponent.prototype.viewContainerRef;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/pipes/limitto.pipe.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TruncatePipe = /** @class */ (function () {
        function TruncatePipe() {
        }
        /**
         * @param {?} value
         * @param {?} args
         * @return {?}
         */
        TruncatePipe.prototype.transform = /**
         * @param {?} value
         * @param {?} args
         * @return {?}
         */
        function (value, args) {
            /** @type {?} */
            var limit = args || 10;
            /** @type {?} */
            var trail = '...';
            return value.length > limit ? value.substring(0, limit) + trail : value;
        };
        TruncatePipe.decorators = [
            { type: core.Pipe, args: [{
                        name: 'limitTo'
                    },] }
        ];
        return TruncatePipe;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/pages/page.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ɵ0$3 = { breadcrumbs: ['Page'] };
    var PageModule = /** @class */ (function () {
        function PageModule() {
        }
        PageModule.decorators = [
            { type: core.NgModule, args: [{
                        exports: [
                            BlockComponent,
                        ],
                        declarations: [
                            PageComponent,
                            TruncatePipe,
                            BlockComponent,
                            BlockAbstractComponent,
                            BlockWrapperComponent,
                            BlockLanguageComponent,
                            BlockParagraphComponent,
                            BlockParagraphLeadComponent,
                            BlockTableComponent,
                            BlockAccordionComponent,
                            BlockH1Component,
                            BlockH2Component,
                            BlockH3Component,
                            BlockH4Component,
                            BlockH5Component,
                            BlockH6Component,
                            BlockImageComponent,
                            BlockBreadcrumbsComponent,
                            BlockCarouselComponent,
                            BlockVideoComponent,
                            BlockBulletsComponent,
                            BlockNumberedComponent,
                            BlockBlockquoteComponent,
                            BlockDownloadComponent,
                            BlockButtonComponent,
                            BlockDividerComponent,
                            BlockProgressComponent,
                            BlockTabsComponent,
                            BlockSearchResultsComponent,
                            BlockSearchComponent,
                            BlockCompileDirective,
                        ],
                        imports: [
                            common.CommonModule,
                            router.RouterModule.forChild([
                                {
                                    path: ':identifier', component: PageComponent,
                                    data: ɵ0$3
                                }
                            ]),
                            expansion.MatExpansionModule,
                            list.MatListModule,
                            progressBar.MatProgressBarModule,
                            tabs$1.MatTabsModule,
                            SpinnerModule,
                            forms.ReactiveFormsModule,
                            forms.FormsModule
                        ],
                        entryComponents: [
                            BlockAbstractComponent,
                            BlockWrapperComponent,
                            BlockLanguageComponent,
                            BlockParagraphComponent,
                            BlockParagraphLeadComponent,
                            BlockTableComponent,
                            BlockAccordionComponent,
                            BlockH1Component,
                            BlockH2Component,
                            BlockH3Component,
                            BlockH4Component,
                            BlockH5Component,
                            BlockH6Component,
                            BlockImageComponent,
                            BlockBreadcrumbsComponent,
                            BlockCarouselComponent,
                            BlockVideoComponent,
                            BlockBulletsComponent,
                            BlockNumberedComponent,
                            BlockBlockquoteComponent,
                            BlockDownloadComponent,
                            BlockButtonComponent,
                            BlockDividerComponent,
                            BlockProgressComponent,
                            BlockTabsComponent,
                            BlockSearchResultsComponent,
                            BlockSearchComponent,
                        ],
                    },] }
        ];
        return PageModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/directives/stub-click.directive.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var StubClickDirective = /** @class */ (function () {
        function StubClickDirective() {
        }
        /**
         * @param {?} event
         * @return {?}
         */
        StubClickDirective.prototype.onMouseDown = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            event.preventDefault();
        };
        StubClickDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: '[coreStubClick]'
                    },] }
        ];
        StubClickDirective.propDecorators = {
            onMouseDown: [{ type: core.HostListener, args: ['click', ['$event'],] }]
        };
        return StubClickDirective;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/utils.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var UtilsModule = /** @class */ (function () {
        function UtilsModule() {
        }
        UtilsModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [StubClickDirective],
                        imports: [
                            common.CommonModule
                        ],
                        exports: [StubClickDirective]
                    },] }
        ];
        return UtilsModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/animations.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @return {?}
     */
    function makeSlideInOut() {
        return animations$1.trigger('slideInOut', [
            animations$1.state('in', animations$1.style({ height: '*', opacity: 0 })),
            animations$1.transition(':leave', [
                animations$1.style({ height: '*', opacity: 1 }),
                animations$1.group([
                    animations$1.animate('200ms ease-in-out', animations$1.style({ height: 0 })),
                    animations$1.animate('200ms ease-in-out', animations$1.style({ opacity: '0' }))
                ])
            ]),
            animations$1.transition(':enter', [
                animations$1.style({ height: '0', opacity: 0 }),
                animations$1.group([
                    animations$1.animate('200ms ease-in-out', animations$1.style({ height: '*' })),
                    animations$1.animate('200ms ease-in-out', animations$1.style({ opacity: '1' }))
                ])
            ])
        ]);
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/panels/panel/panel.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PanelComponent = /** @class */ (function () {
        function PanelComponent(headerClass, dialogs, el, renderer) {
            this.dialogs = dialogs;
            this.el = el;
            this.renderer = renderer;
            this.hasPannel = false;
            this.collapsible = false;
            this.collapsed = false;
            this.fullscreenable = false;
            this.headerClass = headerClass;
        }
        /**
         * @return {?}
         */
        PanelComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            if (typeof this.collapsed !== 'undefined') {
                this.collapsible = true;
            }
            if (typeof this.fullscreenIn !== 'undefined') {
                this.fullscreenable = true;
            }
            if (typeof this.closed !== 'undefined') {
                this.clossable = true;
            }
        };
        /**
         * @param {?} changes
         * @return {?}
         */
        PanelComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            // console.log(22, changes);
            // if (typeof changes.fullscreenIn !== 'undefined') {
            //   console.log('111', changes.fullscreenIn.currentValue, this.fullscreenable);
            // }
        };
        Object.defineProperty(PanelComponent.prototype, "pannelClasses", {
            get: /**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var classes = ['panel'];
                classes.push(this.collapsed ? 'panel-collapsed' : '');
                classes.push(this.fullscreenIn ? 'panel-fullscreen' : '');
                return classes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PanelComponent.prototype, "pannelContainerClasses", {
            get: /**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var classes = ['panel-container'];
                if (this.collapsible) {
                    // classes.push(this.collapsed ? 'collapse' : 'show');
                    classes.push(this.collapsed ? '' : 'show');
                }
                return classes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PanelComponent.prototype, "pannelContentClasses", {
            get: /**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var classes = ['panel-content'];
                return classes;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} $event
         * @return {?}
         */
        PanelComponent.prototype.toggleCollapse = /**
         * @param {?} $event
         * @return {?}
         */
        function ($event) {
            $event.preventDefault();
            this.collapsed = !this.collapsed;
        };
        /**
         * @param {?} $event
         * @return {?}
         */
        PanelComponent.prototype.toggleFullscreen = /**
         * @param {?} $event
         * @return {?}
         */
        function ($event) {
            $event.preventDefault();
            this.fullscreenIn = !this.fullscreenIn;
            handleClassCondition(this.fullscreenIn, 'panel-fullscreen', document.querySelector('body'));
        };
        /**
         * @param {?} $event
         * @return {?}
         */
        PanelComponent.prototype.closePanel = /**
         * @param {?} $event
         * @return {?}
         */
        function ($event) {
            return __awaiter(this, void 0, void 0, function () {
                var titleEl, title, result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            $event.preventDefault();
                            titleEl = this.el.nativeElement.querySelector('h1')
                                || this.el.nativeElement.querySelector('h2')
                                || this.el.nativeElement.querySelector('h3');
                            title = titleEl ? titleEl.innerText : '';
                            return [4 /*yield*/, this.dialogs.confirm({
                                    title: "<i class='fal fa-times-circle text-danger mr-2'></i>\n      Do you wish to delete panel <span class='fw-500'>&nbsp;'" + title + "'&nbsp;</span>?",
                                    message: "<span><strong>Warning:</strong> This action cannot be undone!</span>",
                                    buttons: {
                                        confirm: {
                                            label: 'Yes',
                                            className: 'btn-danger shadow-0'
                                        },
                                        cancel: {
                                            label: 'No',
                                            className: 'btn-default'
                                        }
                                    }
                                }).toPromise()];
                        case 1:
                            result = _a.sent();
                            if (result) {
                                this.renderer.addClass(this.el.nativeElement, 'd-none');
                            }
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * @param {?} headerClass
         * @return {?}
         */
        PanelComponent.prototype.setHeaderClass = /**
         * @param {?} headerClass
         * @return {?}
         */
        function (headerClass) {
            this.headerClass = headerClass;
        };
        PanelComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-panel',
                        template: "<div class=\"panel\" [ngClass]=\"pannelClasses\">\r\n  <div class=\"panel-hdr {{ headerClass }}\">\r\n    <ng-content select=\"[panelTitle]\" #panelTitle></ng-content>\r\n    <ng-content select=\"[panelToolbar]\"> </ng-content>\r\n    <div class=\"panel-toolbar\">\r\n      <button\r\n        *ngIf=\"collapsible\"\r\n        class=\"btn btn-panel\"\r\n        data-action=\"panel-collapse\"\r\n        (click)=\"toggleCollapse($event)\"\r\n        tooltip=\"Collapse\"\r\n      ></button>\r\n      <button\r\n        class=\"btn btn-panel\"\r\n        *ngIf=\"fullscreenable\"\r\n        data-action=\"panel-fullscreen\"\r\n        (click)=\"toggleFullscreen($event)\"\r\n        tooltip=\"Fullscreen\"\r\n      ></button>\r\n      <button\r\n        class=\"btn btn-panel\"\r\n        *ngIf=\"clossable\"\r\n        data-action=\"panel-close\"\r\n        (click)=\"closePanel($event)\"\r\n        tooltip=\"Close\"\r\n      ></button>      \r\n    </div>    \r\n  </div>\r\n\r\n  <div class=\"panel-container\" [ngClass]=\"pannelContainerClasses\">\r\n    <div\r\n      class=\"panel-content\"\r\n      [ngClass]=\"pannelContentClasses\"\r\n      *ngIf=\"!collapsed\"\r\n      [@slideInOut]\r\n    >\r\n      <ng-content select=\"[panelContent]\"> </ng-content>\r\n    </div>\r\n\r\n    <ng-content select=\"[panelFooter]\"> </ng-content>\r\n  </div>\r\n</div>\r\n",
                        animations: [makeSlideInOut()]
                    }] }
        ];
        /** @nocollapse */
        PanelComponent.ctorParameters = function () { return [
            { type: String, decorators: [{ type: core.Attribute, args: ['headerClass',] }] },
            { type: DialogsService },
            { type: core.ElementRef },
            { type: core.Renderer2 }
        ]; };
        PanelComponent.propDecorators = {
            collapsible: [{ type: core.Input }],
            collapsed: [{ type: core.Input }],
            fullscreenable: [{ type: core.Input }],
            fullscreenIn: [{ type: core.Input }],
            clossable: [{ type: core.Input }],
            closed: [{ type: core.Input }],
            panelTitle: [{ type: core.ContentChild, args: ['panelTitle', { static: true },] }]
        };
        return PanelComponent;
    }());
    if (false) {
        /** @type {?} */
        PanelComponent.prototype.hasPannel;
        /** @type {?} */
        PanelComponent.prototype.collapsible;
        /** @type {?} */
        PanelComponent.prototype.collapsed;
        /** @type {?} */
        PanelComponent.prototype.fullscreenable;
        /** @type {?} */
        PanelComponent.prototype.fullscreenIn;
        /** @type {?} */
        PanelComponent.prototype.clossable;
        /** @type {?} */
        PanelComponent.prototype.closed;
        /** @type {?} */
        PanelComponent.prototype.headerClass;
        /** @type {?} */
        PanelComponent.prototype.panelTitle;
        /**
         * @type {?}
         * @private
         */
        PanelComponent.prototype.dialogs;
        /**
         * @type {?}
         * @private
         */
        PanelComponent.prototype.el;
        /**
         * @type {?}
         * @private
         */
        PanelComponent.prototype.renderer;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/panels/panels.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PanelsModule = /** @class */ (function () {
        function PanelsModule() {
        }
        PanelsModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [PanelComponent],
                        imports: [
                            ngxBootstrap.TooltipModule,
                            common.CommonModule,
                            DialogsModule
                        ],
                        exports: [PanelComponent]
                    },] }
        ];
        return PanelsModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/gclog/gclogAngulartics.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AngularticsGCLOG = /** @class */ (function () {
        function AngularticsGCLOG(angulartics2) {
            this.angulartics2 = angulartics2;
        }
        /**
         * @return {?}
         */
        AngularticsGCLOG.prototype.startTracking = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this.angulartics2.pageTrack
                .pipe(this.angulartics2.filterDeveloperMode())
                .subscribe((/**
             * @param {?} x
             * @return {?}
             */
            function (x) { return _this.pageTrack(x.path); }));
            this.angulartics2.eventTrack
                .pipe(this.angulartics2.filterDeveloperMode())
                .subscribe((/**
             * @param {?} x
             * @return {?}
             */
            function (x) { return _this.eventTrack(x.action, x.properties); }));
            this.angulartics2.exceptionTrack
                .pipe(this.angulartics2.filterDeveloperMode())
                .subscribe((/**
             * @param {?} x
             * @return {?}
             */
            function (x) { return _this.exceptionTrack(x); }));
            this.angulartics2.setUsername
                .pipe(this.angulartics2.filterDeveloperMode())
                .subscribe((/**
             * @param {?} x
             * @return {?}
             */
            function (x) { return _this.setUsername(x); }));
        };
        /**
         * @param {?} path
         * @return {?}
         */
        AngularticsGCLOG.prototype.pageTrack = /**
         * @param {?} path
         * @return {?}
         */
        function (path) {
            try {
                gclog.addPageAction(path);
            }
            catch (err) {
            }
        };
        /**
         * @param {?} action
         * @param {?} properties
         * @return {?}
         */
        AngularticsGCLOG.prototype.eventTrack = /**
         * @param {?} action
         * @param {?} properties
         * @return {?}
         */
        function (action, properties) {
            try {
                gclog.addPageAction(action, properties);
            }
            catch (err) {
            }
        };
        /**
         * @param {?} properties
         * @return {?}
         */
        AngularticsGCLOG.prototype.exceptionTrack = /**
         * @param {?} properties
         * @return {?}
         */
        function (properties) {
            if (properties.fatal === undefined) {
                console.log('No "fatal" provided, sending with fatal=true');
                properties.exFatal = true;
            }
            properties.exDescription = properties.event ? properties.event.stack : properties.description;
            this.eventTrack("Exception thrown for " + properties.appName + " <" + properties.appId + "@" + properties.appVersion + ">", {
                category: 'Exception',
                label: properties.exDescription,
            });
        };
        /**
         * @param {?} userId
         * @return {?}
         */
        AngularticsGCLOG.prototype.setUsername = /**
         * @param {?} userId
         * @return {?}
         */
        function (userId) {
            try {
                gclog.setCustomAttribute('username', userId);
            }
            catch (err) {
            }
        };
        AngularticsGCLOG.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */
        AngularticsGCLOG.ctorParameters = function () { return [
            { type: angulartics2.Angulartics2 }
        ]; };
        /** @nocollapse */ AngularticsGCLOG.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function AngularticsGCLOG_Factory() { return new AngularticsGCLOG(core.ɵɵinject(angulartics2.Angulartics2)); }, token: AngularticsGCLOG, providedIn: "root" });
        return AngularticsGCLOG;
    }());
    if (false) {
        /**
         * @type {?}
         * @protected
         */
        AngularticsGCLOG.prototype.angulartics2;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/ui/on-off/on-off.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var OnOffComponent = /** @class */ (function () {
        function OnOffComponent() {
            this.checked = false;
            this.checkedChange = new core.EventEmitter();
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        OnOffComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            if (!changes.checked || changes.checked.currentValue === this.checked) {
                return;
            }
        };
        /**
         * @param {?} $event
         * @return {?}
         */
        OnOffComponent.prototype.onCheck = /**
         * @param {?} $event
         * @return {?}
         */
        function ($event) {
            $event.preventDefault();
            this.checked = !this.checked;
            this.checkedChange.emit(this.checked);
        };
        OnOffComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-on-off',
                        template: "\n        <a\n                href=\"#\"\n                (click)=\"onCheck($event)\"\n                class=\"btn btn-switch {{class}}\"\n                [class.active]=\"checked\"></a>\n    ",
                        host: {
                            class: 'd-inline-block'
                        },
                        changeDetection: core.ChangeDetectionStrategy.OnPush
                    }] }
        ];
        OnOffComponent.propDecorators = {
            checked: [{ type: core.Input }],
            class: [{ type: core.Input }],
            checkedChange: [{ type: core.Output }]
        };
        return OnOffComponent;
    }());
    if (false) {
        /** @type {?} */
        OnOffComponent.prototype.checked;
        /** @type {?} */
        OnOffComponent.prototype.class;
        /** @type {?} */
        OnOffComponent.prototype.checkedChange;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/utils/ui/ui.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var UiModule = /** @class */ (function () {
        function UiModule() {
        }
        UiModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [OnOffComponent],
                        imports: [
                            common.CommonModule
                        ],
                        exports: [OnOffComponent]
                    },] }
        ];
        return UiModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/navigation/navigation.selectors.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var selectNavigationState = store.createFeatureSelector('navigation');
    var ɵ0$4 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return state.items; };
    /** @type {?} */
    var selectNavigationItems = store.createSelector(selectNavigationState, (ɵ0$4));
    var ɵ1$2 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return ({
        active: state.filterActive,
        text: state.filterText
    }); };
    /** @type {?} */
    var selectFilter = store.createSelector(selectNavigationState, (ɵ1$2));
    var ɵ2$2 = /**
     * @param {?} state
     * @return {?}
     */
    function (state) { return ({
        active: state.filterActive && !!state.filterText.trim(),
        total: state.total,
        matched: state.matched
    }); };
    /** @type {?} */
    var selectResult = store.createSelector(selectNavigationState, (ɵ2$2));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core/settings/settings.selectors.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var selectSettingsState = store.createFeatureSelector('settings');

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core-frontend.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoreFrontendService = /** @class */ (function () {
        function CoreFrontendService() {
        }
        CoreFrontendService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        CoreFrontendService.ctorParameters = function () { return []; };
        /** @nocollapse */ CoreFrontendService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function CoreFrontendService_Factory() { return new CoreFrontendService(); }, token: CoreFrontendService, providedIn: "root" });
        return CoreFrontendService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core-frontend.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoreFrontendComponent = /** @class */ (function () {
        function CoreFrontendComponent(translate, translateProvider, router$1, gtm, gclog) {
            this.translate = translate;
            this.translateProvider = translateProvider;
            this.router = router$1;
            this.gtm = gtm;
            this.gclog = gclog;
            this.title = 'core-frontend';
            gtm.startTracking();
            gclog.startTracking();
            translateProvider.setDefaultLang(translate.getDefaultLanguage());
            translate.getLanguages().subscribe((/**
             * @param {?} languages
             * @return {?}
             */
            function (languages) {
                translateProvider.addLangs(languages.map((/**
                 * @param {?} language
                 * @return {?}
                 */
                function (language) {
                    return language.code;
                })));
            }));
            this.router.events.subscribe((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                if (event instanceof router.NavigationEnd) {
                    try {
                        gtm.pageTrack(event.urlAfterRedirects);
                        gclog.pageTrack(event.urlAfterRedirects);
                    }
                    catch (err) {
                        console.log(err);
                    }
                }
            }));
        }
        CoreFrontendComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'core-root',
                        template: '<router-outlet></router-outlet>'
                    }] }
        ];
        /** @nocollapse */
        CoreFrontendComponent.ctorParameters = function () { return [
            { type: TranslateService },
            { type: core$1.TranslateService },
            { type: router.Router },
            { type: gtm.Angulartics2GoogleTagManager },
            { type: AngularticsGCLOG }
        ]; };
        return CoreFrontendComponent;
    }());
    if (false) {
        /** @type {?} */
        CoreFrontendComponent.prototype.title;
        /**
         * @type {?}
         * @private
         */
        CoreFrontendComponent.prototype.translate;
        /**
         * @type {?}
         * @private
         */
        CoreFrontendComponent.prototype.translateProvider;
        /** @type {?} */
        CoreFrontendComponent.prototype.router;
        /** @type {?} */
        CoreFrontendComponent.prototype.gtm;
        /** @type {?} */
        CoreFrontendComponent.prototype.gclog;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core-routing.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ɵ0$5 = { breadcrumbs: ['Home'] };
    /** @type {?} */
    var routes = [
        {
            path: '',
            canActivate: [AuthService],
            children: [
                {
                    path: '', component: PageComponent,
                    data: ɵ0$5
                },
            ]
        },
    ];
    var CoreRoutingModule = /** @class */ (function () {
        function CoreRoutingModule() {
        }
        CoreRoutingModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            router.RouterModule.forRoot(routes)
                        ],
                        exports: [router.RouterModule]
                    },] }
        ];
        return CoreRoutingModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/core-frontend.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoreFrontendModule = /** @class */ (function () {
        function CoreFrontendModule() {
        }
        /**
         * @return {?}
         */
        CoreFrontendModule.forRoot = /**
         * @return {?}
         */
        function () {
            return {
                ngModule: CoreFrontendModule,
                providers: [
                    CacheService,
                    PageService,
                    BlockService,
                    TranslateService,
                ]
            };
        };
        CoreFrontendModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [
                            CoreFrontendComponent,
                        ],
                        imports: [
                            platformBrowser.BrowserModule,
                            CoreModule,
                            AuthModule,
                            PanelsModule,
                            animations.BrowserAnimationsModule,
                            expansion.MatExpansionModule,
                            core$1.TranslateModule,
                            PageModule,
                            SpinnerModule,
                            CoreRoutingModule,
                        ],
                        providers: [],
                        bootstrap: [CoreFrontendComponent],
                        exports: [
                            CoreModule,
                        ]
                    },] }
        ];
        return CoreFrontendModule;
    }());

    exports.APP_CONFIG = APP_CONFIG;
    exports.AngularticsGCLOG = AngularticsGCLOG;
    exports.AuthModule = AuthModule;
    exports.AuthService = AuthService;
    exports.Block = Block;
    exports.BlockAbstractComponent = BlockAbstractComponent;
    exports.BlockCompileDirective = BlockCompileDirective;
    exports.BlockService = BlockService;
    exports.CacheService = CacheService;
    exports.CoreFrontendComponent = CoreFrontendComponent;
    exports.CoreFrontendModule = CoreFrontendModule;
    exports.CoreFrontendService = CoreFrontendService;
    exports.CoreModule = CoreModule;
    exports.MfaModalComponent = MfaModalComponent;
    exports.NavigationEffects = NavigationEffects;
    exports.PageComponent = PageComponent;
    exports.PageModule = PageModule;
    exports.PageService = PageService;
    exports.PanelsModule = PanelsModule;
    exports.SearchService = SearchService;
    exports.SettingsActionTypes = SettingsActionTypes;
    exports.SpinnerModule = SpinnerModule;
    exports.SpinnerService = SpinnerService;
    exports.TranslateService = TranslateService;
    exports.UiModule = UiModule;
    exports.UtilsModule = UtilsModule;
    exports.activeUrl = activeUrl;
    exports.appReset = appReset;
    exports.factoryReset = factoryReset;
    exports.handleClassCondition = handleClassCondition;
    exports.initialState = initialState$1;
    exports.makeSlideInOut = makeSlideInOut;
    exports.mobileNavigation = mobileNavigation;
    exports.navigationFilter = navigationFilter;
    exports.reducer = reducer$1;
    exports.scrollToTop = scrollToTop;
    exports.selectFilter = selectFilter;
    exports.selectNavigationItems = selectNavigationItems;
    exports.selectNavigationState = selectNavigationState;
    exports.selectResult = selectResult;
    exports.selectSettingsState = selectSettingsState;
    exports.setGlobalFontSize = setGlobalFontSize;
    exports.settingsMetaReducer = settingsMetaReducer;
    exports.throwIfAlreadyLoaded = throwIfAlreadyLoaded;
    exports.toggleBiggerContentFont = toggleBiggerContentFont;
    exports.toggleBoxedLayout = toggleBoxedLayout;
    exports.toggleCleanPageBackground = toggleCleanPageBackground;
    exports.toggleDaltonism = toggleDaltonism;
    exports.toggleDisableCSSAnimation = toggleDisableCSSAnimation;
    exports.toggleFixedHeader = toggleFixedHeader;
    exports.toggleFixedNavigation = toggleFixedNavigation;
    exports.toggleFullscreen = toggleFullscreen;
    exports.toggleHideInfoCard = toggleHideInfoCard;
    exports.toggleHideNavigation = toggleHideNavigation;
    exports.toggleHideNavigationIcons = toggleHideNavigationIcons;
    exports.toggleHierarchicalNavigation = toggleHierarchicalNavigation;
    exports.toggleHighContrastText = toggleHighContrastText;
    exports.toggleLeanSubheader = toggleLeanSubheader;
    exports.toggleMinifyNavigation = toggleMinifyNavigation;
    exports.toggleNavSection = toggleNavSection;
    exports.toggleNavigationFilter = toggleNavigationFilter;
    exports.toggleNoOverlay = toggleNoOverlay;
    exports.toggleOffCanvas = toggleOffCanvas;
    exports.togglePreloaderInsise = togglePreloaderInsise;
    exports.togglePushContent = togglePushContent;
    exports.toggleRtl = toggleRtl;
    exports.toggleTopNavigation = toggleTopNavigation;
    exports.ɵa = reducers;
    exports.ɵb = localStorageSyncReducer;
    exports.ɵba = BlockH2Component;
    exports.ɵbb = BlockH3Component;
    exports.ɵbc = BlockH4Component;
    exports.ɵbd = BlockH5Component;
    exports.ɵbe = BlockH6Component;
    exports.ɵbf = BlockImageComponent;
    exports.ɵbg = BlockBreadcrumbsComponent;
    exports.ɵbh = BlockCarouselComponent;
    exports.ɵbi = BlockVideoComponent;
    exports.ɵbj = BlockBulletsComponent;
    exports.ɵbk = BlockNumberedComponent;
    exports.ɵbl = BlockBlockquoteComponent;
    exports.ɵbm = BlockDownloadComponent;
    exports.ɵbn = BlockButtonComponent;
    exports.ɵbo = BlockDividerComponent;
    exports.ɵbp = BlockProgressComponent;
    exports.ɵbq = BlockTabsComponent;
    exports.ɵbr = BlockSearchResultsComponent;
    exports.ɵbs = BlockSearchComponent;
    exports.ɵbt = StubClickDirective;
    exports.ɵbu = PanelComponent;
    exports.ɵbv = OnOffComponent;
    exports.ɵbw = CoreRoutingModule;
    exports.ɵc = metaReducers;
    exports.ɵd = effects;
    exports.ɵe = initialState;
    exports.ɵf = reducer;
    exports.ɵg = CustomSerializer;
    exports.ɵh = environment;
    exports.ɵi = DialogsModule;
    exports.ɵj = ConfirmDialogComponent;
    exports.ɵk = DialogsService;
    exports.ɵl = APIInterceptor;
    exports.ɵm = ApiService;
    exports.ɵn = ParserService;
    exports.ɵo = LoginComponent;
    exports.ɵp = SystemMessageService;
    exports.ɵq = SpinnerComponent;
    exports.ɵr = BlockComponent;
    exports.ɵs = TruncatePipe;
    exports.ɵt = BlockWrapperComponent;
    exports.ɵu = BlockLanguageComponent;
    exports.ɵv = BlockParagraphComponent;
    exports.ɵw = BlockParagraphLeadComponent;
    exports.ɵx = BlockTableComponent;
    exports.ɵy = BlockAccordionComponent;
    exports.ɵz = BlockH1Component;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=gc-core-frontend.umd.js.map
